import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeviceShowComponent }  from './slider/device-show.component';
import { CaseShowComponent }  	from './slider/case-show.component';
import { LayoutShowComponent }  from './layout/layout-show.component';
import { PhotosShowComponent }  from './photos/photos-show.component';
import { InstagramLoginComponent } from './photos/instagram-login.component';
import { LoadingShowComponent } from './loading/loading-show.component';

import { RoutesGuard }          from './shared';

// import { Ng2DndAppComponent }      from './ng2-dnd.component';

const routes: Routes = [
  { path: 'device',     component: DeviceShowComponent, canActivate: [RoutesGuard] },
  { path: 'case', 		component: CaseShowComponent,	canActivate: [RoutesGuard] },
  { path: 'layout', 	component: LayoutShowComponent, canActivate: [RoutesGuard] },
  { path: 'photos', 	component: PhotosShowComponent, canActivate: [RoutesGuard] },
  { path: 'ig-login',   component: InstagramLoginComponent },
  { path: 'loading',   component: LoadingShowComponent },
  { path: 'your-photos',  component: LayoutShowComponent },
  { path: 'initial-monogram',  component: LayoutShowComponent },
  { path: 'astrological',  component: LayoutShowComponent },
  { path: 'gift-cases',  component: LayoutShowComponent },
  { path: '', redirectTo: '/your-photos', pathMatch: 'full' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {
  
}
