$('.responsive').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 991,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});


var progressBarOptions = {
    startAngle: -1.55,
    size: 180,
    value: 0.75,
    fill: {
        color: '#e70e8a'
    }
}

$('.circle').circleProgress(progressBarOptions).on('circle-animation-progress', function(event, progress, stepValue) {
    //$(this).find('strong').text(String(stepValue.toFixed(2)).substr(1));
});
$('#circle-a').circleProgress({
    value: 0.25,
    fill: {
        color: '#e70e8a'
    }
});

$('#circle-b').circleProgress({
    value: 0.50,
    fill: {
        color: '#e70e8a'
    }
});

$('#circle-c').circleProgress({
    value: 0.75,
    fill: {
        color: '#e70e8a'
    }
});

$('#circle-d').circleProgress({
    value: 1.0,
    fill: {
        color: '#e70e8a'
    }
});


// var src1 = "{{url('public/Assets/frontend/images/star.png')}}";
// var src2 = "{{url('public/Assets/frontend/images/star1.png')}}";

var src1 = base_path+"/public/Assets/frontend/images/star.png";
var src2 = base_path+"/public/Assets/frontend/images/star1.png";
   
$("#star-input-1,#star-input-2,#star-input-3,#star-input-4,#star-input-5").rating({
    min: 0,
    max: 5,
    step: 0.5,
    showClear: false,
    showCaption: false,
    theme: 'krajee-fa',
    filledStar: '<i class="fa"><img src="'+src1+'"></i>',
    emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
});

$("#f_rate_instruction,#f_rate_navigate,#f_rate_reccommend").rating({
    min: 0,
    max: 5,
    step: 0.5,
    showClear: false,
    showCaption: false,
    theme: 'krajee-fa',
    filledStar: '<i class="fa"><img src="'+src1+'"></i>',
    emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
});


$(document).ready(function(){

  $(".d-arrow").click(function(){
      $(".my-live").css('display','block');
      $(".option-new").css('display','block');
  });
  $(".option-new").click(function(){
      $(".option-new").hide();
  });
});

// var txt = '<input type="text" name="text" id="autocomplete_head" required class="form-control manu-map" placeholder="Location"';
// var txt += 'oninvalid="this.setCustomValidity';
// var txt += "('Enter a location to see results close by') oninput='setCustomValidity()'";
// var txt += ">";




function masterchange(data){
    
    var txt = '<input type="text" name="text" id="autocomplete_head" class="form-control manu-map" placeholder="Location"';
     txt += 'oninvalid="this.setCustomValidity';
     txt += "('Enter a location to see results close by')";
     txt += '" oninput="setCustomValidity()">';
     // txt += "()'>";
    // var txt = '<input type="text" name="text" id="autocomplete_head" required class="form-control manu-map" placeholder="Location">';
     txt+='<input type="hidden" name="v_latitude" id="v_latitude_head">';
     txt+='<input type="hidden" name="v_longitude" id="v_longitude_head">';

  if(data=="online"){
    
    $("#mastersearchlocation").html("");
    $(".new").css("width","419px");
    $(".input-new").css("width","319px");
    $("#mastersearchtext").attr("placeholder", "Online");
    $("#mastersearchtext").attr("onkeyup", "onlinesearchtop(this.value)");
    $("#e_type").attr("value", "online");
    $(".searchdataclass").addClass("onlinecourseclass");
    


  }else if(data=="inperson"){
    
    $(".new").css("width","262px");
    $(".input-new").css("width","79%");
    $("#mastersearchlocation").html(txt);
    $("#mastersearchtext").attr("placeholder", "In Person");
    $("#mastersearchtext").attr("onkeyup", "inpersonssearchtop(this.value)");

    $("#e_type").attr("value", "inperson");
    $(".searchdataclass").removeClass("onlinecourseclass");
    initAutocomplete();
  
  }else if(data=="courses"){

    $(".new").css("width","419px");
    $(".input-new").css("width","319px");
    $("#mastersearchlocation").html("");
    $("#mastersearchtext").attr("placeholder", "Courses");
    $("#mastersearchtext").attr("onkeyup", "coursesearchtop(this.value)");

    $("#e_type").attr("value", "courses");
    $(".searchdataclass").addClass("onlinecourseclass");
 
  }

}


function confirmDelete( id , slug ) {
  $('#deleteId').val(id);
  $('#deleteSlug').val(slug);
  $('#deleteModal').modal('show');
}

function deleteAction() {
  var deleteId= $('#deleteId').val();
  var deleteSlug= $('#deleteSlug').val();
  window.location.href = deleteSlug;
}

function resendEmail(){
    var actionurl = base_path+"/account/resend-email";
    var formData = "";
    document.getElementById('load').style.visibility="visible"; 
    $.ajax({
        
        processData: false,
        contentType: false,
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            var obj = jQuery.parseJSON(res);
            document.getElementById('load').style.visibility='hidden';
            if(obj['status']==1){
                $("#cnemailid").html("Email sent.");  
            }
        },
        error: function ( jqXHR, exception ) {
            $("#commonerrmsg").css("display","inline");
        }
    });

}
    
function confirmMessage(){
    var actionurl = base_path+"/account/email-confirm-message";
    var formData = "";
    document.getElementById('load').style.visibility="visible"; 
    $.ajax({
        processData: false,
        contentType: false,
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            document.getElementById('load').style.visibility='hidden';
        },
        error: function ( jqXHR, exception ) {
            document.getElementById('load').style.visibility='hidden';
        }
    });

}
   



function footerAlign() {
  $('footer').css('display', 'block');
  $('footer').css('height', 'auto');
  var footerHeight = $('footer').outerHeight();
  $('body').css('padding-bottom', footerHeight);
  $('footer').css('height', footerHeight);
}


$(document).ready(function(){
  footerAlign();
});

$( window ).resize(function() {
  footerAlign();
});

$(document).ready( function() {
    /* Check width on page load*/
    if ( $(window).width() < 1199) {
        function matchHeights() {
          $('.matchHeights').each(function() {
            var elements = $(this).find('.matchheight_div');
            var height = 0;
            elements.css('min-height','0px');
            elements.each(function() {
              height = $(this).height() > height ? $(this).height() : height;
            });
            elements.css('min-height',height+'px');
          });
        };

        $(document).ready(function() {
          matchHeights();
        });

        $(window).resize(function() {
          matchHeights();
        });
    }
    else {}
 });

// $(document).ready( function() {
//     if ( $(window).width() > 768) {
//         function listview_height() {
//           $('.listview_height').each(function() {
//             var elements = $(this).find('.listview_height_div');
//             var height = 0;
//             elements.css('min-height','0px');
//             elements.each(function() {
//               height = $(this).height() > height ? $(this).height() : height;
//             });
//             elements.css('min-height',height+'px');
//           });
//         };

//         $(document).ready(function() {
//           listview_height();
//         });

//         $(window).resize(function() {
//           listview_height();
//         });
//     }
//     else {}
//  });

 function height_container() {
  $('.height_container').each(function() {
    var elements = $(this).find('.height_div');
    var height = 0;
    elements.css('min-height','0px');
    elements.each(function() {
      height = $(this).height() > height ? $(this).height() : height;
    });
    elements.css('min-height',height+'px');
  });
};


$(document).ready( function() {
    if ( $(window).width() > 768) {


       

        height_container();
        setInterval(function(){        
            height_container();
        },1000);


        // $(document).ready(function() {
        //   height_container();
        // setInterval(function(){        
        //     height_container();
        //   },1000);
        // });

        $(window).resize(function() {
          height_container();
          setInterval(function(){
            height_container();
          },1000);
        });
    }
    else {}
 });

$(document).ready( function() {
    if ( $(window).width() > 768) {
        function height_container_text() {
          $('.height_container_text').each(function() {
            var divHeight = $(".height_text").height();
            $(".height_img").css( "height", divHeight +"px");
          });
        };

        $(document).ready(function() {
          height_container_text();
      setInterval(function(){        
            height_container_text();
          },1000);
        });

        $(window).resize(function() {
          height_container();
          setInterval(function(){
            height_container();
          },1000);
        });
    }
    else {}
 });



(function( jQuery, undefined ) {

    jQuery.fn.extend({
        customScroll: function() {

            // measure the default scrollbar width.
            var tdv = jQuery( "<div><div></div></div>" )
                .css({
                    position: "absolute",
                    left: -1000,
                    width: 300,
                    overflow: "scroll"
                })
                .appendTo( "body" ),
                twi = tdv.width() - tdv.find( "div" ).width();
            tdv.remove();

            return this.each(function() {

                // decorate the element with a scrollbar
                var that = jQuery( this ),
                    he = that.outerHeight(),
                    wi = that.outerWidth();

                var sbp = that.css("direction") === "rtl" ? "left" : "right",
                    sbf = that.css("direction") === "rtl" ? "right" : "left",
                    scroller = jQuery( "<div>" )
                        .addClass( "phancy-scroller" )
                        .css({
                            overflow: "hidden",
                            position: "relative",
                            height: he,
                            width: wi,
                            marginTop: that.css( "margin-top" ),
                            marginBottom: that.css( "margin-bottom" ),
                            marginLeft: that.css( "margin-left" ),
                            marginRight: that.css( "margin-right" ),
                            float: that.css( "float" ),
                        }),
                    scrollarea = jQuery( "<div>" )
                        .css({
                            overflow: "scroll",
                            position: "relative",
                            height: he + twi,
                            width: wi + twi
                        })
                        .appendTo( scroller ),
                    scrollareax = jQuery( "<div>" )
                        .css({ float: sbf })
                        .appendTo( scrollarea );

                that
                    .css({
                        overflow: "visible",
                        height: "auto",
                        margin: 0,
                        float: ""
                    })
                    .after( scroller )
                    .appendTo( scrollareax );

                var nhe = scrollareax.outerHeight( true ),
                    ratio = Math.min( 1, he / nhe );
                if ( ratio >= 1 ) {
                    return;
                }

                // create scrollbars
                var scrollbar = jQuery( "<div>" )
                        .addClass( "phancy-scrollbar" )
                        .css({ height: he })
                        .css( sbp, 0 )
                        .appendTo( scroller ),

                    scrollbarbutton = jQuery( "<div>" )
                        .addClass( "phancy-scrollbarbutton" )
                        .css({ height: he * ratio })
                        .css( sbp, 0 )
                        .appendTo( scrollbar );
                that.css({ width: "-=" + scrollbar.css("width") });

                // bind events
                scroller.scroll(function() {
                    scroller.scrollLeft( 0 ).scrollTop( 0 ); /* http://stackoverflow.com/q/10036044 */
                });
                scrollarea.scroll(function() {
                    scrollbarbutton.css({
                        top: scrollarea.scrollTop() * ratio,
                        height: he * ratio
                    });
                });

                (function() {
                    var dragging = false,
                        pageY = null,
                        pageX = null,
                        top = null,
                        timer = null;

                    // scroll by clicking on scrollbar itself (page up and down).
                    scrollbar.on( "mousedown", function( e ) {
                        if ( e.which !== 1 || jQuery( e.target ).hasClass( "scrollbarbutton" ) )
                        {
                            return;
                        }
                        top = parseInt( scrollbarbutton.css( "top" ), 10  ) + ( he * ratio * ( e.pageY > scrollbarbutton.offset().top ? 1 : -1 ));
                        clearTimeout( timer );
                        timer = setTimeout(function() {
                            top = Math.min( Math.max( 0, e.pageY - scrollbar.offset().top ) - he * ratio / 2, he - ( he * ratio ) );
                            scrollbarbutton.css({ top: top });
                            scrollarea.scrollTop( Math.round( top / ratio ) );
                        }, 300);
                        scrollbarbutton.css({ top: top });
                        scrollarea.scrollTop( Math.round( top / ratio ) );
                        return false;
                    });

                    scrollbar.on("mouseup", function() {
                        clearTimeout( timer );
                    });

                    // scroll by clicking on scrollbar button (dragging).
                    scrollbarbutton.on("mousedown", function( e ) {
                        if ( e.which !== 1 )
                        {
                            return;
                        }
                        dragging = true;
                        pageY = e.pageY;
                        pageX = e.pageX;
                        top = parseInt( scrollbarbutton.css( "top" ), 10 );
                        jQuery( document ).on( "mousemove", function( e ) {
                            if ( dragging ) {
                                if ( Math.abs( e.pageX - pageX ) < 50 ) {
                                    var newtop = Math.min( Math.max( 0, top + e.pageY - pageY ), he - he * ratio );
                                    scrollbarbutton.css( "top", newtop );
                                    scrollarea.scrollTop( Math.round( newtop / ratio ) );
                                }
                                else {
                                    scrollarea.scrollTop( Math.round( top / ratio ) );
                                    scrollbarbutton.css({ top: top });
                                }
                                return false;
                            }
                            else {
                                jQuery( document ).unbind( "mousemove" );
                            }
                        });
                        return false;
                    });

                    jQuery( document ).on( "mouseup", function() {
                        if ( dragging ) {
                            dragging = false;
                            jQuery( document ).unbind( "mousemove" );
                            return false;
                        }
                    });
                })();
            });
        }
    });

})( jQuery );

function categoryviewdata(){
    $('html, body').animate({
        scrollTop: $("#skillcategory").offset().top
    }, 1000);
}



// $(".browser-cust").click(function() {
//     $('html, body').animate({
//         scrollTop: $("#skillcategory").offset().top
//     }, 1000);
// });

// $('a[href^="#"]').click(function(){

// var the_id = $(this).attr("href");

//     $('html, body').animate({
//         scrollTop:$(the_id).offset().top
//     }, 'slow');

// return false;});
