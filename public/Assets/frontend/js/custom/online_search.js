$('.responsive').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 991,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});


var progressBarOptions = {
    startAngle: -1.55,
    size: 180,
    value: 0.75,
    fill: {
        color: '#e70e8a'
    }
}

$('.circle').circleProgress(progressBarOptions).on('circle-animation-progress', function(event, progress, stepValue) {
    //$(this).find('strong').text(String(stepValue.toFixed(2)).substr(1));
});
$('#circle-a').circleProgress({
    value: 0.25,
    fill: {
        color: '#e70e8a'
    }
});

$('#circle-b').circleProgress({
    value: 0.50,
    fill: {
        color: '#e70e8a'
    }
});

$('#circle-c').circleProgress({
    value: 0.75,
    fill: {
        color: '#e70e8a'
    }
});

$('#circle-d').circleProgress({
    value: 1.0,
    fill: {
        color: '#e70e8a'
    }
});


// var src1 = "{{url('public/Assets/frontend/images/star.png')}}";
// var src2 = "{{url('public/Assets/frontend/images/star1.png')}}";

var src1 = base_path+"/public/Assets/frontend/images/star.png";
var src2 = base_path+"/public/Assets/frontend/images/star1.png";
   
$("#star-input-1,#star-input-2,#star-input-3,#star-input-4,#star-input-5").rating({
    min: 0,
    max: 5,
    step: 0.5,
    showClear: false,
    showCaption: false,
    theme: 'krajee-fa',
    filledStar: '<i class="fa"><img src="'+src1+'"></i>',
    emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
});

$("#f_rate_instruction,#f_rate_navigate,#f_rate_reccommend").rating({
    min: 0,
    max: 5,
    step: 0.5,
    showClear: false,
    showCaption: false,
    theme: 'krajee-fa',
    filledStar: '<i class="fa"><img src="'+src1+'"></i>',
    emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
});


$(document).ready(function(){

  $(".d-arrow").click(function(){
      $(".my-live").css('display','block');
      $(".option-new").css('display','block');
  });
  $(".option-new").click(function(){
      $(".option-new").hide();
  });
});

function masterchange(data){
  
  var txt = '<input type="text" name="text" class="form-control manu-map" placeholder="Location">';

  if(data=="online"){
    
    $("#mastersearchlocation").html("");
    $(".new").css("width","419px");
    $(".input-new").css("width","319px");
    $("#mastersearchtext").attr("placeholder", "Online");
    $("#e_type").attr("value", "online");


  }else if(data=="inperson"){
    
    $(".new").css("width","200px");
    $(".input-new").css("width","110px");
    $("#mastersearchlocation").html(txt);
    $("#mastersearchtext").attr("placeholder", "In Person");
    $("#e_type").attr("value", "inperson");
    //initAutocompleteHead();
  
  }else if(data=="courses"){

    $(".new").css("width","419px");
    $(".input-new").css("width","319px");
    $("#mastersearchlocation").html("");
    $("#mastersearchtext").attr("placeholder", "Courses");
    $("#e_type").attr("value", "courses");
 
  }

}


function confirmDelete( id , slug ) {
  $('#deleteId').val(id);
  $('#deleteSlug').val(slug);
  $('#deleteModal').modal('show');
}

function deleteAction() {
  var deleteId= $('#deleteId').val();
  var deleteSlug= $('#deleteSlug').val();
  window.location.href = deleteSlug;
}

function resendEmail(){
    
    var actionurl = base_path+"/account/resend-email";
    var formData = "";
    document.getElementById('load').style.visibility="visible"; 
    $.ajax({
        
        processData: false,
        contentType: false,
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            var obj = jQuery.parseJSON(res);
            document.getElementById('load').style.visibility='hidden';
            if(obj['status']==1){
                $("#cnemailid").html("Email sent.");  
            }
        },
        error: function ( jqXHR, exception ) {
            $("#commonerrmsg").css("display","inline");
        }
    });

}

function footerAlign() {
  $('footer').css('display', 'block');
  $('footer').css('height', 'auto');
  var footerHeight = $('footer').outerHeight();
  $('body').css('padding-bottom', footerHeight);
  $('footer').css('height', footerHeight);
}


$(document).ready(function(){
  footerAlign();
});

$( window ).resize(function() {
  footerAlign();
});