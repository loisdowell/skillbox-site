
function  searchhomepage(data) {
    var searchstr="";
    if(data=="inperson"){
        searchstr+='<div class="search-to-find" id="inpersonmasterskill">';
        searchstr+='<input type="text" list="browsers" class="form-control" name="text" placeholder="Find your perfect skill or job" onkeyup="inpersonssearch(this.value)">';
        searchstr+='<datalist id="onlinesearch"></datalist>';
        searchstr+='</div>';
        searchstr+='<div class="Location-line" id="inpersonmasterlocation">';
        searchstr+='<input type="text" class="form-control" name="text" id="autocomplete" placeholder="Location"  autocomplete="off"';
        searchstr += 'oninvalid="this.setCustomValidity';
        searchstr += "('Enter a location to see results close by')";
        searchstr += '" oninput="setCustomValidity()" >';
        searchstr+='<input type="hidden" name="v_latitude" id="v_latitude">';
        searchstr+='<input type="hidden" name="v_longitude" id="v_longitude">';
        searchstr+='</div>';
    }else if(data=="online"){
        searchstr+='<div class="search-to-find" id="onlinemaster">';
        searchstr+='<input type="text" list="onlinesearch" onkeyup="onlinesearch(this.value)" class="form-control form-online" name="keyword" placeholder="Find your perfect skill or job">';
        searchstr+='<datalist id="onlinesearch"></datalist>';
        searchstr+='</div>';
    
    }else{
        searchstr+='<div class="search-to-find" id="coursesmaster">';
        searchstr+='<input type="text" list="coursesearch" onkeyup="coursesearch(this.value)" class="form-control form-online" name="keyword" placeholder="Find your perfect course">';
        searchstr+='<datalist id="coursesearch"></datalist>';
        searchstr+='</div>';
    }
    $("#searchcontent").html(searchstr);
    initAutocomplete();
}




function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

function moreCategory(data){

    document.getElementById('load').style.visibility="visible"; 
    var data = $("#cids").val();
    var actionurl = base_path+"/more-category";
    var formData = "ids="+data;
    
    $.ajax({
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            
            var obj = jQuery.parseJSON(res);
            $("#directservicemore").append(obj['inpersonstr']);
            $("#onlineservicemore").append(obj['onlinestr']);
            $("#cids").val(obj['ids']);
            if(obj['onlinestr']==0){
                $("#btnstatus").html("");
            }
            document.getElementById('load').style.visibility='hidden';
        },
        error: function ( jqXHR, exception ) {
        }
    });
}