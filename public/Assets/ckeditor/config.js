/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E'; 
		
		
		// Link dialog, "Browse Server" button
		config.filebrowserBrowseUrl = 'plugins/ckfinder/ckfinder.html';

		// Image dialog, "Browse Server" button
		config.filebrowserImageBrowseUrl = 'plugins/ckfinder/ckfinder.html?type=Images';
		// Flash dialog, "Browse Server" button
		config.filebrowserFlashBrowseUrl = 'plugins/ckfinder/ckfinder.html?type=Flash';
		// Upload tab in the Link dialog
		config.filebrowserUploadUrl = 'plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
//		config.filebrowserUploadUrl = '../../public/ckeditor';
		// Upload tab in the Image dialog
		config.filebrowserImageUploadUrl = 'plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
//		config.filebrowserImageUploadUrl = '../../public/ckeditor';
		// Upload tab in the Flash dialog
		config.filebrowserFlashUploadUrl = 'plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
		
		config.contentsCss = ['../css/skel-noscript.css'];
		
		config.allowedContent= true;
/*
		config.filebrowserBrowseUrl= "{{ asset('js/ckeditor/plugins/ckfinder/ckfinder.html?Type=Files') }}";
	    //config.filebrowserImageBrowseUrl= "{{ asset('js/ckeditor/plugins/ckfinder/ckfinder.html?Type=Images') }}";
	    config.filebrowserFlashBrowseUrl= "{{ asset('js/ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash') }}";
	    config.filebrowserUploadUrl= "{{ asset('js/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}";
	    config.filebrowserImageUploadUrl= "{{ asset('js/ckeditor/plugins/ckfinder/core/connctor/php/connector.php?command=QuickUpload&type=Images') }}";
	    config.filebrowserFlashUploadUrl= "{{ asset('js/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}";
*/

		// Construct path to file upload route
		// Useful if your dev and prod URLs are different

};