-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 31, 2017 at 12:57 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopify_unite_boost`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_general_settings`
--

CREATE TABLE `tbl_general_settings` (
  `id` int(11) NOT NULL,
  `v_store` varchar(255) NOT NULL,
  `v_key` varchar(255) NOT NULL,
  `v_value` varchar(255) NOT NULL,
  `d_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_installs_shops`
--

CREATE TABLE `tbl_installs_shops` (
  `id` int(11) NOT NULL,
  `v_store` varchar(255) NOT NULL,
  `v_nonce` varchar(255) NOT NULL,
  `v_accesstoken` varchar(300) NOT NULL,
  `d_last_login` datetime NOT NULL,
  `d_added` datetime NOT NULL,
  `d_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_installs_shops`
--

INSERT INTO `tbl_installs_shops` (`id`, `v_store`, `v_nonce`, `v_accesstoken`, `d_last_login`, `d_added`, `d_modified`) VALUES
(1, 'raynanda.myshopify.com', 'vnhBo2KmuH11Tt9bGfgC', '491965c5e17edd5652451e1632571817', '2017-07-29 11:37:38', '2017-07-12 09:14:09', '2017-07-12 09:14:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_offer`
--

CREATE TABLE `tbl_offer` (
  `id` int(11) NOT NULL,
  `v_store` varchar(255) NOT NULL,
  `e_type` enum('countdown','just_take','share_to_get') NOT NULL,
  `e_offer` enum('cart_discount','free_gift','free_ship') NOT NULL,
  `v_discount_code` varchar(255) NOT NULL,
  `v_discount_in` varchar(255) NOT NULL,
  `v_discount_value` varchar(255) NOT NULL,
  `v_product_ids` varchar(255) NOT NULL,
  `v_subject_font` varchar(255) NOT NULL,
  `v_subject_fontsize` varchar(255) NOT NULL,
  `v_subject_color` varchar(255) NOT NULL,
  `v_subject_bold` varchar(255) NOT NULL,
  `v_subject_italic` varchar(255) NOT NULL,
  `v_subject_underline` varchar(255) NOT NULL,
  `v_subject_text` varchar(255) NOT NULL,
  `v_message_font` varchar(255) NOT NULL,
  `v_message_fontsize` varchar(255) NOT NULL,
  `v_message_color` varchar(255) NOT NULL,
  `v_message_bold` varchar(255) NOT NULL,
  `v_message_italic` varchar(255) NOT NULL,
  `v_message_underline` varchar(255) NOT NULL,
  `l_message_text` longtext NOT NULL,
  `d_added` datetime NOT NULL,
  `d_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_translation`
--

CREATE TABLE `tbl_translation` (
  `id` int(11) NOT NULL,
  `v_store` varchar(255) NOT NULL,
  `v_key` varchar(255) NOT NULL,
  `v_value` varchar(255) NOT NULL,
  `d_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_general_settings`
--
ALTER TABLE `tbl_general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_installs_shops`
--
ALTER TABLE `tbl_installs_shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_offer`
--
ALTER TABLE `tbl_offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_translation`
--
ALTER TABLE `tbl_translation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_general_settings`
--
ALTER TABLE `tbl_general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_installs_shops`
--
ALTER TABLE `tbl_installs_shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_offer`
--
ALTER TABLE `tbl_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_translation`
--
ALTER TABLE `tbl_translation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
