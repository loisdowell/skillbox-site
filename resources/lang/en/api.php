<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'common'=> [
        'incorrectEmailPassword' => 'Invalid email and password.',
        'keyExist'               => ':section Already Exist Please Use Different.',
        'detailAdded'            => ':section details has been successfully inserted.',
        'detailUpdated'          => ':section details has been successfully updated.',
        'detailDeleted'          => ':section details has been successfully deleted.',
        'somethingWrong'         => 'Something Went Wrong',
        'passwordNotMatch'       => 'Password and confirm password did not match.',
        'emailAndPassword'       => 'Email Address and Password does not match.',
        'checkEmail'             => 'Please check your email for reset your password.',
        'emailNotExist'          => 'Your email address doesn’t exist in our system.',
        'requestExpired'         => 'Your reset password request has expired.',
        'reseAccount'            => 'Your account password has been reset successfully.',
        'adminNotExist'          => 'Admin dose not exist.',
        'store'                  => ':section has been added.',
        'update'                 => ':section has been updated.',
        'statusActive'           => 'Status of :section has been changed to Active.',
        'statusInactive'         => 'Status of :section has been changed to Inactive.',
        'statusOpen'             => 'Status of :section has been changed to Open.',
        'statusClose'            => 'Status of :section has been changed to Close.',
        'delete'                 => ':section has been deleted.',
        'noRecords'              => 'No Records Found.',
        'sitemap'                => 'Sitemap has been generate successfully.',
        'onlyImage'              => 'Only jpeg or png allowed.',
        'onlyVideo'              => 'Only avi or mov or mp4 allowed.',
        'onlyDocs'               => 'Only pdf or docs or xls allowed.'
    ],

    'login'=> [
        'requireEmail'       => 'Email is required.',
        'requirePassword'    => 'Password is required.',
        'requireDeviceToken' => 'Device Token is required.',
        'invalidCredentials' => 'Invalid login detail.',
        'loginSuccess'       => 'Login Successful.',
        'logoutSuccess'      => 'Logout Successful.',
        'invalidToken'       => 'Invalid authentication token.',
        'forgotPasswordSuccess'      => 'Email has been sent successfully.',
        'invalidEmail'      => 'Invalid Email.',
    ],

    'signUp'=> [
        
    ],


    'moreOption' =>[
        'summary'        => 'Dashboard Summary',
        'supportList'    => 'Support List',
        'myMoney'        => 'MY Money',
        'shortListSkill' => 'ShortList Skills List',
        'shortListJob'   => 'ShortList Jobs List',
        'appliedJob'     => 'Applied Jobs List',
        'myCourses'      => 'My Courses List',
        'emailExist'     => 'Email Already Exist Please Use Different.',
        'updateProfile'  => 'Profile detail has been update successfully.',
    ],

    'message' =>[
        'list'      => 'Message List',
        'detail'    => 'Message detail List',
        'add'       => 'Message details has been successfully sent',
    ],

    "jobs" => [
        'myJobsList' => 'My Jobs List',
        'endjob' => 'Job detail has been ended successfully.',
        'addToShortList' => 'job successfully shortlisted.',
        'removeToShortList' => 'Successfully removed from shortlisted job.',
        'appiedJob' =>'Successfully applied for job.',
        'messageSent' =>'Successfully sent message.',
        'postjob' =>'Successfully post job.',
        
    ]

   


];
