<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'common'=> [
        'incorrectEmailPassword' => 'Invalid email and password.',
        'keyExist'          => ':section Already Exist Please Use Different.',
        'detailAdded'       => ':section details has been successfully inserted.',
        'detailUpdated'     => ':section details has been successfully updated.',    
        'detailDeleted'     => ':section details has been successfully deleted.',
        'somethingWrong'    => 'Something Went Wrong',
        'passwordNotMatch'  => 'Password and confirm password did not match.',
        'emailAndPassword'  => 'Email Address and Password does not match.',
        'checkEmail'        => 'Please check your email for reset your password.',
        'emailNotExist'     => 'Your email address doesn’t exist in our system.',
        'requestExpired'    => 'Your reset password request has expired.',
        'reseAccount'       => 'Your account password has been reset successfully.',
        'adminNotExist'     => 'Admin dose not exist.',
        'store'             => ':section has been added.',
        'update'            => ':section has been updated.',
        'statusActive'      => 'Status of :section has been changed to Active.',
        'statusInactive'    => 'Status of :section has been changed to Inactive.',
        'statusOpen'        => 'Status of :section has been changed to Open.',
        'statusClose'       => 'Status of :section has been changed to Close.',
        'delete'            => ':section has been deleted.',
        'noRecords'         => 'No Records Found.',
        'sitemap'           => 'Sitemap has been generate successfully.',
        'onlyImage'         => 'Only jpeg or png allowed.',
        'onlyVideo'         => 'Only avi or mov or mp4 allowed.',
        'onlyDocs'          => 'Only pdf or docs or xls allowed.'
    ],


    'loginpage'=> [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],


];
