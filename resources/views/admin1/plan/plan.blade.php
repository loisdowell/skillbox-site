@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Plan
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">plan</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} new Plan</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            
            <div class="box-body">
              <br/><br/>
               <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/plan/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                
                <div class="row">
                  
                    
                    <div class="col-md-6">
                     
                      <div class="form-group">
                          <label>Title<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_name" id="v_name" class="form-control" placeholder="Title" value="{{$data->v_name or old('v_name')}}" required="" data-parsley-trigger="keyup">
                      </div>

                      <div class="form-group">
                          <label>Sub Title<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_subtitle" id="v_subtitle" class="form-control" placeholder="Sub Title" value="{{$data->v_subtitle or old('v_subtitle')}}" required="" data-parsley-trigger="keyup">
                      </div>

                      <div class="form-group">
                          <label>Status<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="active" @if( isset($data->e_status) && $data->e_status == 'active' ) selected @endif>Active</option>
                            <option value="inactive" @if( isset($data->e_status ) && $data->e_status == 'inactive') SELECTED @endif >Inactive</option>
                          </select>
                      </div>
                      
                      <div class="form-group">
                          <h2><b>Bullets</b></h2>
                          @if($view == 'add')
                          <div class="input-group control-group after-add-more">
                          <input type="text" name="l_bullet[]" id="l_bullet" class="form-control" placeholder="Bullets" value="" >
                          @endif
                            <div class="input-group-btn"> 
                              <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                            </div>
                          @if($view == 'add') 
                          </div>
                          @endif

                          
                          @if($view == 'edit')
                          <div id="copy1" style="width:100%">
                          @php 
                          $i = 0;
                          @endphp

                          @if(isset($data->l_bullet))
                          @foreach($data->l_bullet as $val)

                          @php 
                          $i++;
                          @endphp
                         
                          <div class="control-group input-group" id="controledit{{$i}}" style="margin-top:10px"><input type="text" name="l_bullet[]" id="l_bullet{{$i}}" class="form-control" value="{{$val}}"><div class="input-group-btn"><button class="btn btn-danger remove" id="remove{{$i}}" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button></div></div>
                           
                           @endforeach
                           @endif
                           </div>
                          @endif

                          <div id="copy">
                          </div>
                      </div>
                      
                      <?php /*
                      <div class="form-group">
                          <h2><b>AddOn</b></h2>
                          
                          <?php
                              $addoneditcnt=1;
                          ?>

                          @if(isset($data->l_addon['text']) && count($data->l_addon['text']))
                            <div id="copyaddon1">
                            @foreach($data->l_addon['text'] as $key=>$val)
                              
                              
                              <div class="input-group control-group after-add-more" id="controladdonedit{{$addoneditcnt}}" style="margin-top: 10px">
                                  <input type="text" name="l_addon[text][]" id="AddOntext" class="form-control" value="{{$val}}" placeholder="AddOn Text">
                                  <input type="text" name="l_addon[price][]" id="AddOnprice" class="form-control" value="{{$data->l_addon['price'][$key]}}" placeholder="AddOn Price">
                                  <div class="input-group-btn">
                                    <button class="btn btn-danger remove" id="remove{{$addoneditcnt}}" type="button"><i class="glyphicon glyphicon-remove"></i>Remove</button>
                                  </div>
                              </div>
                              

                            <?php 
                                $addoneditcnt =$addoneditcnt+1;
                            ?>  

                            @endforeach
                            </div>
                          @else

                              <div class="input-group control-group after-add-more" id="controladdon1" style="margin-top: 10px">
                                  <input type="text" name="l_addon[text][]" id="AddOntext" class="form-control" value="" placeholder="AddOn Text">
                                  <input type="text" name="l_addon[price][]" id="AddOnprice" class="form-control" value="" placeholder="AddOn Price">
                                  <div class="input-group-btn">
                                    <button class="btn btn-danger remove" id="remove1" type="button"><i class="glyphicon glyphicon-remove"></i>Remove</button>
                                  </div>
                              </div>

                          @endif

                          <div id="copyaddon">
                          </div>

                          <div class="input-group-btn"> 
                            <button class="btn btn-success add-more-addon" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                          </div>
                      </div>
                      */ ?>

                    </div>
                    
                    <div class="col-md-6">
                      
                      <div class="form-group">
                        <label>Monthly Price  <span class="asterisk_input">*</span></label>
                        <input type="text" name="f_monthly_price" id="f_monthly_price" class="form-control" placeholder="Monthly Price" value="{{$data->f_monthly_price or old('f_monthly_price')}}" required="" data-parsley-trigger="keyup" data-parsley-type="number">
                      </div>

                      <div class="form-group">
                        <label>Monthly Offer Price<span class="asterisk_input">*</span></label>
                        <input type="text" name="f_monthly_dis_price" id="f_monthly_dis_price" class="form-control" placeholder="Monthly Offer Price" value="{{$data->f_monthly_dis_price or old('f_monthly_dis_price')}}" required="" data-parsley-trigger="keyup" data-parsley-type="number">
                      </div>
                      
                    </div>
                
                </div>

                 
                  <div class="clear:both"></div> 
                        
                      <div class="row">
                        <div class="col-md-12">
                            <a href="{{url('admin/plan')}}">
                            <button type="button" class="btn btn-warning">Back to List</button>
                            </a>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                      </div> 
                  </div>
                  
              </form>
              
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>
          </div>
        </div>
      </section>      
              
    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <div class="box-header pull-right">
              <a href="{{url('admin/plan/add/0')}}">
                 <!-- <button type="submit" class="btn btn-info">Add New Plan</button> -->
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Monthly Price ( £ )</th>
                  <!-- <th>Monthly Offer Price ( £ )</th> -->
                  <th>Yearly Price ( £ )</th>
                  <!-- <th>Yearly Offer Price ( £ )</th> -->
                  <th>Description</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      <td>{{$value->v_name or ''}}</td>
                      <td>{{$value->f_monthly_price or ''}}</td>
                      <!-- <td>{{$value->f_monthly_dis_price or ''}}</td> -->
                      <td>{{$value->f_yearly_price or ''}}</td>
                      <!-- <td>{{$value->f_yearly_dis_price or ''}}</td> -->
                      <td>
                      @if(isset($value->l_bullet))
                      <ul>
                      @foreach($value->l_bullet as $val)
                      <li>{{$val}}</li>
                      @endforeach
                       </ul>
                      @endif
                      </td>
                      <td>
                        @if(isset($value->e_status) && $value->e_status=="active")
                            <span class="badge bg-green">Active</span>         
                        @else
                            <span class="badge bg-yellow">Inactive</span>
                        @endif
                      </td>
                      <td>
                        <a href="{{url('admin/plan/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/plan',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if( $value->e_status == 'inactive' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                        @php $a=url('admin/plan/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','{{$a}}')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
</script>

<script type="text/javascript">

jQuery(document).ready(function(){
  var i = 0;
  var j = 0;
  
  jQuery(".add-more").click(function(){ 
    i++;
        jQuery("#copy").append('<div class="control-group input-group" id="control'+i+'" style="margin-top:10px"><input type="text" name="l_bullet[]" id="l_bullet'+i+'" class="form-control" value=""><div class="input-group-btn"><button class="btn btn-danger remove" id="remove'+i+'" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button></div></div>');
    });

   jQuery("#copy").on("click",".remove",function(){ 
    var id=  jQuery(this).parent().parent().attr("id");
       jQuery("#"+id).remove();
   });

    jQuery("#copy1").on("click",".remove",function(){ 
    var id=  jQuery(this).parent().parent().attr("id");
       jQuery("#"+id).remove();
   });




    jQuery(".add-more-addon").click(function(){ 
        j++;

        var a= '<div class="input-group control-group after-add-more"  id="controladdon" style="margin-top: 10px"><input type="text" name="l_addon[text][]" id="AddOntext'+i+'" class="form-control" value="" placeholder="AddOn Text"><input type="text" name="l_addon[price][]" id="AddOnprice'+i+'" class="form-control" value="" placeholder="AddOn Price"><div class="input-group-btn"><button class="btn btn-danger remove" id="remove'+i+'" type="button"><i class="glyphicon glyphicon-remove"></i>Remove</button></div></div>'
        jQuery("#copyaddon").append(a);

    });


    jQuery("#copyaddon").on("click",".remove",function(){ 
      var id=  jQuery(this).parent().parent().attr("id");
         jQuery("#"+id).remove();
    });

    jQuery("#copyaddon1").on("click",".remove",function(){ 
      alert("sssss");  
      var id=  jQuery(this).parent().parent().attr("id");
         jQuery("#"+id).remove();
    });






}); 

</script>

@stop

