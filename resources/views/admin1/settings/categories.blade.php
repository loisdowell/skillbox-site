@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Categories
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/sitesetting')}}">Settings</a></li>
        <li class="active">categories</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} new Category</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            
            <div class="box-body">
              <br/><br/>
                <div class="row">
                   <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/categories/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Name<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_name" id="v_name" class="form-control" placeholder="Name" value="{{$data->v_name or old('v_name')}}" required="" data-parsley-trigger="keyup">
                      </div>
                      <div class="form-group">
                          <label>Sub headline<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_sub_headline" id="v_sub_headline" class="form-control" placeholder="Sub headline" value="{{$data->v_sub_headline or old('v_sub_headline')}}" required="" data-parsley-trigger="keyup">
                      </div>

                      @if ($view=="edit")
                      <div class="form-group">
                        <div><label>Thumb image [300*150]</label></div>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px;">
                             
                              @if(isset($data->v_image) && $data->v_image != '' )
                                 @php
                                    $imgdata="";
                                    if(Storage::disk('s3')->exists($data->v_image)){
                                        $imgdata = \Storage::cloud()->url($data->v_image);      
                                    }
                                 @endphp 
                                <img src="{{$imgdata}}" style="max-width: 150px" alt="" />
                              @else
                                 @php
                                    $imgdata = \Storage::cloud()->url('common/no-image.png');      
                                 @endphp 
                                <img src="{{$imgdata}}" alt="" />  
                              @endif

                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                            <div>
                               <span class="btn default btn-file">
                                  <span class="fileinput-new"> Select Image </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="file" name="v_image" > </span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                      </div>
                      <div class="form-group">
                        <div><label>Main image [1200*300]</label></div>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px;">
                             
                              @if(isset($data->v_main_image) && count($data->v_main_image) && $data->v_main_image != '' )
                                 @php
                                    $imgdata="";
                                    if(Storage::disk('s3')->exists($data->v_main_image)){
                                        $imgdata = \Storage::cloud()->url($data->v_main_image);      
                                    }
                                 @endphp 
                                <img src="{{$imgdata}}" style="max-width: 150px" alt="" />
                              @else
                                 @php
                                    $imgdata = \Storage::cloud()->url('common/no-image.png');      
                                 @endphp 
                                <img src="{{$imgdata}}" alt="" />  
                              @endif

                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                            <div>
                               <span class="btn default btn-file">
                                  <span class="fileinput-new"> Select Image </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="file" name="v_main_image" > </span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                      </div>
                      @else
                      <div class="form-group">
                        <div><label>Thumb image [430*125]</label></div>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                            <div>
                               <span class="btn default btn-file">
                                  <span class="fileinput-new"> Select Image </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="file" name="v_image" required> </span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                      </div>

                      <div class="form-group">
                        <div><label>Main image [1905*670]</label></div>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                            <div>
                               <span class="btn default btn-file">
                                  <span class="fileinput-new"> Select Image </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="file" name="v_main_image" required> </span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                      </div>
                      @endif
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Category Type</label>
                          <select class="form-control" id="v_type" name="v_type" >
                            <option value="">- select -</option>
                              <option value="inperson" @if( isset($data->v_type) && $data->v_type == 'inperson' ) selected @endif>In Person</option>
                              <option value="online" @if( isset($data->v_type ) && $data->v_type == 'online') SELECTED @endif >Online</option>
                              {{-- <option value="course" @if( isset($data->v_type ) && $data->v_type == 'course') SELECTED @endif >Course</option> --}}
                          </select>
                      </div>
                      <div class="form-group">
                          <label>Status<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="active" @if( isset($data->e_status) && $data->e_status == 'active' ) selected @endif>Active</option>
                            <option value="inactive" @if( isset($data->e_status ) && $data->e_status == 'inactive') SELECTED @endif >Inactive</option>
                          </select>
                      </div>

                 </div>
                  </div>
                  <div class="clear:both"></div>                
                        
                        <div class="row">
                          <div class="col-md-12">
                              <a href="{{url('admin/categories')}}">
                              <button type="button" class="btn btn-warning">Back to List</button>
                              </a>
                              <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                      </div> 
                    </div>
                    </form>
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>
          </div>
        </div>
      </section>      
              
    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <div class="box-header pull-right">
              <a href="{{url('admin/categories/add/0')}}">
                 <button type="submit" class="btn btn-info">Add New Category</button>
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Parent Category</th>
                  <th>Added Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      <td>
                        @if(isset($value->v_image) && $value->v_image != '' )
                          @php
                            $imgdata="";
                            if(Storage::disk('s3')->exists($value->v_image)){
                                $imgdata = \Storage::cloud()->url($value->v_image);      
                            }
                          @endphp 
                          <img src="{{$imgdata}}" height="50" width="50" alt="" />
                        @else
                           @php
                              $imgdata = \Storage::cloud()->url('common/no-image.png');      
                           @endphp 
                          <img src = "{{$imgdata}}" height="50" width="50" alt="" />  
                        @endif
                      </td>
                      
                      <td>{{$value->v_name or ''}}</td>
                      <td>
                      <?php 
                      $v_type = '';
                      if($value->v_type == "inperson"){
                          $v_type ="In Person";
                      }
                      elseif($value->v_type == "online"){
                          $v_type ="Online";
                      }
                      elseif($value->v_type == "course"){
                          $v_type ="Course";
                      }
                      echo $v_type;
                      ?>
                      </td>
                      <td>{{ isset($value->d_added) ? date('M d Y' ,strtotime($value->d_added)) : '' }}</td>
                      <td>
                        <a href="{{url('admin/categories/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/categories',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if( $value->e_status == 'inactive' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                        @php $a=url('admin/categories/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" onclick="confirmDelete('{{ $value->id }}','<?php echo $a ?>')" title="Delete" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
</script>

@stop

