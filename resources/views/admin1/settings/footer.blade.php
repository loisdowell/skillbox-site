@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Footer Menu
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/sitesetting')}}">Settings</a></li>
        <li class="active">Footer Menu</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} Footer Menu</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            
            <div class="box-body">
              <br/><br/>
                <div class="row">
                   <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/footer/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Mwnu Title<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_title" id="v_title" class="form-control" placeholder="Name" value="{{$data->v_title or old('v_title')}}" required="" data-parsley-trigger="keyup">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Select Page<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="i_page_id" name="i_page_id" required="">
                            <option value="">- select -</option>
                            @if(isset($page) && count($page))
                              @foreach($page as $k=>$val)
                                  <option value="{{$val->id}}" @if( isset($data->i_page_id) && $data->i_page_id==$val->id) selected @endif>{{$val->v_title}}</option>
                              @endforeach
                            @endif
                          </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Select Column<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="v_column" name="v_column" required="">
                            <option value="">- select -</option>
                            <option value="1" @if( isset($data->v_column) && $data->v_column == 'active' ) selected @endif>Column 1</option>
                            <option value="2" @if( isset($data->v_column ) && $data->v_column == 'inactive') SELECTED @endif >Column 2</option>
                            <option value="3" @if( isset($data->v_column) && $data->v_column == 'active' ) selected @endif>Column 3</option>
                            <option value="4" @if( isset($data->v_column ) && $data->v_column == 'inactive') SELECTED @endif >Column 4</option>
                          </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Status<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="active" @if( isset($data->e_status) && $data->e_status == 'active' ) selected @endif>Active</option>
                            <option value="inactive" @if( isset($data->e_status ) && $data->e_status == 'inactive') SELECTED @endif >Inactive</option>
                          </select>
                      </div>
                    </div>
                  </div>
                  <div class="clear:both"></div>                
                        
                        <div class="row">
                          <div class="col-md-12">
                              <a href="{{url('admin/footer')}}">
                              <button type="button" class="btn btn-warning">Back to List</button>
                              </a>
                              <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                      </div> 
                    </div>
                    </form>
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>
          </div>
        </div>
      </section>      
              
    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <div class="box-header pull-right">
              <a href="{{url('admin/footer/add/0')}}">
                 <button type="submit" class="btn btn-info">Add New Footer Menu</button>
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Menu Title</th>
                  <th>Page</th>
                  <th>Column</th>
                  <th>Status</th>
                  <th>Added date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      <td>{{$value->v_title or ''}}</td>
                      <?php 
                          $v_title="";
                          if(count($value->hasPage()) && isset($value->hasPage()->v_title)){
                              $v_title = $value->hasPage()->v_title;
                          }
                      ?> 
                      <td>{{$v_title or ''}}</td>
                      <td>Column {{$value->v_column or ''}}</td>
                      <td>
                        @if(isset($value->e_status) && $value->e_status=="active")
                            <span class="badge bg-green">Active</span>         
                        @else
                            <span class="badge bg-yellow">Inactive</span>
                        @endif
                      </td>
                      <td>{{ isset($value->d_added) ? date('M d Y' ,strtotime($value->d_added)) : '' }}</td>
                      <td>
                        <a href="{{url('admin/footer/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/footer',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if( $value->e_status == 'inactive' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                        @php $a=url('admin/footer/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','{{$a}}')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
</script>

@stop

