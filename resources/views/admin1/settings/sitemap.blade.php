@extends('layouts.master')

@section('title')
	{{$section or 'Sitemap'}}
@stop

@section('css')
	<link href="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
	<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />

	<style type="text/css">
		table.table-bordered thead tr{border: 1px solid #000 !important; }
		table.table-bordered tbody th, table.table-bordered tbody td{
		border: 1px solid #ebebeb;
		}
		.table-bordered{
			border: 1px solid #ddd !important;
		}
		.box-title {
			display: inline-block;
			font-size: 29px !important;
			line-height: 1;
			margin: 0;
		}
		.asterisk_input{
			color: #e32;
		}
	</style>
@stop

@section('model')
@stop

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
			<div class="breadcrumbs">
				<h1>Sitemap</h1>
				<ol class="breadcrumb pull-right">
					<li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="{{url('admin/sitemap')}}">Sitemap</a></li>
					<li class="active">Sitemap</li>
				</ol>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
					<span class="sr-only">Toggle Navigation</span>
					<span class="toggle-icon">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</span>
				</button>
			</div>
		</section>
		<div style="clear: both"></div>
		<div class="col-xs-12">         
			@if ($success = Session::get('success'))
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
				</div>
			@endif
			@if ($warning = Session::get('warning'))
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
				</div>
			@endif

		</div>  
		<div style="clear: both"></div>     
		<section class="content">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Sitemap</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
					</div>
				</div>
				<div class="box-body">
					<form class="horizontal-form" name="employment-edit" id="employment-edit" enctype="multipart/form-data" action="{{url('admin/sitemap/generate')}}" method="POST" data-parsley-validate>
						{{ csrf_field() }}
						
						<div class="row">

							{{-- PAGE SITEMAP  --}}

							<div class="col-md-12" style="margin: 20px 0 15px 0">
								<div class="col-md-12"><h3>Pages</h3></div>
							</div>

							<div class="col-md-12">

								<div class="form-group col-md-6">
									<label>Priority</label>
									<select class="form-control" id="priority_page" name="priority_page" >
										<option value="0.1">0.1</option>
										<option value="0.2">0.2</option>
										<option value="0.3">0.3</option>
										<option value="0.4">0.4</option>
										<option value="0.5" selected="selected">0.5</option>
										<option value="0.6">0.6</option>
										<option value="0.7">0.7</option>
										<option value="0.8">0.8</option>
										<option value="0.9">0.9</option>
										<option value="1.0">1.0</option>
									</select>
								</div>
								
								<div class="form-group col-md-6">
									<label>Changefreq</label>
									<select class="form-control" id="changefreq_page" name="changefreq_page" >
										<option value="always">Always</option>
										<option value="hourly">Hourly</option>
										<option value="daily" selected="selected">Daily</option>
										<option value="weekly">Weekly</option>
										<option value="monthly">Monthly</option>
										<option value="yearly">Yearly</option>
										<option value="never">Never</option>
									</select>
								</div>
								
							</div>

							
							{{-- SITEMAP MEMBERSHIP  --}}

							<div class="col-md-12" style="margin: 20px 0 15px 0">
								<div class="col-md-12"><h3>Blog</h3></div>
							</div>

							<div class="col-md-12">
								<div class="form-group col-md-6">
									<label>Priority</label>
									<select class="form-control" id="priority_blog" name="priority_blog" >
										<option value="0.1">0.1</option>
										<option value="0.2">0.2</option>
										<option value="0.3">0.3</option>
										<option value="0.4">0.4</option>
										<option value="0.5" selected="selected">0.5</option>
										<option value="0.6">0.6</option>
										<option value="0.7">0.7</option>
										<option value="0.8">0.8</option>
										<option value="0.9">0.9</option>
										<option value="1.0">1.0</option>
									</select>
								</div>
								
								<div class="form-group col-md-6">
									<label>Changefreq</label>
									<select class="form-control" id="changefreq_blog" name="changefreq_blog" >
										<option value="always">Always</option>
										<option value="hourly">Hourly</option>
										<option value="daily" selected="selected">Daily</option>
										<option value="weekly">Weekly</option>
										<option value="monthly">Monthly</option>
										<option value="yearly">Yearly</option>
										<option value="never">Never</option>
									</select>
								</div>

							</div>



							{{-- LOGIN SITEMAP  --}}

							<div class="col-md-12" style="margin: 20px 0 15px 0">
								<div class="col-md-12"><h3>Login</h3></div>
							</div>

							<div class="col-md-12">
								<div class="form-group col-md-6">
									<label>Priority</label>
									<select class="form-control" id="priority_login" name="priority_login" >
										<option value="0.1">0.1</option>
										<option value="0.2">0.2</option>
										<option value="0.3">0.3</option>
										<option value="0.4">0.4</option>
										<option value="0.5" selected="selected">0.5</option>
										<option value="0.6">0.6</option>
										<option value="0.7">0.7</option>
										<option value="0.8">0.8</option>
										<option value="0.9">0.9</option>
										<option value="1.0">1.0</option>
									</select>
								</div>
								
								<div class="form-group col-md-6">
									<label>Changefreq</label>
									<select class="form-control" id="changefreq_login" name="changefreq_login" >
										<option value="always">Always</option>
										<option value="hourly">Hourly</option>
										<option value="daily" selected="selected">Daily</option>
										<option value="weekly">Weekly</option>
										<option value="monthly">Monthly</option>
										<option value="yearly">Yearly</option>
										<option value="never">Never</option>
									</select>
								</div>
								
							</div>
							{{-- LINKS  --}}


							{{-- FORGOT PASSWORD  --}}

							<div class="col-md-12" style="margin: 20px 0 15px 0">
								<div class="col-md-12"><h3>Forgot Password</h3></div>
							</div>

							<div class="col-md-12">
								<div class="form-group col-md-6">
									<label>Priority</label>
									<select class="form-control" id="priority_forgotpassword" name="priority_forgotpassword" >
										<option value="0.1">0.1</option>
										<option value="0.2">0.2</option>
										<option value="0.3">0.3</option>
										<option value="0.4">0.4</option>
										<option value="0.5" selected="selected">0.5</option>
										<option value="0.6">0.6</option>
										<option value="0.7">0.7</option>
										<option value="0.8">0.8</option>
										<option value="0.9">0.9</option>
										<option value="1.0">1.0</option>
									</select>
								</div>
								
								<div class="form-group col-md-6">
									<label>Changefreq</label>
									<select class="form-control" id="changefreq_forgotpassword" name="changefreq_forgotpassword" >
										<option value="always">Always</option>
										<option value="hourly">Hourly</option>
										<option value="daily" selected="selected">Daily</option>
										<option value="weekly">Weekly</option>
										<option value="monthly">Monthly</option>
										<option value="yearly">Yearly</option>
										<option value="never">Never</option>
									</select>
								</div>
								
							</div>
							{{-- FAQ --}}
							<div class="col-md-12" style="margin: 20px 0 15px 0">
								<div class="col-md-12"><h3>FAQ</h3></div>
							</div>

							<div class="col-md-12">
								<div class="form-group col-md-6">
									<label>Priority</label>
									<select class="form-control" id="priority_faq" name="priority_faq" >
										<option value="0.1">0.1</option>
										<option value="0.2">0.2</option>
										<option value="0.3">0.3</option>
										<option value="0.4">0.4</option>
										<option value="0.5" selected="selected">0.5</option>
										<option value="0.6">0.6</option>
										<option value="0.7">0.7</option>
										<option value="0.8">0.8</option>
										<option value="0.9">0.9</option>
										<option value="1.0">1.0</option>
									</select>
								</div>
								
								<div class="form-group col-md-6">
									<label>Changefreq</label>
									<select class="form-control" id="changefreq_faq" name="changefreq_faq" >
										<option value="always">Always</option>
										<option value="hourly">Hourly</option>
										<option value="daily" selected="selected">Daily</option>
										<option value="weekly">Weekly</option>
										<option value="monthly">Monthly</option>
										<option value="yearly">Yearly</option>
										<option value="never">Never</option>
									</select>
								</div>
								
							</div>


							<div class="col-md-12" style="margin: 20px 0 15px 0">
								<div class="col-md-12"><h3>Skillbox Job</h3></div>
							</div>

							<div class="col-md-12">
								<div class="form-group col-md-6">
									<label>Priority</label>
									<select class="form-control" id="priority_skillboxjob" name="priority_skillboxjob" >
										<option value="0.1">0.1</option>
										<option value="0.2">0.2</option>
										<option value="0.3">0.3</option>
										<option value="0.4">0.4</option>
										<option value="0.5" selected="selected">0.5</option>
										<option value="0.6">0.6</option>
										<option value="0.7">0.7</option>
										<option value="0.8">0.8</option>
										<option value="0.9">0.9</option>
										<option value="1.0">1.0</option>
									</select>
								</div>
								
								<div class="form-group col-md-6">
									<label>Changefreq</label>
									<select class="form-control" id="changefreq_skillboxjob" name="changefreq_skillboxjob" >
										<option value="always">Always</option>
										<option value="hourly">Hourly</option>
										<option value="daily" selected="selected">Daily</option>
										<option value="weekly">Weekly</option>
										<option value="monthly">Monthly</option>
										<option value="yearly">Yearly</option>
										<option value="never">Never</option>
									</select>
								</div>
								
							</div>


							<div class="col-md-12" style="margin: 20px 0 15px 0">
								<div class="col-md-12"><h3>Courses</h3></div>
							</div>

							<div class="col-md-12">
								<div class="form-group col-md-6">
									<label>Priority</label>
									<select class="form-control" id="priority_course" name="priority_course" >
										<option value="0.1">0.1</option>
										<option value="0.2">0.2</option>
										<option value="0.3">0.3</option>
										<option value="0.4">0.4</option>
										<option value="0.5" selected="selected">0.5</option>
										<option value="0.6">0.6</option>
										<option value="0.7">0.7</option>
										<option value="0.8">0.8</option>
										<option value="0.9">0.9</option>
										<option value="1.0">1.0</option>
									</select>
								</div>
								
								<div class="form-group col-md-6">
									<label>Changefreq</label>
									<select class="form-control" id="changefreq_course" name="changefreq_course" >
										<option value="always">Always</option>
										<option value="hourly">Hourly</option>
										<option value="daily" selected="selected">Daily</option>
										<option value="weekly">Weekly</option>
										<option value="monthly">Monthly</option>
										<option value="yearly">Yearly</option>
										<option value="never">Never</option>
									</select>
								</div>
								
							</div>


							<div class="col-md-12" style="margin: 20px 0 15px 0">
								<div class="col-md-12"><h3>Online Skill and job</h3></div>
							</div>

							<div class="col-md-12">
								<div class="form-group col-md-6">
									<label>Priority</label>
									<select class="form-control" id="priority_online" name="priority_online" >
										<option value="0.1">0.1</option>
										<option value="0.2">0.2</option>
										<option value="0.3">0.3</option>
										<option value="0.4">0.4</option>
										<option value="0.5" selected="selected">0.5</option>
										<option value="0.6">0.6</option>
										<option value="0.7">0.7</option>
										<option value="0.8">0.8</option>
										<option value="0.9">0.9</option>
										<option value="1.0">1.0</option>
									</select>
								</div>
								
								<div class="form-group col-md-6">
									<label>Changefreq</label>
									<select class="form-control" id="changefreq_online" name="changefreq_online" >
										<option value="always">Always</option>
										<option value="hourly">Hourly</option>
										<option value="daily" selected="selected">Daily</option>
										<option value="weekly">Weekly</option>
										<option value="monthly">Monthly</option>
										<option value="yearly">Yearly</option>
										<option value="never">Never</option>
									</select>
								</div>
								
							</div>

							<div class="col-md-12" style="margin: 20px 0 15px 0">
								<div class="col-md-12"><h3>Inperson Skill and job</h3></div>
							</div>

							<div class="col-md-12">
								<div class="form-group col-md-6">
									<label>Priority</label>
									<select class="form-control" id="priority_inperson" name="priority_inperson" >
										<option value="0.1">0.1</option>
										<option value="0.2">0.2</option>
										<option value="0.3">0.3</option>
										<option value="0.4">0.4</option>
										<option value="0.5" selected="selected">0.5</option>
										<option value="0.6">0.6</option>
										<option value="0.7">0.7</option>
										<option value="0.8">0.8</option>
										<option value="0.9">0.9</option>
										<option value="1.0">1.0</option>
									</select>
								</div>
								
								<div class="form-group col-md-6">
									<label>Changefreq</label>
									<select class="form-control" id="changefreq_inperson" name="changefreq_inperson" >
										<option value="always">Always</option>
										<option value="hourly">Hourly</option>
										<option value="daily" selected="selected">Daily</option>
										<option value="weekly">Weekly</option>
										<option value="monthly">Monthly</option>
										<option value="yearly">Yearly</option>
										<option value="never">Never</option>
									</select>
								</div>
								
							</div>


							
							

							

							

						
						<div class="clear:both"></div>                  
						<div class="row">
							<div class="col-md-12">
								<a href="{{url('admin/sitemap')}}">
									<button type="button" class="btn btn-warning">Cancel</button>
								</a>
								<button type="submit" class="btn btn-primary">Update</button>
							</div>
						</div> 
					</form>
					<div class="box-footer">
					   Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
					</div>
				</div>
			</div>
		</section>
	</div>
@stop

@section('js')
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script>
  $(function () {
	$('#adminlisttable').DataTable({
	  "paging": true,
	  
	});
  });

  $('#v_title').change(function() {
	
	var string = $('#v_title').val();
	var $v_key = '';
	var trimmed = $.trim(string);

	$v_key = trimmed.replace(/[^a-z0-9-]/gi, '-')
					.replace(/-+/g, '-')
					.replace(/^-|-$/g, '');

	$('#v_key').val($v_key.toLowerCase());

	return true;
});

function setHiddden(dFormate){
	var dFormate = dFormate;
	$("#SITE_DATE_FORMATE_PHP").val(dFormate);
}
</script>

@stop