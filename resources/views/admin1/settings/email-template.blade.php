@extends('layouts.master')

@section('title')
	{{$section or 'Edit Email Template'}}
@stop

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />

@stop



@section('content')
	
<div class="content-wrapper">
     <section class="content-header">
      <h1>
        Email Templates Managment
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/sitesetting')}}">Settings</a></li>
        <li class="active">email template</li>
      </ol>
    </section>

    
    @if($view=="add" || $view=="edit")
    	<div style="clear: both"></div>
    	<div class="col-xs-12">	      	
      		@if ($success = Session::get('success'))
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
				</div>
			@endif
			@if ($warning = Session::get('warning'))
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
				</div>
			@endif

		</div>	
		<div style="clear: both"></div>		
    	<section class="content">
	   		
	   		<div class="box box-default">
		        <div class="box-header with-border">
		          	<h3 class="box-title">{{ucfirst($view)}} New Email Templates</h3>
		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            	<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
		          	</div>
		        </div>
		        
		        <div class="box-body">
		         	<br/><br/>
		          	<div class="row">
		           
					<form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/email-template/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
						{{ csrf_field() }}
						
						<div class="col-md-6">
			         		
			         		{{-- v_title --}}
							<div class="form-group">
								<label>Title <span class="font-red">*</span></label>
								<input type="text" name="v_title" id="v_title" class="form-control" placeholder="Title" value="{{$data->v_title or old('v_title')}}" required="" data-parsley-trigger="keyup">
								
							</div>

							{{-- i_header_id --}}
							<div class="form-group">
								<label>Template Header <span class="font-red">*</span></label>
								<select class="form-control" id="i_header_id" name="i_header_id" required="" data-parsley-trigger="keyup">
									<option value="">- no header -</option>
									@if(isset($TemplateHeader) && count($TemplateHeader))
									@foreach($TemplateHeader as $key => $val)
									<option value="{{ $val->id }}" @if( isset($data->i_header_id) && $data->i_header_id == $val->id ) selected @endif>{{ $val->v_title }}</option>
									@endforeach
									@endif
								</select>
							</div>

							<div class="form-group">
								<label>Type<span class="font-red">*</span></label>
								<select class="form-control" id="e_type" name="e_type" required="" data-parsley-trigger="keyup">
									<option value="">- select -</option>
									<option value="buyer" @if( isset($data->e_type) && $data->e_type == 'buyer' ) selected @endif>Buyer</option>
									<option value="seller" @if( isset($data->e_type) && $data->e_type == 'seller' ) selected @endif>Seller</option>
									<option value="admin" @if( isset($data->e_type) && $data->e_type == 'admin' ) selected @endif>Admin</option>
									<option value="both" @if( isset($data->e_type) && $data->e_type == 'both' ) selected @endif>Both</option>
								</select>
								</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Subject <span class="font-red">*</span></label>
								<input type="text" name="v_subject" id="v_subject" class="form-control" placeholder="Subject" value="{{$data->v_subject or old('v_subject')}}" required="" data-parsley-trigger="keyup">
							
							</div>
							
							<div class="form-group">
								<label>Template Footer <span class="font-red">*</span></label>
								<select class="form-control" id="i_footer_id" name="i_footer_id" required="" data-parsley-trigger="keyup">
									<option value="">- no footer -</option>
									@if(isset($TemplateFooter) && count($TemplateFooter))
									@foreach($TemplateFooter as $key => $val)
									<option value="{{ $val->id }}" @if( isset($data->i_footer_id) && $data->i_footer_id == $val->id ) selected @endif>{{ $val->v_title }}</option>
									@endforeach
									@endif
								</select>
							</div>
						</div>
						<div class="col-md-12">		
							{{-- v_from--}}
							{{--
							<div class="form-group">
								<label>From <span class="font-red">*</span></label>
								<input type="email" name="v_from" id="v_from" class="form-control" placeholder="From Email" value="{{$data->v_from or old('v_from')}}" required="" data-parsley-trigger="keyup">
								
							</div>
							--}}

							{{-- l_body --}}
							<div class="form-group">
								<label>Content <span class="font-red">*</span>
								
								</label>
								<textarea name="l_body" class="form-control ckeditor" id="l_body">{{$data->l_body or old('l_body')}}</textarea>
								
							</div>


						</div>
						<div class="col-md-12">	

							{{-- e_status --}}
							<div class="form-group">
								<label>Status <span class="font-red">*</span></label>
								<select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
									<option value="">- select -</option>
									<option value="active" @if( isset($data->e_status) && $data->e_status == 'active' ) selected @endif>Active</option>
									<option value="inactive" @if( isset($data->e_status) && $data->e_status == 'inactive' ) selected @endif>Inactive</option>
								</select>
								</div>
							</div>
			           </div>
					<div class="clear:both"></div>		          	
		          	
		          	<div class="row">
			          	<div class="col-md-12">
			                <a href="{{url('admin/email-template')}}">
			                <button type="button" class="btn btn-warning">Back to List</button>
			                </a>
			                <button type="submit" class="btn btn-primary">Save</button>
			            </div>
			        </div> 
		        </div>
		        </form>
		        <div class="box-footer">
		           Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
		        </div>
		      </div>
		    </div>
	    </section>      
	            
    @else
	    <section class="content">
	      <div class="row">
				
				<div class="col-xs-12">	      	
		      		@if ($success = Session::get('success'))
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
						</div>
					@endif
					@if ($warning = Session::get('warning'))
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
						</div>
					@endif

				</div>	
		    <div class="col-xs-12">
	          <div class="box">
	            
	            <div class="box-header pull-right">
					<a href="{{url('admin/email-template/add/0')}}">
					<button type="submit" class="btn btn-info">Add New Email Templates</button>
					</a>
				</div>
	            <div style="clear: both;"></div>
	            

	            <div class="box-body" >
	              <table id="adminlisttable" class="table table-striped table-bordered table-hover order-column dataTable ">
	                <thead>
		                <tr>
		                  	<th>Title</th>
							<th>Subject</th>
							<th>Type</th>
							<th>Status</th>
							<th>Actions</th>
		                </tr>
	                </thead>

	                <tbody>
	                	@if( isset($data) && count($data) )
							@foreach( $data as $count => $value )
								<tr>
									<td>{{$value->v_title or ''}}</td>
									<td>{{$value->v_subject or ''}}</td>
									<td>
										@if(isset($value->e_type))
											{{ucfirst($value->e_type)}}
										@else
											-
										@endif
									</td>
									<td>
										 @if(isset($value->e_status) && $value->e_status=="active")
				                        <span class="badge bg-green">Active</span>         
				                      @else
				                        <span class="badge bg-yellow">Inactive</span>
				                      @endif
									</td>
									<td>
										<a href="{{url('admin/email-template/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
										<a href="{{url('admin/email-template',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
											@if( $value->e_status == 'inactive' )
												<i title="Activate" class="fa fa-check"></i>
											@else
												<i title="Inactivate" class="fa fa-times"></i>
											@endif
										</a>
										@php $a=url('admin/email-template/action/delete/').'/'.$value->id; @endphp
										<a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','{{$a}}')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
										
									</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="7" align="center">@lang('message.noRecords')</td>
							</tr>
						@endif
	                
	                </tbody>
	                
	              </table>
	            </div>
	          </div>
	        </div>
	      </div>
	    </section>
    @endif


  </div>


@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/ckeditor/ckeditor/ckeditor.js')}}" ></script>
		

<script>
$(document).load(function() {
		CKEDITOR.replace( 'ckeditor' );
	});
  $(function () {
    $('#adminlisttable').DataTable({
      "paging": true,
      
    });
  });

  $('#v_title').change(function() {
  	
	var string = $('#v_title').val();
	var $v_key = '';
	var trimmed = $.trim(string);

	$v_key = trimmed.replace(/[^a-z0-9-]/gi, '-')
					.replace(/-+/g, '-')
					.replace(/^-|-$/g, '');

	$('#v_key').val($v_key.toLowerCase());

	return true;
});


</script>

@stop
