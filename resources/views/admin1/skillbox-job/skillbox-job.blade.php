@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop


@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        {{$section or ''}}
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/skillbox-job')}}">{{$section or ''}}</a></li>
        <li class="active">{{$section or ''}}</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
        <div class="col-xs-12">    
          <div style="clear: both"></div>     
              @if ($success = Session::get('success'))
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
            </div>
          @endif
          @if ($warning = Session::get('warning'))
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
            </div>
          @endif

        </div>  
    
      <div style="clear: both"></div>   
        <section class="content">
          <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} Job</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            
            <div class="box-body">
              <br/><br/>
                <div class="row">
                   
                   <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/skillbox-job/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    
                    {{ csrf_field() }}
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Category<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="i_category_id" name="i_category_id" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            @if(isset($category) && count($category))
                              @foreach($category as $key => $val)
                                <option value="{{$val->id or ''}}" @if( isset($data->i_category_id) && $data->i_category_id == $val->id ) selected @endif>{{$val->v_title or ''}}</option>
                              @endforeach
                            @endif
                          </select>
                      </div>
                      <div class="form-group">
                          <label>Title<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_title" id="v_title" class="form-control" placeholder="Name" value="{{$data->v_title or old('v_title')}}" required="" data-parsley-trigger="keyup">
                      </div>
                      <div class="form-group">
                          <label>Slug<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_slug" id="v_slug" class="form-control" placeholder="Slug" value="{{$data->v_slug or old('v_slug')}}" required="" data-parsley-trigger="keyup">
                      </div>
                      <div class="form-group">
                          <label>Country<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_country" id="v_country" class="form-control" placeholder="Country" value="{{$data->v_country or old('v_country')}}" required="" data-parsley-trigger="keyup">
                      </div>

                    </div>
                    
                    <div class="col-md-6">

                      <div class="form-group">
                          <label>City<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_city" id="v_city" class="form-control" placeholder="City" value="{{$data->v_city or old('v_city')}}" required="" data-parsley-trigger="keyup">
                      </div>

                      <div class="form-group">
                          <label>Job Time<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="v_job_time" name="v_job_time" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="Full Time" @if( isset($data->v_job_time) && $data->v_job_time == 'Full Time' ) selected @endif>Full Time</option>
                            <option value="Part Time" @if( isset($data->v_job_time ) && $data->v_job_time == 'Part Time') SELECTED @endif >Part Time</option>
                          </select>
                      </div>

                      <div class="form-group">
                        <label>short Description <span class="font-red">*</span>
                        </label>
                        <textarea name="v_short_description" class="form-control" style="    margin: 0px 4px 0px 0px width: 491px; height: 109px" id="v_short_description">{{$data->v_short_description or old('v_short_description')}}</textarea>
                        
                      </div>

                    </div>

                    <div class="col-md-12">

                      <div class="form-group">
                        <label>Description <span class="font-red">*</span>
                        </label>
                        <textarea name="l_description" class="form-control ckeditor" id="l_description">{{$data->l_description or old('l_description')}}</textarea>
                        
                      </div>

                      <div class="form-group">
                          <label>Status<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="active" @if( isset($data->e_status) && $data->e_status == 'active' ) selected @endif>Active</option>
                            <option value="inactive" @if( isset($data->e_status ) && $data->e_status == 'inactive') SELECTED @endif >Inactive</option>
                          </select>
                      </div>

                    </div>
                </div>
                <div class="clear:both"></div>                
                <div class="row">
                  <div class="col-md-12">
                      <a href="{{url('admin/skillbox-job')}}">
                      <button type="button" class="btn btn-warning">Back to List</button>
                      </a>
                      <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </div> 
              </div>
            </form>
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>
          </div>
        </div>
      </section>      
              
    @else

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
            <div class="box-header pull-right">
              <a href="{{url('admin/skillbox-job/add/0')}}">
                 <button type="submit" class="btn btn-info">Add New Job</button>
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Country</th>
                  <th>City</th>
                  <th>Status</th>
                  <th>Added Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      <td>{{$value->v_title or ''}}</td>
                      <td>{{$value->v_country or ''}}</td>
                      <td>{{$value->v_city or ''}}</td>
                      <td align="center">
                          @if(isset($value->e_status) && $value->e_status=="active")
                              <span class="badge bg-green">Active</span>         
                          @else
                              <span class="badge bg-yellow">Inactive</span>
                          @endif
                      </td>

                      <td align="center">{{ isset($value->d_added) ? date('M d Y' ,strtotime($value->d_added)) : '' }}</td>
                      <td>
                        <a href="{{url('admin/skillbox-job/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/skillbox-job',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if( $value->e_status == 'inactive' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                        @php 
                        $a=url('admin/skillbox-job/action/delete/').'/'.$value->id; 
                        @endphp
                        <a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}',{{$a}}')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });

   // Slug Generator (START)

  $('#v_title').change(function() {

    var string = $('#v_title').val();
    var $slug = '';
    var trimmed = $.trim(string);

    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-')
            .replace(/-+/g, '-')
            .replace(/^-|-$/g, '');

    $('#v_slug').val($slug.toLowerCase());

    return true;
  });

  //  Slug Generator (ENDS)
</script>

@stop

