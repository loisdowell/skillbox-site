@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Notify Users
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Notify Users</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif
      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} Notify Users</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            
            <div class="box-body">
              <br/><br/>
                <div class="row">
                   <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/notify-user/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Email<span class="asterisk_input">*</span></label>
                          <input type="email" name="v_email" id="v_email" class="form-control" placeholder="Email" value="{{$data->v_email or old('v_email')}}" required="" data-parsley-trigger="keyup">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Category<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="e_status" name="i_category_id" onchange="getSkill(this.value)" required  data-parsley-trigger="keyup">
                            <option value="">-Select-</option>
                                @if(isset($categories) && count($categories))
                                    @foreach($categories as $k=>$v)
                                        <option value="{{$v->id}}" @if(isset($data->i_category_id) && $data->i_category_id==$v->id) selected @endif >{{$v->v_name or ''}}</option>
                                    @endforeach
                               @endif  
                          </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Skill<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="i_mainskill_id" name="i_skill_id" required>
                              <option value="">-Select-</option>
                              @if($view!="add")
                              @if(isset($skilldata) && count($skilldata))
                                  @foreach($skilldata as $k=>$v)
                                    <option value = '{{$v->id}}' @if(isset($data->i_skill_id) && $data->i_skill_id==$v->id) selected @endif>{{$v->v_name}}</option>
                                  @endforeach
                              @endif
                              @endif

                          </select>
                      </div>
                    </div>
                    
                    <div class="col-md-12">
                    <div class="form-group">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="e_notify" @if(isset($data->e_notify) && $data->e_notify=="on") checked @endif required="">
                          Yes, I want to receive notification's
                        </label>
                      </div>
                      </div>
                    </div>  


                  </div>
                  <div class="clear:both"></div>                
                        
                        <div class="row">
                          <div class="col-md-12">
                              <a href="{{url('admin/notify-user')}}">
                              <button type="button" class="btn btn-warning">Back to List</button>
                              </a>
                              <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                      </div> 
                    </div>
                    </form>
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>
          </div>
        </div>
      </section>      
              
    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <div class="box-header pull-right">
              <a href="{{url('admin/notify-user/add/0')}}">
                 <button type="submit" class="btn btn-info">Add New Notify Users</button>
              </a>
            </div>
              
            <div style="clear: both;"></div>
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Email</th>
                  <th>Category</th>
                  <th>Skill</th>
                  <th>Notify</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      <td><a href="mailto:{{$value->v_email or ''}}">{{$value->v_email or ''}}</a></td>
                      <td>
                        @if(count($value->hasCategory()) && isset($value->hasCategory()->v_name))
                          {{$value->hasCategory()->v_name}}
                        @else
                          -
                        @endif  
                      </td>
                      <td>
                        @if(count($value->hasSkill()) && isset($value->hasSkill()->v_name))
                          {{$value->hasSkill()->v_name}}
                        @else
                          -
                        @endif  
                      </td>
                      <td>{{$value->i_count or '0'}}</td>
                      <td>
                        <a href="{{url('admin/notify-user/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        @php $a=url('admin/notify-user/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','{{$a}}')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });

  function getSkill(i_category_id){
            
    var formdata = "i_category_id="+i_category_id;
    var actionurl = "{{url('admin/job/getskill')}}";

    $.ajax({
          type    : "GET",
          url     : actionurl,
          data    : formdata,
          success : function( res ){
              var obj = jQuery.parseJSON(res);
              if(obj['status']==1){
                $("#i_mainskill_id").html(obj['optionstr']);
                $("#i_otherskill_id").html(obj['optionstr']);
              }
          },
          error: function ( jqXHR, exception ) {
              $("#someerror").show();
          }
      });    
} 



</script>

@stop

