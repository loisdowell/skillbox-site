@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        {{$section}} Manage
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{$section}}</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div> 

	<section class="content">

		<div class="box box-default">
			
			<div class="box-header pull-right">
             <a href="{{url('admin/order/user-plan')}}">
				<button type="button" class="btn btn-warning">Back to List</button>
			</a>
            </div>
              
            <div style="clear: both;"></div>

			<div class="box-body">


				<table id="categoryListing" class="table table-bordered table-striped">
					<thead>
						<tr>
							<tr>
								<th>Order number</th>
								<th>Username</th>
								<th>Payment type</th>
								<th>Subscription plan</th>
								<th>Subscription type</th>
								<th>Amount</th>
								<th>Payment Status</th>
								<th>Order Date</th>
							</tr>
						</tr>
					</thead>
					<tbody>
						@if(count($alldata))
						@foreach($alldata as $key=>$val)

						<tr>  

							<td>
								{{$val->i_order_no or ''}} 
							</td>
							<td>
								@if(count($val->hasUser()) && isset($val->hasUser()->v_fname) && isset($val->hasUser()->v_lname))
								{{$val->hasUser()->v_fname.' '.$val->hasUser()->v_lname}}
								@endif
							</td>
							<td>{{$val->e_payment_type or ''}}</td>
							<td>{{ucfirst($val->v_order_detail['v_plan_name'])}}</td>
							<td>{{ucfirst($val->v_order_detail['v_plan_duration'])}}</td>
							<td>£{{number_format($val->v_amount)}}</td>
							<td>{{$val->e_payment_status or ''}}</td>
							<td>{{date("Y-m-d",strtotime($val->d_added))}}</td>

						</tr>  
						@endforeach
						@else
						<tr>  
							<td colspan="7">There are not found any user subscription.</td>
						</tr>  
						@endif
					</tbody>
				</table>




				{{-- <form class="horizontal-form" role="form" method="POST" name="orderform" id="orderform" action="{{url('admin/order/user-plan/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multiPage/form-data" >
					{{ csrf_field() }}
					<h4>{{ucfirst($view)}} {{$section}}</h4>
					<div style="clear: both"></div>   
					<div class="clear:both"></div>                
					<div class="row">
						<div class="col-md-6">
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title">Order Information</h3>
								</div>
								<div class="box-body">
									<div class="table-responsive">
										<table class="table no-margin">
											<tbody>
												<tr>
													<td width="30%"><b>Order Id </b> :</td>
													<td width="70%">{{$data->i_order_no or ''}}</td>
												</tr>

												<tr>
													<td><b>User name</b> :</td>
													<td>@if(count($data->hasUser()) && isset($data->hasUser()->v_fname) && isset($data->hasUser()->v_lname))
															{{ucfirst($data->hasUser()->v_fname).' '.ucfirst($data->hasUser()->v_lname)}}
															@endif</td>
												</tr>


												<tr>
													<td><b>Order Title</b> :</td>
													<td>Subscription for {{$data->v_order_detail['v_plan_name'] or ''}}</td>
												</tr>

												<tr>
													<td><b>Plan Duration</b> :</td>
													<td>{{isset($data->v_order_detail['v_plan_duration']) ? ucfirst($data->v_order_detail['v_plan_duration']) : ''}}</td>
												</tr>
												<tr>
													<td><b>Total Amount</b> :</td>
													<td>£{{$data->v_amount or ''}}</td>
												</tr>
												
												<tr>
													<td><b>Payment Type</b> :</td>
													<td>{{$data->e_payment_type or ''}}</td>
												</tr>
												
												<tr>
													<td><b>Payment Status</b> :</td>
													<td>{{$data->e_payment_status or ''}}</td>
												</tr>

												<tr>
													<td><b>Transaction Id</b> :</td>
													<td>{{$data->v_transcation_id or ''}}</td>
												</tr>
												<tr>
													<td><b>Created Date</b> :</td>
													<td>{{date("Y-m-d",strtotime($data->d_added))}}</td>
												</tr>
											</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									
									<div class="box box-info">
										<div class="box-header with-border">
											<h3 class="box-title">Order Status</h3>
										</div>
										<div class="box-body">
											<div class="row">
												<div class="col-md-12">
													
													<div class="form-group">
														<label>User name</label> : @if(count($data->hasUser()) && isset($data->hasUser()->v_fname) && isset($data->hasUser()->v_lname))
															{{ucfirst($data->hasUser()->v_fname).' '.ucfirst($data->hasUser()->v_lname)}}
															@endif
													</div>
														
													<div class="form-group">
														<label>Payment Status</label>
														<input type="hidden" name="i_user_id" value="{{$data->i_user_id or ''}}">	
														<select id="e_payment_status" class="form-control" name="e_payment_status" required>
															<option value="Select">Select</option>
															<option value="pending" @if(isset($data->e_payment_status) && $data->e_payment_status == 'pending' ) selected @endif>Pending</option>
															<option value="success" @if(isset($data->e_payment_status) && $data->e_payment_status == 'success' ) selected @endif>success</option>
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
									<a href="{{url('admin/order/user-plan')}}">
										<button type="button" class="btn btn-warning">Back to List</button>
									</a>
									<button type="submit" class="btn btn-primary">Save</button>
								
								</div>
							</div>
						</form> --}}
							
						</section>      



						@else



						<section class="content">
							<div class="row">
								<div class="col-xs-12">
									<div style="clear: both"></div>
									@if ($success = Session::get('success'))
									<div class="alert alert-success alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
									</div>
									@endif
									@if ($warning = Session::get('warning'))
									<div class="alert alert-danger alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
									</div>
									@endif

									<div class="box">

										<div style="clear: both;"></div>

										<div class="box-body">
											<table id="categoryListing" class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>Username</th>
														<th>Payment type</th>
														<th>Subscription plan</th>
														<th>Subscription type</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													@if(count($data))
													@foreach($data as $key=>$val)

													<tr>  
														<td>
															@if(count($val->hasUser()) && isset($val->hasUser()->v_fname) && isset($val->hasUser()->v_lname))
															{{$val->hasUser()->v_fname.' '.$val->hasUser()->v_lname}}
															@endif
														</td>
														<td>{{ucfirst($val->e_payment_type)}}</td>	
														<td>{{ucfirst($val->v_order_detail['v_plan_name'])}}</td>
														<td>{{ucfirst($val->v_order_detail['v_plan_duration'])}}</td>
														<td><a href="{{url('admin/order/user-plan/edit/')}}/{{$val->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>

															@php $a=url('admin/order/user-plan/action/delete/').'/'.$val->id; @endphp
															<a href="javascript:;" onclick="confirmDelete('{{ $val->id }}','{{$a}}')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
														</td>
													</tr>  
													@endforeach
													@else
													<tr>  
														<td colspan="5">There are not found any user subscription.</td>
													</tr>  
													@endif
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</section>

						@endif

					</div>

					@stop

					@section('js')

					<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
					<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
					<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
					<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


					<script>
						$(function () {
							$('#categoryListing').DataTable({
								"paging": true,
							});
						});
					</script>

					@stop

