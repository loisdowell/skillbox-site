@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        {{$section}} Manage
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{$section}}</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div> 

	<section class="content">

		<div class="box box-default">

			<div class="box-body">

				<form class="horizontal-form" role="form" method="POST" name="orderform" id="orderform" action="{{url('admin/order/courses/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multiPage/form-data" >

					{{ csrf_field() }}
					<h4>{{ucfirst($view)}} {{$section}}</h4>
					<div style="clear: both"></div>   
					<div class="clear:both"></div>                
					<div class="row">
						<div class="col-md-6">
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title">Order Information</h3>
								</div>
								<div class="box-body">
									<div class="table-responsive">
										<table class="table no-margin">
											<tbody>
												
												<tr>
													<td><b>Order Id</b></td>
													<td>{{isset($data->i_order_no) ? $data->i_order_no : ''}}</td>
												</tr>

												<tr>
													<td><b>Course Price</b></td>
													<td>£{{isset($data->v_amount) ? $data->v_amount : ''}}</td>
												</tr>

												<tr>
													<td><b>Commission from buyer</b></td>
													<td>£{{isset($data->v_buyer_commission) ? $data->v_buyer_commission : ''}}</td>
												</tr>

												<tr>
													<td><b>Processing fee from buyer</b></td>
													<td>£{{isset($data->v_buyer_processing_fees) ? $data->v_buyer_processing_fees : ''}}</td>
												</tr>

												@php
													$totalamt = $data->v_amount;
													if(isset($data->v_buyer_processing_fees) && $data->v_buyer_processing_fees!=""){
														$totalamt = $totalamt+$data->v_buyer_processing_fees;		
													}
													if(isset($data->v_buyer_commission) && $data->v_buyer_commission!=""){
														$totalamt = $totalamt+$data->v_buyer_commission;		
													}
													$totalamt = number_format($totalamt,2);
												@endphp
												
												<tr>
													<td><b>Total Amount</b></td>
													<td>£{{$totalamt}}</td>
												</tr>

												@if(isset($data->v_seller_commission) && $data->v_seller_commission!="")
												<tr>
													<td><b>Commission from seller</b></td>
													<td>£{{isset($data->v_seller_commission) ? $data->v_seller_commission : ''}}</td>
												</tr>
												@endif

												@if(isset($data->v_seller_processing_fees) && $data->v_seller_processing_fees!="")
												<tr>
													<td><b>Processing fee from seller</b></td>
													<td>£{{isset($data->v_seller_processing_fees) ? $data->v_seller_processing_fees : ''}}</td>
												</tr>
												@endif

												<tr>
													<td><b>Payment Type</b></td>
													<td>{{$data->e_payment_type or ''}}</td>
												</tr>
												
												<tr>
													<td><b>Payment Status</b></td>
													<td>{{$data->e_payment_status or ''}}</td>
												</tr>
													
												<tr>
													<td><b>Transaction Id</b></td>
													<td>{{$data->v_transcation_id or ''}}</td>
												</tr>
												<tr>
													<td><b>Created Date</b></td>
													<td>{{date("Y-m-d",strtotime($data->d_added))}}</td>
												</tr>
												</tbody>
											</table>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="box box-info">
										<div class="box-header with-border">
											<h3 class="box-title">Order Status</h3>
										</div>
										<div class="box-body">
											<div class="row">
												<div class="col-md-12">
													
													<div class="form-group">
														<label>Order status</label>
														<select id="v_order_status" class="form-control" name="v_order_status" required>
															<option value="">Select</option>
															<option value="active" @if(isset($data->v_order_status) && $data->v_order_status == 'active' ) selected @endif>Active</option>
															<option value="completed" @if(isset($data->v_order_status) && $data->v_order_status == 'completed' ) selected @endif>Completed</option>
														</select>
													</div>

													<div class="form-group">
														<label>Order Payment Status</label>
														<select id="e_payment_status" class="form-control" name="e_payment_status" >
															<option value="">Select</option>
															
															<option value="pending" @if(isset($data->e_payment_status) && $data->e_payment_status == 'pending' ) selected @endif>Pending</option>
															<option value="success" @if(isset($data->e_payment_status) && $data->e_payment_status == 'success' ) selected @endif>success</option>
														</select>
													</div>

													<div class="form-group">
														<label>Seller Payment Status</label>
														<select id="e_seller_payment_status" class="form-control" name="e_seller_payment_status">
															<option value="">Select</option>
															<option value="pending" @if(isset($data->e_seller_payment_status) && $data->e_seller_payment_status == 'pending' ) selected @endif>Pending</option>
															<option value="completed" @if(isset($data->e_seller_payment_status) && $data->e_seller_payment_status == 'success' ) selected @endif>Success</option>
														</select>
													</div>

													<div class="form-group">
													<a href="{{url('admin/order/courses')}}">
														<button type="button" class="btn btn-warning">Back to List</button>
													</a>
													<button type="submit" class="btn btn-primary">Save</button>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="order-details" class="col-md-12">
								<div class="box box-info">
									<div class="box-header with-border">
										<h3 class="box-title">Order Details</h3>
									</div>
									<div class="box-body">
										<div class="table-responsive">
											<table class="table no-margin border">
												<thead>
													<tr>
														<th >image</th>
														<th>Name</th>
														<th>Qty</th>
														<th>Price</th>
													</tr>	
													</thead>
													<tbody>
														@php
															$totalamt = 0;	
														@endphp
														@if(isset($data->v_order_detail) && count($data->v_order_detail))
														@foreach($data->v_order_detail as $key => $value)
															<tr>
																<td>
																	@php
																		$totalamt = $totalamt+$value['price'];
																		$imgdata="";
																		if(Storage::disk('s3')->exists($coursesdata->v_instructor_image)){
																			$imgdata = \Storage::cloud()->url($coursesdata->v_instructor_image);	
																		}else{
																			$imgdata = \Storage::cloud()->url("common/no-image-data.png");
																		}
																    @endphp
																    <img src="{{$imgdata}}" style="height: 50px;width: 50px">
																</td>
																<td>{{$value['name']}}</td>
																<td>1</td>
																<td>£{{$value['price']}}</td>
															</tr>
														@endforeach
														@endif
														
														<tr>
															<td colspan="3" align="right"><b>SubTotal</b></td>
															<td>£{{$totalamt}}</td>
														</tr>	



														<tr>
															<td colspan="3" align="right"><b>Commission</b></td>
															<td>£{{isset($data->v_buyer_commission) ? $data->v_buyer_commission : ''}}</td>
														</tr>	

														<tr>
															<td colspan="3" align="right"><b>Processing fee</b></td>
															<td>£{{isset($data->v_buyer_processing_fees) ? $data->v_buyer_processing_fees : ''}}</td>
														</tr>	
														@php
															$totalamt = $totalamt+$data->v_buyer_processing_fees+$data->v_buyer_commission;
															$totalamt = number_format($totalamt,2);	
														@endphp
														<tr>
															<td colspan="3" align="right"><b>Total</b></td>
															<td>£{{$totalamt}}</td>
														</tr>

														@php
															$processingamt = number_format($totalamt*5/100,2);	
															$totalamt = $totalamt+$processingamt;

														@endphp

															
													</tbody>
												</table>

												
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							

						</section>      

						@else

						<section class="content">
							<div class="row">
								<div class="col-xs-12">
									<div style="clear: both"></div>
									@if ($success = Session::get('success'))
									<div class="alert alert-success alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
									</div>
									@endif
									@if ($warning = Session::get('warning'))
									<div class="alert alert-danger alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
									</div>
									@endif

									<div class="box">
										<div style="clear: both;"></div>
										<div class="box-body">
											<table id="categoryListing" class="table table-bordered table-striped">
													
												<thead>
													<tr>
														<th>No.</th>
														<th>Buyer</th>
														<th>Seller</th>
														<th>Payment type</th>
														<th>Amount</th>
														<th>Payment Status</th>
														<th>Order Date</th>
														<th>Action</th>
													</tr>
												</thead>

												<tbody>
													@if(count($data))
													@foreach($data as $key=>$val)

													<tr>  
														<td>
															{{isset($val->i_order_no) ? $val->i_order_no : ''}} 
														</td>

														<td>
															@if(count($val->hasUser()) && isset($val->hasUser()->v_fname) && isset($val->hasUser()->v_lname))
															{{$val->hasUser()->v_fname.' '.$val->hasUser()->v_lname}}
															@endif
														</td>
														<td>
															@if(count($val->hasUserSeller()) && isset($val->hasUserSeller()->v_fname) && isset($val->hasUserSeller()->v_lname))
															{{$val->hasUserSeller()->v_fname.' '.$val->hasUserSeller()->v_lname}}
															@endif
														</td>	
														<td>{{$val->e_payment_type or ''}}</td>
														<td>£{{number_format($val->v_amount)}}</td>

														<td>{{$val->e_payment_status or ''}}</td>
														<td>{{date("Y-m-d",strtotime($val->d_added))}}</td>
														<td><a href="{{url('admin/order/courses/edit/')}}/{{$val->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
															@php $a=url('admin/order/courses/action/delete/').'/'.$val->id; @endphp
															<a href="javascript:;" onclick="confirmDelete('{{ $val->id }}','{{$a}}')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
														</td>
													</tr>  
													@endforeach
													@else
													<tr>  
														<td colspan="7">There is not found any jobpost.</td>
													</tr>  
													@endif
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</section>

						@endif

					</div>

					@stop

					@section('js')

					<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
					<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
					<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
					<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


					<script>
						$(function () {
							$('#categoryListing').DataTable({
								"paging": true,
							});
						});
					</script>

					@stop

