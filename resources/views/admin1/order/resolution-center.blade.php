@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        {{$section}} Manage
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{$section}}</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div> 

	<section class="content">

		<div class="box box-default">

			<div class="box-body">

				<form class="horizontal-form" role="form" method="POST" name="orderform" id="orderform" action="{{url('admin/order/resolution-center/action/')}}/{{$view}}/{{isset($data->i_order_id) ? $data->i_order_id: '0'}}" data-parsley-validate enctype="multiPage/form-data" >
					{{ csrf_field() }}
					<h4>{{ucfirst($view)}} {{$section}}</h4>
					<div style="clear: both"></div>   
					<div class="clear:both"></div>                
					<div class="row">
						<div class="col-md-6">
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title">Resolution Information</h3>
								</div>
								<div class="box-body">
									<div class="table-responsive">
										<table class="table no-margin">
											<tbody>
												<tr>
													<td>Order Id</td>
													<td>{{$data->hasOrder()->i_order_no or ''}} </td>
												</tr>
												<tr>
													<td>Buyer</td>
													<td>@if(count($data->hasUser()) && isset($data->hasUser()->v_fname) && isset($data->hasUser()->v_lname))
															{{$data->hasUser()->v_fname.' '.$data->hasUser()->v_lname}}
															@endif</td>
													</tr>
													<tr>
														<td>Seller</td>
														<td>@if(count($data->hasSeller()) && isset($data->hasSeller()->v_fname) && isset($data->hasSeller()->v_lname))
															{{$data->hasSeller()->v_fname.' '.$data->hasSeller()->v_lname}}
															@endif</td>
													</tr>

													<tr>
														<td>Issue</td>
														<td>{{isset($issue[$data->i_issue_id]) ? $issue[$data->i_issue_id] : ''}}</td>
													</tr>


													<tr>
														<td>Order Date</td>
														<td>{{isset($data->d_added) ? date("F d, Y. h:i:s A",strtotime($data->d_added)) : ''}}</td>
													</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="box box-info">
										<div class="box-header with-border">
											<h3 class="box-title">Order Status</h3>
										</div>
										<div class="box-body">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label>Order Status</label>
														<select id="v_order_status" class="form-control" name="v_order_status" required>
															<option value="Select">Select</option>
															<option value="cancelled" @if(isset($data->hasOrder()->v_order_status) && $data->hasOrder()->v_order_status == 'cancelled' ) selected @endif>Cancelled</option>
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
									<a href="{{url('admin/order/resolution-center')}}">
										<button type="button" class="btn btn-warning">Back to List</button>
									</a>
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
							</div>

							<div id="order-details" class="col-md-12">
								<div class="box box-info">
									<div class="box-header with-border">
										<h3 class="box-title">Resolution Details</h3>
									</div>
									<div class="box-body">
										<div class="table-responsive">
											<table class="table no-margin">
												<thead>
													<tr>
														<th>Message By</th>
														<th>Message</th>
													</thead>
													<tbody>
														<tr>
															<td>@if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_fname) && isset($orderdata->hasUser()->v_lname))
															{{$orderdata->hasUser()->v_fname.' '.$orderdata->hasUser()->v_lname}}
															@endif</td>
															<td>
																<b>Subject </b> : {{$data->v_subject or ''}}<br>	
																<b>Message </b> : {{$data->l_message or ''}}<br>
																{{isset($data->d_added) ? date("F d, Y. h:i:s A",strtotime($data->d_added)) : ''}}	
																
															</td>
														</tr>
														@if(isset($data->l_reply) && $data->l_reply!="")
														<tr>
															<td>@if(count($orderdata->hasUserSeller()) && isset($orderdata->hasUserSeller()->v_fname) && isset($orderdata->hasUserSeller()->v_lname))
															{{$orderdata->hasUserSeller()->v_fname.' '.$orderdata->hasUserSeller()->v_lname}}
															@endif
															</td>
															<td>
																<b>Reply </b> : {{$data->l_reply or ''}}<br>	
																{{isset($data->d_reply_date) ? date("F d, Y. h:i:s A",strtotime($data->d_reply_date)) : ''}}
															</td>
														</tr>
														@endif

															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</section>      
						@else

						<section class="content">
							<div class="row">
								<div class="col-xs-12">
									<div style="clear: both"></div>
									@if ($success = Session::get('success'))
									<div class="alert alert-success alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
									</div>
									@endif
									@if ($warning = Session::get('warning'))
									<div class="alert alert-danger alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
									</div>
									@endif

									<div class="box">

										<div style="clear: both;"></div>

										<div class="box-body">
											<table id="categoryListing" class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>Order number</th>
														<th>Buyer</th>
														<th>Seller</th>
														<th>Subject</th>
														<th>Order Status</th>
														<th>Cancelled By</th>
														<th>Order Date</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													@if(count($data))
													@foreach($data as $key=>$val)

													<tr>  
														<td>
															{{$val->hasOrder()->i_order_no or ''}} 
														</td>
														<td>
															@if(count($val->hasUser()) && isset($val->hasUser()->v_fname) && isset($val->hasUser()->v_lname))
															{{$val->hasUser()->v_fname.' '.$val->hasUser()->v_lname}}
															@endif
														</td>
														<td>
															@if(count($val->hasSeller()) && isset($val->hasSeller()->v_fname) && isset($val->hasSeller()->v_lname))
															{{$val->hasSeller()->v_fname.' '.$val->hasSeller()->v_lname}}
															@endif
														</td>
														<td>{{$val->v_subject or ''}}</td>
														<td>@if(isset($val->hasOrder()->v_order_status) && $val->hasOrder()->v_order_status == 'cancelled')
						                                        Order Cancelled
						                                    @else
						                                        Pending
						                                   @endif</td>
														<td>{{isset($val->e_cancelled_by) ? ucfirst($val->e_cancelled_by) : '-'}}</td>
														<td>{{isset($val->d_added) ? date("F d, Y. h:i:s A",strtotime($val->d_added)) : ''}}</td>

														<td><a href="{{url('admin/order/resolution-center/edit/')}}/{{$val->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>

															@php $a=url('admin/order/resolution-center/action/delete/').'/'.$val->id; @endphp
															<a href="javascript:;" onclick="confirmDelete('{{ $val->id }}','{{$a}}')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
														</td>
													</tr>  
													@endforeach
													@else
													<tr>  
														<td colspan="7">There is not found any jobpost.</td>
													</tr>  
													@endif
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</section>

						@endif

					</div>

					@stop

					@section('js')

					<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
					<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
					<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
					<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
					<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


					<script>
						$(function () {
							$('#categoryListing').DataTable({
								"paging": true,
							});
						});
					</script>

					@stop

