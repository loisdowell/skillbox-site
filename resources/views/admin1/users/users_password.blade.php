@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/public/Assets/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}

.active-manu {
   background:  #2c4762 !important;
   color: #fff;
}
.active-manu a{
  color: #fff;  
}
.list-group-item{
  padding-left: 10px !important;
}


</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Users
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/users')}}">Users</a></li>
        <li class="active">users</li>
      </ol>
    </section>

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif
      </div>  
      <section class="content">
          <div class="row">
              <div class="col-md-3">
                  <div class="box box-primary">
                      <div class="box-body box-profile">

                          @if($view=="add")
                          <img class="profile-user-img img-responsive img-circle" src="{{url('public/theme/images/no-image.png')}}" alt="profile picture" style="height: 250px;width: 250px">
                          @else
                              @php
                                  $imgdata="";  
                                  if(isset($data->v_image) && !is_array($data->v_image) && $data->v_image!=""){
                                      $imgdata = \Storage::cloud()->url($data->v_image);     
                                  }else{
                                      $imgdata = \Storage::cloud()->url('users/img-upload.png');   
                                  }
                              @endphp 
                              <img class="profile-user-img img-responsive img-circle" src="{{$imgdata}}" alt="profile picture" style="height: 250px;width: 250px">    
                              
                          @endif 
                          <h3 class="profile-username text-center">{{isset($data->v_fname) ? $data->v_fname:'User Name' }} {{isset($data->v_lname) ? $data->v_lname:'' }}</h3>

                          <p class="text-muted text-center">{{isset($data->v_email) ? $data->v_email:'User Email' }}</p>
                          <p class="text-muted text-center"><b>Email verified :</b> {{isset($data->e_email_confirm) ? $data->e_email_confirm:'no' }}</p>
                          
                          <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                  <b>Buyer Account Status</b>
                                  <ul class="list-group list-group-unbordered">
                                    <b>In Person :-</b> {{isset($data->v_buyer_inperson_service) ? $data->v_buyer_inperson_service : ''}}</br>
                                    <b>Online :-</b> {{isset($data->v_buyer_online_service) ? $data->v_buyer_online_service : ''}}
                                  </ul>
                              </li>
                              <li class="list-group-item">
                                  <b>Seller Account Status</b>
                                  <ul class="list-group list-group-unbordered">
                                    <b>In Person :-</b> {{isset($data->v_seller_inperson_service) ? $data->v_seller_inperson_service : ''}}</br>
                                    <b>Online :-</b> {{isset($data->v_seller_online_service) ? $data->v_seller_online_service : ''}}
                                  </ul>
                              </li>
                          </ul>

                      </div>
                  </div>
                  <div class="box box-primary">
                      
                      <ul class="list-group list-group-unbordered">
                          
                          <li class="list-group-item ">
                              <a href="{{url('admin/users/edit')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Edit Main profile</b>
                              </a>    
                          </li>

                          @if ($view!="add")
                          <li class="list-group-item">
                              <a href="{{url('admin/users/edit/sellerinperson')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Seller In Person Profile</b>
                              </a>
                          </li>

                          <li class="list-group-item">
                              <a href="{{url('admin/users/edit/selleronline')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Seller Online Profile</b>
                              </a>
                          </li>

                           <li class="list-group-item active-manu" >
                              <a href="{{url('admin/users/edit/password')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Change Password</b>
                              </a>
                          </li>

                          <li class="list-group-item" >
                              <a href="{{url('admin/users/buyer-orders')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Buyer Orders</b>
                              </a>
                          </li>
                          <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-orders')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Seller Orders</b>
                              </a>
                          </li>
                           <li class="list-group-item" >
                              <a href="{{url('admin/users/buyer-reviews')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Buyer Reviews</b>
                              </a>
                          </li>

                          <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-reviews')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Seller Skill Reviews</b>
                              </a>
                          </li>

                          <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-reviews/course')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Seller Course Reviews</b>
                              </a>
                          </li>


                       
                          
                          {{-- <li class="list-group-item">
                              <a href="{{url('admin/users/edit/billing')}}/{{isset($data->id) ? $data->id:'0'}}">
                              <b>Edit Billing Detail</b>
                              </a>
                          </li> --}}

                          
                          @endif




                         


                      </ul>
                  </div>
              </div>
              <!-- /.col -->
              <div class="col-md-9">
                  <div class="nav-tabs-custom">
                      
                      <a class="btn btn-block btn-social btn-tumblr">
                        <i class="fa fa-info"></i>Update password
                      </a>


                      <div class="box-body">
                        <div class="row">
                            
                            <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/users/update-user-password')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            
                              <div class="col-md-6">
                                
                                <div class="form-group">
                                  <label>Password<span class="asterisk_input">*</span></label>
                                  <input type="password" name="password" id="password" class="form-control" required="" data-parsley-trigger="keyup">
                                </div>

                              </div>  

                              <div class="col-md-6">
                                
                                <div class="form-group">
                                  <label>Confirm password<span class="asterisk_input">*</span></label>
                                  <input type="password" name="cnpassword" id="cnpassword" class="form-control"  required="" data-parsley-trigger="keyup">
                                </div>

                              </div>

                              <div class="col-md-12">
                                  <a href="{{url('admin/users')}}">
                                  <button type="button" class="btn btn-warning">Back to List</button>
                                  </a>
                                  <button type="submit" class="btn btn-primary">Save</button>
                              </div>
                              
                            </form>
                        </div>
                  </div>
              </div>

          </div>
      </section>

  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{url('/public/Assets/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
  

  function selectSubCat(id,selectedid=""){

      if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/subCategories/')}}",
                data:'category_id='+id+"&sub_category_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    $("#i_sub_category_id").html(obj['optingStr'])
                }
            }); 
        }
    
  }

  // $('#i_parent_id').on('change',function(){
  //       var categoryID = $(this).val();
  //       if(categoryID){
  //           $.ajax({
  //               type:'get',
  //               url:"{{url('admin/users/subCategories/')}}",
  //               data:'category_id='+categoryID,
  //               success:function(data){
  //                   var obj1 = JSON.parse(data);
  //                   //alert(obj['status']);
  //                   $("#i_sub_category_id").html(obj1['optingStr'])
  //               }
  //           }); 
  //       }
  // });

  function selectSkill(id,selectedid=""){
        
        if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/skills/')}}",
                data:'category_id='+id+"&selected_skill_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    //alert(obj['status']);
                    $("#i_main_skill_id").html(obj['optingStr'])
                }
            }); 
        }
  }

  function selectState(id,selectedid=""){
        
        if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/state/')}}",
                data:'country_id='+id+"&selected_state_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    $("#i_state_id").html(obj['optingStr'])
                }
            }); 
        }
  } 

  function selectCity(id,selectedid=""){
        if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/city/')}}",
                data:'state_id='+id+"&selected_city_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    $("#i_city_id").html(obj['optingStr'])
                }
            }); 
        }
  }    

  $(document).ready(function(){
         $("#i_main_skill_id").select2();
        <?php 
            if($view == 'edit' && isset($data->l_other_skills ) && ($data->l_other_skills != '')){
        ?>
        $('#othes_skill').show();
        <?php }else {?>
         $('#othes_skill').hide();
        <?php 
            }
            if($view == 'edit' && isset($data->e_type ) && ($data->e_type == 'seller' || $data->e_type == 'both' )){
        ?>
          $('#seller_info').show();
          $('#seller_education').show();
          $('#seller_employment').show();
          $('#seller_packages').show();
          $('#company_detail').show();
          

        <?php }else{ ?>
          $('#seller_info').hide();
          $('#seller_education').hide();
          $('#seller_employment').hide();
          $('#seller_packages').hide();
          $('#company_detail').hide();
        
        <?php } ?>
        $('#e_type').on('change', function() {
          var valType = this.value;

          if(valType == 'seller' || valType == 'both' ){
             $('#seller_info').show();
             $('#seller_education').show();
             $('#company_detail').show();
             $('#seller_employment').show();
             $('#seller_packages').show();  
          }else{
            $('#seller_info').hide();
            $('#seller_education').hide();
            $('#seller_employment').hide();
            $('#company_detail').hide();
            $('#seller_packages').hide();
          }
         
      })

      $('#d_date').datepicker({
        
      });
  // $('#d_start_date').datepicker();
  // $('#d_start_date').on('change', function() {
  //   var start_date = $('#d_start_date').val();
  //   $('#d_end_date').datepicker({
  //     minDate: new Date(start_date)
  //   }); 
  // })

     $(function() {
      $("#d_start_date").datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 3,
          onClose: function( selectedDate ) {
              $("#d_end_date").datepicker( "option", "minDate", selectedDate );
          }
      });

      $("#d_end_date").datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 3,
          onClose: function( selectedDate ) {
              $("#d_start_date").datepicker("option", "maxDate", selectedDate);
          }
      });
    });
   
   
});

 // $('#i_main_skill_id').on('change click', function(){
 //      if($('#i_main_skill_id').val() == 'Other'){
 //        $('#othes_skill').show();
 //      }else{
 //        $('#othes_skill').hide();
 //      }
 //  });

 $('#i_main_skill_id').on('change', function() {
       var data = $(this).val();
    //  var data = $(".select2-hidden-accessible option:selected").text();
       if(jQuery.inArray( "Other", data ) != -1){
        $('#othes_skill').show();
      }else{
        $('#othes_skill').hide();
      }
    })
</script>

@stop

