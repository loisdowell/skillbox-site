@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Buyer Orders
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{url('admin/users')}}">Users</a></li>
        <li class="active"><a href="{{url('admin/users/edit', $buyer_user->_id)}}">{{ucfirst($buyer_user->v_fname)}} {{ucfirst($buyer_user->v_lname)}}</a></li>
      </ol>
    </section>

    <!-- Main content -->
 
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				
				<div class="col-xs-12">
	            <div class="box">
	            	@php
	            		$total['active'] = 0;
	            		$total['completed'] = 0;
	            		$total['cancelled'] = 0;
	            		$total['delivered'] = 0;
	            		$total['v_amount'] = 0;
	            	@endphp
	            	@if(count($buyer_orders))
						@foreach($buyer_orders as $key=>$val)
							@php
								if($val->v_order_status=="active"){
									$total['active'] = $total['active']+1;
								}
								else if($val->v_order_status=="completed"){
									$total['completed'] = $total['completed']+1;
								}
								else if($val->v_order_status=="cancelled"){
									$total['cancelled'] = $total['cancelled']+1;
								}
								else if($val->v_order_status=="delivered"){

									if(isset($val->v_buyer_deliver_status) && $val->v_buyer_deliver_status=="accept"){
										$total['completed'] = $total['completed']+1;
									}else{
										$total['delivered'] = $total['delivered']+1;	
									}
									
								}

								$total['v_amount'] += $val->v_amount;
							@endphp
						@endforeach
						@php
							$total_cancelled = $total['cancelled'];
							$total_completed = $total['completed'];
							$total_orders = $total_cancelled + $total_completed;
						@endphp
					@endif
	              <div class="box-body" >
	                <table  class="table table-striped table-bordered table-hover order-column dataTable ">
	                  <thead>
	                    <tr>
	                        <th>Total orders</th>
	                        <th>Cancel orders</th>
	                        <th>Completed orders</th>
	                        <th>Total amount</th>
	                    </tr>
	                  </thead>
	                    <tbody>
	                        <tr>
	                            <td> {{$total_orders or '0'}} </td>
	                            <td> {{$total_cancelled or '0'}} </td>
	                            <td>{{$total_completed or '0'}}</td>
	                            <td class="seller-sale"> £{{number_format($total['v_amount'])}}</td>
	                        </tr>
	                    </tbody>
	                  </table>
	                </div>
	              </div>
	            </div>

				<div style="clear: both"></div>
				@if ($success = Session::get('success'))
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
				</div>
				@endif
				@if ($warning = Session::get('warning'))
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
				</div>
				@endif

				<div class="box">
					<div style="clear: both;"></div>
					<div class="box-body">
						<table id="categoryListing" class="table table-bordered table-striped">
								
							<thead>
								<tr>
									<th>No.</th>
									<th>Seller</th>
									<th>Transaction Type</th>
									<th>Payment type</th>
									<th>Amount</th>
									<th>Payment Status</th>
									<th>Order Status</th>
									<th>Order Date</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody>
								@if(count($buyer_orders))
								@foreach($buyer_orders as $key=>$val)

								<tr>  
									<td>
										{{isset($val->i_order_no) ? $val->i_order_no : ''}} 
									</td>

									<td>
										@if(count($val->hasUserSeller()) && isset($val->hasUserSeller()->v_fname) && isset($val->hasUserSeller()->v_lname))
										{{$val->hasUserSeller()->v_fname.' '.$val->hasUserSeller()->v_lname}}
										@endif
									</td>	
									<td>
										{{isset($val->e_transaction_type) ? ucfirst($val->e_transaction_type) : ''}}
									</td>
									<td>{{$val->e_payment_type or ''}}</td>
									<td>£{{number_format($val->v_amount)}}</td>

									<td>{{$val->e_payment_status or ''}}</td>
									<td>{{ucfirst($val->v_order_status)}}</td>
									<td>{{date("Y-m-d",strtotime($val->d_added))}}</td>
									<td>
									@if(isset($val->e_transaction_type) && $val->e_transaction_type=="skill")
									<a target="_blank" href="{{url('admin/order/skill/edit/')}}/{{$val->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i>
									</a>
									@else
									<a target="_blank" href="{{url('admin/order/courses/edit/')}}/{{$val->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i>
									</a>
									@endif
									</td>
								</tr>  
								@endforeach
								@else
								<tr>  
									<td colspan="9">There is not found any order.</td>
								</tr>  
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

	
</div>

@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script>
	$(function () {
		$('#categoryListing').DataTable({
			"paging": true,
		});
	});
</script>

@stop

