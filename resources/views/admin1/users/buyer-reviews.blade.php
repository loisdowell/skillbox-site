@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" href="{{url('public/Assets/frontend/star-rating/css/star-rating.css')}}" type="text/css" media="all">
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
.user-avatar{
  border-radius: 50%;
  background-color: #df21a1;
  color: white;
  width: 40px;
  height: 40px;
  font-size: 22px;
  text-align: center;
  line-height: 40px;
  float: left;

}
.review-name{
  float: left;
  line-height: 40px;
  margin-left: 10px; 
}
ul.review-ratting{
  list-style: none;
  margin-left: 8px;
}
ul.review-ratting li{
  display:inline-block;
  margin-right: 40px;   
}
.review-comment{
  margin-left: 48px;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Buyer Reviews
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/users')}}">Users</a></li>
        <li class="active"><a href="{{url('admin/users/edit', $buyer_user->_id)}}">{{ucfirst($buyer_user->v_fname)}} {{ucfirst($buyer_user->v_lname)}}</a></li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Review</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($buyer_review) && count($buyer_review) )
                     @foreach( $buyer_review as $count => $value )
                     @php
                        $v_image="";
                        if(count($value->hasUser()) && isset($value->hasUser()->v_image)){
                            $v_image=$value->hasUser()->v_image;
                        }
                        $v_fname="";
                        if(count($value->hasUser()) && isset($value->hasUser()->v_fname)){
                            $v_fname=$value->hasUser()->v_fname;
                        }

                        $v_lname="";
                        if(count($value->hasUser()) && isset($value->hasUser()->v_lname)){
                            $v_lname=$value->hasUser()->v_lname;
                        }
                        
                    @endphp
                    <tr>
                      
                      <td>
                        <div class="col-md-12">
                          <div class="row">
                            <div class="col-md-12">
                              @if(isset($v_image) && $v_image != '' )
                                @php
                                  $imgdata = \Storage::cloud()->url($v_image);
                                @endphp 
                                <img src="{{$imgdata}}" class="user-avatar">
                              @else
                                @if(isset($v_fname) && $v_fname != '' || isset($v_lname) && $v_lname != '' )
                                  <div class="edit-user-avatar">
                                    {{strtoupper(substr($v_fname, 0, 1))}}{{strtoupper(substr($v_lname, 0, 1))}}
                                  </div>
                                @else
                                  @php
                                      $imgdata = \Storage::cloud()->url('users/img-upload.png');   
                                  @endphp
                                  <img src="{{$imgdata}}" class="user-avatar">
                                @endif  
                              @endif
                              <p class="review-name">{{ucfirst($v_fname)}} {{ucfirst($v_lname)}}</p>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <ul class="review-ratting">
                                <li>Person You Work For? <p><input value="{{$value->i_work_star or ''}}" class="star-input-review"></p></li>
                                <li>Support You Get <p><input value="{{$value->i_support_star or ''}}" class="star-input-review"></p></li>
                                <li>Growth opportunities <p><input type="" name="" value="{{$value->i_opportunities_star or ''}}" class="star-input-review"></p></li>
                                <li>Would You Work Again <p><input type="" name="" value="{{$value->i_work_again_star or ''}}" class="star-input-review"></p></li>
                              </ul>
                            </div>
                          </div>  
                          <p class="review-comment">{{$value->l_comment or ''}}</p>
                        </td>
                          
                        </div>

                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
</script>
<script src="{{url('public/Assets/frontend/star-rating/js/star-rating.js')}}"></script> 
<script type="text/javascript">
    var src1 = "{{url('public/Assets/frontend/images/star.png')}}";
    var src2 = "{{url('public/Assets/frontend/images/star1.png')}}";
    $("#star-input-1,#star-input-2,#star-input-3,#star-input-4,#star-input-5,.star-input-review").rating({
        min: 0,
        max: 5,
        step: 0.5,
        showClear: false,
        showCaption: false,
        theme: 'krajee-fa',
        filledStar: '<i class="fa"><img src="'+src1+'"></i>',
        emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
    });

    $("#f_rate_instruction,#f_rate_navigate,#f_rate_reccommend").rating({
        min: 0,
        max: 5,
        step: 0.5,
        showClear: false,
        showCaption: false,
        theme: 'krajee-fa',
        filledStar: '<i class="fa"><img src="'+src1+'"></i>',
        emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
    });
 </script>
@stop

