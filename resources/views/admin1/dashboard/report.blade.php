@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('/public/Assets/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css" />

<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Report Managment
        <small>Dashboard panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Report Managment</li>
      </ol>
    </section>

    <section class="content">
          <form class="horizontal-form" role="form" method="GET" name="adminForm"  action="" data-parsley-validate enctype="multipart/form-data" >
           <div class="row">
                <div class="col-md-2">
                  <div class="form-group">
                    <label>Start date:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="d_start_date" class="form-control pull-right" id="datepickerstart" value="{{isset($d_start_date)?$d_start_date:''}}" required="">
                    </div>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="form-group">
                    <label>End date:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="d_end_date" class="form-control pull-right" id="datepickerend" value="{{isset($d_end_date)?$d_end_date:''}}" required="">
                    </div>
                  </div>
                </div>
                 <div class="col-md-2">
                  <div class="form-group">
                    <label>&nbsp;</label>
                    <div class="input-group date">
                     <button type="submit" class="btn btn-primary">Search</button> 
                    </div>
                  </div>
                </div>
          </div>
          </form>
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
          <div class="box">
         
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><strong>Title</strong></th>
                  <th><strong>Number</strong></th>
                  <th><strong>Amount</strong></th>
                </tr>
                </thead>
                <tbody>
                   @if(isset($data) && count($data))
                        @foreach($data as $k=>$v)
                            <tr>
                                <td><strong>{{$v['title'] or ''}}</strong></td>
                                <td>{{$v['number'] or ''}}</td>
                                <td>
                                @if($v['amount']!="-")
                                  £{{$v['amount'] or '0'}}
                                @else
                                  {{$v['amount'] or '-'}}
                                @endif
                                </td>
                            </tr>

                        @endforeach
                  @else
                  <tr>
                    <td colspan="3" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js" type="text/javascript"></script>

<script src="{{url('/public/Assets/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script type="text/javascript">
 $('#datepicker').datepicker({
      autoclose: true
 })
 
 $('#datepickerend').datepicker({
      autoclose: true
    })
 $('#datepickerstart').datepicker({
      autoclose: true
 })

</script>
<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
      "dom": 'Bfrtip',
      "paging": false,
      "buttons": [
          'csv', 'pdf'
      ]
    });
  });
</script>

@stop

