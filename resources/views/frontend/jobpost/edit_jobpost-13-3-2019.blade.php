@extends('layouts.frontend')

@section('content')
<style type="text/css">
    .imgrem {position: relative;}
    .close-img{height: 15px; position: absolute;right: 15px;top: -30px;}
</style>
<style type="text/css">
  #introducyvideoid {
      height: 0;
      width: 0;
  }
.addimg_brdr .dropzone .dz-preview {
    margin: 0px 0px !important;
}
.addimg_brdr .dropzone .dz-preview, .dropzone-previews .dz-preview, .img-biography .dropzone-previews .dz-preview {
    border: 1px solid #8470ff !important;
}
.dropzone{ min-height: 255px; }
.addimg_brdr .dropzone.dz-started .dz-message, .addimg_brdr .dropzone.dz-clickable .dz-message {
    position: absolute;
    z-index: -11;
    left: 0;
    right: 0;
    text-align: center;
    top: 50%;
    transform: translate(10%, -50%);
    opacity: 1;
}
</style>
<link rel="stylesheet" href="{{url('public/Assets/plugins')}}/dropzone/css/dropzone.css">
  
<div class="modal fade" id="deleteModalimg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font">Confirm Remove Image ?</h3>
            </div>
            <div class="modal-body" style="text-align:center">
            <i class="margin-top-10 fa fa-question fa-5x"></i>
            <h4>Delete !</h4>
            <p>Are you sure you want to delete this image?</p>
            </div>
           <form name="deleteimg" id="deleteimgjobpost" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="i_job_id" id='i_job_id_delete'>
            <input type="hidden" name="v_photo_name" id='v_photo_name_delete'>
            <input type="hidden" name="v_span" id='v_span_delete'>
           </form>      
            <div class="modal-footer" style="padding-top: 20px;">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" type="button" onclick="jobpostimagedelete();" ><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
                <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" type="button" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModalvidso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font">Confirm Remove Video ?</h3>
            </div>
            <div class="modal-body" style="text-align:center">
            <i class="margin-top-10 fa fa-question fa-5x"></i>
            <h4>Delete !</h4>
            <p>Are you sure you want to delete this video?</p>
            </div>
           <form name="deletevideo" id="deletevideojobpost" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="i_job_id" id='i_job_id_video'>
            <input type="hidden" name="v_video_name" id='v_video_name_video'>
            <input type="hidden" name="v_span" id='v_span_video'>
           </form>      
            <div class="modal-footer" style="padding-top: 20px;">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" type="button" onclick="jobpostvideodelete();" ><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
                <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" type="button" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
</div>
   
    <!-- Navigation Bar -->
    <div class="navigation-bar">
        <div class="container">
            <div id="content">
                <ul id="tabs" class="nav nav-tabs nav-dirservice" data-tabs="tabs">
                    <li class="active"><a href="#step1" data-toggle="tab" class="tab-name"> 1. Step </a></li>
                    <li><a href="#step2" data-toggle="tab" class="tab-name aaa"> 2. Step </a></li>
                    @if($isPremiumPlan)
                    <li><a href="#step3" data-toggle="tab" class="tab-name aaa"> 3. Step </a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    <!-- End Navigation Bar -->


    <!-- 02B-sign-up-post-a-job-online-W1 -->
    <div class="container">
        <div id="my-tab-content" class="tab-content">
            
            <div class="tab-pane active" id="step1">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-dirservice">
                            <div class="step1-dirservices">
                                <div class="online-service">
                                    <h1>@if($v_service=="online")
                                            Online
                                        @else
                                            In Person
                                        @endif    
                                        Services </h1>
                                    <p>Post A Job </p>
                                </div>
                                
                                <form class="horizontal-form" role="form" method="POST" name="updatejobpost1" id="updatejobpost1" data-parsley-validate enctype="multipart/form-data" >

                                    <input type="hidden" name="v_service" value="{{$v_service}}">
                                    <input type="hidden" name="job_id" value="@if(isset($jobdata->id)){{$jobdata->id}}@endif">
                                    {{ csrf_field() }}
                                    <div class="dropdown">
                                        <label class="sel"> Select your desired category </label>
                                        <select class="form-control form-sel" name="i_category_id" onchange="getSkill(this.value)" required >
                                            <option value="">-Select-</option>
                                            @if(isset($category) && count($category))
                                                @foreach($category as $k=>$v)
                                                    @if(isset($v->v_name) && isset($v->id))
                                                    <option value = '{{$v->id}}' @if(isset($jobdata->i_category_id) && $jobdata->i_category_id==$v->id) selected @endif >{{$v->v_name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif                                                  
                                        </select>
                                    </div>
                                    
                                    <div class="dropdown">
                                        <label class="sel"> What main skill are you looking for? </label>
                                        <select class="form-control form-sel" name="i_mailskill_id" id="mainskill" required>
                                            <option value="">-Select-</option>
                                            @if(isset($skilldata) && count($skilldata))
                                                @foreach($skilldata as $k=>$v)
                                                    @if(isset($jobdata->i_mailskill_id) && $jobdata->i_mailskill_id==$v->id)
                                                    <option value = '{{$v->id}}' selected >{{$v->v_name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                    <?php 
                                        // echo "<pre>";
                                        //  print_r($skilldata);
                                        //  exit;
                                    ?>
                                    <div class="multiple-category">
                                        <label class="sel"> What other desirable skills are you looking for? </label>
                                        <select class="form-control form-sel" name="i_otherskill" id="otherskill" onchange="otherskilldata(this.value)" required>
                                            <option value="">-Select-</option>
                                            
                                        </select>
                                        
                                        <div class="new-button-f butt-form" id="sel_other_skill">
                                            @if(isset($other_skill_data) && count($other_skill_data))
                                                    @foreach($other_skill_data as $k=>$v)
                                                        
                                                        <button type="button" class="btn directservice-btn" id="btn_{{$k}}"> <img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" onclick="removeotherskill('{{$k}}')"> {{$v}} </button>
                                                        <input type="hidden" name="i_otherskill_id[]" id="hid_{{$k}}" class="item" value="{{$k}}">
                                                    @endforeach
                                            @endif
                                        </div>
                                        <div id="hiddendiv">
                                        </div> 
                                    </div>


                                    <div class="radio-choise">
                                        <label class="sel"> What type of skill level are you looking for?
                                        </label>
                                        <div class="reg">
                                            <bdo dir="ltr">
                                                <input type="radio" class="one" value="entry" name="v_skill_level_looking" @if(isset($jobdata->v_skill_level_looking) && $jobdata->v_skill_level_looking=='entry') checked @endif >
                                                <span></span>
                                                <abbr>Entry</abbr>
                                            </bdo>

                                            <bdo dir="ltr">
                                                <input type="radio" class="one" value="intermediate" name="v_skill_level_looking" @if(isset($jobdata->v_skill_level_looking) && $jobdata->v_skill_level_looking=='intermediate') checked @endif>
                                                <span></span>
                                                <abbr>Intermediate</abbr>
                                            </bdo>

                                            <bdo dir="ltr">
                                                <input type="radio" class="one" value="expert" name="v_skill_level_looking" @if(isset($jobdata->v_skill_level_looking) && $jobdata->v_skill_level_looking=='expert') checked @endif>
                                                <span></span>
                                                <abbr>Expert</abbr>
                                            </bdo>

                                        </div>
                                    </div>
                                  
                                  <hr class="hr-line">
                                  <div class="bottom-btn">
                                      
                                      <button type="button" class="btn form-save-exit ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="saveandexit1" onclick="saveStep1('saveandexit1')">Save &amp; Exit</button>
                                        
                                      <button type="button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savenext2" class="btn form-next ladda-button" onclick="saveStep1('savenext2')">Next</button>
                                  </div>

                                </form>
                                
                            </div>
                        </div>
                        
                        <div class="alert alert-info alert-danger" id="errormsg" style="display: none;">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;<span id="errortxt">Something went wrong.</span>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End 02B-sign-up-post-a-job-online-W1 -->

            <!-- 02B-sign-up-post-a-job-online-W2 -->
            <div class="tab-pane" id="step2">
                <div class=" row">
                    <div class="col-sm-12">
                        <div class="main-dirservice2">
                            <div class="step2-dirservice">
                                <div class="step2-containt">
                                    <div class="title-dirservice">
                                        <h2> @if($v_service=="online")
                                            Online
                                        @else
                                            In Person
                                        @endif    
                                        Services </h2>
                                        <p> Post A Job </p>
                                    </div>
                                    
                                    <form class="horizontal-form" role="form" method="POST" name="updatejobpost2" id="updatejobpost2" data-parsley-validate enctype="multipart/form-data" >
                                    
                                    {{ csrf_field() }}
                                    <input type="hidden" name="v_job_id" id="v_job_id2" value="@if(isset($jobdata->id)){{$jobdata->id}}@endif">

                                    <div class="dirservice-form">
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Job Title
                                                        </label>
                                                        <input type="text" class="form-control input-field" name="v_job_title" data-parsley-minlength="10" data-parsley-minlength-message="Title must be minimum 10 characters" data-parsley-maxlength="50" data-parsley-maxlength-message="Title must be no longer than 50 characters" data-parsley-trigger="keyup focusin focusout" required value="@if(isset($jobdata->v_job_title)){{$jobdata->v_job_title}}@endif">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="radio-choise">
                                                    <div class="col-xs-12">
                                                        <label class="field-name">
                                                            What budget do you have in mind?
                                                        </label>
                                                        <div class="">
                                                            <div class="reg reg_fill">
                                                                <bdo dir="ltr">
                                                                    <input type="radio" value="total_budget" onclick="budgetdata(this.value)" name="v_budget_type" @if(isset($jobdata->v_budget_type) && $jobdata->v_budget_type=='total_budget') checked @endif > 
                                                                    <span></span>
                                                                    <abbr> Total Budget </abbr>
                                                                </bdo>
                                                            </div>
                                                            <div class="reg reg_fill">
                                                                <bdo dir="ltr">
                                                                    <input type="radio" value="hourly_rate" onclick="budgetdata(this.value)" name="v_budget_type" @if(isset($jobdata->v_budget_type) && $jobdata->v_budget_type=='hourly_rate') checked @endif>
                                                                    <span></span>
                                                                    <abbr> Hourly Rate </abbr>
                                                                </bdo>
                                                            </div>
                                                            <div class="reg reg_fill">
                                                                <bdo dir="ltr">
                                                                    <input type="radio" onclick="budgetdata(this.value)" value="open_to_offers" name="v_budget_type" @if(isset($jobdata->v_budget_type) && $jobdata->v_budget_type=='open_to_offers') checked @endif>
                                                                    <span></span>
                                                                    <abbr> Open to Offers </abbr>
                                                                </bdo>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="dirservice-field" id="Specifybudget" @if(isset($jobdata->v_budget_type) && $jobdata->v_budget_type=='open_to_offers') style="display: none" @endif>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name" id = "budgettext" >
                                                            Specify your budget (£)
                                                        </label>
                                                        <input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true"  class="form-control input-field" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" name="v_budget_amt"   id="v_budget_amt" @if(isset($jobdata->v_budget_type) && $jobdata->v_budget_type!='open_to_offers') required @endif  value="@if(isset($jobdata->v_budget_amt)){{$jobdata->v_budget_amt}}@endif">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <script type="text/javascript">
                                            function budgetdata(data){
                                                if(data=="open_to_offers"){
                                                    $("#Specifybudget").css("display",'none');
                                                    $("#v_budget_amt").removeAttr("required");
                                                }else{
                                                    if(data=="hourly_rate"){
                                                        $("#budgettext").html("Specify your hourly rate");
                                                    }else{
                                                        $("#budgettext").html("Specify your budget");
                                                    }
                                                    $("#Specifybudget").css("display",'block');
                                                    $("#v_budget_amt").attr("required");
                                                }
                                            }
                                        </script>

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="textbox-description">
                                                        <label class="field-name">
                                                            Short Description (this will show at search listing)
                                                        </label>
                                                        <textarea rows="2" class="form-control cover-letter" data-parsley-maxlength="100" data-parsley-maxlength-message="Short Description must be no longer than 100 characters"  name="l_short_description" required>@if(isset($jobdata->l_short_description)){{$jobdata->l_short_description}}@endif</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    
                                                    <div class="textbox-description">
                                                        <label class="field-name" id="validatemessagedesclbl">
                                                            Describe the Job
                                                        </label>
                                                        <textarea class="ckeditor" name="l_job_description" id="l_job_description" required>@if(isset($jobdata->l_job_description)){{$jobdata->l_job_description}}@endif</textarea>

                                                         <div id="validatemessagedesc" style="display: none"><ul class="parsley-errors-list filled"><li class="parsley-required">Please enter description of job.</li></ul></div>

                                                    </div>
                                                    <script>
                                                        CKEDITOR.replace('l_job_description');
                                                    </script>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Website
                                                        </label>
                                                        <input type="text" class="form-control input-field" name="v_website" value="@if(isset($jobdata->v_website)){{$jobdata->v_website}}@endif">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Contact Phone
                                                        </label>
                                                        <input type="text" class="form-control input-field" data-parsley-type="number" name="v_contact" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" data-parsley-trigger="keyup" value="@if(isset($jobdata->v_contact)){{$jobdata->v_contact}}@endif">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="dirservice-field">
                                            
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <label class="field-name">
                                                        Your availability
                                                    </label>
                                                </div>
                                            </div>
                                            <?php
                                                
                                                $daydata=array(
                                                    'monday'=>'Monday',
                                                    'tuesday'=>'Tuesday',
                                                    'wednesday'=>'Wednesday',
                                                    'thursday'=>'Thursday',
                                                    'friday'=>'Friday',
                                                    'saturday'=>'Saturday',
                                                    'sunday'=>'Sunday',
                                                );

                                                $monthdata=array(
                                                    '1'=>'January',
                                                    '2'=>'February',
                                                    '3'=>'March',
                                                    '4'=>'April',
                                                    '5'=>'May',
                                                    '6'=>'June',
                                                    '7'=>'July',
                                                    '8'=>'August',
                                                    '9'=>'September',
                                                    '10'=>'Octomber',
                                                    '11'=>'November',
                                                    '12'=>'December',
                                                );
                                            ?>

                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-sm-5 col-xs-12">
                                                            <div class="timepicker-responsive">
                                                                <div class="dirservice-deshline">
                                                                    
                                                                    <div class="dirservice-desh">
                                                                        <select class="form-control date-concept" name="v_avalibility_from"  style="padding-left: 10px">
                                                                            <option value="">Select</option>
                                                                            @foreach($daydata as $k=>$v)
                                                                                <option value="{{$k}}" @if(isset($jobdata->v_avalibility_from) && $jobdata->v_avalibility_from==$k) selected @endif >{{$v}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>

                                                                    <select class="form-control date-concept" name="v_avalibility_to" style="padding-left: 10px">
                                                                    <option value="">Select</option>
                                                                        @foreach($daydata as $k=>$v)
                                                                            <option value="{{$k}}" @if(isset($jobdata->v_avalibility_to) && $jobdata->v_avalibility_to==$k) selected @endif>{{$v}}</option>
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-sm-5 col-xs-12">
                                                            <div class="timepicker-responsive">
                                                                <div class="time">
                                                                    <div class="control-group">
                                                                        <select class="form-control date-concept" name="v_avalibilitytime_from"  style="padding-left: 50px">
                                                                         <?php
                                                                                for($i=1;$i<=24;$i++){ ?>
                                                                                    <option value="{{$i}}" @if(isset($jobdata->v_avalibilitytime_from) && $jobdata->v_avalibilitytime_from==$i) selected @endif >{{$i}}</option>
                                                                            <?php } ?>
                                                                        </select>

                                                                    </div>
                                                                    
                                                                    <div class="control-group">
                                                                          <select class="form-control date-concept" name="v_avalibilitytime_to" required style="padding-left: 50px">
                                                                            <?php
                                                                                for($i=1;$i<=24;$i++){ ?>
                                                                                    <option value="{{$i}}" @if(isset($jobdata->v_avalibilitytime_to) && $jobdata->v_avalibilitytime_to==$i) selected @endif>{{$i}}</option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <label class="field-name">
                                                        When would you like the job to start?
                                                    </label>
                                                </div>
                                                <div class="col-sm-8 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-xs-12">
                                                            <div class="dirservice-slashline">
                                                                
                                                                <input type="text" name="d_start_date" class="form-control input-field form_datetime2" value="@if(isset($jobdata->d_start_date) && $jobdata->d_start_date!=""){{$jobdata->d_start_date}}@endif">
                                                                 <script type="text/javascript">
                                                                    $(".form_datetime2").datetimepicker({
                                                                        pickTime: false,
                                                                        minView: 2,
                                                                        format: 'yyyy-mm-dd',
                                                                        startDate: '-0m',
                                                                        //endDate: '+0d',
                                                                        autoclose: true,
                                                                    });
                                                                </script>

                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <style type="text/css">
                                          #workvideoid {
                                              height: 0;
                                              width: 0;
                                          }
                                        </style>
                                        @php $totalVideo=50; @endphp
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="open-browser">
                                                    <label class="field-name">
                                                        Add your videos (unlimited) (up to 150MB) <br>
                                                        <span style="font-size: 14px;">Supported formats: mp4, mkv, webm, mov, m4v <br></span>
                                                    </label>
                                                    <div class="addimg_brdr">
                                                        <div class="form-group">
                                                            <div class="cl-mcont" style="">
                                                                <div action="{{url('job-post/ajax/upload/video')}}/{{$jobdata->id}}" class="" id="jobpost_video" multiple="true" >
                                                                   @if(isset($jobdata->v_video) && count($jobdata->v_video))
                                                                        @php $totalVideo = 10-count($jobdata->v_video); @endphp
                                                                        @foreach($jobdata->v_video as $k=>$v)
                                                                            @php
                                                                                $videodata = \Storage::cloud()->url($v);     
                                                                            @endphp
                                                                                <div class="dropzone-previews" style="width: auto;display: inline-block;" id="rem{{$k}}">
                                                                                    <div class="dz-preview dz-processing dz-success dz-image-preview">
                                                                                    <div class="dz-details"> 
                                                                                         <video width="100px" height="100px" controls>
                                                                                            <source src="{{$videodata}}" type="video/mp4">
                                                                                        </video> 
                                                                                    </div>
                                                                                    <a href="javascript:" onclick="remove_Video('{{$v}}','{{$k}}','{{$jobdata->id}}')" class="dz-remove">Remove Video</a></div>
                                                                                </div>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                             var totalphotos = '{{$totalVideo}}';
                                             var mydropzonework;    
                                             jQuery(document).ready(function(){
                                                jQuery("#jobpost_video").addClass("dropzone");
                                               mydropzonework = new Dropzone("#jobpost_video", {
                                                  multiple : true,
                                                  maxFiles:150,
                                                  maxFilesize:150,
                                                  parallelUploads: 1,
                                                  acceptedFiles: ".mp4,.mkv,.webm,.mov,.m4v",
                                                  }).on("complete", function(file) {
                                                       $("#jobpost_video").html(file.xhr.response);    
                                                  }).on("removedfile", function(file) {
                                                  });
                                               });

                                               function remove_Video(iname,indexdata,jid){
                                                    document.getElementById('load').style.visibility="visible"; 
                                                    var formData = "name="+iname+"&jid="+jid;
                                                    var actionurl="{{url('job-post/ajax/remove/video')}}";
                                                    $.ajax({
                                                        processData: false,
                                                        contentType: false,
                                                        type  : "GET",
                                                          url     : actionurl,
                                                          data    : formData,
                                                          success : function( res ){
                                                            document.getElementById('load').style.visibility='hidden';
                                                            $("#rem"+indexdata).remove();
                                                             mydropzonework.options.maxFiles = mydropzonework.options.maxFiles + 1;
                                                          },
                                                          error: function ( jqXHR, exception ) {
                                                          }
                                                    });
                                                }
                                        </script>

                                        @php $totalPhotos=50; @endphp
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="open-browser">
                                                    <label class="field-name" id="validatemessagephotolbl">
                                                        Add your photos (up to 10)<br>
                                                        <span style="margin-top:10px;display: inline-block;font-size: 14px;">H : 435px , W : 650px </span><br>
                                                    </label>
                                                    <div class="addimg_brdr">
                                                        <div class="form-group">
                                                            <div class="cl-mcont" style="">
                                                                <div action="{{url('job-post/ajax/upload/photos')}}/{{$jobdata->id}}" class="" id="jobpost_photos" multiple="true" >
                                                                   @if(isset($jobdata->v_photos) && count($jobdata->v_photos))
                                                                        @php $totalPhotos = 10-count($jobdata->v_photos); @endphp
                                                                        @foreach($jobdata->v_photos as $k=>$v)
                                                                            @php
                                                                                $imgdaata = \Storage::cloud()->url($v);     
                                                                            @endphp
                                                                                <div class="dropzone-previews" style="width: auto;display: inline-block;" id="remph{{$k}}">
                                                                                    <div class="dz-preview dz-processing dz-success dz-image-preview">
                                                                                    <div class="dz-details"> 
                                                                                         <img src="{{$imgdaata}}" style="max-width:100px;">
                                                                                    </div>
                                                                                    <a href="javascript:" onclick="remove_Photos('{{$v}}','{{$k}}','{{$jobdata->id}}')" class="dz-remove">Remove Image</a></div>
                                                                                </div>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="validatemessagephoto" style="display: none"><ul class="parsley-errors-list filled"><li class="parsley-required">Please upload your photos.</li></ul></div>

                                                </div>
                                            </div>
                                        </div>



                                        <script type="text/javascript">
                                             var totalphotos = '{{$totalPhotos}}';  
                                             var mydropzonework;      
                                             jQuery(document).ready(function(){
                                                jQuery("#jobpost_photos").addClass("dropzone");
                                                mydropzonework = new Dropzone("#jobpost_photos", {
                                                  multiple : true,
                                                  maxFiles:parseInt(totalphotos),
                                                  maxFilesize:150,
                                                  parallelUploads: 1,
                                                  acceptedFiles: ".jpeg,.jpg,.png",
                                                  }).on("complete", function(file) {
                                                      $("#jobpost_photos").html(file.xhr.response);    
                                                  }).on("removedfile", function(file) {
                                                       //removeWork_video();
                                                  });
                                               });

                                               function remove_Photos(iname,indexdata,jid){
                                                    document.getElementById('load').style.visibility="visible"; 
                                                    var formData = "name="+iname+"&jid="+jid;
                                                    var actionurl="{{url('job-post/ajax/remove/photos')}}";
                                                    $.ajax({
                                                        processData: false,
                                                        contentType: false,
                                                        type  : "GET",
                                                          url     : actionurl,
                                                          data    : formData,
                                                          success : function( res ){
                                                            document.getElementById('load').style.visibility='hidden';
                                                            $("#remph"+indexdata).remove();
                                                            mydropzonework.options.maxFiles = mydropzonework.options.maxFiles + 1;
                                                          },
                                                          error: function ( jqXHR, exception ) {
                                                          }
                                                    });
                                                }
                                         </script>

                                        <?php /*
                                        <div class="row">
                                            
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="open-browser">
                                                    <label class="field-name">
                                                        Add your videos (unlimited)
                                                    </label>
                                                    <input type="text" class="form-control input-field" id="work_video" disabled>
                                                    <div class="text-right">
                                                        <input type='file'  id="workvideoid" accept="video/mp4,video/x-m4v,video/*" onchange="workvideodata(this.value);" name="work_video[]" multiple="multiple" />
                                                        <label id="fileupload"  class="btn-editdetail" for="workvideoid">Browser</label>
                                                    </div>

                                                </div>
                                            </div>
                                            
                                            <script type="text/javascript">
                                                    
                                                function workvideodata(data){

                                                    var fp = $("#workvideoid");
                                                    var lg = fp[0].files.length;
                                                    var items = fp[0].files;
                                                    var fileSize = 0;
                                                    if (lg > 0) {
                                                       for (var i = 0; i < lg; i++) {
                                                           fileSize = fileSize+items[i].size; 
                                                       }
                                                       if(fileSize > 1007) {
                                                            alert('File size not be more than 100 MB');
                                                            $('#'+data).val('');
                                                            return false;
                                                       }
                                                    }
                                                    
                                                    var filename = data.split('\\').pop();
                                                    var lastIndex = filename.lastIndexOf("\\");   
                                                    $('#work_video').val(filename);
                                              }

                                            </script>
                                            
                                            <style type="text/css">
                                                  #workphotoid {
                                                      height: 0;
                                                      width: 0;
                                                  }
                                            </style> 

                                            <div class="col-sm-6 col-xs-12">
                                                <div class="open-browser">
                                                    <label class="field-name">
                                                        Add your photos (up to 10)
                                                    </label>
                                                    <input type="text" class="form-control input-field" id="work_photo" disabled>
                                                    
                                                    @if(!$isPremiumPlan)
                                                    <ul class="parsley-errors-list filled" id="photoupload" style="display: none;"><li class="parsley-required">Maximum 10 photo upload.</li></ul>
                                                    @endif

                                                    <div class="text-right">
                                                        <input type='file' id="workphotoid" accept="image/x-png,image/gif,image/jpeg" onchange="workphotodata(this.value)"  name="work_photo[]" multiple="multiple"/>
                                                        <label id="fileupload" class="btn-editdetail" for="workphotoid">Browser</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <script type="text/javascript">
                                        
                                               function workphotodata(data){
                                                    var filename = data.split('\\').pop();
                                                    var lastIndex = filename.lastIndexOf("\\");   
                                                    $('#work_photo').val(filename);
                                                    if($("#workphotoid")[0].files.length > 2) {
                                                        $("#photoupload").css("display","inline");
                                                     } 
                                                }
                                            </script>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                            


                                            @if(isset($jobdata->v_video) && count($jobdata->v_video))
                                                @foreach($jobdata->v_video as $k=>$v)
                                                    @php
                                                        $videodata = \Storage::cloud()->url($v);     
                                                    @endphp
                                                   <span class="imgrem" id="videorem{{$k}}">
                                                    <video width="100" height="100" controls>
                                                      <source src="{{$videodata}}" type="video/mp4">
                                                    </video> 
                                                   <img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" onclick="removevideojobpost('{{$jobdata->id}}','{{$v}}','videorem{{$k}}')" style="height: 15px;position: absolute;right: 15px;top: -74px;">
                                                   </span>
                                                @endforeach
                                            @endif
                                            </div>
                                        

                                            <div class="col-sm-6 col-xs-12">
                                            
                                            @if(isset($jobdata->v_photos) && count($jobdata->v_photos))
                                                @foreach($jobdata->v_photos as $k=>$v)
                                                    @php
                                                        $imgdata = \Storage::cloud()->url($v);     
                                                    @endphp
                                                   <span class="imgrem" id="imgrem{{$k}}">
                                                   <img src="{{$imgdata}}" style="height: 100px;width: 100px">
                                                   <img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" onclick="removeimgjobpost('{{$jobdata->id}}','{{$v}}','imgrem{{$k}}')" class="close-img">
                                                   </span>
                                                @endforeach
                                            @endif

                                            </div>
                                        </div>
                                        */ ?>

                                        <?php   
                                           $timeschedule=array(
                                                'hours'=>'Hour(s)',
                                                'day'=>"Day(s)",
                                                'months'=>"Month(s)",
                                                'year'=>"Year(s)",
                                            ); 
                                        ?>  

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="subfield-name">
                                                        How long do you expect the job to last?
                                                    </label>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <div class="dilivery-hours">
                                                                
                                                                <input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" class="form-control input-field" name="i_long_expect_duration"  value="@if(isset($jobdata->i_long_expect_duration)){{$jobdata->i_long_expect_duration}}@endif" id="i_long_expect_duration" @if(isset($jobdata->i_permanent_role) && $jobdata->i_permanent_role=='on') disabled @else required @endif>

                                                            </div>
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <div class="dilivery-time">
                                                                
                                                                <select class="form-control form-sell" name="v_long_expect_duration_type" required id="v_long_expect_duration_type"  @if(isset($jobdata->i_permanent_role) && $jobdata->i_permanent_role=='on') disabled @endif>
                                                                    @foreach($timeschedule as $k=>$v)
                                                                        <option value="{{$k}}" @if(isset($jobdata->v_long_expect_duration_type) && $jobdata->v_long_expect_duration_type==$k) selected @endif >{{$v}}</option>
                                                                    @endforeach
                                                                </select>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="checkbox-space">
                                                        <label class="checkbox">
                                                            <input type="checkbox" name="i_permanent_role" id="i_permanent_role" onchange="durationdata(this.value)"  @if(isset($jobdata->i_permanent_role) && $jobdata->i_permanent_role=='on') checked @endif>
                                                            <span class="subfield-name"> 
                                                                Permanent role
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <script type="text/javascript">
                                            
                                            function durationdata(data){
                                                var a=$('#i_permanent_role').is(":checked")
                                                if(a==true){
                                                    $('#i_long_expect_duration').attr("disabled", "disabled"); 
                                                    $('#i_long_expect_duration').val(0); 
                                                    $('#v_long_expect_duration_type').attr("disabled", "disabled"); 
                                                    $('#v_long_expect_duration_type').val("");
                                                    $('#v_long_expect_duration_type').removeAttr("required");
                                                }else{
                                                   $('#i_long_expect_duration').removeAttr("disabled");
                                                   $('#v_long_expect_duration_type').removeAttr("disabled"); 
                                                   $('#v_long_expect_duration_type').attr("required");
                                                }
                                            }
                                        </script>

                                        <div class="dirservice-field">
                                            <div class="row">
                                               
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name" >
                                                            City
                                                        </label>
                                                        <input type="text" id="autocomplete" class="form-control input-field" name="v_city" required value="@if(isset($jobdata->v_city)){{$jobdata->v_city}}@endif" autocomplete="off" >
                                                    </div>
                                                </div>

                                                <input type="hidden" name="v_latitude" id="v_latitude" value="@if(isset($jobdata->v_latitude)){{$jobdata->v_latitude}}@endif">
                                                <input type="hidden" name="v_longitude" id="v_longitude" value="@if(isset($jobdata->v_longitude)){{$jobdata->v_longitude}}@endif">


                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Postcode
                                                        </label>
                                                        <input type="text" class="form-control input-field" name="v_pincode" id="v_pincode" required value="@if(isset($jobdata->v_pincode)){{$jobdata->v_pincode}}@endif" >
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="buttet-concept">
                                            
                                            <div class="name-details">
                                                <label class="field-name">
                                                    Add search tags
                                                </label>
                                            </div>
                                            
                                            <div class="name-details">
                                                <label class="subfield-name">
                                                    Please add up to 3 search tags related to your service or job.
                                                </label>
                                            </div>
                                           
                                            
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-10">
                                                            <div class="input-login">
                                                                <input class="form-control business-name-control" type="text" placeholder="Start typing ...." id="temp_text">
                                                                <ul class="parsley-errors-list filled" id="parsley-id-tags" style="display: none;">
                                                            <li class="parsley-type">You have already enter 3 tags.</li>
                                                            </ul>
                                                            <ul class="parsley-errors-list filled" id="parsley-id-req" style="display: none;">
                                                            <li class="parsley-type">Please enter search tag.</li>
                                                            </ul>

                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 terms-add" id="hiddentags">
                                                               @php $tagcnt = count($jobdata->v_search_tags); @endphp 
                                                             @if(isset($jobdata->v_search_tags) && count($jobdata->v_search_tags))
                                                                    @foreach($jobdata->v_search_tags as $k=>$v)
                                                                       <input type="hidden" id="hiddenedit-edit{{$k}}" name="v_search_tags[]" value="{{$v}}"> 
                                                                    @endforeach
                                                                @endif
                                                            <div class="login-btn" id="addtagbutton" @if($tagcnt>=3)  style="display: none" @endif>
                                                                <button type="button" id="adddatafun" class="btn add-now" onclick="myFunction()">Add</button>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <div class="selection-point" id="add_text">
                                                                @if(isset($jobdata->v_search_tags) && count($jobdata->v_search_tags))
                                                                    @foreach($jobdata->v_search_tags as $k=>$v)
                                                                        <button class='auto-width' type="button" id='edit{{$k}}' onclick='removeButtonedit(this.id)'><span>{{$v}}</span><img src='{{url("public/Assets/frontend/images/close1.png")}}'></button>
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div id="validatemessagetags" style="display: none;"><ul class="parsley-errors-list filled"><li class="parsley-required">Please add at least one search tag to proceed.</li></ul></div>
                                        
                                        </div>

                                        <div class="btn-backnext">

                                            <button type="button" class="btn form-save-exit ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="saveback1" onclick="saveStep2('saveback1')"> Save & Exit</button>

                                            @if($isPremiumPlan)
                                            <button type="button" class="btn form-next ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savenext3" onclick="saveStep2('savenext3')"> Next </button>
                                            @else
                                            <button type="button" class="btn form-next ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="saveexit" onclick="saveStep2('saveexit')"> Next </button>    
                                            @endif
                                        
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                       
                        <div style="clear: both"></div>    
                        <div class="alert alert-info alert-danger" id="errormsgstep2" style="display: none;margin-top: 20px;">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;<span id="errortxtstep2">Something went wrong.</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End 02B-sign-up-post-a-job-online-W2 -->

            <script type="text/javascript">
                $('#temp_text').keypress(function(event){
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if(keycode == 13){
                        $('#adddatafun').click();
                    }
                });
            </script>

            @php
                $standardplanFree= App\Helpers\General::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
            @endphp  

            @if($isPremiumPlan)
            <div class="tab-pane" id="step3">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main-dirservice3">
                            
                            <form class="horizontal-form" role="form" method="POST" name="planform" id="planform" action="{{url('job-post/plan-data')}}" data-parsley-validate enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            <input type="hidden" name="i_job_id" id="i_job_id_step_3" value="@if(isset($jobdata->id)){{$jobdata->id}} @endif">
                            <div class="step3-dirservice">
                                <div class="title-dirservice">
                                    <h2>@if($v_service=="online")
                                            Online
                                        @else
                                            In Person
                                        @endif    
                                        Services</h2>
                                    <p> Post A Job </p>
                                </div>

                                <div class="foll-option">
                                    Please choose the plan that best fits your needs
                                </div>
                                <div class="wrap">
                                        <fieldset>
                                            <div class="toggle">
                                                <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join" checked onchange="plandurationchange('monthly')" >
                                                <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                                <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create" onchange="plandurationchange('yearly')">
                                                <label for="create" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                                <span class="toggle-selection"></span>
                                            </div>
                                        </fieldset>
                                </div>
                            </div>

                            <div class="createLabel">
                            
                            @php
                                $selected="";    
                            @endphp

                            @if(count($plandata))
                                @foreach($plandata as $key=>$val)

                                @if($val['id'] == $userplan['id'])
                                    @php $selected =1; @endphp
                                @endif       
                                @if($selected==1)
                                    <div class="section">
                                        <div class="row matchHeights"> 
                                            
                                            <div class="col-sm-3 col-xs-12">
                                                <div class="@if($key==0)left-choise @else left-side @endif matchheight_div">
                                                    <div class="reg">
                                                        <bdo dir="ltr">
                                                            <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required @if($val['id'] == $userplan['id']) checked  @endif>
                                                            <span class="select-seller-job"></span>
                                                            <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                        </bdo>
                                                    </div>
                                                    <div class="choise-que">{{$val['v_subtitle']}}</div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-3 col-xs-12">
                                                <ul class="list-unstyled choise-avalible matchheight_div">
                                                    
                                                    <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k>=3)
                                                            <li class="right-mark"><img src="images/tick.png" class="tick" alt="" />{{$v}}</li>
                                                            <?php $lcnt = $lcnt+1; ?>
                                                            @endif
                                                        @endforeach
                                                    @endif       
                                                    
                                                </ul>
                                            </div>
                                            
                                            <div class="col-sm-4 col-xs-12">
                                                <div class="right-display matchheight_div">
                                                    
                                                    <ul class="list-unstyled choise-avalible">
                                                        <?php 
                                                            $lcnt=1;
                                                        ?>
                                                        @if(count($val['l_bullet']))
                                                            @foreach($val['l_bullet'] as $k=>$v) 
                                                                @if($k>=3)
                                                                <li class="right-mark"><img src="images/tick.png" class="tick" alt="" />{{$v}}</li>
                                                                @endif
                                                                <?php $lcnt = $lcnt+1; ?>
                                                            @endforeach
                                                        @endif  
                                                    </ul>

                                                    {{-- @if($val['id'] != "5a65b9f4d3e8124f123c986c")

                                                    @if(!isset($jobdata->e_urgent))
                                                    <label class="checkbox">
                                                        <input type="checkbox" value="5" name="v_plan[jobposturgent]">
                                                        <span class="urgent-offer"> 
                                                            Make as Urgent <del>£7</del> offer £5
                                                        </span>
                                                    </label>
                                                    @endif

                                                    @if(!isset($jobdata->e_urgent))
                                                    <label class="checkbox">
                                                        <input type="checkbox" value="3" name="v_plan[e_notify]">
                                                        <span class="urgent-offer"> 
                                                            Notify Seller about your Job Posting <del>£5</del> offer £3
                                                        </span>
                                                    </label>
                                                    @endif
                                                    
                                                    @endif --}}

                                                    {{--
                                                    @if(count($val['l_addon']['text']))
                                                        @foreach($val['l_addon']['text'] as $k=>$v) 
                                                            <label class="checkbox">
                                                                <input type="checkbox" value="{{$val['l_addon']['price'][$k]}}" name="v_plan[{{$v}}]">
                                                                <span class="urgent-offer"> 
                                                                    {{$v}}
                                                                </span>
                                                            </label>
                                                        @endforeach
                                                    @endif 
                                                    --}}


                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-2 col-xs-12 same-height-1">
                                                <div class="@if($key==0) free-price @else final-price @endif matchheight_div">
                                                    <div class="all-in-data">
                                                        
                                                    <div  class="monthyprice">
                                                           @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_monthly_price']}}</del>
                                                                </div>
                                                                Only £{{$val['f_monthly_dis_price']}} p/m

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif


                                                           @endif  
                                                    </div>  
                                                    
                                                    <div class="yealryprice" style="display: none;">
                                                           @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_yealry_price']}}</del>
                                                                </div>
                                                                Only £{{$val['f_yealry_dis_price']}} p/a

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif

                                                           @endif  
                                                    </div>     

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                @endif
                                  
                                @endforeach
                            @endif    
                            </div>
                            
                            <button type="submit" class="btn btn-payment">
                                Proceed
                            </button>
                            </form>
                       
                        </div>
                    </div>
                </div>
            </div>
            @endif

        </div>
        <!-- End 02B-sign-up-i-want-to-sell-in-person-SELLER-W3 -->
    </div>
    <!-- End step 1- Direct Services -->
  

@stop
@section('js')

<script src="{{url('public/Assets/plugins')}}/dropzone/dropzone.js"></script>

<script type="text/javascript">

    $(document).ready(function(){
        var other_id = [];
        var cat_id = '{{isset($jobdata->i_category_id) ? $jobdata->i_category_id : ''}}';
        var nonblanks = $('.item[value!=""]').length;
        if(cat_id != ''){
            getSelOtherSkill(cat_id);
        }
        if(nonblanks != 0){
            $("#otherskill").prop('required',false);
        }
    });

     //Plan duration change
        function plandurationchange(data=""){
            
            if(data=="monthly"){
                $(".monthyprice").css("display","block");
                $(".yealryprice").css("display","none");

            }else if(data=="yearly"){
                $(".yealryprice").css("display","block");
                $(".monthyprice").css("display","none");
            }
        }

        

        function myFunction() {

            var cnt = $('#add_text').children().length

            if(cnt>=3){
                $("#parsley-id-tags").css("display","block");
                jQuery('#temp_text').val("");
                return 0;
            }else{
                $("#parsley-id-tags").css("display","none");
            }

            var dt = new Date();
            var dynamic_id = dt.getDate() + "" + dt.getHours() + "" + dt.getMinutes() + "" + dt.getSeconds() + "" + dt.getMilliseconds();
            var y = jQuery('#temp_text').val();
            var z = '<button>';

            var y = jQuery('#temp_text').val();
            if(y==""){
                $("#parsley-id-req").css("display","block");
                return 0;
            }
            
            jQuery('#add_text').append("<button class='auto-width' type='button' id='" + dynamic_id + "' onclick='removeButton(this.id)'><span>" + y + "</span><img src='{{url('public/Assets/frontend/images/close1.png')}}'></button>");

            

            var cnt = $('#add_text').children().length

            if(cnt==3){
                $("#addtagbutton").css("display","none");
            }

            var abval = $("#temp_text").val();
            var strhidden = '<input type="hidden" id="hidden-'+dynamic_id+'" name="v_search_tags[]" value="'+abval+'">';
            $("#hiddentags").append(strhidden);
            $("#temp_text").val("");
            $("#validatemessagetags").css("display","none");
        }

        function removeButton(id) {
            jQuery("#" + id).remove();
            jQuery("#hidden-"+id).remove();
            var cnt = $("#add_text").find("button").length;//$('#add_text').children().length
            if(cnt<3){
                $("#addtagbutton").css("display","block");
            }

        }

        function removeButtonedit(id) {
            jQuery("#" + id).remove();
            jQuery("#hiddenedit-"+id).remove();
            var cnt = $("#add_text").find("button").length;//$('#add_text').children().length
            if(cnt<3){
                $("#addtagbutton").css("display","block");
            }

        }
         
        function getSkill(i_category_id){
            
            var formdata = "i_category_id="+i_category_id;
            var actionurl = "{{url('seller-signup/get-skill')}}";

            $.ajax({
                  type    : "GET",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){

                        $("#sel_other_skill").html('');
                        $("#otherskill").prop('required',true);
                        $("#mainskill").html(obj['optionstr']);
                        $("#otherskill").html(obj['optionstr']);

                      }
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                  }
              });    
        }

        function getSelOtherSkill(i_category_id){
            
            var formdata = "i_category_id="+i_category_id;
            var actionurl = "{{url('seller-signup/get-skill')}}";

            $.ajax({
                  type    : "GET",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){

                        $("#otherskill").html(obj['optionstr']);

                        var other_ids = <?php echo json_encode($jobdata->i_otherskill_id); ?>;
                        $.each(other_ids, function(key, value) {
                           
                             $("#otherskill option[value="+value+"]").remove();
                        });
                        
                        
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                  }
              });    
        }

        function saveStep1(type=""){
                
            var actionurl = "{{url('job-post/update-step-1')}}";
            var formValidFalg = $("#updatejobpost1").parsley().validate('');

            if(formValidFalg){
                
                document.getElementById('load').style.visibility="visible";
                var l = Ladda.create(document.getElementById(type));
                l.start();  
                var formdata = $("#updatejobpost1").serialize();

                $.ajax({
                    type    : "POST",
                    url     : actionurl,
                    data    : formdata,
                    success : function( res ){
                        l.stop();
                        var obj = jQuery.parseJSON(res);
                          
                        if(obj['status']==1){
                            //$(".aaa")
                            window.scrollTo(0, 0);
                            if(type=="savenext2"){
                                $("#v_job_id2").val(obj['jobid']);
                                $('.nav-tabs a[href="#step2"]').tab('show');
                            }else{
                                window.location = "{{url('job-postings')}}";
                            }

                        }else{
                            $("#errormsg").show();
                            $("#errortxt").html(obj['msg']);
                        }
                        document.getElementById('load').style.visibility='hidden';
                    },
                    error: function ( jqXHR, exception ) {
                        l.stop();
                        $("#errormsg").show();
                        document.getElementById('load').style.visibility='hidden';
                    }
                });
            }    
        }

        function saveStep2(type=""){
          
          var actionurl = "{{url('job-post/update-step-2')}}";
          var formValidFalg = $("#updatejobpost2").parsley().validate('');
          
            var desc = CKEDITOR.instances['l_job_description'].getData();
            
            if(desc==""){
                $("#validatemessagedesc").css("display","block");
                $('html, body').animate({
                    scrollTop: $("#validatemessagedesclbl").offset().top
                }, 2000);
                return false;
            }else{
                $("#validatemessagedesc").css("display","none");
            }

            var workdata_photo = $("#jobpost_photos").find(".dropzone-previews").length
            if(workdata_photo<1){
                $("#validatemessagephoto").css("display","block");
                $('html, body').animate({
                    scrollTop: $("#validatemessagephotolbl").offset().top
                }, 2000);
                return false;
            }else{
                $("#validatemessagephoto").css("display","none");
            }

            var tags = $("#add_text").find("button").length
            if(tags<1){
                $("#validatemessagetags").css("display","block");
                return false;
            }else{
                $("#validatemessagetags").css("display","none");
            }


           if(formValidFalg){

              document.getElementById('load').style.visibility="visible";  
              var l = Ladda.create(document.getElementById(type));
              l.start();
              
              var formData = new FormData($('#updatejobpost2')[0]);
              formData.append('l_job_description', desc);


              $.ajax({
                  processData: false,
                  contentType: false,
                  type    : "POST",
                  url     : actionurl,
                  data    : formData,
                  success : function( res ){
                      
                      l.stop();
                      var obj = jQuery.parseJSON(res);
                      
                      if(obj['status']==1){
                        
                        if(type == "savenext3"){
                            window.scrollTo(0, 0);
                            $('.nav-tabs a[href="#step3"]').tab('show');
                        }else if(type == "saveexit"){
                            window.location = "{{url('job-postings')}}";
                        }else{
                            window.location = "{{url('job-postings')}}";
                            //$('.nav-tabs a[href="#step1"]').tab('show');
                        }
                      }else{
                        $("#errormsgstep2").show();
                        $("#errortxtstep2").html(obj['msg']);
                      }
                      document.getElementById('load').style.visibility='hidden';
                  },
                  error: function ( jqXHR, exception ) {
                      l.stop();
                      $("#errormsgstep2").show();
                      document.getElementById('load').style.visibility='hidden';
                  }

              });

          }    
        }


        function removeimgjobpost(jid,data,remimg){
            $("#i_job_id_delete").val(jid);
            $("#v_photo_name_delete").val(data);
            $("#v_span_delete").val(remimg);
            $("#deleteModalimg").modal("show");
        }
       
        function jobpostimagedelete(){
            
            var v_span = $("#v_span_delete").val();
            var formdata = $("#deleteimgjobpost").serialize();
            var actionurl = "{{url('job-post/remove-img')}}";
            document.getElementById('load').style.visibility="visible";

            $.ajax({
                  type    : "POST",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#"+v_span).remove();
                        $("#deleteModalimg").modal("hide");
                      }
                      document.getElementById('load').style.visibility='hidden';
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                      document.getElementById('load').style.visibility='hidden';
                  }
              });    
        }

        function removevideojobpost(jid,data,remimg){
            $("#i_job_id_video").val(jid);
            $("#v_video_name_video").val(data);
            $("#v_span_video").val(remimg);
            $("#deleteModalvidso").modal("show");
        }

         function jobpostvideodelete(){
            
            var v_span = $("#v_span_video").val();
            var formdata = $("#deletevideojobpost").serialize();
            var actionurl = "{{url('job-post/remove-video')}}";
            document.getElementById('load').style.visibility="visible";

            $.ajax({
                  type    : "POST",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#"+v_span).remove();
                        $("#deleteModalvidso").modal("hide");
                      }
                      document.getElementById('load').style.visibility='hidden';
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                      document.getElementById('load').style.visibility='hidden';
                  }
              });    
        }

     function otherskilldata(data){
        
        if(data!=""){
            
            var hiddenstr = '<input type="hidden" name="i_otherskill_id[]" id="hid_'+data+'" class="item" value="'+data+'">';
            var img_url = "{{url('public/Assets/frontend/images/pinkclose.png')}}";
            var selectedOtherskill = $("#otherskill option:selected").text();
            $("#otherskill option[value='"+data+"']").remove();
            $("#otherskill").removeAttr('required');
            var buttonstr ="<button type='button' class='btn directservice-btn' id='btn_"+ data +"' >"+ selectedOtherskill +" <img src='"+ img_url +"' onclick='removeotherskill(";
            buttonstr +='"'+data+'")';
            buttonstr +="'></button>";
            $("#hiddendiv").append(hiddenstr);
            $("#sel_other_skill").append(buttonstr);
        }
     }   

     function removeotherskill(data){

        
        var textdata = $("#mainskill option[value='"+data+"']").text();
        var stroption = "<option value='"+data+"'>"+textdata+"</option>";
       
        $("#otherskill").append(stroption);
        $('#btn_'+data).remove();
        $('#hid_'+data).remove();
        var nonblanks = $('.item[value!=""]').length;
        
        if(nonblanks == 0){
            $("#otherskill").prop('required',true);
        }

     }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeUrl5sq-j9P3aar5jKpOiTqralR5T5GE&libraries=places&callback=initAutocomplete" async defer></script>
<script>

        var placeSearch, autocomplete;
        var componentForm = {
            premise: 'long_name',
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'long_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {

            var place = autocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            $("#v_latitude").val(lat);
            $("#v_longitude").val(lng);

            var premise = '';
            var street_number = '';
            var route = '';

            for ( var i = 0; i < place.address_components.length; i++ ) {

                var addressType = place.address_components[i].types[0];

                if ( componentForm[addressType] && addressType == 'premise') {
                    premise = place.address_components[i][componentForm[addressType]];
                }
                if ( componentForm[addressType] && addressType == 'street_number') {
                    street_number = place.address_components[i][componentForm[addressType]];
                }
                if ( componentForm[addressType] && addressType == 'route') {
                    route = place.address_components[i][componentForm[addressType]];
                }
                else if ( componentForm[addressType] && addressType == 'postal_code') {
                    $('#v_pincode').val( place.address_components[i][componentForm[addressType]] );
                }
            }

            var address = '';

            if( premise != '' ) {
                address += ' ' + premise;
            }
            else {
                address += premise;
            }

            if( street_number != '' ) {
                address += ' ' + street_number;
            }
            else {
                address += street_number;
            }

            if( route != '' ) {
                address += ' ' + route;
            }
            else {
                address += route;
            }
            $('#v_city').val(address);

        }

            function geolocate() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        var circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocomplete.setBounds(circle.getBounds());
                    });
                }
            }

            $("#autocomplete").attr("autocomplete", "off");
            $("#d_start_date").attr("autocomplete", "off");
             
</script>

@stop

