@extends('layouts.frontend')
@section('content')
<style type="text/css">
   .modal-new .modal-backdrop {z-index: 0;}
</style>

<div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="title-support">
                    <h1> Payment with PayPal </h1>
                </div>
            </div>
        </div>
    </div>

<div class="container">

    <div class="row" style="margin: 5px 0px;">
        <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
           <label class="field-edit-name" style="margin-top: 5px;">
            <h2><span>Processing Payment...</span></h2>
           </label>
        </div>
    </div>

    <form action="{{$paypalURL}}" method="post" name="paypal_form" id="paypal_form">
          <input type="hidden" name="business" value="{{$paypalID}}">
          <input type="hidden" name="cmd" value="{{$cmd}}">
          <input type="hidden" name="lc" value="AE" />
          <input type="hidden" name="button_subtype" value="services" />
          <input type="hidden" name="no_note" value="0" />
          <input type="hidden" name="cn" value="Comments" />
          <input type="hidden" name="no_shipping" value="1" />
          <input type="hidden" name="rm" value="2" />
          <input type='hidden' name='notify_url' value='{{$return_url}}'>
          <input type="hidden" name="item_name" value="{{$item_name}}">
          <input type="hidden" name="item_number" value="{{$item_number}}">
          <input type="hidden" name="amount" value="{{$amount}}">
          <input type="hidden" name="currency_code" value="{{$currency_code}}">
          <input type='hidden' name='cancel_return' value='{{$cancel_return}}'>
          <input type='hidden' name='return' value='{{$return_url}}'>
   </form>

</div>

@stop

@section('js')

<script type="text/javascript">
  document.getElementById('load').style.visibility="visible";
  $(document).ready(function() {
      $("#paypal_form").submit();
  });

</script>
@stop