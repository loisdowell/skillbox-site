@extends('layouts.frontend')

@section('content')

<style type="text/css">
    .modal-new .modal-backdrop {z-index: 0;}
    footer{width: 100%;bottom: 0}
</style> 
   <div class="joblist_modals">
    <!-- Modal -->
    <div class="modal fade" id="sponsorNow" role="dialog">
        <div class="modal-dialog modal-dialog-popup" style="max-width:460px">
            <!-- Modal content-->
            <div class="modal-content modal-content-popup">
                <div class="modal-header modal-header-bottom">
                </div>
                <div class="modal-body">
                    <div class="popup-sponsore">
                        <div class="title-control-postion">
                            <div class="title-popup-table">
                                <h3>Are you sure you want to sponsor this job posting? </h3>
                            </div>
                            <input type="hidden" name="sjid" id="sjid">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel & Close</button> &nbsp;&nbsp;&nbsp;&nbsp;                                         
                                    <button type="button" class="btn btn-success ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="SponsorNowJob" onclick="SponsorNow()">Sponsor Now</button>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-top">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

     <!-- Modal -->
    <div class="modal fade" id="markUrgent" role="dialog">
        <div class="modal-dialog modal-dialog-popup" style="max-width:460px">
            <!-- Modal content-->
            <div class="modal-content modal-content-popup">
                <div class="modal-header modal-header-bottom">
                </div>
                <div class="modal-body">
                    <div class="popup-sponsore">
                        <div class="title-control-postion">
                            <div class="title-popup-table">
                                <h3>Are you sure you want to mark this job post as urgent?</h3>
                            </div>
                            <input type="hidden" name="sjid" id="sjid">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel & Close</button> &nbsp;&nbsp;&nbsp;&nbsp;                                         
                                    <button type="button" class="btn btn-success ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="MarkUrgentJob" onclick="markUrgent()">Mark as Urgent</button>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-top">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

    <!-- Modal -->
    <div class="modal fade" id="notify" role="dialog">
        <div class="modal-dialog modal-dialog-popup" style="max-width:460px">
            <!-- Modal content-->
            <div class="modal-content modal-content-popup">
                <div class="modal-header modal-header-bottom">
                </div>
                <div class="modal-body">
                    <div class="popup-sponsore">
                        <div class="title-control-postion">
                            <div class="title-popup-table">
                                <h3>Are you sure you want to send notification to sellers about this job posting?</h3>
                                <p style="    margin-top: -26px;margin-bottom: 28px !important">All sellers who've opted to receive notification in your category will be notified about this job post.</p>
                            </div>
                            <input type="hidden" name="sjid" id="sjidnotify">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel & Close</button> &nbsp;&nbsp;&nbsp;&nbsp;                                         
                                    <button type="button" class="btn btn-success ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="NotifyJob" onclick="Notify()">Notify Sellers</button>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-top">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->


    <!-- Modal -->
    <div class="modal fade" id="upgradenotify" role="dialog">
        <div class="modal-dialog modal-dialog-popup" style="max-width:460px">
            <div class="modal-content modal-content-popup">
                <div class="modal-header modal-header-bottom">
                </div>
                <div class="modal-body">
                    <div class="popup-sponsore">
                        <div class="title-control-postion">
                            <div class="title-popup-table">
                                <h3>Want to notify sellers about this job post?</h3>
                            </div>
                            <input type="hidden" name="sjid" id="sjid">

                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <h5>It's only <del>£5</del>  offer £3 for single job posting..</h5>
                                </div>
                            </div> 

                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <form class="horizontal-form" role="form" method="POST" action="{{url('job-post/notify-payment')}}" data-parsley-validate enctype="multipart/form-data" >
                                        <input type="hidden" name="i_job_id" id="i_job_id_notify" value="">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-success" id="NotifyJob" onclick=""> Notify Sellers</button>
                                    </form>
                                </div>
                            </div> 

                            <div style="clear: both"></div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <p>There is a better option, upgrade your account to our Premium Plan and notify sellers about all your job postings.</p>
                                </div>
                            </div>
                            <br/>
                            
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel & Close</button>
                                    <button type="button" class="btn btn-success" onclick="upgradePremiumPlan('notify')" >Upgrade plan</button>
                                </div>
                            </div> 


                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-top">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->


    <!-- Modal -->
    <div class="modal fade" id="upgradeurgent" role="dialog">
        <div class="modal-dialog modal-dialog-popup" style="max-width:460px">
            <!-- Modal content-->
            <div class="modal-content modal-content-popup">
                <div class="modal-header modal-header-bottom">
                </div>
                <div class="modal-body">
                    <div class="popup-sponsore">
                        <div class="title-control-postion">
                            <div class="title-popup-table">
                                <h3>Want to make this job posting urgent?</h3>
                            </div>
                            <input type="hidden" name="sjid" id="sjid">
                            
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <h5>It's only <del>£7</del>  offer £5 for a month job posting.</h5>
                                </div>
                            </div>        
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <form class="horizontal-form" role="form" method="POST" action="{{url('job-post/urgent-payment')}}" data-parsley-validate enctype="multipart/form-data" >
                                        <input type="hidden" name="i_job_id" id="i_job_id_urgent" value="">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-success" id="NotifyJob" onclick="">Mark as Urgent</button>
                                    </form>
                                </div>
                            </div> 
                            <div style="clear: both"></div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <p>There is a better option, upgrade your account to our Premium Plan and get mark as urgent all included and unlimited.</p>
                                </div>
                            </div>
                            <br/>
                            
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel & Close</button>
                                    <button type="button" class="btn btn-success" onclick="upgradePremiumPlan('urgent')" >Upgrade plan</button>
                                </div>
                            </div> 

                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-top">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

     @php
        $standardplanFree= App\Helpers\General::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
    @endphp  

    <div id="upgradePlanModal" class="modal1 upgradePlanModal_custom" role="dialog">
        <div class="modal-content2">
            <div class="final-containt">
                <span class="close2"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" alt=""  onclick="upgradePlanModalClose()" /></span>
            </div>
            <div class="container">
                <div class="main-dirservice-popup">
                    
                    <form class="horizontal-form" role="form" method="POST" name="planform" id="planform" action="{{url('job-post/plan-data-urget-notify')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}

                    <input type="hidden" name="i_job_id" id="i_job_id_premium_plan">
                    <input type="hidden" name="i_job_type" id="i_job_type">

                    <div class="popup-dirservice">
                        <div class="title-dirservice">
                            <h4> Upgrade Your Plan </h4>
                        </div>
                        <div class="foll-option-popup">
                            You're currently a {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }} member. Please upgrade your Plan.
                        </div>
                        
                        <div class="wrap">
                            <fieldset>
                                <div class="toggle">
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join" checked onchange="plandurationchange('monthly')" >
                                    <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create" onchange="plandurationchange('yearly')">
                                    <label for="create" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    @php
                       $selected="";

                    @endphp
                    @if(count($plandata))
                        @foreach($plandata as $key=>$val)
                        
                            @if($key==2)
                                <div class="final-leval-popup matchHeights">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-12">
                                            <div class="@if($key==0)left-choise @else left-side @endif matchheight_div">
                                                <div class="reg">
                                                    <bdo dir="ltr">
                                                        <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required >
                                                        <span class="select-seller-job"></span>
                                                        <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                    </bdo>
                                                </div>
                                                <div class="choise-que">{{$val['v_subtitle']}}</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-xs-12">
                                            
                                            <ul class="list-unstyled choise-avalible matchheight_div">
                                                <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k<3)
                                                            <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                            <?php $lcnt = $lcnt+1; ?>
                                                            @endif
                                                        @endforeach
                                                    @endif  
                                            </ul>

                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <div class="right-display matchheight_div">
                                                <ul class="list-unstyled choise-avalible">
                                                        <?php 
                                                            $lcnt=1;
                                                        ?>
                                                        @if(count($val['l_bullet']))
                                                            @foreach($val['l_bullet'] as $k=>$v) 
                                                                @if($k>=3)
                                                                <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                                @endif
                                                                <?php $lcnt = $lcnt+1; ?>
                                                            @endforeach
                                                        @endif  

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 same-height-1">
                                            <div class="@if($key==0) free-price @else final-price @endif matchheight_div">
                                                <div class="all-in-data">
                                                    <div  class="monthyprice">
                                                           @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_monthly_price']}}</del>
                                                                </div>
                                                                Only £{{$val['f_monthly_dis_price']}} p/m

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif

                                                           @endif  
                                                    </div>  
                                                    
                                                    <div class="yealryprice" style="display: none;">
                                                           @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_yealry_price']}}</del>
                                                                </div>
                                                                Only £{{$val['f_yealry_dis_price']}} p/a

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif

                                                           @endif  
                                                    </div>    

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif        

                    <button type="submit" class="btn btn-payment">
                        Proceed
                    </button>

                    </form>

                </div>
                <!-- End 26B-my-profile-with-sponsor-button-POP UP-in-person-or-online -->
            </div>
        </div>
    </div>
</div> 
    

    <div id="sponserUpgradePlanModal" class="modal1 upgradePlanModal_custom" role="dialog">
        <div class="modal-content2">
            <div class="final-containt">
                <span class="close2"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" alt=""  onclick="sponserUpgradePlanModalClose()" /></span>
            </div>
            <div class="container">
                <div class="main-dirservice-popup">
                    
                    <form class="horizontal-form" role="form" method="POST" name="userplanform" id="userplanform" action="{{url('job-post/sponser-plan-data')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    
                    <input type="hidden" name="i_job_id" id="i_job_id_sponser">
                    <div class="popup-dirservice">
                        <div class="title-dirservice">
                            <h4> Upgrade Your Plan </h4>
                        </div>
                        <div class="foll-option-popup">
                            You're currently a {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }} member. Please upgrade your Plan.
                        </div>
                        
                        <div class="wrap">
                            <fieldset>
                                <div class="toggle">
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="sponserplanmonth" checked onchange="plandurationchange('monthly')" >
                                    <label for="sponserplanmonth" class="toggle-label toggle-label-off" id="sponserjoinLabel">Monthly</label>
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="sponserplanyear" onchange="plandurationchange('yearly')">
                                    <label for="sponserplanyear" class="toggle-label toggle-label-on" id="sponsercreateLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    @if(count($plandata))
                        @foreach($plandata as $key=>$val)
                        
                            @if($key!=0)
                                <div class="final-leval-popup matchHeights">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-12">
                                            <div class="@if($key==0)left-choise @else left-side @endif matchheight_div">
                                                <div class="reg">
                                                    <bdo dir="ltr">
                                                        <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required >
                                                        <span class="select-seller-job"></span>
                                                        <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                    </bdo>
                                                </div>
                                                <div class="choise-que">{{$val['v_subtitle']}}</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-xs-12">
                                            
                                            <ul class="list-unstyled choise-avalible matchheight_div">
                                                <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k<3)
                                                            <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                            <?php $lcnt = $lcnt+1; ?>
                                                            @endif
                                                        @endforeach
                                                    @endif  
                                            </ul>

                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <div class="right-display matchheight_div">
                                                <ul class="list-unstyled choise-avalible">
                                                    <?php 
                                                            $lcnt=1;
                                                        ?>
                                                        @if(count($val['l_bullet']))
                                                            @foreach($val['l_bullet'] as $k=>$v) 
                                                                @if($k>=3)
                                                                <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                                @endif
                                                                <?php $lcnt = $lcnt+1; ?>
                                                            @endforeach
                                                        @endif  

                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 same-height-1">
                                            <div class="@if($key==0) free-price @else final-price @endif matchheight_div">
                                                <div class="all-in-data">
                                                    <div  class="monthyprice">
                                                           @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_monthly_price']}}</del>
                                                                </div>
                                                                Only £{{$val['f_monthly_dis_price']}} p/m

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif

                                                           @endif  
                                                    </div>  
                                                    
                                                    <div class="yealryprice" style="display: none;">
                                                           @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_yealry_price']}}</del>
                                                                </div>
                                                                Only £{{$val['f_yealry_dis_price']}} p/a

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif
                                                           @endif  
                                                    </div>    

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif        

                    <button type="submit" class="btn btn-payment">
                        Proceed
                    </button>
                    </form>
                </div>
                <!-- End 26B-my-profile-with-sponsor-button-POP UP-in-person-or-online -->
            </div>
        </div>
    </div>


    <!-- 26B-job-posting -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/dashboard')}}"><button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button></a>
                </div>
                <div class="title-support">
                    <h1> My @if($userservice=="online") Online @else In Person @endif Job Postings </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="content">
            
            @if ($success = Session::get('success'))
              <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 30px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
              </div>
            @endif
            @if ($warning = Session::get('warning'))
              <div class="alert alert-info alert-danger" style="margin-top: 30px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
              </div>
            @endif
                
            <div class="tab-pane active" id="completed">

                <div class="order-status">
                    <div class="alert alert-success alert-big alert-dismissable br-5" id="sponsorsuccmsg" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span>Thank you, your message is now sponsored.</span>
                    </div>
                    <div class="alert alert-info alert-danger" id="sponsorerrmsg" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span>Something went wrong.please try again after some time.</span>
                    </div>

                    <div id="commonmsg"></div>
                    <div class="title-sponsoreads">
                        Job Postings
                        <a href="{{url('buyer/post-job')}}">
                        <button type="button" class="btn btn-new-sponsore btn-postion"> Add New Job Posting </button>
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="order-details">
                                <div class="col-sm-4 no-padding">
                                    <div class="right-boderstyle">
                                        ACTIVE POSTINGS
                                        <p> {{$headerdata['ACTIVE_POSTINGS']}} </p>
                                    </div>
                                </div>
                                <div class="col-sm-4 no-padding">
                                    <div class="right-boderstyle">
                                        EXPIRED POSTINGS
                                        <p> {{$headerdata['EXPIRED_POSTINGS']}} </p>
                                    </div>
                                </div>
                                <div class="col-sm-4 no-padding">
                                    <div class="right-boderstyle last-chlidhead">
                                        SPONSORED ADS
                                        <p> {{$headerdata['SPONSORED_ADS']}} </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contant-table ">
                        <div class="table-responsive">
                        <table class="table table-hover table-jobposting">
                            <thead>
                                <tr>
                                    <th>JOB TITLE</th>
                                    <th>POSTING DATE</th>
                                    <th>EXPIRY DATE</th>
                                    <th>SPONSORED</th>
                                    <th>ACTION</th>
                                    <th>URGENT</th>
                                    <th>NOTIFY</th>
                                </tr>
                            </thead>

                            <tbody>
                            @php
                                $currentdate=date("Y-m-d");    
                            @endphp
                            @if(count($joblist))
                                @foreach($joblist as $key=>$val)

                                <tr>
                                    
                                    <td>{{isset($val->v_job_title) ? $val->v_job_title:''}} <a href="{{url('job-postings/applied-for-jobs')}}/{{$val->id}}" class="jobtitle-value"> ( {{$val->i_applied}} )</a> 
                                        <br>
                                        
                                        <?php /*
                                        @if(isset($val->e_status) && $val->e_status=="inactive")
                                            <button type="button" class="btn btn-jobpost">Inactive Job</button>
                                        @else
                                        */?>

                                        @if(strtotime($val->d_expiry_date)<=strtotime($currentdate))
                                            <button type="button" class="btn btn-jobpost">Job Ended</button>
                                        @else    
                                            @if(isset($val->e_status) && $val->e_status=="inactive")
                                                <button type="button" class="btn btn-pause">Inactive Job</button>
                                            @else
                                                <a href="{{url('job-postings/endjobpost')}}/{{$val->id}}">
                                                <button type="button" class="btn btn-pause">End Job Post</button>
                                                </a>
                                            @endif    
                                        @endif

                                    </td>
                                    
                                    <td>{{isset($val->d_added) ? date("d/m/Y",strtotime($val->d_added)):''}}</td>
                                    <td>{{isset($val->d_expiry_date) ? date("d/m/Y",strtotime($val->d_expiry_date)):''}}</td>
                                    <td>
                                    <div id="sponsor_{{$val->id}}">
                                        @if(isset($val->e_sponsor_status) && $val->e_sponsor_status=="start")
                                            <span class="positive-status">YES</span> 
                                        @else
                                            <span class="negative-status mr_0">NO</span><br>
                                            <button type="button" class="btn btn-pause" onclick="SponsorNowModal('{{$val->id}}')"> Sponsor Now </button>
                                        @endif
                                    </div>
                                    </td>
                                    
                                    <td>
                                        <div id="editjobbtn_{{$val->id}}">
                                        {{-- <span class="negative-status mr_0">&nbsp;</span><br> --}}
                                        
                                        @if(isset($val->e_status) && $val->e_status=="inactive")
                                        <button type="button" class="btn btn-jobpost" disabled>View Job</button>
                                        @else
                                        <a href="{{url('online-job')}}/{{$val->id}}/{{$val->v_job_title}}?preview=job" target="_blank">
                                        <button type="button" class="btn btn-jobpost">View Job</button>
                                        </a>
                                        @endif

                                        <a href="{{url('job-post/edit')}}/{{$val->id}}">
                                        <button type="button" class="btn btn-jobpost">Edit Job Post</button>
                                        </a>
                                        </div>
                                     </td>

                                    <td>
                                        <div id="urgent_{{$val->id}}">
                                        @if(isset($val->e_urgent) && $val->e_urgent=="yes")
                                            <span class="positive-status">YES</span> 
                                        @else
                                            <span class="negative-status mr_0">NO</span><br>
                                            <button type="button" class="btn btn-pause" onclick="MarkUrgentModal('{{$val->id}}')"> Mark Urgent </button>
                                        @endif
                                       
                                    </td>

                                    <td>
                                        <div id="notify_{{$val->id}}">
                                        @if(isset($val->e_notify) && $val->e_notify=="yes")
                                            <span class="positive-status">YES</span> 
                                        @else
                                             <span class="negative-status mr_0">NO</span>
                                                <button type="button" class="btn btn-pause" onclick="NotifyModal('{{$val->id}}')"> Notify </button>
                                            
                                        @endif
                                    </td>

                                </tr>

                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">No job postings found.</td>
                                </tr>    
                            @endif
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End 26B-job-posting -->

        </div>
    </div>
  
@stop
@section('js')

<script type="text/javascript">

      function buyerSignup(){
          var formValidFalg = $("#buyersignupform").parsley().validate('');
          if(formValidFalg){
              var l = Ladda.create(document.getElementById('bsignup'));
              l.start();
              $("#buyersignupform").submit()
          } 
      }

      function SponsorNowModal(jid=""){

            var actionurl = "{{url('job-post/check-sponsor-now')}}";
            var formdata = "";

            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#sjid").val(jid);
                        $("#sponsorNow").modal("show");
                    }else{
                        $("#i_job_id_sponser").val(jid);
                        $("#sponserUpgradePlanModal").modal("show");
                    }       

                },
                error: function ( jqXHR, exception ) {
                    $("#sponsorerrmsg").show();
                }

            });
        }

        function sponserUpgradePlanModalClose(){
            $("#sponserUpgradePlanModal").modal("hide");
        }

      function SponsorNow(){
            
            var actionurl = "{{url('job-post/sponsor-now')}}";
            var job_id = $("#sjid").val();    
            var formdata = "job_id="+job_id;

            var l = Ladda.create(document.getElementById('SponsorNowJob'));
            l.start();  

            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    l.stop();
                    
                    var obj = jQuery.parseJSON(res);
                    
                    $("#sponsorNow").modal("hide");
                    if(obj['status']==1){
                        
                        $("#sponsorsuccmsg").show();
                        $("#sponsorsucmsg").html(obj['msg']);
                        
                        $("#sponsor_"+job_id).html("");
                        $("#sponsor_"+job_id).html("<span class='positive-status'>YES</span>");


                    }else{
                        $("#sponsorerrmsg").show();
                        $("#sponsorerrmsg").html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    l.stop();
                    $("#sponsorerrmsg").show();
                }

            });

        }

    function MarkUrgentModal(jid=""){
        $("#sjid").val(jid);
        $("#markUrgent").modal("show");
    }

    
    function markUrgent(){
        var actionurl = "{{url('job-post/mark-urgent')}}";
        var job_id = $("#sjid").val();    
        var formdata = "job_id="+job_id;
        var l = Ladda.create(document.getElementById('MarkUrgentJob'));
        l.start();  

        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                
                l.stop();
                var obj = jQuery.parseJSON(res);
                $("#markUrgent").modal("hide");
                
                if(obj['status']==1){
                        
                    $('#commonmsg').html(obj['msg']);
                    $("#urgent_"+job_id).html("");
                    $("#urgent_"+job_id).html("<span class='positive-status'>YES</span>");

                }else if(obj['status']==2){
                    
                    $("#i_job_id_urgent").val(job_id);
                    $("#upgradeurgent").modal("show");

                }else{
                    $("#sponsorerrmsg").show();
                    $("#sponsorerrmsg").html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                l.stop();
                $("#sponsorerrmsg").show();
            }

        });

    }

    function upgradePremiumPlan(data=""){
        
        var job_id="";
        if(data=="notify"){
            job_id = $("#i_job_id_notify").val();
        }else{
            job_id = $("#i_job_id_urgent").val();
        }
        $("#i_job_id_premium_plan").val(job_id);
        $("#i_job_type").val(data);
        
        
        
        if(data=="notify"){
            $("#upgradenotify").modal("hide");
        }else{
            $("#upgradeurgent").modal("hide");
        }
        $("#upgradePlanModal").modal("show");
    }

    function upgradePlanModalClose(){
        $("#upgradePlanModal").modal("hide");
    }

    function NotifyModal(jid=""){
        $("#sjidnotify").val(jid);
        $("#notify").modal("show");
    }

    function Notify(){
            
        var actionurl = "{{url('job-post/notify')}}";
        var job_id = $("#sjidnotify").val();    
        var formdata = "job_id="+job_id;

        var l = Ladda.create(document.getElementById('NotifyJob'));
        l.start();  

        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                l.stop();
                
                var obj = jQuery.parseJSON(res);
                
                $("#notify").modal("hide");
                if(obj['status']==1){
                    
                    $('#commonmsg').html(obj['msg']);
                    
                    $("#notify_"+job_id).html("");
                    $("#notify_"+job_id).html("<span class='positive-status'>YES</span>");

                }else if(obj['status']==2){
                    $("#i_job_id_notify").val(job_id);
                    $("#upgradenotify").modal("show");
                }
                else{
                    $('#commonmsg').html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                l.stop();
                $("#sponsorerrmsg").show();
            }

        });

    }

    function plandurationchange(data=""){
            
            if(data=="monthly"){
                $(".monthyprice").css("display","block");
                $(".yealryprice").css("display","none");

            }else if(data=="yearly"){
                $(".yealryprice").css("display","block");
                $(".monthyprice").css("display","none");
            }
    }

    


</script>

@stop

