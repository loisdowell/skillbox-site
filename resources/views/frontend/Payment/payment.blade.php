@extends('layouts.frontend')
@section('content')
<style type="text/css">
   .modal-new .modal-backdrop {z-index: 0;}
   .form-save-exit{width: 250px !important;}
</style>

<div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="title-support">
                    <h1> Order Summery </h1>
                </div>
            </div>
        </div>
    </div>
<div class="container">
   <div class="main-edit-profile">
      <div class="edit-details" style="margin-top: 50px;">
         <div class="row">
            <form class="horizontal-form" role="form" method="POST" name="editprofileform" id="editprofileform" action="http://192.168.0.228/projects/skillbox/profile/update" data-parsley-validate="" enctype="multipart/form-data" novalidate="">
               <input type="hidden" name="_token" value="2qLjHCeuIK5y2xJFAbJVaMGnTX0LjpxZL1r36bNQ">    
               
               <div class="col-xs-12">
                  <div class="upload-details" style="margin: 0px auto;">
                     
                    
                    <div class="row" style="margin: 5px 0px;">
                        <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
                           <label class="field-edit-name" style="margin-top: 5px;">
                            <b>User Plan subscription </b>: {{isset($data['v_plan_name']) ? $data['v_plan_name'] : ''}}
                           </label>
                        </div>
                    </div>

                     @if(isset($data['addon']) && count($data['addon']))
                          @foreach($data['addon'] as $k=>$v)
                          <div class="row" style="margin: 2px 0px;">
                              <div class="col-sm-12 col-xs-12" style="margin: 5px 0px; text-align: center;">
                                 <label class="field-edit-name" style="margin-top: 5px;">
                                  <b>{{$k}}</b>: {{$v}}
                                 </label>
                              </div>
                          </div>
                          @endforeach
                     @endif 

                    <div class="row" style="margin: 5px 0px;">
                        <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
                           <label class="field-edit-name" style="margin-top: 5px;">
                            <b>Total Amount Payable </b>: £{{isset($data['v_total_amount']) ? $data['v_total_amount'] : ''}}
                           </label>
                        </div>
                    </div>
                    
                    <div class="row" style="margin: 5px 0px;">
                        <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
                           <label class="field-edit-name" style="margin-top: 5px;">
                            <b>Plan Duration</b>: {{isset($data['v_plan_duration']) ? ucfirst($data['v_plan_duration']) : ''}}
                           </label>
                        </div>
                    </div>

                    <div style="margin: 15px 0px; text-align: center;">
                         
                         <a href="{{url('payment/mangopay')}}">
                         <button type="button" class="btn form-save-exit"> Proceed to payment </button>
                        </a>

                         {{--
                         <a href="{{url('payment/paypal')}}">
                         <button type="button" class="btn form-save-exit"> Pay with PayPal </button>
                         </a>
                         <button type="button" class="btn form-next">Pay with Escrow</button>
                         --}}
                         
                    </div>

                  </div>
               </div>
            </form>
         </div>
      </div>
      
   </div>
</div>
@stop
@section('js')
<script type="text/javascript">

</script>
@stop