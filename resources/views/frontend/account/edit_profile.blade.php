@extends('layouts.frontend')

@section('content')
<style type="text/css">
    .dimg{
        max-width: 250px;
        max-height: 250px;
        margin: auto;
        overflow: hidden;
        margin-bottom: 30px;
    }
    .img-upload{
        padding-bottom: 0px; 
    }
</style>

 <!-- 26B-control-panel-profile-settings-2-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">

                <div class="left-supportbtn">
                    <a href="{{url('dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                
                <div class="title-support">
                    <h1> Profile Setting </h1>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main-edit-profile">
            
            @if(!isset($userdata->v_company_type) && !isset($userdata->v_company_name)) 
            <center>
            <h4 style="margin-bottom: 10px">Before you proceed to your seller panel, please fill in your required profile details.</h4>
            </center>
            @endif

            <div class="edit-details top-space">
            
                <div class="row">
                    

                    @if ($success = Session::get('success'))
                      <div class="alert alert-success alert-big alert-dismissable br-5">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
                      </div>
                    @endif
                    @if ($warning = Session::get('warning'))
                      <div class="alert alert-info alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
                      </div>
                    @endif

                    <form class="horizontal-form" role="form" method="POST" name="editprofileform" id="editprofileform" action="{{url('profile/update')}}" data-parsley-validate enctype="multipart/form-data" >

                    {{ csrf_field() }}    
                    <div class="col-sm-4 col-xs-12">
                        <div class="upload-edit-detail">
                             
                            @if(isset($userdata->v_image) && $userdata->v_image!="")
                                @php
                                    $imgurl = \Storage::cloud()->url($userdata->v_image);    
                                @endphp 
                                <div class="dimg"> 
                                <img src="{{$imgurl}}" alt="Your Images" class="img-responsive img-upload" id="edit-images">
                                </div>
                            @else
                                @php
                                    $imgurl = \Storage::cloud()->url('users/img-upload.png');    
                                @endphp 
                                <img src="{{$imgurl}}" alt="Your Images" class="img-responsive img-upload" id="edit-images">
                            @endif
                            <input type='file' accept="image/x-png,image/gif,image/jpeg" name="v_image" id="fileupload-example-4" onchange="readURL(this);" />
                            <label id="fileupload-example-4-label" for="fileupload-example-4">Upload Image</label>
                            <center>
                            <span style="margin-top: 10px;display: inline-block;">H : 290px , W : 250px <br></span>
                            </center>
                            <span class="videoerrsize" style="color: #ccc;text-align:center;margin-top: 10px">File size should not be more than 2 MB<br></span>
                             @if(isset($leveldata) && count($leveldata))
                             @php
                                $levelcon="";
                                if(isset($leveldata->v_icon) && $leveldata->v_icon!=""){
                                    $levelcon = \Storage::cloud()->url($leveldata->v_icon);
                                }
                             @endphp
                             @if($levelcon!="")
                             <div class="dashboardbadge">
                                <img src="{{$levelcon}}" alt="" />
                             </div>
                             @endif
                             @endif
                        </div>
                        <div class="upload-msg">
                            @if(isset($leveldata) && count($leveldata))
                              <div class="msg-heading"> SKILLBOX Skill Level:</div>
                              <button type="button" class="btn btn-level" onclick="openleveldata()">{{isset($leveldata->v_title) ? $leveldata->v_title : ''}} </button>


                              


                            <div id="leveltextview" class="modal fade" role="dialog">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title" style="text-align: center;">{{isset($leveldata->v_title) ? $leveldata->v_title : ''}}</h4>
                                  </div>
                                  <div class="modal-body">
                                    <p>{{isset($leveldata->v_text_seller) ? $leveldata->v_text_seller : ''}}</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>

                                                
                            <script type="text/javascript">
                                function openleveldata(){
                                    $("#leveltextview").modal("show");
                                }
                            </script>


                            @endif
                        </div>
                    </div>


                    <div class="col-sm-8 col-xs-12">
                        <div class="upload-details bottom-space">
                            <div class="row">
                                
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        First Name:
                                    </label>
                                    <input type="text" name="v_fname" class="form-control input-field user-name" value="{{isset($userdata->v_fname) ? $userdata->v_fname:''}}" required>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Last Name:
                                    </label>
                                    <input type="text" name="v_lname" class="form-control input-field user-name" value="{{isset($userdata->v_lname) ? $userdata->v_lname:''}}" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Email:
                                    </label>
                                    <input type="email" name="v_email" class="form-control form-mail" value="{{isset($userdata->v_email) ? $userdata->v_email:''}}" required>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Phone No:
                                    </label>
                                    <input type="text" name="v_contact" class="form-control input-field contact-phone" value="{{isset($userdata->v_contact) ? $userdata->v_contact:''}}" required data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$">
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Country :
                                    </label>
                                    <select class="form-control form-sell" name="i_country_id" required readonly>
                                       {{-- <option value="">Select</option> --}}
                                       @if(count($countrylist))
                                            @foreach($countrylist as $k=>$v)
                                            @if($v->id=="5af2949d76fbae1ce44d4e1b")
                                                <option value="{{$v->id}}" @if(isset($userdata->i_country_id) && $userdata->i_country_id==$v->id) selected @endif >{{$v->v_title}}</option>
                                            @endif    
                                            @endforeach
                                       @endif
                                    </select>
                                </div>

                                 <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        City :
                                    </label>
                                    <input type="text" class="form-control input-field map-location" name="v_city" value="{{isset($userdata->v_city) ? $userdata->v_city:''}}" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Address :
                                    </label>
                                    <input type="text" name="l_address" class="form-control input-field map-location" value="{{isset($userdata->l_address) ? $userdata->l_address:''}}" required >
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Postcode :
                                    </label>
                                    <input type="text" name="v_postcode" class="form-control input-field" value="{{isset($userdata->v_postcode) ? $userdata->v_postcode:''}}" required >
                                </div>
                            </div>
                            {{-- <input id="date" data-inputmask="'alias': 'date'" /> --}}

                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Birthday :
                                    </label>
                                    {{-- <div class="input-group"> --}}
                                    <input type="text" data-inputmask="'alias': 'date'"  name="d_birthday" required id="d_birthday" class="form-control input-field form_datetime21" value="{{isset($userdata->d_birthday) ? date("d/m/Y",strtotime($userdata->d_birthday)):''}}">
                                    {{-- <label class="input-group-addon btn" for="d_birthday" style="border-top-right-radius: 20px;border-bottom-right-radius: 20px;">
                                       <span style="color: #e8128c" class="fa fa-calendar open-datetimepicker"></span>
                                    </label>
                                    </div> --}}
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Gender:
                                    </label>
                                    <select class="form-control form-sell" name="v_gender" >
                                        <option value="">-Select-</option>
                                        <option value="male" @if(isset($userdata->v_gender) && $userdata->v_gender=="male") selected @endif >Male</option>
                                        <option value="female" @if(isset($userdata->v_gender) && $userdata->v_gender=="female") selected @endif >Female</option>
                                    </select>
                                </div>
                            </div>

                            <?php
                                $compnaytype=array(
                                    'individual'=>'Individual',
                                    'sole proprietor'=>'Sole proprietor',
                                    'Public limited company'=>'Public limited company',
                                    'Private company limited'=>'Private company limited',
                                    'Limited liability partnership'=>'Limited liability partnership',
                                    'Community interest company'=>'Community interest company',
                                );
                                $daydata=array(
                                    'monday'=>'Monday',
                                    'tuesday'=>'Tuesday',
                                    'wednesday'=>'Wednesday',
                                    'thursday'=>'Thursday',
                                    'friday'=>'Friday',
                                    'saturday'=>'Saturday',
                                    'sunday'=>'Sunday',
                                );

                                $compnaytype=array(
                                    'SOLETRADER'=>'Soletrader',
                                    'BUSINESS'=>'Business',
                                    'ORGANIZATION'=>'Organization',
                                );
                            ?>
                            @if(isset($userdata->v_company_type) && $userdata->v_company_type!="INDIVIDUAL")
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Company type :
                                    </label>
                                    
                                    <select class="form-control form-sell" name="v_company_type" required>
                                        <option value="">-Select-</option>
                                        @if(count($compnaytype))
                                            @foreach($compnaytype as $k=>$v)
                                                <option value="{{$k}}" @if(isset($userdata->v_company_type) && $userdata->v_company_type==$k) selected @endif >{{$v}}</option>
                                            @endforeach
                                       @endif    
                                    </select>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Company Name :
                                    </label>
                                    <input type="text" name="v_company_name" class="form-control input-field" value="{{isset($userdata->v_company_name) ? $userdata->v_company_name:''}}" required>
                                </div>
                               

                            </div>

                            
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        VAT number :
                                    </label>
                                    <input type="text" name="v_vat_number" class="form-control input-field" value="{{isset($userdata->v_vat_number) ? $userdata->v_vat_number:''}}" >
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Company registration number :
                                    </label>
                                    <input type="text" name="v_company_regnumber" class="form-control input-field" value="{{isset($userdata->v_company_regnumber) ? $userdata->v_company_regnumber:''}}" >
                                </div>
                            </div>
                            @endif



                        </div>
                    </div>
                </div>
            </div>
            
            <div class="top-btnspace">
                {{--
                <a href="{{url('dashboard')}}">
                <button type="button" class="btn form-save-exit"> Discard </button>
                </a>
                --}}

                <button type="submit" class="btn form-next">Save Changes</button>
            </div>



        </div>
    </div>
    <!-- end 26B-control-panel-profile-settings-2 -->
@stop

@section('js')

    <script src="{{url('public/js/jquery.mask.js')}}" data-autoinit="true"></script>
    


     <script type="text/javascript">
        
         $(document).ready(function(){
            $('#d_birthday').mask("00/00/0000", {placeholder: "DD/MM/YYYY"});
         });       

        $(document).ready(function(){
            // $(".form_datetime2").datetimepicker({
            //     pickTime: false,
            //     minView: 2,
            //     format: 'yyyy-mm-dd',
            //     endDate: '+0d',
            //     autoclose: true,
            // });
        });

    </script>

     <script type="text/javascript">
        function readURL(input) {

            if (input.files && input.files[0]) {
                
                if(input.files[0].size > 2097152) {
                    $('#fileupload-example-4').parent("div").find(".videoerrsize").css("display","block");
                    return false;
                }
                $('#fileupload-example-4').parent("div").find(".videoerrsize").css("display","none");

                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#edit-images')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }

        }
    </script>
@stop

