@extends('layouts.frontend')

@section('content')

    <style type="text/css">
    .imgrem {position: relative;}
    .close-img{height: 15px; position: absolute;right: 15px;top: -30px;}
</style>
<style type="text/css">
  #introducyvideoid {
      height: 0;
      width: 0;
  }
.addimg_brdr .dropzone .dz-preview {
    margin: 0px 0px !important;
}
.addimg_brdr .dropzone .dz-preview, .dropzone-previews .dz-preview, .img-biography .dropzone-previews .dz-preview {
    border: 1px solid #8470ff !important;
}
.dropzone{ min-height: 222px; }
.addimg_brdr .dropzone.dz-started .dz-message, .addimg_brdr .dropzone.dz-clickable .dz-message {
    position: absolute;
    z-index: -11;
    left: 0;
    right: 0;
    text-align: center;
    top: 50%;
    transform: translate(10%, -50%);
    opacity: 1;
}
</style>

<style type="text/css">
    .input_close{position: relative;}
    .input_close a{position: absolute;top: 6px;right: 10px;}
    .input_close .input-field{padding-right: 36px;}
</style>
<link rel="stylesheet" href="{{url('public/Assets/plugins')}}/dropzone/css/dropzone.css">
 
 <!-- 26B-control-panel-edit-billing-details-2-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1>Identification Documents</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">


            @if ($success = Session::get('success'))
              <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 20px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
              </div>
            @endif
            
            @if ($warning = Session::get('warning'))
              <div class="alert alert-info alert-danger" style="margin-top: 20px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
              </div>
            @endif

                    
            <div class="col-xs-12">
                <div class="main-support1">
                    
                <form class="horizontal-form" role="form" method="POST" name="editbillingform" id="editbillingform" action="{{url('profile/identification-documents')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}    
                    
                    <center>
                    @if(isset($userdata->i_identity_kyc_id) && $userdata->i_identity_kyc_id!="")
                      
                        @if($mangopayuser->KYCLevel=="REGULAR") 
                            <h3 style="margin-bottom: 20px"><strong>KYC Status </strong> : <span style="color: #228B22;">Approved</span></h3>
                        @else
                            <h3 style="margin-bottom: 20px"><strong>KYC Status </strong> : <span style="color: #e8128c;">Pending</span></h3>   
                        @endif
                    @else
                    <h3>No documents have been attached as yet.</h3>   
                    @endif
                    @if(isset($userdata->v_company_type) && $userdata->v_company_type!="")

                        @if($userdata->v_company_type=="SOLETRADER")
                            <h3 style="margin-bottom: 20px"><strong>Your Account Type </strong> : <span style="color: #e8128c;">Sole trader</span></h3>
                        @else
                        <h3 style="margin-bottom: 20px"><strong>Your Account Type </strong> : <span style="color: #e8128c;">{{ucfirst(
                            strtolower($userdata->v_company_type))}}</span></h3>
                        @endif
                            
                    @endif
                   </center>

                    <p>Know Your Customer (KYC) is a legal program upheld by the UK government, as part of The Money Laundering Regulations 2017. Its existence is to ensure a person is who they say they are and does what they say they are doing.</p>

                    <p>The KYC screening process is handled by our payment gateway provider MangoPay. Our objective is to provide Skillbox users with the safest and most secure environment and to ensure Skillbox is being used in the way it is intended.</p>

                    <p>To continue growing our trustworthy and reliable community we ask for sellers and buyers to verify their identity, by presenting official ID before funds can be withdrawn. This is part of an ongoing effort to improve the quality and trust that Skillbox maintains within our marketplace.</p>

                    <p>Verifying your account may take up to 7 days, this is a one time only process. Please attach the relevant documents requested.</p>

                    <p><strong>Note:</strong> Best practice is to scan or screenshot your documentation and simply upload to Identification documents.</p>

                    <h4 style="margin-top: 20px;"><strong>Required documents</strong></h4>   
                    <p>For id proof please provide front and back copy of your identification document.</p>
                    <ul class="list-unstyled">

                    <li><strong>Individual</strong></li>
                        
                        <ul style="margin-top: 10px">
                              <li><strong>Id proof:</strong> Passport, ID card</li>
                        </ul>

                    @if(isset($userdata->v_company_type) && $userdata->v_company_type!="INDIVIDUAL")    
                    <li style="margin-top: 10px"><strong>Sole trader</strong></li>
                        <ul style="margin-top: 10px">
                              <li><strong>Id proof:</strong> Passport, ID card</li>
                              <li><strong>Proof of registration:</strong> Document that proves the registration of the company </li>
                        </ul>

                    <li style="margin-top: 10px"><strong>Businesses</strong></li>
                    <ul style="margin-top: 10px">
                      <li><strong>Id proof:</strong> Passport, ID card</li>
                      <li><strong>Proof of registration of the company:</strong> Document that proves the registration of the company </li>
                      <li><strong>Article of association:</strong> Status Company</li>
                      <li><strong>Up to date shareholder declaration</strong> </li>
                    </ul>

                    <li style="margin-top: 10px"><strong>Organization</strong></li>
                        <ul style="margin-top: 10px">
                          <li><strong>Id proof:</strong> Passport, ID card</li>
                          <li><strong>Proof of registration of the company:</strong> Document that proves the registration of the company </li>
                          <li><strong>Article of association:</strong> The statutes of the association initialed dated and signed</li>
                        </ul>       
                    </ul>
                    @endif

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="open-browser">
                                    <label class="field-name" id="validatemessagephotolbl">
                                        @if(isset($kycidenty) && count($kycidenty))
                                            @if(isset($kycidenty->Status) && $kycidenty->Status=="VALIDATION_ASKED")
                                            <span style="color: #e8128c;">KYC Status : Pending </span>
                                            @elseif(isset($kycidenty->Status) && $kycidenty->Status=="VALIDATED")
                                            <span style="color: #228B22;">KYC Status : Approved </span> 
                                            @else
                                            <span style="color: #FF0000;">KYC Status : Refused </span><br><strong>Reason Message </strong>: {{$kycidenty->RefusedReasonMessage}}
                                            @endif
                                        @endif
                                        <br>
                                        Upload Identity proof<br>
                                        <span style="margin-top:10px;display: inline-block;font-size: 14px;">Max file size 6 MB  | File format should be jpeg, jpg, png</span><br>
                                    </label>
                                    <div class="addimg_brdr">
                                        <div class="form-group">
                                            <div class="cl-mcont" style="">
                                                <div action="{{url('profile/ajax/identity')}}" class="" id="identity" multiple="true" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="validatemessageidentity" style="display: none"><ul class="parsley-errors-list filled"><li class="parsley-required">Please upload identity proof.</li></ul></div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $( document ).ready(function() {
                                
                                jQuery("#identity").addClass("dropzone");
                                mydropzonework = new Dropzone("#identity", {
                                    multiple : true,
                                    maxFiles:100,
                                    maxFilesize:6,
                                    parallelUploads: 1,
                                    acceptedFiles: ".jpeg,.jpg,.png",
                                    }).on("complete", function(file) {
                                        $("#identity").html(file.xhr.response);    
                                    }).on("removedfile", function(file) {
                                    });
                            });

                            function remove_identity(name,id) {
                                    
                                document.getElementById('load').style.visibility="visible"; 
                                var formData = "name="+name+"&id="+id;
                                var actionurl="{{url('profile/ajax/remove/identity')}}";
                                $.ajax({
                                    processData: false,
                                    contentType: false,
                                    type  : "GET",
                                      url     : actionurl,
                                      data    : formData,
                                      success : function( res ){
                                        document.getElementById('load').style.visibility='hidden';
                                        $("#identity"+id).remove();
                                      },
                                      error: function ( jqXHR, exception ) {
                                      }
                                });
                            }

                        </script>
                        @if(isset($userdata->v_company_type) && $userdata->v_company_type!="INDIVIDUAL")
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="open-browser">
                                    <label class="field-name" id="validatemessagephotolbl">
                                         @if(isset($registration_kyc) && count($registration_kyc))
                                            @if(isset($registration_kyc->Status) && $registration_kyc->Status=="VALIDATION_ASKED")
                                            <span style="color: #e8128c;">KYC Status : Pending </span>
                                            @elseif(isset($registration_kyc->Status) && $registration_kyc->Status=="VALIDATED")
                                            <span style="color: #228B22;">KYC Status : Approved </span>
                                            @else
                                            <span style="color: #FF0000;">KYC Status : Refused </span> <br><strong>Reason Message </strong>: {{$registration_kyc->RefusedReasonMessage}}
                                            @endif
                                        @endif
                                        <br>  
                                        Upload Registration proof<br>
                                        <span style="margin-top:10px;display: inline-block;font-size: 14px;">Max file size 6 MB  | File format should be jpeg, jpg, png</span><br>
                                    </label>
                                    <div class="addimg_brdr">
                                        <div class="form-group">
                                            <div class="cl-mcont" style="">
                                                <div action="{{url('profile/ajax/registration')}}" class="" id="registration" multiple="true" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="validatemessageregistration" style="display: none"><ul class="parsley-errors-list filled"><li class="parsley-required">Please upload registration proof.</li></ul></div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $( document ).ready(function() {
                                
                                jQuery("#registration").addClass("dropzone");
                                mydropzonework = new Dropzone("#registration", {
                                    multiple : true,
                                    maxFiles:100,
                                    maxFilesize:6,
                                    parallelUploads: 1,
                                    acceptedFiles: ".jpeg,.jpg,.png",
                                    }).on("complete", function(file) {
                                        $("#registration").html(file.xhr.response);    
                                    }).on("removedfile", function(file) {
                                    });
                            });

                            function remove_registration(name,id) {
                                    
                                document.getElementById('load').style.visibility="visible"; 
                                var formData = "name="+name+"&id="+id;
                                var actionurl="{{url('profile/ajax/remove/registration')}}";
                                $.ajax({
                                    processData: false,
                                    contentType: false,
                                    type  : "GET",
                                      url     : actionurl,
                                      data    : formData,
                                      success : function( res ){
                                        document.getElementById('load').style.visibility='hidden';
                                        $("#registration"+id).remove();
                                      },
                                      error: function ( jqXHR, exception ) {
                                      }
                                });
                            }
                        </script>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="open-browser">
                                    <label class="field-name" id="validatemessagephotolbl">
                                         @if(isset($articles_kyc) && count($articles_kyc))
                                            @if(isset($articles_kyc->Status) && $articles_kyc->Status=="VALIDATION_ASKED")
                                            <span style="color: #e8128c;">KYC Status : Pending </span>
                                            @elseif(isset($articles_kyc->Status) && $articles_kyc->Status=="VALIDATED")
                                            <span style="color: #228B22;">KYC Status : Approved </span>
                                            @else
                                            <span style="color: #FF0000;">KYC Status : Refused </span><br><strong>Reason Message </strong>: {{$articles_kyc->RefusedReasonMessage}}
                                            @endif
                                        @endif
                                        <br> 
                                        Upload Articles of association<br>
                                        <span style="margin-top:10px;display: inline-block;font-size: 14px;">Max file size 6 MB  | File format should be jpeg, jpg, png</span><br>
                                    </label>
                                    <div class="addimg_brdr">
                                        <div class="form-group">
                                            <div class="cl-mcont" style="">
                                                <div action="{{url('profile/ajax/articles')}}" class="" id="articles" multiple="true" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="validatemessagearticles" style="display: none"><ul class="parsley-errors-list filled"><li class="parsley-required">Please upload articles of association.</li></ul></div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $( document ).ready(function() {
                                
                                jQuery("#articles").addClass("dropzone");
                                mydropzonework = new Dropzone("#articles", {
                                    multiple : true,
                                    maxFiles:100,
                                    maxFilesize:6,
                                    parallelUploads: 1,
                                    acceptedFiles: ".jpeg,.jpg,.png",
                                    }).on("complete", function(file) {
                                        $("#articles").html(file.xhr.response);    
                                    }).on("removedfile", function(file) {
                                    });
                            });

                            function remove_articles(name,id) {
                                    
                                document.getElementById('load').style.visibility="visible"; 
                                var formData = "name="+name+"&id="+id;
                                var actionurl="{{url('profile/ajax/remove/articles')}}";
                                $.ajax({
                                    processData: false,
                                    contentType: false,
                                    type  : "GET",
                                      url     : actionurl,
                                      data    : formData,
                                      success : function( res ){
                                        document.getElementById('load').style.visibility='hidden';
                                        $("#articles"+id).remove();
                                      },
                                      error: function ( jqXHR, exception ) {
                                      }
                                });
                            }
                        </script>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="open-browser">
                                    <label class="field-name" id="validatemessagephotolbl">

                                        @if(isset($shareholder_kyc) && count($shareholder_kyc))
                                            @if(isset($shareholder_kyc->Status) && $shareholder_kyc->Status=="VALIDATION_ASKED")
                                            <span style="color: #e8128c;">KYC Status : Pending </span>
                                            @elseif(isset($shareholder_kyc->Status) && $shareholder_kyc->Status=="VALIDATED")
                                            <span style="color: #228B22;">KYC Status : Approved </span>
                                            @else
                                            <span style="color: #FF0000;">KYC Status : Refused </span><br><strong>Reason Message </strong>: {{$shareholder_kyc->RefusedReasonMessage}}
                                            @endif
                                        @endif
                                        <br>

                                        Upload Shareholder declaration<br>
                                        <span style="margin-top:10px;display: inline-block;font-size: 14px;">Max file size 6 MB  | File format should be jpeg, jpg, png</span><br>
                                    </label>
                                    <div class="addimg_brdr">
                                        <div class="form-group">
                                            <div class="cl-mcont" style="">
                                                <div action="{{url('profile/ajax/shareholder')}}" class="" id="shareholder" multiple="true" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="validatemessageshareholder" style="display: none"><ul class="parsley-errors-list filled"><li class="parsley-required">Please upload shareholder declaration.</li></ul></div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $( document ).ready(function() {
                                
                                jQuery("#shareholder").addClass("dropzone");
                                mydropzonework = new Dropzone("#shareholder", {
                                    multiple : true,
                                    maxFiles:100,
                                    maxFilesize:6,
                                    parallelUploads: 1,
                                    acceptedFiles: ".jpeg,.jpg,.png",
                                    }).on("complete", function(file) {
                                        $("#shareholder").html(file.xhr.response);    
                                    }).on("removedfile", function(file) {
                                    });
                            });

                            function remove_shareholder(name,id) {
                                    
                                document.getElementById('load').style.visibility="visible"; 
                                var formData = "name="+name+"&id="+id;
                                var actionurl="{{url('profile/ajax/remove/shareholder')}}";
                                $.ajax({
                                    processData: false,
                                    contentType: false,
                                    type  : "GET",
                                      url     : actionurl,
                                      data    : formData,
                                      success : function( res ){
                                        document.getElementById('load').style.visibility='hidden';
                                        $("#shareholder"+id).remove();
                                      },
                                      error: function ( jqXHR, exception ) {
                                      }
                                });
                            }
                        </script>
                        
                        @endif


                    
                    <?php /*    
                    <div class="billing-detail">
                        <div class="row">
                            
                        </div>
                        
                        <div id="bank_detail_div" >
                           
                            <div class="row-field-support">
                                <div class="row">
                                    

                                    <div class="col-sm-3 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method"><center>
                                            @if(isset($kycidenty) && count($kycidenty))
                                                @if(isset($kycidenty->Status) && $kycidenty->Status=="VALIDATION_ASKED")
                                                <span style="color: #e8128c;">KYC Status : Pending </span>
                                                @elseif(isset($kycidenty->Status) && $kycidenty->Status=="VALIDATED")
                                                <span style="color: #228B22;">KYC Status : Approved </span>
                                                @else
                                                <span style="color: #FF0000;">KYC Status : Refused </span>
                                                @endif
                                            @endif

                                            <br>Identity proof</center></div>
                                            <img src="{{url('public/Assets/frontend/images/ddoc.jpg')}}" style="padding-bottom: 0px;height: 150px;width: 180px" id="v_identity_img" alt="Your Images" class="img-responsive img-upload-course" >
                                            <input type='file' id="v_identity_img_input" name="v_identity_img" onchange="readURLThumb(this,'v_identity_img');" required accept="image/png, image/jpeg, image/jpg" />
                                            <label id="fileupload-course-label" for="v_identity_img_input">Upload Image
                                            </label>
                                            <center>
                                            <span style="margin-top: 15px;display: block;">Max file size 6 MB<br></span>
                                            <span>File format jpeg, jpg, png<br></span>
                                            <span style="color: red" class="videoerrsize">Max file size 6 MB<br></span>
                                            <span style="margin-top: 10px;color: red" class="videoerrtype">File type shoud be jpg, jpeg, png</span>
                                            </center>
                                        </div>  
                                    </div>

                                    <div class="col-sm-3 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method"><center>
                                            @if(isset($registration_kyc) && count($registration_kyc))
                                                @if(isset($registration_kyc->Status) && $registration_kyc->Status=="VALIDATION_ASKED")
                                                <span style="color: #e8128c;">KYC Status : Pending </span>
                                                @elseif(isset($registration_kyc->Status) && $registration_kyc->Status=="VALIDATED")
                                                <span style="color: #228B22;">KYC Status : Approved </span>
                                                @else
                                                <span style="color: #FF0000;">KYC Status : Refused </span>
                                                @endif
                                            @endif
                                            <br>  
                                                

                                            Registration proof</center></div>
                                            <img src="{{url('public/Assets/frontend/images/ddoc.jpg')}}" style="padding-bottom: 0px;height: 150px;width: 180px" id="v_registration_img" alt="Your Images" class="img-responsive img-upload-course" >
                                            <input type='file' id="v_registration_img_input" name="v_registration_img" onchange="readURLThumb(this,'v_registration_img');" @if(isset($userdata->v_company_type) && $userdata->v_company_type!="INDIVIDUAL") required @endif accept="image/png, image/jpeg, image/jpg" />
                                            <label id="fileupload-course-label" for="v_registration_img_input">Upload Image
                                            </label>
                                            <center>
                                            <span style="margin-top: 15px;display: block;">Max file size 6 MB<br></span>
                                            <span>File format jpeg, jpg, png<br></span>
                                            <span style="color: red" class="videoerrsize">Max file size 6 MB<br></span>
                                            <span style="margin-top: 10px;color: red" class="videoerrtype">File type shoud be jpg, jpeg, png</span>
                                            </center>
                                        </div>  
                                    </div>


                                    
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method"><center>
                                            @if(isset($articles_kyc) && count($articles_kyc))
                                                @if(isset($articles_kyc->Status) && $articles_kyc->Status=="VALIDATION_ASKED")
                                                <span style="color: #e8128c;">KYC Status : Pending </span>
                                                @elseif(isset($articles_kyc->Status) && $articles_kyc->Status=="VALIDATED")
                                                <span style="color: #228B22;">KYC Status : Approved </span>
                                                @else
                                                <span style="color: #FF0000;">KYC Status : Refused </span>
                                                @endif
                                            @endif
                                            <br>      
                                            Articles of association</center></div>
                                            <img src="{{url('public/Assets/frontend/images/ddoc.jpg')}}" style="padding-bottom: 0px;height: 150px;width: 180px" id="v_articles_img" alt="Your Images" class="img-responsive img-upload-course" >
                                            <input type='file' id="v_articles_img_input" name="v_articles_img" onchange="readURLThumb(this,'v_articles_img');" accept="image/png, image/jpeg, image/jpg" @if(isset($userdata->v_company_type) && $userdata->v_company_type=="BUSINESS" || $userdata->v_company_type=="ORGANIZATION") required @endif />
                                            <label id="fileupload-course-label" for="v_articles_img_input">Upload Image
                                            </label>
                                            
                                            <center>
                                            <span style="margin-top: 15px;display: block;">Max file size 6 MB<br></span>
                                            <span>File format jpeg, jpg, png<br></span>
                                            <span style="color: red" class="videoerrsize">Max file size 6 MB<br></span>
                                            <span style="margin-top: 10px;color: red" class="videoerrtype">File type shoud be jpg, jpeg, png</span>
                                            </center>

                                        </div>  
                                    </div>
                                    

                                    <div class="col-sm-3 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method"><center>
                                            @if(isset($shareholder_kyc) && count($shareholder_kyc))
                                                @if(isset($shareholder_kyc->Status) && $shareholder_kyc->Status=="VALIDATION_ASKED")
                                                <span style="color: #e8128c;">KYC Status : Pending </span>
                                                @elseif(isset($shareholder_kyc->Status) && $shareholder_kyc->Status=="VALIDATED")
                                                <span style="color: #228B22;">KYC Status : Approved </span>
                                                @else
                                                <span style="color: #FF0000;">KYC Status : Refused </span>
                                                @endif
                                            @endif
                                            <br>
                                                
                                            Shareholder declaration</center></div>
                                            <img src="{{url('public/Assets/frontend/images/ddoc.jpg')}}" style="padding-bottom: 0px;height: 150px;width: 180px" id="v_shareholder_img" alt="Your Images" class="img-responsive img-upload-course" >
                                            <input type='file' id="v_shareholder_img_input" name="v_shareholder_img" onchange="readURLThumb(this,'v_shareholder_img');"  accept="image/png, image/jpeg, image/jpg" @if(isset($userdata->v_company_type) && $userdata->v_company_type=="BUSINESS") required @endif />
                                            <label id="fileupload-course-label" for="v_shareholder_img_input">Upload Image
                                            </label>
                                            <center>
                                            <span style="margin-top: 15px;display: block;">Max file size 6 MB<br></span>
                                            <span>File format jpeg, jpg, png<br></span>
                                            <span style="color: red" class="videoerrsize">Max file size 6 MB<br></span>
                                            <span style="margin-top: 10px;color: red" class="videoerrtype">File type shoud be jpg, jpeg, png</span>
                                            </center>

                                        </div>  
                                    </div>
                                    


                                    {{-- @if(!isset($mangopayuser->LegalPersonType))
                                    <input type="hidden" name="usertype" value="1"> --}}
                                    {{-- <div class="col-sm-3 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method"><center>
                                            @if(isset($address_kyc) && count($address_kyc))
                                                @if(isset($address_kyc->Status) && $address_kyc->Status=="VALIDATION_ASKED")
                                                <span style="color: #e8128c;">KYC Status : Pending </span>
                                                @elseif(isset($address_kyc->Status) && $address_kyc->Status=="VALIDATED")
                                                <span style="color: #228B22;">KYC Status : Approved </span>
                                                @else
                                                <span style="color: #FF0000;">KYC Status : Refused </span>
                                                @endif
                                            @endif
                                            <br>    
                                            Upload address proof</center></div>
                                            <img src="{{url('public/Assets/frontend/images/ddoc.jpg')}}" style="padding-bottom: 0px;height: 150px;width: 180px" id="v_address_img" alt="Your Images" class="img-responsive img-upload-course" >
                                            <input type='file' id="v_address_img_input" name="v_address_img" onchange="readURLThumb(this,'v_address_img');" required accept="image/png, image/jpeg, image/jpg" />
                                            <label id="fileupload-course-label" for="v_address_img_input">Upload Image
                                            </label>
                                            <center>
                                            <span style="margin-top: 15px;display: block;">Max file size 6 MB<br></span>
                                            <span>File format jpeg, jpg, png<br></span>
                                            <span style="color: red" class="videoerrsize">Max file size 6 MB<br></span>
                                            <span style="margin-top: 10px;color: red" class="videoerrtype">File type shoud be jpg, jpeg, png</span>
                                            </center>
                                            
                                        </div>  
                                    </div> --}}
                                   {{--  @else
                                    <input type="hidden" name="usertype" value="2">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method"><center>
                                            @if(isset($registration_kyc) && count($registration_kyc))
                                                @if(isset($registration_kyc->Status) && $registration_kyc->Status=="VALIDATION_ASKED")
                                                <span style="color: #e8128c;">KYC Status : Pending </span>
                                                @elseif(isset($registration_kyc->Status) && $registration_kyc->Status=="VALIDATED")
                                                <span style="color: #228B22;">KYC Status : Approved </span>
                                                @else
                                                <span style="color: #FF0000;">KYC Status : Refused </span>
                                                @endif
                                            @endif
                                            <br>  
                                                
                                            Registration proof</center></div>
                                            <img src="{{url('public/Assets/frontend/images/ddoc.jpg')}}" style="padding-bottom: 0px;height: 150px;width: 180px" id="v_registration_img" alt="Your Images" class="img-responsive img-upload-course" >
                                            <input type='file' id="v_registration_img_input" name="v_registration_img" onchange="readURLThumb(this,'v_registration_img');" required accept="image/png, image/jpeg, image/jpg" />
                                            <label id="fileupload-course-label" for="v_registration_img_input">Upload Image
                                            </label>
                                            <center>
                                            <span style="margin-top: 15px;display: block;">Max file size 6 MB<br></span>
                                            <span>File format jpeg, jpg, png<br></span>
                                            <span style="color: red" class="videoerrsize">Max file size 6 MB<br></span>
                                            <span style="margin-top: 10px;color: red" class="videoerrtype">File type shoud be jpg, jpeg, png</span>
                                            </center>
                                        </div>  
                                    </div>
                                    
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method"><center>
                                            @if(isset($articles_kyc) && count($articles_kyc))
                                                @if(isset($articles_kyc->Status) && $articles_kyc->Status=="VALIDATION_ASKED")
                                                <span style="color: #e8128c;">KYC Status : Pending </span>
                                                @elseif(isset($articles_kyc->Status) && $articles_kyc->Status=="VALIDATED")
                                                <span style="color: #228B22;">KYC Status : Approved </span>
                                                @else
                                                <span style="color: #FF0000;">KYC Status : Refused </span>
                                                @endif
                                            @endif
                                            <br>      
                                            Articles of association</center></div>
                                            <img src="{{url('public/Assets/frontend/images/ddoc.jpg')}}" style="padding-bottom: 0px;height: 150px;width: 180px" id="v_articles_img" alt="Your Images" class="img-responsive img-upload-course" >
                                            <input type='file' id="v_articles_img_input" name="v_articles_img" onchange="readURLThumb(this,'v_articles_img');" accept="image/png, image/jpeg, image/jpg" @if(isset($userdata->v_company_type) && $userdata->v_company_type=="BUSINESS" || $userdata->v_company_type=="ORGANIZATION") required @endif />
                                            <label id="fileupload-course-label" for="v_articles_img_input">Upload Image
                                            </label>
                                            
                                            <center>
                                            <span style="margin-top: 15px;display: block;">Max file size 6 MB<br></span>
                                            <span>File format jpeg, jpg, png<br></span>
                                            <span style="color: red" class="videoerrsize">Max file size 6 MB<br></span>
                                            <span style="margin-top: 10px;color: red" class="videoerrtype">File type shoud be jpg, jpeg, png</span>
                                            </center>

                                        </div>  
                                    </div>
                                    

                                    <div class="col-sm-3 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method"><center>
                                            @if(isset($shareholder_kyc) && count($shareholder_kyc))
                                                @if(isset($shareholder_kyc->Status) && $shareholder_kyc->Status=="VALIDATION_ASKED")
                                                <span style="color: #e8128c;">KYC Status : Pending </span>
                                                @elseif(isset($shareholder_kyc->Status) && $shareholder_kyc->Status=="VALIDATED")
                                                <span style="color: #228B22;">KYC Status : Approved </span>
                                                @else
                                                <span style="color: #FF0000;">KYC Status : Refused </span>
                                                @endif
                                            @endif
                                            <br>
                                                
                                            Shareholder declaration</center></div>
                                            <img src="{{url('public/Assets/frontend/images/ddoc.jpg')}}" style="padding-bottom: 0px;height: 150px;width: 180px" id="v_shareholder_img" alt="Your Images" class="img-responsive img-upload-course" >
                                            <input type='file' id="v_shareholder_img_input" name="v_shareholder_img" onchange="readURLThumb(this,'v_shareholder_img');"  accept="image/png, image/jpeg, image/jpg" @if(isset($userdata->v_company_type) && $userdata->v_company_type=="BUSINESS") required @endif />
                                            <label id="fileupload-course-label" for="v_shareholder_img_input">Upload Image
                                            </label>
                                            <center>
                                            <span style="margin-top: 15px;display: block;">Max file size 6 MB<br></span>
                                            <span>File format jpeg, jpg, png<br></span>
                                            <span style="color: red" class="videoerrsize">Max file size 6 MB<br></span>
                                            <span style="margin-top: 10px;color: red" class="videoerrtype">File type shoud be jpg, jpeg, png</span>
                                            </center>

                                        </div>  
                                    </div>
                                    @endif --}}

                                    </div>
                                    <div style="clear: both"></div>
                                 
                                </div>
                            </div>
                        </div>

                    </div>
                    */ ?>

                    <div class="top-btnspace">
                        <button type="button" onclick="submitKyc()" class="btn form-next" id="sub">Submit</button>
                    </div>

                    <div style="clear: both"></div>
                   



                 </form>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-edit-billing-details-2 -->

@stop

@section('js')

       
    <script src="{{url('public/Assets/plugins')}}/dropzone/dropzone.js"></script>

     <script type="text/javascript">
        
        function submitKyc() {
            
            var identity = $("#identity").find(".dropzone-previews").length
            if(identity<1){
                $("#validatemessageidentity").css("display","block");
                var datascroll = $("#validatemessageidentity").offset().top-400;
                $('html, body').animate({
                    scrollTop: datascroll
                }, 1000);
                return false;
            }else{
                $("#validatemessageidentity").css("display","none");
            }

            @if(isset($userdata->v_company_type) && $userdata->v_company_type!="INDIVIDUAL")
            var registration = $("#registration").find(".dropzone-previews").length
            if(registration<1){
                $("#validatemessageregistration").css("display","block");
                var datascroll = $("#validatemessageregistration").offset().top-400;
                $('html, body').animate({
                    scrollTop: datascroll
                }, 1000);
                return false;
            }else{
                $("#validatemessageregistration").css("display","none");
            }
            @endif

            @if(isset($userdata->v_company_type) && $userdata->v_company_type=="BUSINESS" || $userdata->v_company_type=="ORGANIZATION")
            var articles = $("#articles").find(".dropzone-previews").length
            if(articles<1){
                $("#validatemessagearticles").css("display","block");
                var datascroll = $("#validatemessagearticles").offset().top-400;
                $('html, body').animate({
                    scrollTop: datascroll
                }, 1000);
                return false;
            }else{
                $("#validatemessagearticles").css("display","none");
            }
            @endif

            @if(isset($userdata->v_company_type) && $userdata->v_company_type=="BUSINESS")
            var shareholder = $("#shareholder").find(".dropzone-previews").length
            if(shareholder<1){
                $("#validatemessageshareholder").css("display","block");
                var datascroll = $("#validatemessageshareholder").offset().top-400;
                $('html, body').animate({
                    scrollTop: datascroll
                }, 1000);
                return false;
            }else{
                $("#validatemessageshareholder").css("display","none");
            }
            @endif
            $("#editbillingform").submit();
          
        }
    
        function readURLThumb(input,dataname) {
            
            if (input.files && input.files[0]) {
                if(input.files[0].size > 6291456) {
                    $('#'+dataname+'_input').parent("div").find(".videoerrsize").css("display","block");
                    return false;
                }
                if(input.files[0].type=="image/jpeg" || input.files[0].type=="image/jpg" || input.files[0].type=="image/png") {
                    $('#'+dataname+'_input').parent("div").find(".videoerrtype").css("display","none");
                }else{
                    $('#'+dataname+'_input').parent("div").find(".videoerrtype").css("display","block");
                    return false;
                }   

                $('#'+dataname+'_input').parent("div").find(".videoerrsize").css("display","none");    
                $('#'+dataname+'_input').parent("div").find(".videoerrtype").css("display","none");
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#'+dataname)
                        .attr('src', e.target.result)
                        .width(180)
                        .height(150);
                };
                reader.readAsDataURL(input.files[0]);
            }

        }



    </script>
@stop

