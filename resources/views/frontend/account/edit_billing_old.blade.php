@extends('layouts.frontend')

@section('content')

 <!-- 26B-control-panel-edit-billing-details-2-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> Edit Billing Details </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-support">
                    
                <form class="horizontal-form" role="form" method="POST" name="editbillingform" id="editbillingform" action="{{url('profile/billing-update')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}    
                    <div class="billing-detail">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="field-support">
                                    Payment Method
                                </div>
                            </div>
                            <div class="radio-choise">
                                <div class="col-xs-12">
                                    <div class="payment-method">
                                        Select your payment method
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="reg">
                                        <bdo dir="ltr">
                                          <input type="radio" value="paypal" checked name="billing_detail[v_type]" onchange="billingdetail(this.value)">
                                          <span></span>
                                          <abbr class="support-query"> PayPal </abbr>
                                        </bdo>
                                    </div>
                                    <div class="reg">
                                        <bdo dir="ltr">
                                          <input type="radio" value="bank" name="billing_detail[v_type]" onchange="billingdetail(this.value)">
                                          <span></span>
                                          <abbr class="support-query"> Bank </abbr>
                                        </bdo>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="paypal_detail_div">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="field-support">
                                        PayPal Details
                                    </div>
                                </div>
                            </div>
                            <div class="row-field-support">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Enter your email address
                                            </div>
                                            <input type="email" value="{{isset($userdata->billing_detail['paypal_email']) ? $userdata->billing_detail['paypal_email']:''}}" class="form-control input-field" id="paypalemail" name="billing_detail[paypal_email]" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="bank_detail_div" style="display: none">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="field-support">
                                        Bank Details
                                    </div>
                                </div>
                            </div>
                            <div class="row-field-support">
                                <div class="row">
                                    
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Enter bank name
                                            </div>
                                            <input type="text" class="form-control input-field" value="{{isset($userdata->billing_detail['v_bankname']) ? $userdata->billing_detail['v_bankname']:''}}" name="billing_detail[v_bankname]" id="v_bankname">
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Enter Account No:
                                            </div>
                                            <input type="text" class="form-control input-field" value="{{isset($userdata->billing_detail['v_accountno']) ? $userdata->billing_detail['v_accountno']:''}}" name="billing_detail[v_accountno]" id="v_accountno">
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Enter IFSC Code:
                                            </div>
                                            <input type="text" class="form-control input-field" value="{{isset($userdata->billing_detail['v_ifsc']) ? $userdata->billing_detail['v_ifsc']:''}}" name="billing_detail[v_ifsc]" id="v_ifsc">
                                        </div>
                                    </div>

                                     <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Enter Account Holder name:
                                            </div>
                                            <input type="text" class="form-control input-field" value="{{isset($userdata->billing_detail['v_account_holder_name']) ? $userdata->billing_detail['v_account_holder_name']:''}}" name="billing_detail[v_account_holder_name]" id="v_account_holder_name">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="top-btnspace">
                        <a href="{{url('dashboard')}}">
                        <button type="button" class="btn form-save-exit"> Discard </button>
                        </a>
                        <button type="submit" class="btn form-next"> Save Change </button>
                    </div>
                </form>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-edit-billing-details-2 -->

@stop

@section('js')
     <script type="text/javascript">
        
        function billingdetail(data) {
            
            if(data=="bank"){
                $("#paypal_detail_div").css("display","none");    
                $("#bank_detail_div").css("display","block");  
                $("#paypalemail").removeAttr("required");

                $("#v_bankname").attr("required","true");
                $("#v_accountno").attr("required","true");
                $("#v_ifsc").attr("required","true");
                $("#v_account_holder_name").attr("required","true");
          
            }else{
                $("#paypal_detail_div").css("display","block");   
                $("#bank_detail_div").css("display","none");     

                $("#v_bankname").removeAttr("required");
                $("#v_accountno").removeAttr("required");
                $("#v_ifsc").removeAttr("required");
                $("#v_account_holder_name").removeAttr("required");
                $("#paypalemail").attr("required","true");


            }

        }




    </script>
@stop

