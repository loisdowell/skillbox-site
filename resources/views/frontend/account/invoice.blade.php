@extends('layouts.frontend')

@section('content')
    
    <!-- 26B-control-panel-invoice-2 -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> Invoice Centre </h1>
                </div>
            </div>
        </div>
    </div>  

    <div class="container">
        <div class="main-invoice-setting">
                
            {{-- <form method="GET" name="searchrransaction" id="searchrransaction" action="" >
            <div class="row">
                <div class="col-sm-4">
                    
                    <select class="form-control form-sell" name="type" onchange="masterSearch(this.value)">
                        <option value="">All Transaction</option>
                        
                        <option value="course" @if(isset($data['type']) && $data['type']=='course') selected @endif    > Courses Transaction </option>
                        <option value="skill" @if(isset($data['type']) && $data['type']=='skill') selected @endif    > Skill Transaction </option>
                        <option value="userplan" @if(isset($data['type']) && $data['type']=='userplan') selected @endif    > Subscription Transaction</option>
                    </select>

                </div>
            </div>
            </form> --}}

            <div class="invoice-history">
                <h5>
                    History
                </h5>
                <div class="invoice-table">
                    <table class="table table-invoicedetail">
                        <thead>
                            <tr>
                                <th>DATE</th>
                                <th>FOR</th>
                                <th>AMOUNT</th>
                                <th>DOWNLOAD</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if(isset($invoice) && count($invoice))
                                @foreach($invoice as $key => $val)
                                    <tr>
                                        <td>{{date("M d, Y",strtotime($val->d_added))}}</td>
                                        <td>
                                            @if($val->e_transaction_type=='userplan')
                                                <?php echo ucfirst($val->v_order_detail['v_plan_duration']).' subscription for '.$val->v_order_detail['v_plan_name'];
                                                ?>
                                            @else
                                                Payment for {{$val->v_order_detail[0]['name']}}
                                            @endif
                                        </td>

                                        <td><span class="amount-color"> - £{{$val->v_amount}}</span></td>
                                        
                                        <td> 
                                            <a href="{{url('invoice/download')}}/{{$val->id}}">    
                                                Download Invoice <img src="{{url('public/Assets/frontend/images/download-logo.png')}}" alt="Download" class="download-logo">
                                            </a>
                                           </td>
                                    </tr> 
                                @endforeach

                            @else
                            <tr>
                                <td colspan="4">Currently, there are no invoice available.</td>
                            </tr>

                            @endif
                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-invoice-2 -->

@stop

@section('js')
     <script type="text/javascript">
        
        function masterSearch(data){
            $("#searchrransaction").submit();
        }

    </script>
@stop

