
<!doctype html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <title>Invoice</title>
   </head>
   <body>
      <table style="width: 100%; /*margin: 30px 50px 30px 30px;*/ font-family: arial;">
         <tr>
           <td style="width: 35%;">
             <h1 style="color: #333;">{{$sitename}}</h1>
             <p style="margin: 0px;font-size: 12px; color: #333; margin-bottom: 5px;"><?php echo $companyadd; ?></p>
           </td>

            <td style="width: 65%;">
                <?php
                    $sitelogoheader = App\Helpers\General::getSiteSetting('SITE_LOGO');
                    if($sitelogoheader!=""){ ?>
                        <img src="{{url('public/uploads/settings')}}/{{$sitelogoheader}}" alt="images" style="float: right;">
                <?php }else{ ?>
                        <img src="{{url('public/Assets/frontend/images/logo.png')}}" alt="images" style="float: right;">
                <?php } ?>
               <br>
            </td>
         </tr>
      </table>
      
      <table style="width: 100%; /*margin: 30px 50px 30px 30px;*/ font-family: arial;">
         <tr>
           <td style="width: 35%;">
             <b>To</b>
           </td>
            <td style="width: 65%; ">
               <p style="text-align: right;"><b>Invoice Date:</b> {{isset($data['d_date']) ? date("M d,Y",strtotime($data['d_date'])) : ''}}</p>
            </td>
         </tr>
      </table>

     <table style="width: 100%; /*margin: 30px 50px 30px 30px;*/ font-family: arial;">
         <tr>
           <td style="width: 65%;">
             <p style="margin: 0px;font-size: 12px; color: #333; margin-bottom: 5px;">{{isset($data['v_name']) ? $data['v_name'] :''}} </p>
             <p style="margin: 0px;font-size: 12px; color: #333; margin-bottom: 5px;">{{$data['v_address']}}</p>
           </td>
            <td style="width: 35%;">
            </td>
         </tr>
      </table>

  
      <table style="width: 100%; /*margin: 30px 50px 30px 30px;*/ font-family: arial;">
         <tr>
           <td style="width: 100%;">
             <h2 style="text-align: center; font-size: 45px;  ">Invoice {{isset($data['orderno']) ? $data['orderno'] : ''}} </h2>
           </td>
         </tr>
      </table>

      
      <table class="table table-condensed" style="margin-bottom: 0px; width: 100%;border-collapse: collapse;">
          
          <tbody><tr style="border-bottom: 1px solid #6e6e6e;">
              <th style="width: 40%;padding: 15px 0;border: 0;background: #f1f3f3;text-align: center;color: #e8128c;font-size: 12px; border: 1px solid #000;">TITLE</th>
              <th style="padding: 15px 0;border: 0;background: #f1f3f3;text-align: center;color: #e8128c;font-size: 12px; border: 1px solid #000;">UNIT PRICE</th>
              <th style="padding: 15px 0;border: 0;background: #f1f3f3;text-align: center;color: #e8128c;font-size: 12px; border: 1px solid #000;">EXTENDED PRICE</th>
          </tr>
          
          <tr style="border-bottom: 1px solid #6e6e6e;">
              <td class="Subtotal" style="text-align: left;padding-left: 30px;font-size: 15px;color: #000;padding: 15px 0px; border: 1px solid #000; padding-left: 10px;">{{isset($data['title']) ? $data['title'] :''}}</td>
              <td style="text-align: center;padding-left: 30px;font-size: 15px;color: #000;padding: 15px 0px; border: 1px solid #000;">£{{isset($data['price']) ? $data['price'] :''}}</td>
              <td style="text-align: center;padding-left: 30px;font-size: 15px;color: #000;padding: 15px 0px; border: 1px solid #000;">£{{$data['price']}}</td>
          </tr>
          <tr>
          <td colspan="1"> </td>
            <td colspan="1" style=" text-align: left; padding: 10px 0px; border: 2px solid #000;" >
                <b style="padding-left: 10px;">Total</b>
            </td>
            <td colspan="1"  style="text-align: center; padding: 10px 0px; border: 2px solid #000;">
              <b>£{{isset($data['price']) ? $data['price'] : ''}}</b> 
            </td>
          </tr>
        </tbody>
      </table>

    <table style="text-align: center; width: 100%;">
      <tr>
        <td style=""><h3>Thank you for your Business!</h3></td>
      </tr>
    </table>
    
   </body>
</html>
