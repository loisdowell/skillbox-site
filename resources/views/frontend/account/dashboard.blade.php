@extends('layouts.frontend')

@section('content')
<style type="text/css">
    .modal-new .modal-backdrop {z-index: 0;}
    .btn-basic{
        cursor: auto;
    }
    .dimg{
        max-width: 250px;
        max-height: 250px;
        margin: auto;
        overflow: hidden;
        margin-bottom: 30px;
    }
    .img-upload{
        padding-bottom: 0px; 
    }
</style>    
@php
    $standardplanFree= App\Helpers\General::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
@endphp  




<!--modal-->
    <div id="upgradePlanModal" class="modal1 upgradePlanModal_custom" role="dialog">
        <!-- Modal content -->
        <div class="modal-content2">
            <div class="final-containt">
                <span class="close2"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" alt=""  onclick="upgradePlanModalClose()" /></span>
            </div>
            <div class="container">
                <div class="main-dirservice-popup">
                    
                    <form class="horizontal-form" role="form" method="POST" name="planform" id="planform" action="{{url('dashboard/upgrade-user-plan')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}

                    <div class="popup-dirservice">
                        <div class="title-dirservice">
                            <h4> Upgrade Your Plan </h4>
                        </div>
                        <div class="foll-option-popup">
                            You're currently a {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }} member. Please upgrade your Plan.
                        </div>
                        
                        <div class="wrap">
                            <fieldset>
                                <div class="toggle">
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join" checked onchange="plandurationchange('monthly')" >
                                    <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create" onchange="plandurationchange('yearly')">
                                    <label for="create" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    @php
                        $pcnt=0;
                    @endphp

                    @if(count($plandata))
                        @foreach($plandata as $key=>$val)
                            @if($userplandata['v_plan_id']=="5a65b757d3e8125e323c986a")
                                @php $pcnt=1; @endphp
                            @endif
                            @if($key>=$pcnt)
                            <div class="final-leval-popup matchHeights">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="@if($key==0)left-choise @else left-side @endif matchheight_div">
                                            <div class="reg">
                                                <bdo dir="ltr">
                                                    <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required @if($userplandata['v_plan_id']==$val['id']) checked @endif>
                                                    <span class="select-seller-job"></span>
                                                    <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                </bdo>
                                            </div>
                                            <div class="choise-que">{{$val['v_subtitle']}}</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        
                                        <ul class="list-unstyled choise-avalible matchheight_div">
                                            <?php 
                                                    $lcnt=1;
                                                ?>
                                                @if(count($val['l_bullet']))
                                                    @foreach($val['l_bullet'] as $k=>$v) 
                                                        @if($k<3)
                                                        <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                        <?php $lcnt = $lcnt+1; ?>
                                                        @endif
                                                    @endforeach
                                                @endif  
                                        </ul>

                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="right-display matchheight_div">
                                            <ul class="list-unstyled choise-avalible">
                                                <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k>=3)
                                                            <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                            @endif
                                                            <?php $lcnt = $lcnt+1; ?>
                                                        @endforeach
                                                    @endif  

                                                </ul>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-12 same-height-1">
                                        <div class="@if($key==0) free-price @else final-price @endif matchheight_div">
                                            <div class="all-in-data">
                                                <div  class="monthyprice">
                                                       @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_monthly_price']}}</del>
                                                            </div>
                                                            Only £{{$val['f_monthly_dis_price']}} p/m

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif

                                                       @endif  
                                                </div>  
                                                
                                                <div class="yealryprice" style="display: none;">
                                                       @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_yealry_price']}}</del>
                                                            </div>
                                                            Only £{{$val['f_yealry_dis_price']}} p/a

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif
                                                       @endif  
                                                </div>    

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    @endif        

                    <button type="submit" class="btn btn-payment">
                        Proceed
                    </button>

                    </form>

                </div>
                <!-- End 26B-my-profile-with-sponsor-button-POP UP-in-person-or-online -->
            </div>
        </div>
    </div>
<!--end-modal-->

<!-- Navigation Bar -->
    <div class="navigation-bar">
        <div class="container">
            
            <div class="navigation-bar-postiion">
                <div class="right-tabing position-tabing">
                    <button type="button" class="btn btn-basic">{{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }}</button> 
                    @if(isset($userplandata['upgrade']) && $userplandata['upgrade']==1)
                    <button type="button" class="btn btn-upgrade" onclick="upgradeplanmodal()">UPGRADE</button>
                    @endif
                </div>
            </div>

            <ul class="nav nav-tabs nav-youraccount">
                <li class="active"><a href="{{url('dashboard')}}"> Home </a></li>
                <li><a href="{{url('buyer/dashboard')}}"> Your Buyer Account </a></li>
                <li><a href="{{url('seller/dashboard')}}"> Your Seller Account </a></li>
            </ul>
        </div>
    </div>
    <!-- End Navigation Bar -->

    <div class="container">
        <div id="content">

            <!-- 16B-my-profile-home -->
            <div class="tab-pane active">
                <div class="row">
                    <div style="clear: both"></div>
                    <div class="col-lg-12">
                        <div class="tab-content">

                            <!-- 16B-my-profile-home -->
                            <div class="main-class">
                                <div class="row">

                                    <div class="col-sm-12 col-xs-12">

                                        @if ($success = Session::get('success'))
                                          <div class="alert alert-success alert-big alert-dismissable br-5">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
                                          </div>
                                        @endif
                                        @if ($warning = Session::get('warning'))
                                          <div class="alert alert-info alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
                                          </div>
                                        @endif
                                        
                                        @php
                                            $useralert = App\Helpers\General::getSiteSetting("ACTIVE_SUBSCRIBERS_ALERTBOX");
                                            $appalert = App\Helpers\General::getSiteSetting("SKILLBOX_APP_ALERTBOX");
                                        @endphp

                                        <div class="header-info">
                                            @if(!isset($userdata->v_alert_box_close_1))
                                            @if(isset($useralert) && $useralert!="")
                                            <div class="total-subscribe removealertdata" id="box1">
                                                <img src="{{url('public/Assets/frontend/images/news-paper.png')}}" alt="" />
                                                <span>
                                                @php
                                                    echo $useralert;     
                                                @endphp 
                                                </span>
                                                <a href="javascript:;" class="alertdata2" onclick="closealert('1')">
                                                    <img src="{{url('public/Assets/frontend/images/plus.png')}}" alt="" />
                                                </a>
                                            </div>
                                            @endif
                                            @endif

                                            @if(!isset($userdata->v_alert_box_close_2))
                                            @if(isset($appalert) && $appalert!="")
                                            <div class="download-androidapp removealertdata" id="box2">
                                                <img src="{{url('public/Assets/frontend/images/speaker.png')}}" alt="" />
                                                <span>
                                                   @php
                                                       echo $appalert;     
                                                   @endphp
                                                </span>
                                                <a href="javascript:;" class="alertdata2" onclick="closealert('2')">
                                                    <img src="{{url('public/Assets/frontend/images/plus.png')}}" alt="" />
                                                </a>
                                            </div>
                                            @endif
                                            @endif

                                        </div>
                                    </div>
                                </div>


                               
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        
                                       {{--  <div class="panel-img">
                                            @php
                                                $imgdata = \App\Helpers\General::userdashboardimage($userdata);
                                                echo $imgdata;
                                            @endphp
                                        </div> --}}
                                        <form class="horizontal-form" role="form" method="POST" name="updatejobpost2" id="updatejobpost2" data-parsley-validate enctype="multipart/form-data" >
                                        {{ csrf_field() }}

                                        <div class="upload-edit-detail">
                                            @if(isset($userdata->v_image) && $userdata->v_image!="")
                                                @php
                                                    $imgurl = \Storage::cloud()->url($userdata->v_image);    
                                                @endphp 
                                                <div class="dimg"> 
                                                <img src="{{$imgurl}}" alt="Your Images" class="img-responsive img-upload" id="edit-images">
                                                </div>
                                            @else
                                                @php
                                                    $imgurl = \Storage::cloud()->url('users/img-upload.png');    
                                                @endphp 
                                                <img src="{{$imgurl}}" alt="Your Images" class="img-responsive img-upload" id="edit-images">

                                            @endif
                                            @if(isset($leveldata) && count($leveldata))
                                             @php
                                                $levelcon="";
                                                if(isset($leveldata->v_icon) && $leveldata->v_icon!=""){
                                                    $levelcon = \Storage::cloud()->url($leveldata->v_icon);
                                                }
                                             @endphp
                                             @if($levelcon!="")
                                             <div class="dashboardbadge">
                                                <img src="{{$levelcon}}" alt="" />
                                             </div>
                                             @endif
                                             @endif

                                            <input type='file' accept="image/x-png,image/gif,image/jpeg" name="v_image" id="fileupload-example-4" onchange="readURL(this);" />
                                            <label id="fileupload-example-4-label" for="fileupload-example-4">Upload Image</label>
                                            <center>
                                            <span style="margin-top: 10px;display: inline-block;">H : 290px , W : 250px <br></span>
                                            </center>
                                            <span class="videoerrsize" style="color: #ccc;text-align:center;margin-top: 10px">File size should not be more than 2 MB<br></span>
                                        </div>
                                        </form>

                                        <div class="upload-msg">
                                            
                                            @if(isset($leveldata) && count($leveldata))
                                              <div class="msg-heading"> SKILLBOX Skill Level:</div>
                                              <button type="button" onclick="openleveldata()"  class="btn btn-level-details"> {{isset($leveldata->v_title) ? $leveldata->v_title : ''}} </button>

                                                {{-- <div class="modal fade" id="leveltextview" role="dialog">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content modal-content-1">
                                                          <button type="button" class="close closemodal" data-dismiss="modal"></button>
                                                            <div class="modal-header modal-header-1">
                                                                <h4 class="modal-title"> {{isset($leveldata->v_text_seller) ? $leveldata->v_text_seller : ''}} </h4>
                                                            </div>
                                                            <div class="modal-footer modal-footer-1">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}

                                                <div id="leveltextview" class="modal fade" role="dialog">
                                                  <div class="modal-dialog">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title" style="text-align: center;">{{isset($leveldata->v_title) ? $leveldata->v_title : ''}}</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                        <p>{{isset($leveldata->v_text_seller) ? $leveldata->v_text_seller : ''}}</p>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <script type="text/javascript">
                                                    function openleveldata(){
                                                        $("#leveltextview").modal("show");
                                                    }
                                                </script>
                                            @endif
                                        </div>
                                        

                                        <div class="edit-billing">
                                            <a href="{{url('profile/billing')}}">
                                                <div class="imgwith-info">
                                                    <div class="img-grp"> <img src="{{url('public/Assets/frontend/images/credit.png')}}" class="img-responsive img-middle" alt="Cradit"> </div>
                                                    {{-- Edit billing details --}}
                                                    Refunds / Payments
                                                </div>
                                            </a>

                                            <a href="{{url('profile/identification-documents')}}">
                                                <div class="imgwith-info">
                                                    <div class="img-grp"> <img src="{{url('public/Assets/frontend/images/job-postiion.png')}}" class="img-responsive img-middle" alt="Cradit"> </div>
                                                    Identification Documents
                                                </div>
                                            </a>

                                            <a href="{{url('invoice')}}">
                                                <div class="imgwith-info">
                                                    <div class="img-grp"> <img src="{{url('public/Assets/frontend/images/file.png')}}" class="img-responsive img-middle" alt="File"> </div>
                                                    Subscription Invoices
                                                </div>
                                            </a>

                                            <a href="{{url('profile/change-password')}}">
                                                <div class="imgwith-info">
                                                    <div class="img-grp"> <img src="{{url('public/Assets/frontend/images/user-03.png')}}" class="img-responsive img-middle" alt="change-password"> </div>
                                                    Change My Password
                                                    {{-- user-03.png --}}
                                                </div>
                                            </a>

                                            <a href="{{url('support')}}">
                                                <div class="imgwith-info">
                                                    <div class="img-grp"> <img src="{{url('public/Assets/frontend/images/comment.png')}}" class="img-responsive img-middle" alt="comment"> </div>
                                                    Support &nbsp; @if($supportcnt!="0")<span style="color: #e70e8a">({{$supportcnt}})</span>@endif
                                                </div>
                                            </a>

                                            <a href="{{url('skillbox-guide')}}">
                                                <div class="imgwith-info">
                                                    <div class="img-grp"> <img src="{{url('public/Assets/frontend/images/info.png')}}" class="img-responsive img-middle" alt="info"> </div>
                                                    How to Skillbox Guide
                                                </div>
                                            </a>

                                            <a href="{{url('message/seller/message-centre')}}">
                                                <div class="imgwith-info">
                                                    <div class="img-grp"> <img src="{{url('public/Assets/frontend/images/comment.png')}}" class="img-responsive img-middle" alt="comment"> </div>
                                                    Message &nbsp; @if($msgcnt!="0")<span style="color: #e70e8a">({{$msgcnt}})</span>@endif
                                                </div>
                                            </a>

                                            

                                            <a href="{{url('account/cancel-account-confirm')}}">
                                                <div class="imgwith-info">
                                                    <div class="img-grp"> <img src="{{url('public/Assets/frontend/images/user-03.png')}}" class="img-responsive img-middle" alt="comment"> </div>
                                                    @if(isset($userplandata['v_plan_id']) && $userplandata['v_plan_id']=="5a65b48cd3e812a4253c9869")
                                                    Cancel Skillbox Account
                                                    @else
                                                    Downgrade or Cancel Account
                                                    @endif
                                                </div>
                                            </a>

                                            <a href="{{url('logout')}}">
                                                <div class="imgwith-info">
                                                    <div class="img-grp"> <img src="{{url('public/Assets/frontend/images/logout.png')}}" class="img-responsive img-middle" alt="comment"> </div>
                                                    Logout
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-sm-8 col-xs-12">
                                        <div class="row">
                                            <div class="col-sm-8 col-xs-12">
                                                <div class="panel-name profile-name">
                                                    <h1>{{isset($userdata->v_fname) ? ucfirst($userdata->v_fname):''}} {{isset($userdata->v_lname) ? ucfirst($userdata->v_lname):''}}</h1>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-xs-12 text-center-xs">
                                                <a href="{{url('profile/edit')}}" class="btn btn-profile">  
                                                    Edit Main Profile 
                                                </a>
                                            </div>
                                        </div>

                                        @if(isset($userdata->v_city) && $userdata->v_city!="" && $countryname!="")
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12 profile-name">
                                                <div class="img-pro">
                                                    <img src="{{url('public/Assets/frontend/images/map.png')}}" class="img-profilelist img-middle" alt="" />
                                                </div>
                                                <span class="info">{{isset($userdata->v_city) ? $userdata->v_city:''}} , {{$countryname}} </span>
                                            </div>
                                        </div>
                                        
                                        @endif

                                        
                                        @if(isset($userdata->d_birthday) && $userdata->d_birthday!="")
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12 profile-name">
                                                <div class="img-pro">
                                                    <img src="{{url('public/Assets/frontend/images/logo-gift.png')}}" class="img-profilelist img-middle" alt="" /></div>
                                                <span class="info"> {{isset($userdata->d_birthday) ? date("d , M, Y",strtotime($userdata->d_birthday)):''}} </span>
                                            </div>
                                        </div>
                                        @endif

                                        @if(isset($userdata->v_gender) && $userdata->v_gender!="")
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12 profile-name">
                                                <div class="img-pro">
                                                    <img src="{{url('public/Assets/frontend/images/logo-gender.png')}}" class="img-profilelist img-middle" alt="" /></div>
                                                <span class="info"> {{isset($userdata->v_gender) ? ucfirst($userdata->v_gender):''}} </span>
                                            </div>
                                        </div>
                                        @endif

                                        @if(isset($userdata->v_avalibility_to) && $userdata->v_avalibility_to!="" && isset($userdata->v_avalibility_to_time) && $userdata->v_avalibility_to_time!="")
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12 profile-name">
                                                <p class="change-color day-time">
                                                @if(isset($userdata->v_avalibility_from) && $userdata->v_avalibility_from!="")
                                                    {{ucfirst(substr($userdata->v_avalibility_from, 0, 3))}}-
                                                @endif
                                                @if(isset($userdata->v_avalibility_to) && $userdata->v_avalibility_to!="")
                                                    {{ucfirst(substr($userdata->v_avalibility_to, 0, 3))}} 
                                                @endif
                                                @if(isset($userdata->v_avalibility_to) && $userdata->v_avalibility_to!="")
                                                    / {{date("h a",strtotime($userdata->v_avalibility_from_time.':00'))}} -
                                                @endif
                                                 @if(isset($userdata->v_avalibility_to_time) && $userdata->v_avalibility_to_time!="")
                                                    {{date("h a",strtotime($userdata->v_avalibility_to_time.':00'))}} 
                                                @endif
                                                </p>
                                            </div>
                                        </div>
                                        @endif

                                        <div class="row" style="visibility: hidden;">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="user-interest">
                                                    Interest:
                                                </div>
                                                <div class="btn-pro">
                                                    <button type="button" class="btn btn-editdetail"> Web Design </button>
                                                    <button type="button" class="btn btn-editdetail"> Basketball </button>
                                                    <button type="button" class="btn btn-editdetail"> Gardening </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="text-center account-summary"> YOUR ACCOUNT SUMMARY
                                                </div>
                                                <div class="col-sm-6 no-space">
                                                    <table class="main-table table-responsive">
                                                        <caption class="text-center account-summary"> BUYER </caption>
                                                        <tr>
                                                            <td class="summary-name">Active Profile:</td>
                                                            <td class="table-value">{{$summaryData['buyer']['activeprofile']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=" summary-name">Active Ads:</td>
                                                            <td class="table-value">{{$summaryData['buyer']['activeads']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="summary-name">Total Ad Impressions: </td>
                                                            <td class="table-value">{{$summaryData['buyer']['totaladsimpressions']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="summary-name">Total Reviews:</td>
                                                            <td class="table-value">{{$summaryData['buyer']['totalreviews']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=" summary-name">Jobs Posted:</td>
                                                            <td class=" table-value">{{$summaryData['buyer']['jobsposted']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=" summary-name">Job Responses: </td>
                                                            <td class=" table-value">{{$summaryData['buyer']['jobresponse']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=" summary-name">Total Spent: </td>
                                                            <td class=" table-value"> £{{$summaryData['buyer']['totalspent']}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class=" summary-name"></td>
                                                            <td class=" table-value"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class=" summary-name data-border"></td>
                                                            <td class=" table-value"></td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div class="col-sm-6 no-space">
                                                    <table class="main-table table-responsive">
                                                        <caption class="text-center account-summary"> SELLER </caption>
                                                        <tr>
                                                            <td class="summary-name">Active Profile: </td>
                                                            <td class="table-value">{{$summaryData['seller']['activeprofile']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="summary-name">Active Ads:</td>
                                                            <td class="table-value">{{$summaryData['seller']['activeads']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="summary-name">Total Ad Impressions: </td>
                                                            <td class="table-value">{{$summaryData['seller']['totaladsimpressions']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="summary-name">Total Reviews: </td>
                                                            <td class="table-value">{{$summaryData['seller']['totalreviews']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="summary-name">Jobs Completed:</td>
                                                            <td class="table-value">{{$summaryData['seller']['jobscompleted']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="summary-name">Jobs Pending: </td>
                                                            <td class="table-value">{{$summaryData['seller']['jobpending']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="summary-name"> Job Earnings: </td>
                                                            <td class="table-value"> £{{$summaryData['seller']['jobearning']}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="summary-name data-border"> Payment Pending: </td>
                                                            <td class="table-value"> {{$summaryData['seller']['paymentpending']}} </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End 16B-my-profile-home -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- end 16B-my-profile-home -->

        </div>
    </div>


@stop
@section('js')

<script type="text/javascript">

        var modal1 = document.getElementById('upgradePlanModal');
        var btn = document.getElementById("myBtn");
        var span = document.getElementsByClassName("close2")[0];
        btn.onclick = function() {
            modal1.style.display = "block";
        }
        span.onclick = function() {
            modal1.style.display = "none";
        }
        window.onclick = function(event) {
            if (event.target == modal1) {
                modal1.style.display = "none";
            }
        }
    
    function upgradeplanmodal(){
        $("#upgradePlanModal").modal("show");
    }

    function plandurationchange(data=""){
            if(data=="monthly"){
                $(".monthyprice").css("display","block");
                $(".yealryprice").css("display","none");

            }else if(data=="yearly"){
                $(".yealryprice").css("display","block");
                $(".monthyprice").css("display","none");
            }
    }

    function upgradePlanModalClose(){
        $("#upgradePlanModal").modal("hide");
    }

    function closealert(databox){

        document.getElementById('load').style.visibility="visible"; 
        var formData = "box="+databox;
        var actionurl = '{{url("account/dashbaord-box-data")}}';
        $.ajax({
            processData: false,
            contentType: false,
            type    : "GET",
            url     : actionurl,
            data    : formData,

            success : function( res )   {
                document.getElementById('load').style.visibility='hidden';
                var obj = jQuery.parseJSON(res);   
                if(obj['status']==1){
                    $("#box"+databox).remove();
                }

            }
          });
    }
    
    function readURL(input) {

        if (input.files && input.files[0]) {

            if(input.files[0].size > 2097152) {
                $('#fileupload-example-4').parent("div").find(".videoerrsize").css("display","block");
                return false;
            }
            $('#fileupload-example-4').parent("div").find(".videoerrsize").css("display","none");

            document.getElementById('load').style.visibility="visible"; 
            var formData = new FormData($('#updatejobpost2')[0]);
            var actionurl = '{{url("account/dashbaord-image")}}';
              $.ajax({
                  processData: false,
                  contentType: false,
                  type    : "POST",
                  url     : actionurl,
                  data    : formData,
                  success : function( res ){
                      document.getElementById('load').style.visibility='hidden';
                      var obj = jQuery.parseJSON(res);   
                      if(obj['status']==1){
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#edit-images')
                                .attr('src', e.target.result)
                                .width(230)
                                .height(230);
                        };
                        reader.readAsDataURL(input.files[0]);
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      
                  }

              });

            
            
        }

    }
    


</script>

 <script type="text/javascript">
    $(document).on("click", ".alertdata", function() { 
        $(this).parents(".removealertdata").remove();
    });
</script>
                                

@stop

