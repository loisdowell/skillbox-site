@extends('layouts.frontend')

@section('css')
<link href="{{url('public/Assets/frontend/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />
<script src="{{url('public/Assets/frontend/js/fileinput.js')}}" type="text/javascript"></script>
<script src="{{url('public/Assets/plugins/webcame/webcam.js')}}" type="text/javascript"></script>
@stop

@section('content')
    
   <!-- 02B-sign-up-add profile photo-1 -->
    <div class="container">
        <div class="profile-skills">
            <h1>Set Your Profile Image</h1>
        </div>
        <div class="profile-main">
            <div class="profile-img">
                
                <form class="horizontal-form" role="form" method="POST" name="signupprofileform" id="signupprofileform" action="{{url('account/update-profile')}}" data-parsley-validate enctype="multipart/form-data" >
                  {{ csrf_field() }}
                <div class="row">
                    
                    <div class="col-sm-6">
                        <div class="image-peve">
                            <div id="my_camera"></div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="image-peve">
                            <div id="results"></div>
                            {{-- <img src="{{url('public/Assets/frontend/images/user1.png')}}" style="margin: 0px auto;" alt="" /> --}}
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="upload-img1">
                            <a href="javascript:;" onClick="take_snapshot()">Take a Photo</a>
                            <p>With your webcam</p>
                        </div>
                    </div>

                    <div class="col-sm-12"> 

                        <div class="btn-allskip" style="max-width: 500px; margin: 33px 129px;">

                            <div class="col-xs-6">
                            <a href="{{url('account/profile')}}"><button type="button" class="btn btn-guide btn-Continue">Back</button></a>
                            </div>
                            <div class="col-xs-6">
                                <div class="guide-img">
                                    <button type="button" onclick="nextcont()" class="btn btn-guide btn-Continue">Continue</button>
                                </div>
                            </div>
                            
                        </div>

                    </div>

                </div>
                </form>
            </div>
        </div>
    </div>
    <!--end- 02B-sign-up-add profile photo-1 -->

@stop


@section('js')

<script language="JavaScript">
    Webcam.set({
      width: 320,
      height: 240,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach('#my_camera' );
</script>


<script language="JavaScript">
    var imgsave = "{{url('account/update-profile/webcame')}}";

    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            Webcam.upload(data_uri, imgsave, function(code, text) {
            document.getElementById('results').innerHTML = 
              '<img src="'+data_uri+'"/>';
            });
        });
    }

    function nextcont(){
        var actionurl = "{{url('account/profile-url')}}";
        var formdata="";
        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
              window.location = res;           
            },
            error: function ( jqXHR, exception ) {
                
            }
        });    
    }

</script>

<script type="text/javascript">


      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function(e) {
                  $('#edit-images')
                      .attr('src', e.target.result)
                      .width(200)
                      .height(200);
              };
              reader.readAsDataURL(input.files[0]);
          }

        }


      function buyerSignup(){

          var actionurl = "{{url('buyer-signup/signup')}}";
          


          var formValidFalg = $("#buyersignupform").parsley().validate('');

          if(formValidFalg){
              
              var l = Ladda.create(document.getElementById('bsignup'));
              l.start();

              var formdata = $("#buyersignupform").serialize();

              $.ajax({
                  type    : "POST",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      l.stop();
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#someerror").show();
                        $("#someerror").html(obj['msg']);
                      }else{
                        $("#someerror").show();
                        $("#someerror").html(obj['msg']);
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      l.stop();
                      $("#someerror").show();
                  }
              });    

          }
      }

</script>

@stop

