@extends('layouts.frontend')

@section('content')
<style type="text/css">
    .dimg{
        max-width: 250px;
        max-height: 250px;
        margin: auto;
        overflow: hidden;
        margin-bottom: 30px;
    }
    .img-upload{
        padding-bottom: 0px; 
    }

.top-btnspace.changePasswordbtn {
    text-align: center !important;
    display: block;
    float: none;
}

</style>

 <!-- 26B-control-panel-profile-settings-2-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">

                <div class="left-supportbtn">
                    <a href="{{url('dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                
                <div class="title-support">
                    <h1> Change My Password </h1>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main-edit-profile">
            
            <div class="edit-details top-space">
            
                <div class="row">
                    

                    @if ($success = Session::get('success'))
                      <div class="alert alert-success alert-big alert-dismissable br-5">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
                      </div>
                    @endif
                    @if ($warning = Session::get('warning'))
                      <div class="alert alert-info alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
                      </div>
                    @endif

                    <form class="horizontal-form" role="form" method="POST" name="changePasswordForm" id="changePasswordForm" action="{{url('profile/update-password')}}" data-parsley-validate >

                    {{ csrf_field() }}    
                    <div class="col-sm-4 col-xs-12">
                        
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <div class="">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Your new password:
                                    </label>
                                    <input type="password" name="password" class="form-control input-field form-pass" required id="pwd2" data-parsley-minlength="8" data-parsley-pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" data-parsley-pattern-message="Your password must contain at least a numeric, special character and uppercase letter.">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                       Confirm your new password:
                                    </label>
                                    <input type="password" name="confirm_password" class="form-control form-pass" required="" id="confirm_pwd" data-parsley-equalto="#pwd2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="top-btnspace changePasswordbtn">
                <button type="submit" class="btn form-next ">Save Changes</button>
            </div>



        </div>
    </div>
    <!-- end 26B-control-panel-profile-settings-2 -->
@stop

@section('js')

    <script src="{{url('public/js/jquery.mask.js')}}" data-autoinit="true"></script>
    


     <script type="text/javascript">
        
         $(document).ready(function(){
            $('#d_birthday').mask("00/00/0000", {placeholder: "DD/MM/YYYY"});
         });       

        $(document).ready(function(){
            // $(".form_datetime2").datetimepicker({
            //     pickTime: false,
            //     minView: 2,
            //     format: 'yyyy-mm-dd',
            //     endDate: '+0d',
            //     autoclose: true,
            // });
        });

    </script>

     <script type="text/javascript">
        function readURL(input) {

            if (input.files && input.files[0]) {
                
                if(input.files[0].size > 2097152) {
                    $('#fileupload-example-4').parent("div").find(".videoerrsize").css("display","block");
                    return false;
                }
                $('#fileupload-example-4').parent("div").find(".videoerrsize").css("display","none");

                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#edit-images')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }

        }

    </script>
@stop

