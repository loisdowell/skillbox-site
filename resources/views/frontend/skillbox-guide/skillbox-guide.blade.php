@extends('layouts.frontend')

@section('content')

    <style type="text/css">
        .panel-default > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #EEEEEE;
            background: #fff;
            color: #000;
        }
    </style>

    <!--slide-containt-->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">

                @php
                    $v_banner_image="";
                    //dd($metaDetails);

                    if($metaDetails['v_image'] && $metaDetails['v_image']!=""){
                        if(Storage::disk('s3')->exists($metaDetails['v_image'])){
                            $v_banner_image = \Storage::cloud()->url($metaDetails['v_image']);      
                        }else{
                            $v_banner_image = url('public/Assets/frontend/images/trust.png');      
                        }                
                    }else{
                        $v_banner_image = url('public/Assets/frontend/images/trust.png');      
                    }
                @endphp

                <img src="{{$v_banner_image}}" class="img-responsive" alt="" />
                <div class="center-containt">
                    <div class="container">
                        <div class="trust-page blog-page">

                            @if($metaDetails['v_title'] && $metaDetails['v_title']!="")
                            <h1>{{$metaDetails['v_title']}}</h1>
                            @else
                            <h1>Frequently Asked Questions</h1>
                            @endif

                            @if($metaDetails['v_sub_headline'] && $metaDetails['v_sub_headline']!="")
                            <p style="margin-top: 25px;">{{$metaDetails['v_sub_headline']}}</p>
                            @else
                            <p style="margin-top: 25px;">Follow our blog and be the first in touch with our latest articles!</p>
                            @endif

                            <a href="{{url('pages/how-to-guide')}}">
                            <button class="btn btn-load" onclick="jobReview();">how to guide?</button>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-slide-containt-->
    
   <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="search-box">
                    <div class="form-group no-s">
                        <form name="frm" id= "frm" class="" action="{{url('skillbox-guide')}}" method="get" >
                        <div class="input-group input-group-lg">
                            
                            <input name="search" onkeyup="searchfaq(this.value)" required="" value="{{$search or ''}}" class="form-control border-style" placeholder="Please enter your search terms here..." type="search">
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-default border-style" type="button"><img src="{{url('public/Assets/frontend/images/search.png')}}" alt="" /></button>
                            </span>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

            <div id="searchdata">
            @if(isset($data) && count($data))
                <div class="col-xs-12">
                    @if(isset($data['general']) && count($data['general']))
                        <div class="heading-title" id="general">
                            <h1 class="title-faq" style="color: #000"> General Questions </h1>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                @foreach($data['general'] as $key => $val)
                                    <div class="panel panel-default panel-nonestyle" id="heading{{$val->id or ''}}">
                                        <div class="panel-heading panel-heading-css" role="tab" id="heading1{{$val->id or ''}}">
                                            <h4 class="panel-title">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#{{$val->id or ''}}" aria-expanded="false" aria-controls="collapseOne">
                                                    {{$val->v_question or ''}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="{{$val->id or ''}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$val->id or ''}}">
                                            <div class="panel-body">
                                                <p style="font-size: 18px">
                                                @php
                                                    $ans = str_replace("\r\n\r\n", "<br><span style='height: 30px;display: inline-block;'></span>", $val->l_answer);
                                                @endphp    
                                                <?php echo nl2br($ans); ?>
                                                <?php //echo nl2br($val->l_answer); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    @if(isset($data['seller']) && count($data['seller']))
                        <div class="heading-title" id="seller">
                            <h1 class="title-faq" style="color: #000"> Seller Questions </h1>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                @foreach($data['seller'] as $key => $val)
                                    <div class="panel panel-default panel-nonestyle" id="heading{{$val->id or ''}}">
                                        <div class="panel-heading panel-heading-css" role="tab" id="heading1{{$val->id or ''}}">
                                            <h4 class="panel-title">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#{{$val->id or ''}}" aria-expanded="false" aria-controls="collapseOne">
                                                    {{$val->v_question or ''}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="{{$val->id or ''}}" class="panel-collapse collapse @if(isset($_REQUEST['id']) && $_REQUEST['id'] == $val->id) in @endif" role="tabpanel" aria-labelledby="heading{{$val->id or ''}}">
                                            <div class="panel-body">
                                                <p style="font-size: 18px">
                                                @php
                                                    $ans = str_replace("\r\n\r\n", "<br><span style='height: 30px;display: inline-block;'></span>", $val->l_answer);
                                                @endphp    
                                                <?php echo nl2br($ans); ?>
                                                   
                                                <?php //echo nl2br($val->l_answer); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    @if(isset($data['buyer']) && count($data['buyer']))
                        <div class="heading-title" id="buyer">
                            <h1 class="title-faq" style="color: #000"> Buyer Questions </h1>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                @foreach($data['buyer'] as $key => $val)
                                    <div class="panel panel-default panel-nonestyle">
                                        <div class="panel-heading panel-heading-css" role="tab" id="heading{{$val->id or ''}}">
                                            <h4 class="panel-title">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#{{$val->id or ''}}" aria-expanded="false" aria-controls="collapseOne">
                                                    {{$val->v_question or ''}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="{{$val->id or ''}}" class="panel-collapse collapse @if(isset($_REQUEST['id']) && $_REQUEST['id'] == $val->id) in @endif" role="tabpanel" aria-labelledby="heading{{$val->id or ''}}">
                                            <div class="panel-body">
                                                <p style="font-size: 18px">
                                                @php
                                                    $ans = str_replace("\r\n\r\n", "<br><span style='height: 30px;display: inline-block;'></span>", $val->l_answer);
                                                @endphp    
                                                <?php echo nl2br($ans); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            @else
                <h2 class="text-center title-faq"> No result found. </h2>
                @if($search!="")
                <a href="{{url('skillbox-guide')}}">
                <h4 class="text-center title-faq" style="font-size: 14px"> Clear Search. </h4>
                </a>
                @endif
            @endif
            </div>


        </div>
    </div>    
 
@stop


@section('js')
<script type="text/javascript">
    var hash = window.location.hash;
    $(document).ready(function () {
        var datahe=$(hash).offset().top-100;
        $('html, body').animate({
            scrollTop: datahe
        }, 'slow');
    });
</script>

<script type="text/javascript">

    function searchfaq(data){
        
        var keystr = data.length;
        if(keystr>3){

            var actionurl = "{{url('skillbox-guide-search')}}";     
            var formData = "search="+data;
             
            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#searchdata").html(obj['responsestr']);  
                    }
                },
            });
        }else{

            var actionurl = "{{url('skillbox-guide-search')}}";     
            var formData = "search=";
            
            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#searchdata").html(obj['responsestr']);  
                    }
                },
            });

        }
    }




</script>



@stop

