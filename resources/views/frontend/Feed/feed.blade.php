{{ Request::header('Content-Type : application/xml') }}

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>

<title>Skillbox</title>
<description>RSS Feed</description>
<link>{{ url('/') }}</link>

@foreach ($posts as $post)
   
    @php
    
    $posttitle = str_replace("&", "&amp;", $post->v_name);
    $postdescription = str_replace("&rdquo;", "”", $post->l_long_description);
    $postdescription = str_replace("&ldquo;", "“", $postdescription);

    $posttitle = stripslashes($posttitle);
    $postdescription = stripslashes($postdescription);
    $blogurl=url('blog').'/'.$post->v_slug;
    $postslug = stripslashes($blogurl);

    $img="";
    if($post->v_image !='') {
        $imgdata="";
        if(Storage::disk('s3')->exists($post->v_image)){
            $imgdata = \Storage::cloud()->url($post->v_image);      
        }
        $img = "<img src='".url($imgdata)."' alt='".$posttitle."' width='600'>";
    }else {
        $img = null;
    }
    @endphp

    <item>
        <title>{{ $posttitle }}</title>
        <description><![CDATA[{!! $img !!} {!! $postdescription !!}]]></description>
        <pubDate>{{ date('D, d M Y H:i:s', strtotime($post->d_added)) }} GMT</pubDate>
        <link>{{ $postslug }}</link>
        <guid>{{ $postslug }}</guid>
        <atom:link href="{{ $postslug }}" rel="self" type="application/rss+xml"/>
    </item>

@endforeach

</channel>
</rss>