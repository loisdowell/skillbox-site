@extends('layouts.frontend')

@section('content')


 <!-- 26B-control-panel-my-courses-settings-seller -->
    <div class="title-controlpanel controlpanel_setting_icon">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> Course Settings </h1>
                </div>
                <div class="right-symbol">
                    <a href="{{url('courses/settings')}}/{{$data->id}}" class="setting-course">
                        <img src="{{url('public/Assets/frontend/images/setting-course.png')}}" class="setting-course" alt="" />
                    </a>
                    <a href="{{url('courses/messages')}}/{{$data->id}}"> <img src="{{url('public/Assets/frontend/images/mail.png')}}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    

    <form role="form" method="POST" name="coursessettings" id="coursessettings" action="{{url('courses/update-messages')}}" data-parsley-validate enctype="multipart/form-data" >
    <input type="hidden" name="i_courses_id" value="{{$data->id}}">
    {{ csrf_field() }}
    <div class="container">
        <div class="main-course-Message">

             @if ($success = Session::get('success'))
              <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 20px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
              </div>
            @endif
            @if ($warning = Session::get('warning'))
              <div class="alert alert-info alert-danger" style="margin-top: 20px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
              </div>
            @endif
            
            <div class="welcome-msg">
                <div class="heading-welcome">
                    Welcome message
                     <div class="course-message">
                       Write a welcome message to your students when they buy the course. This could be any message related to course which you would like to convey to your students before they start the course. (Optional).
                    </div>
                </div>
                <textarea name="messages[welcome_message]" rows="6" class="form-control cover-letter">{{isset($data->messages['welcome_message']) ?  $data->messages['welcome_message']:''}}</textarea>
            </div>

            <div class="welcome-msg">
                <div class="heading-welcome">
                    Congratulation Message
                     <div class="course-message">
                       Congratulate your students when they complete the course. The message will be sent to them via email. (Optional).
                    </div>
                </div>
                <textarea name="messages[welcome_congratulations]" rows="6" class="form-control cover-letter">{{isset($data->messages['welcome_congratulations']) ?  $data->messages['welcome_congratulations']:''}}</textarea>
            </div>
            <div class="btn-settingcourse">
                <button type="submit" class="btn btn-savechange"> Save Message </button>
            </div>
        </div>
    </div>

    </form>

    
@stop
@section('js')

<script type="text/javascript">

</script>

@stop

