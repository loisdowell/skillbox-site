@extends('layouts.frontend')

@section('content')

    <style type="text/css">
        .letter-text-myprifile {
            text-align: center;
            font-size: 34px;
            padding: 10px 0px;
            color: rgb(231, 14, 138);
        }
        .new-seller-find {border-radius: 20px;
        margin-right: 10px;}
        .new-submit-live {height: 34px;}
        .new-submit-comment{display: inline-flex;width: 100%}
    </style>


    <!--26b-control-panel-my-courses-overviwe-->
    <div class="title-controlpanel controlpanel_setting_icon">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> My Courses Details </h1>
                </div>
                <div class="right-symbol">
                   <a href="{{url('courses/settings')}}/{{$data->id}}" class="setting-course">
                        <img src="{{url('public/Assets/frontend/images/setting-course.png')}}" class="setting-course" alt="" />
                    </a>
                    <a href="{{url('courses/messages')}}/{{$data->id}}"> <img src="{{url('public/Assets/frontend/images/mail.png')}}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-detail tabdetailplace">
        
        <div class="container">
            <ul class="nav nav-pills nav-pills-new">
                <li><a href="{{url('courses/view')}}/{{$data->id}}">Overview </a></li>
                <li><a href="{{url('courses/section')}}/{{$data->id}}">Course Sections </a></li>
                <li class="active"><a href="{{url('courses/qa')}}/{{$data->id}}">Q&A <span class="place-badge">{{isset($total['qa']) ? $total['qa'] : 0}}</span></a></li>

                <li><a href="{{url('courses/reviews')}}/{{$data->id}}"> Course Reviews <span class="place-course">{{isset($total['reviews']) ? $total['reviews'] : 0}}</span> </a></li>
                <li><a href="{{url('courses/announcements')}}/{{$data->id}}">Announcements <span class="place-annou">{{isset($total['announcements']) ? $total['announcements'] : 0}}</span> </a></li>
            </ul>
        </div>

    </div>


    <!-- Tab panes -->  
    <div class="container">

        <!-- 26B-control-panel-my-courses-q&a-seller -->
        <div class="qaseller-title">
            <div class="row">
                <div class="col-sm-8 ">
                    <form role="form" method="GET" id="qasearch" data-parsley-validate enctype="multipart/form-data" style="width: 100%"  action="" >
                    <input type="text" onkeyup="searchfaq(this.value)" @if(isset($_REQUEST['search'])) value="{{$_REQUEST['search']}}" @endif name="search" class="form-control text-search" placeholder="Search for a question">
                    <input type="hidden" name="icid" value="{{$data->id}}" id="icid">
                    </form>
                </div>
                <div class=" col-sm-4">
                    <div class="wrap selected-radiocourse mt-xs-15">
                        <form>
                            <fieldset>
                                 <div class="toggle">
                                    <input type="radio" class="toggle-input" name="joincreate" onchange="coursesStatus(this.value,'{{$data->id}}')" value="published" id="join" @if(isset($data->e_status) && $data->e_status=='published') checked @endif >
                                    <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Published</label>
                                    <input type="radio" class="toggle-input" name="joincreate" onchange="coursesStatus(this.value,'{{$data->id}}')" value="unpublished" id="create" @if(isset($data->e_status) && $data->e_status=='unpublished') checked @endif>
                                    <label for="create" class="toggle-label toggle-label-on" id="createLabel">Unpublished</label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="commonmsg"></div>
         <div id="searchdata">
        @if(isset($coursesQA) && count($coursesQA))        
            @foreach($coursesQA as $key=>$val)

                <div class="accordion-final-announc final-announc-Q-a"  @if($key=="0") active @endif>
                    <div class="accordion accordion-announcement">
                        <div class="accordion-toggle1 accordion-toggle-qa">
                            <div class="Announcements-find Announcements-q-a">
                                <?php
                                    $imgdata="";
                                    if(isset($userList[$val->i_user_id])){
                                        $imgdata = \App\Helpers\General::userroundimage($userList[$val->i_user_id]);
                                    }
                                ?>
                                <div class="Announcements-man">
                                    <?php 
                                        echo $imgdata;
                                    ?>    
                                </div>

                                <div class="Announcements-name q-a-name">
                                    <p>{{count($val->hasUser()) ? $val->hasUser()->v_fname.' '.$val->hasUser()->v_lname : '-'}}</p>
                                    <p><span><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($val->d_added))->diffForHumans() ?></span></p>
                                    <p>{{$val->v_title}}</p>
                                    <p class="ans-q">{{isset($val->l_questions) ? $val->l_questions:''}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-content"  @if($key=="0") style="display: block;" @endif>
                            <div class="Announcements-text">
                                
                            <div id='commentsstr{{$val->id}}'>
                                @if(isset($val->l_answers) && count($val->l_answers))        
                                    @foreach($val->l_answers as $k=>$v)

                                        <div class="Announcements-second-man qaseller-second-man">
                                            <div class="qa-seller-find">
                                                                                                        
                                                <div class="Announcements-man">
                                                    <?php
                                                        $imgdata="";
                                                        if(isset($userList[$v['i_user_id']])){
                                                            $imgdata = \App\Helpers\General::userroundimage($userList[$v['i_user_id']]);
                                                        }
                                                        echo $imgdata;
                                                    ?>    
                                                </div>
                                                
                                                <div class="Announcements-name">
                                                    <p>

                                                    <?php
                                                        $username="";
                                                        if(isset($userList[$v['i_user_id']])){
                                                            $username = $userList[$v['i_user_id']]['v_name'];
                                                        }
                                                        echo $username;
                                                    ?>    
                                                    </p>
                                                    <p><span><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($v['d_added']))->diffForHumans(); ?></span></p>
                                                    <div class="Announcements-message" style="width: 100%">
                                                        {{$v['v_text']}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                </div>

                                <div class="qa-seller-input">
                                    <div class="qa-seller-find">
                                        <div class="Announcements-man">
                                            <?php
                                                
                                                $imgdata = \App\Helpers\General::userroundimage($curentuser);
                                                echo $imgdata;
                                            ?> 
                                        </div>

                                        <form role="form" method="POST" name="commnetform{{$val->id}}" id="commnetform{{$val->id}}" data-parsley-validate enctype="multipart/form-data" style="width: 100%" onsubmit="submitcomment('{{$val->id}}')" action="javascript:;" >
                                        {{ csrf_field() }}
                                        <input type="hidden" name="i_course_qa_id" value="{{$val->id}}">
                                        <div class="Announcements-name">
                                            <p>{{$curentuser['v_name']}}</p>
                                            <div class="new-submit-comment">
                                            <input type="text" class="form-control comment-seller new-seller-find" name="v_text" id="v_text{{$val->id}}" placeholder="Enter your comment" required>
                                            <button type="button" onclick="submitcomment('{{$val->id}}')" class="btn btn-convarsation-submit new-submit-live"> Submit </button>
                                            </div>
                                        </div>
                                        </form>


                                    </div>
                                </div>
                                <div class="alert alert-info alert-danger" style="display: none;" id="commonerr">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;<span id="commonerrmsg">Something went wrong.pleease try again after some time.</span>
                                  </div>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        @else
            <div class="Course-Reviews">
                 <div class="row">
                    <div class="col-md-6">
                         No questions and answers.
                    </div>
                </div>
            </div>

        @endif        
        </div>
    </div>
    <!--26b-control-panel-my-courses-overviwe-->
   

@stop
@section('js')

<script type="text/javascript">

    $(document).ready(function() {
            $('.accordion').find('.accordion-toggle1').click(function() {
                $(this).next().slideToggle('600');
                $(".accordion-content").not($(this).next()).slideUp('600');
            });
            $('.accordion-toggle1').on('click', function() {
                $(this).toggleClass('active').siblings().removeClass('active');
            });
        });

    function submitcomment(id=""){
       
        var actionurl = "{{url('courses/post-qa')}}";
        var formValidFalg = $("#commnetform"+id).parsley().validate('');

        if(formValidFalg){
            
            document.getElementById('load').style.visibility="visible";            
            var formdata = $("#commnetform"+id).serialize();

            $.ajax({
                type    : "POST",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                   
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        
                        document.getElementById('load').style.visibility="hidden";
                        $('#commentsstr'+id).append(obj['commentstr']);
                        $("#v_text"+id).val("");
                    }else{
                        $('#commonerr').css("display","block");
                        $('#commonerrmsg').html(obj['msg']);
                    }

                },
                
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility="hidden";
                    $('#commonerr').css("display","block");
                }

            });
        }
    }

    function coursesStatus(data="",id=""){
      
        var actionurl = "{{url('courses/updatestatus')}}";
        var formdata = "id="+id+"&e_status="+data;
         document.getElementById('load').style.visibility="visible";   
      
        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
               document.getElementById('load').style.visibility="hidden";
                var obj = jQuery.parseJSON(res);
                
                if(obj['status']==1){
                    $("#commonmsg").html(obj['msg']);
                }else{
                    $('#commonerr').css("display","block");
                    $('#commonerrmsg').html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                document.getElementById('load').style.visibility="hidden";
                $('#commonerr').css("display","block");
            }
        });
    }


     function searchfaq(data){
        
        var keystr = data.length;
        if(keystr>3){

            var actionurl = "{{url('courses/qa-search')}}";     
            var formData = "search="+data;
            var formData = $("#qasearch").serialize();

            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#searchdata").html(obj['responsestr']);  
                    }
                },
            });
        }else{

            var icid = $("#icid").val();
            var actionurl = "{{url('courses/qa-search')}}";     
            var formData = "search="+"&icid="+icid;
            
            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#searchdata").html(obj['responsestr']);  
                    }
                },
            });

        }
    }

</script>

@stop

