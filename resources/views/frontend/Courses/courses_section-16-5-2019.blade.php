@extends('layouts.frontend')

@section('content')
<style type="text/css">
    .browsebutton {
        height: 0;
        width: 0;
        display: none;
    }
    .btn-add-remove {
    background: #a00960;
    border-radius: 20px;
    font-size: 16px;
    color: #fff;
    margin-top: 44px;
    border: 2px solid #a00960;
}

.videopoupmodel video {
    margin: 0 auto;
    height: 700px !important;
}

.btn-add-remove:hover {background:transparent;color: #a00960; border:2px solid #a00960;}
</style>

<link rel="stylesheet" href="{{url('public/Assets/frontend/css/videre.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{url('public/Assets/frontend/css/app.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{url('public/Assets/frontend/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{url('public/Assets/frontend/js')}}/jquery.percentageloader-0.1.css"></script>
<script src="{{url('public/Assets/frontend/js')}}/jquery.percentageloader-0.1.js"></script>

<style type="text/css">
.modal-backdrop.in {display: none;}
.btn-add-remove:hover {background:transparent;color: #a00960; border:2px solid #a00960;}
  .topLoaderinner{
            width: 256px;
            height: 256px;
            margin-bottom: 32px;
            width: 100%;
            height: 100%;
            position: absolute;
            text-align: center;
            top: 0;
            left: 0;
            text-align: center;
            z-index: 99999999;
            background: rgba(0,0,0,0.5);
            margin-top: -62px;
            display: none;
      }
      .topLoaderinner #topLoader{
        height: 100%;
        height: 300px;
        position: absolute;
        left: 0;
        top: 50%;
        bottom: 0;
        right: 0;
        margin: auto;
        transform: translate(0, -50%)
      }
      .topLoaderinner #topLoader p{
        width: 100%;
        color: #fff; font-size: 30px;
      }
      .topLoaderinner #topLoader > div{ margin : auto; }
      .video-play{position: relative; z-index: 0;}

</style>
@php
    
    $totalsec = count($data->section);
    $totalsec = $totalsec+1;

@endphp
<div class="topLoaderinner"><div id="topLoader">
    <p>Uploading Files</p>
</div></div>

<div id="videoplaymodal" class="modal3 videopoupmodel">
    <div class="modal-content-all">
        <div class="container-fluid video_title_container">
            <div class="margin-t-b">
                <div class="row">  
                    <div class="col-sm-6">
                        <div class="vid-tital">
                            <h3>{{isset($data->v_title) ? $data->v_title : ''}}</h3>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-popup-all">
                            {{-- <a href="javascript:;" id="nextvideohtml" onclick="playNext()" class="btn btn-vid">Next Video</a> --}}
                            <span id="nextvideohtml">
                            <button type="button" onclick="playNext()" class="btn btn-vid">Next Video</button>
                            </span>
                            <a href="{{url('courses/qa')}}/{{$data->id}}">
                            <button class="btn btn-vid">BROWSE Q&A</button>
                            </a>
                            <button class="btn btn-vid" type="button" onclick="closevideopopup()">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <div class="video-play">
            <div id="player"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModalCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title custom-font">Confirm Delete Record ?</h3>
          </div>
          <div class="modal-body" style="text-align:center">
          <i class="margin-top-10 fa fa-question fa-5x"></i>
          <h4>Delete !</h4>
          <p>Are you sure you want to delete this course section?</p>
          </div>
          <input type="hidden" name="CDID" id='CDID'>
          <input type="hidden" name="CSDID" id='CSDID'>
          <div class="modal-footer" style="padding-top: 20px;">
            <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" onclick="deleteActionCourse()"><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
            <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
          </div>
        </div>
      </div>
    </div>

<!--26b-control-panel-my-courses-overviwe-->
    <div class="title-controlpanel controlpanel_setting_icon">
        <div class="container">
            <div class="title-control-postion">
                
                <div class="left-supportbtn">
                    <a href="{{url('courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                
                <div class="title-support">
                    <h1> My Courses Details </h1>
                </div>

                <div class="right-symbol">
                   <a href="{{url('courses/settings')}}/{{$data->id}}" class="setting-course">
                        <img src="{{url('public/Assets/frontend/images/setting-course.png')}}" class="setting-course" alt="" />
                    </a>
                    <a href="{{url('courses/messages')}}/{{$data->id}}"> <img src="{{url('public/Assets/frontend/images/mail.png')}}" alt="" />
                    </a>
                </div>

            </div>
        </div>
    </div>
    <div class="tab-detail tabdetailplace">
        <div class="container">
            <ul class="nav nav-pills nav-pills-new">
                
                <li><a href="{{url('courses/view')}}/{{$data->id}}">Overview </a></li>
                <li class="active"><a href="{{url('courses/section')}}/{{$data->id}}">Course Sections </a></li>
                <li><a href="{{url('courses/qa')}}/{{$data->id}}">Q&A <span class="place-badge">{{isset($total['qa']) ? $total['qa'] : 0}}</span></a></li>
                <li><a href="{{url('courses/reviews')}}/{{$data->id}}"> Course Reviews <span class="place-course">{{isset($total['reviews']) ? $total['reviews'] : 0}}</span> </a></li>
                <li><a href="{{url('courses/announcements')}}/{{$data->id}}">Announcements <span class="place-annou">{{isset($total['announcements']) ? $total['announcements'] : 0}}</span> </a></li>
            </ul>
        </div>
    </div>
    <!-- Tab panes -->


    <!-- Modal -->
    <!-- 26B-control-panel-my-courses-add-new-section-1 -->
    <div class="modal  fade" id="addnewsection" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <form class="horizontal-form" role="form" method="POST" name="addnewsectionform" id="addnewsectionform" data-parsley-validate enctype="multipart/form-data" >
            {{ csrf_field() }}  


            <div class="modal-content modal-content-1">
                <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                </div>
               <input type="hidden" name="i_course_id" id="i_course_id" value="{{$data->id}}">    
                <div class="modal-body">

                    <div id="commonpopupmsg"></div>
                    <div class="modal-body-1">

                     <div class="add-chapter">
                            <div class="heading-coursesection">
                                <h2> Course Sections  </h2>
                            </div>
                            
                            <div id="section1">
                                <div class="form-field">
                                    <div class="row-form-field">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <label class="field-name">
                                                    Section {{$totalsec}} Title
                                                </label>
                                                <input type="text" class="form-control input-field" name="section[0][v_title]" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-form-field">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <label class="field-name">
                                                Section {{$totalsec}} Video Title
                                            </label>
                                            <input type="text" class="form-control input-field" name="section[0][video][0][v_title]" required>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            
                                            <label class="field-name">
                                                Section {{$totalsec}} Video
                                            </label>
                                            <input type="text" class="form-control input-field" id="textvideoupload1" readonly>
                                            <span>Supported formats: mp4, mkv, webm, mov, m4v <br></span>
                                            <span>Max file size: 100 MB <br></span>
                                            <span class="videoerr" style="color: red">Video format not supported <br></span>
                                            <span class="videoerrsize" style="color: red">File size should not be more than 100 MB<br></span>

                                            <div class="col-xs-12">
                                                <div class="text-right">
                                                    <input type='file' accept=".mp4, .mkv, .webm, .mov, .m4v" class="browsebutton" name="section[0][video][0][v_video]" required id="videoupload1" onchange="workvideo('videoupload1');"  />
                                                    <label id="fileupload" class="btn-editdetail" for="videoupload1">Browser</label>
                                                </div>
                                            </div>

                                            <div class="checkbox-service">
                                                <label class="checkbox check-boxright">
                                                    <input type="hidden" name="section[0][video][0][i_preview]" value="off">
                                                    <input type="checkbox" name="section[0][video][0][i_preview]">
                                                    <span class="subfield-name"> 
                                                        Preview this course
                                                    </span>
                                                </label>
                                            </div>
                                            
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <label class="field-name">
                                                Video duration 
                                            </label>
                                            <div class="row">
                                                <div class="col-md-6"><input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field col-md-6" name="section[0][video][0][v_duration_min]" value="00" ><div class="text-center">Minutes</div></div>
                                                <div class="col-md-6"><input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true"  class="form-control input-field col-md-6" name="section[0][video][0][v_duration_sec]" value="00"><div class="text-center">Seconds</div></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <label class="field-name">
                                                Section {{$totalsec}} Doc
                                            </label>
                                            <input type="text" class="form-control input-field" id="textdocupload1" readonly>
                                            <span>Supported formats: pdf, doc, docx, txt, xlsx, xls, pptx<br></span>
                                            <span>Max file size: 10 MB <br></span>
                                            <span class="docerr" style="color: red">Document format not supported.<br></span>
                                            <span class="videoerrsize" style="color: red">File size should not be more than 10 MB<br></span>

                                            <div class="col-xs-12">
                                                <div class="text-right">
                                                    <input type='file' accept=".pdf, .docx, .txt, .doc, .xlsx, .xls, .pptx" class="browsebutton" name="section[0][video][0][v_doc]" id="docupload1" onchange="workvideodoc('docupload1');" />
                                                    <label id="fileupload" class="btn-editdetail" for="docupload1">Browser
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>

                                <div id="addchaptersectionvideo1"></div>
                                <input type="hidden" id="addchaptervideouploadcnt1" value="1">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="text-right">
                                        <button type="button" class="btn form-add-course" onclick="addmorevideochapter('1','{{$totalsec}}')" > Add More Video</button>
                                    </div>
                                </div>

                            </div>

                            <div class="add-new-section text-xs-center">
                                <button type="button" class="btn btn-new-section ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="addsectiondata" onclick="addsection('{{$data->id}}')"> Save
                                </button>
                            </div>
                           
                            {{-- </div> --}}

                            <div id="coursessectionmorediv">
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="modal-footer modal-footer-1">
            </div>
            </form>

        </div>
    </div>

    <!-- End 26B-control-panel-my-courses-add-new-section-1 -->
    <!-- End Modal -->

   
    <!-- 26B-control-panel-my-courses-edit-section-1 -->
    <!-- Modal -->
    <div class="modal  fade" id="editcoursesection" role="dialog">
        <div class="modal-dialog modal-lg">

            <form class="horizontal-form" role="form" method="POST" name="editsectionform" id="editsectionform" data-parsley-validate enctype="multipart/form-data" >
            {{ csrf_field() }}    

            <input type="hidden" name="i_course_id" id="i_course_id_edit" value="{{$data->id}}">  
            <input type="hidden" name="i_section_id" id="i_section_id" value="">  

            <!-- Modal content-->
            <div class="modal-content modal-content-1">
                <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                </div>
                <div class="modal-body">
                    <div class="modal-body-1" id="editSectionstr">
                    </div>
                    <div class="add-new-section text-xs-center">
                        <button type="button" onclick="updatesection()" class="btn btn-new-section ladda-button"  data-style="zoom-in" data-spinner-color="#e70e8a" id="editupdatesection">Save</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer modal-footer-1">
            </div>

            </form>

        </div>
    </div>
    <!-- End Modal -->
    <!-- End 26B-control-panel-my-courses-edit-section-1 -->


    <!-- 26B-control-panel-my-courses-edit-section-1 -->
    <!-- Modal -->
    <div class="modal  fade" id="editcoursedetail" role="dialog">
        <div class="modal-dialog modal-lg">

            <form class="horizontal-form" role="form" method="POST" name="editcourseform" id="editcourseform" data-parsley-validate enctype="multipart/form-data" >
            {{ csrf_field() }}    

            <input type="hidden" name="i_course_id" id="i_course_id_edit" value="{{$data->id}}">  
            <!-- Modal content-->
            <div class="modal-content modal-content-1">
            <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <div class="heading-coursesection">
                        <h2> Course Basics  </h2>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="add-chapter" id="editSectionstr">
                            
                            <div class="form-field">
                                <div class="row-form-field">
                                     <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="education-added">
                                                <div class="heading-education">
                                                    <label class="field-name">
                                                        Course Title
                                                    </label>
                                                    <input type="text" data-parsley-minlength="10" data-parsley-minlength-message="Title must be minimum 10 characters" data-parsley-maxlength="50" data-parsley-maxlength-message="Title must be no longer than 50 characters" class="form-control input-field" name="v_title" id="v_title_base" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="employment-added">
                                                <div class="heading-employment">
                                                    <label class="field-name">
                                                        Course Sub Title
                                                    </label>
                                                    <input type="text" class="form-control input-field" name="v_sub_title" id="v_sub_title_base" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-field">
                                <div class="row-form-field">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="education-added">
                                                <div class="heading-education">
                                                    <label class="field-name">
                                                        Author Name
                                                    </label>
                                                    <input type="text" class="form-control input-field" name="v_author_name" id="v_author_name_base" required value="{{$v_author_name or ''}}">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="employment-added">
                                                <div class="heading-employment">
                                                    <label class="field-name">
                                                        Select Language
                                                    </label>
                                                    <select class="form-control form-sell" required name="i_language_id" id="i_language_id_base">
                                                        <option value="">-select-</option>
                                                        @if( isset($coursesLanguage) && count($coursesLanguage) )
                                                          @foreach($coursesLanguage as $key => $val)
                                                            <option value="{{$val->_id}}" >{{$val->v_title}}</option>
                                                          @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-field">
                                <div class="row-form-field">
                                     <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="employment-added">
                                                <div class="heading-employment">
                                                    <label class="field-name">
                                                        Course Level
                                                    </label>
                                                    <select class="form-control form-sell" name="i_level_id" id="i_level_id_base" required>
                                                        <option value="">-select-</option>
                                                        @if( isset($coursesLevel) && count($coursesLevel) )
                                                          @foreach($coursesLevel as $key => $val)
                                                            <option value="{{$val->_id}}">{{$val->v_title}}</option>
                                                          @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="education-added">
                                                <div class="heading-education">
                                                    <label class="field-name">
                                                        Courses Total Duration In Hour's
                                                    </label>
                                                    <input type="time" data-parsley-trigger="keyup change focusin focusout"   class="form-control input-field" name="i_duration" id="i_duration_base" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-field">
                                <div class="row-form-field">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="employment-added">
                                                <div class="heading-employment">
                                                    <label class="field-name">
                                                        Course Category
                                                    </label>
                                                    <select class="form-control form-sell" name="i_category_id" id="i_category_id_base" required>
                                                        <option value="">-select-</option>
                                                        @if( isset($coursesCategory) && count($coursesCategory) )
                                                          @foreach($coursesCategory as $key => $val)
                                                            <option value="{{$val->_id}}">{{$val->v_title}}</option>
                                                          @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-field">
                                <div class="row-form-field">
                                     <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="employment-added">
                                                <div class="heading-employment">
                                                    <label class="field-name">
                                                        Course Description
                                                    </label>
                                                    <textarea name="l_description" id="l_description_base" rows="3" class="form-control cover-letter" required>@if(isset($data->l_description)) @php echo $data->l_description; @endphp @endif</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           

                            <div class="form-field">
                                <div class="row-form-field">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="employment-added">
                                                <div class="heading-employment">
                                                    <label class="field-name">
                                                        Course Requirments
                                                    </label>
                                                    <textarea rows="3" class="form-control cover-letter" name="v_requirement" id="v_requirement_base" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-field">
                                <div class="row-form-field">
                                     <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <label class="field-name">
                                                Instructor Profile
                                            </label>
                                            <div class="upload-edit-course">
                                                <img src="{{url('public/Assets/frontend/images/profile.png')}}" id="v_instructor_image_base" alt="Your Images" class="img-responsive img-upload-course" >
                                                <input type='file' id="fileupload-example-4" name="v_instructor_image" onchange="readURL(this);" />
                                                <label id="fileupload-course-label" for="fileupload-example-4">Upload Image
                                                </label>
                                                <span style="margin-left: 10px">Max file size: 2 MB <br></span>
                                                <span style="margin-left: 10px">H : 195px , W : 260px <br></span>
                                                <span style="margin-left: 10px;color: red" class="videoerrsize">File size should not be more than 2 MB<br></span>
                                            </div>
                                            

                                        </div>
                                        <style type="text/css">
                                                    #fileupload-example-5 {
                                                        height: 0;
                                                        width: 0;
                                                        display: none;
                                                    }
                                                </style>
                                         <div class="col-sm-6 col-xs-12">
                                            <label class="field-name">
                                               Course Thumbnail Image
                                            </label>
                                            <div class="upload-edit-course">
                                                <img src="{{url('public/Assets/frontend/images/profile.png')}}" id="v_course_thumbnail_base" alt="Your Images" class="img-responsive img-upload-course" >
                                                <input type='file' id="fileupload-example-5" name="v_course_thumbnail" onchange="readURLThumb(this);" />
                                                <label id="fileupload-course-label" for="fileupload-example-5">Upload Image
                                                </label>
                                                <span style="margin-left: 10px">Max file size: 2 MB <br></span>
                                                <span style="margin-left: 10px">Height : 195px , Width:260px <br></span>
                                                <span style="margin-left: 10px;color: red" class="videoerrsize">File size should not be more than 2 MB<br></span>

                                            </div>
                                            
                                        </div>

                                        <div class="col-sm-12 col-xs-12">
                                            <label class="field-name">Instructor Details</label>
                                            <textarea name="l_instructor_details" id="l_instructor_details_base" rows="12" class="form-control course-description"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row-form-field">
                                <div class="row">
                                    
                                    <div class="col-sm-6 col-xs-12">
                                        <label class="field-name">
                                            Upload Course Introductory Video
                                        </label>
                                        <input type="text" class="form-control input-field" id="textintrovideoupload1" readonly>
                                        <span>Supported formats: mp4, mkv, webm, mov, m4v <br></span>
                                        <span>Max file size: 100 MB <br></span>
                                        <span class="videoerr" style="color: red">Video format not supported <br></span>
                                        <span class="videoerrsize" style="color: red">File size should not be more than 100 MB<br></span>
                                        <div class="text-right">
                                            <input type='file' accept=".mp4, .mkv, .webm, .mov, .m4v" class="browsebutton" id="introvideoupload1" name="l_video" onchange="workvideo('introvideoupload1');"   />
                                            <label id="fileupload" class="btn-editdetail" for="introvideoupload1">Browser</label>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <label class="field-name">
                                            Enter Course Price (£)
                                        </label>
                                        <input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" step="any" class="form-control input-field" name="f_price" id="f_price_base" required="">
                                        
                                        <div class="checkbox-service">
                                            <label class="checkbox check-boxright">
                                                <input type="checkbox" name="v_course_free" id="v_course_free" onchange="pricevalidation()">
                                                <span class="subfield-name"> 
                                                    Make this course free
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="add-new-section text-xs-center">
                            <button type="button" onclick="updatecourse('{{$data->id}}')" class="btn btn-new-section ladda-button"  data-style="zoom-in" data-spinner-color="#e70e8a" id="updatecoursebutton">Save</button>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer modal-footer-1">
            </div>

            </form>

        </div>
    </div>
    <!-- End Modal -->
    <!-- End 26B-control-panel-my-courses-edit-section-1 -->

    <!-- Tab panes -->
    <!-- The Modal -->
    <div id="myModal-all" class="modal3">
      <!-- Modal content -->
        <div class="modal-content-all">
            <div class="container">
                <div class="margin-t-b">
                    <div class="row">  
                        <div class="col-sm-6">
                            <div class="vid-tital">
                                <h3>Programming for Beginners</h3>
                                <p>Section2, Lecture3</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="btn-popup-all">
                                <button class="btn btn-vid">BROWSE Q&A</button>
                                <button class="btn btn-vid">ADD SHORTLISTED</button>
                                <button class="btn btn-vid">GO TO DASHBOARD</button>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="video-play">
                <iframe src="https://www.youtube.com/embed/C0DPdy98e4c" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
                </iframe>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="required-line">
            <div class="chapter-title">
                <div class="row">
                    @if ($success = Session::get('success'))
                  <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 15px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;success</strong>&nbsp;&nbsp;{{ $success }}
                  </div>
                @endif
                @if ($warning = Session::get('warning'))
                  <div class="alert alert-info alert-danger" style="margin-top: 15px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;{{ $warning }}
                  </div>
                @endif

                    <div id="commonmsg"></div>
                    
                    <div class="alert alert-info alert-danger" style="display: none;" id="commonerrmsg">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;Something went wrong.please try again after sometime.
                    </div>

                    <div class="col-sm-6 ">
                        <h5> Course Content </h5>
                    </div>

                    <div class=" col-sm-6 course-topbtn">
                        <div class="wrap selected-radiocourse mt-xs-15">
                            <form>
                               
                               <fieldset>
                                     <div class="toggle">
                                        <input type="radio" class="toggle-input" name="joincreate" onchange="coursesStatus(this.value,'{{$data->id}}')" value="published" id="join" @if(isset($data->e_status) && $data->e_status=='published') checked @endif >
                                        <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Published</label>
                                        <input type="radio" class="toggle-input" name="joincreate" onchange="coursesStatus(this.value,'{{$data->id}}')" value="unpublished" id="create" @if(isset($data->e_status) && $data->e_status=='unpublished') checked @endif>
                                        <label for="create" class="toggle-label toggle-label-on" id="createLabel">Unpublished</label>
                                        <span class="toggle-selection"></span>
                                    </div>
                                </fieldset>

                            </form>
                        </div>
                        <button type="button" class="btn btn-editcourse btn-editcourse2" onclick="editCourseBasic('{{$data->id}}')"> Edit Course Basics
                        </button>
                    </div>
                </div>
            </div>
            @php
                //dump($data->section);    
            @endphp

            <?php 
                $totalv=0;
                if(isset($data->section) && count($data->section)){
                    foreach($data->section as $k=>$v){
                        if(isset($v['video']) && count($v['video'])){
                            foreach($v['video'] as $key=>$val){
                                $totalv = $totalv+1;
                            }
                        }
                    }
                }
                $totalvl=0;
            ?>


            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">    
            
            @if(isset($data->section) && count($data->section))
                @foreach($data->section as $k=>$v)
                    
                    <div class="panel panel-default">
                        <div class="panel-heading root-position" role="tab" id="heading{{$k}}">
                            <h4 class="panel-tab-content">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$k}}" aria-expanded="true" aria-controls="collapse{{$k}}">
                                  <i class="more-less glyphicon glyphicon-plus"></i>
                                    {{$k+1}}. {{$v['v_title']}}
                                </a>
                            </h4>
                            <?php $sid = $v['_id']; ?>
                            <div class="edit-details-courses"> <a href="javascript:;" onclick="editSection('{{$data->id}}','{{$sid}}')"> Edit </a>
                             <a href="javascript:;" onclick="deleteSection('{{$data->id}}','{{$sid}}')"> Delete </a> </div>

                        </div>

                        <div id="collapse{{$k}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$k}}">
                            <div class="panel-body panel-black">
                                
                                @if(isset($v['video']) && count($v['video']))
                                    @foreach($v['video'] as $key=>$val)

                                        <div class="coursevideo-list root-position">
                                            <div class="video-list-course">
                                                 @php
                                                    $videoUrl="";
                                                    if(Storage::disk('s3')->exists($val['v_video'])){
                                                        $videoUrl = \Storage::cloud()->url($val['v_video']);      
                                                    }
                                                    $totalvl=$totalvl+1;
                                                    $nextv=1;
                                                    if($totalvl==$totalv){
                                                        $nextv=0;    
                                                    }
                                                 @endphp
                                                 <a href="javascript:;" 
                                                    class="cls-video coursepadding" 
                                                    onclick="playVideo(this, '{{$videoUrl}}');" 
                                                    data-video-url="{{$videoUrl}}" 
                                                    data-play="0" 
                                                    data-sid="{{$val['_id']}}" 
                                                    data-next="{{$nextv}}" 
                                                    data-bcid="{{$v['_id']}}" 
                                                    >
                                                    <img src="{{url('public/Assets/frontend/images/symbol-coursevideolist.png')}}" class="coursevideo-player" alt="video-player">{{$val['v_title']}}
                                                    <span style="margin-top: -20px;"> {{$val['v_duration_min']}}:{{$val['v_duration_sec']}} </span>
                                                    @if(isset($val['i_preview']) && $val['i_preview']=="on")
                                                    <div class="fileset-preview-courses" style="margin-right: 52px;">
                                                        <span> PREVIEW  </span>
                                                    </div>
                                                    @endif
                                                </a>
                                            </div>
                                            {{-- @if(isset($val['i_preview']) && $val['i_preview']=="on")
                                            <a href="javascript:;" 
                                                    class="cls-video1 coursepadding" 
                                                    onclick="playVideo(this, '{{$videoUrl}}');" 
                                                    data-video-url="{{$videoUrl}}" 
                                                    data-play="0" 
                                                    data-sid="{{$val['_id']}}" 
                                                    data-bcid="{{$v['_id']}}" 
                                                    >
                                            <div class="fileset-preview-courses" style="margin-right: 52px;">
                                                <span> PREVIEW  </span>
                                            </div>
                                            </a>
                                            @endif    --}}
                                            
                                            @if(isset($val['v_doc']) && $val['v_doc']!="")
                                            <?php 
                                                $doctype = explode(".", $val['v_doc']);
                                                if(isset($doctype[1])){
                                                    $doctype = $doctype[1];     
                                                }else{
                                                    $doctype = ""; 
                                                }
                                            ?>
                                            <?php 
                                                $docUrl="";    
                                                if(Storage::disk('s3')->exists($val['v_doc'])){
                                                    $docUrl = \Storage::cloud()->url($val['v_doc']);      
                                                }
                                            ?>
                                            <div style="clearfix"></div>
                                            <div class="fileset-preview-courses">
                                                <span> <a href="{{$docUrl}}" download target="_blank" >{{strtoupper($doctype)}}</a> </span>
                                            </div>
                                            @endif

                                        </div>
                                    @endforeach
                                @endif        

                            </div>
                        </div>
                    </div>

                @endforeach
            @endif    
            </div>
            <div class="text-center-xs">
                <button type="button" class="btn btn-new-coursesection" data-toggle="modal" data-target="#addnewsection"> Add New Section </button>
            </div>
        </div>
    </div>
<!--26b-control-panel-my-courses-overviwe-->
@stop

@section('js')
<script src="{{url('public/Assets/frontend/js/videre.js')}}"></script>
<script src="{{url('public/Assets/frontend/js/app.js')}}"></script>
<script type="text/javascript">

        function playVideo( curr, videoUrl ){
            jQuery('#player').html('');
            jQuery('#player').attr('class','');
            jQuery('#player').attr('style','');
            
            jQuery('.cls-video').attr('data-play', 0);
            jQuery(curr).attr('data-play', 1);
            
            jQuery('.top-video-title').html( jQuery(curr).find('.video-title').html() );
            
            var nextv = jQuery(curr).attr('data-next');
            if(nextv=="0"){
                jQuery("#nextvideohtml").css("display","none");        
            }
            
            jQuery("#videoplaymodal").modal("show");
            
            jQuery('#player').videre({
                video: {
                    quality: [{
                        label: '720p',
                        src: videoUrl,
                        width:'100%'
                    }, {
                        label: '360p',
                        src: videoUrl+'?SD'
                    }, {
                        label: '240p',
                        src: videoUrl+'?SD'
                    }],
                    title: 'jQueryScript.Net'
                },
                dimensions: 1280
            });
        }

        function readURL(input) {
            
            if (input.files && input.files[0]) {
                
                if(input.files[0].size > 2097152) {
                    $('#v_instructor_image_base').parent("div").find(".videoerrsize").css("display","block");
                    return false;
                }
                $('#v_instructor_image_base').parent("div").find(".videoerrsize").css("display","none");    

                var reader = new FileReader();
                console.log(reader);

                reader.onload = function(e) {
                    $('#v_instructor_image_base')
                        .attr('src', e.target.result)
                        .width(180)
                        .height(150);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function playNext(){
            var curr = '';
            var nextUrl = '';
            var getNext = 0;
            var cnt = 0;
            jQuery('.cls-video').each(function(){
                if( !nextUrl ){
                    if( getNext ){
                        nextUrl = jQuery(this).attr('data-video-url');
                        jQuery(this).attr('data-play',1);
                        curr = jQuery(this);
                    }
                    if( jQuery(this).attr('data-play') == 1 || jQuery(this).attr('data-play') == '1' ){
                        getNext = 1;
                        jQuery(this).attr('data-play',0);
                    }
                }
            });
            if( !nextUrl ){
                curr = jQuery('.cls-video').first();
                nextUrl = curr.attr('data-video-url');
                curr.attr('data-play',1);
            }
            if( nextUrl ){
                playVideo( curr, nextUrl );
            }
        }

        function coursesStatus(data="",id=""){
          
            var actionurl = "{{url('courses/updatestatus')}}";
            var formdata = "id="+id+"&e_status="+data;
          
            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                   
                    var obj = jQuery.parseJSON(res);
                    
                    if(obj['status']==1){
                        $("#commonmsg").html(obj['msg']);
                    }else{
                        $('#commonmsg').html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    $("#commonerrmsg").css("display","inline");
                }
            });
        }

        function workvideo(data){
            var filename = $('#'+data).val().split('\\').pop();
            var lastIndex = filename.lastIndexOf("\\");   
            var myarray =filename.split('.');
            var last = myarray[myarray.length-1];
            format = ["mp4", "mkv", "webm","mov","m4v"];
            if(jQuery.inArray(last, format) == -1) {
                $('#'+data).val("");
                $('#text'+data).val("");
                $('#text'+data).parent("div").find(".videoerr").css("display","block");
                return false;
            } 

            var fp = $('#'+data);
            var lg = fp[0].files.length;
            var items = fp[0].files;
            var fileSize = 0;
            if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; 
               }
               if(fileSize > 100715200) {
                    $('#'+data).val("");
                    $('#text'+data).val("");
                    $('#text'+data).parent("div").find(".videoerrsize").css("display","block");
                    $('#'+data).val('');
                    return false;
               }
            }

            $('#text'+data).val(filename);
            $('#text'+data).parent("div").find(".videoerr").css("display","none");
            $('#text'+data).parent("div").find(".videoerrsize").css("display","none");

        }

        function workvideodoc(data){
            
            var filename = $('#'+data).val().split('\\').pop();
            var lastIndex = filename.lastIndexOf("\\");   
            var myarray =filename.split('.');
            var last = myarray[myarray.length-1];
            format = ["pdf", "doc", "docx","txt","xlsx","xls","pptx"];
            if(jQuery.inArray(last, format) == -1) {
                $('#'+data).val("");
                $('#text'+data).val("");
                $('#text'+data).parent("div").find(".docerr").css("display","block");
                return false;
            }

            var fp = $('#'+data);
            var lg = fp[0].files.length;
            var items = fp[0].files;
            var fileSize = 0;
            if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; 
               }
               if(fileSize > 10485760) {
                    $('#'+data).val("");
                    $('#text'+data).val("");
                    $('#text'+data).parent("div").find(".videoerrsize").css("display","block");
                    $('#'+data).val('');
                    return false;
               }
            }

            $('#text'+data).val(filename);
            $('#text'+data).parent("div").find(".docerr").css("display","none");
            $('#text'+data).parent("div").find(".videoerrsize").css("display","none");

        }

        function workvideosection(data){
            var filename = $('#videoupload'+data).val().split('\\').pop();
            var lastIndex = filename.lastIndexOf("\\");   
            
            var myarray =filename.split('.');
            var last = myarray[myarray.length-1];
            format = ["mp4", "mkv", "webm","mov","m4v"];
            if(jQuery.inArray(last, format) == -1) {
                $('#videoupload'+data).val("");
                $('#textvideoupload'+data).val("");
                $('#textvideoupload'+data).parent("div").find(".videoerr").css("display","block");
                return false;
            }   

            var fp = $('#videoupload'+data);
            var lg = fp[0].files.length;
            var items = fp[0].files;
            var fileSize = 0;
            if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; 
               }
               if(fileSize > 100715200) {
                    $('#videoupload'+data).val("");
                    $('#textvideoupload'+data).val("");
                    $('#textvideoupload'+data).parent("div").find(".videoerrsize").css("display","block");
                    return false;
               }
            }


            $('#textvideoupload'+data).val(filename);
            $('#textvideoupload'+data).parent("div").find(".videoerr").css("display","none");
            $('#textvideoupload'+data).parent("div").find(".videoerrsize").css("display","none");

        }

        function workdocsection(data){
            var filename = $('#docupload'+data).val().split('\\').pop();
            var lastIndex = filename.lastIndexOf("\\");   
            
            var myarray =filename.split('.');
            var last = myarray[myarray.length-1];
            //format = ["pdf", "doc", "docx","txt"];
            format = ["pdf", "doc", "docx","txt","xlsx","xls","pptx"];
            if(jQuery.inArray(last, format) == -1) {
                $('#docupload'+data).val("");
                $('#textdocupload'+data).val("");
                $('#textdocupload'+data).parent("div").find(".docerr").css("display","block");
                return false;
            }


            var fp = $('#docupload'+data);
            var lg = fp[0].files.length;
            var items = fp[0].files;
            var fileSize = 0;
            if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; 
               }
               if(fileSize > 10485760) {
                    $('#docupload'+data).val("");
                    $('#textdocupload'+data).val("");
                    $('#textdocupload'+data).parent("div").find(".videoerrsize").css("display","block");
                    return false;
               }
            }



            $('#textdocupload'+data).val(filename);
            $('#textdocupload'+data).parent("div").find(".docerr").css("display","none");
            $('#textdocupload'+data).parent("div").find(".videoerrsize").css("display","none");

        }

        $(document).on("click", ".rem", function() { 
            $(this).parents(".sectionvideoremove").remove();
        });
        $(document).on("click", ".remdoc", function() { 
            $(this).parents(".sectiondocremove").remove();
        });

        $(document).on("click", ".removesection", function() { 
            $(this).parents(".sectionremove").remove();
        });


         $(document).on("click", ".remsectionvideo", function() { 
            $(this).parents(".sectionvideoremove").remove();
        });


      
        function addmorevideo(section="",sectiontitle="1"){

            
            var videouploadcnt = $("#videouploadcnt"+section).val();
            $("#videouploadcnt"+section).val(parseInt(videouploadcnt)+1);
            var uplodcnt = section+videouploadcnt;
            var arrayindex = videouploadcnt;
            var i = parseInt(section)-1;
            if(section==1){
                var i=0;    
            }
            var appendstr="";            
            appendstr+='<div class="row-form-field sectionvideoremove" style="border-top: 1px solid #c7c7c7;padding-bottom: 24px;">';
            appendstr+='<div class="row" style="margin-top:25px">';
            
            appendstr+='<div class="col-sm-12 col-xs-12">';
            appendstr+='<button type="button" class="btn btn-add-remove remsectionvideo" style="margin-top: 3px;float:right">Remove video</button>';
            appendstr+='</div>';
            
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Section '+sectiontitle+' Video Title';
            appendstr+='</label>';
            appendstr+='<input type="text" class="form-control input-field" name="section['+i+'][video]['+arrayindex+'][v_title]" required>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Section '+sectiontitle+' Video';
            appendstr+='</label>';
            appendstr+='<input type="text" class="form-control input-field" id="textvideoupload'+uplodcnt+'" readonly>';
            appendstr+='<span>Supported formats: mp4, mkv, webm, mov, m4v<br></span>';
            appendstr+='<span>Max file size: 100 MB <br></span>';
            appendstr+='<span class="videoerr" style="color: red">Video format not supported.</span>';
            appendstr+='<span class="videoerrsize" style="color: red">File size should not be more than 100 MB<br></span>';

            appendstr+='<div class="col-xs-12">';
            appendstr+='<div class="text-right">';
            appendstr+='<input type="file" class="browsebutton" accept=".mp4, .mkv, .webm, .mov, .m4v" required name="section['+i+'][video]['+arrayindex+'][v_video]"  id="videoupload'+uplodcnt+'" onchange="workvideosection('+uplodcnt+');" />';
            appendstr+='<label id="fileupload" class="btn-editdetail" for="videoupload'+uplodcnt+'">Browser</label>';
            appendstr+='</div>';
            appendstr+='</div>';


            appendstr+='<div class="checkbox-service">';
            appendstr+='<label class="checkbox check-boxright">';
            appendstr+='<input type="hidden" name="section['+i+'][video]['+arrayindex+'][i_preview]" value="off">';
            appendstr+='<input type="checkbox" name="section['+i+'][video]['+arrayindex+'][i_preview]">';
            appendstr+='<span class="subfield-name"> ';
            appendstr+='Preview this course';
            appendstr+='</span>';
            appendstr+='</label>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Video Duration';
            appendstr+='</label>';
            appendstr+='<div class="row">';
            appendstr+='<div class="col-md-6">';
            appendstr+='<input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field col-md-6" name="section['+i+'][video]['+arrayindex+'][v_duration_min]" value="00" autocomplete="off" /><div class="text-center">Minutes</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-md-6">';
            appendstr+='<input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true"  class="form-control input-field col-md-6" name="section['+i+'][video]['+arrayindex+'][v_duration_sec]" value="00" autocomplete="off" /><div class="text-center">Seconds</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Section '+sectiontitle+' Doc';
            appendstr+='</label>';
            appendstr+='<input type="text" class="form-control input-field" id="textdocupload'+uplodcnt+'" readonly>';
            appendstr+='<span>Supported formats: pdf, doc, docx, txt, xlsx, xls,pptx<br></span>';
            appendstr+='<span>Max file size: 10 MB <br></span>';
            appendstr+='<span class="docerr" style="color: red">Document format not supported.<br></span>';
            appendstr+='<span class="videoerrsize" style="color: red">File size should not be more than 10 MB<br></span>';

            appendstr+='<div class="col-xs-12">';
            appendstr+='<div class="text-right">';
            appendstr+='<input type="file" accept=".pdf, .docx, .txt, .doc, .xlsx, .xls, .pptx" class="browsebutton" name="section['+i+'][video]['+arrayindex+'][v_doc]" id="docupload'+uplodcnt+'" onchange="workdocsection('+uplodcnt+');" />';
            appendstr+='<label id="fileupload" class="btn-editdetail" for="docupload'+uplodcnt+'">Browser';
            appendstr+='</label>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-12 col-xs-12">';
            appendstr+='<div class="text-right">';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            $("#sectionvideoedit"+section).append(appendstr);

        }

        function addmorevideochapter(section="",sectiontitle=""){

            
            var videouploadcnt = $("#addchaptervideouploadcnt"+section).val();
            $("#addchaptervideouploadcnt"+section).val(parseInt(videouploadcnt)+1);
            var uplodcnt = section+videouploadcnt;
            var arrayindex = videouploadcnt;
            var i = parseInt(section)-1;
            if(section==1){
                var i=0;    
            }
            var appendstr="";            
            appendstr+='<div class="row-form-field sectionvideoremove" style="border-top: 1px solid #c7c7c7;padding-bottom: 24px;">';
            appendstr+='<div class="row" style="margin-top:25px">';
            
            appendstr+='<div class="col-sm-12 col-xs-12">';
            appendstr+='<button type="button" class="btn btn-add-remove remsectionvideo" style="margin-top: 3px;float:right">Remove video</button>';
            appendstr+='</div>';
            
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Section '+sectiontitle+' Video Title';
            appendstr+='</label>';
            appendstr+='<input type="text" class="form-control input-field" name="section['+i+'][video]['+arrayindex+'][v_title]" required>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Section '+sectiontitle+' Video';
            appendstr+='</label>';
            appendstr+='<input type="text" class="form-control input-field" id="textvideoupload'+uplodcnt+'" readonly>';

            appendstr+='<span>Supported formats: mp4, mkv, webm, mov, m4v<br></span>';
            appendstr+='<span>Max file size: 100 MB <br></span>';
            appendstr+='<span class="videoerr" style="color: red">Video format not supported.</span>';
            appendstr+='<span class="videoerrsize" style="color: red">File size should not be more than 100 MB<br></span>';

            appendstr+='<div class="col-xs-12">';
            appendstr+='<div class="text-right">';
            appendstr+='<input type="file" class="browsebutton" accept=".mp4, .mkv, .webm, .mov, .m4v" required name="section['+i+'][video]['+arrayindex+'][v_video]"  id="videoupload'+uplodcnt+'" onchange="workvideosection('+uplodcnt+');" />';
            appendstr+='<label id="fileupload" class="btn-editdetail" for="videoupload'+uplodcnt+'">Browser</label>';
            appendstr+='</div>';
            appendstr+='</div>';


            appendstr+='<div class="checkbox-service">';
            appendstr+='<label class="checkbox check-boxright">';
            appendstr+='<input type="hidden" name="section['+i+'][video]['+arrayindex+'][i_preview]" value="off">';
            appendstr+='<input type="checkbox" name="section['+i+'][video]['+arrayindex+'][i_preview]">';
            appendstr+='<span class="subfield-name"> ';
            appendstr+='Preview this course';
            appendstr+='</span>';
            appendstr+='</label>';
            appendstr+='</div>';
            
            

            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Video Duration';
            appendstr+='</label>';
            appendstr+='<div class="row">';
            appendstr+='<div class="col-md-6">';
            appendstr+='<input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field col-md-6" name="section['+i+'][video]['+arrayindex+'][v_duration_min]" value="00"><div class="text-center">Minutes</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-md-6">';
            appendstr+='<input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true"  class="form-control input-field col-md-6" name="section['+i+'][video]['+arrayindex+'][v_duration_sec]" value="00"><div class="text-center">Seconds</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Section '+sectiontitle+' Doc';
            appendstr+='</label>';
            appendstr+='<input type="text" class="form-control input-field" id="textdocupload'+uplodcnt+'" readonly>';
            
            appendstr+='<span>Supported formats: pdf, doc, docx, txt, xlsx, xls, pptx<br></span>';
            appendstr+='<span>Max file size: 10 MB <br></span>';
            appendstr+='<span class="docerr" style="color: red">Document format not supported.<br></span>';
            appendstr+='<span class="videoerrsize" style="color: red">File size should not be more than 10 MB<br></span>';

            appendstr+='<div class="col-xs-12">';
            appendstr+='<div class="text-right">';
            appendstr+='<input type="file" accept=".pdf, .docx, .txt, .doc, .xlsx, .xls, .pptx" class="browsebutton" name="section['+i+'][video]['+arrayindex+'][v_doc]" id="docupload'+uplodcnt+'" onchange="workdocsection('+uplodcnt+');" />';
            appendstr+='<label id="fileupload" class="btn-editdetail" for="docupload'+uplodcnt+'">Browser';
            appendstr+='</label>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-12 col-xs-12">';
            appendstr+='<div class="text-right">';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            $("#addchaptersectionvideo"+section).append(appendstr);

        }

        function addsection(id=""){
          
            var actionurl = "{{url('courses/add-new-section')}}";
            var formValidFalg = $("#addnewsectionform").parsley().validate('');

            if(formValidFalg){


                var ttlSize=9;
                $('#addnewsectionform').find('input[type=file]').each(function() {
                    if(this.files[0]){
                        ttlSize = parseInt(ttlSize)+parseInt(this.files[0].size);
                    }
                }); 
                ttlSize = parseInt(ttlSize)/1000;
                LoaderPercentStart(ttlSize);
                //document.getElementById('load').style.visibility="visible";
                var formData = new FormData($('#addnewsectionform')[0]);
               
                $.ajax({
                    processData: false,
                    contentType: false,
                    type    : "POST",
                    url     : actionurl,
                    data    : formData,
                    success : function( res ){
                        //document.getElementById('load').style.visibility='hidden';
                        LoaderPercentStop();
                        var obj = jQuery.parseJSON(res);
                      
                        if(obj['status']==1){
                            window.location = "{{url('courses/section')}}/"+id;
                        }else{
                            $("#commonpopupmsg").html(obj['msg']);
                        }

                    },
                    error: function ( jqXHR, exception ) {
                        //document.getElementById('load').style.visibility='hidden';
                        LoaderPercentStop();
                        $("#commonpopupmsg").html(obj['msg']);
                    }
                });
            }    
        }

        function editSection(courseid="",id=""){
            
            //var actionurl = "{{url('courses/get-section')}}";
            var actionurl = "{{url('courses/edit-section-detail')}}";
            var formData = "i_course_id="+courseid+"&i_section_id="+id;

            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        
                        $("#i_section_id").val(id);
                        $("#editSectionstr").html(obj['sectionstr']);
                        $("#editcoursesection").modal("show");

                    }else{
                        $("#commonpopupmsg").html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    $("#commonerrmsg").css("display","inline");
                }
            });
        }

        $(document).on("click", ".videoeditrem", function() { 
            $(this).parents(".sectionvideoeditremove").remove();
        });


        function workvideosectionedit(data){
            var filename = $('#videouploadedit'+data).val().split('\\').pop();
            var lastIndex = filename.lastIndexOf("\\");   
            $('#textvideouploadedit'+data).val(filename);
        }


        function editmorevideo(){
            
            var videouploadcnt = $("#videouploadcntedit").val();
            $("#videouploadcntedit").val(parseInt(videouploadcnt)+1);
            
            var uplodcnt = 1+videouploadcnt;
          
            var appendstr="";
            appendstr+='<div class="row-form-field sectionvideoeditremove">';
            appendstr+='<div class="row">';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">Section Video Title</label>';
            appendstr+='<input type="text" class="form-control input-field" name="section[0][video][v_title][]" required>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">Section Video</label>';
            appendstr+='<input type="text" class="form-control input-field" id="textvideouploadedit'+uplodcnt+'">';
            appendstr+='<div class="checkbox-service">';
            appendstr+='<label class="checkbox check-boxright">';
            appendstr+='<input type="hidden" name="section[0][video][i_preview][]" value="off" >'; 
            appendstr+='<input type="checkbox" name="section[0][video][i_preview][]">';
            appendstr+='<span class="subfield-name"> Preview this course</span>';
            appendstr+='</label>';
            appendstr+='</div>';
            appendstr+='<div class="clearfix"></div>';
            appendstr+='<div class="col-xs-6">';
            appendstr+='<div class="text-center">';
            appendstr+='<input type="file" class="browsebutton" name="section[0][video][v_video][]"  id="videouploadedit'+uplodcnt+'" required onchange="workvideosectionedit('+uplodcnt+');" />';
            appendstr+='<label id="fileupload" class="btn-editdetail" for="videouploadedit'+uplodcnt+'">Browser</label>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-xs-6">';
            appendstr+='<div class="text-center">';
            appendstr+='<button type="button" class="btn btn-add-remove videoeditrem" style="margin-top: 14px;" >Remove</button>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            $("#sectionvideoedit").append(appendstr);
        }

        $(document).on("click", ".doceditrem", function() { 
            $(this).parents(".doceditremove").remove();
        });

        function uploadDocEdit(data){
           
            var filename = $('#docuploadedit'+data).val().split('\\').pop();
            var lastIndex = filename.lastIndexOf("\\");   
            $('#textdocuploadedit'+data).val(filename);
        }

        function addmoredocedit(){

            var docuploadcntedit = $("#docuploadcntedit").val();
            $("#docuploadcntedit").val(parseInt(docuploadcntedit)+1);
            
            var uplodcnt = 1+docuploadcntedit;
            
            var appendstr="";
            appendstr+='<div class="row-form-field sectiondocremove">';
            appendstr+='<div class="row">';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">Section 1 Doc Title</label>';
            appendstr+='<input type="text" class="form-control input-field" name="section[0][doc][v_title][]" required>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">Section 1 Doc</label>';
            appendstr+='<input type="text" class="form-control input-field" id="textdocuploadedit'+uplodcnt+'">';
            appendstr+='<div class="col-xs-6">';
            appendstr+='<div class="text-center">';
            appendstr+='<input type="file" class="browsebutton" name="section[0][doc][v_doc][]" id="docuploadedit'+uplodcnt+'" required onchange="uploadDocEdit('+uplodcnt+');" />';
            appendstr+='<label id="fileupload" class="btn-editdetail" for="docuploadedit'+uplodcnt+'">Browser';
            appendstr+='</label>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-xs-6">  ';
            appendstr+='<div class="text-center">';
            appendstr+='<button type="button" class="btn btn-add-remove remdoc" style="margin-top: 14px;">Remove';
            appendstr+='</button>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            $("#sectiondocedit").append(appendstr);

        }

        function updatesection(id=""){
          
            var actionurl = "{{url('courses/edit-section')}}";
            var formValidFalg = $("#editsectionform").parsley().validate('');

            var ttlSize=9;
            $('#editsectionform').find('input[type=file]').each(function() {
                if(this.files[0]){
                    ttlSize = parseInt(ttlSize)+parseInt(this.files[0].size);
                }
            }); 
            ttlSize = parseInt(ttlSize)/1000;
            LoaderPercentStart(ttlSize);
            
            var id=$("#i_course_id_edit").val();
            if(formValidFalg){
                // document.getElementById('load').style.visibility="visible";
                // var l = Ladda.create(document.getElementById('editupdatesection'));
                // l.start();
                LoaderPercentStart(ttlSize);
                var formData = new FormData($('#editsectionform')[0]);
                $.ajax({
                    processData: false,
                    contentType: false,
                    type    : "POST",
                    url     : actionurl,
                    data    : formData,
                    success : function( res ){
                        
                        //document.getElementById('load').style.visibility='hidden';
                        //l.stop();
                        LoaderPercentStop();
                        var obj = jQuery.parseJSON(res);
                        
                        if(obj['status']==1){
                            window.location = "{{url('courses/section')}}/"+id;
                        }else{
                            $("#commonpopupmsg").html(obj['msg']);
                        }

                    },
                    error: function ( jqXHR, exception ) {
                        //document.getElementById('load').style.visibility='hidden';
                        //l.stop();
                        $("#commonerrmsg").css("display","inline");
                    }
                });
            }    
        }

        function editCourseBasic(courseid=""){
            
            var actionurl = "{{url('courses/get-course')}}";
            var formData = "i_course_id="+courseid;
            document.getElementById('load').style.visibility="visible";
            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    document.getElementById('load').style.visibility='hidden';
                    var obj = jQuery.parseJSON(res);
                    
                    if(obj['status']==1){
                        $("#v_title_base").val(obj['data']['v_title']);
                        $("#v_sub_title_base").val(obj['data']['v_sub_title']);
                        $("#v_author_name_base").val(obj['data']['v_author_name']);
                        $("#i_language_id_base").val(obj['data']['i_language_id']);
                        $("#i_level_id_base").val(obj['data']['i_level_id']);
                        $("#i_duration_base").val(obj['data']['i_duration']);
                        $("#i_category_id_base").val(obj['data']['i_category_id']);
                        $("#l_description_base").val(obj['data']['l_description']);
                        $("#v_requirement_base").val(obj['data']['v_requirement']);
                        $("#l_instructor_details_base").val(obj['data']['l_instructor_details']);
                        $("#f_price_base").val(obj['data']['f_price']);
                        $("#v_instructor_image_base").attr("src",obj['v_instructor_image']);
                        $("#v_course_thumbnail_base").attr("src",obj['v_course_thumbnail']);
                        $("#textintrovideoupload1").val(obj['data']['l_video']);
                        $("#editcoursedetail").modal("show");
                        if(obj['data']['f_price']==0){
                            $("#v_course_free").attr("checked","true");    
                        }

                    }else{
                        $("#commonpopupmsg").html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                    $("#commonerrmsg").css("display","inline");
                }
            });
        }

        
         function updatecourse(id=""){
          
            var actionurl = "{{url('courses/update-course')}}";
            var formValidFalg = $("#editcourseform").parsley().validate('');

            if(formValidFalg){
                
                // document.getElementById('load').style.visibility="visible";
                // var l = Ladda.create(document.getElementById('editupdatesection'));
                //l.start();

                var ttlSize=9;
                $('#editcourseform').find('input[type=file]').each(function() {
                    if(this.files[0]){
                        ttlSize = parseInt(ttlSize)+parseInt(this.files[0].size);
                    }
                }); 
                ttlSize = parseInt(ttlSize)/1000;
                LoaderPercentStart(ttlSize);

                var formData = new FormData($('#editcourseform')[0]);
                var desc = CKEDITOR.instances['l_description_base'].getData();
                formData.append('l_description', desc);

                $.ajax({
                    processData: false,
                    contentType: false,
                    type    : "POST",
                    url     : actionurl,
                    data    : formData,
                    success : function( res ){
                        // document.getElementById('load').style.visibility='hidden';
                        // l.stop();
                        LoaderPercentStop();

                        var obj = jQuery.parseJSON(res);
                        if(obj['status']==1){
                            $("#commonmsg").html(obj['msg']);
                            $("#editcoursedetail").modal("hide");
                        }else{
                            $("#commonmsg").html(obj['msg']);
                        }
                    },
                    error: function ( jqXHR, exception ) {
                        // document.getElementById('load').style.visibility='hidden';
                        // l.stop();
                        LoaderPercentStop();
                        $("#commonerrmsg").css("display","inline");
                    }
                });
            }    
        }

        function readURLThumb(input) {

            if (input.files && input.files[0]) {

                if(input.files[0].size > 2097152) {
                    $('#fileupload-example-5').parent("div").find(".videoerrsize").css("display","block");
                    return false;
                }
                $('#fileupload-example-5').parent("div").find(".videoerrsize").css("display","none");    

                var reader = new FileReader();
                reader.onload = function(e) {
                    
                    $('#v_course_thumbnail_base')
                        .attr('src', e.target.result)
                        .width(180)
                        .height(150);
                };
                reader.readAsDataURL(input.files[0]);
            }

        }

        function pricevalidation(){
            if($("#v_course_free").prop('checked') == true){
                $("#f_price_base").removeAttr("required");
                $("#f_price").removeAttr("min");
                $("#f_price_base").val(0);
            }else{
                $("#f_price_base").attr("required","true");
                $("#f_price_base").attr("min","5");
            }

        }


        

</script>

<script>

var $topLoader = $("#topLoader").percentageLoader({width: 256, height: 256, controllable : true, progress : 0.5, onProgressUpdate : function(val) {
  $topLoader.setValue(Math.round(val * 100.0));
}});
var topLoaderRunning = false;
          
function LoaderPercentStart(total){
    if (topLoaderRunning) {
      return;
    }
    topLoaderRunning = true;
    $topLoader.setProgress(0);
    $topLoader.setValue('0kb');
    var kb = 0;
    var totalKb = total;
    
    var animateFunc = function() {
      kb += 17;
      $topLoader.setProgress(kb / totalKb);
      $topLoader.setValue(kb.toString() + 'kb');
      
      if (kb < totalKb) {
        setTimeout(animateFunc, 25);
      } else {
        topLoaderRunning = false;
      }
    }
    $(".topLoaderinner").css("display","block");
    $("#topLoader").append("<p style='font-size:20px'>This could take sometime...</p>");
    $("#topLoader").append("<p style='font-size:20px'>Please do not refresh the page.</p>");
    setTimeout(animateFunc, 25);
}

function LoaderPercentStop(){    
    $(".topLoaderinner").css("display","none");
}

function closevideopopup(){
    jQuery('#player').html('');
    jQuery('#videoplaymodal').modal('hide');
}

$( document ).ready(function() { 
    CKEDITOR.replace("l_description");
});

function deleteSection(cid,csid) {
    $("#CDID").val(cid);
    $("#CSDID").val(csid);
    $("#deleteModalCourse").modal("show");
}

function deleteActionCourse(){
    var cdid = $("#CDID").val();
    var csdid = $("#CSDID").val();
    var actionurl = "{{url('courses/section-delete')}}/"+cdid+"/"+csdid;
    window.location.href = actionurl;
}


              
</script>

@stop

