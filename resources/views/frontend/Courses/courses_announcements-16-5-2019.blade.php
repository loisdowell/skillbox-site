@extends('layouts.frontend')

@section('content')

 <style type="text/css">
    .letter-text-myprifile {
        text-align: center;
        font-size: 34px;
        padding: 10px 0px;
        color: rgb(231, 14, 138);
    }
    .new-seller-find {border-radius: 20px;
    margin-right: 10px;}
    .new-submit-live {height: 34px;}
    .new-submit-comment{display: inline-flex;width: 100%}
</style>
   
   <!--26b-control-panel-my-courses-overviwe-->
    <div class="title-controlpanel controlpanel_setting_icon">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> My Courses Details </h1>
                </div>
                <div class="right-symbol">
                    <a href="{{url('courses/settings')}}/{{$data->id}}" class="setting-course">
                        <img src="{{url('public/Assets/frontend/images/setting-course.png')}}" class="setting-course" alt="" />
                    </a>
                    <a href="{{url('courses/messages')}}/{{$data->id}}"> <img src="{{url('public/Assets/frontend/images/mail.png')}}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-detail tabdetailplace">
        <div class="container">
            <ul class="nav nav-pills nav-pills-new">
                <li><a href="{{url('courses/view')}}/{{$data->id}}">Overview </a></li>
                <li><a href="{{url('courses/section')}}/{{$data->id}}">Course Sections </a></li>
                <li><a href="{{url('courses/qa')}}/{{$data->id}}">Q&A <span class="place-badge">{{isset($total['qa']) ? $total['qa'] : 0}}</span> </a></li>
                <li><a href="{{url('courses/reviews')}}/{{$data->id}}"> Course Reviews <span class="place-course">{{isset($total['reviews']) ? $total['reviews'] : 0}}</span> </a></li>
                <li class="active"><a href="{{url('courses/announcements')}}/{{$data->id}}">Announcements <span class="place-annou">{{isset($total['announcements']) ? $total['announcements'] : 0}}</span> </a></li>
            </ul>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="addAnnouncement" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content modal-content-1">
                <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> New Announcement </h4>
                </div>
                <form role="form" method="POST" name="addannform" id="addannform" data-parsley-validate enctype="multipart/form-data" >
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <input type="hidden" name="i_courses_id" value="{{$data->id}}">
                            <div class="col-xs-12">
                                <div class="text-box-containt box-containt-que">
                                    <label> Announcement Title </label>
                                    <input type="text" name="v_title" class="form-control title-que">
                                </div>

                                <div class="text-box-containt">
                                    <label> Announcement Text </label>
                                    <textarea rows="7" class="form-control" name="l_announcements"></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button class="btn btn-Submit-pop" type="button" onclick="addAnnouncement()">Submit Announcement</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>

        </div>
    </div>
    <!-- End Modal -->

     <!-- Modal -->
    <div class="modal fade" id="EditAnnouncement" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content modal-content-1">
                <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> Edit Announcement </h4>
                </div>
                <form role="form" method="POST" name="editannform" id="editannform" data-parsley-validate enctype="multipart/form-data" >
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <input type="hidden" name="i_courses_announcements_id" id="i_courses_announcements_id" value="">
                            <div class="col-xs-12">
                                <div class="text-box-containt box-containt-que">
                                    <label> Announcement Title </label>
                                    <input type="text" name="v_title" class="form-control title-que" id="v_title_edit">
                                </div>

                                <div class="text-box-containt">
                                    <label> Announcement Text </label>
                                    <textarea rows="7" class="form-control" name="l_announcements" id="l_announcements_edit"></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button class="btn btn-Submit-pop" type="button" onclick="updateAnnouncement()">Submit Announcement</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </form>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>

        </div>
    </div>
    <!-- End Modal -->

    <!-- Tab panes -->
    <div class="container">

        <!-- 26B-control-panel-my-courses-announcement-seller -->
        <div class="timeline-title">
            <div class="row">
                <div class="col-sm-4">
                    <h3>Announcements Timeline</h3>
                </div>
                <div class=" col-sm-8">
                    <div class="wrap selected-radiocourse mt-xs-15">
                        <form>
                            <fieldset>
                                <div class="toggle">
                                    <input type="radio" class="toggle-input" name="joincreate" onchange="coursesStatus(this.value,'{{$data->id}}')" value="published" id="join" @if(isset($data->e_status) && $data->e_status=='published') checked @endif >
                                    <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Published</label>
                                    <input type="radio" class="toggle-input" name="joincreate" onchange="coursesStatus(this.value,'{{$data->id}}')" value="unpublished" id="create" @if(isset($data->e_status) && $data->e_status=='unpublished') checked @endif>
                                    <label for="create" class="toggle-label toggle-label-on" id="createLabel">Unpublished</label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <button type="button" class="btn btn-new-annou btn-center-new" data-toggle="modal" data-target="#addAnnouncement"> Add New Announcement
                    </button>
                </div>
            </div>
        </div>
        <div id="commonmsg"></div>
         <div class="alert alert-info alert-danger" style="display: none;" id="commonerr">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;Something went wrong.Please try again after sometime.
         </div>            

        <div class="Announcements-content">
            @if(isset($coursesAnnouncements) && count($coursesAnnouncements))
                @foreach($coursesAnnouncements as $k=>$v)
                    <div class="accordion-final-announc">
                        <div class="accordion accordion-announcement">
                            <div class="accordion-toggle1 @if($k==0) active @endif">
                                <div class="Announcements-find edit-position">
                                    <div class="Announcements-man">
                                        <?php
                                            $imgdata = \App\Helpers\General::userroundimage($curentuser);
                                            echo $imgdata;
                                        ?> 
                                    </div>
                                    <div class="Announcements-name">
                                        <p>{{$curentuser['v_name']}}</p>
                                        <p><span><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($v->d_added))->diffForHumans() ?></span></p>
                                    </div>

                                    <div class="edit-button-seller">
                                        <?php 
                                            $currentdate=date("Y-m-d H:i:s");
                                            $hourdiff = round((strtotime($currentdate) - strtotime($v->d_added))/3600, 1);
                                        ?>
                                        @if($hourdiff<=24)    
                                        <button type="button" class="btn btn-new-annou" onclick="getAnnouncementData('{{$v->id}}')" >Edit Announcement</button>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <div class="accordion-content" @if($k==0) style="display: block" @endif>

                                <div class="Announcements-text Announcements-space" id="announcementscomments{{$v->id}}">
                                    
                                    <div class="Announcements-text-all">
                                        <h3 id="v_title_{{$v->id}}" >{{isset($v->v_title) ? $v->v_title : ''}}</h3>
                                        <p id="l_announcements_{{$v->id}}">{{isset($v->l_announcements) ? $v->l_announcements : ''}}</p>
                                    </div>
                                        
                                    @if(isset($v->l_comments) && count($v->l_comments))        
                                        @foreach($v->l_comments as $key=>$val)
                                            <div class="Announcements-second-man">
                                                <div class="Announcements-find">
                                                    <div class="Announcements-man">
                                                        <?php
                                                            $imgdata="";
                                                            if(isset($userList[$val['i_user_id']])){
                                                                $imgdata = \App\Helpers\General::userroundimage($userList[$val['i_user_id']]);
                                                            }
                                                            echo $imgdata;
                                                        ?>    
                                                    </div>
                                                    <div class="Announcements-name">
                                                        <p>
                                                        <?php
                                                            $username="";
                                                            if(isset($userList[$val['i_user_id']])){
                                                                $username = $userList[$val['i_user_id']]['v_name'];
                                                            }
                                                            echo $username;
                                                        ?>   
                                                        </p>
                                                        <p><span><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($val['d_added']))->diffForHumans(); ?></span></p>
                                                    </div>
                                                </div>
                                                <div class="Announcements-text">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="Announcements-text-seller">
                                                                <p >
                                                                   {{$val['v_text']}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif        
                                </div>

                                 <form role="form" method="POST" name="commnetform{{$v->id}}" id="commnetform{{$v->id}}" data-parsley-validate enctype="multipart/form-data" onsubmit="submitcomment('{{$v->id}}')" action="javascript:;">
                    {{ csrf_field() }}
                    <input type="hidden" name="i_announcements_id" value="{{$v->id}}">
                    <div class="Announcements-input">
                        <div class="Announcements-find Announcements-find-q">
                            <div class="Announcements-man">
                                <?php
                                    $imgdata = \App\Helpers\General::userroundimage($curentuser);
                                    echo $imgdata;
                                ?> 
                            </div>
                            <div class="Announcements-name Announcements-name-q">
                                <p>{{$curentuser['v_name']}}</p>
                                <div class="new-submit-comment">
                                <input type="text" class="form-control comment-seller new-seller-find" name="v_text" id="v_text{{$v->id}}" placeholder="Enter your comment">
                                <button type="button" onclick="submitcomment('{{$v->id}}')" class="btn btn-convarsation-submit new-submit-live"> Submit </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>

                            </div>
                        </div>
                    </div>

                   


                @endforeach
            @else
                    <div class="Course-Reviews">
                         <div class="row">
                            <div class="col-md-6">
                                 There are no course announcements.
                            </div>
                        </div>
                    </div>

                
            @endif        

        </div>
        <!-- 26B-control-panel-my-courses-announcement-seller -->
    </div>
    <!--26b-control-panel-my-courses-overviwe-->   

@stop
@section('js')

<script type="text/javascript">

    $(document).ready(function() {
            $('.accordion').find('.accordion-toggle1').click(function() {
                $(this).next().slideToggle('600');
                $(".accordion-content").not($(this).next()).slideUp('600');
            });
            $('.accordion-toggle1').on('click', function() {
                $(this).toggleClass('active').siblings().removeClass('active');
            });
        });

    function addAnnouncement(){

        var actionurl = "{{url('courses/announcements/add')}}";
        var formValidFalg = $("#addannform").parsley().validate('');

        document.getElementById('load').style.visibility="visible";

        if(formValidFalg){
                        
            var formdata = $("#addannform").serialize();

            $.ajax({
                type    : "POST",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                   
                   document.getElementById('load').style.visibility='hidden';
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        window.location = "{{url('courses/announcements')}}/"+obj['i_courses_id'];
                    }else{
                        $('#commonerr').css("display","block");
                        $('#commonerrmsg').html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    $('#commonerr').css("display","block");
                }
            });
        }
    }

    function getAnnouncementData(id=""){
        
        var actionurl = "{{url('courses/get-announcements')}}";
        var formdata = "id="+id;
        document.getElementById('load').style.visibility="visible";
        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
               
               document.getElementById('load').style.visibility='hidden';
                var obj = jQuery.parseJSON(res);
                if(obj['status']==1){
                    
                    $("#i_courses_announcements_id").val(id);
                    $("#v_title_edit").val(obj['data']['v_title']);
                    $("#l_announcements_edit").val(obj['data']['l_announcements']);
                    $("#EditAnnouncement").modal("show");

                }else{
                    $('#commonerr').css("display","block");
                    $('#commonerrmsg').html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                $('#commonerr').css("display","block");
            }
        });
    }

    function updateAnnouncement(){

        var actionurl = "{{url('courses/update-announcements')}}";
        var formValidFalg = $("#editannform").parsley().validate('');

        var id=$("#i_courses_announcements_id").val();
        document.getElementById('load').style.visibility="visible";

        if(formValidFalg){
                        
            var formdata = $("#editannform").serialize();

            $.ajax({
                type    : "POST",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    
                    document.getElementById('load').style.visibility='hidden';
                    var obj = jQuery.parseJSON(res);
                    
                    if(obj['status']==1){
                        $("#v_title_"+id).html(obj['data']['v_title']);
                        $("#l_announcements_"+id).html(obj['data']['l_announcements']);
                        $("#EditAnnouncement").modal("hide");
                        $("#commonmsg").html(obj['msg']);
                        // $("#l_announcements_"+id).parent(".accordion-content").css("display","block");
                        // $("#l_announcements_"+id).parent(".accordion-toggle1").addClass("active");
                    }else{
                        $("#commonmsg").html(obj['msg']);
                    }

                },
                
                error: function ( jqXHR, exception ) {
                    $('#commonerr').css("display","block");
                }

            });
        }

    }


    function submitcomment(id=""){
       
        var actionurl = "{{url('courses/comments-announcements')}}";
        var formValidFalg = $("#commnetform"+id).parsley().validate('');

        document.getElementById('load').style.visibility="visible";
        
        if(formValidFalg){
                        
            var formdata = $("#commnetform"+id).serialize();

            $.ajax({
                type    : "POST",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                   document.getElementById('load').style.visibility='hidden';
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $('#announcementscomments'+id).append(obj['commentstr']);
                        $("#v_text"+id).val("");
                    }else{
                        $('#commonerr').css("display","block");
                        $('#commonerrmsg').html(obj['msg']);
                    }
                },
                
                error: function ( jqXHR, exception ) {
                    $('#commonerr').css("display","block");
                }

            });
        }


    }
    
    function coursesStatus(data="",id=""){
      
        var actionurl = "{{url('courses/updatestatus')}}";
        var formdata = "id="+id+"&e_status="+data;
      
        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
               
                var obj = jQuery.parseJSON(res);
                
                if(obj['status']==1){
                    $("#commonmsg").html(obj['msg']);
                }else{
                    $('#commonerr').css("display","block");
                    $('#commonerrmsg').html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                $('#commonerr').css("display","block");
            }
        });
    }

</script>

@stop

