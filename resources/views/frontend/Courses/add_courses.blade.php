@extends('layouts.frontend')

@section('content')

<style type="text/css">
    .browsebutton {
        height: 0;
        width: 0;
        display: none;
    }
    .btn-add-remove {
    background: #a00960;
    border-radius: 20px;
    font-size: 16px;
    color: #fff;
    margin-top: 44px;
    border: 2px solid #a00960;
}
.btn-add-remove:hover {background:transparent;color: #a00960; border:2px solid #a00960;}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

.without_ampm::-webkit-datetime-edit-ampm-field {
    display: none;
}

.without_ampm::-moz-datetime-edit-ampm-field {
   display: none;
}


</style>

<link rel="stylesheet" href="{{url('public/Assets/frontend/js')}}/jquery.percentageloader-0.1.css"></script>
<script src="{{url('public/Assets/frontend/js')}}/jquery.percentageloader-0.1.js"></script>


<style type="text/css">
    .topLoaderinner{
            width: 256px;
            height: 256px;
            margin-bottom: 32px;
            width: 100%;
            height: 100%;
            position: fixed;
            text-align: center;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            text-align: center;
            z-index: 99999999;
            background: rgba(0,0,0,0.5);
            /*margin-top: 200px;*/
            display: none;
      }
      .topLoaderinner #topLoader{
        height: 100%;
        height: 300px;
        position: fixed;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
        margin: auto;
      }
      .topLoaderinner #topLoader p{
        width: 100%;
        color: #fff; font-size: 30px;
      }
      .topLoaderinner #topLoader > div{ margin : auto; }
</style>
     @php
        $standardplanFree= App\Helpers\General::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
    @endphp 

<div class="topLoaderinner"><div id="topLoader">
    <p>Uploading Files</p>
</div></div>


    <!-- 26B-control-panel-my-courses-add-new-course -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> Add New Course </h1>
                </div>
                
            </div>
        </div>
    </div>

     <!-- Navigation Bar -->
    <div class="navigation-bar">
        <div class="container">
            <div id="content">
                <ul id="tabs" class="nav nav-tabs nav-dirservice" data-tabs="tabs">
                    <li class="active"><a href="#step1" data-toggle="" class="tab-name"> 1. Step </a></li>
                    <li><a href="#step2" data-toggle="" class="tab-name"> 2. Step </a></li>
                    @if(!$isBasicPlan)
                    <li><a href="#step3" data-toggle="" class="tab-name"> 3. Step </a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    <!-- End Navigation Bar -->

    <!-- 02B-sign-up-i-want-to-courses-SELLER -->
    <div class="container">
        <div id="content">
            <div id="my-tab-content" class="tab-content">

                <!-- 02B-sign-up-i-want-to-sell-online-SELLER-W1 -->
                <div class="tab-pane active" id="step1">
                    <form class="horizontal-form" role="form" method="POST" name="coursesste1pform" id="coursesste1pform" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-course-seller">
                                <div class="step1-dirservices">
                                    <div class="course-service ">
                                        <h1 class="text-center"> Course Basics </h1>
                                    </div>
                                    <div class="dirservice-form">
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="education-added">
                                                        <div class="heading-education">
                                                            <label class="field-name">
                                                                Course Title
                                                            </label>
                                                            <input type="text" data-parsley-minlength="10" data-parsley-minlength-message="Title must be minimum 10 characters" data-parsley-maxlength="50" data-parsley-maxlength-message="Title must be no longer than 50 characters" class="form-control input-field" name="v_title" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="employment-added">
                                                        <div class="heading-employment">
                                                            <label class="field-name">
                                                                Course Sub Title
                                                            </label>
                                                            <input type="text" class="form-control input-field" name="v_sub_title" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dirservice-form">
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="education-added">
                                                        <div class="heading-education">
                                                            <label class="field-name">
                                                                Author Name
                                                            </label>
                                                            <input type="text" class="form-control input-field" name="v_author_name" required value="{{$v_author_name or ''}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="employment-added">
                                                        <div class="heading-employment">
                                                            <label class="field-name">
                                                                Select Course Language
                                                            </label>
                                                            <select class="form-control form-sell" required name="i_language_id">
                                                                <option value="">-select-</option>
                                                                @if( isset($coursesLanguage) && count($coursesLanguage) )
                                                                  @foreach($coursesLanguage as $key => $val)
                                                                    <option value="{{$val->_id}}" >{{$val->v_title}}</option>
                                                                  @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dirservice-form">
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="employment-added">
                                                        <div class="heading-employment">
                                                            <label class="field-name">
                                                                Course Level
                                                            </label>
                                                            <select class="form-control form-sell" name="i_level_id" required>
                                                                <option value="">-select-</option>
                                                                @if( isset($coursesLevel) && count($coursesLevel) )
                                                                  @foreach($coursesLevel as $key => $val)
                                                                    <option value="{{$val->_id}}">{{$val->v_title}}</option>
                                                                  @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="education-added">
                                                        <div class="heading-education">
                                                            <label class="field-name">
                                                                Course Duration In Hour's
                                                            </label>
                                                            <input type="text" data-parsley-trigger="keyup change focusin focusout" class="form-control input-field " name="i_duration" name="i_duration_base">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dirservice-form">
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="employment-added">
                                                        <div class="heading-employment">
                                                            <label class="field-name">
                                                                Course Category
                                                            </label>
                                                            <select class="form-control form-sell" name="i_category_id" required>
                                                                <option value="">-select-</option>
                                                                @if( isset($coursesCategory) && count($coursesCategory) )
                                                                  @foreach($coursesCategory as $key => $val)
                                                                    <option value="{{$val->_id}}">{{$val->v_title}}</option>
                                                                  @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dirservice-form">
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="employment-added">
                                                        <div class="heading-employment">
                                                            <label class="field-name">
                                                                Course Requirments
                                                            </label>
                                                            <textarea rows="3" class="form-control cover-letter" name="v_requirement" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dirservice-form">
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="employment-added">
                                                        <div class="heading-employment">
                                                            <label class="field-name">
                                                                Course Description
                                                            </label>
                                                            <textarea name="l_description" id="l_description" rows="3" class="form-control cover-letter" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                          $( document ).ready(function() { 
                                          CKEDITOR.replace("l_description");
                                          });
                                    </script>
                                    <div class="text-center">
                                        <div class="courses-bottom">

                                            <button type="button" class="btn form-save-exit ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="saveandexit1" onclick="saveStep1('saveandexit1')">Save &amp; Exit</button>
                                        
                                            <button type="button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savenext2" class="btn form-next ladda-button" onclick="saveStep1('savenext2')">Next</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="alert alert-info alert-danger" id="errormsg" style="display: none;">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;<span id="errortxt">Something went wrong.</span>
                            </div>

                        </div>
                    </div>
                    </form>
                </div>
                <!-- End 02B-sign-up-i-want-to-sell-online-SELLER-W1  -->

                <!-- 02B-sign-up-i-want-to-courses-SELLER-W2 -->
                <div class="tab-pane" id="step2">
                    <form class="horizontal-form" role="form" method="POST" name="coursesste2pform" id="coursesste2pform" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}    
                    <div class=" row">
                        <div class="col-sm-12">
                            <div class="main-course-seller">
                                <div class="step2-dirservice">
                                    <div class="course-service ">
                                        <h2 class="text-center"> Courses Details </h2>
                                    </div>
                                    <input type="hidden" name="i_course_id" id="i_course_id">    
                                    <div class="dirservice-form">
                                        
                                        <div class="row-form-field">
                                            <div class="row">
                                                
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="field-name">
                                                        Instructor Profile
                                                    </label>
                                                    <div class="upload-edit-course">
                                                        <img src="{{url('public/Assets/frontend/images/profile.png')}}" alt="Your Images" class="img-responsive img-upload-course" id="edit-images" >
                                                        <input type='file' id="fileupload-example-4" accept=".png, .jpg, .jpeg" name="v_instructor_image" onchange="readURL(this);" />
                                                        <label id="fileupload-course-label" for="fileupload-example-4">Upload Image
                                                        </label>
                                                        <span style="margin-left: 10px">Max file size: 2 MB <br></span>
                                                        <span style="margin-left: 10px">Height : 195px , Width:260px <br></span>
                                                        <span style="margin-left: 10px;color: red" class="videoerrsize">File size should not be more than 2 MB<br></span>

                                                    </div>
                                                </div>
                                                <style type="text/css">
                                                    #fileupload-example-5 {
                                                        height: 0;
                                                        width: 0;
                                                        display: none;
                                                    }
                                                </style>
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="field-name">
                                                        Course Thumbnail Image
                                                    </label>
                                                    <div class="upload-edit-course">
                                                        <img src="{{url('public/Assets/frontend/images/profile.png')}}" alt="Your Images" class="img-responsive img-upload-course" id="edit-images1" >
                                                        <input type='file' id="fileupload-example-5" accept=".png, .jpg, .jpeg" name="v_course_thumbnail" onchange="readURLThumb(this);" required />
                                                        <label id="fileupload-course-label" for="fileupload-example-5">Upload Image
                                                        </label>
                                                        <span style="margin-left: 10px">Max file size: 2 MB <br></span>
                                                        <span style="margin-left: 10px">H : 195px , W : 260px <br></span>
                                                        <span style="margin-left: 10px;color: red" class="videoerrsize">File size should not be more than 2 MB<br></span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-xs-12">
                                                    <label class="field-name">Instructor Details</label>
                                                    <textarea name="l_instructor_details" rows="12" class="form-control course-description"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row-form-field">
                                            <div class="row">
                                                
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="field-name">
                                                        Upload Course Introductory Video
                                                    </label>
                                                    <input type="text" class="form-control input-field" id="textintrovideoupload1" readonly>
                                                    <span>Supported formats: mp4, mkv, webm, mov, m4v <br></span>
                                                    <span>Max file size: 100 MB <br></span>
                                                    <span class="videoerr" style="color: red">Video format not Supported.use above format </span>

                                                    <div class="text-right">
                                                        <input type='file'  accept=".mp4, .mkv, .webm, .mov, .m4v" class="browsebutton" id="introvideoupload1" name="l_video" onchange="workvideo('introvideoupload1');"  parsley-filemaxsize="0.5" data-parsley-trigger="change" required />
                                                        <label id="fileupload" class="btn-editdetail" for="introvideoupload1">Browser</label>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="field-name">
                                                        Enter Course Price (£)
                                                    </label>
                                                    <input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" name="f_price" id="f_price" required="">
                                                    <div class="checkbox-service">
                                                        <label class="checkbox check-boxright">
                                                            <input type="checkbox" name="v_course_free" id="v_course_free" onchange="pricevalidation()">
                                                            <span class="subfield-name"> 
                                                                Make this course free
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="add-chapter">
                                            <div class="heading-coursesection">
                                                <h2> Course Sections  </h2>
                                            </div>
                                            
                                            <div id="section1">
                                                <div class="form-field">
                                                    <div class="row-form-field">
                                                        <div class="row">
                                                            <div class="col-sm-6 col-xs-12">
                                                                <label class="field-name">
                                                                    Section 1 Title
                                                                </label>
                                                                <input type="text" class="form-control input-field" name="section[0][v_title]" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row-form-field">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-xs-12">
                                                            <label class="field-name">
                                                                Section 1 Video Title
                                                            </label>
                                                            <input type="text" class="form-control input-field" name="section[0][video][0][v_title]" required>
                                                        </div>
                                                        <div class="col-sm-6 col-xs-12">
                                                            
                                                            <label class="field-name">
                                                                Section 1 Video
                                                            </label>
                                                            <input type="text" class="form-control input-field" id="textvideoupload1" readonly>
                                                            
                                                            <span>Supported formats: mp4, mkv, webm, mov, m4v <br></span>
                                                            <span>Max file size: 100 MB <br></span>
                                                            <span class="videoerr" style="color: red">Video format not supported <br></span>
                                                            <span class="videoerrsize" style="color: red">File size should not be more than 100 MB<br></span>

                                                            <div class="col-xs-12">
                                                                <div class="text-right">
                                                                    <input type='file' accept=".mp4, .mkv, .webm, .mov, .m4v" class="browsebutton" name="section[0][video][0][v_video]" required id="videoupload1" onchange="workvideo('videoupload1');"  />
                                                                    <label id="fileupload" class="btn-editdetail" for="videoupload1">Browser</label>
                                                                </div>
                                                            </div>

                                                            <div class="checkbox-service">
                                                                <label class="checkbox check-boxright">
                                                                    <input type="hidden" name="section[0][video][0][i_preview]" value="off">
                                                                    <input type="checkbox" name="section[0][video][0][i_preview]">
                                                                    <span class="subfield-name"> 
                                                                        Preview this course
                                                                    </span>
                                                                </label>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="col-sm-6 col-xs-12">
                                                            <label class="field-name">
                                                                Video duration 
                                                            </label>
                                                            <div class="row">
                                                                <div class="col-md-6"><input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field col-md-6" name="section[0][video][0][v_duration_min]" value="00"><div class="text-center">Minutes</div></div>
                                                                <div class="col-md-6"><input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true"  class="form-control input-field col-md-6" name="section[0][video][0][v_duration_sec]" value="00"><div class="text-center">Seconds</div></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-xs-12">
                                                            <label class="field-name">
                                                                Section 1 Doc
                                                            </label>
                                                            <input type="text" class="form-control input-field" id="textdocupload1" readonly>
                                                            
                                                            <span>Supported formats: pdf, doc, docx, txt , xlsx, xls,pptx, zip<br></span>
                                                            <span>Max file size: 10 MB <br></span>
                                                            <span class="docerr" style="color: red">Document format not supported.<br></span>
                                                            <span class="videoerrsize" style="color: red">File size should not be more than 10 MB<br></span>

                                                            <div class="col-xs-12">
                                                                <div class="text-right">
                                                                    <input type='file' accept=".pdf, .docx, .txt, .doc, .xlsx, .xls, .pptx, .zip" class="browsebutton" name="section[0][video][0][v_doc]" id="docupload1" onchange="workvideodoc('docupload1');" />
                                                                    <label id="fileupload" class="btn-editdetail" for="docupload1">Browser
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" id="videouploadcnt1" value="1">
                                                        <div class="col-sm-12 col-xs-12">
                                                            <div class="text-right">
                                                                <button type="button" class="btn form-add-course" onclick="addmorevideo('1')" > Add More Video</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="sectionvideo1"></div>

                                            </div>

                                            {{-- </div> --}}

                                            <div id="coursessectionmorediv">
                                            </div>
                                        </div>
                                        <input type="hidden" id="sectioncnt" value="1">
                                        
                                        <div class="add-new-section">
                                            <button type="button" class="btn btn-new-section" onclick="addmoresection()"> Add New Section
                                            </button>
                                        </div>

                                    </div>


                                    <div class="row-form-field">
                                        <div class="name-details">
                                            <div class="searching-topspace">
                                                <label class="field-name">
                                                    Add search tags
                                                </label>
                                            </div>
                                        </div>
                                        <div class="name-details">
                                            <label class="subfield-name">
                                                Please add up to 3 search tags related to your courses.
                                            </label>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="input-topspace">
                                                    <div class="row">
                                                        <div class="col-xs-10">
                                                            <div class="input-login">
                                                                <input class="form-control business-name-control" type="text" id="temp_text" placeholder="Start typing ....">
                                                                <ul class="parsley-errors-list filled" id="parsley-id-tags" style="display: none;">
                                                                <li class="parsley-type">You have already enter 3 tags.</li>
                                                                </ul>
                                                                <ul class="parsley-errors-list filled" id="parsley-id-req" style="display: none;">
                                                                <li class="parsley-type">Please enter search tag.</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 terms-add" id="hiddentags">
                                                            <div class="login-btn"  id="addtagbutton">
                                                                <button type="button" class="btn add-now" onclick="myFunction()">Add</button>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="selection-point" id="add_text">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="validatemessagetags" style="display: none;"><ul class="parsley-errors-list filled"><li class="parsley-required">Please add at least one search tag to proceed.</li></ul></div>

                                    </div>
                                  
                                    <div class="text-right">
                                        <div class="btn-save-course">
                                       
                                        @if(!$isBasicPlan)

                                            <button type="button" class="btn save-course ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savedraftnext" onclick="saveStep2('savedraftnext')">Save course as draft</button>

                                            <button type="button" class="btn publish-course ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savepublishnext" onclick="saveStep2('savepublishnext')"> Save and publish course </button>

                                        @else

                                            <button type="button" class="btn save-course ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savedraftexit" onclick="saveStep2('savedraftexit')">Save course as draft</button>

                                            <button type="button" class="btn publish-course ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savepublishexit" onclick="saveStep2('savepublishexit')"> Save and publish course </button>


                                        @endif

                                        </div>

                                    </div>
                                </div>
                            <div style="clear: both"></div>    
                        </div>
                        
                        <div class="alert alert-info alert-danger" id="errormsgstep2" style="display: none;margin-top: 20px;">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;<span id="errortxtstep2">Something went wrong.</span>
                        </div>
                    </div>
                    </form>
                    </div>
                </div>
                <!-- End 02B-sign-up-i-want-to-sell-online-SELLER-W2 -->

                 @php
                    $selected="";    
                @endphp
                <!-- 02B-sign-up-i-want-to-courses-SELLER-W3 -->
                @if(!$isBasicPlan)
                <div class="tab-pane" id="step3">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="main-dirservice3">
                                

                                <form class="horizontal-form" role="form" method="POST" name="planform" id="planform" action="{{url('courses/update-user-plan')}}" data-parsley-validate enctype="multipart/form-data" >
                                {{ csrf_field() }}

                                <div class="step3-dirservice">
                                     <div class="course-service ">
                                            <h2 class="text-center"> Courses Plans </h2>
                                    </div>
                                    <div class="foll-option">
                                        Please choose the plan that best fits your needs
                                    </div>
                                    <div class="wrap">
                                            <fieldset>
                                                <div class="toggle">
                                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join" checked onchange="plandurationchange('monthly')" >
                                                    <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create" onchange="plandurationchange('yearly')">
                                                    <label for="create" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                                    <span class="toggle-selection"></span>
                                                </div>
                                            </fieldset>
                                    </div>
                                </div>

                                <div class="createLabel">
                                    
                                @if(count($plandata))
                                    @foreach($plandata as $key=>$val)

                                     @if($val['id'] == $userplan['id'])
                                        @php $selected =1; @endphp
                                    @endif 
                                    @if($selected==1)
                                    <div class="section">
                                        <div class="row matchHeights">
                                            
                                            <div class="col-sm-3 col-xs-12">
                                                <div class="@if($key==0)left-choise @else left-side @endif matchheight_div">
                                                    <div class="reg">
                                                        <bdo dir="ltr">
                                                            <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required @if($val['id'] == $userplan['id']) checked  @endif>
                                                            <span class="select-seller-job"></span>
                                                            <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                        </bdo>
                                                    </div>
                                                    <div class="choise-que">{{$val['v_subtitle']}}</div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-3 col-xs-12">
                                                <ul class="list-unstyled choise-avalible matchheight_div">
                                                    
                                                    <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k<3)
                                                            <li class="right-mark"><img src="images/tick.png" class="tick" alt="" />{{$v}}</li>
                                                            <?php $lcnt = $lcnt+1; ?>
                                                            @endif
                                                        @endforeach
                                                    @endif       
                                                    
                                                </ul>
                                            </div>
                                            
                                            <div class="col-sm-4 col-xs-12">
                                                <div class="right-display matchheight_div">
                                                    
                                                    <ul class="list-unstyled choise-avalible">
                                                        <?php 
                                                            $lcnt=1;
                                                        ?>
                                                        @if(count($val['l_bullet']))
                                                            @foreach($val['l_bullet'] as $k=>$v) 
                                                                @if($k>=3)
                                                                <li class="right-mark"><img src="images/tick.png" class="tick" alt="" />{{$v}}</li>
                                                                @endif
                                                                <?php $lcnt = $lcnt+1; ?>
                                                            @endforeach
                                                        @endif  
                                                    </ul>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-2 col-xs-12 same-height-1">
                                                <div class="@if($key==0) free-price @else final-price @endif">
                                                    <div class="all-in-data">
                                                        
                                                    <div  class="monthyprice">
                                                           @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_monthly_price']}}</del>
                                                                </div>
                                                                Only £{{$val['f_monthly_dis_price']}} p/m

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif

                                                           @endif  
                                                    </div>  
                                                    
                                                    <div class="yealryprice" style="display: none;">
                                                           @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_yealry_price']}}</del>
                                                                </div>
                                                                Only £{{$val['f_yealry_dis_price']}} p/a

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif

                                                                
                                                           @endif  
                                                    </div>     

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    @endif
                                    
                                    @endforeach
                                @endif    
                                </div>
                                
                                <button type="submit" class="btn btn-payment">
                                    Proceed
                                </button>
                                </form>
                           
                            </div>
                        </div>
                    </div>
                </div>
                @endif

            </div>
        </div>
    </div>
    
   

@stop
@section('js')
<script src="{{url('public/js/jquery.mask.js')}}" data-autoinit="true"></script>

<script type="text/javascript">

        $(document).ready(function(){
            $('#i_duration_base').mask("00:00", {placeholder: "HH/MM"});
        });  
        
        function myFunction() {

            var cnt = $('#add_text').children().length
            if(cnt>=3){
                $("#parsley-id-tags").css("display","block");
                jQuery('#temp_text').val("");
                return 0;
            }else{
                $("#parsley-id-tags").css("display","none");
            }
           
            var dt = new Date();
            var dynamic_id = dt.getDate() + "" + dt.getHours() + "" + dt.getMinutes() + "" + dt.getSeconds() + "" + dt.getMilliseconds();
            var y = jQuery('#temp_text').val();
            var z = '<button>';
             var y = jQuery('#temp_text').val();
            if(y==""){
                $("#parsley-id-req").css("display","block");
                return 0;
            }
            jQuery('#add_text').append("<button class='auto-width' id='" + dynamic_id + "' onclick='removeButton(this.id)'><span>" + y + "</span><img src='{{url('public/Assets/frontend/images/close1.png')}}'></button>");

            var cnt = $('#add_text').children().length
            if(cnt==3){
                $("#addtagbutton").css("display","none");
            }

            var abval = $("#temp_text").val();
            var strhidden = '<input type="hidden" id="hidden-'+dynamic_id+'" name="v_search_tags[]" value="'+abval+'">';
            $("#hiddentags").append(strhidden);
            $("#temp_text").val("");
            $("#validatemessagetags").css("display","none");
        }

        function removeButton(id) {
            jQuery("#" + id).remove();
            jQuery("#hidden-"+id).remove();

             var cnt = $('#add_text').children().length
            if(cnt<3){
                $("#addtagbutton").css("display","block");
            }
        }

        function saveStep1(type=""){
          
            var actionurl = "{{url('courses/add')}}";
            var formValidFalg = $("#coursesste1pform").parsley().validate('');

                if(formValidFalg){
                    
                    document.getElementById('load').style.visibility="visible"; 
                    // var l = Ladda.create(document.getElementById(type));
                    // l.start();  
                    var formdata = new FormData($('#coursesste1pform')[0]);
                    var desc = CKEDITOR.instances['l_description'].getData();
                    formdata.append('l_description', desc);
                    //var formdata = $("#coursesste1pform").serialize();

                    $.ajax({
                        processData: false,
                        contentType: false,
                        type    : "POST",
                        url     : actionurl,
                        data    : formdata,
                        success : function( res ){
                            document.getElementById('load').style.visibility='hidden';
                            var obj = jQuery.parseJSON(res);
                            
                            if(obj['status']==1){
                                $('#i_course_id').val(obj['i_course_id']);
                                if(type=="savenext2"){
                                    $('.nav-tabs a[href="#step2"]').tab('show');
                                    window.scrollTo(0, 0);
                                }else{
                                    window.location = "{{url('courses')}}";
                                }
                            }else{
                                $("#errormsg").show();
                                $("#errortxt").html(obj['msg']);
                            }
                        },
                        error: function ( jqXHR, exception ) {
                            //l.stop();
                            $("#errormsg").show();
                        }
                    });
                }    
        }

        function saveStep2(type=""){
          
            var actionurl = "{{url('courses/add-update')}}";
            var formValidFalg = $("#coursesste2pform").parsley().validate('');

            var hiddenstr="";
            if(type == "savepublishexit"){
                hiddenstr ="<input type='hidden' name='e_status' value='published'>";
            }else if(type == "savedraftexit"){
                hiddenstr ="<input type='hidden' name='e_status' value='unpublished'>";
            }else if(type == "savepublishnext"){
                hiddenstr ="<input type='hidden' name='e_status' value='published'>";
            }else{
                hiddenstr ="<input type='hidden' name='e_status' value='unpublished'>";
            }
            $('#coursesste2pform').append(hiddenstr);

            var tags = $("#add_text").find("button").length
            if(tags<1){
                $("#validatemessagetags").css("display","block");
                return false;
            }else{
                $("#validatemessagetags").css("display","none");
            }

            if(formValidFalg){
                // document.getElementById('load').style.visibility="visible"; 
                // var l = Ladda.create(document.getElementById(type));
                // l.start();

                var ttlSize=9;
                $('#coursesste2pform').find('input[type=file]').each(function() {
                    if(this.files[0]){
                        ttlSize = parseInt(ttlSize)+parseInt(this.files[0].size);
                    }
                }); 
                ttlSize = parseInt(ttlSize)/1000;
                LoaderPercentStart(ttlSize);


                var formData = new FormData($('#coursesste2pform')[0]);
                
                $.ajax({
                    processData: false,
                    contentType: false,
                    type    : "POST",
                    url     : actionurl,
                    data    : formData,
                    success : function( res ){
                        // document.getElementById('load').style.visibility='hidden';
                        // l.stop();
                        LoaderPercentStop();
                        var obj = jQuery.parseJSON(res);
                      
                        if(obj['status']==1){
                        
                            if(type == "savepublishexit"){
                                window.location = "{{url('courses')}}";
                            }else if(type == "savedraftexit"){
                                window.location = "{{url('courses')}}";
                            }else if(type == "savedraftnext"){
                                $('.nav-tabs a[href="#step3"]').tab('show');
                                window.scrollTo(0, 0);
                            }else{
                                $('.nav-tabs a[href="#step3"]').tab('show');
                                window.scrollTo(0, 0);
                            }

                        }else{
                            $("#errormsgstep2").show();
                            $("#errortxtstep2").html(obj['msg']);
                        }

                    },
                    error: function ( jqXHR, exception ) {
                        //l.stop();
                        LoaderPercentStop();
                        $("#errormsgstep2").show();
                    }
                });
            }    
        }

        function readURL(input) {

            if (input.files && input.files[0]) {
                if(input.files[0].size > 2097152) {
                    $('#fileupload-example-4').parent("div").find(".videoerrsize").css("display","block");
                    return false;
                }
                $('#fileupload-example-4').parent("div").find(".videoerrsize").css("display","none");    
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#edit-images')
                        .attr('src', e.target.result)
                        .width(180)
                        .height(150);
                };
                reader.readAsDataURL(input.files[0]);
            }

        }
        function readURLThumb(input) {

            if (input.files && input.files[0]) {
                if(input.files[0].size > 2097152) {
                    $('#fileupload-example-5').parent("div").find(".videoerrsize").css("display","block");
                    return false;
                }
                $('#fileupload-example-5').parent("div").find(".videoerrsize").css("display","none");    

                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#edit-images1')
                        .attr('src', e.target.result)
                        .width(180)
                        .height(150);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function workvideo(data){
            
            var fp = $('#'+data);
            var lg = fp[0].files.length;
            var items = fp[0].files;
            var fileSize = 0;
            if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; 
               }
               if(fileSize > 100715200) {
                    $('#'+data).val("");
                    $('#text'+data).val("");
                    $('#text'+data).parent("div").find(".videoerrsize").css("display","block");
                    return false;
               }
            }

            var filename = $('#'+data).val().split('\\').pop();
            var lastIndex = filename.lastIndexOf("\\");   

            var myarray =filename.split('.');
            var last = myarray[myarray.length-1];
            format = ["mp4", "mkv", "webm","mov","m4v"];
            if(jQuery.inArray(last, format) == -1) {
                $('#'+data).val("");
                $('#text'+data).val("");
                $('#text'+data).parent("div").find(".videoerr").css("display","block");
                return false;
            }

            $('#text'+data).val(filename);
            $('#text'+data).parent("div").find(".videoerr").css("display","none");
            $('#text'+data).parent("div").find(".videoerrsize").css("display","none");

        }

        function workvideodoc(data){
            
            var fp = $('#'+data);
            var lg = fp[0].files.length;
            var items = fp[0].files;
            var fileSize = 0;
            if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; 
               }
               if(fileSize > 10485760) {
                    
                    $('#'+data).val("");
                    $('#text'+data).val("");
                    $('#text'+data).parent("div").find(".videoerrsize").css("display","block");
                    return false;

               }
            }

            var filename = $('#'+data).val().split('\\').pop();
            var lastIndex = filename.lastIndexOf("\\");   

            var myarray =filename.split('.');
            var last = myarray[myarray.length-1];
            //format = format = ["pdf", "doc", "docx","txt"];
            format = ["pdf", "doc", "docx","txt","xlsx","xls","pptx","zip"];
            if(jQuery.inArray(last, format) == -1) {
                $('#'+data).val("");
                $('#text'+data).val("");
                $('#text'+data).parent("div").find(".docerr").css("display","block");
                return false;
            }

            $('#text'+data).val(filename);
            $('#text'+data).parent("div").find(".docerr").css("display","none");
            $('#text'+data).parent("div").find(".videoerrsize").css("display","none");

        }



        function workvideosection(data){
            
            var fp = $('#videoupload'+data);
            var lg = fp[0].files.length;
            var items = fp[0].files;
            var fileSize = 0;
            if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; 
               }
               if(fileSize > 100715200) {
                    
                    $('#videoupload'+data).val("");
                    $('#textvideoupload'+data).val("");
                    $('#textvideoupload'+data).parent("div").find(".videoerrsize").css("display","block");
                    return false;
               }
            }

            var filename = $('#videoupload'+data).val().split('\\').pop();
            var lastIndex = filename.lastIndexOf("\\");   

            var myarray =filename.split('.');
            var last = myarray[myarray.length-1];
            format = ["mp4", "mkv", "webm","mov","m4v"];
            if(jQuery.inArray(last, format) == -1) {
                $('#videoupload'+data).val("");
                $('#textvideoupload'+data).val("");
                $('#textvideoupload'+data).parent("div").find(".videoerr").css("display","block");
                return false;
            }
            $('#textvideoupload'+data).val(filename);
            $('#textvideoupload'+data).parent("div").find(".videoerr").css("display","none");
            $('#textvideoupload'+data).parent("div").find(".videoerrsize").css("display","none");

        }

        function workdocsection(data){
            var filename = $('#docupload'+data).val().split('\\').pop();
            var lastIndex = filename.lastIndexOf("\\");   

            var myarray =filename.split('.');
            var last = myarray[myarray.length-1];
            //format = format = ["pdf", "doc", "docx","txt"];
            format = ["pdf", "doc", "docx","txt","xlsx","xls","pptx","zip"];
            if(jQuery.inArray(last, format) == -1) {
                $('#docupload'+data).val("");
                $('#textdocupload'+data).val("");
                $('#textdocupload'+data).parent("div").find(".docerr").css("display","block");
                return false;
            }



            var fp = $('#docupload'+data);
            var lg = fp[0].files.length;
            var items = fp[0].files;
            var fileSize = 0;
            if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; 
               }
               if(fileSize > 10485760) {
                    $('#docupload'+data).val("");
                    $('#textdocupload'+data).val("");
                    $('#textdocupload'+data).parent("div").find(".videoerrsize").css("display","block");
                    return false;

               }
            }

            $('#textdocupload'+data).val(filename);
            $('#textdocupload'+data).parent("div").find(".docerr").css("display","none");
            $('#textdocupload'+data).parent("div").find(".videoerrsize").css("display","none");

        }

        $(document).on("click", ".rem", function() { 
            $(this).parents(".sectionvideoremove").remove();
        });
        $(document).on("click", ".remdoc", function() { 
            $(this).parents(".sectiondocremove").remove();
        });

        

        $(document).on("click", ".removesection", function() { 
            $(this).parents(".sectionremove").remove();
        });


        $(document).on("click", ".remsectionvideo", function() { 
            $(this).parents(".sectionvideoremove").remove();
        });


      
        function addmorevideo(section=""){
               
            var videouploadcnt = $("#videouploadcnt"+section).val();
            $("#videouploadcnt"+section).val(parseInt(videouploadcnt)+1);
            var uplodcnt = section+videouploadcnt;
            var arrayindex = videouploadcnt;
            var i = parseInt(section)-1;
            if(section==1){
                var i=0;    
            }else{
                var i = parseInt(section);          
            }
            
            var appendstr="";            
            appendstr+='<div class="row-form-field sectionvideoremove" style="border-top: 1px solid #c7c7c7;padding-bottom: 24px;">';
            appendstr+='<div class="row" style="margin-top:25px">';

           // appendstr+='<input type="hidden">';
            
            appendstr+='<div class="col-sm-12 col-xs-12">';
            appendstr+='<button type="button" class="btn btn-add-remove remsectionvideo" style="margin-top: 3px;float:right">Remove video</button>';
            appendstr+='</div>';
            
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Section '+section+' Video Title';
            appendstr+='</label>';
            appendstr+='<input type="text" class="form-control input-field" name="section['+i+'][video]['+arrayindex+'][v_title]" required>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Section '+section+' Video';
            appendstr+='</label>';
            appendstr+='<input type="text" class="form-control input-field" id="textvideoupload'+uplodcnt+'" readonly>';
            
            appendstr+='<span>Supported formats: mp4, mkv, webm, mov, m4v<br></span>';
            appendstr+='<span>Max file size: 100 MB <br></span>';
            appendstr+='<span class="videoerr" style="color: red">Video format not supported.<br></span>';
            appendstr+='<span class="videoerrsize" style="color: red">File size should not be more than 100 MB<br></span>';

            appendstr+='<div class="col-xs-12">';
            appendstr+='<div class="text-right">';
            appendstr+='<input type="file" class="browsebutton" accept=".mp4, .mkv, .webm, .mov, .m4v" required name="section['+i+'][video]['+arrayindex+'][v_video]"  id="videoupload'+uplodcnt+'" onchange="workvideosection('+uplodcnt+');" />';
            appendstr+='<label id="fileupload" class="btn-editdetail" for="videoupload'+uplodcnt+'">Browser</label>';
            appendstr+='</div>';
            appendstr+='</div>';

            appendstr+='<div class="checkbox-service">';
            appendstr+='<label class="checkbox check-boxright">';
            appendstr+='<input type="hidden" name="section['+i+'][video]['+arrayindex+'][i_preview]" value="off">';
            appendstr+='<input type="checkbox" name="section['+i+'][video]['+arrayindex+'][i_preview]">';
            appendstr+='<span class="subfield-name"> ';
            appendstr+='Preview this course';
            appendstr+='</span>';
            appendstr+='</label>';
            appendstr+='</div>';
            
            

            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Video Duration';
            appendstr+='</label>';
            appendstr+='<div class="row">';
            appendstr+='<div class="col-md-6">';
            appendstr+='<input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field col-md-6" name="section['+i+'][video]['+arrayindex+'][v_duration_min]" value="00" ><div class="text-center">Minutes</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-md-6">';
            appendstr+='<input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true"  class="form-control input-field col-md-6" name="section['+i+'][video]['+arrayindex+'][v_duration_sec]" value="00"><div class="text-center">Seconds</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">';
            appendstr+='Section '+section+' Doc';
            appendstr+='</label>';
            appendstr+='<input type="text" class="form-control input-field" id="textdocupload'+uplodcnt+'" readonly>';

            appendstr+='<span>Supported formats: pdf, doc, docx, txt,xlsx,xls,pptx,zip<br></span>';
            appendstr+='<span>Max file size: 10 MB <br></span>';
            appendstr+='<span class="docerr" style="color: red">Document format not supported.<br></span>';
            appendstr+='<span class="videoerrsize" style="color: red">File size should not be more than 10 MB<br></span>';

            appendstr+='<div class="col-xs-12">';
            appendstr+='<div class="text-right">';
            appendstr+='<input type="file" accept=".pdf, .docx, .txt, .doc, .xlsx, .xls, .pptx, .zip" class="browsebutton" name="section['+i+'][video]['+arrayindex+'][v_doc]" id="docupload'+uplodcnt+'" onchange="workdocsection('+uplodcnt+');" />';
            appendstr+='<label id="fileupload" class="btn-editdetail" for="docupload'+uplodcnt+'">Browser';
            appendstr+='</label>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-12 col-xs-12">';
            appendstr+='<div class="text-right">';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            $("#sectionvideo"+section).append(appendstr);

        }


        $(document).on("click", ".remsectiondata", function() { 
           $(this).parents(".removechapter").remove();
        });


        function addmoresection(){


            var totalsec = $(".removechapter").length;
            totalsec = parseInt(totalsec)+2;

            var b = $("#sectioncnt").val();
            var appendstr="";
            var i=parseInt(b)+1;
            $("#sectioncnt").val(i);
            var arrayindex=0;
            var uplodcnt =b+0;

var appendstr="";
appendstr+='<div id="section1" class="removechapter" style="border-top: 1px solid #c7c7c7;padding-bottom: 24px;">';
appendstr+='<div class="form-field">';
appendstr+='<div class="row-form-field">';
appendstr+='<div class="row">';

appendstr+='<div class="col-sm-12 col-xs-12">';
appendstr+='<button type="button" class="btn btn-add-remove remsectiondata" style="margin-top: 0px;float:right">Remove Section</button>';
appendstr+='</div>';

appendstr+='<div class="col-sm-6 col-xs-12">';
appendstr+='<label class="field-name">';
appendstr+='Section '+totalsec+' Title';
appendstr+='</label>';
//appendstr+='<button type="button" class="btn btn-add-remove remsectiondata" style="margin-top: 0px;float:right">Remove Section</button>';
appendstr+='<input type="text" class="form-control input-field" name="section['+totalsec+'][v_title]" required>';
appendstr+='</div>';
appendstr+='</div>';
appendstr+='</div>';
appendstr+='</div>';
appendstr+='<div class="row-form-field sectionvideoremove">';
appendstr+='<div class="row">';
appendstr+='<div class="col-sm-6 col-xs-12">';
appendstr+='<label class="field-name">';
appendstr+='Section '+totalsec+' Video Title';
appendstr+='</label>';
appendstr+='<input type="text" class="form-control input-field" name="section['+totalsec+'][video]['+arrayindex+'][v_title]" required>';
appendstr+='</div>';
appendstr+='<div class="col-sm-6 col-xs-12">';
appendstr+='<label class="field-name">';
appendstr+='Section '+totalsec+' Video';
appendstr+='</label>';
appendstr+='<input type="text" class="form-control input-field" id="textvideoupload'+uplodcnt+'" readonly>';
appendstr+='<span>Supported formats: mp4, mkv, webm, mov, m4v<br></span>';
appendstr+='<span>Max file size: 100 MB <br></span>';
appendstr+='<span class="videoerr" style="color: red">Video format not supported.<br></span>';
appendstr+='<span class="videoerrsize" style="color: red">File size should not be more than 100 MB<br></span>';

appendstr+='<div class="col-xs-12">';
appendstr+='<div class="text-right">';
appendstr+='<input type="file" class="browsebutton" accept=".mp4, .mkv, .webm, .mov, .m4v" required name="section['+totalsec+'][video]['+arrayindex+'][v_video]"  id="videoupload'+uplodcnt+'" onchange="workvideosection('+uplodcnt+');" />';
appendstr+='<label id="fileupload" class="btn-editdetail" for="videoupload'+uplodcnt+'">Browser</label>';
appendstr+='</div>';
appendstr+='</div>';

appendstr+='<div class="checkbox-service">';
appendstr+='<label class="checkbox check-boxright">';
appendstr+='<input type="hidden" name="section['+totalsec+'][video]['+arrayindex+'][i_preview]" value="off">';
appendstr+='<input type="checkbox" name="section['+totalsec+'][video]['+arrayindex+'][i_preview]">';
appendstr+='<span class="subfield-name"> ';
appendstr+='Preview this course';
appendstr+='</span>';
appendstr+='</label>';
appendstr+='</div>';

appendstr+='</div>';
appendstr+='<div class="col-sm-6 col-xs-12">';
appendstr+='<label class="field-name">';
appendstr+='Video Duration';
appendstr+='</label>';
appendstr+='<div class="row">';
appendstr+='<div class="col-md-6">';
appendstr+='<input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field col-md-6" name="section['+totalsec+'][video]['+arrayindex+'][v_duration_min]" value="00"><div class="text-center">Minutes</div>';
appendstr+='</div>';
appendstr+='<div class="col-md-6">';
appendstr+='<input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field col-md-6" name="section['+totalsec+'][video]['+arrayindex+'][v_duration_sec]" value="00"><div class="text-center">Seconds</div>';
appendstr+='</div>';
appendstr+='</div>';
appendstr+='</div>';
appendstr+='<div class="col-sm-6 col-xs-12">';
appendstr+='<label class="field-name">';
appendstr+='Section '+totalsec+' Doc';
appendstr+='</label>';
appendstr+='<input type="text" class="form-control input-field" id="textdocupload'+uplodcnt+'" readonly>';
appendstr+='<span>Supported formats: pdf, doc, docx, txt, xlsx, xls,pptx, zip<br></span>';
appendstr+='<span>Max file size: 10 MB <br></span>';
appendstr+='<span class="docerr" style="color: red">Document format not supported.<br></span>';
appendstr+='<span class="videoerrsize" style="color: red">File size should not be more than 10 MB<br></span>';


appendstr+='<div class="col-xs-12">';
appendstr+='<div class="text-right">';
appendstr+='<input type="file" accept=".pdf, .docx, .txt, .doc, .xlsx, .xls, .pptx, .zip" class="browsebutton" name="section['+totalsec+'][video]['+arrayindex+'][v_doc]" id="docupload'+uplodcnt+'" onchange="workdocsection('+uplodcnt+');" />';
appendstr+='<label id="fileupload" class="btn-editdetail" for="docupload'+uplodcnt+'">Browser';
appendstr+='</label>';
appendstr+='</div>';
appendstr+='</div>';
appendstr+='</div>';

appendstr+='<input type="hidden" id="videouploadcnt'+totalsec+'" value="'+totalsec+'">';
appendstr+='<div class="col-sm-12 col-xs-12">';
appendstr+='<div class="text-right">';
appendstr+='<button type="button" class="btn form-add-course" onclick="addmorevideo('+totalsec+')" > Add More</button>';
appendstr+='</div>';
appendstr+='</div>';

appendstr+='<div class="col-sm-12 col-xs-12">';
appendstr+='<div class="text-right">';
appendstr+='</div>';
appendstr+='</div>';
appendstr+='</div>';
appendstr+='</div>';
appendstr+='<div id="sectionvideo'+totalsec+'"></div></div>';





            $("#coursessectionmorediv").append(appendstr);

        }

        // function addmorevideo(section=""){
            
        //     var videouploadcnt = $("#videouploadcnt").val();
        //     $("#videouploadcnt").val(parseInt(videouploadcnt)+1);
        //     var uplodcnt = section+videouploadcnt;

        //     var i = parseInt(section)-1;
        //     var appendstr="";
        //     appendstr+='<div class="row-form-field sectionvideoremove">';
        //     appendstr+='<div class="row">';
        //     appendstr+='<div class="col-sm-6 col-xs-12">';
        //     appendstr+='<label class="field-name">Section 1 Video Title</label>';
        //     appendstr+='<input type="text" class="form-control input-field" name="section['+i+'][video][v_title][]">';
        //     appendstr+='</div>';
        //     appendstr+='<div class="col-sm-6 col-xs-12">';
        //     appendstr+='<label class="field-name">Section 1 Video</label>';
        //     appendstr+='<input type="text" class="form-control input-field" id="textvideoupload'+uplodcnt+'">';
        //     appendstr+='<div class="checkbox-service">';
        //     appendstr+='<label class="checkbox check-boxright">';
        //     appendstr+='<input type="hidden" name="section['+i+'][video][i_preview][]" value="off" >'; 
        //     appendstr+='<input type="checkbox" name="section['+i+'][video][i_preview][]">';
        //     appendstr+='<span class="subfield-name"> Preview this course</span>';
        //     appendstr+='</label>';
        //     appendstr+='</div>';
        //     appendstr+='<div class="clearfix"></div>';
        //     appendstr+='<div class="col-xs-6">';
        //     appendstr+='<div class="text-center">';
        //     appendstr+='<input type="file" class="browsebutton" name="section['+i+'][video][v_video][]"  id="videoupload'+uplodcnt+'" onchange="workvideosection('+uplodcnt+');" />';
        //     appendstr+='<label id="fileupload" class="btn-editdetail" for="videoupload'+uplodcnt+'">Browser</label>';
        //     appendstr+='</div>';
        //     appendstr+='</div>';
        //     appendstr+='<div class="col-xs-6">';
        //     appendstr+='<div class="text-center">';
        //     appendstr+='<button type="button" class="btn btn-add-remove rem" style="margin-top: 14px;" >Remove</button>';
        //     appendstr+='</div>';
        //     appendstr+='</div>';
        //     appendstr+='</div>';
        //     appendstr+='</div>';
        //     appendstr+='</div>';
        //     $("#sectionvideo"+section).append(appendstr);
        // }

        function addmoredoc(section=""){

            var docuploadcnt = $("#docuploadcnt").val();
            $("#docuploadcnt").val(parseInt(docuploadcnt)+1);
            var uplodcnt = section+docuploadcnt;
            var i = parseInt(section)-1;
            var appendstr="";
            appendstr+='<div class="row-form-field sectiondocremove">';
            appendstr+='<div class="row">';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">Section 1 Doc Title</label>';
            appendstr+='<input type="text" class="form-control input-field" name="section['+i+'][doc][v_title][]" >';
            appendstr+='</div>';
            appendstr+='<div class="col-sm-6 col-xs-12">';
            appendstr+='<label class="field-name">Section 1 Doc</label>';
            appendstr+='<input type="text" class="form-control input-field" id="textdocupload'+uplodcnt+'">';
            appendstr+='<div class="col-xs-6">';
            appendstr+='<div class="text-center">';
            appendstr+='<input type="file" class="browsebutton" name="section['+i+'][doc][v_doc][]" id="docupload'+uplodcnt+'" onchange="workdocsection('+uplodcnt+');" />';
            appendstr+='<label id="fileupload" class="btn-editdetail" for="docupload'+uplodcnt+'">Browser';
            appendstr+='</label>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='<div class="col-xs-6">  ';
            appendstr+='<div class="text-center">';
            appendstr+='<button type="button" class="btn btn-add-remove remdoc" style="margin-top: 14px;">Remove';
            appendstr+='</button>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            appendstr+='</div>';
            $("#sectiondoc"+section).append(appendstr);
        }


        // function addmoresection(){
              
        //         var b = $("#sectioncnt").val();
              
        //         var appendstr="";
        //         var i=parseInt(b)+1;
        //         $("#sectioncnt").val(i);

        //         appendstr+='<div id="section'+i+'" style="border-top: 2px solid black;" class="sectionremove">';
        //         appendstr+='<div class="form-field">';
        //         appendstr+='<div class="row-form-field">';
        //         appendstr+='<div class="row">';
        //         appendstr+='<div class="col-sm-6 col-xs-12">';
        //         appendstr+='<label class="field-name">Section '+i+' Title</label>';
        //         appendstr+='<input type="text" class="form-control input-field" name="section[][v_title]" required>';
        //         appendstr+='</div>';

        //         appendstr+='<div class="col-sm-6 col-xs-12">';
        //         appendstr+='<label class="field-name">&nbsp;</label>';
        //         appendstr+='<button type="button" class="btn btn-add-remove removesection"> Remove section '+i+'</button>';
        //         appendstr+='</div>';

        //         appendstr+='</div>';
        //         appendstr+='</div>';

        //         appendstr+='</div>';
        //         appendstr+='<div class="row-form-field">';
        //         appendstr+='<div class="row">';
        //         appendstr+='<div class="col-sm-6 col-xs-12">';
        //         appendstr+='<label class="field-name">Section '+i+' Video Title</label>';
        //         appendstr+='<input type="text" class="form-control input-field" name="section['+b+'][video][v_title][]" required>';
        //         appendstr+='</div>';
        //         appendstr+='<div class="col-sm-6 col-xs-12">';
        //         appendstr+='<label class="field-name">Section '+i+' Video</label>';
        //         appendstr+='<input type="text" class="form-control input-field" id="textvideoupload'+i+'">';
        //         appendstr+='<div class="checkbox-service">';
        //         appendstr+='<label class="checkbox check-boxright">';

        //         appendstr+='<input type="hidden" name="section['+b+'][video][i_preview][]" value="off">';
        //         appendstr+='<input type="checkbox" name="section['+b+'][video][i_preview][]">';
        //         appendstr+='<span class="subfield-name"> Preview this course</span>';
        //         appendstr+='</label>';
        //         appendstr+='</div>';
        //         appendstr+='<div class="clearfix"></div>';
        //         appendstr+='<div class="col-xs-6">';
        //         appendstr+='<div class="text-center">';
        //         appendstr+='<input type="file" class="browsebutton" name="section['+b+'][video][v_video][]" required id="videoupload'+i+'" onchange="workvideosection('+i+');"  />';
        //         appendstr+='<label id="fileupload" class="btn-editdetail" for="videoupload'+i+'">Browser</label>';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         appendstr+='<input type="hidden" id="videouploadcnt" value="1">';
        //         appendstr+='<div class="col-xs-6">';
        //         appendstr+='<div class="text-center">';
        //         appendstr+='<button type="button" class="btn form-add-course" onclick="addmorevideo('+i+')" > Add More</button>';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         appendstr+='<div id="sectionvideo'+i+'">';
        //         appendstr+='</div>';
        //         appendstr+='<div class="row-form-field">';
        //         appendstr+='<div class="row">';
        //         appendstr+='<div class="col-sm-6 col-xs-12">';
        //         appendstr+='<label class="field-name">Section '+i+' Doc Title</label>';
        //         appendstr+='<input type="text" class="form-control input-field" name="section['+b+'][doc][v_title][]" required>';
        //         appendstr+='</div>';
        //         appendstr+='<div class="col-sm-6 col-xs-12">';
        //         appendstr+='<label class="field-name">Section '+i+' Doc</label>';
        //         appendstr+='<input type="text" class="form-control input-field" id="textdocupload'+i+'">';
        //         appendstr+='<div class="col-xs-6">';
        //         appendstr+='<div class="text-center">';
        //         appendstr+='<input type="file" class="browsebutton" name="section['+b+'][doc][v_doc][]" id="docupload'+i+'" onchange="workdocsection('+i+');" />';
        //         appendstr+='<label id="fileupload" class="btn-editdetail" for="docupload'+i+'">Browser';
        //         appendstr+='</label>';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         appendstr+='<input type="hidden" id="docuploadcnt" value="1">';
        //         appendstr+='<div class="col-xs-6">  ';
        //         appendstr+='<div class="text-center">';
        //         appendstr+='<button type="button" class="btn form-add-course" onclick="addmoredoc('+i+')"> Add More';
        //         appendstr+='</button>';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         appendstr+='<div id="sectiondoc'+i+'">';
        //         appendstr+='</div>';
        //         appendstr+='</div>';
        //         $("#coursessectionmorediv").append(appendstr);
        // }        
          

        function pricevalidation(){
            if($("#v_course_free").prop('checked') == true){
                $("#f_price").removeAttr("required");
                $("#f_price").removeAttr("min");
                $("#f_price").val("");
            }else{
                $("#f_price").attr("required","true");
                $("#f_price_base").attr("min","5");
            }

        }


       

</script>


<script>

var $topLoader = $("#topLoader").percentageLoader({width: 256, height: 256, controllable : true, progress : 0.5, onProgressUpdate : function(val) {
  $topLoader.setValue(Math.round(val * 100.0));
}});
var topLoaderRunning = false;
          
function LoaderPercentStart(total){
    if (topLoaderRunning) {
      return;
    }
    topLoaderRunning = true;
    $topLoader.setProgress(0);
    $topLoader.setValue('0kb');
    var kb = 0;
    var totalKb = total;
    
    var animateFunc = function() {
      kb += 17;
      $topLoader.setProgress(kb / totalKb);
      $topLoader.setValue(kb.toString() + 'kb');
      
      if (kb < totalKb) {
        setTimeout(animateFunc, 25);
      } else {
        topLoaderRunning = false;
      }
    }
    $(".topLoaderinner").css("display","block");
    $("#topLoader").append("<p style='font-size:20px'>This could take sometime...</p>");
    $("#topLoader").append("<p style='font-size:20px'>Please do not refresh the page.</p>");
    setTimeout(animateFunc, 25);
}

function LoaderPercentStop(){    
    $(".topLoaderinner").css("display","none");
}
</script>


@stop

