@extends('layouts.frontend')

@section('content')


 <!-- 26B-control-panel-my-courses-settings-seller -->
    <div class="title-controlpanel controlpanel_setting_icon">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> Course Settings </h1>
                </div>
                <div class="right-symbol">
                    <a href="{{url('courses/settings')}}/{{$data->id}}" class="setting-course">
                        <img src="{{url('public/Assets/frontend/images/setting-course.png')}}" class="setting-course" alt="" />
                    </a>
                    <a href="{{url('courses/messages')}}/{{$data->id}}"> <img src="{{url('public/Assets/frontend/images/mail.png')}}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>

    <form role="form" method="POST" name="coursessettings" id="coursessettings" action="{{url('courses/update-settings')}}" data-parsley-validate enctype="multipart/form-data" >
    {{ csrf_field() }}
    <div class="container">
        <div class="main-course-setting">

            @if ($success = Session::get('success'))
              <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 20px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
              </div>
            @endif
            @if ($warning = Session::get('warning'))
              <div class="alert alert-info alert-danger" style="margin-top: 20px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
              </div>
            @endif

            <div class="section-enable">
                <div class="heading-setting">
                    Enable/disable certain course sections from students
                </div>
                <input type="hidden" name="i_courses_id" value="{{$data->id}}">    
                <div class="row">
                    <div class="col-sm-4 text-center-xs">
                        <label class="checkbox">
                            <input type="hidden" name="e_qa_enabled" value="off">
                            <input type="checkbox" name="e_qa_enabled" value="on"  @if(isset($data->e_qa_enabled) && $data->e_qa_enabled=="off")  @else checked  @endif >
                            <span class="setting-option"> 
                                Q&A
                            </span>
                        </label>
                    </div>
                    
                    <div class="col-sm-4 text-center-xs">
                        <label class="checkbox">
                            <input type="hidden" name="e_reviews_enabled" value="off">
                            <input type="checkbox" name="e_reviews_enabled" value="on" @if(isset($data->e_reviews_enabled) && $data->e_reviews_enabled=="off")  @else checked  @endif>
                            <span class="setting-option"> 
                                Reviews 
                            </span>
                        </label>
                    </div>
                
                    <div class="col-sm-4 text-center-xs">
                        <label class="checkbox">
                            <input type="hidden" name="e_announcements_enabled" value="off" >    
                            <input type="checkbox" name="e_announcements_enabled" value="on" @if(isset($data->e_announcements_enabled) && $data->e_announcements_enabled=="off")  @else checked  @endif >
                            <span class="setting-option"> 
                                Announcements
                            </span>
                        </label>
                    </div>
                </div>

            </div>
            <div class="btn-settingcourse">
                <button type="submit" class="btn btn-savechange"> Save Changes </button>
            </div>
        </div>
    </div>
    </form>


    <!-- 26B-control-panel-my-courses-settings-seller -->

@stop
@section('js')

<script type="text/javascript">

</script>

@stop

