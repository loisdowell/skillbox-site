@extends('layouts.frontend')

@section('content')
    
    <style type="text/css">
        .modal-new .modal-backdrop{z-index: 0;}
        .btn-new-sponsore{
            max-width: 280px !important;
        }
    </style>

     @php
        $standardplanFree= App\Helpers\General::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
    @endphp 

    <div id="viewdatajobs" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align: center;">Sponsored Ad Stats</h4>
          </div>
          <div class="modal-body">
            <p id="popupmessage" style="text-align: center;"></p>
            
            <div class="contant-table">
                <div class="table-responsive">
                    <table class="table table-hover table-sponsoreads s">
                        <thead>
                            <tr>
                                <th>TOTAL AD SALES</th>
                                <th>TOTAL AD IMPRESSIONS</th>
                                <th>TOTAL SPONSORED CLICKS</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="SALESPOP">0</td>
                            <td id="IMPRESSIONSPOP">0</td>
                            <td id="CLICKSPOP">0</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!--modal-->
    <div id="upgradePlanModal" class="modal1 upgradePlanModal_custom" role="dialog">
        <!-- Modal content -->
        <div class="modal-content2">
            <div class="final-containt">
                <span class="close2"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" alt=""  onclick="upgradePlanModalClose()" /></span>
            </div>
            <div class="container">
                <div class="main-dirservice-popup">
                    
                    <form class="horizontal-form" role="form" method="POST" name="planform" id="planform" action="{{url('sponsor-courses/upgrade-user-plan')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <input type="hidden" name="i_payment_cid" id="i_payment_cid" value="">

                    <div class="popup-dirservice">
                        <div class="title-dirservice">
                            <h4> Upgrade Your Plan </h4>
                        </div>
                        <div class="foll-option-popup">
                            You're currently a {{isset($userplandata['planName']) ? $userplandata['planName']:'' }} member. Please upgrade your Plan.
                        </div>
                        
                        <div class="wrap">
                            <fieldset>
                                <div class="toggle">
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join" checked onchange="plandurationchange('monthly')" >
                                    <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create" onchange="plandurationchange('yearly')">
                                    <label for="create" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    @if(count($plandata))
                        @foreach($plandata as $key=>$val)
                            @if($key!=0)
                            <div class="final-leval-popup">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="@if($key==0)left-choise @else left-side @endif">
                                            <div class="reg">
                                                <bdo dir="ltr">
                                                    <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required>
                                                    <span class="select-seller-job"></span>
                                                    <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                </bdo>
                                            </div>
                                            <div class="choise-que">{{$val['v_subtitle']}}</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        
                                        <ul class="list-unstyled choise-avalible">
                                            <?php 
                                                    $lcnt=1;
                                                ?>
                                                @if(count($val['l_bullet']))
                                                    @foreach($val['l_bullet'] as $k=>$v) 
                                                        @if($k<3)
                                                        <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                        <?php $lcnt = $lcnt+1; ?>
                                                        @endif
                                                    @endforeach
                                                @endif  
                                        </ul>

                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="right-display">
                                            <ul class="list-unstyled choise-avalible">
                                                <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k>=3)
                                                            <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                            @endif
                                                            <?php $lcnt = $lcnt+1; ?>
                                                        @endforeach
                                                    @endif  

                                                </ul>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-12 same-height-1">
                                        <div class="@if($key==0) free-price @else final-price @endif">
                                            <div class="all-in-data">
                                                <div  class="monthyprice">
                                                       @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_monthly_price']}}</del>
                                                            </div>
                                                            offer £{{$val['f_monthly_dis_price']}} per month

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif
                                                       @endif  
                                                </div>  
                                                
                                                <div class="yealryprice" style="display: none;">
                                                       @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_yealry_price']}}</del>
                                                            </div>
                                                            offer £{{$val['f_yealry_dis_price']}} per yearly

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif

                                                       @endif  
                                                </div>    
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    @endif        

                    <button type="submit" class="btn btn-payment">
                        Proceed
                    </button>

                    </form>

                </div>
                <!-- End 26B-my-profile-with-sponsor-button-POP UP-in-person-or-online -->

            </div>
        </div>
    </div>
    <!--end-modal-->

    <div class="modal fade" id="sponsorNow" role="dialog">
        <div class="modal-dialog modal-dialog-popup" style="max-width:35% !important">
            <!-- Modal content-->
            <div class="modal-content modal-content-popup">
                <div class="modal-header modal-header-bottom">
                </div>
                <div class="modal-body">
                    <div id="commonpopupmsg"></div>    
                    <div class="popup-sponsore">
                        <div class="title-control-postion">
                            <div class="title-popup-table">
                                <h3>Are you sure you want to sponsor this course ? </h3>
                            </div>
                            <input type="hidden" name="cid" id="cid">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel & Close</button> &nbsp;&nbsp;&nbsp;&nbsp;                                         
                                    <button type="button" class="btn btn-success ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="SponsorNowJob" onclick="SponsorNow()">Sponsor Now</button>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                    <br/>
                    <div id="commonerrpopup">
                    </div>
                </div>
                <div class="modal-footer modal-footer-top">
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addNewSponsoredCourse" role="dialog">
        <div class="modal-dialog modal-dialog-popup">
            <!-- Modal content-->

            <div class="modal-content modal-content-popup modal-content-1">
                <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-bottom modal-header-1">
                </div>
                <div class="modal-body">
                    <div class="popup-sponsore">
                        <div class="title-control-postion">
                            <div class="title-popup-table">
                                <h3> Add New Sponsored Courses </h3>
                            </div>
                            <div class="contant-table">
                                <table class="table table-hover table-sponsoreads">
                                    <thead>
                                        <tr>
                                            <th>COURSE TITLE</th>
                                            <th>POSTING DATE</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody id="newsponsoradsdata">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-top">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->




    <!-- 26B-sponsored-ads -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                
                <div class="left-supportbtn">
                    <a href="{{url('seller/dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control panel</button>
                    </a>
                </div>

                <div class="title-support">
                    <h1> My Sponsored Courses </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="content">
            <div class="tab-pane active" id="completed">
                <div class="order-status">

                     @if ($success = Session::get('success'))
                      <div class="alert alert-success alert-big alert-dismissable br-5">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
                      </div>
                    @endif
                    @if ($warning = Session::get('warning'))
                      <div class="alert alert-info alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
                      </div>
                    @endif

                    <div id="commonmsg"></div>


                    <div class="alert alert-info alert-danger" id="sponsorerrmsg" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span>Something went wrong.please try again after some time.</span>
                    </div>

                    <div class="alert alert-info alert-success" id="commonsucc" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span id="commonsuccmsg">Thank you, your message is now sponsored.</span>
                    </div>

                    <div class="alert alert-info alert-danger" id="commonerr" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span id="commonerrmsg">Something went wrong.please try again after some time.</span>
                    </div>

                    <div class="title-sponsoreads">
                        Sponsored Courses Stats
                        <button type="button" class="btn btn-new-sponsore" onclick="NewSponsoredCourse()"> Add New Sponsored Courses
                        </button>
                        <div class="clearfix"></div>
                    </div>

                    @php
                        $IMPRESSIONS=0;
                        $CONTACTS=0;
                        $CLICKS=0;

                        if(count($data)){
                            foreach($data as $key=>$val){
                                if(isset($val->i_impression)){
                                    $IMPRESSIONS = $IMPRESSIONS+$val->i_impression;
                                }

                                if(isset($val->i_contact)){
                                    $CONTACTS = $CONTACTS+$val->i_contact;
                                }

                                if(isset($val->i_clicks)){
                                    $CLICKS = $CLICKS+$val->i_clicks;
                                }
                            }
                        }
                    @endphp

                    
                    <div class="order-details order-details2">
                        <div class="col-sm-3 no-padding">
                            <div class="right-boderstyle">
                                TOTAL SPONSORED COURSE SALES
                                <p> £{{number_format($sponsoredSales)}}</p>
                            </div>
                        </div>
                        <div class="col-sm-3 no-padding">
                            <div class="right-boderstyle">
                                TOTAL COURSE IMPRESSIONS
                                <p class="extra-pad"> {{number_format($IMPRESSIONS)}} </p>
                            </div>
                        </div>
                        {{-- <div class="col-sm-3 no-padding">
                            <div class="right-boderstyle">
                                TOTAL COURSE CONTACTS
                                <p class="extra-pad"> {{number_format($CONTACTS)}} </p>
                            </div>
                        </div> --}}
                        <div class="col-sm-3 no-padding">
                            <div class="right-boderstyle last-chlidhead">
                                TOTAL COURSE CLICKS
                                <p class="extra-pad"> {{number_format($CLICKS)}} </p>
                            </div>
                        </div>
                    </div>

                    <div class="contant-table">
                        <div class="table-responsive">
                            <table class="table table-hover table-sponsoreads">
                                <thead>
                                    <tr>
                                        <th>COURSE TITLE</th>
                                        <th>POSTING DATE</th>
                                        <th>SPONSORED</th>
                                    </tr>
                                </thead>

                                <tbody>
                                @if(count($data))
                                    @foreach($data as $key=>$val)
                                    <tr>
                                        <td><a href="javascript:;" onclick="ViewData('{{$val->id}}')">{{isset($val->v_title) ? $val->v_title:''}}</a></td>
                                        <td>{{isset($val->d_added) ? date("d/m/Y",strtotime($val->d_added)):''}}</td>
                                        <td>
                                            <div id="statusdivid_{{$val->id}}">    
                                            @if(isset($val->e_sponsor_status) && $val->e_sponsor_status=="start")
                                                <button type="button" class="btn btn-pause" onclick="SponsoreStatusChange('{{$val->id}}','pause')" >Pause</button>
                                            @else
                                                <button type="button" class="btn btn-start" onclick="SponsoreStatusChange('{{$val->id}}','start')">Start</button>
                                            @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3"> Currently, there are no Sponsored Courses available.</td>
                                    </tr>
                                @endif
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End 26B-sponsored-ads -->

        </div>
    </div> 
   


@stop
@section('js')

<script type="text/javascript">

        function ViewData(id=0){
            var actionurl = "{{url('sponsor-courses/get-view-data')}}";
            var formdata = "i_course_id="+id;
            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#IMPRESSIONSPOP").html(obj['impression']);
                        $("#SALESPOP").html(obj['sales']);                    
                        $("#CLICKSPOP").html(obj['click']);
                        $("#viewdatajobs").modal("show");
                    }else{
                        
                    }
                },
                error: function ( jqXHR, exception ) {
                    
                }
            });

            
        } 

      function NewSponsoredCourse(){
            
            var actionurl = "{{url('sponsor-courses/new')}}";
            var formdata = "";

            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                     
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#newsponsoradsdata").html(obj['htmlstr']);
                        $("#addNewSponsoredCourse").modal("show");

                    }else{
                        $("#sponsorerrmsg").show();
                        $("#sponsorerrmsg").html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    $("#sponsorerrmsg").show();
                }
            });
      }

      function SponsorNowModal(jid=""){
            
            $("#addNewSponsoredCourse").modal("hide");
            $("#cid").val(jid);
            $("#sponsorNow").modal("show");

      }

      function SponsorNow(){
                
            var actionurl = "{{url('sponsor-courses/add-sponsor')}}";
            var cid = $("#cid").val();    
            var formdata = "i_course_id="+cid;

            var l = Ladda.create(document.getElementById('SponsorNowJob'));
            l.start();  

            $.ajax({
                type    : "get",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    l.stop();
                    
                    var obj = jQuery.parseJSON(res);
                    //$("#sponsorNow").modal("hide");
                    if(obj['status']==1){
                        window.location = "{{url('sponsor-courses')}}";
                    }else if(obj['status']==2){
                        $("#commonerrpopup").html(obj['msg']);
                    }else{
                        $("#commonerrpopup").html(obj['msg']);
                        $("#commonerrpopup").html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    l.stop();
                    $("#commonerrpopup").css("display","block");
                    $("#commonerrpopup").html('<div class="alert alert-info alert-danger" id="commonerrpopup"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong><i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;<span id="commonerrmsg">Something went wrong.please try again after some time.</span></div>');
                }

            });
      }
      

      function upgradeUserPlan(cid=""){
        
            $("#i_payment_cid").val(cid);
            $("#sponsorNow").modal("hide");
            $("#upgradePlanModal").modal("show");
            

      }

       function plandurationchange(data=""){
        
        if(data=="monthly"){
            $(".monthyprice").css("display","block");
            $(".yealryprice").css("display","none");

        }else if(data=="yearly"){
            $(".yealryprice").css("display","block");
            $(".monthyprice").css("display","none");
        }

    }


      function SponsoreStatusChange(jid="",status=""){
            
            var actionurl = "{{url('sponsor-courses/sponsor-status')}}";
            var formdata = "i_course_id="+jid+"&status="+status;

            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                   
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#statusdivid_"+jid).html(obj['btnstr']);
                        $("#commonmsg").html(obj['msg']);
                    }else{
                        $("#commonmsg").html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    $("#commonerr").show();
                    $("#commonerrmsg").html(obj['msg']);
                }
            });

      }

      function upgradePlanModalClose(){
            $("#upgradePlanModal").modal("hide");
        }

</script>

@stop

