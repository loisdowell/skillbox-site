<div class="add-chapter">
    <div class="heading-coursesection">
        <h2> Course Sections  </h2>
    </div>
    
    <div id="section1">
        <div class="form-field">
            <div class="row-form-field">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <label class="field-name">
                            Section {{$sectionnumber}} Title
                        </label>
                        <input type="text" class="form-control input-field" name="section[0][v_title]" required value="{{$sectiondata['v_title'] or ''}}">
                    </div>
                </div>
            </div>
        </div>

        @if(isset($sectiondata['video']) && count($sectiondata['video']))
            @foreach($sectiondata['video'] as $k=>$v)

                <div class="row-form-field @if($k>0) sectionvideoremove @endif" @if($k>0) style="border-top: 1px solid #c7c7c7;padding-bottom: 24px;" @endif>
                    <div class="row" @if($k>0) style="margin-top:25px" @endif>
                        @if($k>0)
                         <div class="col-sm-12 col-xs-12">
                            <button type="button" class="btn btn-add-remove remsectionvideo" style="margin-top: 3px;float:right">Remove video</button>
                        </div>
                        @endif
                        <div class="col-sm-6 col-xs-12">
                            <label class="field-name">
                                Section {{$sectionnumber}} Video Title
                            </label>
                            <input type="text" class="form-control input-field" name="section[0][video][{{$k}}][v_title]" required value="{{$v['v_title'] or ''}}">
                        </div>
                        <input type="hidden" name="section[0][video][{{$k}}][_id]" value="{{$v['_id']}}">
                        
                        <div class="col-sm-6 col-xs-12">
                            <label class="field-name">
                                Section {{$sectionnumber}} Video
                            </label>

                            @php 
                                $namedata = array();
                                if($v['v_video']!=""){
                                   $namedata = explode("/", $v['v_video']);
                                }
                                $videotitle=$v['v_video'];
                                if(count($namedata)){
                                    $videotitle = $namedata[count($namedata)-1]; 
                                }

                            @endphp

                            <input type="text" class="form-control input-field" id="textvideouploaded{{$k}}" readonly value="{{$videotitle or ''}}">
                            <span>Supported formats: mp4, mkv, webm, mov, m4v <br></span>
                            <span>Max file size: 100 MB <br></span>
                            <span class="videoerr" style="color: red">Video format not supported <br></span>
                            <span class="videoerrsize" style="color: red">File size should not be more than 100 MB<br></span>
                            <div class="col-xs-12">
                                <div class="text-right">
                                    <input type='file' accept=".mp4, .mkv, .webm, .mov, .m4v" class="browsebutton" name="section[0][video][{{$k}}][v_video]" id="videouploaded{{$k}}" onchange="workvideo('videouploaded{{$k}}');" @if($v['v_video']=="") required @endif />
                                    <label id="fileupload" class="btn-editdetail" for="videouploaded{{$k}}">Browser</label>
                                </div>
                            </div>

                            <div class="checkbox-service">
                                <label class="checkbox check-boxright">
                                    <input type="hidden" name="section[0][video][{{$k}}][i_preview]" value="off">
                                    <input type="checkbox" name="section[0][video][{{$k}}][i_preview]" @if($v['i_preview']=="on") checked @endif>
                                    <span class="subfield-name"> 
                                        Preview this course
                                    </span>
                                </label>
                            </div>
                            
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <label class="field-name">
                                Video duration 
                            </label>
                            <div class="row">
                                <div class="col-md-6"><input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field col-md-6" name="section[0][video][{{$k}}][v_duration_min]"  value="{{$v['v_duration_min'] or '00'}}"><div class="text-center">Minutes</div></div>
                                <div class="col-md-6"><input type="number" min="0" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true"  class="form-control input-field col-md-6" name="section[0][video][{{$k}}][v_duration_sec]" value="{{$v['v_duration_sec'] or ''}}"><div class="text-center">Seconds</div></div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <label class="field-name">
                                Section {{$sectionnumber}} Doc
                            </label>

                            @php 
                                $namedata = array();
                                if(isset($v['v_doc']) && $v['v_doc']!=""){
                                   $namedata = explode("/", $v['v_doc']);
                                }
                                $doctitle="";
                                if(isset($v['v_doc']) && $v['v_doc']!=""){
                                    $doctitle=$v['v_doc'];
                                }
                                if(count($namedata)){
                                    $doctitle = $namedata[count($namedata)-1]; 
                                }

                            @endphp


                            <input type="text" class="form-control input-field" id="textdocuploaded{{$k}}" readonly value="{{$doctitle or ''}}">
                            <span>Supported formats: pdf, doc, docx, txt<br></span>
                            <span>Max file size: 10 MB <br></span>
                            <span class="docerr" style="color: red">Document format not supported.<br></span>
                            <span class="videoerrsize" style="color: red">File size should not be more than 10 MB<br></span>
                            <div class="col-xs-12">
                                <div class="text-right">
                                    <input type='file' accept=".pdf, .docx, .txt, .doc" class="browsebutton" name="section[0][video][{{$k}}][v_doc]" id="docuploaded{{$k}}" onchange="workvideodoc('docuploaded{{$k}}');"  />
                                    <label id="fileupload" class="btn-editdetail" for="docuploaded{{$k}}">Browser
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach
        @endif
        <div id="sectionvideoedit1"></div>

        <input type="hidden" id="videouploadcnt1" value="{{count($sectiondata['video'])}}">
        <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="text-right">
                <button type="button" class="btn form-add-course" onclick="addmorevideo('1','{{$sectionnumber}}')" > Add More Video</button>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
        

    </div>

    {{-- </div> --}}
    <div id="coursessectionmorediv">
    </div>
</div>