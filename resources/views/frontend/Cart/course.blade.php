@extends('layouts.frontend')


@section('content')
    <style type="text/css">
        .margin-right{
            margin-right: 10px
        }
    </style>

    
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                {{-- <div class="left-supportbtn">
                    <a href="{{url('buyer/courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Order </button>
                    </a>
                </div> --}}
                <div class="title-support">
                    <h1> Order Details </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="commonmsg"></div>
        <div class="chapter-title">
            <h5> Your Orders </h5>
        </div>
        <div class="border-tab">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="Product-th">Product</th>
                        <th class="Price-th">Price</th>
                        <th class="Price-th">Quantity</th>
                        <th class="Price-th">Total</th>
                        <th class=""></th>
                    </tr>
                </thead>

                <tbody>
                    @php
                        $Subtotal=0;
                        $Total=0;
                    @endphp
                    @if(sizeof(Cart::content()) > 0)
                        @foreach (Cart::content() as $item)
                            @php
                                $imgdata = \Storage::cloud()->url($item->options->v_instructor_image);
                                $Subtotal = $Subtotal+$item->price;
                            @endphp
                            <tr id="{{$item->rowId}}">
                                <td>
                                    <img class="imtab" src="{{$imgdata}}" alt="" />
                                    <span class="imtab-text">{{$item->name}}</span>
                                </td>
                                <td>£{{$item->price}}</td>
                                <td>
                                    <input type="number" class="form-control no-name" value="1" name="number" readonly>
                                </td>
                                <td>£{{$item->price}}</td>
                                <td>
                                    <a href="{{url('cart/course/remove')}}/{{$item->rowId}}">
                                        <img src="{{url('public/Assets/frontend/images/closep.png')}}" alt="" class="close-img" />
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" align="center">Currently, there are no items available in your cart..</td>
                        </tr>    


                    @endif
                </tbody>
            </table>
        </div>
        
        @if(sizeof(Cart::content()) > 0)
        <div class="row">
            <div class="col-xs-12 col-sm-offset-5 col-sm-7">
                <div class="heading-total">
                    <h3>order total</h3>
                </div>
                <div class="border-tab border-tab-1">
                    <table class="table table-bordered table-checkout">
                        <tbody>
                            <tr>
                                <td>Subtotal</td>
                                <td class="two-th"> £{{$Subtotal}}</td>
                            </tr>
                            @php
                                $comission = number_format($Subtotal*$comission/100,2);
                                $processing = number_format($processing,2);
                            @endphp
                            <tr>
                                <td>Skillbox Commission</td>
                                <td class="two-th"> £{{$comission}}</td>
                            </tr>
                            <tr>
                                <td>processing fee</td>
                                <td class="two-th"> £{{$processing}}</td>
                            </tr>
                            @php
                                $Total= $Subtotal+$comission+$processing;    
                                $Total = number_format($Total,2);
                            @endphp
                            <tr>
                                <td class="two-th1"> Total </td>
                                <td class="two-th"> £{{$Total}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="update-cart">
                    
                    <a href="{{url('cart/course/mangopay')}}">
                        <button class="btn btn-post-update ">Proceed to payment</button>
                    </a>

                    {{--
                    <a href="{{url('cart/course/payment/paypal')}}">
                    <button class="btn btn-post-update ">
                        Payment with paypal
                    </button>
                    </a>
                    <button class="btn btn-post-update margin-right">
                        Payment with escrow
                    </button>
                    --}}    

                </div>
            </div>
        </div>
        @endif

    </div>


@stop
@section('js')
<script type="text/javascript">

function removeItem(data) {
    
    var actionurl = "{{url('cart/course/remove')}}";
    var formdata = "rowid="+data; 

    $.ajax({
        type    : "GET",
        url     : actionurl,
        data    : formdata,
        success : function( res ){
            var obj = jQuery.parseJSON(res);
            if(obj['status']==1){
                $("#commonmsg").html(obj['msg'])
                $("#"+data).remove()
            }else{
                $("#commonmsg").html(obj['msg'])
            }
        },
        error: function ( jqXHR, exception ) {
            
         }
    });


}

</script>
@stop

