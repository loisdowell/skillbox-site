@extends('layouts.frontend')

@section('content')
    <style type="text/css">
        .form-msg {position: relative;}
        .form-msg .parsley-required{position: absolute;bottom: -16px;left: 18px}
    </style>
   
   <!-- 26B-control-panel-support-2-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button></a>
                </div>
                <div class="title-support">
                    <h1> Support </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-support">
                    <div class="heading-support">
                        Create New Ticket
                    </div>

                    <form class="horizontal-form" role="form" method="POST" name="buyersignupform" id="buyersignupform" action="{{url('support/add')}}" data-parsley-validate enctype="multipart/form-data" >

                    {{ csrf_field() }}    
                    <div class="support-containt">
                        <div class="row">
                            <div class="col-xs-12 form-msg">
                                <div class="field-support">
                                    Select the field regarding your query
                                </div>
                                
                                    @if(count($supportquery))
                                        @foreach($supportquery as $k=>$v)
                                            <div class="reg reg_fill">
                                                <bdo dir="ltr">
                                                    <input type="radio" value="{{$k}}" name="e_query" required="">
                                                    <span></span>
                                                    <abbr class="support-query">{{$v}}</abbr>
                                                </bdo>
                                            </div>
                                        @endforeach
                                    @endif        
                            </div>
                        </div>

                        <div class="row-field-support">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <label class="field-edit-name">
                                        Select Category
                                    </label>
                                    <select class="form-control form-sell" name="e_type" required="">
                                        <option value="inperson" >In Person</option>
                                        <option value="inperson" >Online</option>
                                        <option value="inperson" >Course</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <label class="field-name">
                                        Ask question
                                    </label>
                                    <textarea name="l_question" rows="7" class="form-control cover-letter" required=""></textarea>
                                </div>

                                <div class="">
                                <input type='file' name="v_document[]" multiple id="fileupload-down" style="display: none;" onchange="docuploaddata();" />
                                <label id="fileupload-down" for="fileupload-down" style="margin-top: 10px;margin-left: 10px;"><img src="{{url('public/Assets/frontend/images/link-save.png')}}" alt="" />Attach document</label>
                            </div>
                            </div>
                            <ul class="" id="docupload">
                            </ul>
                        </div>
                        <script type="text/javascript">
                            function docuploaddata(){
                                var input = document.getElementById('fileupload-down');
                                var list = document.getElementById('docupload');
                                var strappend="";
                                for (var x = 0; x < input.files.length; x++) {
                                    strappend = strappend+"<li>"+input.files[x].name+"</li>";
                                }
                                $("#docupload").html(strappend);
                            }
                        </script>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="field-support">
                                    Select priority
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-msg">
                                
                                    <div class="reg reg_fill">
                                        <bdo dir="ltr">
                                            <input type="radio" value="low" name="e_priority" required="">
                                            <span></span>
                                            <abbr class="support-priority"> Low </abbr>
                                        </bdo>
                                    </div>
                                    <div class="reg reg_fill">
                                        <bdo dir="ltr">
                                            <input type="radio" value="medium" name="e_priority" required="">
                                            <span></span>
                                            <abbr class="support-priority"> Medium </abbr>
                                        </bdo>
                                    </div>

                                    <div class="reg reg_fill">
                                        <bdo dir="ltr">
                                            <input type="radio" value="high"  name="e_priority" required="">
                                            <span></span>
                                            <abbr class="support-priority"> High </abbr>
                                        </bdo>
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="right-btn">
                        <a href="{{url('support')}}">
                        <button type="button" class="btn form-save-exit"> Discard </button>
                        </a>
                        <button type="Submit" class="btn form-next"> Submit Ticket </button>
                    </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-support-2 -->
   

@stop
@section('js')

<script type="text/javascript">

</script>

@stop

