@extends('layouts.frontend')

@section('content')
    <style type="text/css">
        .letter-text-myprifile {
            text-align: center;
            font-size: 34px;
            padding: 10px 0px;
            color: rgb(231, 14, 138);
        }
    </style>


    <!-- 26B-control-panel-support-ticketing listing-conversation -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1>Support</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-support-ticket">
                    <div class="support-ticket">
                        Ticket Details
                        <a href="{{url('support')}}">
                        <button type="button" class="btn btn-supportcenter"> Back to Support Center </button>
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="contant-table">
                        <table class="table table-hover table-listconversation">
                            <thead>
                                <tr>
                                    <th> # </th>
                                    <th>CREATE DATE</th>
                                    <th>STATUS</th>
                                    <th>QUERY</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td> {{isset($supportdata->i_ticket_num) ? $supportdata->i_ticket_num : ''}} </td>
                                    <td> {{isset($supportdata->d_added) ? date("M d, Y",strtotime($supportdata->d_added)) : ''}} </td>
                                    <td> {{isset($supportdata->e_status) ? $supportdata->e_status : ''}} </td>
                                    <td> {{isset($supportquery[$supportdata->e_query]) ? $supportquery[$supportdata->e_query] : ''}} </td>
                                </tr>
                            </tbody>
                        </table>

                       

                    </div>

                    @if(isset($supportdata->v_document) && count($supportdata->v_document))
                        <div style="margin-top: 20px">
                        <label>Attached Document</label>    
                        <ul>
                            @foreach($supportdata->v_document as $k=>$v)
                            @php
                                $docurl = '';
                                if(Storage::disk('s3')->exists($v)){
                                    $docurl = \Storage::cloud()->url($v);
                                }
                            @endphp  

                            <li><a href="{{$docurl}}" download target="_blank" >{{$v}}</a></li>
                            @endforeach
                        </ul>
                       </div> 
                    @endif

                    <div class="conversation-detail">
                        <h3> Ticket Conversation Details </h3>
                        <div class="conversation-details-block">
                            <div class="accordion conversation-blog">
                                <div class="conversation-find">
                                    <div class="conversation-man">
                                        <?php 
                                            $cuserdata=array();
                                            $v_image="";
                                            if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_image)){
                                                $cuserdata['v_image'] = $supportdata->hasUser()->v_image;
                                                $v_image = $supportdata->hasUser()->v_image;
                                            }
                                            $v_fname="";
                                            if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_fname)){
                                                $cuserdata['v_fname'] = $supportdata->hasUser()->v_fname;
                                                $v_fname = $supportdata->hasUser()->v_fname;
                                            }
                                            $v_lname="";
                                            if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_lname)){
                                                $cuserdata['v_lname']=$supportdata->hasUser()->v_lname;
                                                $v_lname=$supportdata->hasUser()->v_lname;
                                            }
                                            $imgdata = \App\Helpers\General::userroundimage($cuserdata);
                                            echo $imgdata;
                                        ?>  
                                    </div>
                                    <div class="conversation-name">
                                        <p>{{$v_fname}} {{$v_lname}}</p>
                                        @php 
                                            $humanTime= App\Helpers\General::time_elapsed_string(strtotime($supportdata->d_added));
                                        @endphp
                                        <p><span>{{$humanTime}}</span></p>
                                    </div>
                                </div>
                                <div class="conversation-content">
                                    <div class="conversation-text">
                                        <div class="conversation-text-all">
                                            
                                            <p>
                                                <?php 
                                                    if(isset($supportdata->l_question)){
                                                        echo $supportdata->l_question;
                                                    }else{
                                                        echo "";
                                                    }
                                                ?>
                                            </p>    
                                        </div>

                                       <div id="commentdata"> 
                                        @if(count($commentdata))
                                            @foreach($commentdata as $k=>$v)
                                                @if(isset($v->e_user_type) && $v->e_user_type=="admin")
                                                <div class="Announcements-second-man">
                                                    <div class="Announcements-find">
                                                        <div class="Announcements-man">
                                                            <?php 
                                                                
                                                                $userdata=array();
                                                                if(count($v->hasAdmin())){
                                                                    $userdata=$v->hasAdmin();
                                                                }
                                                                $v_name="";
                                                                if(count($v->hasAdmin()) && isset($v->hasAdmin()->v_name)){
                                                                    $v_name = $v->hasAdmin()->v_name;
                                                                }
                                                                $imgdata = \App\Helpers\General::adminroundimage($userdata);
                                                                echo $imgdata;
                                                            ?>  
                                                        </div>
                                                        <div class="conversation-name">
                                                            <p>{{$v_name}}</p>
                                                            @php 
                                            $humanTime= App\Helpers\General::time_elapsed_string(strtotime($v->d_added));
                                        @endphp
                                                            <p><span>{{$humanTime}}</span></p>
                                                        </div>
                                                    </div>
                                                    <div class="conversation-text">
                                                        <div class="ticket-conversation-msg">
                                                            <div class="row">
                                                                <div class="col-sm-11">
                                                                    <p>
                                                                        <?php 
                                                                            if(isset($v->l_question)){
                                                                                echo $v->l_question;
                                                                            }else{
                                                                                echo "";
                                                                            }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                @else
                                                <div class="Announcements-second-man">
                                                    <div class="Announcements-find">
                                                        <div class="Announcements-man">
                                                            <?php 
                                            
                                                                $v_image="";
                                                                $userdata=array();
                                                                if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
                                                                    $v_image=$v->hasUser()->v_image;
                                                                    $userdata['v_image'] = $v->hasUser()->v_image;
                                                                }

                                                                $v_fname="";
                                                                if(count($v->hasUser()) && isset($v->hasUser()->v_fname)){
                                                                    $v_fname=$v->hasUser()->v_fname;
                                                                    $userdata['v_fname'] = $v->hasUser()->v_fname;
                                                                }

                                                                $v_lname="";
                                                                if(count($v->hasUser()) && isset($v->hasUser()->v_lname)){
                                                                    $v_lname=$v->hasUser()->v_lname;
                                                                    $userdata['v_lname'] = $v->hasUser()->v_lname;
                                                                }

                                                                $imgdata = \App\Helpers\General::userroundimage($userdata);
                                                                echo $imgdata;

                                                            ?>  
                                                        
                                                        </div>
                                                        <div class="conversation-name">
                                                            <p>{{$v_fname}} {{$v_lname}}</p>
                                                              @php 
                                                                $humanTime= App\Helpers\General::time_elapsed_string(strtotime($v->d_added));
                                                            @endphp
                                                            <p><span>{{$humanTime}}</span></p>
                                                        </div>
                                                    </div>

                                                    <div class="conversation-text">
                                                        <div class="ticket-conversation-msg">
                                                            <div class="row">
                                                                <div class="col-sm-11">
                                                                    <p>
                                                                        <?php 
                                                                            if(isset($v->l_question)){
                                                                                echo $v->l_question;
                                                                            }else{
                                                                                echo "";
                                                                            }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif


                                            @endforeach
                                        @endif    
                                        </div>

                                        <form class="horizontal-form" role="form" method="POST" name="Commnetform" id="Commnetform" data-parsley-validate enctype="multipart/form-data" >
                                        {{ csrf_field() }}
                                        <div class="conversation-second-man">
                                            <div class="conversation-find conversation-find-q">
                                                <div class="conversation-man">
                                                    @php
                                                        $imgdata = \App\Helpers\General::userroundimage($cuserdata);
                                                        echo $imgdata;    
                                                    @endphp
                                                    <input type="hidden" name="i_support_id" value="{{$supportdata->id}}">
                                                </div>
                                                <div class="conversation-name conversation-name-q">
                                                    <p>{{$v_fname}} {{$v_lname}}</p>
                                                    <textarea name="l_question" id="l_question" rows="3" class="form-control cover-letter" placeholder="Enter Your Commnet"></textarea>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="checkbox-ticket">Consider this request resolved
                                                                <input type="checkbox" class="ticket-conversation" name="e_status" @if(isset($supportdata->e_status) && $supportdata->e_status=="close") checked @endif>
                                                                <div class="checkmark"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 text-center">
                                                            <button type="button" onclick="submitcomment()" class="btn btn-convarsation-submit"> Submit </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </form>


                                          <div class="alert alert-success alert-big alert-dismissable br-5" style="display: none;" id="commonsucc">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span id="commonsuccmsg"></span>
                                          </div>
                                        
                                          <div class="alert alert-info alert-danger" style="display: none;" id="commonerr">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span id="commonerrmsg">Something went wrong.pleease try again after some time.</span>
                                          </div>
                                        


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-support-ticketing listing-conversation -->
   

@stop
@section('js')

<script type="text/javascript">

        function submitcomment() {
            
            var actionurl = "{{url('support/post-comment')}}";
            var formValidFalg = $("#Commnetform").parsley().validate('');
            document.getElementById('load').style.visibility="visible"; 
            if(formValidFalg){
                            
                var formdata = $("#Commnetform").serialize();

                $.ajax({
                    type    : "POST",
                    url     : actionurl,
                    data    : formdata,
                    success : function( res ){
                        document.getElementById('load').style.visibility='hidden';
                        var obj = jQuery.parseJSON(res);
                        if(obj['status']==1){
                            $('#commentdata').append(obj['commentstr']);
                            $('#commonsucc').css("display","block");
                            $('#commonsuccmsg').html(obj['msg']);
                            $("#l_question").val("");

                        }else{
                            $('#commonerr').css("display","block");
                            $('#commonerrmsg').html(obj['msg']);
                        }
                    },
                    error: function ( jqXHR, exception ) {
                        $('#commonerr').css("display","block");
                    }
                });
            }  

        }

</script>

@stop

