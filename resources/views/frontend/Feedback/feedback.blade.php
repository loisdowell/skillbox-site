@extends('layouts.frontend')

@section('content')
    

     <?php

       
        $bannertext="";    
        if(isset($cmsdata->v_banner_text)){
            $bannertext = $cmsdata->v_banner_text;
        }


    ?>


   <!-- 31B-feedback-1 -->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">
                <img src="{{$metaDetails['v_image'] or ''}}" class="img-responsive" alt="" />
                <div class="center-containt">
                    <div class="container">
                        <div class="applybox-notification">
                            <h1>{{$bannertext}}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-feedbackpage">
                    <?php
                            if(isset($cmsdata->l_description)){
                                echo $cmsdata->l_description;
                            }else{
                                echo "";
                            }
                    ?>
                </div>
            </div>
        </div>
    </div>                
   
@stop
@section('js')

<script type="text/javascript">

      function buyerSignup(){
          var formValidFalg = $("#buyersignupform").parsley().validate('');
          if(formValidFalg){
              var l = Ladda.create(document.getElementById('bsignup'));
              l.start();
              $("#buyersignupform").submit()
          } 
      }

</script>

@stop

