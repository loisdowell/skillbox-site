@extends('layouts.frontend')

@section('content')
   
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            </div>
            <div class="col-xs-12">
              <div class="thank_you">
                <h3 style="padding-top: 150px">Now, you have been unsubscribed from our skill notification mailing list. You will not receive any more notifications from us.</h3>
                <a href="{{url('')}}" class="btn btn-default">Continue to search</a>
              </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-support-ticketing listing -->
   
@stop
@section('js')

<script type="text/javascript">
</script>

@stop

