@extends('layouts.frontend')

@section('content')

<style type="text/css">
    .modal-new .modal-backdrop{z-index: 0;}
    .btn-basic{
        cursor: auto;
    }
    .dimg{
        max-width: 250px;
        max-height: 250px;
        margin: auto;
        overflow: hidden;
        margin-bottom: 30px;
    }
    .img-upload{
        padding-bottom: 0px; 
    }
</style>

    @php
        $standardplanFree= App\Helpers\General::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
    @endphp  

    <!--modal-->
    <div id="upgradePlanModal" class="modal1 upgradePlanModal_custom" role="dialog">
        <!-- Modal content -->
        <div class="modal-content2">
            <div class="final-containt">
                <span class="close2"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" alt=""  onclick="upgradePlanModalClose()" /></span>
            </div>
            <div class="container">
                <div class="main-dirservice-popup">
                    
                    <form class="horizontal-form" role="form" method="POST" name="planform" id="planform" action="{{url('seller/upgrade-user-plan')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}

                    <div class="popup-dirservice">
                        <div class="title-dirservice">
                            <h4> Upgrade Your Plan </h4>
                        </div>
                        <div class="foll-option-popup">
                            You're currently a {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }} member. Please upgrade your Plan.
                        </div>
                        
                        <div class="wrap">
                            <fieldset>
                                <div class="toggle">
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join2" checked onchange="plandurationchange('monthly')" >
                                    <label for="join2" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create2" onchange="plandurationchange('yearly')">
                                    <label for="create2" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    
                    @php
                        $pcnt=0;
                    @endphp

                    @if(count($plandata))
                        @foreach($plandata as $key=>$val)
                           
                            @if($userplandata['v_plan_id']=="5a65b757d3e8125e323c986a")
                                @php $pcnt=1; @endphp
                            @endif
                            @if($key>=$pcnt)
                            <div class="final-leval-popup matchHeights">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="@if($key==0)left-choise @else left-side @endif matchheight_div">
                                            <div class="reg">
                                                <label style="font-weight: normal;">
                                                <bdo dir="ltr">
                                                    <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required @if($val['id']== $userplandata['v_plan_id']) checked @endif >
                                                    <span class="select-seller-job"></span>
                                                    <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                </bdo>
                                                </label>
                                            </div>
                                            <div class="choise-que">{{$val['v_subtitle']}}</div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3 col-xs-12">
                                        <ul class="list-unstyled choise-avalible matchheight_div">
                                            <?php 
                                                    $lcnt=1;
                                                ?>
                                                @if(count($val['l_bullet']))
                                                    @foreach($val['l_bullet'] as $k=>$v) 
                                                        @if($k<3)
                                                        <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                        <?php $lcnt = $lcnt+1; ?>
                                                        @endif
                                                    @endforeach
                                                @endif  
                                        </ul>

                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="right-display">
                                            <ul class="list-unstyled choise-avalible matchheight_div">
                                                <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k>=3)
                                                            <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                            @endif
                                                            <?php $lcnt = $lcnt+1; ?>
                                                        @endforeach
                                                    @endif  

                                                </ul>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-12 same-height-1">
                                        <div class="@if($key==0) free-price @else final-price @endif matchheight_div">
                                            <div class="all-in-data">
                                                <div  class="monthyprice">
                                                       @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_monthly_price']}}</del>
                                                            </div>
                                                            Only £{{$val['f_monthly_dis_price']}} p/m

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif

                                                       @endif  
                                                </div>  
                                                
                                                <div class="yealryprice" style="display: none;">
                                                       @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_yealry_price']}}</del>
                                                            </div>
                                                            Only £{{$val['f_yealry_dis_price']}} p/a

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif

                                                       @endif  
                                                </div>    

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    @endif        

                    <button type="submit" class="btn btn-payment">
                        Proceed
                    </button>

                    </form>

                </div>
                <!-- End 26B-my-profile-with-sponsor-button-POP UP-in-person-or-online -->

            </div>
        </div>
    </div>
    <!--end-modal-->


    <div id="upgradePlanModalActive" class="modal1 upgradePlanModal_custom" role="dialog">
        <!-- Modal content -->
        <div class="modal-content2">
            <div class="final-containt">
                <span class="close2"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" alt=""  onclick="upgradePlanModalactiveClose()" /></span>
            </div>
            <div class="container">
                <div class="main-dirservice-popup">
                    
                    <form class="horizontal-form" role="form" method="POST" name="planformactive" id="planformactive" action="{{url('seller/activate-user-plan')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}

                    <div class="popup-dirservice">
                        <div class="title-dirservice">
                            <h4> Upgrade Your Plan </h4>
                        </div>
                        <div class="foll-option-popup">
                            You're currently a {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }} member. Please upgrade your Plan.
                        </div>
                        
                        <div class="wrap">
                            <fieldset>
                                <div class="toggle">
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join1" checked onchange="plandurationchange('monthly')" >
                                    <label for="join1" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create1" onchange="plandurationchange('yearly')">
                                    <label for="create1" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    @php
                        $pcnt=0;
                    @endphp

                    @if(count($plandata))
                        @foreach($plandata as $key=>$val)
                            
                            @if($userplandata['v_plan_id']=="5a65b757d3e8125e323c986a")
                                @php $pcnt=1; @endphp
                            @endif
                            @if($key>=$pcnt)
                            <div class="final-leval-popup matchHeights">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="@if($key==0)left-choise @else left-side @endif matchheight_div">
                                        
                                            <div class="reg">
                                                <label style="font-weight: normal;">
                                                <bdo dir="ltr">
                                                    <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" @if($userplandata['v_plan_id']==$val['id']) checked @endif required>
                                                    <span class="select-seller-job"></span>
                                                    <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                </bdo>
                                                </label>
                                            </div>
                                        
                                            <div class="choise-que">{{$val['v_subtitle']}}</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        
                                        <ul class="list-unstyled choise-avalible matchheight_div">
                                            <?php 
                                                    $lcnt=1;
                                                ?>
                                                @if(count($val['l_bullet']))
                                                    @foreach($val['l_bullet'] as $k=>$v) 
                                                        @if($k<3)
                                                        <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                        <?php $lcnt = $lcnt+1; ?>
                                                        @endif
                                                    @endforeach
                                                @endif  
                                        </ul>

                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="right-display matchheight_div">
                                            <ul class="list-unstyled choise-avalible">
                                                <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k>=3)
                                                            <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                            @endif
                                                            <?php $lcnt = $lcnt+1; ?>
                                                        @endforeach
                                                    @endif  

                                                </ul>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-12 same-height-1">
                                        <div class="@if($key==0) free-price @else final-price @endif matchheight_div">
                                            <div class="all-in-data">
                                                <div  class="monthyprice">
                                                       @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_monthly_price']}}</del>
                                                            </div>
                                                            Only £{{$val['f_monthly_dis_price']}} p/m

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif
                                                       @endif  
                                                </div>  
                                                
                                                <div class="yealryprice" style="display: none;">
                                                       @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_yealry_price']}}</del>
                                                            </div>
                                                            Only £{{$val['f_yealry_dis_price']}} p/a

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif
                                                       @endif  
                                                </div>    

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif

                        @endforeach
                    @endif        

                    <button type="submit" class="btn btn-payment">
                        Proceed
                    </button>

                    </form>

                </div>
                <!-- End 26B-my-profile-with-sponsor-button-POP UP-in-person-or-online -->

            </div>
        </div>
    </div>
    

   <!-- Navigation Bar -->
    <div class="navigation-bar">
        <div class="container">
            <div class="navigation-bar-postiion">
                
                <div class="right-tabing position-tabing">
                    <button type="button" class="btn btn-basic">{{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }}</button> 
                    @if(isset($userplandata['upgrade']) && $userplandata['upgrade']==1)
                    <button type="button" class="btn btn-upgrade" onclick="upgradeplanmodal()">UPGRADE</button>
                    @endif
                </div>


            </div>
            <ul id="tabs" class="nav nav-tabs nav-youraccount" data-tabs="tabs">
                <li><a href="{{url('dashboard')}}"> Home </a></li>
                <li><a href="{{url('buyer/dashboard')}}"> Your Buyer Account </a></li>
                <li class="active"><a href="{{url('seller/dashboard')}}"> Your Seller Account </a></li>
            </ul>
        </div>
    </div>

    <!-- End Navigation Bar -->

    <div class="container">
        <div id="content">
            <!-- 19B-my-profile-your-seller-account -->

            <div class="tab-pane">

                <div class="buyer-account">

                    @if ($success = Session::get('success'))
                      <div class="alert alert-success alert-big alert-dismissable br-5">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
                      </div>
                    @endif
                    @if ($warning = Session::get('warning'))
                      <div class="alert alert-info alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
                      </div>
                    @endif

                    <div id="commonmsg"></div>

                    <div class="row">

                        <div class="col-sm-4 col-xs-12">
                           
                             <form class="horizontal-form" role="form" method="POST" name="updatejobpost2" id="updatejobpost2" data-parsley-validate enctype="multipart/form-data" >
                                        {{ csrf_field() }}
                                <div class="upload-edit-detail">
                                    @if(isset($userdata->v_image) && $userdata->v_image!="")
                                        @php
                                            $imgurl = \Storage::cloud()->url($userdata->v_image);    
                                        @endphp 
                                        <div class="dimg"> 
                                        <img src="{{$imgurl}}" alt="Your Images" class="img-responsive img-upload" id="edit-images">
                                        </div>
                                    @else
                                        @php
                                            $imgurl = \Storage::cloud()->url('users/img-upload.png');    
                                        @endphp 
                                        <img src="{{$imgurl}}" alt="Your Images" class="img-responsive img-upload" id="edit-images">
                                    @endif

                                    @if(isset($leveldata) && count($leveldata))
                                     @php
                                        $levelcon="";
                                        if(isset($leveldata->v_icon) && $leveldata->v_icon!=""){
                                            $levelcon = \Storage::cloud()->url($leveldata->v_icon);
                                        }
                                     @endphp
                                     @if($levelcon!="")
                                     <div class="dashboardbadge">
                                        <img src="{{$levelcon}}" alt="" />
                                     </div>
                                     @endif
                                     @endif

                                    <input type='file' accept="image/x-png,image/gif,image/jpeg" name="v_image" id="fileupload-example-4" onchange="readURL(this);" />
                                    <label id="fileupload-example-4-label" for="fileupload-example-4">Upload Image</label>
                                    <center>
                                    <span style="margin-top: 10px;display: inline-block;">H : 290px , W : 250px <br></span>
                                    </center>
                                    <span class="videoerrsize" style="color: #ccc;text-align:center;margin-top: 10px">File size should not be more than 2 MB<br></span>
                                </div>
                            </form>

                            <div class="upload-msg">
                                            
                                            @if(isset($leveldata) && count($leveldata))
                                              <div class="msg-heading"> SKILLBOX Skill Level:</div>
                                              <button type="button" onclick="openleveldata()"  class="btn btn-level-details"> {{isset($leveldata->v_title) ? $leveldata->v_title : ''}} </button>

                                                {{-- <div class="modal fade" id="leveltextview" role="dialog">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content modal-content-1">
                                                          <button type="button" class="close closemodal" data-dismiss="modal"></button>
                                                            <div class="modal-header modal-header-1">
                                                                <h4 class="modal-title"> {{isset($leveldata->v_text_seller) ? $leveldata->v_text_seller : ''}} </h4>
                                                            </div>
                                                            <div class="modal-footer modal-footer-1">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}

                                                <div id="leveltextview" class="modal fade" role="dialog">
                                                  <div class="modal-dialog">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title" style="text-align: center;">{{isset($leveldata->v_title) ? $leveldata->v_title : ''}}</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                        <p>{{isset($leveldata->v_text_seller) ? $leveldata->v_text_seller : ''}}</p>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <script type="text/javascript">
                                                    function openleveldata(){
                                                        $("#leveltextview").modal("show");
                                                    }
                                                </script>
                                            @endif
                                        </div>


                            <div class="buyer-detail">
                                <p></p>
                                
                                <div class="wrap">
                                    <form>
                                        <fieldset>
                                            <div class="toggle">
                                                <input type="radio" class="toggle-input" onchange="changeService(this.value)" name="joincreate" value="inperson" id="join" @if(isset($userdata->seller['v_service']) && $userdata->seller['v_service']=="inperson") checked  @endif>
                                                <label for="join" class="toggle-label toggle-label-off" id="joinLabel"> In Person </label>
                                                <input type="radio" class="toggle-input" onchange="changeService(this.value)" name="joincreate" value="online" id="create" @if(isset($userdata->seller['v_service']) && $userdata->seller['v_service']=="online") checked  @endif >
                                                <label for="create" class="toggle-label toggle-label-on" id="createLabel"> Online </label>
                                                <span class="toggle-selection"></span>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>


                                {{-- @if(isset($userdata->seller['e_status']) && $userdata->seller['e_status']=="active") --}}
                                    
                                    <div class="profile-Status" id="accountstatus" @if(isset($userdata->seller['e_status']) && $userdata->seller['e_status']=="inactive") style="display: none"@endif>
                                        <p>PROFILE STATUS: <span>
                                        @if(isset($userplan['isBasic']) && $userplan['isBasic']==1)
                                            <img src="{{url('public/Assets/frontend/images/click.png')}}" alt="" /> ACTIVE (FREE)
                                        @else
                                            <img src="{{url('public/Assets/frontend/images/click.png')}}" alt="" /> ACTIVE
                                        @endif
                                        </span></p>
                                    </div>
                                {{-- @else --}}
                                    <div class="profile-Status" id="accountstatusbutton" @if(isset($userdata->seller['e_status']) && $userdata->seller['e_status']=="active") style="display: none"@endif >
                                        <button class="btn btn-active" type="button" onclick="activateProfile()">ACTIVATE PROFILE</button>
                                    </div>

                                {{-- @endif --}}

                                <style type="text/css">
                                </style>

                                <div class="profile-data  @if(isset($userdata->seller['e_status']) && $userdata->seller['e_status']!="active") menuvisibility @endif" >
                                    <a href="{{url('seller/my-profile')}}" disabled >
                                        <div class="data-find">
                                            <img src="{{url('public/Assets/frontend/images/user-03.png')}}" alt="" />
                                            <span>My<span id="servieprofile" style="margin-left: -1px">@if(isset($userdata->seller['v_service']) && $userdata->seller['v_service']=="inperson") In Person @else Online @endif</span> Profile</span>
                                        </div>
                                    </a>
                                    
                                    <a href="{{url('seller/my-wallet')}}">
                                        <div class="data-find">
                                            <img src="{{url('public/Assets/frontend/images/pound.png')}}" alt="" />
                                            <span> My Money</span>
                                        </div>
                                    </a>
                                    <a href="{{url('seller/shortlisted-jobs')}}">
                                        <div class="data-find">
                                            <img src="{{url('public/Assets/frontend/images/heart-03.png')}}" alt="" />
                                            <span>My Shortlisted Jobs</span>
                                        </div>
                                    </a>
                                    <a href="{{url('seller/applied-jobs')}}">
                                        <div class="data-find">
                                            <img src="{{url('public/Assets/frontend/images/job-applied.png')}}" alt="" />
                                            <span>Jobs Applied</span>
                                        </div>
                                    </a>
                                    <a href="{{url('seller/orders/active')}}">
                                        <div class="data-find">
                                            <img src="{{url('public/Assets/frontend/images/invoice-menu.png')}}" alt="" />
                                            <span>My Skill Orders</span>
                                        </div>
                                    </a>
                                    <div id="menudata" @if(isset($userdata->seller['v_service']) && $userdata->seller['v_service']=="inperson") style="display: none" @endif>
                                    <a href="{{url('courses')}}">
                                        <div class="data-find">
                                            <img src="{{url('public/Assets/frontend/images/course.png')}}" alt="" />
                                            <span>My Courses</span>
                                        </div>
                                    </a>
                                    <a href="{{url('sponsor-courses')}}">
                                        <div class="data-find">
                                            <img src="{{url('public/Assets/frontend/images/cup.png')}}" alt="" />
                                            <span>My Sponsored Courses</span>
                                        </div>
                                    </a>
                                    </div>
                                     <a href="{{url('seller/invoice/course')}}">
                                        <div class="data-find">
                                            <img src="{{url('public/Assets/frontend/images/invoice-menu.png')}}" alt="" />
                                            <span>My Sale Invoices</span>
                                        </div>
                                    </a>
                                </div>
                               
                               @if(isset($howitswork) && count($howitswork))
                                <div class="work-time">
                                    <h2>HOW IT WORKS</h2>
                                    <ul>
                                        @foreach($howitswork as $k=>$v)
                                                <li><span>{{$k+1}}) </span>{{$v->v_title}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                
                                @if(isset($fAQs) && count($fAQs))
                                    <div class="work-time">
                                        <h2>FAQs</h2>
                                        @foreach($fAQs as $key => $val)
                                            <p><a href="{{url('skillbox-faqs')}}?id={{$val->id or ''}}#heading{{$val->id or ''}}">{{$val->v_question or ''}}</a></p>
                                         @endforeach
                                        <a href="{{url('skillbox-faqs#seller')}}" class="btn btn-faq">See all FAQs</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-sm-8">
                            <div class="col-sm-4 col-xs-4">
                                <div class="pull-left">
                                <div class="text-left pull-left h3" id="accntpa" style="margin-top: 4px;margin-right: 10px;">@if(isset($userdata->e_profile_status) && $userdata->e_profile_status=="on") Account Paused @endif</div>
                                </div>
                            </div>
                            <div class="col-sm-8 col-xs-8">
                                <div class="pull-right">
                                <div class="text-right pull-left h3" style="margin-top: 4px;margin-right: 10px;">Holiday Mode </div>
                                <div class="pull-right">
                                <div class="wrap" style="width: 285px;">
                                    <form>
                                        <fieldset>
                                            
                                            <div class="toggle">
                                                <input type="radio" class="toggle-input" onchange="changeProfileData(this.value)" name="joincreate" value="on" id="joinprofle" @if(isset($userdata->e_profile_status) && $userdata->e_profile_status=="on") checked  @endif>
                                                <label for="joinprofle" class="toggle-label toggle-label-off" id="joinLabelprofile"> On </label>
                                                <input type="radio" class="toggle-input" onchange="changeProfileData(this.value)" name="joincreate" value="off" id="createprofile" @if(!isset($userdata->e_profile_status)) checked @endif  @if(isset($userdata->e_profile_status) && $userdata->e_profile_status!="on") checked  @endif >
                                                <label for="createprofile" class="toggle-label toggle-label-on" id="createLabelprofile"> Off </label>
                                                <span class="toggle-selection"></span>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div> 
                                </div>
                                </div>
                            </div>
                            <div class="buyer-reviews">

                                <div class="buyer-first">
                                    <div class="buyer-profile">
                                            <div class="buyer-profile">
                                                <h1>Reviews by Buyers</h1>
                                                 <a href="{{url('seller/review-center')}}" class="btn btn-review-center"> 
                                                    Review Centre
                                                </a>
                                                <div class="select-filter1">
                                                    <div class="select-filter">
                                                        <span>Filter by:</span>&nbsp;
                                                        <select style="width: auto !important" class="resizing_select1 arr_img" name="v_order_by" id="v_order_by_review" onchange="dashboardReviewOrder()">
                                                            <option value="DESC" selected>Latest First</option>
                                                            <option value="ASC">Oldest First</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                                
                                <div id="dashboardreviewdata">
                                </div>    
                                <input type="hidden" name="reviewpage" id="reviewpage" value="1" autocomplete="off" />
                                <div id="showmorebtn">
                                <div class="load-profile">
                                    <button class="btn btn-load" onclick="dashboardreview();">
                                        Load More Reviews (<span id="reviewcntdata">0</span>)
                                    </button>
                                </div>
                                </div>
                            </div>

                            <div class="buyer-reviews buyer-final">
                                <div class="buyer-first buyer-second">
                                    <div class="buyer-profile buyer-profile-one">
                                        <h2>Your Messages</h2>
                                        <a href="{{url('message/seller/message-centre')}}" class="btn btn-review-center"> 
                                            Message Centre 
                                        </a>
                                        <div class="select-filter1">
                                            <div class="select-filter">
                                                <span>Filter by:</span>&nbsp;
                                                <select style="width: auto !important" class="resizing_select3 arr_img" name="v_order_by" id="v_order_by_message" onchange="dashboardMessageOrder()">
                                                    <option value="DESC" selected>Latest First</option>
                                                    <option value="ASC">Oldest First</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="dashboardmessagedata">
                                </div>

                                <input type="hidden" name="messagepage" id="messagepage" value="1" autocomplete="off" />
                                <div id="showmorebtnmessage">
                                <div class="load-profile">
                                    <button class="btn btn-load" onclick="dashboardMessage();">
                                        Load More Messages (<span id="msgcntdata">0</span>)
                                    </button>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End 19B-my-profile-your-seller-account -->

        </div>
    </div>



   

     <!--modal-->
   

    <!--end-modal-->


@stop

@section('js')

<script type="text/javascript">
        
        function dashboardMessageOrder(){
            var v_order_by = $("#v_order_by_message").val();
            if(v_order_by=="ASC"){
                var $divs = $("div.message-first");
                var numericallyOrderedDivs = $divs.sort(function (a, b) {
                    var date1 = $(a).find("h2").text();
                    var date2 = $(b).find("h2").text();
                    return (date1 < date2) ? -1 : (date1 > date2) ? 1 : 0;
                    //return $(a).find("h2").text() > $(b).find("h2").text();
                });
                $("#mainordermessage").html(numericallyOrderedDivs);
            }else{
                var $divs = $("div.message-first");
                var numericallyOrderedDivs = $divs.sort(function (a, b) {
                    
                    var date1 = $(a).find("h2").text();
                    var date2 = $(b).find("h2").text();
                    return (date1 > date2) ? -1 : (date1 < date2) ? 1 : 0;
                    //return $(a).find("h2").text() < $(b).find("h2").text();
                });
                $("#mainordermessage").html(numericallyOrderedDivs);
            }
        }

        function dashboardReviewOrder(){
            
            var v_order_by = $("#v_order_by_review").val();
            
            if(v_order_by=="ASC"){
                var $divs = $("div.review-first");
                var numericallyOrderedDivs = $divs.sort(function (a, b) {
                    var date1 = $(a).find("h2").text();
                    var date2 = $(b).find("h2").text();
                    return (date1 < date2) ? -1 : (date1 > date2) ? 1 : 0;
                    //return $(a).find("h2").text() > $(b).find("h2").text();
                });
                $("#mainorderreview").html(numericallyOrderedDivs);
            }else{
                var $divs = $("div.review-first");
                var numericallyOrderedDivs = $divs.sort(function (a, b) {
                    var date1 = $(a).find("h2").text();
                    var date2 = $(b).find("h2").text();
                    return (date1 > date2) ? -1 : (date1 < date2) ? 1 : 0;
                    //return $(a).find("h2").text() < $(b).find("h2").text();
                });
                $("#mainorderreview").html(numericallyOrderedDivs);
            }
        }

    </script>


<script type="text/javascript">

    function upgradeplanmodal(){
        $("#upgradePlanModal").modal("show");
    }
    function plandurationchange(data=""){
        
        if(data=="monthly"){
            $(".monthyprice").css("display","block");
            $(".yealryprice").css("display","none");

        }else if(data=="yearly"){
            $(".yealryprice").css("display","block");
            $(".monthyprice").css("display","none");
        }

    }

    function upgradePlanModalClose(){
        $("#upgradePlanModal").modal("hide");
    }

    function upgradePlanModalactiveClose(){
        $("#upgradePlanModalActive").modal("hide");
    }

    function activateProfile(){
           
            var actionurl = "{{url('seller/activate-profile')}}";
            var formdata = "";
            
            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#commonmsg").html(obj['msg']);
                        if(obj['redirectprofile']==1){
                            window.location = "{{url('seller/edit-my-profile')}}";
                        }else{
                            window.location = "{{url('seller/dashboard')}}";    
                        }
                    }else if(obj['status']==2){
                        $("#upgradePlanModalActive").modal("show");
                    }else{
                        $('#commonmsg').html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    $('#commonerr').css("display","block");
                }
            });

    }

    function changeProfileData(data) {
        
        var actionurl = "{{url('seller/update-profile-status')}}";
        var formdata = "e_profile_status="+data;

        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                var obj = jQuery.parseJSON(res);
                if(obj['status']==1){
                    $("#commonmsg").html(obj['msg']);
                    if(obj['active']==1){
                        $("#accntpa").html("");
                    }else{
                        $("#accntpa").html("Account Paused");
                    }
                }else{
                    $('#commonmsg').html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                $('#commonerr').css("display","block");
            }
        });

    }    

    function changeService(data) {
        
        var actionurl = "{{url('seller/update-service')}}";
        var formdata = "v_service="+data;

        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                var obj = jQuery.parseJSON(res);
                if(obj['status']==1){
                    //$("#commonmsg").html(obj['msg']);
                    
                    if(data=="online"){
                        $("#servieprofile").html(" Online");
                        //$("#servieprofilespon").html(" Online");
                        $("#menudata").css("display","block");
                    }else{
                        $("#servieprofile").html(" In Person");
                        //$("#servieprofilespon").html(" In Person");
                        $("#menudata").css("display","none");
                    }
                    if(obj['active']=="inactive"){
                        $("#accountstatusbutton").css("display","block");
                        $("#accountstatus").css("display","none");
                        $(".profile-data").addClass("menuvisibility");
                        $(".profile-data").find("a").addClass("isDisabled");
                    }else{
                        $("#accountstatusbutton").css("display","none");
                        $("#accountstatus").css("display","block");
                        $(".profile-data").removeClass("menuvisibility");
                        $(".profile-data").find("a").removeClass("isDisabled");
                    }
                }else{
                    $('#commonmsg').html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                $('#commonerr').css("display","block");
            }
        });

    }

    @if(isset($userdata->seller['e_status']) && $userdata->seller['e_status']!="active")
        $(".profile-data").find("a").addClass("isDisabled");
    @endif

    function dashboardreview(){

          var page = $("#reviewpage").val();
          var actionurl = "{{url('seller/dashboard-review')}}";
          var formdata = "&page="+page; 
          page=parseInt(page)+1;
          $("#reviewpage").val(page)
          document.getElementById('load').style.visibility="visible"; 
          
          $.ajax({
              type    : "GET",
              url     : actionurl,
              data    : formdata,
              success : function( res ){
                var obj = jQuery.parseJSON(res);
                document.getElementById('load').style.visibility='hidden';
                if(obj['status']==1){
                    
                    $("#dashboardreviewdata").append(obj['responsestr']);
                    if(obj['btnstatus']==0){
                        $("#showmorebtn").html("");
                    }
                    $("#reviewcntdata").html(obj['reviewcntdata']);
               
                }    
                else{
                    $("#commonmsg").html(obj['msg']);
                }
              },
              error: function ( jqXHR, exception ) {
              }
          });

    }
    dashboardreview();

    function readURL(input) {

        if (input.files && input.files[0]) {

            if(input.files[0].size > 2097152) {
                $('#fileupload-example-4').parent("div").find(".videoerrsize").css("display","block");
                return false;
            }
            $('#fileupload-example-4').parent("div").find(".videoerrsize").css("display","none");

            document.getElementById('load').style.visibility="visible"; 
            var formData = new FormData($('#updatejobpost2')[0]);
            var actionurl = '{{url("account/dashbaord-image")}}';
              $.ajax({
                  processData: false,
                  contentType: false,
                  type    : "POST",
                  url     : actionurl,
                  data    : formData,
                  success : function( res ){
                      document.getElementById('load').style.visibility='hidden';
                      var obj = jQuery.parseJSON(res);   
                      if(obj['status']==1){
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#edit-images')
                                .attr('src', e.target.result)
                                .width(230)
                                .height(230);
                        };
                        reader.readAsDataURL(input.files[0]);
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      
                  }

              });
        }
    }

    function dashboardMessage(){

          var page = $("#messagepage").val();
          var actionurl = "{{url('message/dashboard/seller')}}";
          var formdata = "&page="+page; 
          page=parseInt(page)+1;
          $("#messagepage").val(page)
          document.getElementById('load').style.visibility="visible"; 
          
          $.ajax({
              type    : "GET",
              url     : actionurl,
              data    : formdata,
              success : function( res ){

                var obj = jQuery.parseJSON(res);
                document.getElementById('load').style.visibility='hidden';
                if(obj['status']==1){
                    $("#dashboardmessagedata").append(obj['responsestr']);
                    if(obj['btnstatus']==0){
                        $("#showmorebtnmessage").html("");
                    }
                    $("#msgcntdata").html(obj['msgcntdata']);
                }    
                else{
                    $("#commonmsg").html(obj['msg']);
                }
              },
              error: function ( jqXHR, exception ) {
              }
          });
    }

    dashboardMessage();


</script>
     
@stop

