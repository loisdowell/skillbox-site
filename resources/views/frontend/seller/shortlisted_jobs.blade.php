@extends('layouts.frontend')

<?php
// echo "<pre>";
// print_r($jobs_data);
// exit;
 
?>

@section('content')

<style type="text/css">
    .highimgshortlist{
        max-width: 26px;
        margin-top: -4px;
    }
</style>
    

    <!-- Modal -->
    <div class="modal  fade" id="leaveReview" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content modal-content-1">
                <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> Leave Your Review </h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="text-box-containt box-containt-que">
                                    <label> Review rating </label>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="1" required> <span class="star-no"> Person You Work For?</span>
                                    </div>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="1" required> <span class="star-no"> Support You Get </span>
                                    </div>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="1" required> <span class="star-no"> Growth opportunities </span>
                                    </div>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="1" required> <span class="star-no"> Would You Work Again </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="text-box-containt">
                                    <label> Enter Your Review </label>
                                    <textarea rows="5" class="form-control"></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button class="btn btn-Submit-pop"> Submit Review </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

   
   <!-- 26B-control-panel-buyer-account-shortlisted-skills-1 -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('seller/dashboard')}}"><button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button></a>
                </div>
                <div class="title-support">
                    <h1>My Shortlisted Jobs</h1>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="main-shortlisted">
            <div id="commonmsg" style="margin-top: 25px"></div>
            

            @if(isset($jobs_data) && count($jobs_data))
                
                <div class="edit-btn" id="deletebutton">
                    <button type="button" class="btn edit-short" onclick="fn_delete()"> Delete Selected </button>
                    <button type="button" class="btn btn-selectall" onclick="fn_select_all()"> Select All </button>
                </div>
                @foreach($jobs_data as $k=>$v)
                    <?php 
                    $v_image="";
                    if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
                        $v_image=$v->hasUser()->v_image;
                    }
                    ?>
                    <div class="editshort-list" id="editshort-list_{{$v->_id}}">
                        <div class="border-space">
                            <div class="row">
                                <div class="col-sm-3 col-xs-12">
                                    <div class="header-dirservice-find">
                                        <div class="Shortlisted-img-person">
                                            @php
                                            $imgdata = '';
                                            if(Storage::disk('s3')->exists($v->v_photos[0])){
                                                $imgdata = \Storage::cloud()->url($v->v_photos[0]);
                                            }else{
                                                $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                            }
                                            @endphp
                                            <img src="{{$imgdata}}" class="img-responsive img-category" alt="" />
                                        </div>
                                        <div class="squaredChk">
                                            <input type="checkbox" id="squaredChk_{{$v->_id}}" name="shortlisted_ids[]" value="{{$v->_id}}" />
                                            <label for="squaredChk_{{$v->_id}}"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9 col-xs-12">
                                    <div class="category-heading ">
                                        <div class="row">
                                            <div class="col-sm-8 col-xs-12 ">
                                                <?php  ?>
                                                <h4>{{isset($v->v_job_title) ? $v->v_job_title:''}}</h4>
                                                <img src="{{url('public/Assets/frontend/images/lira.png')}}" class="img-category-list highimgshortlist" alt="" />
                                                
                                                @if(isset($v->v_budget_type) && $v->v_budget_type=='open_to_offers')
                                                    <span class="info">Open to Offers</span>
                                                @endif
                                                @if(isset($v->v_budget_type) && $v->v_budget_type=='total_budget')
                                                    <span class="info">£{{isset($v->v_budget_amt) ? $v->v_budget_amt:'0'}} Total Budget</span>
                                                @endif
                                                @if(isset($v->v_budget_type) && $v->v_budget_type=='hourly_rate')
                                                    <span class="info">£{{isset($v->v_budget_amt) ? $v->v_budget_amt:'0'}} Hourly Rate</span>
                                                @endif

                                                <img src="{{url('public/Assets/frontend/images/strength.png')}}" class="img-category-list highimgshortlist" alt="" />
                                                <span class="info"> Skill Level:<strong style="text-transform: uppercase;"> {{isset($v->v_skill_level_looking) ? $v->v_skill_level_looking:''}}
                                            </strong> </span>
                                            </div>
                                            <div class="col-sm-4 col-xs-12 ">
                                                <div class="heading-right">
                                                    <span class="sponsored"> {{( isset($v->e_sponsor) && $v->e_sponsor == 'yes') ? 'Sponsored Post' : ''}} </span>
                                                    @if(count($a_jobs) && in_array($v->id, $a_jobs))
                                                          <button type="button" class="btn btn-final-job" id="apply_btn_{{$v->_id}}" value="" disabled="disabled"> Applied </button>
                                                    @else
                                                          <button type="button" class="btn btn-final-job" onclick="jobApply('{{$v->_id}}')" id="apply_btn_{{$v->_id}}" value=""> APPLY NOW </button>
                                                    @endif
                                                    {{-- <button type="button" class="btn btn-sponsored">
                                                        APPLY NOW</button> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Shortlisted-text">
                                        <p>
                                        <?php            
                                        $l_job_description = substr(isset($v['l_job_description']) ? $v['l_job_description'] : $v['l_job_description'] , 0, 680);
                                        
                                            echo $l_job_description;

                                        ?>
                                        </p>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        {{--
                        <button type="button" class="btn btn-leave" data-toggle="modal" data-target="#leaveReview"> Leave Review </button>
                        --}}

                        @php
                            $title =App\Helpers\General::TitleSlug($v->v_job_title);
                        @endphp
                        <a href="{{url('online-job')}}/{{$v->_id}}/{{$title}}?id={{$v->_id}}"><button type="button" class="btn btn-detail"> Details </button></a>
                        <div class="clearfix">
                        </div>
                    </div>
                @endforeach
            
            @else

                <div class="editshort-list">
                    <div class="border-space">
                        <div class="row" style="text-align: center;">
                            No shortlisted jobs found.
                        </div>
                    </div>    
                </div>
            
            @endif

        </div>
    </div>
    <!-- End 26B-control-panel-buyer-account-shortlisted-skills-1 -->



<!--- start delete model -->
<div class="modal fade" id="commentdeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title custom-font">Confirm Delete Record ?</h3>
      </div>
      <div class="modal-body" style="text-align:center">
          <i class="margin-top-10 fa fa-question fa-5x"></i>
          <h4>Delete !</h4>
          <p>Are you sure you want to delete this record?</p>
      </div>
      <input type="hidden" name="commentdeleteId[]" id='commentdeleteId'>
      <div class="modal-footer" style="padding-top: 20px;">
        <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" onclick="commentdeleteAction()"><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
        <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
      </div>
    </div>
  </div>
</div>
 
 <!--- end delete model -->   
@stop


@section('js')

<script type="text/javascript">
  
function fn_delete(){
      
    var id = [];
       
    $("input[name='shortlisted_ids[]']:checked").each(function(i){
        id.push($(this).val());
    });
      
    $("#commentdeleteId").val(id);
    $("#commentdeleteModal").modal("show");
}

function commentdeleteAction(){
   var cid = $("#commentdeleteId").val();
   var actionurl = "{{url('seller/delete-shortlisted-jobs')}}";
   var formdata = "shortlistIds="+cid; 
   var array_cid = cid.split(",");
   document.getElementById('load').style.visibility="visible"; 
    $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                document.getElementById('load').style.visibility='hidden'; 
                var obj = jQuery.parseJSON(res);
                  
                if(obj['status']==1){
                    $("#commentdeleteModal").modal("hide");
                    for (var i = 0; i < array_cid.length; i++) {
                       $("#editshort-list_"+array_cid[i]).hide();
                    }  
                    
                    $('#commonmsg').html(obj['msg']);     
                    window.location = "{{url('seller/shortlisted-jobs')}}";

                }else{
                    $("#commentdeleteModal").modal("hide");
                    $('#commonmsg').html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                // $("#errormsg").show();
             }
        });

}

function fn_select_all(){

    var is_checked = $("input[name='shortlisted_ids[]']:checked").length;
   
    if (!is_checked) {
        $("input[name='shortlisted_ids[]").prop('checked', true); 
    }else {
        $("input[name='shortlisted_ids[]").prop('checked', false);
    } 
   
}

function jobApply(id=""){
        
        var actionurl = "{{url('apply-job')}}";
        var formdata = "_id="+id; 
        document.getElementById('load').style.visibility="visible"; 
       
        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                
                document.getElementById('load').style.visibility='hidden'; 
                var obj = jQuery.parseJSON(res);
                
                if(obj['status']==1){
                    $('#apply_btn_'+id).text('Applied');
                    $('#apply_btn_'+id).prop('disabled', true);
                    $('#commonmsg').html(obj['msg']);     
                }else if(obj['status']==3){
                   window.location = "{{url('login')}}";
                }else{
                    $("#commonmsg").html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                // $("#errormsg").show();
             }
        });
}

</script>

@stop

