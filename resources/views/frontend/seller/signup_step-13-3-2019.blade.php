@extends('layouts.frontend')

<style type="text/css">
    .imgrem {position: relative;}
    .close-img{height: 15px; position: absolute;right: 15px;top: -30px;}
</style>
<style type="text/css">
  #introducyvideoid {
      height: 0;
      width: 0;
  }
.addimg_brdr .dropzone .dz-preview {
    margin: 0px 0px !important;
}
.addimg_brdr .dropzone .dz-preview, .dropzone-previews .dz-preview, .img-biography .dropzone-previews .dz-preview {
    border: 1px solid #8470ff !important;
}
.dropzone{ min-height: 255px; }
.addimg_brdr .dropzone.dz-started .dz-message, .addimg_brdr .dropzone.dz-clickable .dz-message {
    position: absolute;
    z-index: -11;
    left: 0;
    right: 0;
    text-align: center;
    top: 50%;
    transform: translate(10%, -50%);
    opacity: 1;
}
</style>

<style type="text/css">
    .input_close{position: relative;}
    .input_close a{position: absolute;top: 6px;right: 10px;}
    .input_close .input-field{padding-right: 36px;}
</style>
<link rel="stylesheet" href="{{url('public/Assets/plugins')}}/dropzone/css/dropzone.css">

<div class="modal fade" id="deleteModalimg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font">Confirm Remove Image ?</h3>
            </div>
            <div class="modal-body" style="text-align:center">
            <i class="margin-top-10 fa fa-question fa-5x"></i>
            <h4>Delete !</h4>
            <p>Are you sure you want to delete this image?</p>
            </div>
           <form name="deleteimg" id="deleteimgjobpost" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="i_seller_id" id='i_seller_id_delete'>
            <input type="hidden" name="v_photo_name" id='v_photo_name_delete'>
            <input type="hidden" name="v_span" id='v_span_delete'>
           </form>      
            <div class="modal-footer" style="padding-top: 20px;">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" type="button" onclick="jobpostimagedelete();" ><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
                <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" type="button" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModalvidso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font">Confirm Remove Video ?</h3>
            </div>
            <div class="modal-body" style="text-align:center">
            <i class="margin-top-10 fa fa-question fa-5x"></i>
            <h4>Delete !</h4>
            <p>Are you sure you want to delete this video?</p>
            </div>
           <form name="deletevideo" id="deletevideojobpost" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="i_seller_id" id='i_seller_id_video'>
            <input type="hidden" name="v_video_name" id='v_video_name_video'>
            <input type="hidden" name="v_span" id='v_span_video'>
           </form>      
            <div class="modal-footer" style="padding-top: 20px;">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" type="button" onclick="jobpostvideodelete();" ><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
                <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" type="button" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModalworkvidso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font">Confirm Remove Video ?</h3>
            </div>
            <div class="modal-body" style="text-align:center">
            <i class="margin-top-10 fa fa-question fa-5x"></i>
            <h4>Delete !</h4>
            <p>Are you sure you want to delete this video?</p>
            </div>
           <form name="deletevideo" id="deletevideojobpost" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="i_seller_id" id='i_seller_id_video'>
            <input type="hidden" name="v_video_name" id='v_video_name_video'>
            <input type="hidden" name="v_span" id='v_span_video'>
           </form>      
            <div class="modal-footer" style="padding-top: 20px;">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" type="button" onclick="workvideodelete();" ><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
                <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" type="button" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
</div>


@section('content')

  <!-- Navigation Bar -->
    <div class="navigation-bar">
        <div class="container">
            <div id="content">
                <ul id="tabs" class="nav nav-tabs nav-dirservice" data-tabs="tabs">
                    
                    {{-- <li class="active"><a href="#step1" data-toggle="tab" class="tab-name"> 1. Step </a></li>
                    <li><a href="#step2" data-toggle="tab" class="tab-name"> 2. Step </a></li>
                    
                    @if($planstep==1)
                    <li><a href="#step3" data-toggle="tab" class="tab-name"> 3. Step </a></li>
                    @endif --}}
                   
                    @if(isset($sellerProfile) && count($sellerProfile))
                    
                    <li class="active"><a href="#step1" data-toggle="tab" class="tab-name"> 1. Step </a></li>
                    <li><a href="#step2" data-toggle="tab" class="tab-name"> 2. Step </a></li>
                    @if($planstep==1)
                    <li><a href="#step3" data-toggle="tab" class="tab-name"> 3. Step </a></li>
                    @endif
                   
                    @else
                    
                    <li class="active"><a href="#step1" data-toggle="" id="tstep1" class="tab-name"> 1. Step </a></li>
                    <li><a href="#step2" data-toggle="" id="tstep2" class="tab-name"> 2. Step </a></li>
                    @if($planstep==1)
                    <li><a href="#step3" data-toggle="" id="tstep3" class="tab-name"> 3. Step </a></li>
                    @endif
                   
                    @endif

                    
                    {{-- <li class="active"><a href="#step1" data-toggle="" class="tab-name"> 1. Step </a></li>
                    <li><a href="#step2" data-toggle="" class="tab-name"> 2. Step </a></li>
                    @if($planstep==1)
                    <li><a href="#step3" data-toggle="" class="tab-name"> 3. Step </a></li>
                    @endif --}}

                </ul>
            </div>
        </div>
    </div>
    <!-- End Navigation Bar -->

    <!-- 02B-sign-up-i-want-to-sell-online-SELLER-W1 -->
    <div class="container">
        <div id="my-tab-content" class="tab-content ">
            
            <!-- Start 02B-sign-up-i-want-to-sell-online-SELLER-W1  -->
            <div class="tab-pane active" id="step1">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-dirservice">
                            <div class="step1-dirservices">
                                

                                <div class="online-service">
                                    <h1> @if($v_service=="online")
                                            Online
                                        @else
                                            In Person
                                        @endif    
                                        Services</h1>
                                </div>
                                
                                <form class="horizontal-form" role="form" method="POST" name="sellersignupste1pform" id="sellersignupste1pform" data-parsley-validate enctype="multipart/form-data" >
                                        
                                    {{ csrf_field() }}
                                    <div class="option-category">
                                            
                                        <label class="sel">Select desired category</label>
                                        <select class="form-control form-sel" name="i_category_id" onchange="getSkill(this.value)" required>
                                            <option value="">-Select-</option>
                                            @if(isset($category) && count($category))
                                                @foreach($category as $k=>$v)
                                                    @if(isset($v->v_name) && isset($v->id))
                                                    <option value = '{{$v->id}}' @if(isset($sellerProfile->i_category_id) && $sellerProfile->i_category_id==$v->id) selected @endif>{{$v->v_name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif                                                  
                                        </select>
                                    
                                    </div>

                                    <div class="option-category">
                                        <label class="sel"> What is the main skill you can offer? </label>
                                        <select class="form-control form-sel" name="i_mainskill_id" id="mainskill" required>
                                            <option value="">-Select-</option>
                                            @if(isset($skilldata) && count($skilldata))
                                                @foreach($skilldata as $k=>$v)
                                                    
                                                    @if(isset($sellerProfile->i_mainskill_id) && $sellerProfile->i_mainskill_id==$v->id)
                                                    <option value = '{{$v->id}}' selected >{{$v->v_name}}</option>
                                                    @endif

                                                @endforeach
                                            @endif
                                        </select>
                                    
                                    </div>

                                    <div class="multiple-category">
                                        {{-- <label class="sel"> What other desirable skills you're looking into the Freelancer? </label> --}}
                                        {{-- <label class="sel">What other desirable skills do you have as a freelancer? </label> --}}
                                        <label class="sel">What other desirable skills can you offer?</label>
                                        <select class="form-control form-sel" id="otherskill" onchange="otherskilldata(this.value)" required>
                                            <option value="">-Select-</option>
                                        </select>
                                        
                                        <div class="new-button-f butt-form" id="sel_other_skill">
                                            @if(isset($other_skill_data) && count($other_skill_data))
                                                @foreach($other_skill_data as $k=>$v)
                                                    <button type="button" class="btn directservice-btn" id="btn_{{$k}}"> <img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" onclick="removeotherskill('{{$k}}')"> {{$v}} </button>
                                                    <input type="hidden" name="i_otherskill_id[]" id="hid_{{$k}}" class="item" value="{{$k}}">
                                                @endforeach
                                            @endif
                                        </div>
                                        <div id="hiddendiv">
                                        </div> 
                                    </div>

                                    <div class="radio-choise">
                                        <label class="sel"> What is your experience level </label>
                                        <div class="reg">
                                        <label style="font-weight: normal;">
                                            <bdo dir="ltr">
                                               <input type="radio" checked class="one" value="entry" name="v_experience_level" @if(isset($sellerProfile->v_experience_level) && $sellerProfile->v_experience_level=='entry') checked @endif>
                                               <span></span>
                                               <abbr> Entry </abbr>
                                            </bdo>
                                         </label>   
                                         <label style="font-weight: normal;">   
                                            <bdo dir="ltr">
                                               <input type="radio" class="one" value="intermediate" name="v_experience_level" @if(isset($sellerProfile->v_experience_level) && $sellerProfile->v_experience_level=='intermediate') checked @endif>
                                               <span></span>
                                               <abbr> Intermediate </abbr>
                                            </bdo>
                                        </label>
                                        <label style="font-weight: normal;">    
                                            <bdo dir="ltr">
                                               <input type="radio" class="one" value="expert" name="v_experience_level" @if(isset($sellerProfile->v_experience_level) && $sellerProfile->v_experience_level=='expert') checked @endif>
                                               <span></span>
                                               <abbr> Expert </abbr>
                                            </bdo>
                                        </label>    
                                        </div>
                                    </div>

                                    <label class="checkbox wc-checkbox-space">
                                        <input type="checkbox" class="singup-checkup" name="e_notification_subscribe"  @if(isset($sellerProfile->e_notification_subscribe) && $sellerProfile->e_notification_subscribe=='on') checked @endif >
                                        <span class="notice-jobpostion"> 
                                          I want to receive notifications about new job postings in my category.
                                        </span>
                                    </label>
                                    <hr class="hr-line">

                                    <div class="bottom-btn">
                                        <button type="button" class="btn form-save-exit ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="saveandexit" onclick="saveStep1('saveandexit')">Save &amp; Exit</button>
                                        
                                        <button type="button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savenext" class="btn form-next ladda-button" onclick="saveStep1('savenext')">Next</button>
                                    </div> 
                                </form>
                            </div>

                        </div>
                        
                        


                        <div class="alert alert-info alert-danger" id="errormsg" style="display: none;">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;<span id="errortxt">Something went wrong.</span>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End 02B-sign-up-i-want-to-sell-online-SELLER-W1  -->

            <!-- 02B-sign-up-i-want-to-sell-online-SELLER-W2 -->
            <div class="tab-pane" id="step2">
                
                <div class=" row">
                    <div class="col-sm-12">
                       
                        <div class="main-dirservice2">
                            <div class="step2-dirservice">
                                <div class="step2-containt">

                                    <div class="title-dirservice">
                                        <h2>@if($v_service=="online")
                                            Online
                                        @else
                                            In Person
                                        @endif    
                                        Services</h2>
                                    </div>

                                    <form class="horizontal-form" role="form" method="POST" name="sellersignupste2pform" id="sellersignupste2pform" data-parsley-validate enctype="multipart/form-data" >
                                    
                                    {{ csrf_field() }}

                                    <div class="dirservice-form">
                                        <div class="dirservice-field">
                                            <div class="row">
                                                
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Add your profile title 
                                                        </label>

                                                        <input type="text" data-parsley-minlength="10" data-parsley-minlength-message="Title must be minimum 10 characters" data-parsley-maxlength="50" data-parsley-maxlength-message="Title must be no longer than 50 characters" class="form-control input-field" data-parsley-trigger="keyup focusin focusout" name="v_profile_title" required value="@if(isset($sellerProfile->v_profile_title)){{$sellerProfile->v_profile_title}}@endif">
                                                        @if($v_service=="online") i.e. Logo Designer @else i.e. Landscape Designer @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Hourly Rate (£)
                                                        </label>
                                                        <input type="number" step="any" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" name="v_hourly_rate"  value="@if(isset($sellerProfile->v_hourly_rate)){{$sellerProfile->v_hourly_rate}}@endif">
                                                            Hourly Rate is separate from your packages
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Website
                                                        </label>
                                                        <input type="url" class="form-control input-field" name="v_website" value="@if(isset($sellerProfile->v_website)){{$sellerProfile->v_website}}@endif">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Phone No.
                                                        </label>
                                                        <input type="text" class="form-control input-field"  name="v_contact_phone" data-parsley-trigger="keyup" value="@if(isset($sellerProfile->v_contact_phone)){{$sellerProfile->v_contact_phone}}@endif" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        @if($v_service=="inperson")
                                        @php
                                        $daydata=array(
                                            'monday'=>'Monday',
                                            'tuesday'=>'Tuesday',
                                            'wednesday'=>'Wednesday',
                                            'thursday'=>'Thursday',
                                            'friday'=>'Friday',
                                            'saturday'=>'Saturday',
                                            'sunday'=>'Sunday',
                                        );
                                        @endphp

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <label class="field-name">
                                                        Opening hours
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-sm-5 col-xs-12">
                                                            <div class="timepicker-responsive">
                                                                <div class="dirservice-deshline">
                                                                    <div class="dirservice-desh">
                                                                        
                                                                        <select class="form-control date-concept" name="v_avalibility_from"    required>
                                                                        @foreach($daydata as $k=>$v)
                                                                            <option value="{{$k}}" @if(isset($sellerProfile->v_avalibility_from) && $sellerProfile->v_avalibility_from==$k) selected @endif>{{$v}}</option>
                                                                        @endforeach
                                                                        </select>

                                                                    </div>
                                                                    <select class="form-control date-concept" name="v_avalibility_to" required>
                                                                        @foreach($daydata as $k=>$v)
                                                                        <option value="{{$k}}" @if(isset($sellerProfile->v_avalibility_to) && $sellerProfile->v_avalibility_to==$k) selected @endif>{{$v}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-5 col-xs-12">
                                                            <div class="timepicker-responsive">
                                                                <div class="time">
                                                                    <div class="control-group">
                                                                        
                                                                        <select class="form-control date-concept" name="v_avalibilitytime_from"  style="padding-left: 50px">
                                                                         <?php
                                                                            for($i=1;$i<=24;$i++){ ?>
                                                                                    <option value="{{$i}}" @if(isset($sellerProfile->v_avalibilitytime_from) && $sellerProfile->v_avalibilitytime_from==$i) selected @endif>{{$i}}</option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <select class="form-control date-concept" name="v_avalibilitytime_to" required style="padding-left: 50px">
                                                                            <?php
                                                                                for($i=1;$i<=24;$i++){ ?>
                                                                                    <option value="{{$i}}" @if(isset($sellerProfile->v_avalibilitytime_to) && $sellerProfile->v_avalibilitytime_to==$i) selected @endif>{{$i}}</option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dirservice-field">
                                            <div class="row">
                                               
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name" >
                                                            City
                                                        </label>
                                                        <input type="text" id="autocomplete" class="form-control input-field" name="v_city" required value="@if(isset($sellerProfile->v_city)){{$sellerProfile->v_city}}@endif" autocomplete="off" >
                                                    </div>
                                                </div>

                                                <input type="hidden" name="v_latitude" id="v_latitude" value="@if(isset($sellerProfile->v_latitude)){{$sellerProfile->v_latitude}}@endif">
                                                <input type="hidden" name="v_longitude" id="v_longitude" value="@if(isset($sellerProfile->v_longitude)){{$sellerProfile->v_longitude}}@endif">


                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Postcode
                                                        </label>
                                                        <input type="text" class="form-control input-field" name="v_pincode" id="v_pincode" required value="@if(isset($sellerProfile->v_pincode)){{$sellerProfile->v_pincode}}@endif" >
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        
                                        @endif
                                        
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="textbox-description">
                                                        <label class="field-name">
                                                            Short description (this will show at the search listing)
                                                        </label>
                                                        <textarea rows="2" placeholder="I will..." class="form-control cover-letter" data-parsley-maxlength="100" data-parsley-maxlength-message="Short Description must be no longer than 100 characters" name="l_short_description" required>@if(isset($sellerProfile->l_short_description)){{$sellerProfile->l_short_description}}@endif</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="textbox-description">
                                                        <label class="field-name" id="validatemessagedesclbl">
                                                            Brief description of the work you do
                                                        </label>
                                                        <textarea class="information1" name="l_brief_description" id="l_brief_description"  required >@if(isset($sellerProfile->l_brief_description)){{$sellerProfile->l_brief_description}}@endif</textarea>
                                                        <div id="validatemessagedesc" style="display: none"><ul class="parsley-errors-list filled"><li class="parsley-required">Please enter description the work.</li></ul></div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-xs-12">
                                                <div class="open-browser ">
                                                    <label class="field-name" id="validatemessagephotolbldoc">
                                                        Add documents (optional)<br>
                                                        <span style="margin-top:0px;display: inline-block;font-size: 14px;">Supported formats: doc, docx, pdf, csv, xlsx</span><br>
                                                    </label>
                                                    @php $totaldoc = 10; @endphp

                                                    @if(isset($sellerProfile->id) && $sellerProfile->id != '')
                                                        <div class="addimg_brdr">
                                                           <div class="form-group">
                                                              <div class="cl-mcont" style="">
                                                        <div action="{{url('seller/ajax/upload/doc')}}/{{$sellerProfile->id}}" class="" id="workdata_doc" multiple="true" >
                                                            @if(isset($sellerProfile->v_doc) && count($sellerProfile->v_doc))
                                                            @php $totalphotos = 10-count($sellerProfile->v_doc); @endphp
                                                            @foreach($sellerProfile->v_doc as $k=>$v)
                                                            @php
                                                                $imgdaata = \Storage::cloud()->url($v);     
                                                            @endphp
                                                            @php 
                                                                $ext = pathinfo($v, PATHINFO_EXTENSION);
                                                                if($ext=="pdf"){
                                                                    $imgdaata = url('public/Assets/frontend/images').'/pdf.png';         
                                                                }else if($ext=="doc"){
                                                                    $imgdaata = url('public/Assets/frontend/images').'/doc.png';         
                                                                }else if($ext=="docx"){
                                                                    $imgdaata = url('public/Assets/frontend/images').'/docx.jpeg';         
                                                                }else if($ext=="csv"){
                                                                    $imgdaata = url('public/Assets/frontend/images').'/csv.png';         
                                                                }else if($ext=="xls"){
                                                                    $imgdaata = url('public/Assets/frontend/images').'/xls.jpeg';         
                                                                }else{
                                                                    $imgdaata = url('public/Assets/frontend/images').'/doc.png';
                                                                }

                                                            @endphp

                                                            <div class="dropzone-previews" style="width: auto;display: inline-block;" id="remd{{$k}}">
                                                                <div class="dz-preview dz-processing dz-success dz-image-preview"  id="removed-row-{{$sellerProfile->id}}">
                                                                <div class="dz-details"> 
                                                                     <img src="{{$imgdaata}}" style="max-width:100px;">
                                                                </div>
                                                                <a href="javascript:" onclick="removeWork_doc('{{$v}}','{{$k}}')" class="dz-remove">Remove image</a></div>
                                                            </div>
                                                            @endforeach
                                                            @endif
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                    @else
                                                        <div class="addimg_brdr">
                                                           <div class="form-group">
                                                              <div class="cl-mcont" style="">
                                                                <div action="{{url('seller/ajax/upload/doc/1')}}" class="" id="workdata_doc" multiple="true" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    @endif

                                                    <div id="validatemessagephotod" style="display: none"><ul class="parsley-errors-list filled"><li class="parsley-required">Please upload your work photos.</li></ul></div>
                                                    
                                                    <script type="text/javascript">
                                                        var totaldoc = '{{$totaldoc}}';    
                                                        var mydropzonephoto;
                                                        jQuery(document).ready(function(){
                                                            jQuery("#workdata_doc").addClass("dropzone");
                                                            mydropzonephoto = new Dropzone("#workdata_doc", {
                                                              multiple : true,
                                                              maxFiles:parseInt(totaldoc),
                                                              maxFilesize:2,
                                                              parallelUploads: 1,
                                                              acceptedFiles: ".doc,.docx,.pdf,.csv,.xlsx",
                                                              }).on("complete", function(file) {
                                                                  $("#workdata_doc").html(file.xhr.response);    
                                                              }).on("removedfile", function(file) {
                                                                   //removeWork_video();
                                                              });
                                                           });

                                                           function removeWork_doc(iname,indexdata){
                                                                document.getElementById('load').style.visibility="visible"; 
                                                                var formData = "name="+iname;
                                                                var actionurl="{{url('seller/ajax/remove/doc')}}";
                                                                $.ajax({
                                                                    // processData: false,
                                                                    // contentType: false,
                                                                    type    : "GET",
                                                                      url     : actionurl,
                                                                      data    : formData,
                                                                      success : function( res ){
                                                                        document.getElementById('load').style.visibility='hidden';
                                                                        $("#remd"+indexdata).remove();
                                                                        mydropzonephoto.options.maxFiles = mydropzonephoto.options.maxFiles + 1;

                                                                      },
                                                                      error: function ( jqXHR, exception ) {
                                                                      }
                                                                });
                                                            }
                                                     </script>
                                                </div>
                                            </div>
                                    
                                    <!-- <div class="dirservice-form"> -->
                                    <div class="dirservice-field">
                                        <div class="radio-choise">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <label class="field-name">
                                                        English Proficiency
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    
                                                        <div class="reg">
                                                        <label style="font-weight: normal;">
                                                            <bdo dir="ltr">
                                                              <input type="radio" value="limited" name="v_english_proficiency" @if(isset($sellerProfile->v_english_proficiency) && $sellerProfile->v_english_proficiency=='limited') checked @endif ><span></span>
                                                              <abbr>Limited</abbr>
                                                            </bdo>
                                                        </label>    
                                                        </div>

                                                        <div class="reg">
                                                        <label style="font-weight: normal;">
                                                            <bdo dir="ltr">
                                                              <input type="radio" value="professional" name="v_english_proficiency" @if(isset($sellerProfile->v_english_proficiency) && $sellerProfile->v_english_proficiency=='professional') checked @endif><span></span>
                                                              <abbr>Professional</abbr>
                                                            </bdo>
                                                        </label>    
                                                        </div>

                                                        <div class="reg">
                                                        <label style="font-weight: normal;">
                                                            <bdo dir="ltr">
                                                              <input type="radio" value="native" name="v_english_proficiency" @if(isset($sellerProfile->v_english_proficiency) && $sellerProfile->v_english_proficiency=='native') checked @endif><span></span>
                                                              <abbr>Native</abbr>
                                                            </bdo>
                                                        </label>    
                                                        </div>

                                                        <div class="reg">
                                                        <label style="font-weight: normal;">
                                                            <bdo dir="ltr">
                                                              <input type="radio" value="billingual"  @if(isset($sellerProfile->v_english_proficiency) && $sellerProfile->v_english_proficiency=='billingual') checked @endif name="v_english_proficiency"><span></span>
                                                              <abbr>Billingual</abbr>
                                                            </bdo>
                                                        </label>    
                                                        </div>


                                                  </div>
                                              </div>
                                        </div>
                                    </div>

                                    

                                    <div class="dirservice-field">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="open-browser ">
                                                    <label class="field-name" id="validatemessagevideolbl">
                                                        Add your introductory video (up to 150mb)<br>
                                                        <span style="font-size: 14px;">Supported formats: mp4, mkv, webm, mov, m4v <br></span>
                                                        <span style="font-size: 14px;">(Video is optional)</span>
                                                    </label>

                                                    @if(isset($sellerProfile->id) && $sellerProfile->id != '')
                                                        <div class="addimg_brdr">
                                                           <div class="form-group">
                                                              <div class="cl-mcont" style="">
                                                        <div action="{{url('seller/ajax/upload/introducy_video')}}/{{$sellerProfile->id}}" class="" id="introducy_video" multiple="false" >
                                                            
                                                            @if(isset($sellerProfile->v_introducy_video) && $sellerProfile->v_introducy_video != '')
                                                            <div class="dropzone-previews" style="width: auto;display: inline-block;" >
                                                                @php
                                                                    $videodata = \Storage::cloud()->url($sellerProfile->v_introducy_video);     
                                                                @endphp
                                                                <div class="dz-preview dz-processing dz-success dz-image-preview edit-img"  id="remove-row-{{$sellerProfile->id}}">
                                                                <div class="dz-details"> 
                                                                     <video width="100px" height="100px" controls>
                                                                        <source src="{{$videodata}}" type="video/mp4">
                                                                    </video> 
                                                                </div>
                                                                <a href="javascript:" onclick="removeIntroducy_video()" class="dz-remove">Remove video</a></div>
                                                            </div>
                                                            @endif

                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                    @else
                                                        <div class="addimg_brdr">
                                                           <div class="form-group">
                                                              <div class="cl-mcont" style="">
                                                                    <div action="{{url('seller/ajax/upload/introducy_video/1')}}" class="" id="introducy_video" multiple="false" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    
                                                    <div id="validatemessagevideo" style="display: none"><ul class="parsley-errors-list filled"><li class="parsley-required">Please upload introducy video.</li></ul></div>
                                                    
                                                    <script type="text/javascript">

                                                        var mydropzone;
                                                        jQuery(document).ready(function(){
                                                            
                                                            jQuery("#introducy_video").addClass("dropzone");
                                                          mydropzone = new Dropzone("#introducy_video", {
                                                              multiple : false,
                                                              maxFiles:1,
                                                              maxFilesize:150,
                                                              acceptedFiles: ".mp4,.mkv,.webm,.mov,.m4v",
                                                              }).on("complete", function(file) {
                                                                  $("#introducy_video").html(file.xhr.response);
                                                              }).on("removedfile", function(file) {
                                                                   //removeIntroducy_video();
                                                              });
                                                           });

                                                           function removeIntroducy_video(){
                                                                document.getElementById('load').style.visibility="visible"; 
                                                                var formData = "";
                                                                var actionurl="{{url('seller/ajax/remove/introducy_video')}}";
                                                                $.ajax({
                                                                    processData: false,
                                                                    contentType: false,
                                                                    type    : "GET",
                                                                      url     : actionurl,
                                                                      data    : formData,
                                                                      success : function( res ){
                                                                        document.getElementById('load').style.visibility='hidden';
                                                                        mydropzone.options.maxFiles = mydropzone.options.maxFiles + 1;
                                                                        $(".edit-img").remove();
                                                                      },
                                                                      error: function ( jqXHR, exception ) {
                                                                      }
                                                                });
                                                           }
                                                     </script>

                                                  
                                                </div>
                                            </div>
                                            
                                            <div style="clear: both"></div>
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="open-browser ">
                                                    <label class="field-name">
                                                        Add your work videos (up to 150MB)<br>
                                                        {{-- <span style="margin-top:10px;display: inline-block;font-size: 14px;">H : 435px , W : 650px </span><br> --}}
                                                        <span style="font-size: 14px;">Supported formats: mp4, mkv, webm, mov, m4v <br></span>
                                                        <span style="font-size: 14px;">(Video is optional)</span>
                                                    </label>
                                                 
                                                    @if(isset($sellerProfile->id) && $sellerProfile->id != '')
                                                        <div class="addimg_brdr">
                                                           <div class="form-group">
                                                              <div class="cl-mcont" style="">
                                                        
                                                        <div action="{{url('seller/ajax/upload/work_video')}}/{{$sellerProfile->id}}" class="" id="workdata_video" multiple="true" >
                                                            
                                                            @if(isset($sellerProfile->v_work_video) && count($sellerProfile->v_work_video))
                                                            @foreach($sellerProfile->v_work_video as $k=>$v)
                                                            <div class="dropzone-previews" style="width: auto;display: inline-block;" id="remvideo{{$k}}">
                                                                @php
                                                                    $videodata = \Storage::cloud()->url($v);     
                                                                @endphp
                                                                <div class="dz-preview dz-processing dz-success dz-image-preview edit-videowork"  id="remove-row-{{$sellerProfile->id}}">
                                                                <div class="dz-details"> 
                                                                     <video width="100px" height="100px" controls>
                                                                        <source src="{{$videodata}}" type="video/mp4">
                                                                    </video> 
                                                                </div>
                                                                <a href="javascript:" onclick="removeWork_video('{{$v}}','{{$k}}')" class="dz-remove">Remove video</a></div>
                                                            </div>
                                                            @endforeach
                                                            @endif

                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                    @else
                                                        <div class="addimg_brdr">
                                                           <div class="form-group">
                                                              <div class="cl-mcont" style="">
                                                                <div action="{{url('seller/ajax/upload/work_video/1')}}" class="" id="workdata_video" multiple="true" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    @endif
                                                    
                                                    <script type="text/javascript">
                                                        var mydropzonework;
                                                        jQuery(document).ready(function(){
                                                            jQuery("#workdata_video").addClass("dropzone");
                                                        mydropzonework = new Dropzone("#workdata_video", {
                                                              multiple : true,
                                                              maxFiles:150,
                                                              maxFilesize:150,
                                                              parallelUploads: 1,
                                                              acceptedFiles: ".mp4,.mkv,.webm,.mov,.m4v",
                                                              }).on("complete", function(file) {
                                                                  $("#workdata_video").html(file.xhr.response);    
                                                              }).on("removedfile", function(file) {
                                                                   removeWork_video();
                                                              });
                                                           });
                                                           function removeWork_video(iname,indexdata){
                                                                document.getElementById('load').style.visibility="visible"; 
                                                                var formData = "name="+iname;
                                                                var actionurl="{{url('seller/ajax/remove/work_video')}}";
                                                                $.ajax({
                                                                    processData: false,
                                                                    contentType: false,
                                                                    type    : "GET",
                                                                      url     : actionurl,
                                                                      data    : formData,
                                                                      success : function( res ){
                                                                         document.getElementById('load').style.visibility='hidden';
                                                                         mydropzonework.options.maxFiles = mydropzonework.options.maxFiles + 1;
                                                                         $("#remvideo"+indexdata).remove();
                                                                      },
                                                                      error: function ( jqXHR, exception ) {
                                                                      }
                                                                });
                                                            }
                                                     </script>
                                                </div>
                                            </div>

                                            <div class="col-sm-12 col-xs-12">
                                                <div class="open-browser ">
                                                    <label class="field-name" id="validatemessagephotolbl">
                                                        Add your work photos (up to 10)<br>
                                                        <span style="margin-top:0px;display: inline-block;font-size: 14px;">H : 435px , W : 650px </span><br>
                                                    </label>
                                                    @php $totalphotos = 10; @endphp
                                                    
                                                    @if(isset($sellerProfile->id) && $sellerProfile->id != '')
                                                        <div class="addimg_brdr">
                                                           <div class="form-group">
                                                              <div class="cl-mcont" style="">
                                                        <div action="{{url('seller/ajax/upload/work_photos')}}/{{$sellerProfile->id}}" class="" id="workdata_photo" multiple="true" >
                                                            @if(isset($sellerProfile->v_work_photos) && count($sellerProfile->v_work_photos))
                                                            @php $totalphotos = 10-count($sellerProfile->v_work_photos); @endphp
                                                            @foreach($sellerProfile->v_work_photos as $k=>$v)
                                                            @php
                                                                $imgdaata = \Storage::cloud()->url($v);     
                                                            @endphp
                                                            <div class="dropzone-previews" style="width: auto;display: inline-block;" id="rem{{$k}}">
                                                                <div class="dz-preview dz-processing dz-success dz-image-preview"  id="remove-row-{{$sellerProfile->id}}">
                                                                <div class="dz-details"> 
                                                                     <img src="{{$imgdaata}}" style="max-width:100px;">
                                                                </div>
                                                                <a href="javascript:" onclick="removeWork_workphotos('{{$v}}','{{$k}}')" class="dz-remove">Remove image</a></div>
                                                            </div>
                                                            @endforeach
                                                            @endif
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                    @else
                                                        <div class="addimg_brdr">
                                                           <div class="form-group">
                                                              <div class="cl-mcont" style="">
                                                                <div action="{{url('seller/ajax/upload/work_photos/1')}}" class="" id="workdata_photo" multiple="true" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    @endif

                                                    <div id="validatemessagephoto" style="display: none"><ul class="parsley-errors-list filled"><li class="parsley-required">Please upload your work photos.</li></ul></div>
                                                    
                                                    <script type="text/javascript">
                                                        var totalphotos = '{{$totalphotos}}';    
                                                        var mydropzonephoto;
                                                        jQuery(document).ready(function(){
                                                            jQuery("#workdata_photo").addClass("dropzone");
                                                            mydropzonephoto = new Dropzone("#workdata_photo", {
                                                              multiple : true,
                                                              maxFiles:parseInt(totalphotos),
                                                              maxFilesize:2,
                                                              parallelUploads: 1,
                                                              acceptedFiles: ".jpeg,.jpg,.png",
                                                              }).on("complete", function(file) {
                                                                  $("#workdata_photo").html(file.xhr.response);    
                                                              }).on("removedfile", function(file) {
                                                                   //removeWork_video();
                                                              });
                                                           });

                                                           function removeWork_workphotos(iname,indexdata){
                                                                document.getElementById('load').style.visibility="visible"; 
                                                                var formData = "name="+iname;
                                                                var actionurl="{{url('seller/ajax/remove/work_photos')}}";
                                                                $.ajax({
                                                                    // processData: false,
                                                                    // contentType: false,
                                                                    type    : "GET",
                                                                      url     : actionurl,
                                                                      data    : formData,
                                                                      success : function( res ){
                                                                        document.getElementById('load').style.visibility='hidden';
                                                                        $("#rem"+indexdata).remove();
                                                                        mydropzonephoto.options.maxFiles = mydropzonephoto.options.maxFiles + 1;

                                                                      },
                                                                      error: function ( jqXHR, exception ) {
                                                                      }
                                                                });
                                                            }
                                                     </script>
                                                </div>
                                            </div>

                                            <?php /* 
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="open-browser">
                                                    <label class="field-name">
                                                        Add Your work photos (up to 10)
                                                    </label>

                                                    <div action="{{url('seller-panel/artworks/fileupload/')}}" class="" id="image-dropzone1" multiple="true" >
                                                    </div>
                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function(){
                                                            jQuery("#image-dropzone1").addClass("dropzone");
                                                            new Dropzone("#image-dropzone1", {
                                                              multiple : true,
                                                              maxFiles:50,
                                                              maxFilesize:1,
                                                              acceptedFiles: ".jpeg,.jpg,.png",
                                                              }).on("complete", function(file) {
                                                                jQuery("#image-dropzone1 .dz-success input").each(function(){
                                                                    if(file.name == $(this).val()){
                                                                       jQuery(this).attr("name", "v_image[]");
                                                                    }
                                                                });
                                                              });
                                                           });
                                                     </script>


                                                    {{-- <input type="text" class="form-control input-field" id="workphotoid" disabled>
                                                    <div class="text-right">
                                                        <input type='file' id="fileupload-example-4" name="work_photo[]" onchange="workphoto();" multiple />
                                                        <label id="fileupload" class="btn-editdetail" for="fileupload-example-4">Browser</label>
                                                    </div> --}}
                                                </div>
                                            </div>
                                            */ ?>

                                        </div>
                                    </div>

                                  
                                    <div style="clear: both"></div>
                                    <div class="dirservice-form">
                                        <div class="dirservice-field">
                                            
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="buttet-concept">
                                                        <div class="row">
                                                            <div class="col-sm-12 col-xs-12">
                                                                <p class="field-name">
                                                                    {{-- Add Your Services (max 3 packages) --}}
                                                                    Add your service (Basic, Standard & Premium). 
                                                                </p>
                                                                <div class="extra-package show_basic_pkg_btn arrow_up_down active_arrow">
                                                                    Basic Service
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="show_basic_pkg">
                                                        <div class="package-added">
                                                            <input type="hidden" name="information[basic_package][v_title]" value="Basic">
                                                            
                                                            {{-- <div class="package-title">
                                                                <label class="subfield-name">
                                                                    Package 1 title
                                                                </label>
                                                                <input type="text" maxlength="18" class="form-control input-field" name="information[basic_package][v_title]" required value="@if(isset($sellerProfile->information['basic_package']['v_title'])){{$sellerProfile->information['basic_package']['v_title']}} @endif">
                                                            </div> --}}

                                                            <div class="package-title" id="basicpackage_bullets">
                                                                
                                                                <label class="subfield-name">
                                                                    What's Included?
                                                                </label>
                                                                @if(isset($sellerProfile->information['basic_package']['v_bullets']) && count($sellerProfile->information['basic_package']['v_bullets']))
                                                                @foreach($sellerProfile->information['basic_package']['v_bullets'] as $k=>$v)
                                                                
                                                                @if($k>0)
                                                                <div class="input_close">
                                                                <input type="text" maxlength="18" style="margin-top: 5px;" class="form-control input-field" name="information[basic_package][v_bullets][]" value="{{$v}}">
                                                                <a href="javascript:;"  class="removebullet"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}"></a>
                                                                </div>
                                                                @else
                                                                <input type="text" maxlength="18" style="margin-top: 5px;" class="form-control input-field" name="information[basic_package][v_bullets][]" value="{{$v}}">
                                                                @endif
                                                                @endforeach
                                                                @else
                                                                <input type="text" maxlength="18" class="form-control input-field" name="information[basic_package][v_bullets][]" required>
                                                                @endif
                                                            </div>
                                                            <div class="text-right">
                                                                <button name="button" type="button" class="btn btn-bullet" onclick="addBullets('information[basic_package][v_bullets][]','basicpackage_bullets')">Add Bullets
                                                                </button>
                                                            </div>
                                                        </div>
                                                        
                                                        <?php   
                                                           $delivery_type=array(
                                                                'hours'=>'Hour(s)',
                                                                'day'=>"Day(s)",
                                                                'month'=>"Month(s)",
                                                                'year'=>"Year(s)",
                                                            ); 
                                                        ?>  

                                                        <div class="time-buttle">
                                                            <label class="subfield-name">
                                                                Delivery Time
                                                            </label>
                                                            <div class="time-concept">
                                                                <div class="row">
                                                                    <div class="col-sm-7">
                                                                    <div class="dilivery-hours">
                                                                        <input type="number" step="any" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" name="information[basic_package][v_delivery_time]" required  value="@if(isset($sellerProfile->information['basic_package']['v_delivery_time'])){{$sellerProfile->information['basic_package']['v_delivery_time']}}@endif">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-5">
                                                                    <div class="dilivery-time">
                                                                        <select class="form-control form-sell" name="information[basic_package][v_delivery_type]" required>
                                                                                
                                                                            @foreach($delivery_type as $k=>$v)
                                                                                <option value="{{$k}}" @if(isset($sellerProfile->information['basic_package']['v_delivery_type']) && $sellerProfile->information['basic_package']['v_delivery_type']==$k) selected @endif >{{$v}}</option>
                                                                            @endforeach
                                                                        
                                                                        </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="buttet-option">
                                                                <label class="buttet-heading">
                                                                    Package price (£)
                                                                    <br><span style="font-size: 13px;">Minimum recommended project fee £5</span>
                                                                </label>
                                                                <input type="number" step="any" min="5" onkeydown="javascript: return event.keyCode == 69 ? false : true" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" class="form-control input-field" name="information[basic_package][v_price]" data-parsley-type="number" required value="@if(isset($sellerProfile->information['basic_package']['v_price'])){{$sellerProfile->information['basic_package']['v_price']}}@endif">
                                                            </div>
                                                        </div>

                                                        <div class="more-package">
                                                            <label class="buttet-money">
                                                                Add-ons (Add extras to make more money e.g. £5.99 )
                                                            </label>
                                                            
                                                            <div class="package-added" >
                                                              <div id="basicpackageaddon">
                                                                
                                                                @if(isset($sellerProfile->information['basic_package']['add_on']['title']) && count($sellerProfile->information['basic_package']['add_on']['title']))
                                                                @foreach($sellerProfile->information['basic_package']['add_on']['title'] as $k=>$v)
                                                                
                                                                <div>
                                                                <div class="package-title">
                                                                    <label class="buttet-heading">
                                                                        <span>Add-on title (max 2 add-ons)</span>
                                                                        <br><span style="font-size: 13px;">i.e. 24hr Delivery</span>
                                                                        @if($k>0)
                                                                        <a href="javascript:;" class="removeaddon pull-right"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}"></a>
                                                                        @endif
                                                                    </label>
                                                                    <input type="text" class="form-control input-field" name="information[basic_package][add_on][title][]"  value="{{$v}}">
                                                                </div>
                                                                
                                                                <div class="package-title">
                                                                    <label class="buttet-heading">
                                                                        Add-on price (£)
                                                                    </label>
                                                                    <input type="number" step="any" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" name="information[basic_package][add_on][v_price][]" value="@if(isset($sellerProfile->information['basic_package']['add_on']['v_price'][$k])){{$sellerProfile->information['basic_package']['add_on']['v_price'][$k]}}@endif">
                                                                </div>
                                                                </div>

                                                                @endforeach
                                                                
                                                                @else
                                                                <div class="package-title">
                                                                    <label class="buttet-heading">
                                                                        Add-on title (max 2 add-ons)
                                                                        <br><span style="font-size: 13px;">i.e. 24hr Delivery</span>
                                                                    </label>
                                                                    <input type="text" class="form-control input-field" name="information[basic_package][add_on][title][]" >
                                                                </div>
                                                                
                                                                <div class="package-title">
                                                                    <label class="buttet-heading">
                                                                        Add-on price (£)
                                                                    </label>
                                                                    <input type="number" step="any" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" name="information[basic_package][add_on][v_price][]" >
                                                                </div>

                                                                @endif

                                                              </div>
                                                                
                                                                <div class="text-right basicpackageaddon">
                                                                    <button name="button" type="button" class="btn btn-editdetail" onclick="addAddon('information[basic_package]','basicpackageaddon')"> Add more </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="buttet-concept">
                                                        <div class="extra-package show_standard_pkg_btn arrow_up_down">
                                                            Standard Service
                                                        </div>
                                                        <div class="show_standard_pkg">
                                                        <div class="package-added">
                                                            <input type="hidden" name="information[standard_package][v_title]" value="Standard">

                                                            {{-- <div class="package-title">
                                                                <label class="subfield-name">
                                                                    Package 2 title
                                                                </label>
                                                                <input type="text" maxlength="18" class="form-control input-field" name="information[standard_package][v_title]" value="@if(isset($sellerProfile->information['standard_package']['v_title'])){{$sellerProfile->information['standard_package']['v_title']}}@endif">
                                                            </div> --}}
                                                            
                                                            <div class="package-title" id="standardpackage_bullets">
                                                                <label class="subfield-name">
                                                                    What's Included?
                                                                </label>
                                                                @if(isset($sellerProfile->information['standard_package']['v_bullets']) && count($sellerProfile->information['standard_package']['v_bullets']))
                                                                @foreach($sellerProfile->information['standard_package']['v_bullets'] as $k=>$v)
                                                                
                                                                @if($k>0)
                                                                    <div class="input_close">
                                                                        <input type="text" maxlength="18" style="margin-top: 5px;" class="form-control input-field" name="information[standard_package][v_bullets][]" value="{{$v}}">
                                                                        <a href="javascript:;"  class="removebullet"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}"></a>
                                                                    </div>
                                                                    @else
                                                                        <input type="text" maxlength="18" style="margin-top: 5px" class="form-control input-field" name="information[standard_package][v_bullets][]" value="{{$v}}">

                                                                @endif
                                                                
                                                                @endforeach
                                                                @else
                                                                <input type="text" maxlength="18" class="form-control input-field" name="information[standard_package][v_bullets][]">
                                                                @endif
                                                            </div>

                                                            <div class="text-right">
                                                                <button name="button" type="button" class="btn btn-bullet" onclick="addBullets('information[standard_package][v_bullets][]','standardpackage_bullets')" > Add Bullets
                                                                </button>
                                                            </div>
                                                        </div>

                                                        <div class="time-buttle">
                                                            <label class="subfield-name">
                                                                Delivery Time
                                                            </label>
                                                        </div>
                                                        <div class="time-concept">
                                                            <div class="row">
                                                                <div class="col-sm-7">
                                                                    <div class="dilivery-hours">
                                                                        <input type="number" step="any"  data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" name="information[standard_package][v_delivery_time]" data-parsley-trigger="keyup" value="@if(isset($sellerProfile->information['standard_package']['v_delivery_time'])){{$sellerProfile->information['standard_package']['v_delivery_time']}}@endif" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-5">
                                                                    <div class="dilivery-time">
                                                                        <select class="form-control form-sell" name="information[standard_package][v_delivery_type]">
                                                                            @foreach($delivery_type as $k=>$v)
                                                                                <option value="{{$k}}" @if(isset($sellerProfile->information['standard_package']['v_delivery_type']) && $sellerProfile->information['standard_package']['v_delivery_type']==$k) selected @endif >{{$v}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="buttet-option">
                                                                <label class="buttet-heading">
                                                                    Package price (£)
                                                                    <br><span style="font-size: 13px;">Minimum recommended project fee £5</span>
                                                                </label>
                                                                {{-- <select class="form-control form-sell" name="information[standard_package][v_price]">
                                                                    <option value="5.99"> £5.99 </option>
                                                                    <option value="6.99"> £6.99 </option>
                                                                </select> --}}
                                                                <input type="number" step="any" min="5" onkeydown="javascript: return event.keyCode == 69 ? false : true" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" class="form-control input-field" name="information[standard_package][v_price]"  value="@if(isset($sellerProfile->information['standard_package']['v_price'])){{$sellerProfile->information['standard_package']['v_price']}}@endif">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="more-package">
                                                            <label class="buttet-money">
                                                                Add-ons (Add extras to make more money e.g. £5.99 )
                                                            </label>
                                                            <div class="package-added">
                                                              
                                                              <div id="standardpackageaddon">
                                                                
                                                                @if(isset($sellerProfile->information['standard_package']['add_on']['title']) && count($sellerProfile->information['standard_package']['add_on']['title']))
                                                                @foreach($sellerProfile->information['standard_package']['add_on']['title'] as $k=>$v)
                                                                    <div class="package-title">
                                                                        <label class="buttet-heading">
                                                                            Add-on title (max 2 add-ons)
                                                                            <br><span style="font-size: 13px;">i.e. 24hr Delivery</span>
                                                                        </label>
                                                                        <input type="text" class="form-control input-field" name="information[standard_package][add_on][title][]" value="{{$v}}">
                                                                    </div>
                                                                    <div class="package-title">
                                                                        <label class="buttet-heading">
                                                                            Add-on price (£)
                                                                        </label>
                                                                        <input type="number" step="any" onkeydown="javascript: return event.keyCode == 69 ? false : true" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" class="form-control input-field" name="information[standard_package][add_on][v_price][]"  value="@if(isset($sellerProfile->information['standard_package']['add_on']['v_price'][$k])){{$sellerProfile->information['standard_package']['add_on']['v_price'][$k]}}@endif">
                                                                    </div>

                                                                @endforeach
                                                                
                                                                @else
                                                                    <div class="package-title">
                                                                        <label class="buttet-heading">
                                                                            Add-on title (max 2 add-ons)
                                                                            <br><span style="font-size: 13px;">i.e. 24hr Delivery</span>
                                                                        </label>
                                                                        <input type="text" class="form-control input-field" name="information[standard_package][add_on][title][]">
                                                                    </div>
                                                                    <div class="package-title">
                                                                        <label class="buttet-heading">
                                                                            Add-on price (£)
                                                                        </label>
                                                                        <input type="number" step="any" onkeydown="javascript: return event.keyCode == 69 ? false : true" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" class="form-control input-field" name="information[standard_package][add_on][v_price][]">
                                                                    </div>
                                                                
                                                                @endif
                                                                



                                                              </div>  
                                                              <div class="text-right standardpackageaddon">
                                                                    <button name="button" type="button" class="btn btn-editdetail" onclick="addAddon('information[standard_package]','standardpackageaddon')"> Add more </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="buttet-concept">
                                                        <div class="extra-package show_premium_pkg_btn arrow_up_down">
                                                            Premium Service
                                                        </div>
                                                        <div class="show_premium_pkg">
                                                        <div class="package-added">
                                                            
                                                            <input type="hidden" name="information[premium_package][v_title]" value="Premium">
                                                            
                                                            {{-- <div class="package-title">
                                                                <label class="subfield-name">
                                                                    Package 3 title
                                                                </label>
                                                                <input type="text" maxlength="18" class="form-control input-field" name="information[premium_package][v_title]" value="@if(isset($sellerProfile->information['premium_package']['v_title'])){{$sellerProfile->information['premium_package']['v_title']}}@endif">
                                                            </div> --}}
                                                            
                                                            <div class="package-title" id="premium_bullets">
                                                                
                                                                

                                                                <label class="subfield-name">
                                                                    What's Included?
                                                                </label>
                                                                @if(isset($sellerProfile->information['premium_package']['v_bullets']) && count($sellerProfile->information['premium_package']['v_bullets']))
                                                                    @foreach($sellerProfile->information['premium_package']['v_bullets'] as $k=>$v)
                                                                        @if($k>0)
                                                                        <div class="input_close">
                                                                            <input type="text" maxlength="18" style="margin-top: 5px;" class="form-control input-field" name="information[premium_package][v_bullets][]" value="{{$v}}">
                                                                            <a href="javascript:;"  class="removebullet"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}"></a>
                                                                        </div>
                                                                        @else
                                                                        <input type="text" maxlength="18" style="margin-top:5px " class="form-control input-field" name="information[premium_package][v_bullets][]" value="{{$v}}">
                                                                        @endif

                                                                    @endforeach
                                                                @else    
                                                                <input type="text" maxlength="18" style="margin-top:5px " class="form-control input-field" name="information[premium_package][v_bullets][]">
                                                                @endif
                                                            </div>

                                                            <div class="text-right">
                                                                <button name="button"  type="button" class="btn btn-bullet" onclick="addBullets('information[premium_package][v_bullets][]','premium_bullets')"> Add Bullets
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="time-buttle">
                                                            <label class="subfield-name">
                                                                Delivery Time
                                                            </label>
                                                        </div>
                                                        <div class="time-concept">
                                                            <div class="row">
                                                                
                                                                <div class="col-sm-7">
                                                                <div class="dilivery-hours">
                                                                    <input type="number" step="any" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" name="information[premium_package][v_delivery_time]" value="@if(isset($sellerProfile->information['premium_package']['v_delivery_time'])){{$sellerProfile->information['premium_package']['v_delivery_time']}}@endif">
                                                                </div>
                                                                </div>

                                                                <div class="col-sm-5">
                                                                <div class="dilivery-time">
                                                                    <select class="form-control form-sell" name="information[premium_package][v_delivery_type]">
                                                                        @foreach($delivery_type as $k=>$v)
                                                                            <option value="{{$k}}" @if(isset($sellerProfile->information['premium_package']['v_delivery_type']) && $sellerProfile->information['premium_package']['v_delivery_type']==$k) selected @endif >{{$v}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                </div>
                                                            </div>
                                                            <div class="buttet-option">
                                                                
                                                                <label class="buttet-heading">
                                                                    Package price (£)
                                                                    <br><span style="font-size: 13px;">Minimum recommended project fee £5</span>
                                                                </label>
                                                                {{-- <select class="form-control form-sell" name="information[premium_package][v_price]">
                                                                    <option value="5.99"> £5.99 </option>
                                                                    <option value="6.99"> £6.99 </option>
                                                                </select> --}}

                                                                <input type="number" step="any" min="5" onkeydown="javascript: return event.keyCode == 69 ? false : true" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" class="form-control input-field" name="information[premium_package][v_price]"  data-parsley-trigger="keyup" value="@if(isset($sellerProfile->information['premium_package']['v_price'])){{$sellerProfile->information['premium_package']['v_price']}}@endif">

                                                            </div>
                                                        </div>
                                                        <div class="more-package">
                                                            <label class="buttet-money">
                                                                Add-ons (Add extras to make more money e.g. £5.99 )
                                                            </label>
                                                            <div class="package-added">
                                                              
                                                              <div id="premiumpackageaddon">

                                                                @if(isset($sellerProfile->information['premium_package']['add_on']['title']) && count($sellerProfile->information['premium_package']['add_on']['title']))
                                                                    @foreach($sellerProfile->information['premium_package']['add_on']['title'] as $k=>$v)

                                                                    <div class="package-title">
                                                                        <label class="buttet-heading">
                                                                            Add-on title (max 2 add-ons)
                                                                            <br><span style="font-size: 13px;">i.e. 24hr Delivery</span>
                                                                        </label>
                                                                        <input type="text" class="form-control input-field" name="information[premium_package][add_on][title][]" value="{{$v}}">
                                                                    </div>
                                                                    <div class="package-title">
                                                                        <label class="buttet-heading">
                                                                            Add-on price (£)
                                                                        </label>
                                                                        <input type="number" step="any" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" name="information[premium_package][add_on][v_price][]" value="@if(isset($sellerProfile->information['premium_package']['add_on']['v_price'][$k])){{$sellerProfile->information['premium_package']['add_on']['v_price'][$k]}}@endif">
                                                                    </div>
                                                                    @endforeach
                                                                @else

                                                                    <div class="package-title">
                                                                        <label class="buttet-heading">
                                                                            Add-on title (max 2 add-ons)
                                                                            <br><span style="font-size: 13px;">i.e. 24hr Delivery</span>
                                                                        </label>
                                                                        <input type="text" class="form-control input-field" name="information[premium_package][add_on][title][]">
                                                                    </div>
                                                                    <div class="package-title">
                                                                        <label class="buttet-heading">
                                                                            Add-on price (£)
                                                                        </label>
                                                                        <input type="number" step="any" onkeydown="javascript: return event.keyCode == 69 ? false : true" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" class="form-control input-field" name="information[premium_package][add_on][v_price][]">
                                                                    </div>
                                                               
                                                                @endif  

                                                                </div>

                                                                <div class="text-right premiumpackageaddon">
                                                                    <button name="button" type="button" class="btn btn-editdetail" onclick="addAddon('information[premium_package]','premiumpackageaddon')" > Add more </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                   
                                    <div class="row">
                                        <div class="col-xs-12">
                                             <div class="option-system">
                                            <label class="secure-payment">
                                                <b>Visit me</b><br>    
                                                Please add your social handle without @ or #. Your social media platform will be added automatically.
                                            </label>
                                        </div>
                                        </div>
                                    </div>    

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="name-details">
                                            <label class="subfield-name">Facebook</label>
                                            <input type="text" class="form-control input-field" value="{{isset($sellerProfile->v_facebook) ? $sellerProfile->v_facebook:''}}" name="v_facebook">
                                            </div>
                                        </div>

                                        <div class="col-xs-6">
                                            <div class="name-details">
                                                <label class="subfield-name">Twitter</label>
                                                <input type="text" class="form-control input-field" value="{{isset($sellerProfile->v_twitter) ? $sellerProfile->v_twitter:''}}" name="v_twitter">
                                            </div>
                                        </div>

                                         <div class="col-xs-6">
                                            <div class="name-details">
                                                <label class="subfield-name">Pinterest</label>
                                                <input type="text" class="form-control input-field" value="{{isset($sellerProfile->v_pinterest) ? $sellerProfile->v_pinterest:''}}" name="v_pinterest">
                                            </div>
                                        </div>

                                        <div class="col-xs-6">
                                             <div class="name-details">
                                                <label class="subfield-name">Instagram</label>
                                                <input type="text" class="form-control input-field" value="{{isset($sellerProfile->v_instagram) ? $sellerProfile->v_instagram:''}}" name="v_instagram">
                                            </div>
                                        </div>

                                    </div>    

                                    <!--demo -->
                                    <div class="name-details" style="margin-top: 20px">
                                        <label class="subfield-name order-topspace">
                                            Order requirements
                                        </label>
                                    </div>

                                    <div class="name-details">
                                        <label class="subfield-name specify-orderspace">
                                            Please specify any order requirements you would like your customers to know before they contact you.
                                        </label>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <textarea name="v_order_requirements" id="v_order_requirements" required>{{$sellerProfile->v_order_requirements or ''}}</textarea>

                                        </div>
                                        <script>
                                           // CKEDITOR.replace("v_order_requirements");
                                            // $('#v_order_requirements').ckeditor();
                                        </script>
                                    </div>

                                    <div class="buttet-concept">
                                        <div class="name-details">
                                            <div class="searching-topspace">
                                                <label class="field-name">
                                                    Add search tags
                                                </label>
                                            </div>
                                        </div>
                                        <div class="name-details">
                                            <label class="subfield-name">
                                                Please add up to 3 search tags related to your service or job.
                                            </label>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="input-login">
                                                            <input class="form-control business-name-control" type="text" placeholder="Start typing ...." id="temp_text">
                                                            <ul class="parsley-errors-list filled" id="parsley-id-tags" style="display: none;">
                                                            <li class="parsley-type">You have already enter 3 tags.</li>
                                                            </ul>
                                                        </div>
                                                    </div>


                                                    <div class="col-xs-2 terms-add" id="hiddentags">
                                                    @php $tagcnt=0; @endphp
                                                        @if(isset($sellerProfile->v_search_tags) && count($sellerProfile->v_search_tags))
                                                            @foreach($sellerProfile->v_search_tags as $k=>$v)
                                                                <input type="hidden" id="hidden-{{$v}}" name="v_search_tags[]" value="{{$v}}">
                                                                @php $tagcnt=$tagcnt+1; @endphp
                                                            @endforeach
                                                        @endif    

                                                        <div class="login-btn" id="addtagbutton" @if($tagcnt>3)  style="display: none" @endif>
                                                            <button type="button" id="adddatafun" class="btn add-now" onclick="myFunction()">Add</button>
                                                        </div>

                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="selection-point" id="add_text">
                                                            @if(isset($sellerProfile->v_search_tags) && count($sellerProfile->v_search_tags))
                                                                @foreach($sellerProfile->v_search_tags as $k=>$v)
                                                            <button type="button" class='auto-width' id='{{$v}}' onclick='removeButton(this.id)'><span>{{$v}}</span><img src="{{url('public/Assets/frontend/images/close1.png')}}"></button>
                                                                @endforeach
                                                            @endif    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            
                                        <div id="validatemessagetags" style="display: none;"><ul class="parsley-errors-list filled"><li class="parsley-required">Please add at least one search tag to proceed.</li></ul></div>

                                    </div>
                                    <!--end-demo -->  
                                    <div class="btn-backnext">
                                        
                                        <button type="button" class="btn form-save-exit ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="saveback" onclick="saveStep2('saveback')"> Save & exit
                                        </button>
                                        @if($planstep==1)
                                        <button type="button" class="btn form-next ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savenext3" onclick="saveStep2('savenext3')"> Next </button>
                                        @else
                                        <button type="button" class="btn form-next ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="save" onclick="saveStep2('save')"> Save </button>  
                                        @endif

                                    </div>
                                  </form>  
                                </div>
                            </div>
                        </div>



                        <div style="clear: both"></div>    
                        <div class="alert alert-info alert-danger" id="errormsgstep2" style="display: none;margin-top: 20px;">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;<span id="errortxtstep2">Something went wrong.</span>
                        </div>

                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $('#temp_text').keypress(function(event){
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if(keycode == 13){
                        $('#adddatafun').click();
                    }
                });
            </script>

            @php
                $standardplanFree= App\Helpers\General::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
            @endphp  

            <!-- End 02B-sign-up-i-want-to-sell-online-SELLER-W2 -->
            @if($planstep==1)
            <div class="tab-pane" id="step3">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main-dirservice3">
                            <form class="horizontal-form" role="form" method="POST" name="planform" id="planform" action="{{url('seller/plan-data')}}" data-parsley-validate enctype="multipart/form-data" >
                            {{ csrf_field() }}

                            <div class="step3-dirservice">
                                <div class="title-dirservice">
                                    <h2> @if($v_service=="online")
                                            Online
                                        @else
                                            In Person
                                        @endif    
                                        Services </h2>
                                </div>

                                <div class="foll-option">
                                    Please choose the plan that best fits your needs
                                </div>
                                <div class="wrap">
                                        <fieldset>
                                            <div class="toggle">
                                                <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join" checked onchange="plandurationchange('monthly')" >
                                                <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                                <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create" onchange="plandurationchange('yearly')">
                                                <label for="create" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                                <span class="toggle-selection"></span>
                                            </div>
                                        </fieldset>
                                </div>
                            </div>

                            <div class="createLabel">
                                
                            @if(count($plandata))
                                @foreach($plandata as $key=>$val)

                                <div class="section">
                                    <div class="row">
                                        
                                        <div class="col-sm-3 col-xs-12">
                                            <div class="@if($key==0)left-choise @else left-side @endif">
                                                <div class="reg">
                                                <label style="font-weight: normal;">
                                                    <bdo dir="ltr">
                                                        <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" @if($key==0) checked @endif required>
                                                        <span class="select-seller-job"></span>
                                                        <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                    </bdo>
                                                <label style="font-weight: normal;">
                                                </div>
                                                <div class="choise-que">{{$val['v_subtitle']}}</div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-3 col-xs-12">
                                            <ul class="list-unstyled choise-avalible">
                                                
                                                <?php 
                                                    $lcnt=1;
                                                ?>
                                                @if(count($val['l_bullet']))
                                                    @foreach($val['l_bullet'] as $k=>$v) 
                                                        @if($k<3)
                                                        <li class="right-mark"><img src="images/tick.png" class="tick" alt="" />{{$v}}</li>
                                                        <?php $lcnt = $lcnt+1; ?>
                                                        @endif
                                                    @endforeach
                                                @endif       
                                                
                                            </ul>
                                        </div>
                                        
                                        <div class="col-sm-4 col-xs-12">
                                            <div class="right-display">
                                                
                                                <ul class="list-unstyled choise-avalible">
                                                    <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k>=3)
                                                            <li class="right-mark"><img src="images/tick.png" class="tick" alt="" />{{$v}}</li>
                                                            @endif
                                                            <?php $lcnt = $lcnt+1; ?>
                                                        @endforeach
                                                    @endif  
                                                </ul>
                                                <?php /*
                                                @if(count($val['l_addon']['text']))
                                                    @foreach($val['l_addon']['text'] as $k=>$v) 
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="{{$val['l_addon']['price'][$k]}}" name="v_plan[{{$v}}]">
                                                            <span class="urgent-offer"> 
                                                                {{$v}}
                                                            </span>
                                                        </label>
                                                    @endforeach
                                                @endif  
                                                */ ?>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-2 col-xs-12 same-height-1">
                                            <div class="@if($key==0) free-price @else final-price @endif">
                                                <div class="all-in-data">
                                                    
                                                <div  class="monthyprice">
                                                       @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_monthly_price']}}</del>
                                                            </div>
                                                            Only £{{$val['f_monthly_dis_price']}} p/m
                                                            
                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif

                                                       @endif  
                                                </div>  
                                                
                                                <div class="yealryprice" style="display: none;">
                                                       @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_yealry_price']}}</del>
                                                            </div>
                                                            Only £{{$val['f_yealry_dis_price']}} p/a

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif

                                                       @endif  
                                                </div>     

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                
                                @endforeach
                            @endif    
                            </div>
                            
                            <button type="submit" class="btn btn-payment">
                                Proceed
                            </button>
                            </form>
                       
                        </div>
                    </div>
                </div>
            </div>
            @endif
            
            
        </div>
    </div>



@stop
@section('js')
<script src="{{url('public/Assets/plugins')}}/dropzone/dropzone.js"></script>
<script type="text/javascript">

        jQuery(document).ready(function(){
            // $('#v_order_requirements').ckeditor();
            CKEDITOR.replace("l_brief_description");
            CKEDITOR.replace("v_order_requirements");
            var other_id = [];
            var cat_id = '{{isset($sellerProfile->i_category_id) ? $sellerProfile->i_category_id : ''}}';
            var nonblanks = $('.item[value!=""]').length;
            
            if(cat_id != ''){
                getSelOtherSkill(cat_id);
            }

            if(nonblanks != 0){
                $("#otherskill").prop('required',false);
            }
            
        });
        
        function myFunction() {
            
            var cnt = $('#add_text').children().length
            
           
            if(cnt>=3){
                $("#parsley-id-tags").css("display","block");
                jQuery('#temp_text').val("");
                return 0;
            }else{
                $("#parsley-id-tags").css("display","none");
            }

            var dt = new Date();
            var dynamic_id = dt.getDate() + "" + dt.getHours() + "" + dt.getMinutes() + "" + dt.getSeconds() + "" + dt.getMilliseconds();
            var y = jQuery('#temp_text').val();
            
            if(y==""){
                return 0;
            }
            var z = '<button>';
            jQuery('#add_text').append("<button class='auto-width' type='button' id='" + dynamic_id + "' onclick='removeButton(this.id)'><span>" + y + "</span><img src='{{url('public/Assets/frontend/images/close1.png')}}'></button>");
            
            var cnt = $('#add_text').children().length

            if(cnt==3){
                $("#addtagbutton").css("display","none");
            }

            var abval = $("#temp_text").val();
            var strhidden = '<input type="hidden" id="hidden-'+dynamic_id+'" name="v_search_tags[]" value="'+abval+'">';
            $("#hiddentags").append(strhidden);
            $("#temp_text").val("");
            $("#validatemessagetags").css("display","none");


        }

        function removeButton(id) {
            jQuery("#" + id).remove();
            jQuery("#hidden-"+id).remove();
            
            var cnt = $('#add_text').children().length
            if(cnt<3){
                $("#addtagbutton").css("display","block");
            }

        }

</script>


<script type="text/javascript">

        var closeimgurl = "{{url('public/Assets/frontend/images/pinkclose.png')}}";
        $(document).on("click", ".removebullet", function() {
               $(this).parent().remove(); 
        });

        $(document).on("click", ".removeaddon", function() {
            $(this).parent().parent().parent().remove();
            if($("#premiumpackageaddon > div").length==2){
                $(".premiumpackageaddon").css("display","block");
            }
            if($("#basicpackageaddon > div").length==1){
                $(".basicpackageaddon").css("display","block");
            }
            if($("#standardpackageaddon > div").length==1){
                $(".standardpackageaddon").css("display","block");
            }
            
        });

        function addBullets(name,id){
            var strrappend = '<div class="input_close"><input type="text" style="margin-top:5px" class="form-control input-field" name="'+name+'"><a href="javascript:;" class="removebullet"><img src="'+closeimgurl+'"></a><div>';
            // var strrappend = ' <input type="text" style="margin-top:5px" class="form-control input-field" name="'+name+'">';
            $("#"+id).append(strrappend);

        }

        function addAddon(name,id){
          
          var strrappend ='<div><div class="package-title"><label class="buttet-heading">Add-on title (max 2 add-ons)<a href="javascript:;" class="removeaddon pull-right"><img src="'+closeimgurl+'"></a></label><input type="text" class="form-control input-field" name="'+name+'[add_on][title][]"></div><div class="package-title"><label class="buttet-heading">Add-on price (£)</label><input type="number" step="any" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" name="'+name+'[add_on][v_price][]"></div></div>';
          $("#"+id).append(strrappend);
          $("."+id).css("display","none");
        
        }
        //<input type="hidden" name="information[v_tags][]" value="'+abval+'">

        function addTags(){
          
          var abval = $("#tagvalue").val();

          var strrappend = '<div class="package-title"><label class="buttet-heading">Add-on title (max 2 add-ons)</label><input type="text" class="form-control input-field" name="'+name+'[add_on][title][]"></div><div class="package-title"><label class="buttet-heading">Add-on price</label><input type="text" class="form-control input-field" name="'+name+'[add_on][v_price][]"></div>';
          
          $("#"+id).append(strrappend);
          $("."+id).html("");

        }


        function getSkill(i_category_id){
            
            var formdata = "i_category_id="+i_category_id;
            var actionurl = "{{url('seller-signup/get-skill')}}";

            $.ajax({
                  type    : "GET",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#sel_other_skill").html('');
                        $("#otherskill").prop('required',true);
                        $("#mainskill").html(obj['optionstr']);
                        $("#otherskill").html(obj['optionstr']);
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                  }
              });    
        }


        function saveStep1(typedata=""){
          
          var actionurl = "{{url('seller/update-profile')}}";
          var formValidFalg = $("#sellersignupste1pform").parsley().validate('');

          if(formValidFalg){
                
                document.getElementById('load').style.visibility="visible"; 
                var l = Ladda.create(document.getElementById(typedata));
                l.start();  
                var formdata = $("#sellersignupste1pform").serialize();

                $.ajax({
                    type    : "POST",
                    url     : actionurl,
                    data    : formdata,
                    success : function( res ){
                        document.getElementById('load').style.visibility='hidden';
                        l.stop();
                        var obj = jQuery.parseJSON(res);
                      
                        if(obj['status']==1){
                            
                            if(typedata=="savenext"){
                                $('.nav-tabs a[href="#step2"]').tab('show');
                                window.scrollTo(0, 0);
                            }else{
                                window.location = "{{url('seller/my-profile')}}";
                            }
                            $('#tstep1').attr('data-toggle','tab');
                        }else{
                            $("#errormsg").show();
                            $("#errortxt").html(obj['msg']);
                        }
                    },

                    error: function ( jqXHR, exception ) {
                        document.getElementById('load').style.visibility='hidden';
                        l.stop();
                        $("#errormsg").show();
                    }
              });

          }    
        }



        function saveStep2(type=""){
          
          var actionurl = "{{url('seller/update-profile')}}";
          var formValidFalg = $("#sellersignupste2pform").parsley().validate('');

          // var introvideo = $("#introducy_video").find(".dropzone-previews").length
          // if(introvideo<1){
          //   $("#validatemessagevideo").css("display","block");
          //   $('html, body').animate({
          //       scrollTop: $("#validatemessagevideolbl").offset().top
          //   }, 2000);
          //   return false;
          // }else{
          //   $("#validatemessagevideo").css("display","none");
          // }

          var workdata_photo = $("#workdata_photo").find(".dropzone-previews").length
          if(workdata_photo<1){
            $("#validatemessagephoto").css("display","block");
            
            $('html, body').animate({
                scrollTop: $("#validatemessagephotolbl").offset().top
            }, 2000);

            
            return false;
          }else{
            $("#validatemessagephoto").css("display","none");
          }

          var desc = CKEDITOR.instances['l_brief_description'].getData();
           if(desc==""){
                $("#validatemessagedesc").css("display","block");
                $('html, body').animate({
                    scrollTop: $("#validatemessagedesclbl").offset().top
                }, 2000);
                return false;
            }else{
                $("#validatemessagedesc").css("display","none");
            }


            var tags = $("#add_text").find("button").length
            if(tags<1){
                $("#validatemessagetags").css("display","block");
                return false;
            }else{
                $("#validatemessagetags").css("display","none");
            }

          if(formValidFalg){
              document.getElementById('load').style.visibility="visible"; 
              var l = Ladda.create(document.getElementById(type));
              l.start();
              
              var formData = new FormData($('#sellersignupste2pform')[0]);

              var desc = CKEDITOR.instances['v_order_requirements'].getData();
              formData.append('v_order_requirements', desc);
              var desc = CKEDITOR.instances['l_brief_description'].getData();
              formData.append('l_brief_description', desc);

              $.ajax({
                  processData: false,
                  contentType: false,
                  type    : "POST",
                  url     : actionurl,
                  data    : formData,
                  success : function( res ){

                      document.getElementById('load').style.visibility='hidden';
                      l.stop();
                      var obj = jQuery.parseJSON(res);
                      
                      if(obj['status']==1){
                        if(type == "savenext3"){
                            $('.nav-tabs a[href="#step3"]').tab('show');
                            window.scrollTo(0, 0);
                        }else if(type == "saveback"){
                            window.location = "{{url('seller/my-profile')}}";

                            //$('.nav-tabs a[href="#step1"]').tab('show');
                        }else{
                            window.location = "{{url('seller/my-profile')}}";
                        }
                        $('#tstep2').attr('data-toggle','tab');
                      }else{
                        $("#errormsgstep2").show();
                        $("#errortxtstep2").html(obj['msg']);
                      }

                  },
                  error: function ( jqXHR, exception ) {
                      document.getElementById('load').style.visibility='hidden';  
                      l.stop();
                      $("#errormsgstep2").show();
                  }
              });

          }    
        }


        function billing_detail(data){
          
            if(data=="bank"){
                
                $("#paypalinformation").css("display","none");    
                $("#bankinformation").css("display","block");  
                $("#paypalemail").removeAttr("required");

                $("#v_bankname").attr("required","true");
                $("#v_accountno").attr("required","true");
                $("#v_ifsc").attr("required","true");
                $("#v_account_holder_name").attr("required","true");
          
            }else{

                $("#paypalinformation").css("display","block");   
                $("#bankinformation").css("display","none");   
                $("#v_bankname").removeAttr("required");
                $("#v_accountno").removeAttr("required");
                $("#v_ifsc").removeAttr("required");
                $("#v_account_holder_name").removeAttr("required");
                $("#paypalemail").attr("required","true");

            }
        }

        //Plan duration change
        function plandurationchange(data=""){
            
            if(data=="monthly"){
                $(".monthyprice").css("display","block");
                $(".yealryprice").css("display","none");

            }else if(data=="yearly"){
                $(".yealryprice").css("display","block");
                $(".monthyprice").css("display","none");
            }
        }

        function removeimgjobpost(jid,data,remimg){
            $("#i_seller_id_delete").val(jid);
            $("#v_photo_name_delete").val(data);
            $("#v_span_delete").val(remimg);
            $("#deleteModalimg").modal("show");
        }
       
        function jobpostimagedelete(){
            
            var v_span = $("#v_span_delete").val();
            var formdata = $("#deleteimgjobpost").serialize();
            var actionurl = "{{url('seller/remove-img')}}";
            document.getElementById('load').style.visibility="visible";
            $.ajax({
                  type    : "POST",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#"+v_span).remove();
                        $("#deleteModalimg").modal("hide");
                      }
                      document.getElementById('load').style.visibility='hidden';

                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                      document.getElementById('load').style.visibility='hidden';
                  }
              });    
        }

        function removevideojobpost(jid,data,remimg){
            $("#i_seller_id_video").val(jid);
            $("#v_video_name_video").val(data);
            $("#v_span_video").val(remimg);
            $("#deleteModalvidso").modal("show");
        }

        function jobpostvideodelete(){
            
            var v_span = $("#v_span_video").val();
            var formdata = $("#deletevideojobpost").serialize();
            var actionurl = "{{url('seller/remove-video')}}";
            document.getElementById('load').style.visibility="visible";

            $.ajax({
                  type    : "POST",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#"+v_span).remove();
                        $("#deleteModalvidso").modal("hide");
                      }
                      document.getElementById('load').style.visibility='hidden';
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                      document.getElementById('load').style.visibility='hidden';
                  }
              });    
        }


        function removeworkvideo(jid,data,remimg){
            $("#i_seller_id_video").val(jid);
            $("#v_video_name_video").val(data);
            $("#v_span_video").val(remimg);
            $("#deleteModalworkvidso").modal("show");
        }

        function workvideodelete(){
            
            var v_span = $("#v_span_video").val();
            var formdata = $("#deletevideojobpost").serialize();
            var actionurl = "{{url('seller/remove-work-video')}}";
            document.getElementById('load').style.visibility="visible";

            $.ajax({
                  type    : "POST",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#"+v_span).remove();
                        $("#deleteModalworkvidso").modal("hide");
                      }
                      document.getElementById('load').style.visibility='hidden';
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                      document.getElementById('load').style.visibility='hidden';
                  }
              });    
        }

     function otherskilldata(data){
        
        if(data!=""){
            
            var hiddenstr = '<input type="hidden" name="i_otherskill_id[]" id="hid_'+data+'" class="item" value="'+data+'">';
            var img_url = "{{url('public/Assets/frontend/images/pinkclose.png')}}";
            var selectedOtherskill = $("#otherskill option:selected").text();
            $("#otherskill option[value='"+data+"']").remove();
            $("#otherskill").removeAttr('required');
            var buttonstr ="<button type='button' class='btn directservice-btn' id='btn_"+ data +"' >"+ selectedOtherskill +" <img src='"+ img_url +"' onclick='removeotherskill(";
            buttonstr +='"'+data+'")';
            buttonstr +="'></button>";
            $("#hiddendiv").append(hiddenstr);
            $("#sel_other_skill").append(buttonstr);
        }
     }   

    function removeotherskill(data){

        
        var textdata = $("#mainskill option[value='"+data+"']").text();
        var stroption = "<option value='"+data+"'>"+textdata+"</option>";
        $("#otherskill").append(stroption);
        $('#btn_'+data).remove();
        $('#hid_'+data).remove();
        var nonblanks = $('.item[value!=""]').length;
        
        if(nonblanks == 0){
            $("#otherskill").prop('required',true);
        }

     }

    function getSelOtherSkill(i_category_id){
            
            var formdata = "i_category_id="+i_category_id;
            var actionurl = "{{url('seller-signup/get-skill')}}";

            $.ajax({
                  type    : "GET",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){

                        $("#otherskill").html(obj['optionstr']);

                        var other_ids = <?php echo json_encode(isset($sellerProfile->i_otherskill_id) ? $sellerProfile->i_otherskill_id : ''); ?>;
                        $.each(other_ids, function(key, value) {
                           
                               $("#otherskill option[value="+value+"]").remove();
                        });
                        
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                  }
              });    
    }

   
    </script>
<script type="text/javascript">
    $(document).ready( function() {
        $(".show_basic_pkg_btn").click(function(){
            $(".show_basic_pkg").slideToggle(500);
        });
        $(".show_standard_pkg_btn").click(function(){
            $(".show_standard_pkg").slideToggle(500);
        });
         $(".show_premium_pkg_btn").click(function(){
            $(".show_premium_pkg").slideToggle(500);
        });
     });

    // add and remove .active
    $('body').on('click', '.arrow_up_down', function () {

        var self = $(this);

        if (self.hasClass('active_arrow')) {
            $('.arrow_up_down').removeClass('active_arrow');
            return false;
        }

        $('.arrow_up_down').removeClass('active_arrow');

        self.toggleClass('active_arrow');
        hide = false;
    });
</script>

@stop

