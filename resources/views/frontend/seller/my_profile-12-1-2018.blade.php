@extends('layouts.frontend')

@section('content')

<style type="text/css">
    .modal-new .modal-backdrop {z-index: 0;}
    .btn-profile-link-contact{
        background: transparent;
            border: 2px solid #e70e8a;
            border-radius: 25px;
            color: #e70e8a;
            padding: 5px 35px;
            font-size: 18px;
            float: right;
     }
     .highresprofile{
        max-width: 42px;
     }
</style>    

    @php
        $standardplanFree= App\Helpers\General::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
    @endphp  

<!--modal-->
    <div id="sponsorThisProfileModal" class="modal1 upgradePlanModal_custom" role="dialog">
        <!-- Modal content -->
        <div class="modal-content2">
            <div class="final-containt">
                <span class="close2"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" alt=""  onclick="modalclose()"  /></span>
            </div>
            <div class="container">
                <div class="main-dirservice-popup">
                    
                    <form class="horizontal-form" role="form" method="POST" name="planform" id="planform" action="{{url('seller/sponsor-profile')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}

                    <div class="popup-dirservice">
                        <div class="title-dirservice">
                            <h4> Upgrade Your Plan </h4>
                        </div>
                        <div class="foll-option-popup">
                            You're currently a {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }} member. Please upgrade your Plan.
                        </div>
                        
                        <div class="wrap">
                            <fieldset>
                                <div class="toggle">
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join" checked onchange="plandurationchange('monthly')" >
                                    <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create" onchange="plandurationchange('yearly')">
                                    <label for="create" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                     @php
                        $pcnt=0;
                    @endphp
                    @if(count($plandata))
                        @foreach($plandata as $key=>$val)
                            
                            @if($userplandata['v_plan_id']=="5a65b757d3e8125e323c986a")
                                @php $pcnt=1; @endphp
                            @endif
                            
                            @if($key>$pcnt)
                            <div class="final-leval-popup">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="@if($key==0)left-choise @else left-side @endif">
                                            
                                            <div class="reg">
                                                <label style="font-weight: normal;">
                                                <bdo dir="ltr">
                                                    <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required>
                                                    <span class="select-seller-job"></span>
                                                    <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                </bdo>
                                                </label>
                                            </div>
                                            </label>
                                            <div class="choise-que">{{$val['v_subtitle']}}</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        
                                        <ul class="list-unstyled choise-avalible">
                                            <?php 
                                                    $lcnt=1;
                                                ?>
                                                @if(count($val['l_bullet']))
                                                    @foreach($val['l_bullet'] as $k=>$v)  
                                                        @if($k<3)
                                                        <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                        <?php $lcnt = $lcnt+1; ?>
                                                        @endif
                                                    @endforeach
                                                @endif  
                                        </ul>
                                    </div>
                                    
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="right-display">
                                            <ul class="list-unstyled choise-avalible">
                                                <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k>=3)
                                                            <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                            @endif
                                                            <?php $lcnt = $lcnt+1; ?>
                                                        @endforeach
                                                    @endif  

                                                </ul>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-12 same-height-1">
                                        <div class="@if($key==0) free-price @else final-price @endif">
                                            <div class="all-in-data">
                                                <div  class="monthyprice">
                                                       @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_monthly_price']}}</del>
                                                            </div>
                                                            Only £{{$val['f_monthly_dis_price']}} p/m

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif

                                                       @endif  
                                                </div>  
                                                
                                                <div class="yealryprice" style="display: none;">
                                                       @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>£{{$val['f_yealry_price']}}</del>
                                                            </div>
                                                            Only £{{$val['f_yealry_dis_price']}} p/a

                                                            @if($val['id']=="5a65b757d3e8125e323c986a")
                                                            @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                            <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                            @endif
                                                            @endif
                                                       @endif  
                                                </div>    

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    @endif        
                    <button type="submit" class="btn btn-payment">
                        Proceed
                    </button>
                    </form>
                </div>
                <!-- End 26B-my-profile-with-sponsor-button-POP UP-in-person-or-online -->

            </div>
        </div>
    </div>
<!--end-modal-->


    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                
                <div class="left-supportbtn">
                    <a href="{{url('seller/dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control panel</button>
                    </a>
                </div>
                <div class="title-support">
                    <h1>My<span id="servieprofile" style="margin-left: -1px">@if(isset($userdata->seller['v_service']) && $userdata->seller['v_service']=="inperson") In Person @else Online @endif</span> Profile</h1>
                </div>
            </div>
        </div>
    </div>
     @php
        $displaysponser=0;    
    @endphp
    @if(isset($sellerProfile->i_category_id) && $sellerProfile->i_category_id!="" && isset($sellerProfile->v_profile_title) && $sellerProfile->v_profile_title!="" )
        @php
            $displaysponser=1;    
        @endphp
    @endif
    <div class="container">

        <div class="edit-profile-link">
            <a href="{{url('seller/edit-my-profile')}}">
                <button class="btn btn-profile-link">Edit My<span id="servieprofile" style="margin-left: -1px">@if(isset($userdata->seller['v_service']) && $userdata->seller['v_service']=="inperson") In Person @else Online @endif</span> Profile</button>
            </a>
        </div>
        
        <div class="title-sponsoreads" @if(!$displaysponser) style="display: none;" @endif>
            Sponsored Profile Ad Stats
            <div class="clearfix"></div>
        </div>

        <div class="order-details" @if(!$displaysponser) style="display: none;" @endif >
        <div class="col-sm-3 no-padding">
            <div class="right-boderstyle">
                Total Sponsored Ad Sales
                 <p> £{{number_format($sponsoredSales)}}</p>
            </div>
        </div>
        <div class="col-sm-3 no-padding">
            <div class="right-boderstyle">
                Total Ad Impressions
                <p class="extra-pad">{{isset($sellerProfile->i_impression) ? number_format($sellerProfile->i_impression) : '0' }}</p>
            </div>
        </div>
        <div class="col-sm-3 no-padding">
            <div class="right-boderstyle">
                Total Ad Contacts
                <p class="extra-pad">{{isset($sellerProfile->i_contact) ? number_format($sellerProfile->i_contact) : '0' }}</p>
            </div>
        </div>
        <div class="col-sm-3 no-padding">
            <div class="right-boderstyle last-chlidhead">
                Total Sponsored Ad clicks
                <p class="extra-pad">{{isset($sellerProfile->i_clicks) ? number_format($sellerProfile->i_clicks) : '0' }}</p>
            </div>
        </div>
    </div>
    </div>
    
    <div style="clear: both"></div>
    <br>
    <div class="container">
        <div class="sponsor-add-new">
            <div class="row">

                <div class="alert alert-info alert-danger" id="commonerrmsg" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span id="commonerrtxt">Something went wrong.</span>
                </div>

                <div class="alert alert-success alert-big alert-dismissable br-5" id="commonsuccmsg" style="display: none;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span id="commonsucctxt"></span>
                </div>

                 

                <div id="commonmsg"></div>

                <div class="col-sm-2">
                    <div class="online-img">
                        
                    @php
                        $imgdata = \App\Helpers\General::userroundimagemyprofile($userdata);
                        echo $imgdata;
                    @endphp

                        @if(isset($leveldata) && count($leveldata))
                         @php
                            $levelcon="";
                            if(isset($leveldata->v_icon) && $leveldata->v_icon!=""){
                                $levelcon = \Storage::cloud()->url($leveldata->v_icon);
                            }
                         @endphp
                         @if($levelcon!="")
                         <div class="starall">
                            <img src="{{$levelcon}}" alt="" />
                         </div>
                         @endif
                         @endif
                    </div>
                </div>


                <div class="col-sm-5 left-padding1">
                    <div class="name-the-tab">
                        {{isset($userdata->v_fname) ? $userdata->v_fname : ''}} {{isset($userdata->v_lname) ? $userdata->v_lname : ''}}
                        <p>{{isset($sellerProfile->l_short_description) ? $sellerProfile->l_short_description : ''}}</p>
                    </div>
                    
                    <div class="name-the-tab-link">
                        <a href="#">{{isset($sellerProfile->v_website) ? $sellerProfile->v_website : ''}}</a>
                        <p>{{isset($sellerProfile->v_contact_phone) ? $sellerProfile->v_contact_phone : ''}}</p> 
                    </div>

                </div>
               

                <div class="col-sm-5">
                    <div class="sponsor-add-first">
                        
                        <div id="sponsorprofileiddiv" @if(!$displaysponser) style="display: none;" @endif>
                        @if(isset($sellerProfile->e_sponsor))
                            @if(isset($sellerProfile->e_sponsor_status) && $sellerProfile->e_sponsor_status=="start")
                            <button class="btn btn-sponsor-add-first" id="myBtn" onclick="updateSponsoredStatus('pause')">Pause Sponsored Ad</button>
                            <button class="btn btn-sponsor-add-green">Sponsored</button>
                            @else
                            <button class="btn btn-sponsor-add-first" id="myBtn" @if($userplandata['v_plan_id']=="5a65b48cd3e812a4253c9869") onclick="sponsorThisProfile()" @else  onclick="updateSponsoredStatus('start')" @endif>Start Sponsored Ad</button>
                            @endif
                        @else
                            <button class="btn btn-sponsor-add-first" id="myBtn" @if($userplandata['v_plan_id']=="5a65b48cd3e812a4253c9869") onclick="sponsorThisProfile()" @else  onclick="updateSponsoredStatus('start')" @endif>Sponsor This Profile</button>       
                        @endif
                        </div>

                        {{-- @if(isset($userdata->v_plan) && count($userdata->v_plan))
                            @if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']!="5a65b48cd3e812a4253c9869")
                                <div id="sponsorprofileiddiv">
                                @if(isset($userdata->e_sponsor_profile_status) && $userdata->e_sponsor_profile_status=="start")
                                    <button class="btn btn-sponsor-add-first" id="myBtn" onclick="updateSponsoredStatus('pause')">Pause Sponsored Ad</button>
                                    <button class="btn btn-sponsor-add-green">Sponsored</button>
                                @else
                                    <button class="btn btn-sponsor-add-first" id="myBtn" onclick="updateSponsoredStatus('start')">Start Sponsored Ad</button>
                                @endif
                                </div>
                            @else
                                <button class="btn btn-sponsor-add-first" id="myBtn" onclick="sponsorThisProfile()">Sponsor This Profile</button>    
                            @endif    
                        @else  
                            <button class="btn btn-sponsor-add-first" id="myBtn" onclick="sponsorThisProfile()">Sponsor This Profile</button>
                        @endif --}}

                    </div>

                   
                    <div class="tree-line-link">
                        <div class="row">

                            <div class="col-sm-4 no-padding">
                                <div class="verifey-profile">
                                    <img class="highresprofile" src="{{url('public/Assets/frontend/images/clock-add.png')}}" alt="" />
                                    <p>Replies in {{$userdata->v_replies_time}}</p>
                                </div>
                            </div>
                            <div class="col-sm-4 no-padding">
                                @if(isset($userdata->e_email_confirm) && $userdata->e_email_confirm=="yes")
                                <div class="verifey-profile">
                                    <img class="highresprofile" src="{{url('public/Assets/frontend/images/right-lot.png')}}" alt="" />
                                    <p>Profile verified</p>
                                </div>
                                @else
                                <div class="verifey-profile">
                                    <img class="highresprofile" src="{{url('public/Assets/frontend/images/close-lot.png')}}" alt="" />
                                    <p>Profile not verified</p>
                                </div>
                                @endif
                            </div>
                            <div class="col-sm-4 no-padding">
                                <div class="verifey-profile">
                                    <img class="highresprofile" src="{{url('public/Assets/frontend/images/classification.png')}}" alt="" />
                                    @php
                                      $start =strtotime($userdata->d_added);
                                      $end =  time();
                                      $diff=$end - $start;
                                      $days_between = round($diff / 86400);
                                      if($days_between>30){
                                          $days_between = round($days_between / 30);
                                          $days_between = $days_between." Months";
                                      }else{
                                        if($days_between>1){
                                            $days_between = $days_between." days";    
                                        }else{
                                            $days_between = "1 day";
                                        }
                                        
                                      }
                                     @endphp
                                    <p>With Skillbox ({{$days_between}})</p>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="linking-time">
                        <div class="row">
                            <div class="col-sm-6">
                                {{--
                                <div class="time-all">
                                    <p>Open<span> Mon - Fri 8am - 5pm</span></p>
                                </div>
                                 --}}   

                                <div class="time-all">
                                    <p>English Proficiency<span> {{isset($sellerProfile->v_english_proficiency) ? strtoupper($sellerProfile->v_english_proficiency) : ''}}</span></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="edit-profile-me">
                                    @if(isset($sellerProfile->v_profile_title) && $sellerProfile->v_profile_title!="")
                                    <a href="{{url('online')}}/{{$sellerProfile->id}}/{{$sellerProfile->v_profile_title}}?preview=profile" target="_blank">
                                    <button class="btn btn-profile-link-contact">View my profile</button>
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('js')
<script type="text/javascript">
    
    function sponsorThisProfile(){
        $("#sponsorThisProfileModal").modal("show");
    }

    function modalclose(){
       $("#sponsorThisProfileModal").modal("hide"); 
    }

    
    function plandurationchange(data=""){
        
        if(data=="monthly"){
            $(".monthyprice").css("display","block");
            $(".yealryprice").css("display","none");

        }else if(data=="yearly"){
            $(".yealryprice").css("display","block");
            $(".monthyprice").css("display","none");
        }
    }


    function updateSponsoredStatus(data){

      var formdata = "e_sponsor_profile_status="+data;
      var actionurl = "{{url('seller/update-sponsor-profile')}}";

      $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                
                var obj = jQuery.parseJSON(res);
                if(obj['status']==1){
                    $("#commonmsg").html(obj['msg']);
                    $("#sponsorprofileiddiv").html(obj['btnstr']);
                }else{
                    $("#commonmsg").html(obj['msg']);
                }
            },

            error: function ( jqXHR, exception ) {
                $("#commonerrmsg").css("display","block");  
            }
      });  

    }

</script>
@stop

