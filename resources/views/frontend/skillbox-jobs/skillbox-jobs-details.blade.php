@extends('layouts.frontend')


@section('content')
    <!--slide-containt-->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">
                
                 @php
                    $v_banner_image="";
                    if($metaDetails['v_image'] && $metaDetails['v_image']!=""){
                        if(Storage::disk('s3')->exists($metaDetails['v_image'])){
                            $v_banner_image = \Storage::cloud()->url($metaDetails['v_image']);      
                        }                
                    }else{
                    
                    }
                    $v_banner_image = url('public/Assets/frontend/images/skills-jobs.png');      
               
                @endphp
                <img src="{{$v_banner_image}}" class="img-responsive" alt="" />

                <div class="center-containt">
                    <div class="container">
                        <div class="applybox-notification blog-page">
                            
                            @if($metaDetails['v_title'] && $metaDetails['v_title']!="")
                            <h1>{{$metaDetails['v_title']}}</h1>
                            @else
                            <h1> SKILLBOX JOBS </h1>
                            @endif

                            @if($metaDetails['v_sub_headline'] && $metaDetails['v_sub_headline']!="")
                            <p style="margin-top: 25px;">{{$metaDetails['v_sub_headline']}}</p>
                            @else
                            <p style="margin-top: 25px;">Follow our blog and be the first in touch with our latest articles!</p>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end slide-containt-->

    <!--28B-skillbox-jobs-position-details-2 -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-position-detail">
                    <div class="partner-manager position-detail">
                        <div class="row">
                            <div class="col-xs-9">
                                <div class="manager-name">
                                    <h3>  {{$data->v_title or ''}} </h3>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <a href="{{url('join-our-team/apply-job', $data->id)}}" class="btn btn-apply-now"> Apply Now </a>
                            </div>
                        </div>
                        <div class="manager-address">
                            <p> {{$data->v_country or ''}}, {{$data->v_city or ''}}</p>
                            <p> {{$data->v_short_description or ''}} </p>
                            <p> {{$data->v_job_time or ''}} </p>
                        </div>
                    </div>
                    {!!$data->l_description!!}
                </div>
            </div>
        </div>
    </div>
    <!--end-28B-skillbox-jobs-position-details-2 -->
@stop
@section('js')

@stop

