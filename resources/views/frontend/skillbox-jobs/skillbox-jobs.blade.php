@extends('layouts.frontend')


@section('content')
      <!--slide-containt-->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">    
                @php
                    $v_banner_image="";
                    if($metaDetails['v_image'] && $metaDetails['v_image']!=""){
                        if(Storage::disk('s3')->exists($metaDetails['v_image'])){
                            $v_banner_image = \Storage::cloud()->url($metaDetails['v_image']);      
                        }                
                    }else{
                    
                    }
                    $v_banner_image = url('public/Assets/frontend/images/skills-jobs.png');      
               
                @endphp
                <img src="{{$v_banner_image}}" class="img-responsive" alt="" />
                <div class="center-containt">
                    <div class="container">
                        <div class="skill-box blog-page">
                            @if($metaDetails['v_title'] && $metaDetails['v_title']!="")
                            <h1>{{$metaDetails['v_title']}}</h1>
                            @else
                            <h1> SKILLBOX JOBS </h1>
                            @endif

                            @if($metaDetails['v_sub_headline'] && $metaDetails['v_sub_headline']!="")
                            <p style="margin-top: 25px;">{{$metaDetails['v_sub_headline']}}</p>
                            @else
                            <p style="margin-top: 25px;">Follow our blog and be the first in touch with our latest articles!</p>
                            @endif
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end slide-containt-->

    <!--28B-skillbox-jobs-3-->
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-skillbox">
                    <div class="join-team-heading">
                        <h2> Join Our Team</h2>
                        <p class="para-skill-jobs">
                            If you're passionate and ready to dive in, we'd love to meet you.
                        </p>
                    </div>
                    <div class="join-team">
                        @if(isset($finalresult) && count($finalresult))
                            @foreach($finalresult as $key => $val)
                                <div class="team-name">
                                    <h3> 
                                        @if(isset($categorylist[$key]))
                                        {{$categorylist[$key]}}
                                        @endif
                                    </h3>
                                        @if(count($val))
                                           @foreach($val as $k=>$v) 
                                                <div class="team-type">
                                                    <a href="{{url('join-our-team', $v->id)}}">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <div class="team-type-name text-left" style="font-size: 16px">
                                                                  {{isset($v->v_title) ? $v->v_title : ''}} 
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <div class="team-type-location text-right">
                                                                    {{isset($v->v_country) ? $v->v_country : ''}}, {{isset($v->v_city) ? $v->v_city : ''}} 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endforeach    
                                        @else
                                            <div class="team-type">
                                                    
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="team-type-name text-center">
                                                                  There're currently no vacancies available. Please check back for any future openings.
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </a>
                                                </div>

                                            
                                        @endif
                                </div>
                            @endforeach
                        @else
                        <div class="team-type">
                                                    
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="team-type-name text-center" >
                                          There're currently no vacancies available. Please check back for any future openings.
                                        </div>
                                    </div>
                                    
                                </div>
                            </a>
                        </div>    

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-28B-skillbox-jobs-3-->
@stop
@section('js')

@stop

