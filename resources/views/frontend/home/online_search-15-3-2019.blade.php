@extends('layouts.frontend')
@section('content')

<style type="text/css">
    .clearboth:nth-child(3n+1){
        clear: both;
    }
    @media screen and (max-width: 767px){
        .clearboth:nth-child(2n+1){clear: both;}
    }
</style>

@php

    $experience['entry'] = array(
        'cnt'=>0,
        'name'=>"Entry",
    );

    $experience['intermediate'] = array(
        'cnt'=>0,
        'name'=>"Intermediate",
    );
    $experience['expert'] = array(
        'cnt'=>0,
        'name'=>"Expert",
    );

    $ispricefilter=0;
    $totalcntskill=0;
    $totalcntjob=0;
    $maxprice=0;

    if(isset($skillsidebar) && count($skillsidebar)){
        $totalcntskill=count($skillsidebar);    

        foreach($skillsidebar as $k=>$v){
            if(isset($v['i_price']) && $v['i_price']>50){
                $ispricefilter = 1;
            }
        }

    }

    if(isset($jobsidebar) && count($jobsidebar)){
        $totalcntjob=count($jobsidebar);

        foreach($jobsidebar as $k=>$v){
            if(isset($v['v_budget_amt']) && $v['v_budget_amt']>50){
                $ispricefilter = 1;
            }
        }
        
    }


    if(isset($skillsidebar1) && count($skillsidebar1)){
        foreach($skillsidebar1 as $k=>$v){
            if(isset($experience[$v['v_experience_level']])){
                $experience[$v['v_experience_level']]['cnt'] = $experience[$v['v_experience_level']]['cnt']+1;
            }
            if(isset($v['i_price']) && $v['i_price']>50){
                $ispricefilter = 1;
            }
            if($maxprice<$v['i_price']){
                $maxprice = $v['i_price'];    
            }
            
        }

    }

    if(isset($jobsidebar1) && count($jobsidebar1)){
        

        foreach($jobsidebar1 as $k=>$v){
            if(isset($experience[$v['v_skill_level_looking']])){
                $experience[$v['v_skill_level_looking']]['cnt'] = $experience[$v['v_skill_level_looking']]['cnt']+1;
            }
            if(isset($v['v_budget_amt']) && $v['v_budget_amt']>50){
                $ispricefilter = 1;
            }
            if($maxprice<$v['v_budget_amt']){
                $maxprice = $v['v_budget_amt'];    
            }
        }
    }

    //dump($ispricefilter);
   
@endphp

<div class="modal fade" id="Notifyme" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 600px">
            <div class="modal-content modal-content-1">
              <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title">Notify Me</h4>
                    <p style="margin-top: 10px;width: 75%"><b>Get notified, as soon as your desired skill is active in this category.</b></p>
                    <p style="margin-top: 10px;width: 75%">If you don't want to receive notifications in future, please click the unsubscribe link in the notification email you will receive from us.</p>
                </div>
                <form role="form" method="POST" name="notifyform" id="notifyform" data-parsley-validate enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body" style="padding:0">
                    <div class="modal-body-1" style="padding: 0px 30px !important;" id="contactsellerdetail">
                        

                        <div class="sign-main" style="margin-top: -23px;margin-bottom: 40px">
                            <div class="sign-in">
                                <p class="text-center" style="font-size: 20px;margin-bottom: 20px;"><b>Your Details</b></p>
                                <div class="mail-pass">
                                    <input type="hidden" name="i_category_id" @if(isset($data['i_category_id']) && $data['i_category_id']!="") value="{{$data['i_category_id']}}" @endif >     
                                    <input type="email" name="v_email" id="v_email" class="form-control form-mail" placeholder="Email Address" required>
                                    <select class="form-control form-sel" name="i_skill_id" required id="i_skill_id">
                                            <option value="">Select your desired skill</option>
                                            @if(isset($skills) && count($skills))
                                                @foreach($skills as $k=>$v)
                                                    <option value="{{$v->id}}" @if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']==$v->id) selected @endif>{{$v->v_name}}</option>
                                                @endforeach
                                            @endif
                                    </select>
                                    <label class="checkbox wc-checkbox-space" style="margin-top: 13px;">
                                        <input type="checkbox" class="singup-checkup" name="e_notify" required>
                                        <span class="notice-jobpostion" style="font-size:14px;"> 
                                          Yes, I want to receive notifications from skillbox.
                                        </span>
                                    </label>

                                </div>
                                <div class="signin-bottom" style="margin-top: -17px;">
                                    <button type="button" onclick="submitNotify()" class="btn btn-block btn-signin">Notify me</button>
                                </div>

                                <p class="join-us" style="margin-top: -17px;">By clicking Notify me, I agree to the <span class="join"><a target="_blank" href="{{url('pages/terms-of-service')}}">Terms of service </a></span> and <span class="join"><a target="_blank" href="{{url('pages/privacy-policy')}}">Privacy policy </a></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
</div>



<div id="applymsg" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;">Message</h4>
      </div>
      <div class="modal-body">
        <p style="text-align: center;">To fill this post change your desired category by editing your seller profile.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div id="sendMessageSuccess" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;">Message</h4>
      </div>
      <div class="modal-body">
        <p id="popupmessage" style="text-align: center;"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

   <!--slide-containt-->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">
                
                @php
                    $v_banner_image="";
                    if($metaDetails['v_image'] && $metaDetails['v_image']!=""){
                        if(Storage::disk('s3')->exists($metaDetails['v_image'])){
                            $v_banner_image = $_S3_PATH.'/'.$metaDetails['v_image'];
                            //$v_banner_image = \Storage::cloud()->url($metaDetails['v_image']);      
                        }else{
                            $v_banner_image = url('public/Assets/frontend/images/filtaring.png');
                        }                
                    }else{
                        $v_banner_image = url('public/Assets/frontend/images/filtaring.png');
                    }
                @endphp

                <img src="{{$v_banner_image}}" class="img-responsive" alt="" />
                <div class="center-containt">
                    <div class="container">
                        <div class="filtaring-page">
                            @if($metaDetails['v_title'] && $metaDetails['v_title']!="")
                            <h1>{{$metaDetails['v_title']}}</h1>
                            @else
                            <h1>Search</h1>
                            @endif
                            @if($metaDetails['v_sub_headline'] && $metaDetails['v_sub_headline']!="")
                            <p style="margin-top: 25px;">{{$metaDetails['v_sub_headline']}}</p>
                            @else
                            <p style="margin-top: 25px;">You’ve got the idea, now make it official with the perfect garden designs</p>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--end-slide-containt-->

    <!-- 04B-filtering-results-online -->
    <div class="container">
        <div class="sort-for">
            
            <div class="sort-forep">
                  <a href="javascript:;" onclick="viewchange('grid')">
                    <img id="gridicon" src="{{url('public/Assets/frontend/images/fore.png')}}" class="fore-all" alt="" />
                </a>
                <a href="javascript:;" onclick="viewchange('list')">
                    <img id="listicon" src="{{url('public/Assets/frontend/images/fore2.png')}}" alt="" />
                </a>
            </div>
        </div>
        
        <div class="filtaring-main">
            <div class="row">
               <style type="text/css">
                   .radiolbl {
                        display: inline-block;
                        position: relative;
                        padding-left: 5px;
                        font-size: 18px;
                        margin-top: 2px;
                        color: #7c7b7b;
                    }
               </style>
                
                <div class="col-sm-3">
                    
                    <form method="GET" name="searchcoursesside" id="searchcoursesside" action="{{url('search')}}" >
                   
                    <div class="all-type">
                        
                        <div class="name-of-type">
                            <span>Type</span>
                        </div>
                        <br/>
                        <div class="reg">
                             <bdo dir="ltr">
                                <input type="radio" id="v_type_skill" onclick="masterfilterMain('v_type_skill')" class="one" value="on" name="v_type_skill" @if(isset($data['v_type_skill']) && $data['v_type_skill']=='on') checked @endif>
                                <span style="border:1px solid #7c7b7b;"></span>
                                <abbr class="abberall radiolbl">Skills ({{$totalcntskill}})</abbr>
                             </bdo>
                        </div>
                        <div style="clear: both"></div>
                        <div class="reg">
                             <bdo dir="ltr">
                                <input type="radio" id="v_type_job" onclick="masterfilterMain('v_type_job')" class="one" value="on" name="v_type_job" @if(isset($data['v_type_job']) && $data['v_type_job']=='on') checked @endif>
                                <span style="border:1px solid #7c7b7b;"></span>
                                <abbr class="abberall radiolbl" >Jobs ({{$totalcntjob}})</abbr>
                             </bdo>
                        </div>


                        {{-- <div class="checkbox checkbox-warning">
                            <input id="checkbox" type="checkbox"  name="v_type_skill" onchange="masterfilterMain()" @if(isset($data['v_type_skill']) && $data['v_type_skill']=='on') checked @endif >
                            <label for="checkbox">
                                Skills (<span>{{$totalcntskill}}</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox1" type="checkbox" name="v_type_job" onchange="masterfilterMain()" @if(isset($data['v_type_job']) && $data['v_type_job']=='on') checked @endif>
                            <label for="checkbox1">
                                Jobs (<span>{{$totalcntjob}}</span>)
                            </label>
                        </div> --}}

                        {{-- @if(isset($data['v_type_job']) && $data['v_type_job']!="")
                        <a href="javascript:;" onclick="cleardata('v_type_job')">Clear Filter</a>
                        @endif --}}
                    </div>

                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Experience</span>
                        </div>



                        @if(isset($experience) && count($experience))    
                            @foreach($experience as $k=>$v)
                                <div class="checkbox checkbox-warning">
                                    <input id="checkbox{{$k}}" value="{{$k}}" type="checkbox" class="checkboxdata" name="v_skill_level_looking[]" onchange="masterfilter()" @if(isset($data['v_skill_level_looking']) && in_array($k, $data['v_skill_level_looking'])) checked @endif  >
                                    <label for="checkbox{{$k}}">
                                        {{$v['name']}} (<span>{{$v['cnt']}}</span>)
                                    </label>
                                </div>
                            @endforeach
                        @endif

                        @if(isset($data['v_skill_level_looking']) && $data['v_skill_level_looking']!="")
                        <a href="javascript:;" onclick="cleardata('v_skill_level_looking')">Clear Filter</a>
                        @endif

                    </div>

                    
                    <div class="all-type" @if($ispricefilter) @else style="display: none;" @endif id="pricefilterdiv">
                        <div class="name-of-type">
                            <span>Price</span>
                        </div>

                        @if(isset($pricefilter) && count($pricefilter))    
                            @foreach($pricefilter as $k=>$v)
                                <div class="checkbox checkbox-warning">
                                    <input id="checkbox{{$v->id}}" value="{{$v->id}}" class="checkboxdata" type="checkbox" name="i_price_id[]" onchange="masterfilter()" @if(isset($data['i_price_id']) && in_array($v->id, $data['i_price_id'])) checked @endif >
                                    <label for="checkbox{{$v->id}}">
                                        <span>{{$v->v_title}}</span>
                                    </label>
                                </div>
                            @endforeach
                        @endif

                    </div>
                   
                    @if(isset($v_service) && $v_service=="inperson")
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Radius</span>
                        </div>
                        @if(isset($milesfilter) && count($milesfilter))    
                            @foreach($milesfilter as $k=>$v)
                                
                                <div style="clear: both"></div>
                                <div class="reg">
                                     <bdo dir="ltr">
                                        <input type="radio" onclick="masterfilterMain('v_type_skill')" class="one" value="{{$v}}" name="i_radius" @if(isset($data['i_radius']) && $data['i_radius']==$v) checked @endif>
                                        <span style="border:1px solid #7c7b7b;"></span>
                                        <abbr class="abberall radiolbl">{{$v}} Miles</abbr>
                                     </bdo>
                                </div>

                                {{-- <div class="checkbox checkbox-warning">
                                    <input id="checkbox{{$v}}" value="{{$v}}" type="checkbox" class="checkboxdata" name="i_radius" onchange="masterfilter()" @if(isset($data['i_radius']) && $data['i_radius']==$v) checked @endif >
                                    <label for="checkbox{{$v}}">
                                        <span>{{$v}} Miles</span>
                                    </label>
                                </div> --}}
                            @endforeach
                        @endif
                    </div>
                    @endif
                    </form>

                </div>

                <div class="col-sm-9 no-padding">
                    <div class="select-box">
                        
                        <form class="horizontal-form" role="form" method="GET" name="searchcoursesmaster" id="searchcoursesmaster" action="{{url('search')}}" >
                            <input type="hidden" name="e_type" value="{{$v_service}}">
                            <input type="hidden" name="keyword" value="{{$keyword}}">

                            @if($v_service=="inperson")

                                <input type="hidden" name="v_latitude" value="{{isset($data['v_latitude']) ? $data['v_latitude'] : ''}}">
                                <input type="hidden" name="v_longitude" value="{{isset($data['v_longitude']) ? $data['v_longitude'] : ''}}">

                            @endif
                        <div class="col-sm-4">
                            <select class="form-control form-sel" name="i_category_id" onchange="masterfiltertop()">
                                <option value="">Select Category</option>
                                @if(isset($categories) && count($categories))
                                    @foreach($categories as $k=>$v)
                                        <option value="{{$v->id}}" @if(isset($data['i_category_id']) && $data['i_category_id']==$v->id) selected @endif>{{$v->v_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="col-sm-4">
                            <select class="form-control form-sel" name="i_mainskill_id" onchange="masterfiltertop()">
                                <option value="">Select Skill</option>
                                @if(isset($skills) && count($skills))
                                    @foreach($skills as $k=>$v)
                                        <option value="{{$v->id}}" @if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']==$v->id) selected @endif>{{$v->v_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control form-sel" name="relevance" onchange="masterfiltertop()">
                                <option value="">Sort by Relevance</option>
                                <option value="newest_first" @if(isset($data['relevance']) && $data['relevance']=='newest_first') selected @endif >Newest First</option>
                                <option value="oldest_first" @if(isset($data['relevance']) && $data['relevance']=='oldest_first') selected @endif>Oldest First</option>
                                <option value="toprated" @if(isset($data['relevance']) && $data['relevance']=='toprated') selected @endif>Top Rated</option>
                                <option value="sponsored" @if(isset($data['relevance']) && $data['relevance']=='sponsored') selected @endif>Sponsored</option>
                            </select>
                        </div>
                        </form>
                    </div>
                    <div style="clear: both"></div><br></br>
                    <div id="commonmsg"></div>

                    @php 
                        $skillidsarray=array();  
                        $jobidsarray=array();  
                    @endphp

                    <div id="gridview" class="gridview_courses" >
                        <div class="box-content-find" id="box-content-grid">
                            @if(isset($finalresult) && count($finalresult))
                                @foreach($finalresult as $k=>$v)
                                    @if($v['type'] == "jobs")
                                        @php
                                            $jobidsarray[]=$v['data']['_id'];  
                                            $title =App\Helpers\General::TitleSlug($v['data']['v_job_title']);
                                        @endphp

                                        <div class="col-sm-6  col-md-4">
                                            <div class="all-category @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start') gridviewbordersponseredjob  @endif">
                                                <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                <div class="category-img">
                                                    @php
                                                        $imgins="";
                                                        if(isset($v['data']['v_photos'][0])){
                                                            $imgins = $v['data']['v_photos'][0];
                                                        } 
                                                        $imgdata="";
                                                        if($imgins!=""){
                                                            $imgdata = $_S3_PATH.'/'.$imgins;    
                                                        }else{
                                                            $imgdata = $_S3_PATH.'/jobpost/photos/No_Image_Available.jpg';    
                                                        }
                                                    @endphp
                                                    <img src="{{$imgdata}}" alt="" />
                                                </div>
                                                </a>    
                                                
                                                @if(isset($v['data']['e_urgent']) && $v['data']['e_urgent']=="yes")    
                                                    <div class="urgent1">
                                                        <label>URGENT</label>
                                                    </div>
                                                @endif

                                                <div class="category-text">
                                                    <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                    <h2 style="min-height: 37px;max-height: 37px;overflow: hidden;">{{isset($v['data']['v_job_title']) ? $v['data']['v_job_title'] : ''}}</h2> 
                                                    </a>
                                                    <div class="gridview_txt">
                                                    <p style="min-height: 60px;max-height: 60px;overflow: hidden;margin: 10px 0px;">
                                                    @if(isset($v['data']['l_short_description']))
                                                        @if(strlen($v['data']['l_short_description'])>70)
                                                            @php
                                                                $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                                                $ldesc = str_replace("</strong>", " ", $ldesc);
                                                                echo substr($ldesc,0,70).'...';
                                                            @endphp
                                                        @else
                                                            @php
                                                                $ldesc = str_replace("<strong>", "", $v['data']['l_short_description']);
                                                                $ldesc = str_replace("</strong>", " ", $ldesc);
                                                                echo $ldesc;
                                                            @endphp
                                                        @endif
                                                   @endif
                                                    </p>
                                                    </div>
                                                    <div class="star-category">
                                                        @if($v['data']['i_total_review']>0)
                                                    	<img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                                                        @else
                                                        <img src="{{url('public/Assets/frontend/images/star-gray.png')}}" alt="" />
                                                        @endif
                                                        <p class="text-star">
                                                        	{{isset($v['data']['i_total_avg_review']) ? number_format($v['data']['i_total_avg_review'],1) : ''}}
                                                            <span>({{isset($v['data']['i_total_review']) ? $v['data']['i_total_review'] : ''}})</span>
                                                        </p>
                                                    </div>
                                                </div>

                                                @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start')
                                                    <div class="sponsored-course">
                                                        <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}" class="btn btn-course-job">Sponsored AD</a>
                                                    </div>
                                                @endif
                                                <span class="text-right defineresult">Job</span>
                                                <div class="starting-price starting-price-job">
                                                    <div class="pull-left">
                                                        @if(in_array($v['data']['_id'], $s_job))
                                                            <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                            <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_{{$v['data']['_id']}}" /></a>
                                                            <input type="hidden" name="img_click_{{$v['data']['_id']}}" id="img_click_{{$v['data']['_id']}}" value="1">
                                                        @else
                                                            <a href="javascript:;" title="shortlist" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                            <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_{{$v['data']['_id']}}" /></a>
                                                            <input type="hidden" name="img_click_{{$v['data']['_id']}}" id="img_click_{{$v['data']['_id']}}" value="0">
                                                        @endif
                                                    </div>
                                                    <div class="pull-right">
                                                        @if(isset($v['data']['v_budget_type']) && $v['data']['v_budget_type']=='open_to_offers')
                                                        <p>Open to Offers</p>
                                                        @else
                                                        <P>Budget Amount <span>£{{isset($v['data']['v_budget_amt']) ? $v['data']['v_budget_amt'] :'0'}}</span></P>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @else

                                        @php
                                            $skillidsarray[]=$v['data']['_id'];  

                                            $title="";
                                            if(isset($v['data']['v_profile_title'])){
                                                $title =App\Helpers\General::TitleSlug($v['data']['v_profile_title']);
                                            }
                                            
                                        @endphp

                                        <div class="col-sm-6 col-md-4">
                                            <div class="all-category @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start') gridviewbordersponsered  @endif">
                                                
                                                <div class="category-img category_video" >
                                                @if(isset($v['data']['v_introducy_video']) && $v['data']['v_introducy_video']!="")
                                                    @php
                                                       $videodata=$_S3_PATH.'/'.$v['data']['v_introducy_video'];
                                                    @endphp
                                                    <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                    <video class="playcontroler" id="videoPlayergrid{{$k}}" width="100%" height>
                                                        <source src="{{$videodata}}" type="video/mp4">
                                                    </video>
                                                    </a>
                                                    <img src="{{url('public/Assets/frontend/images/player.png')}}" class="imgcontroler" id="videoControlergrid{{$k}}"  onclick="playPausevideogrid('{{$k}}')"  alt="" style="width: auto;height: auto;position: absolute;left: 0;right: 0;margin: auto;top: 3%;margin-right: 10px" /> 
                                                @elseif(isset($v['data']['v_work_photos']) && count($v['data']['v_work_photos']))
                                                    @php
                                                        $imag=$_S3_PATH.'/'.$v['data']['v_work_photos'][0];
                                                        //$imag=\Storage::cloud()->url($v['data']['v_work_photos'][0]);
                                                    @endphp
                                                    <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                    <img src="{{$imag}}" alt="" />
                                                    </a>
                                                @else
                                                    @php 
                                                    $imgdata=$_S3_PATH.'/jobpost/photos/No_Image_Available.jpg';
                                                    //$imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg'); 
                                                    @endphp
                                                    <img src="{{$imgdata}}" alt="" />
                                                @endif
                                               
                                                </div>

                                                <div class="category-text">
                                                    <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                    <h2 style="min-height: 37px;max-height: 37px;overflow: hidden;">{{isset($v['data']['v_profile_title']) ? $v['data']['v_profile_title'] : ''}}</h2>
                                                    </a>
                                                    <p style="min-height: 60px;max-height: 60px;overflow: hidden;margin: 10px 0px;">
                                                         @if(isset($v['data']['l_short_description']))
                                                            @if(strlen($v['data']['l_short_description'])>70)
                                                                @php
                                                                    $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                                                    $ldesc = str_replace("</strong>", " ", $ldesc);
                                                                    echo substr($ldesc,0,70).'...';
                                                                @endphp
                                                            @else
                                                                @php
                                                                    $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                                                    $ldesc = str_replace("</strong>", " ", $ldesc);
                                                                    echo $ldesc;
                                                                @endphp
                                                            @endif
                                                       @endif

                                                        
                                                    </p>
                                                    <div class="star-category">
                                                        {{-- <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" /> --}}
                                                        @if($v['data']['i_total_review']>0)
                                                        <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                                                        @else
                                                        <img src="{{url('public/Assets/frontend/images/star-gray.png')}}" alt="" />
                                                        @endif

                                                        <p class="text-star">{{number_format($v['data']['i_total_avg_review'],1)}} <span>({{$v['data']['i_total_review']}})</span></p>
                                                    </div>
                                                </div>

                                                @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status']=="start")
                                                <div class="sponsored-course">
                                                    <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}" class="btn btn-course">Sponsored AD</a>
                                                </div>
                                                @endif
                                                <span class="text-right defineresult">Skill</span>
                                                <div class="starting-price">
                                                    <div class="pull-left">
                                                        @if(in_array($v['data']['_id'], $s_skill))
                                                            <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                            <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_{{$v['data']['_id']}}" /></a>
                                                            <input type="hidden" name="img_click_{{$v['data']['_id']}}" id="img_click_{{$v['data']['_id']}}" value="1">
                                                        @else
                                                            <a href="javascript:;" title="shortlist" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                            <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_{{$v['data']['_id']}}" /></a>
                                                            <input type="hidden" name="img_click_{{$v['data']['_id']}}" id="img_click_{{$v['data']['_id']}}" value="0">
                                                        @endif
                                                    </div>
                                                    <div class="pull-right">
                                                        <P>STARTING AT <span>
                                                            @if(isset($v['data']['information']['basic_package']) && count($v['data']['information']['basic_package']))
                                                                  £{{$v['data']['information']['basic_package']['v_price']}}
                                                            @endif 
                                                        </span></P>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @else
                                @if(isset($data['i_category_id']) && $data['i_category_id']!="")
                                    <div class="no-skill-added">
                                        <div class="category-list">
                                            <div class="bottom-border">
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12 noskill-heading" >
                                                        <div class="noskill-title " style="padding-left: 20px;">
                                                        <h4>
                                                            @if(isset($categories) && count($categories))
                                                                @foreach($categories as $k=>$v)
                                                                    @if(isset($data['i_category_id']) && $data['i_category_id']==$v->id)
                                                                        {{$v->v_name}}
                                                                    @endif
                                                                @endforeach
                                                            @endif    
                                                        </h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-xs-12">
                                                        <div class="noskill-body noskill-heading ">
                                                            <div class="row">
                                                                <div class="col-sm-12 col-xs-12 text-center">
                                                                    <img src="{{url('public/Assets/frontend')}}/images/no-skilluser.png" class="img-responsive no-skilluser" alt="" />
                                                                    <div class="noskill-title" style="padding: 10px 0 10px;">
                                                                        <p style="margin: 5px 0px 14px;">We are curently looking for the skills in this category.</p>
                                                                        
                                                                        @if(auth()->guard('web')->check())
                                                                        <button type="button" class="btn btn-applynow" data-toggle="modal" data-target="#applymsg" > APPLY NOW</button>    
                                                                        @else
                                                                        <a href="{{url('account/seller')}}">
                                                                        <button type="button" class="btn btn-applynow"> APPLY NOW</button>
                                                                        </a>
                                                                        @endif
                                                                    
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="notify-msg" style="margin-bottom: 75px;padding: 0px 18px;">
                                            <div class="col-sm-9">
                                                <span class="pull-left" style="padding-top: 10px">LOOKING TO HIRE A SKILL IN THIS CATEGORY? <br/>
                                                    Get notified, as soon as your desired skill is active in this category.
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="button" class="btn btn-noskill-notify pull-right" onclick="NotifymeModal()"> <img src="{{url('public/Assets/frontend')}}/images/bell.png" onclick="NotifymeModal()" alt="NOTIFY"> Notify me </button>
                                            </div>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="no-skill-added">
                                        <div class="category-list">
                                            <div class="bottom-border">
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12 noskill-heading" >
                                                        <div class="noskill-title text-center" style="padding-left: 20px;">
                                                        <h5>We have not found any results for your search. Please try a different search term.</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                @endif
                                {{-- <div class="col-sm-12">
                                    <div class="all-category all-cate-results">
                                        <div class="category-text">
                                            <h2 style="text-align:center;">We Didn't Find Any Results For Your Search.</h2>
                                            <p></p>
                                        </div>
                                    </div>
                                </div> --}}

                            @endif
                        </div>
                    </div>

                    <div id="listview" class="courses_listing" style="display: none">
                        
                        @if(isset($finalresult) && count($finalresult))
                            @foreach($finalresult as $k=>$v)
                                
                                @if($v['type'] == "jobs")
                                    @php
                                        $title =App\Helpers\General::TitleSlug($v['data']['v_job_title']);
                                    @endphp

                                    <div class="col-xs-12">
                                        <div class="all-leval height_container @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start') gridviewbordersponseredjob  @endif">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="leval-img-sortng">
                                                        
                                                        @php
                                                            $imgins="";
                                                            if(isset($v['data']['v_photos'][0])){
                                                                $imgins = $v['data']['v_photos'][0];
                                                            } 

                                                            $imgdata="";
                                                            if($imgins!=""){
                                                                $imgdata = $_S3_PATH.'/'.$imgins;    
                                                            }else{
                                                                $imgdata = $_S3_PATH.'/jobpost/photos/No_Image_Available.jpg';          
                                                            }

                                                            //$imgdata = $_S3_PATH.'/'.$imgins;
                                                            
                                                            // if(Storage::disk('s3')->exists($imgins)){
                                                            //     $imgdata = \Storage::cloud()->url($imgins);
                                                            // }else{
                                                            //     $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                                            // }  
                                                        @endphp
                                                         <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                        <img class="height_div" src="{{$imgdata}}" alt="" />
                                                        </a>
                                                       @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start')
                                                       <div class="Sponsored-second">
                                                            <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">Sponsored AD</a>
                                                       </div>
                                                       @endif
                                                    </div>
                                                </div>

                                                @if(isset($v['data']['e_urgent']) && $v['data']['e_urgent']=="yes")    
                                                    <div class="urgent1">
                                                        <label>URGENT</label>
                                                    </div>
                                                @endif
                                                
                                                <div class="col-sm-8 height_div">
                                                    <div class="center-courses">
                                                        <div class="row">
                                                            <div class="col-xs-10 no-padding">
                                                                <div class="center-courses-text">
                                                                    
                                                                    <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                                    <span>{{isset($v['data']['v_job_title']) ? $v['data']['v_job_title'] : ''}}</span> 
                                                                    </a>

                                                                    <p>
                                                                    @php
                                                                        $f_name="";
                                                                        $l_name="";
                                                                        if(isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])){
                                                                            if(isset($user_data[$v['data']['i_user_id']]['v_fname'])){
                                                                                $f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
                                                                            }
                                                                            if(isset($user_data[$v['data']['i_user_id']]['v_lname'])){
                                                                                $l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
                                                                            }
                                                                        } 
                                                                        echo $f_name.' '.$l_name; 
                                                                     @endphp
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-xs-2">
                                                                <div class="courses-icon">
                                                                    @if(in_array($v['data']['_id'], $s_job))
                                                                        <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist_listview('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                                        <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_listview_{{$v['data']['_id']}}" /></a>
                                                                        <input type="hidden" name="img_click_listview_{{$v['data']['_id']}}" id="img_click_listview_{{$v['data']['_id']}}" value="1">

                                                                    @else
                                                                        <a href="javascript:;" title="shortlist" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist_listview('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                                        <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_listview_{{$v['data']['_id']}}" /></a>
                                                                        <input type="hidden" name="img_click_listview_{{$v['data']['_id']}}" id="img_click_listview_{{$v['data']['_id']}}" value="0">
                                                                    @endif
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-12 no-padding">
                                                                <div class="center-courses-text">
                                                                    <p>
                                                                   @if(isset($v['data']['l_short_description']))
                                                                        @if(strlen($v['data']['l_short_description'])>280)
                                                                            @php
                                                                            echo substr($v['data']['l_short_description'],0,280).'...';
                                                                            @endphp
                                                                        @else
                                                                            @php
                                                                            echo $v['data']['l_short_description'];
                                                                            @endphp
                                                                        @endif
                                                                        {{-- @php echo $v['data']['l_job_description']; @endphp --}}
                                                                   @endif
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6 no-padding text-center-xs">
                                                                <label class="leval-all-seller">Job</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="courses-icon course-pricevalue">
                                                                     @if(isset($v['data']['v_budget_type']) && $v['data']['v_budget_type']=='open_to_offers')
                                                                    <p>Open to Offers</p>
                                                                    @else
                                                                    <P>Budget Amount <span>£{{isset($v['data']['v_budget_amt']) ? $v['data']['v_budget_amt'] :'0'}}</span></P>
                                                                    @endif
                                                                </div>
                                                                <div class="rating-leval rating_inline">
                                                                    <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{number_format($v['data']['i_total_avg_review'],1)}}" readonly>
                                                                    <p> {{number_format($v['data']['i_total_avg_review'],1)}}<span class="point"> &nbsp;({{$v['data']['i_total_review']}})</span></p>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @else
                                    @php
                                        $title="";
                                            if(isset($v['data']['v_profile_title'])){
                                                $title =App\Helpers\General::TitleSlug($v['data']['v_profile_title']);
                                            }

                                        
                                    @endphp
                                    <div class="col-xs-12">
                                        <div class="all-leval @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start') gridviewbordersponsered  @endif">
                                            <div class="row height_container">
                                                <div class="col-sm-4 ">
                                                    
                                                    <div class="leval-img-sortng" >
                                                    @if(isset($v['data']['v_introducy_video']) && $v['data']['v_introducy_video']!="")
                                                        @php
                                                            $videodata = $_S3_PATH.'/'.$v['data']['v_introducy_video'];
                                                            //$videodata=\Storage::cloud()->url($v['data']['v_introducy_video']);
                                                        @endphp
                                                        <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                        <video class="height_div" id="videoPlayerlist{{$k}}" width="100%">
                                                            <source src="{{$videodata}}" type="video/mp4">
                                                        </video>
                                                        </a>
                                                        <img src="{{url('public/Assets/frontend/images/player.png')}}" onclick="playPausevideo('{{$k}}')" class="imgcontroler" id="videoControlerlist{{$k}}"  alt="" style="width: auto;height: auto;position: absolute;left: 0;right: 0;margin: auto;top: 2%;margin-right: 10px" /> 

                                                    @elseif(isset($v['data']['v_work_photos']) && count($v['data']['v_work_photos']))
                                                        @php
                                                            $imag = $_S3_PATH.'/'.$v['data']['v_work_photos'][0];
                                                            //$imag=\Storage::cloud()->url($v['data']['v_work_photos'][0]);
                                                        @endphp
                                                        <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                        <img class="height_div" src="{{$imag}}" alt="" />
                                                        </a>
                                                    
                                                    @else
                                                        @php
                                                            $imgdata = $_S3_PATH.'/jobpost/photos/No_Image_Available.jpg'; 
                                                            //$imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg'); 
                                                        @endphp
                                                        <img class="height_div" src="{{$imgdata}}" alt="" />
                                                    
                                                    @endif

                                                    @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status']=="start")
                                                    <div class="Sponsored-second">
                                                        <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">Sponsored AD</a>
                                                    </div>
                                                    @endif

                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-8 height_div">
                                                    <div class="center-courses">
                                                        <div class="row">
                                                            <div class="col-xs-10 no-padding">
                                                                <div class="center-courses-text">
                                                                    
                                                                    <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                                    <span>{{isset($v['data']['v_profile_title']) ? $v['data']['v_profile_title'] : ''}}</span>
                                                                    </a>
                                                                    <p>
                                                                        @php
                                                                            $f_name="";
                                                                            $l_name="";
                                                                            if(isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])){
                                                                                
                                                                                if(isset($user_data[$v['data']['i_user_id']]['v_fname'])){
                                                                                    $f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
                                                                                }
                                                                                if(isset($user_data[$v['data']['i_user_id']]['v_lname'])){
                                                                                    $l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
                                                                                }
                                                                            } 
                                                                            echo ucfirst($f_name).' '.$l_name; 
                                                                        @endphp
                                                                     </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-2">
                                                                <div class="courses-icon">
                                                                    @if(in_array($v['data']['_id'], $s_skill))
                                                                        <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist_listview('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                                        <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_listview_{{$v['data']['_id']}}" /></a>
                                                                        <input type="hidden" name="img_click_listview_{{$v['data']['_id']}}" id="img_click_listview_{{$v['data']['_id']}}" value="1">

                                                                    @else
                                                                        <a href="javascript:;" title="shortlist" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist_listview('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                                        <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_listview_{{$v['data']['_id']}}" /></a>
                                                                        <input type="hidden" name="img_click_listview_{{$v['data']['_id']}}" id="img_click_listview_{{$v['data']['_id']}}" value="0">
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 no-padding">
                                                                <div class="center-courses-text">
                                                                    <p>@if(isset($v['data']['l_short_description']))
                                                                        @if(strlen($v['data']['l_short_description'])>280)
                                                                            @php
                                                                                $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                                                                $ldesc = str_replace("</strong>", " ", $ldesc);
                                                                                echo substr($ldesc,0,280).'...';
                                                                            @endphp
                                                                        @else
                                                                            @php
                                                                                $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                                                                $ldesc = str_replace("</strong>", " ", $ldesc);
                                                                                echo $ldesc;
                                                                            @endphp
                                                                        @endif
                                                                        {{-- @php 
                                                                        $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                                                        $ldesc = str_replace("</strong>", " ", $ldesc);
                                                                        echo $ldesc;
                                                                        @endphp --}}
                                                                   @endif
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                            
                                                        <div class="row">
                                                            
                                                            <div class="col-md-6 no-padding text-center-xs">
                                                                <label class="leval-all-seller">Skill</label>
                                                            </div>


                                                           
                                                            <div class="col-md-6">
                                                                <div class="courses-icon course-pricevalue">
                                                                    <p>STARTING AT <span>
                                                                        @if(isset($v['data']['information']['basic_package']) && count($v['data']['information']['basic_package']))
                                                                              £{{$v['data']['information']['basic_package']['v_price']}}
                                                                        @endif 
                                                                    </span></p>
                                                                </div>
                                                                <div class="rating-leval rating_inline">
                                                                    <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$v['data']['i_total_avg_review']}}" readonly>
                                                                    <p>{{number_format($v['data']['i_total_avg_review'],1)}}<span class="point">&nbsp;({{$v['data']['i_total_review']}})</span></p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                @endif
                            @endforeach
                        @else
                            @if(isset($data['i_category_id']) && $data['i_category_id']!="")
                                    <div class="no-skill-added">
                                        <div class="category-list">
                                            <div class="bottom-border">
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12 noskill-heading" >
                                                        <div class="noskill-title " style="padding-left: 20px;">
                                                        <h4>
                                                            @if(isset($categories) && count($categories))
                                                                @foreach($categories as $k=>$v)
                                                                    @if(isset($data['i_category_id']) && $data['i_category_id']==$v->id)
                                                                        {{$v->v_name}}
                                                                    @endif
                                                                @endforeach
                                                            @endif    
                                                        </h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-xs-12">
                                                        <div class="noskill-body noskill-heading ">
                                                            <div class="row">
                                                                <div class="col-sm-12 col-xs-12 text-center">
                                                                    <img src="{{url('public/Assets/frontend')}}/images/no-skilluser.png" class="img-responsive no-skilluser" alt="" />
                                                                    <div class="noskill-title" style="padding: 10px 0 10px;">
                                                                        <p style="margin: 5px 0px 14px;">We are curently looking for the skills in this category.</p>
                                                                        <a href="{{url('account/seller')}}">
                                                                        <button type="button" class="btn btn-applynow"> APPLY NOW</button>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="notify-msg" style="margin-bottom: 75px;padding: 0px 18px;">
                                            <div class="col-sm-9">
                                                <span class="pull-left" style="padding-top: 10px">LOOKING TO HIRE A SKILL IN THIS CATEGORY? <br/>
                                                    Get notified, as soon as your desired skill is active in this category.
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="button" class="btn btn-noskill-notify pull-right" onclick="NotifymeModal()"> <img src="{{url('public/Assets/frontend')}}/images/bell.png" onclick="NotifymeModal()" alt="NOTIFY"> Notify me </button>
                                            </div>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="no-skill-added">
                                        <div class="category-list">
                                            <div class="bottom-border">
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12 noskill-heading" >
                                                        <div class="noskill-title text-center" style="padding-left: 20px;">
                                                        <h5>We have not found any results for your search. Please try a different search term.</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                @endif
                        @endif        
                    </div>
                   

                </div>
            </div>
        </div>
    </div>

    <?php 
        $skillidsdata="";
        if(count($skillidsarray)){
            $skillidsdata = implode(",", $skillidsarray);
        }

        $jobidsdata="";
        if(count($jobidsarray)){
            $jobidsdata = implode(",", $jobidsarray);
        }
    ?>
    <input type="hidden" name="skillidsdata" value="{{$skillidsdata}}" id = "skillidsdata" autocomplete="off" />
    <input type="hidden" name="jobidsdata" value="{{$jobidsdata}}" id = "jobidsdata" autocomplete="off" />

@stop


@section('js')

<script type="text/javascript">
    @if(isset($_REQUEST['i_category_id']))
    var datahe=$(".filtaring-main").offset().top-100;
    $('html, body').animate({
        scrollTop: datahe
    }, 800);
@endif

</script>

<script type="text/javascript">
    function NotifymeModal(){
        $("#Notifyme").modal("show");
        $("#v_email").val("");
        $("#i_skill_id").val("");
    }

    function submitNotify(){
        
        var actionurl = "{{url('notifyformdata')}}";
        var formValidFalg = $("#notifyform").parsley().validate('');

        if(formValidFalg){

            document.getElementById('load').style.visibility="visible"; 
            var formdata = $("#notifyform").serialize();

            $.ajax({
                type    : "POST",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    document.getElementById('load').style.visibility='hidden';
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#Notifyme").modal("hide");
                        $("#popupmessage").html("Thank you, we'll notify you as soon as your requested skill is active.");
                        $("#sendMessageSuccess").modal("show");

                    }    
                    else{
                        $("#Notifyme").modal("hide");
                        $("#popupmessage").html("Something went wrong.Please try again after sometime.");
                        $("#sendMessageSuccess").modal("show");
                    }
                },
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                    $("#Notifyme").modal("hide");
                    $("#popupmessage").html("Something went wrong.Please try again after sometime.");
                    $("#sendMessageSuccess").modal("show");
                }
            });

        }


        var hidval = $("#img_click_"+id).val();
        var actionurl = "{{url('shortlist')}}";
        var formdata = "_id="+id+"&hidval="+hidval+"&type="+type; 

        
    }

</script>


<script type="text/javascript">

    $(window).scroll(function () {
        if($(window).scrollTop() + window.innerHeight >= document.body.scrollHeight) { 
        //if($(window).scrollTop() == $(document).height() - $(window).height()) {
            
            var actionurl = "{{url('Loadmoreskilljob')}}";

            var mdata=$("#searchcoursesmaster").serialize();
            var sdata=$("#searchcoursesside").serialize();
            var formdata = mdata+"&"+sdata;;
            
            var skillidsdata = $("#skillidsdata").val();
            var jobidsdata = $("#jobidsdata").val();
            formdata = formdata+"&skillidsdata="+skillidsdata+"&jobidsdata="+jobidsdata;

            document.getElementById('load').style.visibility="visible";
            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    document.getElementById('load').style.visibility='hidden';
                    if(obj['status']==1){
                        $("#box-content-grid").append(obj['gridviewtext']);
                        $("#listview").append(obj['listviewtext']);
                        if(obj['ispricefilter']==1){
                            $("#pricefilterdiv").css("display","block");
                        }
                        $("#skillidsdata").val(obj['existskillids']);
                        $("#jobidsdata").val(obj['existjobids']);                        
                    }else{
                    }
                },
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                }
            });

        }
    });


    // $(window).scroll(function() { //detact scroll
    //     if($(window).scrollTop() + $(window).height() >= 500){ //scrolled to bottom of the page
    //         alert("Ddd"); //load content chunk 
    //     }
    // }); 

    // $(window).scroll(function() {
    //     if($(window).scrollTop() == $(document).height() - $(window).height()) {
    //          alert("ddd"); 
    //     }
    // });

    function shortlist(id="", type=""){
        
        var hidval = $("#img_click_"+id).val();
        var actionurl = "{{url('shortlist')}}";
        var formdata = "_id="+id+"&hidval="+hidval+"&type="+type; 

        $.ajax({
            
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                var obj = jQuery.parseJSON(res);
                if(obj['status']==1){
                    $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                    $("#bg_listview_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                    $(".menut"+id).attr('title',"shortlisted");
                    $("#img_click_"+id).val("1");
                    $("#commonmsg").html(obj['msg']);
                }    
                else if(obj['status']==2){
                    $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                    $("#bg_listview_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                    $(".menut"+id).attr('title',"shortlist");
                    $("#img_click_"+id).val("0");
                    $("#commonmsg").html(obj['msg']);
                }
                else if(obj['status']==3){
                   window.location.replace("{{'login'}}");
                }else{
                    $("#commonmsg").html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
            }
        });
    }

    function shortlist_listview(id="", type=""){
        
        var hidval = $("#img_click_listview_"+id).val();
        var actionurl = "{{url('shortlist')}}";
        var formdata = "_id="+id+"&hidval="+hidval+"&type="+type; 

        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                var obj = jQuery.parseJSON(res);
                if(obj['status']==1){
                    $("#bg_listview_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                    $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                    $(".menut"+id).attr('title',"shortlisted");
                    $("#img_click_listview_"+id).val("1");
                    $("#commonmsg").html(obj['msg']);
                }    
                else if(obj['status']==2){
                    $("#bg_listview_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                    $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                    $(".menut"+id).attr('title',"shortlist");
                    $("#img_click_listview_"+id).val("0");
                    $("#commonmsg").html(obj['msg']);
                }
                else if(obj['status']==3){
                   window.location.replace("{{'login'}}");
                }else{
                    $("#commonmsg").html(obj['msg']);
                }

            },
            error: function ( jqXHR, exception ) {
            }
        });
    }


</script>


<script type="text/javascript">
    
    // $("input:checkbox").on('click', function() {
    //   var $box = $(this);
    //   if ($box.is(":checked")) {
    //     var group = "input:checkbox[name='" + $box.attr("name") + "']";
    //     $(group).prop("checked", false);
    //     $box.prop("checked", true);
    //   } else {
    //     $box.prop("checked", false);
    //   }
    // });

</script>

<script type="text/javascript">

function viewchange(data) {
    
    var imgpath = "{{url('public/Assets/frontend/images')}}";
    
    if(data=="grid"){
        $("#listview").css("display","none");
        $("#gridview").css("display","inline");
        
        var gridicon = imgpath+'/fore.png';
        var listicon = imgpath+'/fore2.png';
        $("#gridicon").attr("src",gridicon);
        $("#listicon").attr("src",listicon);

    }else{  
        $("#listview").css("display","block");
        $("#gridview").css("display","none");
        var gridicon = imgpath+'/fore33.png';
        var listicon = imgpath+'/fore22.png';
        $("#gridicon").attr("src",gridicon);
        $("#listicon").attr("src",listicon);
    }
}


function masterfilter(){
    
    var mdata=$("#searchcoursesmaster").serialize();
    var sdata=$("#searchcoursesside").serialize();
    window.location.href = "{{url('search')}}?"+mdata+"&"+sdata;
    //$("#searchcoursesmaster").submit();
}


function masterfiltertop(){
    
    //$('.checkboxdata').prop("checked", false);
    var mdata=$("#searchcoursesmaster").serialize();
    var sdata=$("#searchcoursesside").serialize();
    window.location.href = "{{url('search')}}?"+mdata+"&"+sdata;
}

function masterfilterMain(data){
    
    if(data=="v_type_skill"){
        $("#v_type_job").prop( "checked", false );
        $("#v_type_skill").prop( "checked", true );
    }else{
        $("#v_type_job").prop( "checked", true );
        $("#v_type_skill").prop( "checked", false );
    }
    //$('.checkboxdata').prop("checked", false);
    var mdata=$("#searchcoursesmaster").serialize();
    var sdata=$("#searchcoursesside").serialize();
    window.location.href = "{{url('search')}}?"+mdata+"&"+sdata;
}


function cleardata(data){
    $('input[name="'+data+'[]"]').prop("checked", false);
    masterfilter();
}


// function masterfilter(){
//     $("#searchcoursesmaster").submit();
// }

$( document ).ready(function() {
    var s1 = $('.same-height-two').height();
    var s2 = $('.same-height').height();

    if (s1 > s2)
        $('.same-height-two').css('height', s1 + "px");
    else
        $('.same-height').css('height', s2 + "px");
});


function playPausevideogrid(data) { 
           
            var video = document.getElementById("videoPlayergrid"+data);

            if (video.paused)
            {
                video.play();
                if (video.play)
                {   
                    $("#videoControlergrid"+data).attr('src',"{{url('public/Assets/frontend/images/pause.png')}}");
                    //$("#videoControlergrid"+data).hide();
                }
            }
            else
            {
                video.pause();
                if (video.pause)
                {
                    $("#videoControlergrid"+data).attr('src',"{{url('public/Assets/frontend/images/player.png')}}");    
                    //$("#videoControlergrid"+data).show();
                }
            }

        }


 function playPausevideo(data) { 
    var video = document.getElementById("videoPlayerlist"+data);
    
    if (video.paused)
    {
        video.play();
        if (video.play)
        {   
            $("#videoControlerlist"+data).attr('src',"{{url('public/Assets/frontend/images/pause.png')}}");
            //$("#videoControlerlist"+data).hide();
        }
    }
    else
    {
        video.pause();
        if (video.pause)
        {
            $("#videoControlerlist"+data).attr('src',"{{url('public/Assets/frontend/images/player.png')}}");    
            //$("#videoControlerlist"+data).show();
        }
    }
}


</script>


@stop

