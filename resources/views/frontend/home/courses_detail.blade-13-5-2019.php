@extends('layouts.frontend')


@section('content')
    <style type="text/css">
        .all-category-course .category-img > video {height: 215px;width: 100%;}
        .all-category-course .controlerimg {position: absolute;width: 100%;left: -3%;top: 59%;text-align: right;cursor: pointer;}
        .videopoupmodel video {
    margin: 0 auto;
    height: 700px !important;
}
.video_title_container .close{
    right:31px;
}



    </style>


<link rel="stylesheet" href="{{url('public/Assets/frontend/css/videre.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{url('public/Assets/frontend/css/app.css')}}" type="text/css" media="all"> 
<link rel="stylesheet" href="{{url('public/Assets/frontend/css/ionicons.min.css')}}">

<style type="text/css">
.modal-backdrop.in {display: none;}
.btn-add-remove:hover {background:transparent;color: #a00960; border:2px solid #a00960;}
@media (min-width: 768px) {
  .container {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .container {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .container {
    width: 1170px;
  }
}
</style>

<div id="videoplaymodal" class="modal3 videopoupmodel coursevideofront">
    <div class="modal-content-all">
        <div class="container-fluid video_title_container">
            <div class="margin-t-b">
                <div class="row">  
                    <div class="col-sm-6">
                        <div class="vid-tital">
                            <h3>{{isset($courses->v_title) ? $courses->v_title : ''}}</h3>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-popup-all">
                        <button class="btn btn-vid" type="button" onclick="closevideopopup()">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <div class="video-play">
            <div id="player"></div>
        </div>
    </div>
</div>
    
    @php
        $categorydata=array();
        if(count($courses->hasCategory())){
            $categorydata = $courses->hasCategory();
        }

        $bannerimage="";
        if(isset($categorydata['v_main_image']) && $categorydata['v_main_image']!=""){
            $bannerimage =  \Storage::cloud()->url($categorydata['v_main_image']);  
        }else{
            $bannerimage =  url('public/Assets/frontend/images/33b-courses.png');  
        }

        $bannerTitle="";
        if(isset($categorydata['v_title']) && $categorydata['v_title']!=""){
            $bannerTitle =  $categorydata['v_title'];  
        }else{
            $bannerTitle =  "Courses";  
        }

        $bannerSubTitle="";
        if(isset($categorydata['v_sub_headline']) && $categorydata['v_sub_headline']!=""){
            $bannerSubTitle =  $categorydata['v_sub_headline'];  
        }else{
            $bannerSubTitle =  "The best marketplace for finding excellent course content";  
        }

    @endphp
   
    <!--slide-containt-->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">
                <img src="{{$bannerimage}}" class="img-responsive" alt="" style="max-height: 566px;object-fit: cover;"/>
                <div class="center-containt">
                    <div class="container">
                        <div class="filtaring-page">
                            <h1>{{$bannerTitle}}</h1>
                            <p style="margin-top: 25px">{{$bannerSubTitle}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-slide-containt-->

    <!--final-containt-->
    <div class="container">
        <div class="courses-detail">
            <div class="row">

                @if ($success = Session::get('success'))
                  <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 0px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
                  </div>
                @endif
                @if ($warning = Session::get('warning'))
                  <div class="alert alert-info alert-danger" style="margin-top: 0px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
                  </div>
                @endif

                <div class="col-sm-8">
                    <div class="center-courses-text">
                        <span class="only-text">{{isset($courses->v_title) ? $courses->v_title:''}}</span>
                        <p>Course by : {{isset($courses->v_author_name) ? ucfirst($courses->v_author_name):''}}</p>
                    </div>
                    @php
                    $totallacture=0;
                    if(isset($courses->section) && count($courses->section)){
                        foreach ($courses->section as $key => $value) {
                            $totallacture= $totallacture+count($value['video']);   
                        }
                    }
                    @endphp
                    <div class="star-point">
                        <div class="list-leval-final">
                            <ul class="list-leval">
                                <li><span>{{$totallacture}}</span> Lectures</li>
                                |
                                <li><span>{{isset($courses->i_duration) ? $courses->i_duration:''}}</span> Hours</li>
                                |
                                <li><span>{{count( $courses->hasLevel() ) ? $courses->hasLevel()->v_title : ''}}</span> Level</li>
                                |
                                <li>{{count( $courses->hasCategory() ) ? $courses->hasCategory()->v_title : ''}} </li>
                            </ul>
                        </div>
                        
                        <div class="rating-leval1 _scrollreviewdata">
                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating disrating" title="" value="{{$courses->i_total_avg_review}}" disabled>
                            <p><a href="#review_scroll">
                            {{number_format($courses->i_total_avg_review,1)}}    
                            <span class="point">({{$courses->i_total_review}})</span></a></p>
                        </div>
                    </div>
                   
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="course-2">
                                <p>{{isset($courses->v_sub_title) ? $courses->v_sub_title:''}}</p>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="required-line">
                                <h2>Requirements</h2>
                                <p><?php 
                                    if(isset($courses->v_requirement)){
                                        echo $courses->v_requirement;    
                                    }
                                ?>
                                </p>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="required-line">
                                <h2>Description</h2>
                                <p><?php 
                                    if(isset($courses->l_description)){
                                        echo $courses->l_description;    
                                    }
                                ?>
                                </p>
                            </div>
                        </div>


                        <div class="col-xs-12">
                            <div class="required-line">
                                <h2>Course Chapters</h2>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    
                                    @if(isset($courses->section) && count($courses->section))
                                        @foreach($courses->section as $k=>$v)
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$k}}" aria-expanded="false" aria-controls="collapse{{$k}}">
                                                            <i class="more-less glyphicon glyphicon-plus"></i>
                                                           {{$k+1}}. {{$v['v_title']}}
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse{{$k}}" class="panel-collapse collapse @if($k<1)in @endif" role="tabpanel" aria-labelledby="heading{{$k}}">
                                                    <div class="panel-body">
                                                        
                                                        
                                                        @if(isset($v['video']) && count($v['video']))
                                                            @foreach($v['video'] as $key=>$val)
                                                                <div class="coursevideo-list root-position">
                                                                    <div class="video-list-course onerowvideo">
                                                                         @php
                                                                            $videoUrl="";
                                                                            if(Storage::disk('s3')->exists($val['v_video'])){
                                                                                $videoUrl = \Storage::cloud()->url($val['v_video']);      
                                                                            }
                                                                         @endphp
                                                                         @if(isset($val['i_preview']) && $val['i_preview']=="on")
                                                                         <a href="javascript:;" 
                                                                            class="cls-video" 
                                                                            onclick="playVideo(this, '{{$videoUrl}}');" 
                                                                            data-video-url="{{$videoUrl}}" 
                                                                            data-play="0" 
                                                                            data-sid="{{$val['_id']}}" 
                                                                            data-bcid="{{$v['_id']}}" 
                                                                            >
                                                                        @endif    
                                                                            
                                                                            <img src="{{url('public/Assets/frontend/images/videoplay-icon.png')}}" class="coursevideo-player" alt="video-player">{{$val['v_title']}}
                                                                            <span> {{$val['v_duration_min']}}:{{$val['v_duration_sec']}} </span>
                                                                        @if(isset($val['i_preview']) && $val['i_preview']=="on")    
                                                                        </a>
                                                                        @endif
                                                                    
                                                                    
                                                                    @if(isset($val['i_preview']) && $val['i_preview']=="on")
                                                                        <a href="javascript:;" 
                                                                            class="cls-video" 
                                                                            onclick="playVideo(this, '{{$videoUrl}}');" 
                                                                            data-video-url="{{$videoUrl}}" 
                                                                            data-play="0" 
                                                                            data-sid="{{$val['_id']}}" 
                                                                            data-bcid="{{$v['_id']}}" 
                                                                            >
                                                                            <div class="fileset-preview-courses cpreview" style="margin-right: 130px;">
                                                                                <span> PREVIEW  </span>
                                                                            </div>
                                                                        </a>    
                                                                    @endif   
                                                                    
                                                                    @if(isset($val['v_doc']) && $val['v_doc']!="")
                                                                    <?php 
                                                                        $doctype = explode(".", $val['v_doc']);
                                                                        if(isset($doctype[1])){
                                                                            $doctype = $doctype[1];     
                                                                        }else{
                                                                            $doctype = ""; 
                                                                        }
                                                                        $docUrl="";    
                                                                        if(Storage::disk('s3')->exists($val['v_doc'])){
                                                                            $docUrl = \Storage::cloud()->url($val['v_doc']);      
                                                                        }

                                                                    ?>
                                                                    <div class="fileset-preview-courses">
                                                                        @if(isset($val['i_preview']) && $val['i_preview']=="on")
                                                                        <span> <a href="{{$docUrl}}" download target="_blank" >{{strtoupper($doctype)}}</a> </span>
                                                                        @else
                                                                        <span> <a href="javascript:;" download target="_blank" >{{strtoupper($doctype)}}</a> </span>
                                                                        @endif

                                                                    </div>
                                                                    @endif
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endif        


                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif        
                                </div>
                            </div>
                        </div>
                       
                       <?php 
                            $v_image="";
                            if(count($courses->hasUser()) && isset($courses->hasUser()->v_image)){
                                $v_image=$courses->hasUser()->v_image;
                            }
                            $v_fname="";
                            if(count($courses->hasUser()) && isset($courses->hasUser()->v_fname)){
                                $v_fname=$courses->hasUser()->v_fname;
                            }

                            $v_lname="";
                            if(count($courses->hasUser()) && isset($courses->hasUser()->v_lname)){
                                $v_lname=$courses->hasUser()->v_lname;
                            }

                            $i_course_total_avg_review="0";
                            if(count($courses->hasUser()) && isset($courses->hasUser()->i_course_total_avg_review)){
                                $i_course_total_avg_review=$courses->hasUser()->i_course_total_avg_review;
                            }
                            $i_course_total_review="0";
                            if(count($courses->hasUser()) && isset($courses->hasUser()->i_course_total_review)){
                                $i_course_total_review = $courses->hasUser()->i_course_total_review;
                            }

                            
                         ?> 

                        <div class="col-xs-12">
                            <div class="required-line">
                                <h2>About the Instructor</h2>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="About-img">
                                        @php
                                            $v_image="";
                                            if(isset($courses->v_instructor_image) && $courses->v_instructor_image!=""){
                                                $v_image = \Storage::cloud()->url($courses->v_instructor_image);
                                            }else{
                                                $v_image =  \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                            }   
                                        @endphp
                                        <img src="{{$v_image}}" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="about-name">
                                            <h3>{{isset($courses->v_author_name) ? ucfirst($courses->v_author_name):''}}</h3>
                                            <p>&nbsp;</p>
                                            @php
                                                $sellerrev = \App\Helpers\General::sellerReview($courses->i_user_id);    
                                            @endphp
                                           {{--  <div class="rating-leval2">
                                                <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$i_course_total_avg_review}}" readonly>
                                                <p>&nbsp;{{number_format($i_course_total_avg_review,1)}}&nbsp;<span class="point">({{$i_course_total_review}})</span></p>
                                            </div> --}}
                                        </div>
                                        <div class="about-all-text">
                                            <p style="margin-top: 0px;">
                                                @php
                                                    if(isset($courses->l_instructor_details)){
                                                        echo $courses->l_instructor_details;
                                                    }
                                                @endphp
                                               </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @php
                            $totalavgInstructions=0;
                            $totalavgNavigate=0;
                            $totalavgrecommend=0;

                            if(isset($reviews) && count($reviews)){
                                foreach ($reviews as $key => $value) {
                                    $totalavgInstructions  = $totalavgInstructions+$value->f_rate_instruction;
                                    $totalavgNavigate  = $totalavgNavigate+$value->f_rate_navigate;
                                    $totalavgrecommend  = $totalavgrecommend+$value->f_rate_reccommend;

                                }
                                $totalavgInstructions  = $totalavgInstructions/count($reviews);
                                $totalavgNavigate  = $totalavgNavigate/count($reviews);
                                $totalavgrecommend  = $totalavgrecommend/count($reviews);
                            }

                        @endphp


                        <div class="col-xs-12">
                            <div class="required-line" id="review_scroll">
                                
                                <div class="rating-inline">
                                    <h2>Review</h2>
                                    <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$courses->i_total_avg_review}}" required>
                                    <p>&nbsp; {{number_format($courses->i_total_avg_review,1)}}  <span class="review-point">({{$courses->i_total_review}})</span></p>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 text-center">
                                        Clear Instructions
                                        <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalavgInstructions}}" readonly>
                                    </div>
                                    
                                    <div class="col-sm-4 text-center">
                                        Easy to Navigate
                                        <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalavgNavigate}}" readonly>
                                    </div>
                                    <div class="col-sm-4 text-center">
                                        Would recommend
                                        <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalavgrecommend}}" readonly>
                                     </div>
                                </div>
                                
                                @if(isset($reviews) && count($reviews))
                                    @foreach($reviews as $key=>$val)
                                        <div class="Review-profile-all">
                                            <div class="Review-profile">
                                                <div class="Review-img">
                                                    <!-- <img src="images/rev.png" alt="" /> -->
                                                    <?php
                                                        $imgdata="";
                                                        if(isset($userList[$val->i_user_id])){
                                                            $imgdata = \App\Helpers\General::userroundimage($userList[$val->i_user_id]);
                                                        }
                                                        echo $imgdata;
                                                    ?>  
                                                </div>
                                                <div class="Review-name">
                                                    <h3><?php
                                                            $username="";
                                                            if(isset($userList[$val->i_user_id])){
                                                                $username = $userList[$val->i_user_id]['v_name'];
                                                            }
                                                            echo $username;
                                                        ?>
                                                    </h3>
                                                    @php
                                                        $ratedata = ($val->f_rate_instruction+$val->f_rate_navigate+$val->f_rate_reccommend)/3;
                                                    @endphp
                                                    <div class="Review-name-final review_align position_unset mt_3">
                                                        <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$ratedata}}" readonly> &nbsp;<span class="review_number">{{number_format($ratedata,1)}}</span>
                                                    </div>
                                                    <p>{{$val->l_comment}}</p>
                                                </div>
                                            </div>
                                            
                                            @if(isset($val->l_answers) && count($val->l_answers))
                                                    <div class="Review-profile1">
                                                        <div class="Review-img1">
                                                            <?php
                                                                $imgdata="";
                                                                if(isset($userList[$val->l_answers['i_user_id']])){
                                                                    $imgdata = \App\Helpers\General::userroundimage($userList[$val->l_answers['i_user_id']]);
                                                                }
                                                                echo $imgdata;
                                                            ?>  
                                                        </div>
                                                        <div class="Review-name">
                                                            <h3><?php
                                                                $username="";
                                                                if(isset($userList[$val->l_answers['i_user_id']])){
                                                                    $username = $userList[$val->l_answers['i_user_id']]['v_name'];
                                                                }
                                                                echo $username;
                                                            ?></h3>
                                                            {{-- <div class="Review-name-final">
                                                                <input type="text" id="star-input-1" class="rating" title="" value="4" readonly>
                                                            </div> --}}
                                                            <p>{{$val->l_answers['l_comment']}}</p>
                                                        </div>
                                                    </div>
                                            @endif    
                                        </div>
                                    @endforeach
                                @endif    


                            </div>
                        </div>

                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="course-fream">
                        <div class="all-category-course">
                            
                            @php
                                $imgdata = \Storage::cloud()->url($courses->v_instructor_image);
                                $videodata = \Storage::cloud()->url($courses->l_video);
                            @endphp
                            <div class="category-img">
                                <a href="javascript:;" 
                                        class="cls-video" 
                                        onclick="playVideo(this, '{{$videodata}}');" 
                                        data-video-url="{{$videodata}}" 
                                        data-play="0" 
                                        data-sid="1" 
                                        data-bcid="1" >
                                <div class="controlerimg">
                                    <img src="{{url('public/Assets/frontend/images/player-new.png')}}" class="imgcontroler" id="videoControlergrid{{$courses->id}}"  alt="" />
                                </div>
                                 @php
                                    $imgins="";
                                    if(isset($courses->v_course_thumbnail) && $courses->v_course_thumbnail!=""){
                                        $imgins = $courses->v_course_thumbnail;
                                    }
                                    $imgdata=$_S3_PATH.'/'.$imgins
                                @endphp 

                                <video id="videoPlayergrid{{$courses->id}}" width="100%" height="100%" poster="{{$imgdata}}">
                                    <source src="{{$videodata}}" type="video/mp4">
                                </video>
                                </a>
                            </div>
                           
                            <div class="course-span-final">
                                <div class="course-span">
                                    @if($courses->f_price!="0")
                                    <span>£{{isset($courses->f_price) ? $courses->f_price:''}}</span>
                                    @else
                                    <span>Free</span>
                                    @endif
                                </div>
                                <div class="btn-class">
                                    <a href="{{url('course-cart/buy-now')}}/{{$courses->id}}">
                                    <button class="btn btn-buy-now">Buy Now</button>
                                    </a>
                                    <a href="{{url('course-cart/add')}}/{{$courses->id}}">
                                    <button class="btn btn-add-crat">Add To Cart</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--
            <div class="more-info-course">
                <a href="#"> Show More </a>
            </div>
            --}}
        </div>
    </div>
    <!--end-final-containt-->


@stop






@section('js')
<script src="{{url('public/Assets/frontend/js/videre.js')}}"></script>
  <script src="{{url('public/Assets/frontend/js/app.js')}}"></script>

<script type="text/javascript">

function playPausevideogrid(data) { 
           
    var video = document.getElementById("videoPlayergrid"+data);
    if (video.paused)
    {
        video.play();
        if (video.play)
        {
            $("#videoControlergrid"+data).hide();
        }
    }
    else
    {
        video.pause();
        if (video.pause)
        {
            $("#videoControlergrid"+data).show();
        }
    }

}
   
function playVideo( curr, videoUrl ){
    jQuery('#player').html('');
    jQuery('#player').attr('class','');
    jQuery('#player').attr('style','');
    
    jQuery('.cls-video').attr('data-play', 0);
    jQuery(curr).attr('data-play', 1);
    
    jQuery('.top-video-title').html( jQuery(curr).find('.video-title').html() );
    
    
    jQuery("#videoplaymodal").modal("show");
    
    jQuery('#player').videre({
        video: {
            quality: [{
                label: '720p',
                src: videoUrl,
                width:'100%'
            }, {
                label: '360p',
                src: videoUrl+'?SD'
            }, {
                label: '240p',
                src: videoUrl+'?SD'
            }],
            title: 'jQueryScript.Net'
        },
        dimensions: 1280
    });
    
    // markAsComplete(
    //     jQuery(curr).attr('data-sid'),
    //     jQuery(curr).attr('data-bcid'),
    //     videoUrl
    // );
}

function playNext(){
    var curr = '';
    var nextUrl = '';
    var getNext = 0;
    var cnt = 0;
    jQuery('.cls-video').each(function(){
        if( !nextUrl ){
            if( getNext ){
                nextUrl = jQuery(this).attr('data-video-url');
                jQuery(this).attr('data-play',1);
                curr = jQuery(this);
            }
            if( jQuery(this).attr('data-play') == 1 || jQuery(this).attr('data-play') == '1' ){
                getNext = 1;
                jQuery(this).attr('data-play',0);
            }
        }
    });
    if( !nextUrl ){
        curr = jQuery('.cls-video').first();
        nextUrl = curr.attr('data-video-url');
        curr.attr('data-play',1);
    }
    if( nextUrl ){
        playVideo( curr, nextUrl );
    }
}

</script>

<script type="text/javascript">
        $('a[href^="#review_scroll"]').on('click', function(event) {
        var target = $(this.getAttribute('href'));
        if( target.length ) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 2000);
        }
    });

    // $('._scrollreviewdata').on('click', function(event) {
    //     alert("Sss");
    //     // $('html, body').stop().animate({
    //     //         scrollTop: $("#review_scroll").offset().top
    //     // }, 1000);

    // });    

function closevideopopup(){
    jQuery('#player').html('');
    jQuery('#videoplaymodal').modal('hide');
}
        

</script>

@stop

