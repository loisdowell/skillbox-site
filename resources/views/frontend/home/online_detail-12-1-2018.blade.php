
@extends('layouts.frontend')

@section('content')

<style type="text/css">
  /*.slick-list{
    height: auto !important;
  }*/
  .slider_video_responsive .slick-slide > img{ max-height: 435px !important; }
</style>


<div class="modal fade" id="sendMessage" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 600px">
            <div class="modal-content modal-content-1">
              <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> Contact Seller </h4>
                </div>
                <form role="form" method="POST" name="sendMessageForm" id="sendMessageForm" data-parsley-validate enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="i_to_id" value="{{$sellerprofile->hasUser()->id}}">
                <input type="hidden" name="i_profile_id" value="{{$sellerprofile->id}}">

                <div class="modal-body">
                    <div class="modal-body-1" style="padding: 0px 30px !important;" id="contactsellerdetail">
                        <div class="row">
                             <div class="final-point">
                                <div class="buyer-name">
                                    <img src="https://skillboxs3bucket.s3.us-east-2.amazonaws.com/users/profile-1538572663.png" alt="">
                                </div>
                                <div class="buyer-tital">
                                    <h5 style="margin-left: 5px">Ray nanda</h5>
                                    <div class="rating-buyer rating-link rating_counts" style="margin-top: 6px !important;margin-bottom: 6px !important">
                                    <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="0" readonly>
                                    <span class="star-no"> 0.0 (0)</span>
                                    </div>
                                    <br>
                                    <span style="margin-left: 5px">Location : asdsadsadsa,sadsadsa</span>
                                </div>
                            </div>
                            <div id="popupcommonmsg"></div>
                            <div class="col-xs-12">
                              <div class="text-box-containt">
                                    <label> Subject </label>
                                    <input type="text" class="form-control input-field" name="v_subject_title" required>
                                </div>

                                <div class="text-box-containt">
                                    <label> Enter Your Message </label>
                                    <textarea rows="3" name="l_message" class="form-control popup-letter" required></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button  type="button" onclick="submitMessage()" class="btn btn-Submit-pop">Send Message</button>
                                </div>
                            </div>    
                            <div class="col-xs-12">
                                
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
</div>

<div class="modal fade" id="reportListingModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content modal-content-1">
              <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> Report listing </h4>
                </div>
                <form role="form" method="POST" name="reportListingForm" id="reportListingForm" data-parsley-validate enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="i_sellerprofile_id" value="{{$sellerprofile->id}}">
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <div id="popupcommonmsg"></div>
                            <div class="col-xs-6">
                              <div class="text-box-containt">
                                    <label> Name </label>
                                    <input type="text" class="form-control input-field" name="v_name" @if(isset($userdata['name'])) value="{{$userdata['name']}}" @endif required>
                              </div>
                            </div>    
                            <div class="col-xs-6">
                              <div class="text-box-containt">
                                    <label> Email </label>
                                    <input type="text" class="form-control input-field" name="v_email" @if(isset($userdata['email'])) value="{{$userdata['email']}}" @endif required>
                              </div>
                            </div>    
                            <div class="col-xs-12">
                              <div class="text-box-containt">
                                    <label> Please tell us about the issue </label>
                                    <select class="form-control input-field select_arrows" name="v_about_issue" required onchange="changedropdowndata(this.value)">
                                        <option value="">Select</option>
                                        <option value="Images">Images</option>
                                        <option value="Listing_Name">Listing Name</option>
                                        <option value="Bullet_Points">Bullet Points</option>
                                        <option value="Other_listing_details">Other listing details</option>
                                    </select>
                              </div>
                            </div>    
                            <div class="col-xs-12" style="margin-top: 20px;">
                              <div class="text-box-containt">
                                    <select class="form-control input-field select_arrows" name="v_about_issue_detail" id="v_about_issue_detail" required>
                                        <option value="">Select</option>
                                    </select>
                              </div>
                            </div> 
                            <div class="col-xs-12">
                                <div class="text-box-containt">
                                    <label>Comments</label>
                                    <textarea rows="3" name="l_comments" class="form-control popup-letter"></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button  type="button" onclick="submitReport()" class="btn btn-Submit-pop">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
</div>

<div id="sendMessageSuccess" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;">Message</h4>
      </div>
      <div class="modal-body">
        <p id="popupmessage" style="text-align: center;"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="responsive_online_detail">       
<div class="container">
 <div class="row">

    <div style="clear: both"></div><br>
    <div id="commonmsg"></div>

    <div class="col-sm-7 online_slider slider_video_responsive">
       <div class="slider-for">
            @if(isset($sellerprofile->v_introducy_video) && $sellerprofile->v_introducy_video!="")
                @php
                    $videodata = \Storage::cloud()->url($sellerprofile->v_introducy_video); 
                @endphp
                <div class="slide-location play_button" onclick="playPausevideogridSlider('intro')">
                    <img class="play_button_icon" src="{{url('public/Assets/frontend/images/play_button.png')}}" class="imgcontroler" id="videoControlergridSliderintro"  alt="" />
                    <span class="imgrem" id="workvideorem">
                        <video id="videoPlayergridSliderintro" class="videostop">
                          <source src="{{$videodata}}" type="video/mp4">
                        </video> 
                    </span>
                </div>
            @endif
          
           @if(isset($sellerprofile->v_work_video) && count($sellerprofile->v_work_video))
                
                @foreach($sellerprofile->v_work_video as $k=>$v)
                @php
                    $videodata = \Storage::cloud()->url($v); 
                @endphp
                 <div class="slide-location play_button" onclick="playPausevideogridSlider('work{{$k}}')">
                      <img class="play_button_icon" src="{{url('public/Assets/frontend/images/play_button.png')}}" class="imgcontroler" id="videoControlergridSliderwork{{$k}}"  alt="" />
                      <span class="imgrem" id="workvideorem">
                          <video id="videoPlayergridSliderwork{{$k}}" class="videostop">
                            <source src="{{$videodata}}" type="video/mp4">
                          </video> 
                      </span>
                  </div> 
                @endforeach
            @endif

             @if(isset($sellerprofile->v_work_photos))
                @foreach($sellerprofile->v_work_photos as $k=>$v)
                @php
                $imgdata = '';
                if(Storage::disk('s3')->exists($v)){
                    $imgdata = \Storage::cloud()->url($v);
                }else{
                    $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                }
                @endphp
                    <div class="slide-location">
                        <img src="{{$imgdata}}" class="img-responsive pausevideo" alt="" />   
                    </div>
                @endforeach
           @endif 
       </div>
       
       <div class="slider-nav">
            @if(isset($sellerprofile->v_introducy_video) && $sellerprofile->v_introducy_video!="")
                @php
                    $videodata = \Storage::cloud()->url($sellerprofile->v_introducy_video); 
                @endphp
                <div class="slider-look slide-location-nav slide-video-nav" >
                  <div class="slide-img-final play_button" onclick="playPausevideogridSlider('intro')">
                    <img class="play_button_icon" src="{{url('public/Assets/frontend/images/play_button.png')}}" class="imgcontroler" id="videoControlergrid"  alt="" />
                      <span class="imgrem" id="workvideorem">
                          <video>
                            <source src="{{$videodata}}" type="video/mp4">
                          </video> 
                      </span>
                  </div>
              </div>
            @endif

            @if(isset($sellerprofile->v_work_video) && $sellerprofile->v_work_video!="")
              
                @foreach($sellerprofile->v_work_video as $k=>$v)
                @php
                    $videodata = \Storage::cloud()->url($v); 
                @endphp
                <div class="slider-look slide-location-nav slide-video-nav">
                  <div class="slide-img-final play_button" onclick="playPausevideogridSlider('work{{$k}}')">
                    <img class="play_button_icon" src="{{url('public/Assets/frontend/images/play_button.png')}}" class="imgcontroler" id="videoControlergrid"  alt="" />
                      <span class="imgrem" id="workvideorem">
                      <video>
                        <source src="{{$videodata}}" type="video/mp4">
                      </video> 
                      </span>
                  </div>
              </div>
              @endforeach
            @endif

            @if(isset($sellerprofile->v_work_photos))
                @foreach($sellerprofile->v_work_photos as $k=>$v)
                @php
                $imgdata = '';
                if(Storage::disk('s3')->exists($v)){
                    $imgdata = \Storage::cloud()->url($v);
                }else{
                    $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                }
                @endphp    
                  <div class="slide-location-nav">
                     <img src="{{$imgdata}}" class="img-responsive pausevideo" alt="" />
                  </div>
                @endforeach
            @endif 
       </div>

       <div class="compare-tab">
          <div class="compare-header">
             Compare Packages
          </div>
          <div class="table-responsive">
          <table class="table">
             <thead>
                <tr>
                   <th class="all-leval-hight">
                      <p>Price</p>
                   </th>
                  @if(isset($sellerprofile->information) && count($sellerprofile->information))
                      @foreach($sellerprofile->information as $k=>$v)
                          @if(isset($v['v_price']) && $v['v_price']!="")
                          <th class="all-tab-hight">
                            <span>£{{isset($v['v_price']) ? $v['v_price']:''}} {{isset($v['v_title']) ? $v['v_title'] : ''}}</span>
                         </th>
                         @endif
                      @endforeach
                  @endif
                </tr>
             </thead>

             <tbody>
                <tr>
                  <td class="all-leval-hight">
                      <span class="time-price">What's Included</span>
                   </td>

                   @if(isset($sellerprofile->information) && count($sellerprofile->information))
                      @foreach($sellerprofile->information as $k=>$v)
                          @if(isset($v['v_price']) && $v['v_price']!="")
                            {{-- @if(isset($v['v_bullets']) && isset($v['v_bullets'][0]) && $v['v_bullets'][0]!="" && $v['v_bullets'][0]!=null) --}}
                            <td class="all-tab-hight">
                            @if(isset($v['v_bullets']))
                                @foreach($v['v_bullets'] as $data)
                                  @if($data!="" && $data!=null)
                                  <p>{{$data}}</p>
                                  @endif
                                @endforeach
                            @endif
                            </td>
                            {{-- @endif --}}
                         @endif
                      @endforeach
                  @endif
                </tr>
                <tr>
                   <td class="all-leval-hight">            
                      <span class="time-price">Delivery time</span>
                   </td>
                   @if(isset($sellerprofile->information) && count($sellerprofile->information))
                      @foreach($sellerprofile->information as $k=>$v)
                          @if(isset($v['v_price']) && $v['v_price']!="")
                            <td class="all-tab-hight">
                               <p>{{$v['v_delivery_time']}} 
                                @if($v['v_delivery_type']=="hours")
                                  Hour(s)
                                @endif
                                 @if($v['v_delivery_type']=="day")
                                  Day(s)
                                @endif
                                @if($v['v_delivery_type']=="month")
                                  Month(s)
                                @endif
                                @if($v['v_delivery_type']=="year")
                                  Year(s)
                                @endif
                                </p>
                            </td>
                         @endif
                      @endforeach
                  @endif
                </tr>

                @php
                  $addondis=0;  
                @endphp
                @if(isset($sellerprofile->information) && count($sellerprofile->information))
                    @foreach($sellerprofile->information as $k=>$v)
                          
                          @if(isset($v['add_on']['title']))
                              @foreach($v['add_on']['title'] as $key=>$val)
                                @if($val!="" && isset($v['add_on']['v_price'][$key]) && $v['add_on']['v_price'][$key]!="")
                                    @php
                                      $addondis=1;
                                    @endphp
                                @endif
                              @endforeach
                          @endif
                       

                    @endforeach
                @endif

                @if($addondis)  
                <tr>
                   
                   <td class="all-leval-hight">            
                      <span class="time-price">Add-on</span>
                   </td>

                   @if(isset($sellerprofile->information) && count($sellerprofile->information))
                      @foreach($sellerprofile->information as $k=>$v)
                          @if(isset($v['v_price']) && $v['v_price']!="")
                            <td class="all-tab-hight">
                            <form name="{{$k}}" id="{{$k}}" method="post" action="{{url('skill/buy-now')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="v_seller_plan" value="{{$k}}">
                            <input type="hidden" name="i_seller_profile_id" value="{{$sellerprofile->id}}">
                            @if(isset($v['add_on']['title']))
                                @foreach($v['add_on']['title'] as $key=>$val)
                                  @if($val!="" && isset($v['add_on']['v_price'][$key]) && $v['add_on']['v_price'][$key]!="")

                                  <div class="reg">
                                     <bdo dir="ltr">
                                        <input type="radio" class="one" value="{{$key}}" name="planaddon">
                                        <span></span>
                                        <abbr class="abberall">{{$val}} (+ £{{$v['add_on']['v_price'][$key]}}) </abbr>
                                     </bdo>
                                  </div>
                                  @endif
                                @endforeach
                            @endif
                            </form>
                            </td>
                         @endif
                      @endforeach
                  @endif
                </tr>
                @endif

                <tr>
                   <td class="all-leval-hight">            
                   </td>
                   
                    @if(isset($sellerprofile->information) && count($sellerprofile->information))
                      @foreach($sellerprofile->information as $k=>$v)
                          @if(isset($v['v_price']) && $v['v_price']!="")
                            <td class="all-tab-hight">
                              <div class="payment-progress">
                                 @if(!auth()->guard('web')->check() || auth()->guard('web')->user()->id!=$sellerprofile->i_user_id)
                                 <button class="btn btn-select" type="button" onclick="selectplan('{{$k}}')" >Select</button>
                                 @else
                                 <button class="btn btn-select" type="button">Select</button>
                                 @endif
                              </div>
                            </td>
                         @endif
                      @endforeach
                    @endif

                </tr>
             </tbody>
          </table>
          </div>
       </div>

       <div class="clearfix"></div>
       <div class="description-status">
          
          <div class="details-description descunder">
            
            <h3> Description </h3>

             @php
               if(isset($sellerprofile->l_brief_description)){
                  echo $sellerprofile->l_brief_description;
               }else{
                  echo "";
               }  
             @endphp
          </div>
       </div>


       @php
          $avgrw=0;
          if($review['total']!=0){
              $avgrw = $review['avgtotal']/$review['total'];
          }
       @endphp
       
       <div class="all-final-viwe reviews_block" id="reviews_block_online">
          <div class="rating-buyer rating-link review_align">
             <p>Reviews</p>
             <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$avgrw}}" readonly>
             <span class="star-no">{{number_format($avgrw,1)}} ({{isset($review['total']) ? $review['total'] :''}})</span>
          </div>

          @php
            $avgcommunication=0;
            if($review['total']!=0){
                $avgcommunication = $review['i_communication_star']/$review['total'];
            }
          @endphp

          <div class="row">
             <div class="col-sm-4 text-center">
                Seller Communication
                <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$avgcommunication}}" readonly >
             </div>

              @php
                $avgdescribed=0;
                if($review['total']!=0){
                    $avgdescribed = $review['i_described_star']/$review['total'];
                }
              @endphp
             
             <div class="col-sm-4 text-center">
                Service As Described 
                <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$avgdescribed}}" readonly >
             </div>

             @php
                $avgrecommend=0;
                if($review['total']!=0){
                    $avgrecommend = $review['i_recommend_star']/$review['total'];
                }
              @endphp

              <div class="col-sm-4 text-center">
                Would Recommend
                <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$avgrecommend}}" readonly >
             </div>

          </div>
       </div>

       <input type="hidden" name="reviewpage" id="reviewpage" value="1" autocomplete="off" />
         <div id="allreviewdata"></div>
         <div id="showmorebtn">
            <a href="javascript:;" onclick="SkillReview('{{$sellerprofile->id}}')" class="show-details"> Show More </a>
        </div>
    </div>

    <div class="col-sm-5">
       <div class="final-point-all">
          
          <div class="row">
             <div class="col-xs-7">
                <div class="packege-find">
                   <h1>{{isset($sellerprofile->v_profile_title) ? $sellerprofile->v_profile_title:''}}</h1>
                </div>
             </div>
             <div class="col-xs-5 text-right">
                <div class="rating-buyer rating-link rating_counts">
                   <a href="#reviews_block_online">
                   <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$avgrw}}" disabled>&nbsp;
                   {{number_format($avgrw,1)}} <span class="star-no">({{isset($review['total']) ? $review['total'] :''}})</span>
                   </a>
                </div>
             </div>
          </div>

         


          <div class="price-start">
             <p>STARTING AT 
              <span>£{{isset($sellerprofile->information['basic_package']['v_price']) ? $sellerprofile->information['basic_package']['v_price']:''}}
              </span>
              
              @if(isset($sellerprofile->v_hourly_rate) && $sellerprofile->v_hourly_rate!="")
               | HOURLY RATE 
              <span>£{{isset($sellerprofile->v_hourly_rate) ? $sellerprofile->v_hourly_rate:''}}</span>
              @endif

              @if(!auth()->guard('web')->check() || auth()->guard('web')->user()->id!=$sellerprofile->i_user_id)
              <a href="javascript:;" title="@if(isset($is_shortlisted) && $is_shortlisted == 1) shortlisted @else shortlist @endif" class="menulink menut{{$sellerprofile->_id}}" onclick="shortlist('{{$sellerprofile->_id}}')" >
                  @if(isset($is_shortlisted) && $is_shortlisted == 1)
                    <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_{{$sellerprofile->_id}}" class="right-love"/>
                  @else
                    <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_{{$sellerprofile->_id}}" class="right-love"/>
                  @endif  
              </a>
              @endif
            </p>
             <p class="detail-price">{{isset($sellerprofile->l_short_description) ? $sellerprofile->l_short_description:''}}</p>
          </div>
       </div>
        

        @if(isset($sellerprofile->information) && count($sellerprofile->information))
            @foreach($sellerprofile->information as $k=>$v)
                @if(isset($v['v_price']) && $v['v_price']!="")
                  <div class="accordion-final">
                      <div class="accordion accordion-play">
                         <h4 class="accordion-toggle @if($k=="basic_package") active @endif"  >£{{isset($v['v_price']) ? $v['v_price']:''}} {{isset($v['v_title']) ? $v['v_title'] : ''}}</h4>
                         <div class="accordion-content" @if($k=="basic_package") style="display: block;" @endif">
                            <ul class="green_stick">
                              @if(isset($v['v_bullets']))
                                @foreach($v['v_bullets'] as $data)
                                  @if($data!="" && $data!=null)
                                  <li>{{$data}}</li>
                                  @endif
                                @endforeach
                              @endif
                            </ul>
                            <form name="$v['v_title']" method="post" action="{{url('skill/buy-now')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="v_seller_plan" value="{{$k}}">
                            <input type="hidden" name="i_seller_profile_id" value="{{$sellerprofile->id}}">
                            @if(isset($v['add_on']['title']) && count($v['add_on']['title']))
                               @foreach($v['add_on']['title'] as $key=>$val)
                                  @if($val!="" && isset($v['add_on']['v_price'][$key]) && $v['add_on']['v_price'][$key]!="" )
                                      <div class="reg">
                                         <bdo dir="ltr">
                                            <input type="radio" class="one" value="{{$key}}" name="planaddon">
                                            <span></span>
                                            <abbr class="abberall">{{$val}} (+ £{{$v['add_on']['v_price'][$key]}})</abbr>
                                         </bdo>
                                      </div>
                                      <br>
                                  @endif
                               @endforeach
                            @endif
                            
                            @if(!auth()->guard('web')->check() || auth()->guard('web')->user()->id!=$sellerprofile->i_user_id)
                            <a href="{{url('skill/buy-now')}}/{{$sellerprofile->id}}/{{$k}}">
                            <div class="payment-progress">
                               <button class="btn btn-Proceed" type="submit">Proceed to Payment</button>
                            </div>
                            </a>
                            @else
                            <div class="payment-progress">
                               <button class="btn btn-Proceed" type="button">Proceed to Payment</button>
                            </div>
                            
                            @endif

                            </form>
                         </div>
                      </div>
                  </div>
               @endif
            @endforeach
        @endif
       <div class="clearfix"></div>
       
       @php
          $v_plan="";
          if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_plan)){
              $v_plan = $sellerprofile->hasUser()->v_plan;
          }
       @endphp

       
       <div class="all-icon-social">

          @if(isset($v_plan['id']) && $v_plan['id']!="5a65b48cd3e812a4253c9869")
          @if(isset($sellerprofile->v_facebook) && $sellerprofile->v_facebook!="")
              <h4>Visit me on</h4>
              <a href="https://www.facebook.com/{{$sellerprofile->v_facebook}}" class="facebook"><i class="zmdi zmdi-facebook"></i> Share</a>
          @endif
          @if(isset($sellerprofile->v_twitter) && $sellerprofile->v_twitter!="")
              <a href="https://twitter.com/{{$sellerprofile->v_twitter}}" class="twitter"><i class="zmdi zmdi-twitter"></i> Tweet</a>
          @endif
          @if(isset($sellerprofile->v_pinterest) && $sellerprofile->v_pinterest!="")
              <a href="https://in.pinterest.com/{{$sellerprofile->v_pinterest}}" class="pinterest"><i class="zmdi zmdi-pinterest"></i> Pin it</a>
          @endif
          @if(isset($sellerprofile->v_instagram) && $sellerprofile->v_instagram!="")
              <a href="https://www.instagram.com/{{$sellerprofile->v_instagram}}" class="share"><i class="zmdi zmdi-instagram"></i> Share</a>
          @endif
          @endif
       </div>

       <div class="online-main">
          <div class="online-img">  
            <?php 
                $v_image="";
                if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_image)){
                    $v_image=$sellerprofile->hasUser()->v_image;
                }

                $useridseller="";
                if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->id)){
                    $useridseller = $sellerprofile->hasUser()->id;
                }


                $v_fname="";
                if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_fname)){
                    $v_fname=$sellerprofile->hasUser()->v_fname;
                }

                $v_lname="";
                if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_lname)){
                    $v_lname=$sellerprofile->hasUser()->v_lname;
                }

                $v_name_main_skill="";
                if(count($sellerprofile->hasSkill()) && isset($sellerprofile->hasSkill()->v_name)){
                    $v_name_main_skill=$sellerprofile->hasSkill()->v_name;
                }


                $v_name_other_skill="";
                if(count($sellerprofile->hasOtherSkill()) && isset($sellerprofile->hasOtherSkill()->v_name)){
                    $v_name_other_skill=$sellerprofile->hasOtherSkill()->v_name;
                }

                $v_verified="";
                $i_verified=0;
                
                if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->e_email_confirm)){
                    $v_verified= ($sellerprofile->hasUser()->e_email_confirm  == 'yes') ? 'verified' :  'not verified' ;

                    if($sellerprofile->hasUser()->e_email_confirm  == 'yes'){
                        $i_verified=1;      
                    }
                }
               
                $availabel="";
                if(isset($sellerprofile->v_avalibility_from) && $sellerprofile->v_avalibility_from!=""){
                  $availabel = ucfirst(substr($sellerprofile->v_avalibility_from, 0, 3)) .' - ';
                }
                if(isset($sellerprofile->v_avalibility_to) && $sellerprofile->v_avalibility_to!=""){
                  $availabel .= ucfirst(substr($sellerprofile->v_avalibility_to, 0, 3)) .' / ';
                }

                if(isset($sellerprofile->v_avalibilitytime_from) && $sellerprofile->v_avalibilitytime_from!=""){
                  $availabel .= date("h a",strtotime($sellerprofile->v_avalibilitytime_from.':00')) .' - ';
                }
                
                if(isset($sellerprofile->v_avalibilitytime_to) && $sellerprofile->v_avalibilitytime_to!=""){
                  $availabel .= date("h a",strtotime($sellerprofile->v_avalibilitytime_to.':00')) ;
                }

                $v_replies_time="";
                if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_replies_time)){
                    $v_replies_time = $sellerprofile->hasUser()->v_replies_time;
                }

                $v_plan="";
                if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_plan)){
                    $v_plan=$sellerprofile->hasUser()->v_plan;
                }
            
             ?> 
              @php
              $imgdata = '';
              if(Storage::disk('s3')->exists($v_image)){
                  $imgdata = \Storage::cloud()->url($v_image);
              }else{
                  $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
              }
              @endphp  
             <img src="{{$imgdata}}" alt="" />
             @if(isset($leveldata) && count($leveldata))
             @php
                $levelcon="";
                if(isset($leveldata->v_icon) && $leveldata->v_icon!=""){
                    $levelcon = \Storage::cloud()->url($leveldata->v_icon);
                }
             @endphp
             @if($levelcon!="")
             <div class="starall">
                <img src="{{$levelcon}}" alt="" />
             </div>
             @endif
             @endif
          </div>
          @if(isset($leveldata) && count($leveldata))
          <button type="button" onclick="openleveldata()" class="btn btn-level-details"> {{isset($leveldata->v_title) ? $leveldata->v_title : ''}} </button>

            {{-- <div class="modal fade" id="leveltextview" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content modal-content-1">
                      <button type="button" class="close closemodal" data-dismiss="modal"></button>
                        <div class="modal-header modal-header-1">
                            <h4 class="modal-title"> {{isset($leveldata->v_text_public) ? $leveldata->v_text_public : ''}} </h4>
                        </div>
                        <div class="modal-footer modal-footer-1">
                        </div>
                    </div>
                </div>
            </div> --}}

            <div id="leveltextview" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title" style="text-align: center;">{{isset($leveldata->v_title) ? $leveldata->v_title : ''}}</h4>
                    </div>
                    <div class="modal-body">
                      <p>{{isset($leveldata->v_text_public) ? $leveldata->v_text_public : ''}}</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>

            <script type="text/javascript">
                function openleveldata(){
                    $("#leveltextview").modal("show");
                }
            </script>

          @endif
          
          <div class="name-man">
             {{$v_fname}} {{$v_lname}}
          </div>
          <div class="skill-details-man">
            <p> Main Skill: <span style="color: #337ab7">{{$v_name_main_skill}}</span> </p>
            <p> Other Skills: 
              <span style="color: #337ab7">{{$other_skills_data or ''}}</span>   
            </p>
          </div>
          <div class="final-man-txt">
             <p>{{isset($sellerprofile->l_short_description) ? $sellerprofile->l_short_description:''}} </p>
             <div class="rating-buyer rating-link">
                <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" readonly value="{{$avgrw}}">
                <span class="star-no"> {{number_format($avgrw,1)}} ({{$review['total']}})</span>

                {{-- <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$avgrw}}" disabled>&nbsp;
                   {{number_format($avgrw,1)}} <span class="star-no">({{isset($review['total']) ? $review['total'] :''}})</span> --}}
                   
             </div>
          </div>
          <div class="open-man">
              @if($availabel!="")
              <p>Open <span>{{$availabel}}</span></p>              
              @endif
             <p>English Proficiency <span style="text-transform: capitalize;">{{isset($sellerprofile->v_english_proficiency) ? $sellerprofile->v_english_proficiency:''}}</span></p>
          </div>
          <div class="all-time-free">
             <img src="{{url('public/Assets/frontend/images/clock-add.png')}}" alt="" style="max-width: 42px;"/>
             <p>Replies in {{$v_replies_time}}</p>
          </div>
          <div class="all-time-free">
             @if($i_verified)
             <img src="{{url('public/Assets/frontend/images/right-lot.png')}}" alt="" style="max-width: 42px;" />
             @else
             <img src="{{url('public/Assets/frontend/images/close-lot.png')}}" alt="" style="max-width: 42px;" />
             @endif
             <p>Profile {{$v_verified}}</p>
          </div>

          <div class="all-time-free">
             <img src="{{url('public/Assets/frontend/images/classification.png')}}" alt="" style="max-width: 42px;" />
             @php
              $start =strtotime($sellerprofile->hasUser()->d_added);
              $end =  time();
              $diff=$end - $start;
              $days_between = round($diff / 86400);
              if($days_between>30){
                  $days_between = round($days_between / 30);
                  $days_between = $days_between." Months";
              }else{
                if($days_between>1){
                    $days_between = $days_between." days";    
                }else{
                    $days_between = "1 day";
                }

              }
             @endphp
             <p>With Skillbox ( {{$days_between}} )</p>
          </div>
          @if(isset($v_plan['id']) && $v_plan['id']!="5a65b48cd3e812a4253c9869")
          <div class="web-name">
             <p>{{$sellerprofile->v_website}}</p>
             <p>{{$sellerprofile->v_contact_phone}}</p>
          </div>
          @endif  
          <div class="payment-progress">
            @if(!auth()->guard('web')->check() || auth()->guard('web')->user()->id!=$sellerprofile->i_user_id)
             <button class="btn btn-Proceed" onclick="SendMessage('{{$useridseller}}','{{$sellerprofile->id}}')">Contact Me</button>
            @else
            <button class="btn btn-Proceed">Contact Me</button> 
            @endif
          </div>
       </div>

        <div class="payment-progress">
         @if(!auth()->guard('web')->check() || auth()->guard('web')->user()->id!=$sellerprofile->i_user_id)
         <button class="btn btn-Proceed" onclick="reportListing('{{$useridseller}}')">Report this listing</button>
         @endif
      </div>


    </div>

   




    @if(isset($skilldata) && count($skilldata))

    <div class="col-xs-12">
       <div class="intrested-all maybe-intrested">
          <p>Services you may be interested in</p>
       </div>
       <div class="box-content-find">
         @foreach($skilldata as $k=>$v)

           @php
              $title =App\Helpers\General::TitleSlug($v['v_profile_title']);
          @endphp


          <div class="col-md-3 col-sm-4">
             <div class="detail-category ">
                
                <div class="category-img">
                  @if(isset($v['v_introducy_video']) && $v['v_introducy_video']!="")
                      @php
                          $videodata=\Storage::cloud()->url($v['v_introducy_video']);
                      @endphp

                      <a href="{{url('online')}}/{{$v['_id']}}/{{$title}}?id={{$v['_id']}}">
                      <video id="videoPlayergrid{{$k}}" width="100%" height="100%">
                          <source src="{{$videodata}}" type="video/mp4">
                      </video>
                      </a>
                      <img src="{{url('public/Assets/frontend/images/player.png')}}" onclick="playPausevideogrid('{{$k}}')" class="imgcontroler" id="videoControlergrid{{$k}}"  alt="" style="width: auto;height: auto;position: absolute;left: 0;right: 0;margin: auto;top: 3%;margin-right: 10px" /> 
                  @elseif(isset($v['v_work_photos']) && count($v['v_work_photos']))
                      @php
                          $imag=\Storage::cloud()->url($v['v_work_photos'][0]);
                      @endphp
                      <a href="{{url('online')}}/{{$v['_id']}}/{{$title}}?id={{$v['_id']}}">
                      <img src="{{$imag}}" alt="" />
                      </a>
                  @else
                      @php $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg'); @endphp
                      <a href="{{url('online')}}/{{$v['_id']}}/{{$title}}?id={{$v['_id']}}">
                      <img src="{{$imgdata}}" alt="" />
                      </a>
                  @endif
                </div>
               
                <div class="category-text">
                   <div style="min-height: 97px;max-height: 97px;overflow: hidden;">
                   <a href="{{url('online')}}/{{$v['_id']}}/{{$title}}?id={{$v['_id']}}">
                   <h2>{{isset($v['v_profile_title']) ? $v['v_profile_title'] : ''}}</h2>
                   </a>
                   <p>
                    @if(isset($v['l_short_description']))
                          @if(strlen($v['l_short_description'])>70)
                              @php
                              echo substr($v['l_short_description'],0,70).'...';
                              @endphp
                          @else
                              @php
                              echo $v['l_short_description'];
                              @endphp
                          @endif
                     @endif
                   </p>
                   </div>
                   <div class="star-category">
                    @if(isset($v['i_total_review']) && $v['i_total_review']>0)
                    <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                      @else
                      <img src="{{url('public/Assets/frontend/images/star-gray.png')}}" alt="" />
                      @endif
                      {{-- <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" /> --}}
                      <p class="text-star">{{isset($v['i_total_avg_review']) ? number_format($v['i_total_avg_review'],1) : '0.0'}}<span> ( {{isset($v['i_total_review']) ? $v['i_total_review'] : '0'}} )</span></p>
                   </div>
                </div>

                @if(isset($v['e_sponsor']) && $v['e_sponsor'] == 'yes')
                    <div class="sponsored-course">
                        <a href="#" class="btn btn-course">Sponsored AD</a>
                    </div>
                @endif
                <div class="starting-price">
                   
                   {{-- <div class="pull-left"> 
                      <img src="{{url('public/Assets/frontend/images/hart.png')}}" alt="" />
                   </div> --}}
                   
                   <div class="pull-left">
                      @if(in_array($v['_id'], $s_skill))
                          <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['_id']}}" onclick="shortlistdata('{{$v['_id']}}', 'skill')" >
                          <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" class="bg_{{$v['_id']}}" id="bg_{{$v['_id']}}" /></a>
                          <input type="hidden" name="img_click_{{$v['_id']}}" id="img_click_{{$v['_id']}}" value="1">
                      @else
                          <a href="javascript:;" title="shortlist" class="menulink menut{{$v['_id']}}" onclick="shortlistdata('{{$v['_id']}}', 'skill')" >
                          <img src="{{url('public/Assets/frontend/images/hartB.png')}}" class="bg_{{$v['_id']}}" id="bg_{{$v['_id']}}" /></a>
                          <input type="hidden" name="img_click_{{$v['_id']}}" id="img_click_{{$v['_id']}}" value="0">
                      @endif
                  </div>
                   <div class="pull-right">
                      <P>STARTING AT <span>£
                          @if(isset($v['information']['basic_package']) && count($v['information']['basic_package']))
                              {{$v['information']['basic_package']['v_price']}}
                          @endif 
                      </span></P>
                   </div>
                </div>
             </div>
          </div>
          @endforeach
       </div>
    </div>
    @endif

    <div style="clear: both"></div><br>
    <div id="commonmsgskill"></div>


 </div>
</div>
</div>


@stop
@section('js')


<script type="text/javascript">
  
  function reportListing(id){

    var actionurl = "{{url('message/userLogin/check')}}";
    var formData = "i_to_id="+id;
    $.ajax({
        processData: false,
        contentType: false,
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            document.getElementById('load').style.visibility='hidden';
            var obj = jQuery.parseJSON(res);
            if(obj['status']==1){
                $("#reportListingModal").modal("show");
            }else{
              window.location = "{{url('login')}}";
            }
        },
        error: function ( jqXHR, exception ) {
            document.getElementById('load').style.visibility='hidden';
            $("#successmsgpop").css("display","none");
            $("#errormsgpop").css("display","block");
            $("#sendMessageSuccess").modal("show");
            $("#sendMessage").modal("hide");
        }
    });

  }

  function SendMessage(id,pid){
    
    var actionurl = "{{url('message/userLogin/check')}}";
    var formData = "i_to_id="+id+"&from=buyer&pid="+pid;

    $.ajax({
        processData: false,
        contentType: false,
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            document.getElementById('load').style.visibility='hidden';
            var obj = jQuery.parseJSON(res);
            if(obj['status']==1){
                $("#contactsellerdetail").html(obj['responsestr']);
                $("#popupmessage").html(obj['msg']);
                $("#sendMessage").modal("show");
                
            }else{
              window.location = "{{url('login')}}";
            }
        },
        error: function ( jqXHR, exception ) {
            document.getElementById('load').style.visibility='hidden';
            $("#successmsgpop").css("display","none");
            $("#errormsgpop").css("display","block");
            $("#popupmessage").html("Something went wrong.please try again after sometime.");
            //$("#sendMessageSuccess").modal("show");
            $("#sendMessage").modal("hide");
        }
    });
  }
   function changedropdowndata(data){
      var appendstr="";
      if(data=="Images"){
          var appendstr ='<option value="">Select</option><option value="Doesnt_match_listing">Doesnt match listing</option><option value="Offensive_or_adult_content">Offensive or adult content</option><option value="Is_not_clear">Is not clear</option><option value="Other">Other</option>';
      }else if(data=="Listing_Name"){
          var appendstr ='<option value="">Select</option><option value="Different_from_listing">Different from listing</option><option value="Missing_information">Missing information</option><option value="Unimportant_information">Unimportant information</option><option value="Incorrect_information">Incorrect information</option><option value="Other">Other</option>';
      }else if(data=="Bullet_Points"){
          var appendstr ='<option value="">Select</option><option value="Different_from_listing">Different from listing</option><option value="Missing_information">Missing information</option><option value="Unimportant_information">Unimportant information</option><option value="Other">Other</option>';
      }else if(data=="Other_listing_details"){
          var appendstr ='<option value="">Select</option><option value="Price_issue">Price issue</option><option value="Missing_information">Missing information</option><option value="Conflicting_information">Conflicting information</option><option value="Listing_quality_issue">Listing quality issue</option><option value="Incorrect_information">Incorrect information</option>';
      }
      $("#v_about_issue_detail").html(appendstr);
   }

   function submitReport(){

      var actionurl = "{{url('message/sendReportAdmin')}}";
      var formValidFalg = $("#reportListingForm").parsley().validate('');
      
      if(formValidFalg){
        
          document.getElementById('load').style.visibility="visible";  
          var formData = new FormData($('#reportListingForm')[0]);

          $.ajax({
              processData: false,
              contentType: false,
              type    : "POST",
              url     : actionurl,
              data    : formData,
              success : function( res ){
                  
                  document.getElementById('load').style.visibility='hidden';
                  var obj = jQuery.parseJSON(res);
                  if(obj['status']==1){
                    $("#popupmessage").html(obj['msg']);
                    $("#sendMessageSuccess").modal("show");
                    $("#reportListingModal").modal("hide");
                  }else{
                    $("#popupmessage").html(obj['msg']);
                    $("#sendMessageSuccess").modal("show");
                    $("#sendMessage").modal("hide");
                  }
              },
              error: function ( jqXHR, exception ) {
                  document.getElementById('load').style.visibility='hidden';
                  $("#popupmessage").html("Something went wrong.please try again after sometime.");
                  $("#sendMessageSuccess").modal("show");
                  $("#reportListingModal").modal("hide");
              }
          });

      }

  }


  function submitMessage(type=""){
            
      var actionurl = "{{url('message/sendMessageSeller')}}";
      var formValidFalg = $("#sendMessageForm").parsley().validate('');
     
      if(formValidFalg){
        
          document.getElementById('load').style.visibility="visible";  
          var formData = new FormData($('#sendMessageForm')[0]);
          
          $.ajax({
              processData: false,
              contentType: false,
              type    : "POST",
              url     : actionurl,
              data    : formData,
              success : function( res ){
                  document.getElementById('load').style.visibility='hidden';
                  var obj = jQuery.parseJSON(res);
                  if(obj['status']==1){
                    $("#popupmessage").html(obj['msg']);
                    $("#sendMessageSuccess").modal("show");
                    $("#sendMessage").modal("hide");
                  }else{
                    $("#popupmessage").html(obj['msg']);
                    $("#sendMessageSuccess").modal("show");
                    $("#sendMessage").modal("hide");
                  }
              },
              error: function ( jqXHR, exception ) {
                  document.getElementById('load').style.visibility='hidden';
                  $("#popupmessage").html("Something went wrong.please try again after sometime.");
                  $("#sendMessageSuccess").modal("show");
                  $("#sendMessage").modal("hide");
              }
          });

      }    
  }


</script>



<script type="text/javascript">
 $(document).ready(function() {
     $('.accordion').find('.accordion-toggle').click(function() {
       $(this).next().slideToggle('600');
       $(".accordion-content").not($(this).next()).slideUp('600');
     });
     $('.accordion-toggle').on('click', function() {
       $(this).toggleClass('active').siblings().removeClass('active');
     });
 });
</script>

<script type="text/javascript">
 $(document).ready(function() {
     $('.resizing_select').change(function(){
      $("#width_tmp_option").html($('.resizing_select option:selected').text()); 
      $(this).width($("#width_tmp_select").width());  
     });
 });
</script>

<script type="text/javascript">
 $(document).ready(function(){
 $('.slider-for').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: true,
     fade: true,
     // asNavFor: '.slider-nav',
     adaptiveHeight: true
     });
     $('.slider-nav').slick({
     slidesToShow: 3,
     slidesToScroll: 1,
     asNavFor: '.slider-for',
     dots: false,
     centerMode: false,
     focusOnSelect: true,
     prevArrow: "<a href='#' class='prev3'></a>",
     nextArrow: "<a href='#' class='next3'></a>",
     responsive: [
                    {
                      breakpoint: 575,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                      }
                    },
                  ]
 });

 });    
</script>

<script>
 function move() {
   var elem = document.getElementById("myBar");   
   var width = 50;
   var id = setInterval(frame, 10);
   function frame() {
     if (width >= 100) {
       clearInterval(id);
     } else {
       width++; 
       elem.style.width = width + '%'; 
       document.getElementById("demo").innerHTML = width * 1  + '%';
     }
   }
 }

  function selectplan(data=""){
    $("#"+data).submit();
  }


    function shortlistdata(id="", type=""){
        
        var hidval = $("#img_click_"+id).val();
        var actionurl = "{{url('shortlist')}}";
        var formdata = "_id="+id+"&hidval="+hidval+"&type="+type; 

        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                var obj = jQuery.parseJSON(res);
                if(obj['status']==1){
                    $(".bg_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                    $(".menut"+id).attr('title',"shortlisted");
                    $("#img_click_"+id).val("1");
                    $("#commonmsgskill").html(obj['msg']);
                    $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                }    
                else if(obj['status']==2){
                    $(".bg_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                    $(".menut"+id).attr('title',"shortlist");
                    $("#img_click_"+id).val("0");
                    $("#commonmsgskill").html(obj['msg']);
                    $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                }
                else if(obj['status']==3){
                   window.location = "{{url('login')}}";
                }else{
                    $("#commonmsg").html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
            }
        });
    }

  
  function shortlist(id=""){
      var actionurl = "{{url('shortlist')}}";
      var formdata = "_id="+id+"&type=skill"; 

      $.ajax({
          type    : "GET",
          url     : actionurl,
          data    : formdata,
          success : function( res ){
            
            var obj = jQuery.parseJSON(res);
            if(obj['status']==1){
                $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                $(".menut"+id).attr('title',"shortlisted");
                $("#commonmsg").html(obj['msg']);
            }    
            else if(obj['status']==2){
                $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                $(".menut"+id).attr('title',"shortlist");
                $("#commonmsg").html(obj['msg']);
            }
            else if(obj['status']==3){
               window.location = "{{url('login')}}";
            }else{
                $("#commonmsg").html(obj['msg']);
            }
          },
          error: function ( jqXHR, exception ) {
          }
      });
  }


  function SkillReview(id=""){

      var page = $("#reviewpage").val();
      var actionurl = "{{url('skill/review')}}";
      var formdata = "id="+id+"&page="+page; 
      
      // page=parseInt(page)+1;
      // $("#reviewpage").val(page);

      document.getElementById('load').style.visibility="visible"; 
      $.ajax({
          type    : "GET",
          url     : actionurl,
          data    : formdata,
          cache: false,
          success : function( res ){
            var obj = jQuery.parseJSON(res);
            document.getElementById('load').style.visibility='hidden';
            if(obj['status']==1){
                
                $("#allreviewdata").append(obj['responsestr']);
                
                page=parseInt(page)+1;
                $("#reviewpage").val(page);

                // page=parseInt(page)+1; 
                // $("#reviewpage").val(obj['page']);

                if(obj['btnstatus']==0){
                    $("#showmorebtn").html("");
                }
            }  
            else{
                $("#commonmsg").html(obj['msg']);
            }
          },
          error: function ( jqXHR, exception ) {
          }
      });
  }
  $(function(){
    SkillReview('{{$sellerprofile->id}}');  
  });
  
  
  function playPausevideogrid(data) { 
           
        var video = document.getElementById("videoPlayergrid"+data);
        if (video.paused){
            video.play();
            if (video.play){
                $("#videoControlergrid"+data).attr('src',"{{url('public/Assets/frontend/images/pause.png')}}");
                //$("#videoControlergrid"+data).hide();
            }
        }
        else{
            video.pause();
            if (video.pause){
                $("#videoControlergrid"+data).attr('src',"{{url('public/Assets/frontend/images/player.png')}}");    
                //$("#videoControlergrid"+data).show();
            }
        }
  }


  function playPausevideogridSlider(data) { 
        
        $("video").each(function () { this.pause() });  
        $(".play_button_icon").show();

        var video = document.getElementById("videoPlayergridSlider"+data);
        if (video.paused){
            video.play();
            if (video.play){
                $("#videoControlergridSlider"+data).hide();
            }
        }
        else{
            video.pause();
            if (video.pause){
                $("#videoControlergridSlider"+data).show();
            }
        }
  }

  $(document).on('click', '.slick-next', function () {
      $("video").each(function () { this.pause() });
      $(".play_button_icon").show();
  });

  $(document).on('click', '.pausevideo', function () {
      $("video").each(function () { this.pause() });
      $(".play_button_icon").show();
  });
  $(document).on('click', '.slick-prev', function () {
      $("video").each(function () { this.pause() }); 
      $(".play_button_icon").show();   
  });

  

(function($) {
  $.fn.uncheckableRadio = function() {
    var $root = this;
    $root.each(function() {
      var $radio = $(this);
      if ($radio.prop('checked')) {
        $radio.data('checked', true);
      } else {
        $radio.data('checked', false);
      }
      $radio.click(function() {
        var $this = $(this);
        if ($this.data('checked')) {
          $this.prop('checked', false);
          $this.data('checked', false);
          $this.trigger('change');
        } else {
          $this.data('checked', true);
          $this.closest('form').find('[name="' + $this.prop('name') + '"]').not($this).data('checked', false);
        }
      });
    });
    return $root;
  };
}(jQuery));

$('[type=radio]').uncheckableRadio();
  
 
  // $("video").each(function () { this.pause() });


</script>

<script type="text/javascript">
 $(".one").click(function(){
  $(".section1").addClass("intro");
 });
</script>

 <script type="text/javascript">
                $('a[href^="#reviews_block_online"]').on('click', function(event) {
                    var target = $(this.getAttribute('href'));
                    if( target.length ) {
                        event.preventDefault();
                        $('html, body').stop().animate({
                            scrollTop: target.offset().top
                        }, 2000);
                    }
                });
          </script>

@stop 


