@extends('layouts.frontend')
@section('content')
<style type="text/css">
   .modal-new .modal-backdrop {z-index: 0;}
</style>

<div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="title-support">
                    <h1> Order Summery </h1>
                </div>
            </div>
        </div>
    </div>
<div class="container">
   <div class="main-edit-profile">
      <div class="edit-details" style="margin-top: 50px;">
         <div class="row">
            <form class="horizontal-form" role="form" method="POST" name="editprofileform" id="editprofileform" action="http://192.168.0.228/projects/skillbox/profile/update" data-parsley-validate="" enctype="multipart/form-data" novalidate="">
               <input type="hidden" name="_token" value="2qLjHCeuIK5y2xJFAbJVaMGnTX0LjpxZL1r36bNQ">    
               
               <div class="col-xs-12">
                  <div class="upload-details" style="margin: 0px auto;">
                     
                    <div class="row" style="margin: 5px 0px;">
                        <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
                           <label class="field-edit-name" style="margin-top: 5px;">
                            <b>User Plan subscription </b>: Standard Plan
                           </label>
                        </div>
                    </div>

                    <div class="row" style="margin: 5px 0px;">
                        <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
                           <label class="field-edit-name" style="margin-top: 5px;">
                            <b>Total Amount Payable </b>: $105
                           </label>
                        </div>
                    </div>

                    <div class="row" style="margin: 5px 0px;">
                        <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
                           <label class="field-edit-name" style="margin-top: 5px;">
                            <b>Plan Duration</b>: Yealy
                           </label>
                        </div>
                    </div>

                    <div style="margin: 15px 0px; text-align: center;">
                         <a href="http://192.168.0.228/projects/skillbox/dashboard">
                         <button type="button" class="btn form-save-exit"> Pay with PayPal </button>
                         </a>
                         <button type="submit" class="btn form-next">Pay with Escrow</button>
                    </div>

                  </div>
               </div>
            </form>
         </div>
      </div>
      
   </div>
</div>
@stop
@section('js')
<script type="text/javascript">

</script>
@stop