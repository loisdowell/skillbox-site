<link rel="stylesheet" href="{{url('public/Assets/frontend/star-rating/css/star-rating.css')}}" type="text/css" media="all">
<script src="{{url('public/Assets/frontend/star-rating/js/star-rating.js')}}"></script> 

@if(isset($type) && $type=="list")
    
    @if(isset($finalresult) && count($finalresult))
        @foreach($finalresult as $k=>$v)
            
            @if($v['type'] == "jobs")

                @php
                    $title =App\Helpers\General::TitleSlug($v['data']['v_job_title']);
                @endphp

                <div class="col-xs-12">
                    <div class="all-leval height_container @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start') gridviewbordersponseredjob  @endif">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="leval-img-sortng">
                                    
                                    @php
                                        $imgins="";
                                        if(isset($v['data']['v_photos'][0])){
                                            $imgins = $v['data']['v_photos'][0];
                                        } 
                                        $imgdata="";
                                        if($imgins!=""){
                                            $imgdata = $_S3_PATH.'/'.$imgins;    
                                        }else{
                                            $imgdata = $_S3_PATH.'/jobpost/photos/No_Image_Available.jpg';    
                                        }

                                        //$imgdata=$_S3_PATH.'/'.$imgins;
                                        // if(Storage::disk('s3')->exists($imgins)){
                                        //     $imgdata = \Storage::cloud()->url($imgins);
                                        // }else{
                                        //     $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                        // }  
                                    @endphp
                                     <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                    <img class="height_div" src="{{$imgdata}}" alt="" />
                                    </a>
                                   @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start')
                                   <div class="Sponsored-second">
                                        <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">Sponsored AD</a>
                                   </div>
                                   @endif
                                </div>
                            </div>

                            @if(isset($v['data']['e_urgent']) && $v['data']['e_urgent']=="yes")    
                                <div class="urgent1">
                                    <label>URGENT</label>
                                </div>
                            @endif
                            
                            <div class="col-sm-8 height_div">
                                <div class="center-courses">
                                    <div class="row">
                                        <div class="col-xs-10 no-padding">
                                            <div class="center-courses-text">
                                                
                                                <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                <span>{{isset($v['data']['v_job_title']) ? $v['data']['v_job_title'] : ''}}</span> 
                                                </a>

                                                <p>
                                                @php
                                                    $f_name="";
                                                    $l_name="";
                                                    if(isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])){
                                                        if(isset($user_data[$v['data']['i_user_id']]['v_fname'])){
                                                            $f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
                                                        }
                                                        if(isset($user_data[$v['data']['i_user_id']]['v_lname'])){
                                                            $l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
                                                        }
                                                    } 
                                                    echo $f_name.' '.$l_name; 
                                                 @endphp
                                                </p>
                                            </div>
                                        </div>
                                        
                                        <div class="col-xs-2">
                                            <div class="courses-icon">
                                                @if(in_array($v['data']['_id'], $s_job))
                                                    <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist_listview('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                    <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_listview_{{$v['data']['_id']}}" /></a>
                                                    <input type="hidden" name="img_click_listview_{{$v['data']['_id']}}" id="img_click_listview_{{$v['data']['_id']}}" value="1">

                                                @else
                                                    <a href="javascript:;" title="shortlist" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist_listview('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                    <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_listview_{{$v['data']['_id']}}" /></a>
                                                    <input type="hidden" name="img_click_listview_{{$v['data']['_id']}}" id="img_click_listview_{{$v['data']['_id']}}" value="0">
                                                @endif
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 no-padding">
                                            <div class="center-courses-text">
                                                <p>
                                               @if(isset($v['data']['l_short_description']))
                                                    @if(strlen($v['data']['l_short_description'])>280)
                                                        @php
                                                        echo substr($v['data']['l_short_description'],0,280).'...';
                                                        @endphp
                                                    @else
                                                        @php
                                                        echo $v['data']['l_short_description'];
                                                        @endphp
                                                    @endif
                                                    {{-- @php echo $v['data']['l_job_description']; @endphp --}}
                                               @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 no-padding text-center-xs">
                                            <label class="leval-all-seller">Job</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="courses-icon course-pricevalue">
                                                 @if(isset($v['data']['v_budget_type']) && $v['data']['v_budget_type']=='open_to_offers')
                                                <p>Open to Offers</p>
                                                @else
                                                <P>Budget Amount <span>£{{isset($v['data']['v_budget_amt']) ? $v['data']['v_budget_amt'] :'0'}}</span></P>
                                                @endif
                                            </div>
                                            <div class="rating-leval rating_inline">
                                                <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{number_format($v['data']['i_total_avg_review'],1)}}" readonly>
                                                <p> {{number_format($v['data']['i_total_avg_review'],1)}}<span class="point"> &nbsp;({{$v['data']['i_total_review']}})</span></p>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @else

                 @php
                    $title =App\Helpers\General::TitleSlug($v['data']['v_profile_title']);
                @endphp

                <div class="col-xs-12">
                    <div class="all-leval @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start') gridviewbordersponsered  @endif ">
                        <div class="row height_container">
                            <div class="col-sm-4 ">
                                
                                <div class="leval-img-sortng" >
                                @if(isset($v['data']['v_introducy_video']) && $v['data']['v_introducy_video']!="")
                                    @php
                                        //$videodata=\Storage::cloud()->url($v['data']['v_introducy_video']);
                                        $videodata=$_S3_PATH.'/'.$v['data']['v_introducy_video'];
                                    @endphp
                                    <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                    <video class="height_div" id="videoPlayerlist{{$k}}" width="100%">
                                        <source src="{{$videodata}}" type="video/mp4">
                                    </video>
                                    </a>
                                    <img src="{{url('public/Assets/frontend/images/player.png')}}" onclick="playPausevideo('{{$k}}')" class="imgcontroler" id="videoControlerlist{{$k}}"  alt="" style="width: auto;height: auto;position: absolute;left: 0;right: 0;margin: auto;top: 2%;margin-right: 10px" /> 

                                @elseif(isset($v['data']['v_work_photos']) && count($v['data']['v_work_photos']))
                                    @php
                                        //$imag=\Storage::cloud()->url($v['data']['v_work_photos'][0]);
                                        $imag=$_S3_PATH.'/'.$v['data']['v_work_photos'][0];

                                    @endphp
                                    <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                    <img class="height_div" src="{{$imag}}" alt="" />
                                    </a>
                                
                                @else
                                    @php 
                                        $imgdata=$_S3_PATH.'/jobpost/photos/No_Image_Available.jpg';
                                        //$imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg'); 
                                    @endphp
                                    <img class="height_div" src="{{$imgdata}}" alt="" />
                                
                                @endif

                                @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status']=="start")
                                <div class="Sponsored-second">
                                    <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">Sponsored AD</a>
                                </div>
                                @endif

                                </div>
                            </div>
                            
                            <div class="col-sm-8 height_div">
                                <div class="center-courses">
                                    <div class="row">
                                        <div class="col-xs-10 no-padding">
                                            <div class="center-courses-text">
                                                
                                                <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                                                <span>{{isset($v['data']['v_profile_title']) ? $v['data']['v_profile_title'] : ''}}</span>
                                                </a>
                                                <p>
                                                    @php
                                                        $f_name="";
                                                        $l_name="";
                                                        if(isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])){
                                                            
                                                            if(isset($user_data[$v['data']['i_user_id']]['v_fname'])){
                                                                $f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
                                                            }
                                                            if(isset($user_data[$v['data']['i_user_id']]['v_lname'])){
                                                                $l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
                                                            }
                                                        } 
                                                        echo ucfirst($f_name).' '.$l_name; 
                                                    @endphp
                                                 </p>
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="courses-icon">
                                                @if(in_array($v['data']['_id'], $s_skill))
                                                    <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist_listview('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                    <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_listview_{{$v['data']['_id']}}" /></a>
                                                    <input type="hidden" name="img_click_listview_{{$v['data']['_id']}}" id="img_click_listview_{{$v['data']['_id']}}" value="1">

                                                @else
                                                    <a href="javascript:;" title="shortlist" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist_listview('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                                    <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_listview_{{$v['data']['_id']}}" /></a>
                                                    <input type="hidden" name="img_click_listview_{{$v['data']['_id']}}" id="img_click_listview_{{$v['data']['_id']}}" value="0">
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 no-padding">
                                            <div class="center-courses-text">
                                                <p>@if(isset($v['data']['l_short_description']))
                                                    @if(strlen($v['data']['l_short_description'])>280)
                                                        @php
                                                            $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                                            $ldesc = str_replace("</strong>", " ", $ldesc);
                                                            echo substr($ldesc,0,280).'...';
                                                        @endphp
                                                    @else
                                                        @php
                                                            $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                                            $ldesc = str_replace("</strong>", " ", $ldesc);
                                                            echo $ldesc;
                                                        @endphp
                                                    @endif
                                                    {{-- @php 
                                                    $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                                    $ldesc = str_replace("</strong>", " ", $ldesc);
                                                    echo $ldesc;
                                                    @endphp --}}
                                               @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                        
                                    <div class="row">
                                        
                                        <div class="col-md-6 no-padding text-center-xs">
                                            <label class="leval-all-seller">Skill</label>
                                        </div>


                                       
                                        <div class="col-md-6">
                                            <div class="courses-icon course-pricevalue">
                                                <p>STARTING AT <span>
                                                    @if(isset($v['data']['information']['basic_package']) && count($v['data']['information']['basic_package']))
                                                          £{{$v['data']['information']['basic_package']['v_price']}}
                                                    @endif 
                                                </span></p>
                                            </div>
                                            <div class="rating-leval rating_inline">
                                                <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$v['data']['i_total_avg_review']}}" readonly>
                                                <p>{{number_format($v['data']['i_total_avg_review'],1)}}<span class="point">&nbsp;({{$v['data']['i_total_review']}})</span></p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            @endif
        @endforeach
    @endif


@else

@php
  $knt=0;      
@endphp
@if(isset($finalresult) && count($finalresult))
    @foreach($finalresult as $k=>$v)
        
        @if($v['type'] == "jobs")
            @php
                $title =App\Helpers\General::TitleSlug($v['data']['v_job_title']);
            @endphp

            <div class="col-sm-6  col-md-4 @if($knt==0) fresult @endif">
                <div class="all-category @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start') gridviewbordersponseredjob  @endif">
                    <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                    <div class="category-img">
                        @php
                            $imgins="";
                            if(isset($v['data']['v_photos'][0])){
                                $imgins = $v['data']['v_photos'][0];
                            } 
                            $imgdata="";
                            if($imgins!=""){
                                $imgdata = $_S3_PATH.'/'.$imgins;    
                            }else{
                                $imgdata = $_S3_PATH.'/jobpost/photos/No_Image_Available.jpg';    
                            }
                            //$imgdata = $_S3_PATH.'/'.$imgins;
                            // if(Storage::disk('s3')->exists($imgins)){
                            //     $imgdata = \Storage::cloud()->url($imgins);
                            // }else{
                            //     $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                            // }  
                        @endphp
                        <img src="{{$imgdata}}" alt="" />
                    </div>
                    </a>    
                    
                    @if(isset($v['data']['e_urgent']) && $v['data']['e_urgent']=="yes")    
                        <div class="urgent1">
                            <label>URGENT</label>
                        </div>
                    @endif

                    <div class="category-text">
                        <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                        <h2 style="min-height: 37px;max-height: 37px;overflow: hidden;">{{isset($v['data']['v_job_title']) ? $v['data']['v_job_title'] : ''}}</h2> 
                        </a>
                        <div class="gridview_txt">
                        <p style="min-height: 60px;max-height: 60px;overflow: hidden;margin: 10px 0px;">
                        
                        @if(isset($v['data']['l_short_description']))
                            @if(strlen($v['data']['l_short_description'])>70)
                                @php
                                    $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                    $ldesc = str_replace("</strong>", " ", $ldesc);
                                    echo substr($ldesc,0,70).'...';
                                @endphp
                            @else
                                @php
                                    $ldesc = str_replace("<strong>", "", $v['data']['l_short_description']);
                                    $ldesc = str_replace("</strong>", " ", $ldesc);
                                    echo $ldesc;
                                @endphp
                            @endif
                       @endif

                        </p>
                        </div>

                        <div class="star-category">
                            @if($v['data']['i_total_review']>0)
                            <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                            @else
                            <img src="{{url('public/Assets/frontend/images/star-gray.png')}}" alt="" />
                            @endif
                            {{-- <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" /> --}}
                            <p class="text-star">
                                {{isset($v['data']['i_total_avg_review']) ? number_format($v['data']['i_total_avg_review'],1) : ''}}
                                <span>({{isset($v['data']['i_total_review']) ? $v['data']['i_total_review'] : ''}})</span>
                            </p>
                        </div>
                    </div>

                    @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start')
                        <div class="sponsored-course">
                            <a href="{{url('online-job')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}" class="btn btn-course-job">Sponsored AD</a>
                        </div>
                    @endif
                    <span class="text-right defineresult">Job</span>
                    <div class="starting-price starting-price-job">
                        <div class="pull-left">
                            @if(in_array($v['data']['_id'], $s_job))
                                <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_{{$v['data']['_id']}}" /></a>
                                <input type="hidden" name="img_click_{{$v['data']['_id']}}" id="img_click_{{$v['data']['_id']}}" value="1">
                            @else
                                <a href="javascript:;" title="shortlist" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_{{$v['data']['_id']}}" /></a>
                                <input type="hidden" name="img_click_{{$v['data']['_id']}}" id="img_click_{{$v['data']['_id']}}" value="0">
                            @endif
                        </div>
                        <div class="pull-right">
                            @if(isset($v['data']['v_budget_type']) && $v['data']['v_budget_type']=='open_to_offers')
                            <p>Open to Offers</p>
                            @else
                            <P>Budget Amount <span>£{{isset($v['data']['v_budget_amt']) ? $v['data']['v_budget_amt'] :'0'}}</span></P>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @else

            @php
                $title =App\Helpers\General::TitleSlug($v['data']['v_profile_title']);
            @endphp

            <div class="col-sm-6 col-md-4 @if($knt==0) fresult @endif">
                <div class="all-category @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == 'start') gridviewbordersponsered  @endif">
                    
                    <div class="category-img category_video" >
                    @if(isset($v['data']['v_introducy_video']) && $v['data']['v_introducy_video']!="")
                        @php
                            $videodata = $_S3_PATH.'/'.$v['data']['v_introducy_video'];
                            //$videodata=\Storage::cloud()->url($v['data']['v_introducy_video']);
                        @endphp
                        <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                        <video class="playcontroler" id="videoPlayergrid{{$k}}" width="100%" height>
                            <source src="{{$videodata}}" type="video/mp4">
                        </video>
                        </a>
                        <img src="{{url('public/Assets/frontend/images/player.png')}}" class="imgcontroler" id="videoControlergrid{{$k}}"  onclick="playPausevideogrid('{{$k}}')"  alt="" style="width: auto;height: auto;position: absolute;left: 0;right: 0;margin: auto;top: 3%;margin-right: 10px" /> 
                    @elseif(isset($v['data']['v_work_photos']) && count($v['data']['v_work_photos']))
                        @php
                            $imag = $_S3_PATH.'/'.$v['data']['v_work_photos'][0];
                            //$imag=\Storage::cloud()->url($v['data']['v_work_photos'][0]);
                        @endphp
                        <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                        <img src="{{$imag}}" alt="" />
                        </a>
                    @else
                        @php 
                        $imgdata = $_S3_PATH.'/jobpost/photos/No_Image_Available.jpg'; 
                        //$imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg'); 
                        @endphp
                        <img src="{{$imgdata}}" alt="" />
                    @endif
                    </div>

                    <div class="category-text">
                        <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}">
                        <h2 style="min-height: 37px;max-height: 37px;overflow: hidden;">{{isset($v['data']['v_profile_title']) ? $v['data']['v_profile_title'] : ''}}</h2>
                        </a>
                        <p style="min-height: 60px;max-height: 60px;overflow: hidden;margin: 10px 0px;">
                             @if(isset($v['data']['l_short_description']))
                                @if(strlen($v['data']['l_short_description'])>70)
                                    @php
                                        $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                        $ldesc = str_replace("</strong>", " ", $ldesc);
                                        echo substr($ldesc,0,70).'...';
                                    @endphp
                                @else
                                    @php
                                        $ldesc = str_replace("<strong>", " ", $v['data']['l_short_description']);
                                        $ldesc = str_replace("</strong>", " ", $ldesc);
                                        echo $ldesc;
                                    @endphp
                                @endif
                           @endif
                        </p>
                        <div class="star-category">
                        @if($v['data']['i_total_review']>0)
                        <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                        @else
                        <img src="{{url('public/Assets/frontend/images/star-gray.png')}}" alt="" />
                        @endif
                            {{-- <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" /> --}}
                            <p class="text-star">{{number_format($v['data']['i_total_avg_review'],1)}} <span>({{$v['data']['i_total_review']}})</span></p>
                        </div>
                    </div>

                    @if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status']=="start")
                    <div class="sponsored-course">
                        <a href="{{url('online')}}/{{$v['data']['_id']}}/{{$title}}?id={{$v['data']['_id']}}" class="btn btn-course">Sponsored AD</a>
                    </div>
                    @endif
                    <span class="text-right defineresult">Skill</span>
                    <div class="starting-price">
                        <div class="pull-left">
                            @if(in_array($v['data']['_id'], $s_skill))
                                <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_{{$v['data']['_id']}}" /></a>
                                <input type="hidden" name="img_click_{{$v['data']['_id']}}" id="img_click_{{$v['data']['_id']}}" value="1">
                            @else
                                <a href="javascript:;" title="shortlist" class="menulink menut{{$v['data']['_id']}}" onclick="shortlist('{{$v['data']['_id']}}', '{{$v['type']}}')" >
                                <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_{{$v['data']['_id']}}" /></a>
                                <input type="hidden" name="img_click_{{$v['data']['_id']}}" id="img_click_{{$v['data']['_id']}}" value="0">
                            @endif
                        </div>
                        <div class="pull-right">
                            <P>STARTING AT <span>
                                @if(isset($v['data']['information']['basic_package']) && count($v['data']['information']['basic_package']))
                                      £{{$v['data']['information']['basic_package']['v_price']}}
                                @endif 
                            </span></P>
                        </div>
                    </div>

                </div>
            </div>
        @endif

        @php
          $knt=$knt+1;      
        @endphp
    @endforeach
             
@endif

@endif

<script type="text/javascript">
    var src1 = "{{url('public/Assets/frontend/images/star.png')}}";
    var src2 = "{{url('public/Assets/frontend/images/star1.png')}}";
    $("#star-input-1,#star-input-2,#star-input-3,#star-input-4,#star-input-5").rating({
        min: 0,
        max: 5,
        step: 0.5,
        showClear: false,
        showCaption: false,
        theme: 'krajee-fa',
        filledStar: '<i class="fa"><img src="'+src1+'"></i>',
        emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
    });

    $("#f_rate_instruction,#f_rate_navigate,#f_rate_reccommend").rating({
        min: 0,
        max: 5,
        step: 0.5,
        showClear: false,
        showCaption: false,
        theme: 'krajee-fa',
        filledStar: '<i class="fa"><img src="'+src1+'"></i>',
        emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
    });
 </script>