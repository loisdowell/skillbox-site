@extends('layouts.frontend')
<?php 
// echo "<pre>";
// print_r($skill);
// exit;

?>
@section('content')

   <!--slide-containt-->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">

                <img src="{{url('public/Assets/frontend/images/filtaring.png')}}" class="img-responsive" alt="" />
                <div class="center-containt">
                    <div class="container">
                        <div class="filtaring-page">
                            <h1>gardening</h1>
                            <p>You’ve got the idea, now make it official with the perfect garden designs</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--end-slide-containt-->

    <!-- 04B-filtering-results-in person-list view-skills -->
    <div class="container">
        <div class="sort-for">
            <div class="sort-forep">
                  <a href="javascript:;" onclick="viewchange('grid')">
                    <img src="{{url('public/Assets/frontend/images/fore.png')}}" class="fore-all" alt="" />
                </a>
                <a href="javascript:;" onclick="viewchange('list')">
                    <img src="{{url('public/Assets/frontend/images/fore2.png')}}" alt="" />
                </a>
            </div>
        </div>
        <div class="filtaring-main">
            <div class="row">
                <div class="col-sm-3">
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Type</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox" type="checkbox" checked="">
                            <label for="checkbox">
                                Skills (<span>6</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox1" type="checkbox" checked="">
                            <label for="checkbox1">
                                Jobs (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning" style="visibility: hidden;">
                            <input id="checkbox2" type="checkbox" checked="">
                            <label for="checkbox2">
                                Courses (<span>1</span>)
                            </label>
                        </div>
                    </div>
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Experience</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox3" type="checkbox" checked="">
                            <label for="checkbox3">
                                Entry (<span>6</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox4" type="checkbox" checked="">
                            <label for="checkbox4">
                                Intermediate (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox5" type="checkbox" checked="">
                            <label for="checkbox5">
                                Expert (<span>1</span>)
                            </label>
                        </div>
                    </div>
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Language</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox6" type="checkbox" checked="">
                            <label for="checkbox6">
                                English (<span>6</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox7" type="checkbox" checked="">
                            <label for="checkbox7">
                                Spanish (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox8" type="checkbox" checked="">
                            <label for="checkbox8">
                                French (<span>1</span>)
                            </label>
                            <select class="more-info">
                                <option>More</option>
                            </select>
                        </div>
                    </div>
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Location</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox9" type="checkbox" checked="">
                            <label for="checkbox9">
                                Birmingham (<span>6</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox10" type="checkbox" checked="">
                            <label for="checkbox10">
                                Oxford (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox11" type="checkbox" checked="">
                            <label for="checkbox11">
                                Birmingham (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox12" type="checkbox" checked="">
                            <label for="checkbox12">
                                Wiltshire (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox13" type="checkbox" checked="">
                            <label for="checkbox13">
                                French (<span>1</span>)
                            </label>
                            <select class="more-info">
                                <option>More</option>
                            </select>
                        </div>
                    </div>
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Price</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox14" type="checkbox" checked="">
                            <label for="checkbox14">
                                Under £50
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox15" type="checkbox" checked="">
                            <label for="checkbox15">
                                £50 - £100
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox16" type="checkbox" checked="">
                            <label for="checkbox16">
                                £100 - £200
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox17" type="checkbox" checked="">
                            <label for="checkbox17">
                                £250 - £350
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox18" type="checkbox" checked="">
                            <label for="checkbox18">
                                £400 +
                            </label>
                        </div>
                    </div>
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Radius</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox19" type="checkbox" checked="">
                            <label for="checkbox19">
                                5 Miles
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox20" type="checkbox" checked="">
                            <label for="checkbox20">
                                10 Miles
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox21" type="checkbox" checked="">
                            <label for="checkbox21">
                                15 Miles
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox22" type="checkbox" checked="">
                            <label for="checkbox22">
                                25 Miles
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox23" type="checkbox" checked="">
                            <label for="checkbox23">
                                50 Miles
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 no-padding">
                    <div class="select-box">
                        <div class="col-sm-4">
                            <select class="form-control form-sel">
                                <option>Select Category</option>
                                <option>Select-1</option>
                                <option>Select-2</option>
                                <option>Select-3</option>
                                <option>Select-4</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control form-sel">
                                <option>Select Subcategory</option>
                                <option>Select-1</option>
                                <option>Select-2</option>
                                <option>Select-3</option>
                                <option>Select-4</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control form-sel">
                                <option>Sort by Relevance</option>
                                <option>Select-1</option>
                                <option>Select-2</option>
                                <option>Select-3</option>
                                <option>Select-4</option>
                            </select>
                        </div>
                    </div>

                      <div id="gridview" >
                        <div class="box-content-find">
                            
                            @if(isset($skill) && count($skill))
                                @foreach($skill as $k=>$v)

                                     <div class="col-sm-4">
                                        <div class="all-category all-cate-results">
                                            <div class="category-img category-img-results">
                                                
                                                @php
                                                    $imgins=$imgdata="";
                                                    if(isset($v['type']) && $v['type'] == 'skill'){
                                                        
                                                        $imgins = isset($skill_users[$v['i_user_id']]->v_image) ? $skill_users[$v['i_user_id']]->v_image : '' ;
                                                    }else if(isset($v['type']) && $v['type'] == 'job'){
                                                        $imgins = (isset($v['v_photos']) && count($v['v_photos'])) ? $v['v_photos'][0] : '' ;    
                                                       
                                                    }   

                                                    if(Storage::disk('s3')->exists($imgins)){
                                                        $imgdata = \Storage::cloud()->url($imgins);
                                                    }else{
                                                        $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                                    }
                                                    // print_r($imgdata);
                                                    // exit; 
                                                 @endphp   
                                                <img src="{{$imgdata}}" alt="" />
                                            </div>
                                            <div class="category-text">

                                                <?php 
                                                if(isset($v['type']) && $v['type'] == 'skill'){
                                                    $url = 'online';
                                                } else if(isset($v['type']) && $v['type'] == 'job'){
                                                    $url = 'online-job';
                                                }
                                                ?>
                                                <a href="{{url($url)}}/{{$v['_id']}}">
                                                 <?php 

                                                  $title = '';
                                                  if(isset($v['type']) && $v['type'] == 'skill'){
                                                    $title = isset($v['v_profile_title']) ? $v['v_profile_title']: '';
                                                  } else if(isset($v['type']) && $v['type'] == 'job'){ 
                                                    $title = isset($v['v_job_title']) ? $v['v_job_title']: '';
                                                  }
                                                 ?>    
                                                <h2>{{$title}}</h2>
                                                </a>
                                                <div class="gridview_txt">
                                                <p> 
                                               
                                                <?php 

                                                  $l_short_description = '';
                                                  if(isset($v['type']) && $v['type'] == 'skill'){

                                                    $l_short_description = substr(isset($v['l_short_description']) ? $v['l_short_description'] : '' , 0, 110);
                                                  } 
                                                  else if(isset($v['type']) && $v['type'] == 'job'){ 
                                                    $l_short_description = substr(isset($v['l_job_description']) ? $v['l_job_description'] : '' , 0, 110);
                                                  }
                                                 ?>  
                                                {{$l_short_description or ''}}    </p>
                                                </div>
                                                <div class="star-category">
                                                    <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                                                    <p class="text-star">4.9 <span>( 34 )</span></p>
                                                </div>
                                            </div>

                                            @if(isset($v['e_sponsor']) && $v['e_sponsor'] == 'yes')
                                            <div class="sponsored-course">
                                                <a href="#" class="btn btn-course">Sponsored AD</a>
                                            </div>
                                            @endif

                                            <div class="starting-price">
                                                <div class="pull-left">
                                                    <?php 
                                                        $s_data = array();
                                                        if(isset($v['type']) && $v['type'] == 'skill'){
                                                            $s_data = $s_skill;
                                                        }else if(isset($v['type']) && $v['type'] == 'job'){
                                                            $s_data = $s_job;
                                                        }

                                                    ?>
                                                    @if(in_array($v['_id'], $s_data))
                                                        <a href="javascript:;" title="Switch" class="menulink" onclick="shortlist('{{$v['_id']}}', '{{$v['type']}}')" >
                                                        <img src="{{url('public/Assets/frontend/images/hartA.png')}}" id="bg_{{$v['_id']}}" /></a>
                                                        <input type="hidden" name="img_click_{{$v['_id']}}" id="img_click_{{$v['_id']}}" value="1">
                                                    
                                                    @else

                                                    <a href="javascript:;" title="Switch" class="menulink" onclick="shortlist('{{$v['_id']}}', '{{$v['type']}}')" >
                                                    <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_{{$v['_id']}}" /></a>
                                                    <input type="hidden" name="img_click_{{$v['_id']}}" id="img_click_{{$v['_id']}}" value="0">
                                                    
                                                    @endif
                                                </div>
                                                <div class="pull-right">
                                                    <P> STARTING AT 
                                                        <span> 
                                                            @if(isset($v['information']['basic_package']) && count($v['information']['basic_package']))
                                                                  {{$v['information']['basic_package']['v_price']}}

                                                            @else 
                                                            
                                                                {{isset($v['v_budget_amt']) ? $v['v_budget_amt']:''}}        
                                                            @endif   
                                                        </span>
                                                    </P>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                @endforeach
                            @else
                                <div class="col-sm-12">
                                    <div class="all-category all-cate-results">
                                        
                                        <div class="category-text">
                                            <h2 style="text-align:center;">Courses data can not found.</h2>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>

                            @endif
                        </div>
                    </div>
                   
                    <div id="listview" style="display: none;">
                          @if(count($skill))
                            @foreach($skill as $k=>$v)

                                <div class="col-xs-12">
                                <div class="all-leval">
                                    <div class="row">
                                        
                                        <div class="col-sm-4">
                                            <div class="leval-img-sortng">
                                                <?php 
                                                    
                                                    if(isset($v['type']) && $v['type'] == 'skill'){
                                                         $img = isset($skill_users[$v['i_user_id']]->v_image) ? $skill_users[$v['i_user_id']]->v_image : '' ;
                                                         $url_img = 'public/uploads/userprofiles/'.$img;
                                                    } else if(isset($v['type']) && $v['type'] == 'job'){
                                                         $img1 = (isset($v['v_photos']) && count($v['v_photos'])) ? $v['v_photos'][0] : '' ;
                                                         $url_img = 'public/uploads/userprofiles/'.$img1;
                                                    }
                                                   
                                                ?>
                                                <img src="{{url($url_img)}}" alt="" />
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-8">
                                            <div class="center-courses">
                                                <div class="row">
                                                    <div class="col-xs-10 no-padding">
                                                        <div class="center-courses-text">
                                                            <?php 
                                                            $url = '';
                                                            if(isset($v['type']) && $v['type'] == 'skill'){
                                                                $url = 'online';
                                                            } else if(isset($v['type']) && $v['type'] == 'job'){
                                                                $url = 'online-job';
                                                            }
                                                            ?>
                                                            <a href="{{url($url)}}/{{$v['_id']}}"><span>

                                                            <?php 

                                                              $title = '';
                                                              if(isset($v['type']) && $v['type'] == 'skill'){
                                                                $title = isset($v['v_profile_title']) ? $v['v_profile_title']: '';
                                                              } else if(isset($v['type']) && $v['type'] == 'job'){ 
                                                                $title = isset($v['v_job_title']) ? $v['v_job_title']: '';
                                                              }
                                                             ?>        
                                                            {{$title}}

                                                            </span></a>
                                                            <p>
                                                            <?php 
                                                            if(isset($v['type']) && $v['type'] == 'skill'){
                                                                $f_name = isset($skill_users[$v['i_user_id']]->v_fname) ? $skill_users[$v['i_user_id']]->v_fname : '' ;
                                                                $l_name = isset($skill_users[$v['i_user_id']]->v_lname) ? $skill_users[$v['i_user_id']]->v_lname : ''; 

                                                                echo $f_name.' '.$l_name; 
                                                            }
                                                            else if(isset($v['type']) && $v['type'] == 'job'){
                                                                $f_name = isset($job_users[$v['i_user_id']]->v_fname) ? $job_users[$v['i_user_id']]->v_fname : '' ;
                                                                $l_name = isset($job_users[$v['i_user_id']]->v_lname) ? $job_users[$v['i_user_id']]->v_lname : ''; 

                                                                echo $f_name.' '.$l_name; 
                                                            }
                                                            ?>
                                                                
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-2">
                                                        <div class="courses-icon">
                                                            <?php 
                                                                $s_data = array();
                                                                if(isset($v['type']) && $v['type'] == 'skill'){
                                                                    $s_data = $s_skill;
                                                                }else if(isset($v['type']) && $v['type'] == 'job'){
                                                                    $s_data = $s_job;
                                                                }

                                                            ?>
                                                             @if(in_array($v['_id'], $s_data))
                                                                <a href="javascript:;" title="Switch" class="menulink" onclick="shortlist_listview('{{$v['_id']}}', '{{$v['type']}}')" >
                                                                <img src="{{url('public/Assets/frontend/images/hartA.png')}}" id="bg_listview_{{$v['_id']}}" /></a>
                                                                <input type="hidden" name="img_click_listview_{{$v['_id']}}" id="img_click_listview_{{$v['_id']}}" value="1">

                                                            @else
                                                                <a href="javascript:;" title="Switch" class="menulink" onclick="shortlist_listview('{{$v['_id']}}', '{{$v['type']}}')" >
                                                                <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_listview_{{$v['_id']}}" /></a>
                                                                <input type="hidden" name="img_click_listview_{{$v['_id']}}" id="img_click_listview_{{$v['_id']}}" value="0">

                                                            @endif
                                                           {{--  <a href="javascript:;" title="Switch" class="menulink" onclick="shortlist_listview('{{$v['_id']}}', '{{$v['type']}}')" >
                                                            <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_listview_{{$v['_id']}}" /></a>
                                                            <input type="hidden" name="img_click_listview_{{$v['_id']}}" id="img_click_listview_{{$v['_id']}}" value="0"> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 no-padding">
                                                        <div class="center-courses-text">
                                                            <p>
                                                            <?php 

                                                              $l_short_description = '';
                                                              if(isset($v['type']) && $v['type'] == 'skill'){

                                                                $l_short_description = substr(isset($v['l_short_description']) ? $v['l_short_description'] : '' , 0, 110);
                                                              } 
                                                              else if(isset($v['type']) && $v['type'] == 'job'){ 
                                                                $l_short_description = substr(isset($v['l_job_description']) ? $v['l_job_description'] : '' , 0, 110);
                                                              }
                                                             ?>  
                                                            {{$l_short_description or ''}}  
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6 no-padding text-center-xs">
                                                        <label class="leval-all-seller">level 1 seller</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="courses-icon course-pricevalue">
                                                            <p>STARTING AT <span>£
                                                             @if(isset($v['information']['basic_package']) && count($v['information']['basic_package']))
                                                                  {{$v['information']['basic_package']['v_price']}} 
                                                             @else 
                                                            
                                                                {{isset($v['v_budget_amt']) ? $v['v_budget_amt']:''}}        
                                                             @endif       
                                                                
                                                            </span></p>
                                                        </div>
                                                        <div class="rating-leval">
                                                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="5" required>
                                                            <p>4.9 <span class="point">( 34 )</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach
                         
                        @else
                             <div class="col-sm-12">
                                <div class="all-category all-cate-results">
                                    
                                    <div class="category-text">
                                        <h2 style="text-align:center;">Courses data can not found.</h2>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-04B-filtering-results-in person-list view-skills-->

@stop


@section('js')


<script type="text/javascript">
   
    function shortlist_listview(id="", type=""){
        var hidval = $("#img_click_listview_"+id).val();
        var actionurl = "{{url('shortlist-skill')}}";
        var formdata = "_id="+id+"&hidval="+hidval+"&type="+type; 

        $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                      
                    if(obj['status']==1){

                       if(hidval==0){
                           $("#bg_listview_"+id).attr('src',"{{url('public/Assets/frontend/images/hartA.png')}}");
                           $("#img_click_listview_"+id).val("1");
                       }else{
                           $("#bg_listview_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                           $("#img_click_listview_"+id).val("0");
                       } 
                       
                    }else{
                       window.location.replace("{{'login'}}");
                    }
                },
                error: function ( jqXHR, exception ) {
                    // $("#errormsg").show();
                 }
            });
    }

    function shortlist(id="", type=""){
        var hidval = $("#img_click_"+id).val();
        var actionurl = "{{url('shortlist-skill')}}";
        var formdata = "_id="+id+"&hidval="+hidval+"&type="+type; 

        $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                      
                    if(obj['status']==1){

                       if(hidval==0){
                           $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/hartA.png')}}");
                           $("#img_click_"+id).val("1");
                       }else{
                           $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                           $("#img_click_"+id).val("0");
                       } 
                       
                    }else{
                       window.location.replace("{{'login'}}");
                    }
                },
                error: function ( jqXHR, exception ) {
                    // $("#errormsg").show();
                 }
            });
    }

</script>



<script type="text/javascript">



function viewchange(data) {
  
    if(data=="grid"){
        $("#listview").css("display","none");
        $("#gridview").css("display","inline");
    }else{  
        $("#listview").css("display","block");
        $("#gridview").css("display","none");
    }


}

function masterfilter(){
    $("#searchmaster").submit();
}




</script>

@stop

