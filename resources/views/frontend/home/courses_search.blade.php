@extends('layouts.frontend')
<?php 
?>
@section('content')
<style type="text/css">
    .clearboth:nth-child(3n+1){
        clear: both;
    }
</style>

@php
    $experience=array();
    if(isset($coursesLevel) && count($coursesLevel)){
        foreach ($coursesLevel as $k=>$v){
            $experience[$v->id]['name']=$v->v_title;
            $experience[$v->id]['cnt']=0;
         }   
    }

    $language=array();
    if(isset($coursesLanguage) && count($coursesLanguage)){
        foreach ($coursesLanguage as $k=>$v){
            $language[$v->id]['name']=$v->v_title;
            $language[$v->id]['cnt']=0;
         }   
    }
    

    $islangfilter=0;     
    $ispricefilter=0;
    
    if(isset($coursessidebar) && count($coursessidebar)){
        foreach($coursessidebar as $k=>$v){
            if(isset($experience[$v->i_level_id])){
                $islangfilter=1;
                $experience[$v->i_level_id]['cnt']=$experience[$v->i_level_id]['cnt']+1;
            }else{
                $experience[$v->i_level_id]['cnt']=1;
            }
            // if(isset($language[$v->i_language_id])){
            //     $language[$v->i_language_id]['cnt']=$language[$v->i_language_id]['cnt']+1;
            // }
            if(isset($v->f_price) && $v->f_price>50){
                $ispricefilter = 1;
            }


        }
    }

    if(isset($coursessidebar1) && count($coursessidebar1)){
        foreach($coursessidebar1 as $k=>$v){
            if(isset($language[$v->i_language_id])){
                $language[$v->i_language_id]['cnt']=$language[$v->i_language_id]['cnt']+1;
            }
        }
    }

    // if(isset($courses) && count($courses)){
    //     foreach($courses as $k=>$v){
    //         if(isset($language[$v->i_language_id])){
    //             $language[$v->i_language_id]['cnt']=$language[$v->i_language_id]['cnt']+1;
    //         }
    //     }
    // }

@endphp

 <!--slide-containt-->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">
                @php
                    $v_banner_image="";
                    if($metaDetails['v_image'] && $metaDetails['v_image']!=""){
                        if(Storage::disk('s3')->exists($metaDetails['v_image'])){
                            $v_banner_image = \Storage::cloud()->url($metaDetails['v_image']);      
                        }else{
                            $v_banner_image = url('public/Assets/frontend/images/filtaring.png');
                        }                
                    }else{
                        $v_banner_image = url('public/Assets/frontend/images/filtaring.png');
                    }
                @endphp

                <img src="{{$v_banner_image}}" class="img-responsive" alt=""  />
                <div class="center-containt">
                    <div class="container">
                        <div class="filtaring-page">
                            @if($metaDetails['v_title'] && $metaDetails['v_title']!="")
                            <h1>{{$metaDetails['v_title']}}</h1>
                            @else
                            <h1>Course search</h1>
                            @endif
                            @if($metaDetails['v_sub_headline'] && $metaDetails['v_sub_headline']!="")
                            <p style="margin-top: 25px;">{{$metaDetails['v_sub_headline']}}</p>
                            @else
                            <p style="margin-top: 25px;">You’ve got the idea, now make it official with the perfect garden designs</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-slide-containt-->


    <!-- 04B-filtering-results-courses -->
    <div class="container">
        <div class="sort-for">
            <div class="sort-forep">
                <a href="javascript:;" onclick="viewchange('grid')">
                    <img  id="gridicon" src="{{url('public/Assets/frontend/images/fore.png')}}" class="fore-all" alt="" />
                </a>
                <a href="javascript:;" onclick="viewchange('list')">
                    <img id="listicon" src="{{url('public/Assets/frontend/images/fore2.png')}}" alt="" />
                </a>
            </div>
        </div>


        <div class="filtaring-main">
            <div class="row">
                @php
                    // dump($experience);
                @endphp

                <div class="col-sm-3">
                    <form method="GET" name="searchcoursesside" id="searchcoursesside" action="{{url('search')}}" >
                        <div class="all-type">
                            <div class="name-of-type">
                                <span>Experience</span>
                            </div>
                            @if(isset($experience) && count($experience))    
                                @foreach($experience as $k=>$v)
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox{{$k}}" value="{{$k}}" type="checkbox" name="i_level_id_sidebar[]" onchange="masterfiltermain()" @if(isset($data['i_level_id_sidebar']) && in_array($k, $data['i_level_id_sidebar'])) checked @endif  >
                                        <label for="checkbox{{$k}}">
                                            {{$v['name']}} (<span>{{$v['cnt']}}</span>)
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                            @if(isset($data['i_level_id_sidebar']) && $data['i_level_id_sidebar']!="")
                            <a href="javascript:;" onclick="cleardata('i_level_id_sidebar')">Clear Filter</a>
                            @endif

                        </div>
                        @if($islangfilter)    
                        <div class="all-type">
                            <div class="name-of-type">
                                <span>Language</span>
                            </div>
                            @php
                              $cnt=5;  
                            @endphp
                             @if(isset($language) && count($language))    
                                @foreach($language as $k=>$v)
                                    @if($v['cnt']!="0")
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox{{$cnt}}" value="{{$k}}" class="checkboxdata" type="checkbox" name="i_language_id[]" onchange="masterfilter()" @if(isset($data['i_language_id']) && in_array($k,$data['i_language_id'])) checked @endif >
                                        <label for="checkbox{{$cnt}}">
                                            @php
                                                $lngname =strtolower($v['name']);    
                                                $lngname = ucfirst($lngname);
                                            @endphp
                                            {{$lngname}} (<span>{{$v['cnt']}}</span>)
                                        </label>
                                    </div>
                                    @endif
                                    @php
                                      $cnt=$cnt+1;  
                                    @endphp
                                @endforeach

                            @endif

                            @if(isset($data['i_language_id']) && $data['i_language_id']!="")
                            <a href="javascript:;" onclick="cleardata('i_language_id')">Clear Filter</a>
                            @endif
                        </div>
                        @endif
                       
                        {{-- @if($ispricefilter) --}}
                        <div class="all-type" @if($ispricefilter) @else style="display: none;" @endif id="pricefilterdiv">
                            
                            <div class="name-of-type">
                                <span>Price</span>
                            </div>

                            @if(isset($pricefilter) && count($pricefilter))    
                                @foreach($pricefilter as $k=>$v)
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox{{$v->id}}" class="checkboxdata" value="{{$v->id}}" type="checkbox" name="i_price_id[]" onchange="masterfilter()" @if(isset($data['i_price_id']) && in_array($v->id, $data['i_price_id'])) checked @endif >
                                        <label for="checkbox{{$v->id}}">
                                            <span>{{$v->v_title}}</span>
                                        </label>
                                    </div>
                                @endforeach
                            @endif

                            @if(isset($data['i_price_id']) && count($data['i_price_id']))
                            <a href="javascript:;" onclick="cleardata('i_price_id')">Clear all</a>
                            @endif
                        </div>
                        {{-- @endif --}}
                    </form>

                </div>

                <div class="col-sm-9 no-padding">
                    
                    <form class="horizontal-form" role="form" method="GET" name="searchcoursesmaster" id="searchcoursesmaster" action="{{url('search')}}" >
                    
                    <div class="select-box">
                        
                        <input type="hidden" name="e_type" value="courses">
                        <input type="hidden" name="keyword" value="{{$keyword}}">
                            
                        <div class="col-sm-4">
                            <select class="form-control form-sel" name="i_category_id" onchange="masterfiltertop()">
                                <option value="">Select Category</option>
                                @if(isset($coursesCategory) && count($coursesCategory))
                                    @foreach($coursesCategory as $k=>$v)
                                        <option value="{{$v->id}}" @if(isset($data['i_category_id']) && $data['i_category_id']==$v->id) selected @endif>{{$v->v_title}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="col-sm-4">
                            <select class="form-control form-sel" name="i_level_id" onchange="masterfiltertop()">
                                <option value="">Select Level</option>
                                @if(isset($coursesLevel) && count($coursesLevel))
                                    @foreach($coursesLevel as $k=>$v)
                                        <option value="{{$v->id}}" @if(isset($data['i_level_id']) && $data['i_level_id']==$v->id) selected @endif>{{$v->v_title}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        
                        <div class="col-sm-4">
                            <select class="form-control form-sel" name="relevance" onchange="masterfiltertop()">
                                <option value="">Sort by Relevance</option>
                                <option value="latest" @if(isset($data['relevance']) && $data['relevance']=='latest') selected @endif>Newest First</option>
                                <option value="toprated" @if(isset($data['relevance']) && $data['relevance']=='toprated') selected @endif>Top Rated</option>
                                <option value="lowtohigh" @if(isset($data['relevance']) && $data['relevance']=='lowtohigh') selected @endif>Price: Low to High</option>
                                <option value="hightolow" @if(isset($data['relevance']) && $data['relevance']=='hightolow') selected @endif>Price: High to Low</option>
                                <option value="sponsored" @if(isset($data['relevance']) && $data['relevance']=='sponsored') selected @endif>Sponsored</option>
                            </select>
                        </div>

                    </div>
                    </form>

                     <div style="clear: both"></div>
                    <div id="commonmsg" style="margin-top: 10px"></div>
                    @php $cidsarray=array();  @endphp
                    <div id="gridview" >
                        <div class="box-content-find" id="box-content-grid">
                            @if(isset($courses) && count($courses))
                                @foreach($courses as $k=>$v)
                                    @php $cidsarray[]=$v->id; @endphp 

                                    @php
                                        $title =App\Helpers\General::TitleSlug($v->v_title);
                                    @endphp

                                    <div class="col-sm-4">
                                        <div class="all-category @if(isset($v->e_sponsor_status) && $v->e_sponsor_status == 'start') all-cate-results @endif">
                                            <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}">
                                            <div class="category-img category-img-results">
                                                 @php
                                                    $imgins="";
                                                    if(isset($v->v_course_thumbnail) && $v->v_course_thumbnail!=""){
                                                        $imgins = $v->v_course_thumbnail;
                                                    }

                                                    $imgdata=$_S3_PATH.'/'.$imgins
                                                 @endphp   
                                                <img src="{{$imgdata}}" alt="" />
                                            </div>
                                            </a>
                                            <div class="category-text">
                                                <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}">
                                                <h2 style="min-height: 36px;max-height: 36px;overflow: hidden;">{{isset($v->v_title) ? $v->v_title:''}}</h2>
                                                </a>
                                                <p style="min-height: 45px;max-height: 45px;overflow: hidden;">
                                                   @if(isset($v->v_sub_title))
                                                        @if(strlen($v->v_sub_title)>55)
                                                            {{substr($v->v_sub_title,0,55)}}...
                                                        @else
                                                            {{$v->v_sub_title}}
                                                        @endif
                                                   @endif
                                                <div class="star-category">
                                                    @if($v->i_total_avg_review>0)
                                                    <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                                                    @else
                                                    <img src="{{url('public/Assets/frontend/images/star-gray.png')}}" alt="" />           
                                                    @endif
                                                    <p class="text-star">{{number_format($v->i_total_avg_review,1)}}   
                                                            &nbsp;<span>({{$v->i_total_review}})</span></p>
                                                </div>
                                            </div>  
                                           
                                            @if(isset($v->e_sponsor_status) && $v->e_sponsor_status == 'start')
                                            <div class="sponsored-course">
                                                <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}" class="btn btn-course">Sponsored AD</a>
                                            </div>
                                            @endif

                                            <div class="starting-price">
                                                <div class="pull-left">

                                                    @if(in_array($v->_id, $s_course))
                                                        <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['_id']}}" onclick="shortlist('{{$v['_id']}}')" >
                                                        <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_{{$v['_id']}}" /></a>
                                                        <input type="hidden" name="img_click_{{$v['_id']}}" id="img_click_{{$v['_id']}}" value="1">

                                                    @else
                                                       <a href="javascript:;" title="shortlist" class="menulink menut{{$v['_id']}}" onclick="shortlist('{{$v['_id']}}')" >
                                                        <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_{{$v['_id']}}" /></a>
                                                        <input type="hidden" name="img_click_{{$v['_id']}}" id="img_click_{{$v['_id']}}" value="0">

                                                    @endif
                                                </div>
                                                <div class="pull-right">
                                                    
                                                    @if(isset($v->f_price) && $v->f_price!="0")
                                                    <P> COURSE PRICE 
                                                    <span>£{{isset($v->f_price) ? $v->f_price:''}}</span>
                                                    @else
                                                    <P>  
                                                    FREE COURSE
                                                    @endif
                                                    </P>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    
                                @endforeach
                            @else

                                <div class="no-skill-added">
                                    <div class="category-list">
                                        <div class="bottom-border">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12 noskill-heading" >
                                                    <div class="noskill-title text-center" style="padding-left: 20px;">
                                                    <h5>We have not found any results for your search. Please try a different search term.</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                    
                                {{-- <div class="col-sm-12">
                                    <div class="all-category all-cate-results">
                                        
                                        <div class="category-text">
                                            <h2 style="text-align:center;">We Didn't Find Any Results For Your Search.</h2>
                                            <p></p>
                                        </div>
                                    </div>
                                </div> --}}

                            @endif
                        </div>
                    </div>

                    <div id="listview" class="listview_courses_search d" style="display: none;">
                        @if(isset($courses) && count($courses))
                            @foreach($courses as $k=>$v)

                                @php
                                    $title =App\Helpers\General::TitleSlug($v->v_title);
                                @endphp

                                <div class="col-xs-12">
                                    <div class="all-leval @if(isset($v->e_sponsor_status) && $v->e_sponsor_status == 'start') listviewbordersponsered @endif">
                                        <div class="row height_container">
                                            <div class="col-sm-4">
                                                <div class="leval-img-sortng">
                                             <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}">
                                                    @php
                                                     $imgins="";
                                                    if(isset($v->v_course_thumbnail) && $v->v_course_thumbnail!=""){
                                                        $imgins = $v->v_course_thumbnail;
                                                    }
                                                    $imgdata=$_S3_PATH.'/'.$imgins
                                                    // if(Storage::disk('s3')->exists($imgins)){
                                                    //     $imgdata = \Storage::cloud()->url($imgins);
                                                    // }else{
                                                    //     $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                                    // }
                                                 @endphp   
                                                    <img class="height_div" src="{{$imgdata}}"  alt="" />
                                                </a>
                                                    @if(isset($v->e_sponsor_status) && $v->e_sponsor_status == 'start')
                                                        <div class="sponsored-course" style="top: 87%;">
                                                            <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}" class="btn btn-course">Sponsored AD</a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm-8 height_div">
                                                <div class="center-courses">
                                                
                                                    <div class="row">
                                                        <div class="col-xs-10 no-padding">
                                                            <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}">
                                                            <div class="center-courses-text">
                                                                <span>{{isset($v->v_title) ? $v->v_title:''}}</span>
                                                                <p>{{isset($v->v_author_name) ? $v->v_author_name:''}}</p>
                                                            </div>
                                                            </a>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <div class="courses-icon">
                                                                @if(in_array($v->_id, $s_course))
                                                                    <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['_id']}}" onclick="shortlist_listview('{{$v['_id']}}')" >
                                                                    <img src="{{url('public/Assets/frontend/images/hartA.png')}}" id="bg_listview_{{$v['_id']}}" /></a>
                                                                    <input type="hidden" name="img_click_listview_{{$v['_id']}}" id="img_click_listview_{{$v['_id']}}" value="1">

                                                                @else
                                                                   <a href="javascript:;" title="shortlist" class="menulink menut{{$v['_id']}}" onclick="shortlist_listview('{{$v['_id']}}')" >
                                                                <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_listview_{{$v['_id']}}" /></a>
                                                                <input type="hidden" name="img_click_listview_{{$v['_id']}}" id="img_click_listview_{{$v['_id']}}" value="0">

                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 no-padding">
                                                            <div class="center-courses-text">
                                                                <p>
                                                                    @if(isset($v->v_sub_title))
                                                                        @if(strlen($v->v_sub_title)>150)
                                                                            {{substr($v->v_sub_title,0,150)}}...
                                                                        @else
                                                                            {{$v->v_sub_title}}
                                                                        @endif
                                                                   @endif

                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 no-padding">
                                                            <ul class="list-leval">
                                                                <li><span>{{isset($v->section) ? count($v->section):'0'}}</span>Lectures</li>
                                                                |
                                                                <li><span>{{isset($v->i_duration) ? $v->i_duration:''}}</span>Hours Duration</li>
                                                                |
                                                                <li><span>{{count( $v->hasLevel() ) ? $v->hasLevel()->v_title : ''}}</span>Level</li>
                                                                |
                                                                <li>{{count( $v->hasCategory() ) ? $v->hasCategory()->v_title : ''}} </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="courses-icon course-pricevalue">
                                                                @if(isset($v->f_price) && $v->f_price!="0")
                                                                <P> COURSE PRICE 
                                                                <span>£{{isset($v->f_price) ? $v->f_price:''}}</span>
                                                                </P>
                                                                @else
                                                                <P>  
                                                                FREE COURSE
                                                                @endif
                                                                </P>
                                                            </div>
                                                            <div class="rating-leval">

                                                                <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$v->i_total_avg_review}}" required>
                                                                <p> {{number_format($v->i_total_avg_review,1)}}
                                                                <span class="point">({{$v->i_total_review}})
                                                                  </span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             @endforeach
                        @else
                              <div class="no-skill-added">
                                <div class="category-list">
                                    <div class="bottom-border">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12 noskill-heading" >
                                                <div class="noskill-title text-center" style="padding-left: 20px;">
                                                <h5>We have not found any results for your search. Please try a different search term.</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        @endif
                    </div>


                </div>
            </div>
        </div>
    </div>
    
    <?php 
        $cidsdata="";
        if(count($cidsarray)){
            $cidsdata = implode(",", $cidsarray);
        }
    ?>
    <input type="hidden" name="cidsdata" value="{{$cidsdata}}" id = "cidsdata" autocomplete="off" />

@stop


@section('js')

<script type="text/javascript">
    
    $(window).scroll(function () {
        //if($(window).scrollTop() == $(document).height() - $(window).height()) {
        if($(window).scrollTop() + window.innerHeight >= document.body.scrollHeight) {     
            var actionurl = "{{url('Loadmorecourse')}}";
            
            var mdata=$("#searchcoursesmaster").serialize();
            var sdata=$("#searchcoursesside").serialize();
            var formdata = mdata+"&"+sdata;
           
            var cidsdata = $("#cidsdata").val();
            formdata = formdata+"&cidsdata="+cidsdata;
            document.getElementById('load').style.visibility="visible";
            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    document.getElementById('load').style.visibility='hidden';
                    if(obj['status']==1){
                        $("#box-content-grid").append(obj['gridviewtext']);
                        $("#listview").append(obj['listviewtext']);
                        if(obj['ispricefilter']==1){
                            $("#pricefilterdiv").css("display","block");
                        }
                        $("#cidsdata").val(obj['cids']);
                    }else{
                    }
                },
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                }
            });

        }
    });

</script>


<script type="text/javascript">
    
@if(isset($_REQUEST['i_category_id']))
    var datahe=$(".filtaring-main").offset().top-100;
    $('html, body').animate({
        scrollTop: datahe
    }, 1000);
@endif

    // $("input:checkbox").on('click', function() {
    //   var $box = $(this);
    //   if ($box.is(":checked")) {
    //     var group = "input:checkbox[name='" + $box.attr("name") + "']";
    //     $(group).prop("checked", false);
    //     $box.prop("checked", true);
    //   } else {
    //     $box.prop("checked", false);
    //   }
    // });

</script>


<script type="text/javascript">
    
    function shortlist(id=""){

        var hidval = $("#img_click_"+id).val();
        var actionurl = "{{url('shortlist-course')}}";
        var formdata = "_id="+id+"&hidval="+hidval; 
        $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);

                    if(obj['status']==1){

                       if(hidval==0){
                           $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                           $("#bg_listview_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                           $(".menut"+id).attr('title',"shortlisted");
                           $("#img_click_"+id).val("1");
                       }else{
                           $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                           $("#bg_listview_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                           $("#img_click_"+id).val("0");
                           $(".menut"+id).attr('title',"shortlist");
                       } 
                       $("#commonmsg").html(obj['msg']);
                       
                    }else if (obj['status']==0){
                         window.location.replace("{{'login'}}");
                       
                    }
                },
                error: function ( jqXHR, exception ) {
                    // $("#errormsg").show();
                 }
            });
    }

     function shortlist_listview(id=""){
        var hidval = $("#img_click_listview_"+id).val();
        var actionurl = "{{url('shortlist-course')}}";
        var formdata = "_id="+id+"&hidval="+hidval; 

        $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                      
                    if(obj['status']==1){

                       if(hidval==0){
                           $("#bg_listview_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                           $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/yellow-heart.png')}}");
                           $(".menut"+id).attr('title',"shortlisted");
                           $("#img_click_listview_"+id).val("1");
                       }else{
                           $("#bg_listview_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                           $("#bg_"+id).attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                           $(".menut"+id).attr('title',"shortlist");
                           $("#img_click_listview_"+id).val("0");
                       } 
                       $("#commonmsg").html(obj['msg']);
                       
                    }else{
                       window.location.replace("{{'login'}}");
                    }
                },
                error: function ( jqXHR, exception ) {
                    // $("#errormsg").show();
                }
            });
    }

   
</script>


<script type="text/javascript">

function viewchange(data) {
    
    var imgpath = "{{url('public/Assets/frontend/images')}}";

    if(data=="grid"){
        $("#listview").css("display","none");
        $("#gridview").css("display","inline");

        var gridicon = imgpath+'/fore.png';
        var listicon = imgpath+'/fore2.png';
        $("#gridicon").attr("src",gridicon);
        $("#listicon").attr("src",listicon);

    }else{  
        $("#listview").css("display","block");
        $("#gridview").css("display","none");
        var gridicon = imgpath+'/fore33.png';
        var listicon = imgpath+'/fore22.png';
        $("#gridicon").attr("src",gridicon);
        $("#listicon").attr("src",listicon);
    }
}

function masterfilter(){
    
    var mdata=$("#searchcoursesmaster").serialize();
    var sdata=$("#searchcoursesside").serialize();

    window.location.href = "{{url('search')}}?"+mdata+"&"+sdata;
    //$("#searchcoursesmaster").submit();
}

function masterfiltertop(){
    
    //$('input[type="checkbox"]').prop("checked", false);
    var mdata=$("#searchcoursesmaster").serialize();
    var sdata=$("#searchcoursesside").serialize();

    window.location.href = "{{url('search')}}?"+mdata+"&"+sdata;
    //$("#searchcoursesmaster").submit();
}

function masterfiltermain(){
    
    //$('.checkboxdata').prop("checked", false);
    var mdata=$("#searchcoursesmaster").serialize();
    var sdata=$("#searchcoursesside").serialize();

    window.location.href = "{{url('search')}}?"+mdata+"&"+sdata;
    //$("#searchcoursesmaster").submit();
}
  
function cleardata(data){
    $('input[name="'+data+'[]"]').prop("checked", false);
    masterfilter();
}
    

$( document ).ready(function() {
    var s1 = $('.same-height-two').height();
    var s2 = $('.same-height').height();

    if (s1 > s2)
        $('.same-height-two').css('height', s1 + "px");
    else
        $('.same-height').css('height', s2 + "px");
});


</script>

@stop

