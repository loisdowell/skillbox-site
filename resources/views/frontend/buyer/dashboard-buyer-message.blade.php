      
      <div class="mainorder" id="mainordermessage">
      @if(isset($buyermessage) && count($buyermessage))
        @foreach($buyermessage as $k=>$v)
            @php
                $cnt = $k++;
                $v_fromimage="";
                if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_image)){
                    $v_fromimage=$v->hasFromUser()->v_image;
                }
               
                if($v_fromimage!=""){
                    $v_fromimage = \Storage::cloud()->url($v_fromimage);
                }else{
                    $v_fromimage = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                }
                
                $v_fromfname="";
                if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_fname)){
                    $v_fromfname=$v->hasFromUser()->v_fname;
                }
                $v_fromlname="";
                if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_lname)){
                    $v_fromlname=$v->hasFromUser()->v_lname;
                }
            @endphp

            <!-- <div class="clearfix"></div> -->
            <div class="media media_msg p-3 dashed_border message-first ">
                 <h2 style="display: none;">{{strtotime($v->d_added)}}</h2>
                <div class="media-left">
                  <img src="{{$v_fromimage}}" alt="" style="width: 60px;">
                </div>
                <div class="media-body">
                    <a href="{{url('message/buyer/message-centre')}}?mid={{$v->id}}"">
                        <h4 class="media-heading">{{$v->v_subject_title}}</h4>
                    </a>
                    @php 
                        $humanTime=$v->d_added;   
                        $humanTime = date("Y-m-d H:i:s",strtotime($humanTime));
                        // $humanTime = \Carbon\Carbon::createFromTimeStamp(strtotime($humanTime))->diffForHumans();
                        // if($v->d_added!="" && $v->d_added){
                        //     $humanTime = \Carbon\Carbon::createFromTimeStamp(strtotime($v->d_added))->diffForHumans();
                        // }
                        //$humanTime = \Carbon\Carbon::createFromTimeStamp(strtotime($v->d_added))->diffForHumans();   
                        //$humanTime=$v->d_added;
                    @endphp

                    @php 
                        $humanTime= App\Helpers\General::time_elapsed_string(strtotime($v->d_added));
                    @endphp
                  <p><span>From: </span>{{$v_fromfname}} {{$v_fromlname}}<span class="time-buyer">{{date("h:i a",strtotime($v->d_added))}} ({{$humanTime}})</span></p>
                  <div class="buyer-tital-text">
                            <p>
                                @if(isset($v->l_message))
                                    @if(strlen($v->l_message)>200)
                                        {{substr($v->l_message,0,200)}}...
                                    @else
                                        {{$v->l_message}}
                                    @endif
                               @endif

                            
                            </p>
                        </div>
                </div>
            </div>
            </div>

           @endforeach
           
           @else

            <div class="buyer-first message-first">
                <h2 style="display: none;">1</h2>
                <div class="buyer-rating">
                    <center><h3>No message found.</h3></center>
                </div>
            </div>
          
        @endif   
        </div>
