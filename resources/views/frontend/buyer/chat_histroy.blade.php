@extends('layouts.frontend')

@section('css')

<style type="text/css">
    .chat-history .input-group .form-control {
        height: 40px;
        border-radius: 0px !important;
    }
</style>
@stop

@section('content')

  <!-- 26B-control-panel-buyer-account-chat-history-1  -->
    <div class="modal fade" id="sendMessageSuccess" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content modal-content-1">
                
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> Message </h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <div id="successmsgpop" style="display: none;">
                              <div class="alert alert-success alert-big alert-dismissable br-5">
                                <center>
                                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>succesfully send message<span></span>
                                </center>
                              </div>
                            </div>

                            <div id="errormsgpop" style="display: none;">
                              <div class="alert alert-danger alert-big alert-dismissable br-5">
                                <center>
                                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>Something went wrong.please try again after sometime<span></span>
                                </center>
                              </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="stb-btn">
                                    <center>
                                    <button  type="button" data-dismiss="modal" class="btn btn-Submit-pop">Close</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
</div>

    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1>Message Centre </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="chat-history">
            <div class="row">
                <div class="col-xs-12 col-sm-6 no-padding-right pr-15 border-right">
                    
                    <div class="chat-filter">
                        <h3 class="chatnow_heading">Inbox</h3>
                        <div class="select-filter">
                            <span>Filter by:</span>&nbsp;
                            <select style="width: auto !important" class="resizing_select arr_img" name="v_order_by" id="v_order_by" onchange="dashboardMessageOrder()">
                                <option value="DESC" selected>Latest First</option>
                                <option value="ASC">Oldest First</option>
                            </select>
                            {{-- <select id="width_tmp_select" style>
                                <option id="width_tmp_option">Latest First</option>
                            </select> --}}
                            {{-- <div class="Filter-img">
                                <img src="{{url('public/Assets/frontend/images/select.png')}}" alt="" />
                            </div> --}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- <div class="scrollbar-outer scrollbar_1"> -->
                    <div class="scrollbar-outer scrollbar_1">   
                    <div id="dashboardmessagedata" class="scrolldata"></div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function(){
                            $('.scrollbar_1').scrollbar();
                        });
                    </script>
                        
                    <!-- </div> -->
                    <div class="clearfix"></div>

                    <input type="hidden" name="messagepage" id="messagepage" value="0" autocomplete="off" />
                    <div id="showmorebtnmessage">            
                    <div class="load-profile chat-first">
                        <button class="btn btn-load" onclick="dashboardMessage();">
                            Load More Messages (<span id="msgcntdata">1</span>)
                        </button>
                    </div>
                    </div>

                </div>
                
                <div class="col-xs-12 col-sm-6 no-padding-left chat_send border-left">
                    <div class="name-of-chat ">
                        <h3 id="nameother">Start conversation</h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="scrollbar-outer scrollbar_2">
                        <div class="chat-padd scrolldata" id="chatdetail" ></div>
                    </div>
                    
                    <div class="clearfix"></div>
                    <style type="text/css">
                        .chat-history .input-group .form-control{height: 40px;border-radius: 50px;}
                    </style>
                    <div id="chatnow" class="chatnow_div" style="display: none;">
                      <form name="sendmessageform" id="sendmessageform" data-parsley-validate enctype="multipart/form-data" >
                      {{ csrf_field() }}
                      <input type="hidden" name="i_parent_id" id="i_parent_id_data">
                     
                      <div class="your-field input-group" >
                        <textarea type="textarea" class="form-control " name="l_message" id="l_message_data" placeholder="Enter your message" required></textarea>
                        <span class="input-group-addon"><button type="button" onclick="sendMessage()" class="btn btn-guide">Send</button></span>
                      </div>

                      </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-buyer-account-chat-history-1  -->

@stop
@section('js')



<script type="text/javascript">
    $(document).ready(function(){
        $('.scrollbar_2').scrollbar();
    });
</script>
 <script type="text/javascript">
       
        $('#l_message_data').on('paste input', function () {
            if ($(this).outerHeight() > this.scrollHeight){
                $(this).height(1)
            }
            while ($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))){
                $(this).height($(this).height() + 1)
            }
        });

        $(".one").click(function() {
            $(".section1").addClass("intro");
        });

        function dashboardMessageOrder(){
            var v_order_by = $("#v_order_by").val();
            if(v_order_by=="ASC"){
                var $divs = $("div.buyer-first");
                var numericallyOrderedDivs = $divs.sort(function (a, b) {
                    return $(a).find("h2").text() > $(b).find("h2").text();
                });
                $("#mainorder").html(numericallyOrderedDivs);
            }else{
                var $divs = $("div.buyer-first");
                var numericallyOrderedDivs = $divs.sort(function (a, b) {
                    return $(a).find("h2").text() < $(b).find("h2").text();
                });
                $("#mainorder").html(numericallyOrderedDivs);
            }
        }

        
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.resizing_select').change(function() {
                $("#width_tmp_option").html($('.resizing_select option:selected').text());
                $(this).width($("#width_tmp_select").width());
            });
        });

        function sendMessage(){

            var actionurl = "{{url('message/sendMessage/Seller')}}";
            var formValidFalg = $("#sendmessageform").parsley().validate('');
           
            if(formValidFalg){
              
                document.getElementById('load').style.visibility="visible";  
                var formData = new FormData($('#sendmessageform')[0]);
                
                $.ajax({
                    processData: false,
                    contentType: false,
                    type    : "POST",
                    url     : actionurl,
                    data    : formData,
                    success : function( res ){
                        $("#l_message_data").val("");
                        document.getElementById('load').style.visibility='hidden';
                        var obj = jQuery.parseJSON(res);
                        if(obj['status']==1){
                          $("#chatdetail").prepend(obj['messagestr']);
                        }else{
                          $("#successmsgpop").css("display","none");
                          $("#errormsgpop").css("display","block");
                          $("#sendMessageSuccess").modal("show");
                        }
                    },
                    error: function ( jqXHR, exception ) {
                        document.getElementById('load').style.visibility='hidden';
                        $("#successmsgpop").css("display","none");
                        $("#errormsgpop").css("display","block");
                        $("#sendMessageSuccess").modal("show");
                    }
                });
            }    
        }  

        function dashboardMessage(filter=""){

              var page = $("#messagepage").val();
              var actionurl = "{{url('message/buyer/ajaxlist')}}";
              var formdata = "page="+page+"&filter="+filter; 
              page=parseInt(page)+1;
              $("#messagepage").val(page)
              document.getElementById('load').style.visibility="visible"; 
              
              $.ajax({
                  type    : "GET",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    document.getElementById('load').style.visibility='hidden';
                    if(obj['status']==1){
                        $("#dashboardmessagedata").append(obj['responsestr']);
                        if(obj['btnstatus']==0){
                            $("#showmorebtnmessage").html("");
                            //$("#showmorebtnmessage").css("visibility","hidden");
                        }
                        detailMessage(obj['pid']);
                        $("#msgcntdata").html(obj['msgcntdata']);
                        $(".scrollbar_1").css("max-height","500px");
                        $('.scrollbar_1').scrollbar();
                        
                    }    
                    else{
                        $("#commonmsg").html(obj['msg']);
                    }
                  },
                  error: function ( jqXHR, exception ) {
                  }
              });
        }
        dashboardMessage('asc');
        
        function detailMessage(id){
                 
              var actionurl = "{{url('message/detail')}}";
              var formdata = "&id="+id; 
              document.getElementById('load').style.visibility="visible"; 
              $("#i_parent_id_data").val(id);
              $.ajax({
                  type    : "GET",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    document.getElementById('load').style.visibility='hidden';
                    if(obj['status']==1){
                        $("#chatdetail").html(obj['responsestr']);
                        $("#nameother").html(obj['othername']);
                        $("#chatnow").css("display","inline");
                        $(".chat-first").removeClass("activemessage");
                        $("#"+id).addClass("activemessage");
                    }    
                    else{
                        $("#chatdetail").html("");
                        $("#commonmsg").html(obj['msg']);
                    }
                  },
                  error: function ( jqXHR, exception ) {
                  }
              });
        }


        @if(isset($_REQUEST['mid']) && $_REQUEST['mid']!="")
            var mid = "{{$_REQUEST['mid']}}";
            setTimeout(function(){ 
                detailMessage(mid); 
            }, 500);
        @endif

      
    </script>

@stop

