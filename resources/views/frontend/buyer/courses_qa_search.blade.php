 
     @if(isset($coursesQA) && count($coursesQA))        
            @foreach($coursesQA as $key=>$val)

                <div class="accordion-final-announc final-announc-Q-a" @if($key=="0") active @endif>
                    <div class="accordion accordion-announcement">
                        
                        <div class="accordion-toggle1 accordion-toggle-qa">
                            <div class="Announcements-find Announcements-q-a">
                                <?php
                                    $imgdata="";
                                    if(isset($userList[$val->i_user_id])){
                                        $imgdata = \App\Helpers\General::userroundimage($userList[$val->i_user_id]);
                                    }
                                ?>
                                <div class="Announcements-man">
                                    <?php 
                                        echo $imgdata;
                                    ?>    
                                </div>
                                <div class="Announcements-name q-a-name">
                                    <p>{{count($val->hasUser()) ? ucfirst($val->hasUser()->v_fname).' '.ucfirst($val->hasUser()->v_lname) : '-'}}</p>
                                    @php 
                                        $humanTime= App\Helpers\General::time_elapsed_string(strtotime($val->d_added));
                                    @endphp
                                    <p><span>{{$humanTime}}</span></p>
                                    <p>{{$val->v_title}}</p>
                                    <p class="ans-q">{{isset($val->l_questions) ? $val->l_questions:''}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-content" @if($key=="0") style="display: block;" @endif>
                            <div class="Announcements-text">
                                
                            <div id='commentsstr{{$val->id}}'>
                                @if(isset($val->l_answers) && count($val->l_answers))        
                                    @foreach($val->l_answers as $k=>$v)

                                        <div class="Announcements-second-man qaseller-second-man">
                                            <div class="qa-seller-find">
                                                                                                        
                                                <div class="Announcements-man">
                                                    <?php
                                                        $imgdata="";
                                                        if(isset($userList[$v['i_user_id']])){
                                                            $imgdata = \App\Helpers\General::userroundimage($userList[$v['i_user_id']]);
                                                        }
                                                        echo $imgdata;
                                                    ?>    
                                                </div>
                                                <div class="Announcements-name">
                                                    <p>
                                                    <?php
                                                        $username="";
                                                        if(isset($userList[$v['i_user_id']])){
                                                            $username = $userList[$v['i_user_id']]['v_name'];
                                                        }
                                                        echo $username;
                                                    ?>    
                                                    </p>
                                                    @php 
                                                        $humanTime= App\Helpers\General::time_elapsed_string(strtotime($v['d_added']));
                                                    @endphp
                                                    <p><span>{{$humanTime}}</span></p>
                                                    <div class="Announcements-message" style="width: 100%">
                                                        {{$v['v_text']}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                </div>

                                <div class="qa-seller-input">
                                    <div class="qa-seller-find">
                                        <div class="Announcements-man">
                                            <?php
                                                
                                                $imgdata = \App\Helpers\General::userroundimage($curentuser);
                                                echo $imgdata;
                                            ?> 

                                        </div>

                                        <form role="form" method="POST" name="commnetform{{$val->id}}" id="commnetform{{$val->id}}" data-parsley-validate enctype="multipart/form-data" style="width: 100%" onsubmit="submitcomment('{{$val->id}}')" action="javascript:;" >
                                        {{ csrf_field() }}
                                        <input type="hidden" name="i_course_qa_id" value="{{$val->id}}">
                                        <div class="Announcements-name">
                                            <p>{{$curentuser['v_name']}}</p>
                                            <div class="new-submit-comment">
                                            <input type="text" class="form-control comment-seller new-seller-find" name="v_text" id="v_text" placeholder="Enter your comment" required>
                                            <button type="button" onclick="submitcomment('{{$val->id}}')" class="btn btn-convarsation-submit new-submit-live"> Submit </button>
                                            </div>
                                        </div>
                                        </form>


                                    </div>
                                </div>
                                <div class="alert alert-info alert-danger" style="display: none;" id="commonerr">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span id="commonerrmsg">Something went wrong.pleease try again after some time.</span>
                                  </div>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        @else
            
        <div class="Course-Reviews">
             <div class="row">
                <div class="col-md-6">
                     There are no questions and answers.
                </div>
            </div>
        </div> 

    
    @endif        

    <script type="text/javascript">

        $(document).ready(function() {
            $('.accordion').find('.accordion-toggle1').click(function() {
                $(this).next().slideToggle('600');
                $(".accordion-content").not($(this).next()).slideUp('600');
            });
            $('.accordion-toggle1').on('click', function() {
                $(this).toggleClass('active').siblings().removeClass('active');
            });
        });
    </script>    

