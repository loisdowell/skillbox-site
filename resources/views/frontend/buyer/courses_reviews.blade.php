@extends('layouts.frontend')

@section('content')

    <style type="text/css">
        .letter-text-myprifile {
            text-align: center;
            font-size: 34px;
            padding: 10px 0px;
            color: rgb(231, 14, 138);
        }
        .new-seller-find {border-radius: 20px;
        margin-right: 10px;}
        .new-submit-live {height: 34px;margin-top: auto;}
        .new-submit-comment{display: inline-flex;width: 100%}
        .modal-header-1 p{width: auto !important;}
    </style>

    <!-- Modal Leave Review -->
    <div class="modal  fade" id="leaveReview" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content modal-content-1">
                <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> My Review </h4>
                    <p>Rate this course and leave the review. The edit review option will be avaliable to you for up to <span> 24 hours</span>.</p>
                </div>

                <form role="form" method="POST" name="coursesreview" id="coursesreview" data-parsley-validate enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="buyer_courses_id" value="{{$buyercoursedata->id}}">
                <input type="hidden" name="i_courses_id" value="{{$buyercoursedata->i_course_id}}">
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="text-box-containt box-containt-que">
                                    <label> Review rating </label>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="f_rate_instruction" id="star-input-4" class="rating" title="" value="1" required> <span class="star-no"> Clear Instruction </span>
                                    </div>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="f_rate_navigate" id="star-input-4" class="rating" title="" value="1" required> <span class="star-no"> Easy to Navigate </span>
                                    </div>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="f_rate_reccommend" id="star-input-4" class="rating" title="" value="1" required> <span class="star-no"> Would Recommend </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="text-box-containt">
                                    <label> Enter your review </label>
                                    <textarea rows="4" name="l_comment" class="form-control popup-letter" required></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button  type="button" onclick="submitreview()" class="btn btn-Submit-pop">Submit Review</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>

                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Leave Review -->



    <!-- Modal Edit Leave Review -->
    <div class="modal  fade" id="editReview" role="dialog">
        <div class="modal-dialog modal-lg">


            <!-- Modal content-->
            <div class="modal-content modal-content-1">
            <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> My Review </h4>
                    <p> Edit review will be avaliable for 24 hours only. </p>
                </div>


                <form role="form" method="POST" name="coursesreviewedit" id="coursesreviewedit" data-parsley-validate enctype="multipart/form-data">
                {{ csrf_field() }}
               
                <input type="hidden" name="buyer_courses_id" value="{{$buyercoursedata->id}}">
                <input type="hidden" name="i_courses_id" value="{{$buyercoursedata->i_course_id}}">
                <input type="hidden" name="i_review_id" value="{{isset($currentuserreview->id) ? $currentuserreview->id : '0'}}">

                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <div id="reviewpopmsg"></div>    
                            <div class="col-xs-6">
                                <div class="text-box-containt box-containt-que">
                                    <label>Review rating </label>
                                </div>
                                <div class="leave-popup" id="aa">
                                    <div class="rating-seller">
                                        <input type="text" name="f_rate_instruction" id="f_rate_instruction" class="rating" title="" value="{{isset($currentuserreview->f_rate_instruction) ? $currentuserreview->f_rate_instruction : '1'}}" required> <span class="star-no"> Clear Instruction </span>
                                    </div>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="f_rate_navigate" id="f_rate_navigate" class="rating" title="" value="{{isset($currentuserreview->f_rate_navigate) ? $currentuserreview->f_rate_navigate : '1'}}" required> <span class="star-no"> Easy to Navigate </span>
                                    </div>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="f_rate_reccommend" id="f_rate_reccommend" class="rating" title="" value="{{isset($currentuserreview->f_rate_reccommend) ? $currentuserreview->f_rate_reccommend : '1'}}" required> <span class="star-no"> Would Recommend </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="text-box-containt">
                                    <label> Enter your review </label>
                                    <textarea rows="4" name="l_comment" class="form-control popup-letter">{{isset($currentuserreview->l_comment) ? $currentuserreview->l_comment : ''}}</textarea>
                                </div>
                                <div class="stb-btn">
                                    <button type="button" class="btn btn-Submit-pop" onclick="editMyReview()" >Submit Review</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>

                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
    </div>
    <!--- End Modal Edit Leave Review -->




    <!--26b-control-panel-my-courses-overviwe-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> My Courses Details </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-detail">
        <div class="container">
            <ul class="nav nav-pills nav-pills-new">
                <li><a href="{{url('buyer/courses/overview')}}/{{$buyercoursedata->id}}">Overview </a></li>
                <li><a href="{{url('buyer/courses/section')}}/{{$buyercoursedata->id}}">Course Content</a></li>
                
                @if(isset($data->e_qa_enabled) && $data->e_qa_enabled=="on")
                <li><a href="{{url('buyer/courses/qa')}}/{{$buyercoursedata->id}}">Q&A</a></li>
                @endif
                
                @if(isset($data->e_reviews_enabled) && $data->e_reviews_enabled=="on")
                <li class="active"><a href="{{url('buyer/courses/reviews')}}/{{$buyercoursedata->id}}"> Course Reviews</a></li>
                @endif
                
                @if(isset($data->e_announcements_enabled) && $data->e_announcements_enabled=="on")
                <li><a href="{{url('buyer/courses/announcements')}}/{{$buyercoursedata->id}}">Announcements</a></li>
                @endif

            </ul>
        </div>
    </div>
    <!-- Tab panes -->

    <div class="container">
        <!-- 26B-control-panel-my-courses-leave-a-review-seller -->
        <div class="Course-ans">
            <div class="Course-final">
                <div class="Course-text">
                    <h3>Course Reviews</h3>
                </div>
                <div class="rating-Course">
                    <div class="rating-buyer">
                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$totalavg}}" required>
                        <span class="star-no">{{number_format($totalavg,1)}}</span>
                        <span class="star-no">( {{count($coursesReviews)}} )</span>
                    </div>
                </div>
            </div>
            @if(!count($currentuserreview))
            <button type="button" class="btn btn-leave-review" data-toggle="modal" data-target="#leaveReview"> Leave Review</button>
            @endif
        </div>


        @php 

        $totalInstructions=0;
        $totalNavigate=0;
        $totalrecommend=0;
        
        if(isset($coursesReviews) && count($coursesReviews)){
            foreach($coursesReviews as $key=>$val){
                $totalInstructions = $totalInstructions+$val->f_rate_instruction;
                $totalNavigate = $totalNavigate+$val->f_rate_navigate;
                $totalrecommend = $totalrecommend+$val->f_rate_reccommend;

            }  
            $totalInstructions = $totalInstructions/count($coursesReviews);
            $totalNavigate = $totalNavigate/count($coursesReviews);
            $totalrecommend = $totalrecommend/count($coursesReviews);
        }        
        
        @endphp            
        
        <div class="row">
        <div class="col-sm-2 text-center">
            Clear Instructions
            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalInstructions}}" readonly>
        </div>
        
        <div class="col-sm-2 text-center">
            Easy to Navigate
            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalNavigate}}" readonly>
        </div>
        <div class="col-sm-2 text-center">
            Would recommend
            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalrecommend}}" readonly>
        </div>
        </div>
        <div style="clear: both"></div>
        <br>

        <div id="commonmsg"></div>
        <div class="Course-Reviews">
            
            @if(isset($coursesReviews) && count($coursesReviews))        
                @foreach($coursesReviews as $key=>$val)
                    <div class="row">
                        
                        <div class="col-sm-10">
                            <div class="conversation-find">
                                <?php
                                    $imgdata="";
                                    if(isset($userList[$val->i_user_id])){
                                        $imgdata = \App\Helpers\General::userroundimage($userList[$val->i_user_id]);
                                    }
                                ?>
                                <div class="conversation-man">
                                    <?php 
                                        echo $imgdata;
                                    ?>    
                                </div>
                                @php
                                    $ratedata = ($val->f_rate_instruction+$val->f_rate_navigate+$val->f_rate_reccommend)/3;
                                @endphp
                                <div class="conversation-name">
                                    <p>{{count($val->hasUser()) ? $val->hasUser()->v_fname.' '.$val->hasUser()->v_lname : '-'}}<span class="rating-buyer rating-Course-one">
                                    <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$ratedata}}" readonly>   </span> {{number_format($ratedata,1)}} </p>
                                    @php 
                                        $humanTime= App\Helpers\General::time_elapsed_string(strtotime($val->d_added));
                                    @endphp
                                    <p><span>{{$humanTime}}</span></p>
                                </div>
                            </div>
                            <div class="conversation-text">
                                <div class="course-review-all">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p>{{isset($val->l_comment) ? $val->l_comment:''}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="commentstrans{{$val->id}}">
                                @if(isset($val->l_answers) && count($val->l_answers))
                            
                                    <div class="conversation-text extra-margin">
                                        <div class="conversation-find conversation-find-course">
                                            
                                            <div class="conversation-man">
                                                <?php
                                                    $imgdata="";
                                                    if(isset($userList[$val->l_answers['i_user_id']])){
                                                        $imgdata = \App\Helpers\General::userroundimage($userList[$val->l_answers['i_user_id']]);
                                                    }
                                                    echo $imgdata;
                                                ?>  
                                            </div>

                                            <div class="conversation-name">
                                                <p>
                                                    <?php
                                                        $username="";
                                                        if(isset($userList[$val->l_answers['i_user_id']])){
                                                            $username = $userList[$val->l_answers['i_user_id']]['v_name'];
                                                        }
                                                        echo $username;
                                                    ?>   
                                                <span class="rating-buyer rating-Course-one">
                                                </span></p>
                                                @php 
                                                    $humanTime= App\Helpers\General::time_elapsed_string(strtotime($val->l_answers['d_added']));
                                                @endphp
                                                <p><span>{{$humanTime}}</span></p>
                                            </div>
                                        </div>

                                        <div class="conversation-text conversation-text-final">
                                            <div class="conversation-text-all conversation-text-course">
                                                <p class="response-seller"> {{isset($val->l_answers['l_comment']) ? $val->l_answers['l_comment'] :'' }} </p>
                                            </div>
                                        </div>
                                    </div>

                                @else
                                    <?php /*
                                    <div class="col-xs-12">
                                        <div class="conversation-second-man second-course">
                                            <div class="conversation-find courses-reviews-noborder">
                                                <div class="conversation-man">
                                                    <?php
                                                        $imgdata = \App\Helpers\General::userroundimage($curentuser);
                                                        echo $imgdata;
                                                    ?> 
                                                </div>
                                                
                                                <form method="POST" name="commnetform{{$val->id}}" id="commnetform{{$val->id}}" data-parsley-validate enctype="multipart/form-data" >
                                                {{ csrf_field() }}
                                                <input type="hidden" name="i_course_reviews_id" value="{{$val->id}}">

                                                <div class="conversation-name1 conversation-name-q">
                                                    <p>{{$curentuser['v_name']}}</p>
                                                    <div class="new-submit-comment">
                                                    <input type="text" class="form-control comment-seller new-seller-find" name="l_comment" placeholder="Enter Your Comment">
                                                    <button type="button" onclick="submitcomment('{{$val->id}}')" class="btn btn-convarsation-submit new-submit-live"> Submit </button>
                                                    </div>
                                                </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    */ ?>

                                @endif

                            </div>

                        </div>
                        
                        @php
                            $currentdate=date("Y-m-d H:i:s");
                            $hourdiff = round((strtotime($currentdate) - strtotime($val->d_added))/3600, 1);
                        @endphp
                        
                        @if(auth()->guard('web')->user()->id==$val->i_user_id && $hourdiff<=24)
                            <div class="col-sm-2">
                                <div class="my-review-pop">
                                    MY REVIEW
                                </div>
                                <div class="edit-review-popup">
                                    <button class="btn btn-post-edit" data-toggle="modal" data-target="#editReview">
                                        Edit My Review
                                    </button>
                                </div>
                            </div>
                        @endif

                    </div>
                @endforeach
            @else
                <div class="row">
                    <div class="col-md-6">
                          No reviews available for this course. Be the first one to review this course.
                    </div>
                </div>

            @endif
        </div>
        <!-- 26B-control-panel-my-courses-leave-a-review-seller -->
    </div>
    <!--26b-control-panel-my-courses-overviwe-->

   

@stop
@section('js')

<script type="text/javascript">

    $(document).ready(function() {
            $('.accordion').find('.accordion-toggle1').click(function() {
                $(this).next().slideToggle('600');
                $(".accordion-content").not($(this).next()).slideUp('600');
            });
            $('.accordion-toggle1').on('click', function() {
                $(this).toggleClass('active').siblings().removeClass('active');
            });
        });

    function submitreview(){

        var actionurl = "{{url('buyer/courses/reviews')}}";
        var formValidFalg = $("#coursesreview").parsley().validate('');
        
        if(formValidFalg){

            document.getElementById('load').style.visibility="visible";                
            var formdata = $("#coursesreview").serialize();

            $.ajax({
                type    : "POST",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        
                        window.location = "{{url('buyer/courses/reviews')}}/"+obj['buyer_courses_id'];

                    }else{
                        
                        $('#commonerr').css("display","block");
                        $('#commonerrmsg').html(obj['msg']);
                    }
                    
                },
                
                error: function ( jqXHR, exception ) {
                    $('#commonerr').css("display","block");
                }

            });
        }

    }

   function submitcomment(id=""){
        
        var actionurl = "{{url('buyer/courses/reviews-comment')}}"; 
        var formValidFalg = $("#commnetform"+id).parsley().validate('');

        if(formValidFalg){
                        
            var formdata = $("#commnetform"+id).serialize();

            $.ajax({
                type    : "POST",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                   
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $('#commentstrans'+id).html(obj['commentstr']);
                    }else{
                        $('#commonerr').css("display","block");
                        $('#commonerrmsg').html(obj['msg']);
                    }
                    
                },
                
                error: function ( jqXHR, exception ) {
                    $('#commonerr').css("display","block");
                }

            });
        }
    }

    function editMyReview(){

        var actionurl = "{{url('buyer/courses/reviews-update')}}";
        var formValidFalg = $("#coursesreviewedit").parsley().validate('');
        
        if(formValidFalg){

            document.getElementById('load').style.visibility="visible";                
            var formdata = $("#coursesreviewedit").serialize();

            $.ajax({
                type    : "POST",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        
                        window.location = "{{url('buyer/courses/reviews')}}/"+obj['buyer_courses_id'];

                    }else{
                        $('#reviewpopmsg').html(obj['msg']);
                    }
                    
                },
                
                error: function ( jqXHR, exception ) {
                    $('#commonerr').css("display","block");
                }

            });
        }
   
    }

</script>

@stop

