@extends('layouts.frontend')

@section('content')
<style type="text/css">
    .ans-qus-final-progress ul.parsley-errors-list{
        margin-left: 106px !important;
    }
</style>
    <script src="{{url('public/Assets/plugins/countdown.js')}}" type="text/javascript"></script>
    <style type="text/css">
        .countdown_dashboard {margin: 0px 0px 89px;}
    </style>

    <div class="modal fade" id="sendMessage" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 600px">
            <div class="modal-content modal-content-1">
              <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> Contact Seller </h4>
                </div>
                <form role="form" method="POST" name="sendMessageForm" id="sendMessageForm" data-parsley-validate enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="i_to_id" value="{{$sellerprofile->hasUser()->id}}">
                <input type="hidden" name="i_profile_id" value="{{$sellerprofile->id}}">

                <div class="modal-body">
                    <div class="modal-body-1" style="padding: 0px 30px !important;" id="contactsellerdetail">
                        <div class="row">
                             <div class="final-point">
                                <div class="buyer-name">
                                    <img src="https://skillboxs3bucket.s3.us-east-2.amazonaws.com/users/profile-1538572663.png" alt="">
                                </div>
                                <div class="buyer-tital">
                                    <h5 style="margin-left: 5px">Ray nanda</h5>
                                    <div class="rating-buyer rating-link rating_counts" style="margin-top: 6px !important;margin-bottom: 6px !important">
                                    <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="0" readonly>
                                    <span class="star-no"> 0.0 (0)</span>
                                    </div>
                                    <br>
                                    <span style="margin-left: 5px">Location : asdsadsadsa,sadsadsa</span>
                                </div>
                            </div>
                            <div id="popupcommonmsg"></div>
                            <div class="col-xs-12">
                              <div class="text-box-containt">
                                    <label> Subject </label>
                                    <input type="text" class="form-control input-field" name="v_subject_title" required>
                                </div>

                                <div class="text-box-containt">
                                    <label> Enter Your Message </label>
                                    <textarea rows="3" name="l_message" class="form-control popup-letter" required></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button  type="button" onclick="submitMessage()" class="btn btn-Submit-pop">Send Message</button>
                                </div>
                            </div>    
                            <div class="col-xs-12">
                                
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
</div>


<div id="sendMessageSuccess" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;">Message</h4>
      </div>
      <div class="modal-body">
        <p id="popupmessage" style="text-align: center;"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <div class="modal  fade" id="acceptReviewOrder" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content modal-content-1">
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> Leave Your Review </h4>
                </div>
                
                <form role="form" method="POST" name="orderreview" id="orderreview" data-parsley-validate enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="i_order_id" id="i_order_id_review" value="{{$data->id}}">
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <div id="popupcommonmsg"></div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="text-box-containt box-containt-que">
                                    <label> Review rating </label>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="i_communication_star" id="star-input-4" class="rating" title="" value="1" required> <span class="star-no"> Seller Communication </span>
                                    </div>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="i_described_star" id="star-input-4" class="rating" title="" value="1" required><span class="star-no"> Service As Described </span>
                                    </div>
                                </div>
                                <div class="leave-popup">
                                    <div class="rating-seller">
                                        <input type="text" name="i_recommend_star" id="star-input-4" class="rating" title="" value="1" required> <span class="star-no"> Would Recommend </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="text-box-containt">
                                    <label> Enter Your Review </label>
                                    <textarea rows="3" name="l_comment" class="form-control popup-letter" required></textarea>
                                </div>

                                <div class="stb-btn">
                                    <button  type="button" onclick="submitreview()" class="btn btn-Submit-pop">Submit Review</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>

                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS-leave_review -->


    
    <!-- 26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS-in progress -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/orders/active')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Order </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> Order Details </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-detail tab_detail_buyerorder">
        <div class="container">
            <ul class="nav nav-pills nav-buyerorder-detail">
                
                @if(isset($data->v_order_requirements) && $data->v_order_requirements=="yes")
                <li><a href="#"><i class="zmdi zmdi-hc-fw"></i> SELLER REQUIREMENTS</a></li>
                @endif

                @if(isset($data->v_order_requirements_submited) && $data->v_order_requirements_submited=="yes")
                <li><a href="#"><i class="zmdi zmdi-hc-fw"></i> REQUIREMENTS SUBMITTED </a></li>
                @endif

                @if($data->v_order_status=="active")
                    <li><a href="#"><i class="zmdi zmdi-hc-fw"></i> IN PROGRESS </a></li>
                @endif    
                
                @if($data->v_order_status=="delivered" && !isset($data->v_buyer_deliver_status) )    
                {{-- <li><a href="#"><i class="zmdi zmdi-hc-fw"></i> IN PROGRESS </a></li> --}}
                <li><a href="#"><i class="zmdi zmdi-hc-fw"></i> DELIVERY SUBMITTED </a></li>
                @endif

                @if($data->v_order_status=="delivered" && isset($data->v_buyer_deliver_status))
                <li><a href="#"><i class="zmdi zmdi-hc-fw"></i> ORDER COMPLETED </a></li>
                @endif

                @if($data->v_order_status=="cancelled")
                    <li><a href="#"><i class="zmdi zmdi-hc-fw"></i> ORDER CANCELLED </a></li>
                @endif  




                {{-- <li class="active"><a href="#"><i class="zmdi zmdi-hc-fw"></i> ORDER   
                    @if($data->v_order_status=="active")
                    IN PROGRESS
                    @elseif($data->v_order_status=="delivered")
                        
                        @if($data->v_order_status=="delivered" && !isset($data->v_buyer_deliver_status))
                            IN DELIVERED
                        @else
                            COMPLETED
                        @endif
                    @elseif($data->v_order_status=="completed")
                    COMPLETED
                    @elseif($data->v_order_status=="cancelled")
                    CANCELLED
                    @endif
                </a></li> --}}
            </ul>
        </div>
    </div>

    <!-- Tab panes -->
    <div class="container">

         @if ($success = Session::get('success'))
          <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 10px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-info alert-danger" style="margin-top: 10px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
          </div>
        @endif

        <div class="order-progress">
            <div class="row">
                <div class="col-sm-9">
                   
                    <div class="download-invoice">
                        <a href="{{url('buyer/invoice/course/download')}}/{{$data->id}}">
                            <img src="{{url('public/Assets/frontend/images/download-invoice.png')}}" alt="" /><span>DOWNLOAD INVOICE</span>
                        </a>
                    </div>

                    <div class="download-invoice-final">
                        <div class="row">
                            <div class="col-sm-5">
                                @php
                                    $imgdata="";
                                @endphp
                                @if(isset($sellerprofile) && count($sellerprofile))
                                    @if(isset($sellerprofile->v_work_photos) && isset($sellerprofile->v_work_photos[0]))
                                        @php
                                            $imgdata = \Storage::cloud()->url($sellerprofile->v_work_photos[0]);    
                                        @endphp
                                    @else
                                        @php
                                            $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');    
                                        @endphp
                                    @endif
                                @else
                                    @php
                                        $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');    
                                    @endphp    
                                @endif    
                                <img class="img-person-on" src="{{$imgdata}}" alt="" />
                            </div>

                            <div class="col-sm-7">
                                <div class="list-heading">
                                    <h3>{{isset($data->v_profile_title) ? $data->v_profile_title : ''}}</h3>
                                    <ul>
                                        @php
                                            $v_fname="";
                                            if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_fname)){
                                                $v_fname=$sellerprofile->hasUser()->v_fname;
                                            }

                                            $v_lname="";
                                            if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_lname)){
                                                $v_lname=$sellerprofile->hasUser()->v_lname;
                                            }
                                        @endphp
                                        <li>{{$v_fname}} {{$v_lname}}</li>
                                        |
                                        <li>Order: #{{isset($data->i_order_no) ? $data->i_order_no : ''}} </li>
                                        |
                                        <li>{{isset($data->d_date) ? date("M d, Y ",strtotime($data->d_date)) : ''}}.</li>
                                    </ul>
                                    <span>£{{isset($data->v_amount) ? $data->v_amount : ''}}</span>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="grand-total">
                                    <table class="table table-condensed">
                                        <tr>
                                            <th style="width: 40%;">TITLE</th>
                                            <th>QUANTITY</th>
                                            <th>DURATION</th>
                                            <th>AMOUNT</th>
                                        </tr>
                                        @php
                                            $subtotal=0;
                                        @endphp
                                        @if(isset($data->v_order_detail) && count($data->v_order_detail))
                                            @foreach($data->v_order_detail as $k=>$v)
                                                @php
                                                    $subtotal = $subtotal+($v['price']*$v['qty']);
                                                @endphp
                                                <tr>
                                                    <td class="Subtotal">{{isset($v['name']) ? $v['name'] : ''}}</td>
                                                    <td>{{isset($v['qty']) ? $v['qty'] : ''}}</td>
                                                    @if($k>0)
                                                    <td>-</td>    
                                                    @else
                                                    <td>{{isset($data->v_duration_time) ? $data->v_duration_time : ''}} {{isset($data->v_duration_in) ? $data->v_duration_in : ''}}</td>
                                                    @endif
                                                    <td>£{{ $v['price']*$v['qty'] }}</td>
                                                </tr>
                                            @endforeach
                                        @endif  
                                    </table>
                                </div>
                                <div class="grand-total2">
                                    <div class="row">
                                        <div class="all-total-one">
                                            <div class="col-xs-8">
                                                <div class="Subtotal-point">
                                                    <b>Subtotal</b>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="sub-total-leval">
                                                    <b>@if($data->v_order_status=="cancelled") - @endif £{{$subtotal}}</b>
                                                </div>
                                            </div>

                                            <div class="col-xs-8">
                                                <div class="Subtotal-point">
                                                    <b>Skillbox Commission</b>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="sub-total-leval">
                                                    <b>@if($data->v_order_status=="cancelled") - @endif £{{$data->v_buyer_commission}}</b>
                                                </div>
                                            </div>

                                            <div class="col-xs-8">
                                                <div class="Subtotal-point">
                                                    <b>Processing fees</b>
                                                </div>
                                            </div>


                                            @php
                                                $processfees = 0;
                                                $processfees = number_format($subtotal*5/100,2);
                                            @endphp

                                            <div class="col-xs-4">
                                                <div class="sub-total-leval">
                                                    <b>£{{$data->v_buyer_processing_fees}}</b>
                                                </div>
                                            </div>


                                            @if($data->v_order_status=="cancelled")
                                            <div class="col-xs-8">
                                                <div class="Subtotal-point">
                                                    <b>Administration fees</b>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="sub-total-leval">
                                                    <b>£{{isset($data->v_admin_fees) ? $data->v_admin_fees : '0'}}</b>
                                                </div>
                                            </div>
                                            @endif



                                        </div>
                                    </div>
                                </div>
                                <div class="grand-viwe">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="Subtotal-point">
                                                <b>Total</b>
                                            </div>
                                        </div>
                                        @php
                                            if($data->v_order_status=="cancelled"){
                                                $adminfees=0;
                                                if(isset($data->v_admin_fees)){
                                                    $adminfees=$data->v_admin_fees;
                                                }
                                                $Total= $subtotal+$data->v_buyer_commission;
                                                $Total=$Total-$adminfees;  
                                                $Total = number_format($Total,2);  
                                            }else{
                                                $Total= $subtotal+$data->v_buyer_processing_fees+$data->v_buyer_commission;  
                                                $Total = number_format($Total,2);  

                                            }
                                        @endphp

                                        <div class="col-xs-4">
                                            <div class="sub-total-leval">
                                                <b>@if($data->v_order_status=="cancelled") - @endif £{{$Total}}</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($data->v_order_status=="delivered" && isset($data->v_buyer_deliver_status))
                @else
                    @if($data->v_order_status!="cancelled")
                        @if($data->v_order_status!="completed")
                        <div class="col-sm-3">
                            <div class="resolution-center">
                                <p>Need to contact</p>
                                <p>Customer Support?</p>
                                <a href="{{url('resolution')}}/{{$data->id}}" class="btn btn-text-pop">
                                    Resolution Center
                                </a>
                            </div>
                        </div>
                        @endif
                    @endif

                @endif

            </div>
        </div>
    </div>


    @php

        $hours=0;
        $day=0;
        $month=0;
        $year=0;
        $min=0;
        $sec=0;


        $enddate = $data->d_added;
        if(isset($data->v_order_start) && isset($data->d_order_start_date) && $data->v_order_start=="yes"){
            $enddate = $data->d_order_start_date;
        }

        if(isset($data->v_duration_in) && $data->v_duration_in=="hours"){
            
            if(isset($data->v_duration_time)){
                $hours = (int)$data->v_duration_time;
            }

            $cdate = date("Y-m-d H:i:s");
            if(isset($data->v_order_start) && $data->v_order_start!="yes"){
                if(isset($data->v_order_deliver_date) && $data->v_order_deliver_date!=""){
                    $cdate = $data->v_order_deliver_date;    
                }else{
                    $cdate = $data->d_added;    
                }
            }else{
                $cdate=date("Y-m-d H:i:s");
            }
            //$cdate=date("Y-m-d H:i:s");
            $seconds = strtotime($cdate)-strtotime($enddate);
            $totalsecond = $hours*3600;

            if($totalsecond>$seconds){
                $seconds = $totalsecond - $seconds;    
            }else{
                $seconds = 0;    
            }
            
            $hours=0;
            $hours=0;
            $min=0;
            $sec = $seconds;

        }

        if(isset($data->v_duration_in) && $data->v_duration_in=="day"){
                
            if(isset($data->v_duration_time)){
                $day = $data->v_duration_time;
            }

            $cdate = date("Y-m-d H:i:s");
            if(isset($data->v_order_start) && $data->v_order_start!="yes"){
                if(isset($data->v_order_deliver_date) && $data->v_order_deliver_date!=""){
                    $cdate = $data->v_order_deliver_date;    
                }else{
                    $cdate = $data->d_added;    
                }
            }else{
                $cdate=date("Y-m-d H:i:s");
            }
            //$cdate=date("Y-m-d H:i:s");
            $odate = $enddate;
            $seconds = strtotime($cdate)-strtotime($odate);
            $daydiff = ceil(($seconds / (60 * 60 * 24)));
            $totalsecond = $day*86400;
            $seconds = $totalsecond - $seconds;
            $day=0;
            $day = 0;  
            $hours =0;
            $min = 0;
            $sec = $seconds;
        }    
        
        if(isset($data->v_duration_in) && $data->v_duration_in=="month"){

                if(isset($data->v_duration_time)){
                    $day = $data->v_duration_time*30;
                }

                $cdate = date("Y-m-d H:i:s");
                if(isset($data->v_order_start) && $data->v_order_start!="yes"){
                    if(isset($data->v_order_deliver_date) && $data->v_order_deliver_date!=""){
                        $cdate = $data->v_order_deliver_date;    
                    }else{
                        $cdate = $data->d_added;    
                    }
                }else{
                    $cdate=date("Y-m-d H:i:s");
                }
                //$cdate=date("Y-m-d H:i:s");
                $odate = $enddate;
                $seconds = strtotime($cdate)-strtotime($odate);
                $daydiff = ceil($seconds / (60 * 60 * 24));

                $totalsecond = $day*86400;
                $seconds = $totalsecond - $seconds;
                $day=0;
                $day = 0;  
                $hours =0;
                $min = 0;
                $sec = $seconds;
            }

        if(isset($data->v_duration_in) && $data->v_duration_in=="year"){

            if(isset($data->v_duration_time)){
                $day = $data->v_duration_time*288;
            }

            $cdate = date("Y-m-d H:i:s");
            if(isset($data->v_order_start) && $data->v_order_start!="yes"){
                if(isset($data->v_order_deliver_date) && $data->v_order_deliver_date!=""){
                    $cdate = $data->v_order_deliver_date;    
                }else{
                    $cdate = $data->d_added;    
                }
            }else{
                $cdate=date("Y-m-d H:i:s");
            }

            $cdate=date("Y-m-d H:i:s");
            $seconds = strtotime($cdate)-strtotime($enddate);
            $daytotal = $seconds / (60 * 60 * 24);

            $totalsecond = $day*86400;
            $seconds = $totalsecond - $seconds;
            $day=0;
            $day = 0;  
            $hours =0;
            $min = 0;
            $sec = $seconds;
        }    




        
            // $enddate = $data->d_added;
            // if(isset($data->v_order_start) && isset($data->d_order_start_date) && $data->v_order_start=="yes"){
            //     $enddate = $data->d_order_start_date;
            // }

            // if(isset($data->v_duration_in) && $data->v_duration_in=="hours"){
                
            //     if(isset($data->v_duration_time)){
            //         $hours = (int)$data->v_duration_time;
            //     }
            //     $cdate=date("Y-m-d H:i:s");
            //     $seconds = strtotime($cdate)-strtotime($enddate);
            //     $hourstotal = $seconds / 60 / 60;
                
            //     $totalduration = $data->v_duration_time.":00:00";
            //     if($hours>$hourstotal){
                    
            //         $a = strtotime($totalduration)-$seconds;
            //         $hours = date("H",$a);
            //         $min = date("i",$a);
            //         $sec = date("s",$a);
              
            //     }else{
            //         $hours=0;
            //         $hours = 0;
            //         $min = 0;
            //         $sec = 0;
            //     }
            // }

            // if(isset($data->v_duration_in) && $data->v_duration_in=="day"){
                
            //     if(isset($data->v_duration_time)){
            //         $day = $data->v_duration_time;
            //     }
            //     $cdate=date("Y-m-d H:i:s");
            //     $odate = $enddate;
            //     $seconds = strtotime($cdate)-strtotime($odate);
            //     $daydiff = ceil(($seconds / (60 * 60 * 24)));
            //     $totalsecd = $day*86400;    
            //     $seconds1 = $totalsecd - $seconds;
                
            //     if($day>$daydiff){
            //         $day=0;
            //         //$day = $day - $daydiff;  
            //         //$hours = date("H",$seconds);
            //         //$min = date("i",$seconds);
            //         $sec = $seconds1;//date("s",$seconds);
            //     }
            // }

            // if(isset($data->v_duration_in) && $data->v_duration_in=="month"){

            //     if(isset($data->v_duration_time)){
            //         $day = $data->v_duration_time*30;
            //     }

            //     $cdate=date("Y-m-d H:i:s");
            //     $odate = $enddate;
            //     $seconds = strtotime($cdate)-strtotime($odate);
            //     $daydiff = ceil($seconds / (60 * 60 * 24));
                
            //     if($day>$daydiff){
            //         $day = $day-$daydiff;  
            //         $hours = date("H",$seconds);
            //         $min = date("i",$seconds);
            //         $sec = date("s",$seconds);
            //     }
            // }

            // if(isset($data->v_duration_in) && $data->v_duration_in=="year"){

            //     if(isset($data->v_duration_time)){
            //         $day = $data->v_duration_time*288;
            //     }

            //     $cdate=date("Y-m-d H:i:s");
            //     $seconds = strtotime($cdate)-strtotime($enddate);
            //     $daytotal = $seconds / (60 * 60 * 24);

            //     if($day>$daytotal){
            //         $a = strtotime($totalduration)-$seconds;
            //         $hours = date("H",$a);
            //         $min = date("i",$a);
            //         $sec = date("s",$a);
            //     }else{
            //         $day=0;
            //     }
            // }
        
    @endphp
    
    @if(isset($data->v_order_start) && $data->v_order_start=="yes")
    <div class="container">
        <div class="timer-date">
            <div class="countdown_dashboard">
            <div class="timer-date-section dash day_dash">
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="text-days">
                    DAYS
                </div>
            </div>
            <div class="timer-date-section dash hour_dash">
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="text-days">
                    HOURS
                </div>
            </div>
            <div class="timer-date-section dash min_dash">
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="text-days">
                    MINUTES
                </div>
            </div>
            <div class="timer-date-section dash sec_dash">
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="text-days">
                    SECONDS
                </div>
            </div>
        </div>
        </div>
    </div>
    @else
    <div class="container">
        <div class="timer-date">
            <div class="countdown_dashboard">
            <div class="timer-date-section dash day_dash">
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="text-days">
                    DAYS
                </div>
            </div>
            <div class="timer-date-section dash hour_dash">
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="text-days">
                    HOURS
                </div>
            </div>
            <div class="timer-date-section dash min_dash">
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="text-days">
                    MINUTES
                </div>
            </div>
            <div class="timer-date-section dash sec_dash">
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="timer-date-box digit">
                    0
                </div>
                <div class="text-days">
                    SECONDS
                </div>
            </div>
        </div>
        </div>
    </div>
    
    @endif
    @php 
        $submitorderreq=0;
    @endphp
    <div class="container">

        {{-- @if ($success = Session::get('success'))
          <div class="alert alert-success alert-big alert-dismissable br-5">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-info alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
          </div>
        @endif --}}

        <div class="accordion-final-progress">
            <div class="accordion accordion-next">
                <h4 class="accordion-toggle-progress">ORDER REQUIREMENTS
                    @if(isset($data->v_order_requirements) && $data->v_order_requirements=="yes")
                    <p>Please fill out order requirements</p>
                    @else
                    <p>You’ve filled out the requirements</p>
                    @endif
                </h4>
                <div class="accordion-content-progress">
                    <div id="msgconver">
                    @if(isset($orderRequirements) && count($orderRequirements))
                        @foreach($orderRequirements as $k=>$v)

                            @if($v->e_type == "order_deliver")
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>HERE'S YOUR DELIVERY!</p>
                                    <p class="litral-text">Your order delivered</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i d M Y",strtotime($v->d_added)) : ''}}.</p>
                                </div>

                            @elseif($v->e_type == "not_satisfied")
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>I'm not Satisfied</p>
                                    <p class="litral-text">You are not satisfied with this order.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i d M Y",strtotime($v->d_added)) : ''}}.</p>
                                </div>

                            @elseif($v->e_type == "order_cancel_req")
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>Order Cancel Request</p>
                                    <p class="litral-text">You have requested to cancel the order.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i d M Y",strtotime($v->d_added)) : ''}}.</p>
                                </div>  

                             @elseif($v->e_type == "order_cancelled")
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>Order Cancelled</p>
                                    <p class="litral-text">Your order is cancelled by the Seller.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i d M Y",strtotime($v->d_added)) : ''}}.</p>
                                </div> 

                            @elseif($v->e_type == "req_posted")
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>Order Requirements </p>
                                    <p class="litral-text">You've filled out the order requirements.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i d M Y",strtotime($v->d_added)) : ''}}.</p>
                                </div> 

                                @php 
                                    $submitorderreq=2;
                                @endphp   

                            @elseif($v->e_type == "extendtime")
                                <div id="extendconvetion_{{$v->id}}">
                                @if($v->v_extend_resquest_status == "pending")
                                    <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                        <p>Extend Order Request</p>
                                        <p class="litral-text">Seller, requested to extend the order delivery by {{isset($v->v_duration_time) ? $v->v_duration_time : ''}} {{isset($v->v_duration_in) ? $v->v_duration_in : ''}}.</p>
                                        <p class="litral-text">{{isset($v->d_added) ? date("H:i d M Y",strtotime($v->d_added)) : ''}}.</p>
                                        <div class="text-center">
                                            <button type="button" onclick="extendRequest('{{$v->id}}','reject')" class="btn form-save-exit">Cancel</button>
                                            <button type="button" onclick="extendRequest('{{$v->id}}','accept')" class="btn form-next"> Accept </button>
                                        </div>
                                    </div>
                                @elseif($v->v_extend_resquest_status == "reject")  
                                    <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                        <p>Extend Order Request</p>
                                        <p class="litral-text">You have rejected seller request to extend the order delivery by {{isset($v->v_duration_time) ? $v->v_duration_time : ''}} {{isset($v->v_duration_in) ? $v->v_duration_in : ''}}.</p>
                                        <p class="litral-text">{{isset($v->d_added) ? date("H:i d M Y",strtotime($v->d_added)) : ''}}.</p>
                                    </div> 
                                 @else
                                    <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                        <p>Order Extended</p>
                                        <p class="litral-text">You have accepted seller request to extend the order delivery by {{isset($v->v_duration_time) ? $v->v_duration_time : ''}} {{isset($v->v_duration_in) ? $v->v_duration_in : ''}}.</p>
                                        <p class="litral-text">{{isset($v->d_added) ? date("H:i d M Y",strtotime($v->d_added)) : ''}}.</p>
                                    </div>
                                @endif
                                </div>
                            @elseif($v->e_type == "requirements")
                                <div class="accordion-content-progress1">
                                    <div class="ans-qus">
                                        <p><span>1.</span>Please provide with your details</p>
                                    </div>
                                    <div class="ans-qus-final">
                                        @php
                                            if(isset($v->l_message)){
                                                echo $v->l_message;
                                            }
                                        @endphp
                                    </div>
                                </div>
                                
                                @php
                                    $submitorderreq=1;    
                                @endphp

                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>Order Requirements</p>
                                    <p class="litral-text">Please fill out your order requirements.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i d M Y") : ''}}.</p>
                                </div>

                                {{-- @if(isset($data->v_order_start) && $data->v_order_start=="yes")
                                    <div class="accepted-msg">
                                        <p>Order Requirements</p>
                                        <p class="litral-text">Please filled out your order requirements.</p>
                                        <p class="litral-text">{{isset($v->d_added) ? date("H:i d M Y") : ''}}.</p>
                                    </div>
                                @endif --}}

                            @else

                                <div class="accordion-content-progress1">
                                    <div class="ans-qus-final">
                                        <div class="Announcements-find-progress">
                                            
                                            <div class="Announcements-man">
                                                @php
                                                    $user=array();
                                                    if(count($v->hasUser())){
                                                        $user = $v->hasUser();
                                                    }
                                                    $imgdata = \App\Helpers\General::userroundimage($user);
                                                    echo $imgdata;
                                                @endphp
                                            </div>

                                            <div class="Announcements-p-tag">
                                                <p>
                                                    @php
                                                        if(isset($user['v_fname']) && $user['v_lname']){
                                                            echo $user['v_fname'].' '.$user['v_lname'];
                                                        }    
                                                    @endphp 
                                                </p>
                                                <span class="span-text" style="width: 100% !important;">
                                                    @php
                                                        if(isset($v->l_message)){
                                                            echo $v->l_message;
                                                        }
                                                    @endphp
                                                </span>
                                                @if(isset($v->v_document) && $v->v_document!="" && count($v->v_document))
                                                    @foreach($v->v_document as $key=>$val)
                                                        @php
                                                            $durl = \Storage::cloud()->url($val);
                                                            $docname="";
                                                            if($v!=""){
                                                                $docname = explode("/", $val);
                                                            }
                                                            $docname = str_replace("=", '.', $key);    
                                                        @endphp
                                                        <p>
                                                        <a href="{{$durl}}" target="_blank">    
                                                        <img src="{{url('public/Assets/frontend/images/link-save.png')}}" alt="" /><span style="margin-left: 5px">{{$docname}}</span></p>
                                                        </a>
                                                            
                                                    @endforeach
                                                @endif

                                                <p class="time-p-tag"> {{date("H:i M d, Y",strtotime($v->d_added))}} </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        @endforeach
                    @endif
                    </div>    

                    @if(isset($data->v_order_status) && $data->v_order_status=="active")

                        <form class="horizontal-form" role="form" method="POST" name="submitreq" id="submitreq" action="{{url('buyer/order-post-requirement')}}" data-parsley-validate enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <input type="hidden" name="v_order_id" value="{{$data->id}}">
                        <div class="ans-qus-final-progress">
                            <div class="ans-qus-final-counter">
                                <textarea cols="200" name="l_message" rows="8" class="char-counter" placeholder="Type your message here...." required></textarea>
                            </div>
                            <div class="link-img">
                                <input type='file' name="v_document_order[]" multiple id="fileupload-down" style="display: none;" onchange="docuploaddata();" />
                                <label id="fileupload-down" for="fileupload-down"><img src="{{url('public/Assets/frontend/images/link-save.png')}}" alt="" /></label>
                            </div>
                        </div>

                        @if($submitorderreq)
                            <input type="hidden" name="v_order_reqpost" value="{{$submitorderreq}}">
                        @endif
                        <div class="ans-qus-final-progress2 pull-right">
                            <ul class="pull-right" id="docupload">
                            </ul>
                        </div>
                        <script type="text/javascript">
                            function docuploaddata(){
                                var input = document.getElementById('fileupload-down');
                                var list = document.getElementById('docupload');
                                var strappend="";
                                for (var x = 0; x < input.files.length; x++) {
                                    strappend = strappend+"<li>"+input.files[x].name+"</li>";
                                }
                                $("#docupload").html(strappend);
                            }
                        </script>

                        <div class="ans-qus-final-progress2">
                            <div class="btn-textarea-text">
                                <button type="submit" class="btn btn-post-message">Post Message</button>
                            </div>
                        </div>
                        </form>

                    @elseif(isset($data->v_order_status) && !isset($data->v_buyer_deliver_status) && $data->v_order_status=="delivered")
                        
                        <form class="horizontal-form" role="form" method="POST" name="submitreqsatisfied" id="submitreqsatisfied" action="{{url('buyer/order-post-requirement')}}" data-parsley-validate enctype="multipart/form-data" >
                        {{ csrf_field() }}

                        <input type="hidden" name="v_order_id" value="{{$data->id}}">
                        <input type="hidden" name="v_satisfied" id="v_satisfied" value="1">
                        
                        <div class="ans-qus-final-progress">
                            <div class="ans-qus-final-counter">
                                <textarea cols="200" name="l_message" rows="8" class="char-counter" placeholder="Type your message here..."></textarea>
                            </div>
                            <div class="link-img">
                                <input type='file' name="v_document_order[]" multiple id="fileupload-down" style="display: none;" onchange="docuploaddata();" />
                                <label id="fileupload-down" for="fileupload-down"><img src="{{url('public/Assets/frontend/images/link-save.png')}}" alt="" /></label>
                            </div>
                        </div>

                        <div class="ans-qus-final-progress2 pull-right">
                            <ul class="pull-right" id="docupload">
                            </ul>
                        </div>
                        <script type="text/javascript">
                            function docuploaddata(){
                                var input = document.getElementById('fileupload-down');
                                var list = document.getElementById('docupload');
                                var strappend="";
                                for (var x = 0; x < input.files.length; x++) {
                                    strappend = strappend+"<li>"+input.files[x].name+"</li>";
                                }
                                $("#docupload").html(strappend);
                            }
                        </script>

                        <div class="ans-qus-final-progress2">
                            <div class="btn-textarea-text">
                            <button type="button" onclick="orderAccept()" class="btn btn-Review-Order" >Accept & Review Order</button>
                            <button type="submit" class="btn btn-Review-Order">I'm not satisfied</button>
                            </div>
                        </div>
                        </form>

                    @elseif(isset($data->v_order_status) && $data->v_order_status=="delivered")
                        @php
                            $compdate = $data->d_order_completed_date;
                            $currentdate = date("Y-m-d H:i:s",strtotime("+ 1 day"));
                        @endphp
                       
                        @if(isset($sellerreview) && count($sellerreview))
                            <div class="accordion-content-progress1">
                                <div class="ans-qus-final text-center">
                                    <div class="accepted-msg1">
                                        <p data-toggle="modal" data-target="#leaveReview">ORDER REVIEWS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-content-progress1">
                                <div class="ans-qus-final">
                                    <div class="Announcements-find-progress">
                                        <div class="Announcements-man">
                                            @php
                                                $imgdata = \App\Helpers\General::userroundimage($userdata);
                                                echo $imgdata;
                                            @endphp
                                        </div>
                                        
                                        <div class="Announcements-p-tag">
                                            <p>Me</p>
                                            <div class="all-in-one-star">
                                                <span class="span-text">
                                                    {{isset($sellerreview->l_comment) ? $sellerreview->l_comment : ''}}
                                                </span>
                                            </div>
                                            
                                            <div class="all-in-one-star">
                                                <span class="span-text">
                                                    Communication with seller
                                                </span>
                                                <div class="rating-Course">
                                                    <div class="rating-buyer">
                                                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" value="{{isset($sellerreview->i_communication_star) ? $sellerreview->i_communication_star : ''}}" readonly>
                                                        <span class="star-no">( {{$sellerreview->i_communication_star}} )</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all-in-one-star">
                                                <span class="span-text">
                                                    Service as Described
                                                </span>
                                                <div class="rating-Course">
                                                    <div class="rating-buyer">
                                                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{isset($sellerreview->i_described_star) ? $sellerreview->i_described_star : ''}}" readonly>
                                                        <span class="star-no">( {{$sellerreview->i_described_star}} )</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all-in-one-star">
                                                <span class="span-text">
                                                    Buy Again or Recommend
                                                </span>
                                                <div class="rating-Course">
                                                    <div class="rating-buyer">
                                                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{isset($sellerreview->i_recommend_star) ? $sellerreview->i_recommend_star : ''}}" readonly>
                                                        <span class="star-no">( {{$sellerreview->i_recommend_star}} )</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            @if(isset($sellerreview->l_answers) && count($sellerreview->l_answers))
                            
                            <div class="accordion-content-progress1">
                                <div class="ans-qus-final">
                                    <div class="Announcements-find Announcements-find-q">
                                        <div class="Announcements-man">
                                            @php
                                                $v_image="";
                                                if(count($sellerreview->hasSeller()) && isset($sellerreview->hasSeller()->v_image)){
                                                    $v_image=$sellerreview->hasSeller()->v_image;
                                                }
                                                $v_fname="";
                                                if(count($sellerreview->hasSeller()) && isset($sellerreview->hasSeller()->v_fname)){
                                                    $v_fname=$sellerreview->hasSeller()->v_fname;
                                                }

                                                $v_lname="";
                                                if(count($sellerreview->hasSeller()) && isset($sellerreview->hasSeller()->v_lname)){
                                                    $v_lname=$sellerreview->hasSeller()->v_lname;
                                                }
                                            @endphp
                                            @php
                                                  $imgdata = '';
                                                  if(Storage::disk('s3')->exists($v_image)){
                                                      $imgdata = \Storage::cloud()->url($v_image);
                                                  }else{
                                                      $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                                  }
                                            @endphp  
                                           <img src="{{$imgdata}}" alt="" />

                                        </div>
                                        <div class="Announcements-name Announcements-name-q">
                                            <p>{{$v_fname}} {{$v_lname}}</p>
                                            <span class="span-text">
                                                {{isset($sellerreview->l_answers['l_comment']) ? $sellerreview->l_answers['l_comment'] : ''}}
                                            </span>
                                            @php
                                                $rdata = App\Helpers\General::sellerReview($sellerreview->i_seller_id);
                                            @endphp
                                            
                                            {{-- <div class="rating-complted">
                                                <div class="rating-buyer">
                                                    <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$rdata['avgtotal']}}" readonly>
                                                    <span class="star-no">( {{$rdata['avgtotal']}} )</span>
                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif


                        @elseif(strtotime($compdate)<strtotime($currentdate))

                            <div class="accordion-content-progress1">
                                <div class="ans-qus-final text-center">
                                    <div class="accepted-msg1">
                                        <p data-toggle="modal" style="cursor: pointer;" data-target="#acceptReviewOrder">CLICK HERE ORDER REVIEWS</p>
                                    </div>
                                </div>
                            </div>
                            
                        @endif        
                        @php 
                            $useridseller="";
                            if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->id)){
                                $useridseller = $sellerprofile->hasUser()->id;
                            }
                        @endphp
                        
                        <div class="ans-qus-final-progress-not">
                            <div class="social-order">
                                <h3>ORDER COMPLETED</h3>
                                <p> This order is complete. <a href="javascript:;" onclick="SendMessage('{{$useridseller}}','{{$sellerprofile->id}}')"> Click here </a> to contact the seller.
                                </p>
                            </div>
                        </div>
                    @endif


                </div>
            </div>
            <div id="footercommonmsg" style="margin-top: 20px"></div>
        </div>
    </div>
    <!-- 26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS-in progress -->

@stop
@section('js')
<script type="text/javascript">


    function SendMessage(id,pid){
    
    var actionurl = "{{url('message/userLogin/check')}}";
    //var formData = "i_to_id="+id;
    var formData = "i_to_id="+id+"&from=buyer&pid="+pid;

    $.ajax({
        processData: false,
        contentType: false,
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            document.getElementById('load').style.visibility='hidden';
            var obj = jQuery.parseJSON(res);
            if(obj['status']==1){
                $("#contactsellerdetail").html(obj['responsestr']);
                $("#popupmessage").html(obj['msg']);
                $("#sendMessage").modal("show");
                
            }else{
              window.location = "{{url('login')}}";
            }
        },
        error: function ( jqXHR, exception ) {
            document.getElementById('load').style.visibility='hidden';
            $("#successmsgpop").css("display","none");
            $("#errormsgpop").css("display","block");
            $("#popupmessage").html("Something went wrong.please try again after sometime.");
            //$("#sendMessageSuccess").modal("show");
            $("#sendMessage").modal("hide");
        }
    });
  }


  function submitMessage(type=""){
            
      var actionurl = "{{url('message/sendMessageSeller')}}";
      var formValidFalg = $("#sendMessageForm").parsley().validate('');
     
      if(formValidFalg){
        
          document.getElementById('load').style.visibility="visible";  
          var formData = new FormData($('#sendMessageForm')[0]);
          
          $.ajax({
              processData: false,
              contentType: false,
              type    : "POST",
              url     : actionurl,
              data    : formData,
              success : function( res ){
                  document.getElementById('load').style.visibility='hidden';
                  var obj = jQuery.parseJSON(res);
                  if(obj['status']==1){
                    $("#popupmessage").html(obj['msg']);
                    $("#sendMessageSuccess").modal("show");
                    $("#sendMessage").modal("hide");
                  }else{
                    $("#popupmessage").html(obj['msg']);
                    $("#sendMessageSuccess").modal("show");
                    $("#sendMessage").modal("hide");
                  }
              },
              error: function ( jqXHR, exception ) {
                  document.getElementById('load').style.visibility='hidden';
                  $("#popupmessage").html("Something went wrong.please try again after sometime.");
                  $("#sendMessageSuccess").modal("show");
                  $("#sendMessage").modal("hide");
              }
          });

      }    
  }

</script>
    
 <script type="text/javascript">
        $(document).ready(function() {
            $(".accordion-content-progress").show();
            $('.accordion').find('.accordion-toggle-progress').click(function() {
                $(this).next().slideToggle('600');
                $(".accordion-content-progress").not($(this).next()).slideUp('600');
            });
            $('.accordion-toggle-progress').on('click', function() {
                $(this).toggleClass('active').siblings().removeClass('active');
            });
        });
    </script>
    
    <script type="text/javascript">
        (function($) {
            "use strict";

            $.fn.charCounter = function(options) {
                String.prototype.format = function() {
                    var content = this;
                    for (var i = 0; i < arguments.length; i++) {
                        var replacement = '{' + i + '}';
                        content = content.replace(replacement, arguments[i]);
                    }
                    return content;
                };

                var options = $.extend({
                    backgroundColor: "#FFFFFF",
                    position: {
                        bottom: -10,
                        left: 20
                    },
                    font: {
                        size: 16,
                        color: "#08090a"
                    },
                    limit: 2500
                }, options);

                return this.each(function() {
                    var el = $(this),
                        wrapper = $("<div/>").css("position", "relative").css("display", "inline-block"),
                        label = $("<span/>").text("{0}/{1}".format(0, options.limit)).css({
                            "backgroundColor": options.backgroundColor,
                            "position": "absolute",
                            "font-size": options.font.size,
                            "color": options.font.color
                        }).css(options.position);

                    el.prop("maxlength", options.limit);
                    el.wrap(wrapper);
                    el.before(label);
                    el.on("keyup", updateLabel).on("keypress", updateLabel).on('keydown', updateLabel);

                    function updateLabel(e) {
                        label.text("{0}/{1}".format($(this).val().length, options.limit));
                    }
                });
            }
        })(jQuery);

        $(document).ready(function() {
            $(".char-counter").charCounter();
        });
    </script>

    <script type="text/javascript">
        

        function extendRequest(id="",action=""){
                
            var actionurl = "{{url('buyer/orders-extend-request')}}";
            var formData = "id="+id+"&action="+action;

            $.ajax({
                
                type    : "get",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                  
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#extendconvetion_"+id).html(obj['btnstr']);  
                        $("#footercommonmsg").html(obj['msg']);  
                  
                    }else{
                        $("#footercommonmsg").html(obj['msg']);  
                    }

                  },
                  error: function ( jqXHR, exception ) {
                      $("#errormsgstep2").show();
                  }
            });
        }

        function orderAccept(){

            var actionurl = "{{url('buyer/order-accept')}}";
            var formValidFalg = $("#submitreqsatisfied").parsley().validate('');
            $("#v_satisfied").val("0");

            if(formValidFalg){
              var formData = new FormData($('#submitreqsatisfied')[0]);
              $.ajax({
                  processData: false,
                  contentType: false,
                  type    : "POST",
                  url     : actionurl,
                  data    : formData,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#msgconver").append(obj['msgstr']);  
                        $("#footercommonmsg").html(obj['msg']);  
                        $("#acceptReviewOrder").modal("show");
                      }else{
                        $("#footercommonmsg").html(obj['msg']);  
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      
                  }
              });
          }
        }

        function submitreview(){

            var actionurl = "{{url('buyer/order-reviews')}}";
            var formValidFalg = $("#orderreview").parsley().validate('');
            var orderid = $("#i_order_id_review").val();
                
            if(formValidFalg){

                document.getElementById('load').style.visibility="visible";                
                var formdata = $("#orderreview").serialize();

                $.ajax({
                    type    : "POST",
                    url     : actionurl,
                    data    : formdata,
                    success : function( res ){
                        
                        var obj = jQuery.parseJSON(res);
                        if(obj['status']==1){
                            window.location = "{{url('buyer/orders/detail')}}/"+orderid;
                        }else{
                            $("#popupcommonmsg").html(obj['msg']);  
                        }
                    },
                    
                    error: function ( jqXHR, exception ) {
                        $("#footercommonmsg").html(obj['msg']);  
                    }

                });
            }
        }

        var sec = '{{$sec}}';    
        var min = '{{$min}}';    
        var hourstotal = '{{$hours}}';
        var daytotal = '{{$day}}';    

        $('.countdown_dashboard').countdown();
        $('.countdown_dashboard').data('countdown').update({
              day: parseInt(daytotal),
              hour: parseInt(hourstotal),
              min: parseInt(min),
              sec: parseInt(sec)
        });
        $('.countdown_dashboard').countdown('stop');

        var timet='{{$data->v_order_start}}';
        if(timet!="yes"){
            setTimeout(function(){ 
                $('.countdown_dashboard').data('countdown').stop({
                  day: parseInt(daytotal),
                  hour: parseInt(hourstotal),
                  min: parseInt(min),
                  sec: parseInt(sec)
                });
             }, 1500);
        }

         @if(isset($data->v_order_status) && $data->v_order_status=="cancelled")

            setTimeout(function(){ 
                $('.countdown_dashboard').data('countdown').stop({
                  day: parseInt(daytotal),
                  hour: parseInt(hourstotal),
                  min: parseInt(min),
                  sec: parseInt(sec)
                });
             }, 1500);
        
        @endif
       
    </script>


@stop

