@extends('layouts.frontend')


@section('content')
<style type="text/css">
    /*footer {margin-top: 0px;position: relative;}
    body {    padding-bottom: 0px !important;}*/
    .wrapper {
         margin-bottom: 0; 
    }
</style>
<div class="single_search_page">
  <div class="container-fluid">
        <div class="row">
            <div class="header-img">
                @php
                    
                    // $v_banner_image="";
                    // if(isset($homepage) && count($homepage) && isset($homepage[0])){
                    //     $v_banner_image = $homepage[0];
                    // }  
                    // else{
                    //     $v_banner_image = url('public/Assets/frontend/images/header.png');
                    // }  

                    $v_banner_image="";
                    if($metaDetails['v_image'] && $metaDetails['v_image']!=""){
                        if(Storage::disk('s3')->exists($metaDetails['v_image'])){
                            $v_banner_image = \Storage::cloud()->url($metaDetails['v_image']);      
                        }else{
                            $v_banner_image = url('public/Assets/frontend/images/one-page.png');
                        }                
                    }else{
                        $v_banner_image = url('public/Assets/frontend/images/one-page.png');
                    }
                    
                @endphp
                <img src="{{$v_banner_image}}" class="img-responsive" alt="" />
                
                <div class="center-containt">
                    <div class="container">
                        <div class="slide-containt">
                            <p>Discover<span> . </span>Select<span> . </span>Connect<span> . </span></p>
                        </div>
                        <div class="free-lancer">
                            <h1>Find your Perfect FREELANCER</h1>
                        </div>
                        <div class="free-lancer">
                            <p>Feel the Freedom <span><img src="images/camera.png" alt="" /></span></p>
                        </div>
                        
                        <form class="horizontal-form" role="form" method="get" name="searchmaster" id="searchmaster" action="{{url('search')}}" >

                        <div class="input-box-slide-second">
                            <div class="new-location1">
                                <div class="new-location">
                                    <div class="select-box-find">
                                        <select id="generalsearch" name="e_type" onchange="searchhomepage(this.value)" style="height: 50px;">
                                            <option value="inperson">In Person</option>
                                            <option value="online">Online</option>
                                            <option value="courses">Courses</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div style="display: inline-flex;" id="searchcontent"> 
                                   
                                    <div class="search-to-find" id="inpersonmasterskill">
                                        <input type="text" class="form-control" name="text" placeholder="Find your perfect skill or job" style="height: 50px;" >
                                    </div>

                                    <div class="Location-line" id="inpersonmasterlocation">
                                        <input type="text" class="form-control" name="text" id="autocomplete" placeholder="Location" style="height: 50px;" autocomplete="off" oninvalid="this.setCustomValidity('Enter a location to see results close by')" oninput="setCustomValidity('')">
                                    </div>

                                    <input type="hidden" name="v_latitude" id="v_latitude">
                                    <input type="hidden" name="v_longitude" id="v_longitude">

                                </div>

                                <div class="btn-perfect1">
                                    <button type="submit" class="btn" style="height: 50px;">Get Started</button>
                                </div>

                            </div>
                        </div>
                        </form>
                        <div class="guide">
                            <a href="{{url('pages/how-to-guide')}}" target="_blank">
                            <button class="btn btn-guide">how to guide?</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div>
</div>
@stop


@section('js')
<script type="text/javascript">

function  searchhomepage(data) {

    var searchstr="";
    
    if(data=="inperson"){
        searchstr+='<div class="search-to-find" id="inpersonmasterskill">';
        searchstr+='<input type="text" class="form-control" name="text" placeholder="Find your perfect skill or job" style="height: 50px;">';
        searchstr+='</div>';
        searchstr+='<div class="Location-line" id="inpersonmasterlocation">';
        
        // searchstr += '<input type="text" name="text" id="autocomplete" required class="form-control manu-map" placeholder="Location"';
        // searchstr += 'oninvalid="this.setCustomValidity';
        // searchstr += "('Enter a location to see results close by')";
        // searchstr += '" oninput="setCustomValidity()" >';
        
        searchstr+='<input type="text" class="form-control" name="text" id="autocomplete" placeholder="Location" style="height: 50px;" autocomplete="off" ';
        searchstr += 'oninvalid="this.setCustomValidity';
        searchstr += "('Enter a location to see results close by')";
        searchstr += '" oninput="setCustomValidity()" >';

        searchstr+='<input type="hidden" name="v_latitude" id="v_latitude">';
        searchstr+='<input type="hidden" name="v_longitude" id="v_longitude">';
        searchstr+='</div>';

    }else if(data=="online"){
        searchstr+='<div class="search-to-find" id="onlinemaster">';
        searchstr+='<input type="text" class="form-control form-online" name="keyword" placeholder="Find your perfect skill or job" style="height: 50px;">';
        searchstr+='</div>';

    }else{
        searchstr+='<div class="search-to-find" id="coursesmaster">';
        searchstr+='<input type="text" class="form-control form-online" name="keyword" placeholder="Find your perfect course" style="height: 50px;">';
        searchstr+='</div>';
    }
    $("#searchcontent").html(searchstr);
    initAutocomplete();




}

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeUrl5sq-j9P3aar5jKpOiTqralR5T5GE&libraries=places&callback=initAutocomplete" async defer></script>
<script>

        var placeSearch, autocomplete;
        var componentForm = {
            premise: 'long_name',
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'long_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode'],,componentRestrictions: {country: "uk"}});
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {

            var place = autocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            $("#v_latitude").val(lat);
            $("#v_longitude").val(lng);

            var premise = '';
            var street_number = '';
            var route = '';

            for ( var i = 0; i < place.address_components.length; i++ ) {

                var addressType = place.address_components[i].types[0];

                if ( componentForm[addressType] && addressType == 'premise') {
                    premise = place.address_components[i][componentForm[addressType]];
                }
                if ( componentForm[addressType] && addressType == 'street_number') {
                    street_number = place.address_components[i][componentForm[addressType]];
                }
                if ( componentForm[addressType] && addressType == 'route') {
                    route = place.address_components[i][componentForm[addressType]];
                }
                else if ( componentForm[addressType] && addressType == 'postal_code') {
                    $('#v_pincode').val( place.address_components[i][componentForm[addressType]] );
                }
            }

            var address = '';

            if( premise != '' ) {
                address += ' ' + premise;
            }
            else {
                address += premise;
            }

            if( street_number != '' ) {
                address += ' ' + street_number;
            }
            else {
                address += street_number;
            }

            if( route != '' ) {
                address += ' ' + route;
            }
            else {
                address += route;
            }
            //$('#v_city').val(address);

        }

            function geolocate() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        var circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocomplete.setBounds(circle.getBounds());
                    });
                }
            }
            </script>
@stop

