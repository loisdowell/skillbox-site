@extends('layouts.frontend')

@section('content')

<style type="text/css">
    .letter-text-myprifile {
        text-align: center;
        font-size: 34px;
        padding: 10px 0px;
        color: rgb(231, 14, 138);
    }
    .new-seller-find {border-radius: 20px;
    margin-right: 10px;}
    .new-submit-live {height: 34px;}
    .new-submit-comment{display: inline-flex;width: 100%}
</style>


     <!-- Modal -->
    <div class="modal  fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <div class="modal-content modal-content-1">
                <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title">Submit Question</h4>
                </div>

                <div id="commonpopmsg"></div>
                <div class="alert alert-info alert-danger" id="commonpopuperr" style="display: none" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;Something went wrong.please try again after sometime.
                </div>
                

                <form role="form" method="POST" name="coursesqa" id="coursesqa" data-parsley-validate enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="buyer_courses_id" value="{{$buyercoursedata->id}}">
                <input type="hidden" name="i_courses_id" value="{{$buyercoursedata->i_course_id}}">
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="text-box-containt box-containt-que">
                                    <label>Question Title</label>
                                    <input type="text" name="v_title" class="form-control title-que" required>
                                </div>

                                <div class="text-box-containt">
                                    <label>Your Question</label>
                                    <textarea rows="4" name="l_questions" class="form-control popup-letter" required></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button type="button" onclick="submitqestion()" class="btn btn-Submit-pop">Submit Question</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>

                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->


    <!--26b-control-panel-my-courses-overviwe-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> My Courses Details </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-detail">
        
        <div class="container">
            <ul class="nav nav-pills nav-pills-new">
                
                <li><a href="{{url('buyer/courses/overview')}}/{{$buyercoursedata->id}}">Overview </a></li>
                <li><a href="{{url('buyer/courses/section')}}/{{$buyercoursedata->id}}">Course Content</a></li>
                
                @if(isset($data->e_qa_enabled) && $data->e_qa_enabled=="on")
                <li class="active" ><a href="{{url('buyer/courses/qa')}}/{{$buyercoursedata->id}}">Q&A</a></li>
                @endif
                
                @if(isset($data->e_reviews_enabled) && $data->e_reviews_enabled=="on")
                <li><a href="{{url('buyer/courses/reviews')}}/{{$buyercoursedata->id}}"> Course Reviews</a></li>
                @endif
                
                @if(isset($data->e_announcements_enabled) && $data->e_announcements_enabled=="on")
                <li><a href="{{url('buyer/courses/announcements')}}/{{$buyercoursedata->id}}">Announcements</a></li>
                @endif

            </ul>
        </div>

    </div>


    <!-- Tab panes -->  
    <div class="container">

            <form role="form" method="GET" data-parsley-validate enctype="multipart/form-data" id="qasearch" style="width: 100%"  action="" >
            <div class="qa-find">
                <input type="text" name="search" onkeyup="searchfaq(this.value)" class="form-control text-search" placeholder="Search for a question" required  @if(isset($_REQUEST['search'])) value="{{$_REQUEST['search']}}" @endif>
                <input type="hidden" name="icid" value="{{$buyercoursedata->id}}" id="icid">
                </form>
                <span> or </span>
                <button type="button" class="btn ask-question" data-toggle="modal" data-target="#myModal"> Ask a New Question </button>
            </div>
            
        <!-- 26B-control-panel-my-courses-q&a-seller -->
        <div id="commonmsg"></div>
        
        <div id="searchdata">
        @if(isset($coursesQA) && count($coursesQA))        
            @foreach($coursesQA as $key=>$val)

                <div class="accordion-final-announc final-announc-Q-a" @if($key=="0") active @endif>
                    <div class="accordion accordion-announcement">
                        
                        <div class="accordion-toggle1 accordion-toggle-qa">
                            <div class="Announcements-find Announcements-q-a">
                                <?php
                                    $imgdata="";
                                    if(isset($userList[$val->i_user_id])){
                                        $imgdata = \App\Helpers\General::userroundimage($userList[$val->i_user_id]);
                                    }
                                ?>
                                <div class="Announcements-man">
                                    <?php 
                                        echo $imgdata;
                                    ?>    
                                </div>
                                <div class="Announcements-name q-a-name">
                                    <p>{{count($val->hasUser()) ? ucfirst($val->hasUser()->v_fname).' '.ucfirst($val->hasUser()->v_lname) : '-'}}</p>
                                    @php 
                                        $humanTime= App\Helpers\General::time_elapsed_string(strtotime($val->d_added));
                                    @endphp
                                    <p><span>{{$humanTime}}</span></p>
                                    <p>{{$val->v_title}}</p>
                                    <p class="ans-q">{{isset($val->l_questions) ? $val->l_questions:''}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-content" @if($key=="0") style="display: block;" @endif>
                            <div class="Announcements-text">
                                
                            <div id='commentsstr{{$val->id}}'>
                                @if(isset($val->l_answers) && count($val->l_answers))        
                                    @foreach($val->l_answers as $k=>$v)

                                        <div class="Announcements-second-man qaseller-second-man">
                                            <div class="qa-seller-find">
                                                                                                        
                                                <div class="Announcements-man">
                                                    <?php
                                                        $imgdata="";
                                                        if(isset($userList[$v['i_user_id']])){
                                                            $imgdata = \App\Helpers\General::userroundimage($userList[$v['i_user_id']]);
                                                        }
                                                        echo $imgdata;
                                                    ?>    
                                                </div>
                                                <div class="Announcements-name">
                                                    <p>
                                                    <?php
                                                        $username="";
                                                        if(isset($userList[$v['i_user_id']])){
                                                            $username = $userList[$v['i_user_id']]['v_name'];
                                                        }
                                                        echo $username;
                                                    ?>    
                                                    </p>

                                                    <p><span>
                                                        @php 
                                                            $humanTime= App\Helpers\General::time_elapsed_string(strtotime($v['d_added']));
                                                        @endphp
                                                        {{$humanTime}}</span></p>
                                                    <div class="Announcements-message" style="width: 100%">
                                                        {{$v['v_text']}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                </div>

                                <div class="qa-seller-input">
                                    <div class="qa-seller-find">
                                        <div class="Announcements-man">
                                            <?php
                                                
                                                $imgdata = \App\Helpers\General::userroundimage($curentuser);
                                                echo $imgdata;
                                            ?> 

                                        </div>

                                        <form role="form" method="POST" name="commnetform{{$val->id}}" id="commnetform{{$val->id}}" data-parsley-validate enctype="multipart/form-data" style="width: 100%" onsubmit="submitcomment('{{$val->id}}')" action="javascript:;" >
                                        {{ csrf_field() }}
                                        <input type="hidden" name="i_course_qa_id" value="{{$val->id}}">
                                        <div class="Announcements-name">
                                            <p>{{$curentuser['v_name']}}</p>
                                            <div class="new-submit-comment">
                                            <input type="text" class="form-control comment-seller new-seller-find" name="v_text" id="v_text" placeholder="Enter your comment" required>
                                            <button type="button" onclick="submitcomment('{{$val->id}}')" class="btn btn-convarsation-submit new-submit-live"> Submit </button>
                                            </div>
                                        </div>
                                        </form>


                                    </div>
                                </div>
                                <div class="alert alert-info alert-danger" style="display: none;" id="commonerr">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span id="commonerrmsg">Something went wrong.pleease try again after some time.</span>
                                  </div>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        @else
            
            <div class="Course-Reviews">
                 <div class="row">
                    <div class="col-md-6">
                         There are no questions and answers.
                    </div>
                </div>
            </div> 

        
        @endif        
        </div>

    </div>
    <!--26b-control-panel-my-courses-overviwe-->
   

@stop
@section('js')

<script type="text/javascript">

        $(document).ready(function() {
            $('.accordion').find('.accordion-toggle1').click(function() {
                $(this).next().slideToggle('600');
                $(".accordion-content").not($(this).next()).slideUp('600');
            });
            $('.accordion-toggle1').on('click', function() {
                $(this).toggleClass('active').siblings().removeClass('active');
            });
        });

        function submitqestion(){

            
            var actionurl = "{{url('buyer/courses/qa')}}";
            var formValidFalg = $("#coursesqa").parsley().validate('');
           
            if(formValidFalg){
                document.getElementById('load').style.visibility="visible";
                var formdata = $("#coursesqa").serialize();
                $.ajax({
                    type    : "POST",
                    url     : actionurl,
                    data    : formdata,
                    success : function( res ){
                        var obj = jQuery.parseJSON(res);
                        if(obj['status']==1){
                            window.location = "{{url('buyer/courses/qa')}}/"+obj['buyer_courses_id'];
                        }else{
                            $('#commonpopmsg').html(obj['msg']);
                        }
                    },
                    
                    error: function ( jqXHR, exception ) {
                        $('#commonpopuperr').css("display","block");
                    }

                });

            }

        }

        function submitcomment(id=""){
       
            var actionurl = "{{url('buyer/courses/qacomment')}}";
            var formValidFalg = $("#commnetform"+id).parsley().validate('');

            if(formValidFalg){
                document.getElementById('load').style.visibility="visible";
                var formdata = $("#commnetform"+id).serialize();

                $.ajax({
                    type    : "POST",
                    url     : actionurl,
                    data    : formdata,
                    success : function( res ){
                       
                        var obj = jQuery.parseJSON(res);
                        if(obj['status']==1){

                            setTimeout(function(){ 
                                document.getElementById('load').style.visibility="hidden";
                                $('#commentsstr'+id).append(obj['commentstr']);
                            }, 3000);

                            $("#v_text").val("");
                            

                        }else{
                            document.getElementById('load').style.visibility="hidden";
                            $('#commonerr').css("display","block");
                            $('#commonerrmsg').html(obj['msg']);
                        }

                    },
                    
                    error: function ( jqXHR, exception ) {
                        document.getElementById('load').style.visibility="hidden";
                        $('#commonerr').css("display","block");
                    }

                });
            }
        }



    function searchfaq(data){
        
        var keystr = data.length;
        if(keystr>3){

            var actionurl = "{{url('buyer/qa-search')}}";     
            var formData = "search="+data;
            var formData = $("#qasearch").serialize();

            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#searchdata").html(obj['responsestr']);  
                    }
                },
            });
        }else{

            var icid = $("#icid").val();
            var actionurl = "{{url('buyer/qa-search')}}";     
            var formData = "search="+"&icid="+icid;
            
            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#searchdata").html(obj['responsestr']);  
                    }
                },
            });

        }
    }




</script>

@stop

