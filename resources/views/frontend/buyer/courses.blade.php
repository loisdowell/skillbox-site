@extends('layouts.frontend')

@section('content')
<style type="text/css">
    .leval-img img {
    width: 100%;
    min-height: 200px;
    max-height: 200px;
}
</style>
   <!--26B-control-panel-my-courses-1 -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> My Courses </h1>
                </div>
            </div>
        </div>
    </div>

    <!--select-box-->
    <div class="container">
         <div class="sort-for">
            <div class="sort-forep">
                <a href="javascript:;" onclick="viewchange('grid')">
                    <img  id="gridicon" src="{{url('public/Assets/frontend/images/fore.png')}}" class="fore-all" alt="" />
                </a>
                <a href="javascript:;" onclick="viewchange('list')">
                    <img  id="listicon" src="{{url('public/Assets/frontend/images/fore2.png')}}" alt="" />
                </a>
            </div>
        </div>
        <div class="filtaring-main">
            <div class="row">
                
                <form method="GET" name="searchcoursesmaster" id="searchcoursesmaster" action="{{url('buyer/courses')}}" >
                <div class="col-sm-3 pull-right">
                    <select class="form-control form-sel" name="e_status" onchange="masterfilter()">
                        <option value="">Course Status</option>
                        <option value="inprogress" @if(isset($filter['e_status']) && $filter['e_status']=='inprogress') selected @endif>In Progress</option>
                        <option value="not_started" @if(isset($filter['e_status']) && $filter['e_status']=='not_started') selected @endif>Not Started</option>
                        <option value="completed" @if(isset($filter['e_status']) && $filter['e_status']=='completed') selected @endif>Completed</option>
                    </select>
                </div>
                
                
                </form>

            </div>
        </div>
    </div>
    <!--select-box-->

    <div class="container">
    
    <div id="gridview" class="my-courses_listing">
        @if(count($data))
            @foreach($data as $key=>$val)
                <?php 
                $percent = 0;
                if( $val->v_complete_lectures ) $percent = ceil( $val->v_complete_lectures * 100  / $val->v_total_lectures );
                ?>  
                <div class="col-xs-6 col-sm-4 col-md-3 full-size">
                    <div class="all-category all-category-final">
                        <a href="{{url('buyer/courses/overview')}}/{{$val->id}}">
                        <div class="category-img" onclick="playPausevideogrid('{{$val->id}}')">
                           @php
                                $imgins="";
                                if(count($val->hasCourse()) && isset($val->hasCourse()->v_course_thumbnail) && $val->hasCourse()->v_course_thumbnail!=""){
                                    $imgins = $val->hasCourse()->v_course_thumbnail;
                                }else{
                                    $imgins = 'jobpost/photos/No_Image_Available.jpg';
                                }
                                $imgdata = \Storage::cloud()->url($imgins);
                            @endphp
                            <img src="{{$imgdata}}" alt="" />
                        </div>
                        </a>
                        
                        <div class="category-text">
                            <a href="{{url('buyer/courses/overview')}}/{{$val->id}}">
                            <h2 style="min-height:36px;max-height:36px;overflow: hidden;">{{count( $val->hasCourse() ) ? $val->hasCourse()->v_title : ''}}</h2></a>
                           <p style="min-height: 60px;max-height: 60px;overflow: hidden;">
                            @if(count($val->hasCourse()) && isset($val->hasCourse()->v_sub_title))
                                @if(strlen($val->hasCourse()->v_sub_title)>75)
                                    {{substr($val->hasCourse()->v_sub_title,0,75)}}...
                                @else
                                    {{$val->hasCourse()->v_sub_title}}
                                @endif
                            @endif
                            </p>
                            <div class="star-category">
                                <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                                <p class="text-star">
                                {{count( $val->hasCourse() ) ? number_format($val->hasCourse()->i_total_avg_review,1) : '0'}}
                                   <span>({{count( $val->hasCourse() ) ? $val->hasCourse()->i_total_review : ''}})
                                </span></p>
                            </div>
                        </div>
                        @php
                              $hasCourse = $val->hasCourse();
                              $hasCategory = count($hasCourse) ? $hasCourse->hasCategory() : [];
                              $hasLevel = count($hasCourse) ? $hasCourse->hasLevel() : [];
                            @endphp
                            @php
                                $totallacture=0;
                                if(count($val->hasCourse())){
                                    foreach ($val->hasCourse()->section as $key => $value) {
                                        $totallacture = $totallacture+count($value['video']);   
                                    }
                                }
                            @endphp
                        <div class="progress-icon-data" st>
                            <div class="progress-data" style="height: 6px">
                                <div id="myBar" class="prosess-progress" style="width:{{$percent}}%" ></div>
                            </div>
                            <p style="text-align: center;"><span class="complete-progress" id="demo">{{$percent}}%</span>&nbsp;Completed</p>
                        </div>
                    
                    </div>
                </div>
            @endforeach
        @else
        <div class="all-leval">
            <div class="row">
                <div class="col-sm-12">
                    <div class="center-courses">
                        <div class="row">
                             <div class="col-xs-12 no-padding">
                                <div class="center-courses-text">
                                    <center>
                                    <span>Currently, there are no courses available.</span>
                                    </center>
                                </div>
                              </div>      
                        </div>
                    </div>        
                </div>
            </div>
        </div>        
          
        @endif  
    </div>

    <div style="clear: both"></div>            
    <div id="listview" style="display: none">    
    @if(isset($data) && count($data))
      @foreach($data as $key=>$val)
      	<?php 
		$percent = 0;
		if( $val->v_complete_lectures ) $percent = ceil( $val->v_complete_lectures * 100  / $val->v_total_lectures );
		?>
        <div class="all-leval all-leval_custom">
            <div class="row height_container">
                <div class="col-sm-3">
                    <a href="{{url('buyer/courses/overview')}}/{{$val->id}}">
                    <div class="leval-img leval-img-sortng">
                    @php
                        $imgins="";
                        if(count($val->hasCourse()) && isset($val->hasCourse()->v_course_thumbnail) && $val->hasCourse()->v_course_thumbnail!=""){
                            $imgins = $val->hasCourse()->v_course_thumbnail;
                        }else{
                            $imgins = 'jobpost/photos/No_Image_Available.jpg';
                        }
                        $imgdata = \Storage::cloud()->url($imgins);
                    @endphp
                    <img class="height_div" src="{{$imgdata}}" />
                        
                    </div>
                    </a>
                </div>

                <div class="col-sm-9 height_div">
                    <div class="center-courses">
                        <div class="row">
                            <div class="col-xs-12 no-padding">
                                <div class="center-courses-text">
                                    <a href="{{url('buyer/courses/overview')}}/{{$val->id}}">
                                        <span>{{count( $val->hasCourse() ) ? $val->hasCourse()->v_title : ''}}</span>
                                        <p>Course by : {{count( $val->hasCourse() ) ? $val->hasCourse()->v_author_name : ''}}</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 no-padding">
                                <div class="center-courses-text">
                                    <p>{{count( $val->hasCourse() ) ? $val->hasCourse()->v_sub_title : ''}}</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="progress-icon">
                                    <div class="progress-data">
                                        <div id="myBar" class="prosess-progress" style="width:{{$percent}}%" ></div>
                                    </div>
                                    <p><span class="complete-progress" id="demo">{{$percent}}%</span>Completed</p>
                                </div>

                                <div class="rating-leval">
                                    <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{count( $val->hasCourse() ) ? $val->hasCourse()->review['avgtotal'] : ''}}" readonly>
                                    <p><span class="point">{{count( $val->hasCourse() ) ? number_format($val->hasCourse()->i_total_avg_review,1) : ''}} ( {{count( $val->hasCourse() ) ? $val->hasCourse()->i_total_review : ''}} )</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 no-padding">
                                <ul class="list-leval">
                                    @php
                                      $hasCourse = $val->hasCourse();
                                      $hasCategory = count($hasCourse) ? $hasCourse->hasCategory() : [];
                                      $hasLevel = count($hasCourse) ? $hasCourse->hasLevel() : [];
                                    @endphp
                                    @php
                                        $totallacture=0;
                                        if(count($val->hasCourse())){
                                            foreach ($val->hasCourse()->section as $key => $value) {
                                                $totallacture = $totallacture+count($value['video']);   
                                            }
                                        }
                                    @endphp
                                    <li><span>{{$totallacture}}</span> Lectures</li>
                                    |
                                    <li><span>{{count( $val->hasCourse() ) ? $val->hasCourse()->i_duration : ''}}</span> Hours Duration</li>
                                    |
                                    <li><span>{{count( $hasLevel) ? $hasLevel->v_title : ''}}</span> Level</li>
                                    |
                                    <li><span>{{count( $hasCategory) ? $hasCategory->v_title : ''}}</span></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      @endforeach
    @else

        <div class="all-leval">
            <div class="row">
                <div class="col-sm-12">
                    <div class="center-courses">
                        <div class="row">
                             <div class="col-xs-12 no-padding">
                                <div class="center-courses-text">
                                    <center>
                                    <span>Currently, there are no courses available.</span>
                                    </center>
                                </div>
                              </div>      
                        </div>
                    </div>        
                </div>
            </div>
        </div>        
   
    @endif   
    </div>


        <div class="col-xs-12">
            <div class="center">
                {{ $data->links() }}
            </div>
        </div>
    
    </div>
   


@stop
@section('js')
<script type="text/javascript">
  
    function masterfilter(){
        $("#searchcoursesmaster").submit();
    }


    // function masterfilter(){
    
    //     var mdata=$("#searchcoursesmaster").serialize();
    //     var sdata=$("#searchcoursesside").serialize();

    //     window.location.href = "{{url('courses-search')}}?"+mdata+"&"+sdata;
    //     //$("#searchcoursesmaster").submit();
    // }

$( document ).ready(function() {
    var s1 = $('.same-height-two').height();
    var s2 = $('.same-height').height();

    if (s1 > s2)
        $('.same-height-two').css('height', s1 + "px");
    else
        $('.same-height').css('height', s2 + "px");
});


 function viewchange(data) {
            
            var imgpath = "{{url('public/Assets/frontend/images')}}";

            if(data=="grid"){
                $("#listview").css("display","none");
                $("#gridview").css("display","inline");

                var gridicon = imgpath+'/fore.png';
                var listicon = imgpath+'/fore2.png';
                $("#gridicon").attr("src",gridicon);
                $("#listicon").attr("src",listicon);

            }else{  
                
                $("#listview").css("display","block");
                $("#gridview").css("display","none");

                var gridicon = imgpath+'/fore33.png';
                var listicon = imgpath+'/fore22.png';
                $("#gridicon").attr("src",gridicon);
                $("#listicon").attr("src",listicon);
            }

        }


</script>
@stop

