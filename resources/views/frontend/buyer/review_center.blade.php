@extends('layouts.frontend')

@section('content')
    <!-- 26B-control-panel-your-buyer-reviews-1 -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/dashboard')}}" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </a>
                </div>
                <div class="title-support">
                    <h1>Your Job Posting Reviews</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main-buyeraccount">
            <div class="buyer-reviews">
                
                 <div class="buyer-first">
                      <div class="buyer-profile">
                          <div class="buyer-profile">
                              <h1>Your Job Posting <br> Reviews</h1>
                              <div class="select-filter1">
                                  <div class="select-filter">
                                      <span>Filter by:</span>&nbsp;
                                      <select style="width: auto !important" class="resizing_select1 arr_img" name="v_order_by" id="v_order_by_review" onchange="dashboardReviewOrder()">
                                          <option value="DESC" selected>Latest First</option>
                                          <option value="ASC">Oldest First</option>
                                      </select>
                                      {{-- <select id="width_tmp_select1">
                                          <option id="width_tmp_option1"></option>
                                      </select>
                                      <div class="Filter-img">
                                          <img src="{{url('public/Assets/frontend/images/select.png')}}" alt="" />
                                      </div> --}}
                                  </div>
                              </div>
                          </div>
                      </div>


                      @php
                          $totalWork=0;
                          $totalSupport=0;
                          $totalGrowth=0;
                          $totalWorkAgain=0;

                          $totalWorkavg=0;
                          $totalSupportavg=0;
                          $totalGrowthavg=0;
                          $totalWorkAgainavg=0;
                          $totalavg=0;
                          if(count($buyerreview)){

                              foreach($buyerreview as $k=>$v){
                                  $totalWork = $totalWork+$v->i_work_star;
                                  $totalSupport = $totalSupport+$v->i_support_star;
                                  $totalGrowth = $totalGrowth+$v->i_opportunities_star;
                                  $totalWorkAgain = $totalWorkAgain+$v->i_work_again_star;
                              }
                              $totalWorkavg = $totalWork/count($buyerreview);
                              $totalSupportavg = $totalSupport/count($buyerreview);
                              $totalGrowthavg = $totalGrowth/count($buyerreview);
                              $totalWorkAgainavg = $totalWorkAgain/count($buyerreview);

                              $totalavg = $totalWorkavg+$totalSupportavg+$totalGrowthavg+$totalWorkAgainavg;
                              $totalavg = $totalavg/4;

                          }
                      @endphp



                       <div class="row">
                         <div class="col-sm-3 text-center">
                             Person You Work For?
                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalWorkavg}}" readonly >
                         </div>
                         <div class="col-sm-3 text-center">
                            Support You Get
                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalSupportavg}}" readonly >
                         </div>
                        <div class="col-sm-3 text-center">
                            Growth opportunities
                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalGrowthavg}}" readonly >
                         </div>

                         <div class="col-sm-3 text-center">
                            Would You Work Again
                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalWorkAgainavg}}" readonly >
                         </div>
                      </div>



                  </div>


                <div id="jobreviewdata">
                </div>    
                <input type="hidden" name="reviewpage" id="reviewpage" value="1" autocomplete="off" />
                <div id="showmorebtn">
                <div class="load-profile">
                    <button class="btn btn-load" onclick="jobReview();">
                        Load More Reviews (<span>{{count($buyerreview)}}</span>)
                    </button>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-your-buyer-reviews-1 -->
@stop

@section('js')
    <script type="text/javascript">
      function dashboardReviewOrder(){
            
            var v_order_by = $("#v_order_by_review").val();
            
            if(v_order_by=="ASC"){
                var $divs = $("div.review-first");
                var numericallyOrderedDivs = $divs.sort(function (a, b) {
                    return $(a).find("h2").text() > $(b).find("h2").text();
                });
                $("#mainorderreview").html(numericallyOrderedDivs);
            }else{
                var $divs = $("div.review-first");
                var numericallyOrderedDivs = $divs.sort(function (a, b) {
                    return $(a).find("h2").text() < $(b).find("h2").text();
                });
                $("#mainorderreview").html(numericallyOrderedDivs);
            }
        }
        
    </script>
    <script type="text/javascript">
        function jobReview(){

          var page = $("#reviewpage").val();
          var actionurl = "{{url('buyer/user-review')}}";
          var formdata = "&page="+page; 
          page=parseInt(page)+1;
          $("#reviewpage").val(page)
          document.getElementById('load').style.visibility="visible"; 
          
          $.ajax({
              type    : "GET",
              url     : actionurl,
              data    : formdata,
              success : function( res ){
                var obj = jQuery.parseJSON(res);
                document.getElementById('load').style.visibility='hidden';
                if(obj['status']==1){
                    
                    $("#jobreviewdata").append(obj['responsestr']);
                    if(obj['btnstatus']==0){
                        $("#showmorebtn").html("");
                    }
                }    
                else{
                    $("#commonmsg").html(obj['msg']);
                }
              },
              error: function ( jqXHR, exception ) {
              }
          });

    }
      jobReview();
    </script>     
@stop

