@extends('layouts.frontend')

@section('content')


<div id="passwordmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;">Security</h4>
      </div>
      <div class="modal-body">
        <p id="popupmessage"></p>
        <div class="text-box-containt" style="margin-top: -19px;margin-bottom: 10px;">
            <label>Please enter your valid login password to withdraw the amount.</label>
            <input type="password" id="upassword" class="form-control input-field" name="upassword" required>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" onclick="checkpassword()"  class="btn btn-success">Proceed</button>
      </div>
    </div>
  </div>
</div>

 <!-- 26B-control-panel-edit-billing-details-2-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1>My Money</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            @if ($success = Session::get('success'))
              <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 10px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
              </div>
            @endif
            @if ($warning = Session::get('warning'))
              <div class="alert alert-info alert-danger" style="margin-top: 10px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
              </div>
            @endif  

            <div class="col-xs-12">
                <div class="main-support">
                
               <center>    
                @if($kycdoc->KYCLevel=="REGULAR")    
                    <h3 style="margin-bottom: 20px"><strong>KYC Status </strong> : <span style="color: #228B22;">Approved</span></h3>
                @else
                    <h3 style="margin-bottom: 20px"><strong>KYC Status </strong> : <span style="color: #e8128c;">Pending</span></h3>   
                @endif
                </center>
                    
                <form class="horizontal-form" role="form" method="POST" name="editbillingform" id="editbillingform" action="{{url('buyer/my-wallet/withdraw')}}" data-parsley-validate enctype="multipart/form-data" >

                    {{ csrf_field() }}    
                    <div class="billing-detail">
                        <center>
                       
                        <div id="bank_detail_div" >
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="field-support">
                                        Amount
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="bank_detail_div" >
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="field-support">
                                        £{{isset($wallet) ? number_format($wallet,2) : '0'}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        </center>
                    
                    </div>

                    @if($wallet>1)
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="field-support">
                                <div class="payment-method">
                                    Enter amount to withdraw
                                </div>
                                <input type="number" min="1" step="any" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="vAmt" class="form-control input-field" required="" data-parsley-trigger="keyup focusin focusout">
                            </div>
                        </div>
                    </div>
                    <div class="top-btnspace">
                        <button type="button" onclick="passwordmodal()" class="btn form-next">Withdraw</button>
                    </div>
                    @endif

                 </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-edit-billing-details-2 -->

@stop

@section('js')
    <script type="text/javascript">
        
        function passwordmodal(){
            var formValidFalg = $("#editbillingform").parsley().validate('');
            if(formValidFalg){
                $("#passwordmodal").modal("show");    
            }
        }

        function checkpassword(){
            
            var actionurl = "{{url('checkpassword')}}";
            var password=$("#upassword").val();
            var formdata = "password="+password; 
            document.getElementById('load').style.visibility="visible"; 

            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    document.getElementById('load').style.visibility='hidden';
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#popupmessage").html("");
                        $("#editbillingform").submit();
                    }else{
                        $("#popupmessage").html(obj['msg']);
                        $("#upassword").val("");
                    }
                },
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                    $("#popupmessage").html('<div class="alert alert-info alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span>somthing went wrong.Please try again after somtime.</span></div>');
                }
            });



        }
    </script>
@stop

