

    <div class="mainorder " id="mainorder">
        @if(isset($data) && count($data))
            
            @foreach($data as $k=>$v)
               
                @php

                $v_image="";
                $v_fname="";
                $v_lname="";
                
                if($v->i_from_id!=$userid){
                    if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_image)){
                        $v_image=$v->hasFromUser()->v_image;
                    }
                    if($v_image!=""){
                        $v_image = \Storage::cloud()->url($v_image);
                    }else{
                        $v_image = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                    }
                    if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_fname)){
                        $v_fname=$v->hasFromUser()->v_fname;
                    }
                    if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_lname)){
                        $v_lname=$v->hasFromUser()->v_lname;
                    }

                }else{

                    if(count($v->hasToUser()) && isset($v->hasToUser()->v_image)){
                        $v_image=$v->hasToUser()->v_image;
                    }
                    if($v_image!=""){
                        $v_image = \Storage::cloud()->url($v_image);
                    }else{
                        $v_image = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                    }
                    if(count($v->hasToUser()) && isset($v->hasToUser()->v_fname)){
                        $v_fname=$v->hasToUser()->v_fname;
                    }
                    if(count($v->hasToUser()) && isset($v->hasToUser()->v_lname)){
                        $v_lname=$v->hasToUser()->v_lname;
                    }

                }
                @endphp
                <div class="media buyer-first media_msg p-3 dashed_border chat-first @if($k<1) activemessage @endif " id="{{$v->id}}">
                    <a href="javascript:;" onclick="detailMessage('{{$v->id}}')">
                        <h2 style="display: none;">{{strtotime($v->d_added)}}</h2>
                        <div class="media-left">
                          <img src="{{$v_image}}" alt="image">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{$v_fname}} {{$v_lname}}</h4>
                             @php 
                                $humanTime= App\Helpers\General::time_elapsed_string(strtotime($v->d_added));
                              @endphp
                            <p><span>{{date("h:i a",strtotime($v->d_added))}}</span><span> ({{$humanTime}})</span></p>
                           <div class="buyer-tital-text">
                                <p><b>{{$v->v_subject_title}}</b></p>
                                <p>

                                @if(isset($v->l_message))
                                    @if(strlen($v->l_message)>100)
                                        {{substr($v->l_message,0,100)}}...
                                    @else
                                        {{$v->l_message}}
                                    @endif
                                @endif
                               </p>
                            </div>
                        </div>
                    </a>
                </div>
                
            @endforeach
        @else
        
        <div class="buyer-first chat-first">
            <div class="final-point">
                <div class="buyer-tital">
                    <h5>No messages found.</h5>
                </div>
            </div>
        </div>    

        @endif
        <div class="clearfix"></div>
    </div>
<div class="clearfix"></div>

