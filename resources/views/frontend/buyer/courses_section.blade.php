@extends('layouts.frontend')

@section('content')
<link rel="stylesheet" href="{{url('public/Assets/frontend/css/videre.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{url('public/Assets/frontend/css/app.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{url('public/Assets/frontend/css/ionicons.min.css')}}">
<style type="text/css">
    .browsebutton {
        height: 0;
        width: 0;
        display: none;
    }
    .btn-add-remove {
    background: #a00960;
    border-radius: 20px;
    font-size: 16px;
    color: #fff;
    margin-top: 44px;
    border: 2px solid #a00960;
}

.videopoupmodel video {
    margin: 0 auto;
    height: 700px !important;
}
 .video-play{position: relative; z-index: 0;}
.modal-backdrop.in {display: none;}
.btn-add-remove:hover {background:transparent;color: #a00960; border:2px solid #a00960;}
</style>
@php
    //dump($_REQUEST);    
@endphp

<div id="videoplaymodal" class="modal3 videopoupmodel">
    <div class="modal-content-all">
        <div class="container-fluid video_title_container">
            <div class="margin-t-b">
                <div class="row">  
                    <div class="col-sm-6">
                        <div class="vid-tital">
                            <h3>{{isset($data->v_title) ? $data->v_title : ''}}</h3>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-popup-all">
                            <span id="nextvideohtml">
                            <button type="button" onclick="playNext()" class="btn btn-vid">Next Video</button>
                            </span>
                            <a href="{{url('buyer/courses/qa')}}/{{$buyercoursedata->id}}">
                            <button class="btn btn-vid">BROWSE Q&A</button>
                            </a>
                            <a href="{{url('buyer/courses')}}">
                            <button class="btn btn-vid">GO TO DASHBOARD</button>
                            </a>
                            <button class="btn btn-vid" type="button" onclick="closevideopopup()">CLOSE</button>
                        </div>
                    </div>
                </div>
                
            </div>  
        </div>
        <div class="video-play">
            <div id="player"></div>
        </div>
    </div>
</div>



<!--26b-control-panel-my-courses-overviwe-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                
                <div class="left-supportbtn">
                    <a href="{{url('buyer/courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image">Back to My Courses</button>
                    </a>
                </div>
                <div class="title-support">
                    <h1>My Courses Details</h1>
                </div>
            </div>
            
        </div>
    </div>
    <div class="tab-detail">
        <div class="container">
            <ul class="nav nav-pills nav-pills-new">
                <li><a href="{{url('buyer/courses/overview')}}/{{$buyercoursedata->id}}">Overview </a></li>
                <li class="active"><a href="{{url('buyer/courses/section')}}/{{$buyercoursedata->id}}">Course Content</a></li>
                
                @if(isset($data->e_qa_enabled) && $data->e_qa_enabled=="on")
                <li><a href="{{url('buyer/courses/qa')}}/{{$buyercoursedata->id}}">Q&A</a></li>
                @endif
                
                @if(isset($data->e_reviews_enabled) && $data->e_reviews_enabled=="on")
                <li><a href="{{url('buyer/courses/reviews')}}/{{$buyercoursedata->id}}"> Course Reviews</a></li>
                @endif
                
                @if(isset($data->e_announcements_enabled) && $data->e_announcements_enabled=="on")
                <li><a href="{{url('buyer/courses/announcements')}}/{{$buyercoursedata->id}}">Announcements</a></li>
                @endif

             </ul>
        </div>
    </div>
    <!-- Tab panes -->
    
    
    <div class="container">

        <div class="required-line">
            <div class="chapter-title">
                <div class="row">
                    <div id="commonmsg"></div>
                    
                    <div class="col-sm-6 ">
                        <h5> Course Content </h5>
                    </div>
                </div>
            </div>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">    
            @php
                $lact=0;    
            @endphp

            <?php 
                $totalv=0;
                if(isset($data->section) && count($data->section)){
                    foreach($data->section as $k=>$v){
                        if(isset($v['video']) && count($v['video'])){
                            foreach($v['video'] as $key=>$val){
                                $totalv = $totalv+1;
                            }
                        }
                    }
                }
                $totalvl=0;
            ?>

            @if(isset($data->section) && count($data->section))
                @foreach($data->section as $k=>$v)
                    
                    <div class="panel panel-default">
                        <div class="panel-heading root-position" role="tab" id="heading{{$k}}">
                            <h4 class="panel-tab-content">
                                <a role="button" class="aass_main{{$lact}}" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$k}}" aria-expanded="true" aria-controls="collapseOne">
                                  <i class="more-less glyphicon glyphicon-plus"></i>
                                    {{$k+1}}. {{$v['v_title']}}
                                </a>
                            </h4>
                        </div>
                       
                        <div id="collapse{{$k}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$k}}">
                            <div class="panel-body panel-black">
                                
                                 @if(isset($v['video']) && count($v['video']))
                                    @foreach($v['video'] as $key=>$val)

                                        <div class="coursevideo-list root-position">
                                            <div class="video-list-course">
                                                 @php
                                                    $videoUrl="";
                                                    if(Storage::disk('s3')->exists($val['v_video'])){
                                                        $videoUrl = \Storage::cloud()->url($val['v_video']);      
                                                    }
                                                    $lact=$lact+1;
                                                    $totalvl=$totalvl+1;
                                                    $nextv=1;
                                                    if($totalvl==$totalv){
                                                        $nextv=0;    
                                                    }

                                                 @endphp
                                                 <a href="javascript:;" 
                                                    class="cls-video coursepadding sec{{$lact}}" 
                                                    onclick="playVideo(this, '{{$videoUrl}}');" 
                                                    data-video-url="{{$videoUrl}}" 
                                                    data-play="0" 
                                                    data-sid="{{$val['_id']}}" 
                                                    data-next="{{$nextv}}" 
                                                    data-bcid="{{$buyercoursedata->_id}}"
                                                    >
                                                    <img src="{{url('public/Assets/frontend/images/symbol-coursevideolist.png')}}" class="coursevideo-player" alt="video-player">{{$val['v_title']}}
                                                    <span> {{$val['v_duration_min']}}:{{$val['v_duration_sec']}} </span>
                                                </a>
                                            </div>
                                            
                                            @if(isset($val['v_doc']) && $val['v_doc']!="")
                                            <?php 
                                                $doctype = explode(".", $val['v_doc']);
                                                if(isset($doctype[1])){
                                                    $doctype = $doctype[1];     
                                                }else{
                                                    $doctype = ""; 
                                                }

                                            ?>
                                            <?php 
                                                $docUrl="";    
                                                if(Storage::disk('s3')->exists($val['v_doc'])){
                                                    $docUrl = \Storage::cloud()->url($val['v_doc']);      
                                                }
                                            ?>
                                            <div class="fileset-preview-courses">
                                                <span> <a href="{{$docUrl}}" download target="_blank" >{{strtoupper($doctype)}}</a> </span>
                                            </div>
                                            @endif

                                        </div>
                                    @endforeach
                                @endif
                                
                            </div>
                        </div>
                    </div>

                @endforeach
            @endif    
            </div>
        </div>
    </div>
<!--26b-control-panel-my-courses-overviwe-->

@php
    $contnuelact="";
    if(isset($_REQUEST['lact']) && $_REQUEST['lact']!=""){
        $contnuelact=$_REQUEST['lact'];
    }
@endphp   


@stop
@section('js')
  
  <script src="{{url('public/Assets/frontend/js/videre.js')}}"></script>
  <script src="{{url('public/Assets/frontend/js/app.js')}}"></script>
<script type="text/javascript">

@if(isset($_REQUEST['lact']) && $_REQUEST['lact']!="")
    var lactcinbt = "{{$contnuelact}}";
    $('.aass_main'+lactcinbt).click()
    $(".sec"+lactcinbt).click()
@endif

function markAsComplete( sid, bcid, fileUrl ){

    var actionurl = "{{url('buyer/courses/view/section')}}";
	var formData = "sid="+sid+"&bcid="+bcid+"&file-url="+fileUrl;

    $.ajax({
        processData : false,
        contentType : false,
        type : "GET",
        url : actionurl,
        data : formData,
        success : function( res ){
            var obj = jQuery.parseJSON( res );
            if( !obj['status'] ){
                $("#commonpopupmsg").html(obj['msg']);
            }
        },
		error: function ( jqXHR, exception ){
		}
    });
}

function closevideopopup(){
    jQuery('#player').html('');
    jQuery('#videoplaymodal').modal('hide');
}


function playVideo( curr, videoUrl ){
	
    jQuery('#player').html('');
	jQuery('#player').attr('class','');
	jQuery('#player').attr('style','');
	
	jQuery('.cls-video').attr('data-play', 0);
	jQuery(curr).attr('data-play', 1);
    jQuery('.top-video-title').html( jQuery(curr).find('.video-title').html() );

    var nextv = jQuery(curr).attr('data-next');
    if(nextv=="0"){
        jQuery("#nextvideohtml").css("display","none");        
    }

    jQuery("#videoplaymodal").modal("show");
	jQuery('#player').videre({
		video: {
			quality: [{
				label: '720p',
				src: videoUrl,
				width:'100%'
			}, {
				label: '360p',
				src: videoUrl+'?SD'
			}, {
				label: '240p',
				src: videoUrl+'?SD'
			}],
			title: 'jQueryScript.Net'
		},
        dimensions: 1280
	});
	
    markAsComplete(
		jQuery(curr).attr('data-sid'),
		jQuery(curr).attr('data-bcid'),
		videoUrl
	);
}

function playNext(){
   
    var curr = '';
	var nextUrl = '';
	var getNext = 0;
	var cnt = 0;
	jQuery('.cls-video').each(function(){
		if( !nextUrl ){
			if( getNext ){
				nextUrl = jQuery(this).attr('data-video-url');
				jQuery(this).attr('data-play',1);
				curr = jQuery(this);
			}
			if( jQuery(this).attr('data-play') == 1 || jQuery(this).attr('data-play') == '1' ){
				getNext = 1;
				jQuery(this).attr('data-play',0);
			}
		}
	});
	if( !nextUrl ){
		curr = jQuery('.cls-video').first();
		nextUrl = curr.attr('data-video-url');
		curr.attr('data-play',1);
	}
	if( nextUrl ){
		playVideo( curr, nextUrl );
	}
}

</script>

@stop

