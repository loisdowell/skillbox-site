@extends('layouts.frontend')

@section('content')
    
    <!-- 26B-control-panel-buyer-ORDERS -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> Orders </h1>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <div class="tab-detail tab_detail_custom">
        <div class="container">
            <ul class="nav nav-pills nav-pills-buyer-order">
                <li @if($status=='active') class="active" @endif>
                    <a href="{{url('buyer/orders/active')}}"  class="tab-name">Active ({{$total['active']}})</a>
                </li>
                <li @if($status=='awaiting') class="active" @endif>
                    <a href="{{url('buyer/orders/awaiting')}}"  class="tab-name">Awaiting my review ({{$total['awaiting']}}) </a>
                </li>
                <li @if($status=='delivered') class="active" @endif>
                    <a href="{{url('buyer/orders/delivered')}}"  class="tab-name">Delivered ({{$total['delivered']}})</a>
                </li>
                <li @if($status=='completed') class="active" @endif>
                    <a href="{{url('buyer/orders/completed')}}"  class="tab-name">Completed ({{$total['completed']}})</a>
                </li>
                <li @if($status=='cancelled') class="active" @endif>
                    <a href="{{url('buyer/orders/cancelled')}}"  class="tab-name">Cancelled ({{$total['cancelled']}})</a>
                </li>
                <li @if($status=='all') class="active" @endif>
                    <a href="{{url('buyer/orders/all')}}" class="tab-name">All</a>
                </li>
            </ul>
        </div>
    </div>


    <!-- Tab panes -->
    <div class="container">

        @if ($success = Session::get('success'))
          <div style="clear: both;"></div>
          <br>
          <div class="alert alert-success alert-big alert-dismissable br-5">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;success</strong>&nbsp;&nbsp;{{ $success }}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div style="clear: both;"></div>
          <br>
          <div class="alert alert-info alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;{{ $warning }}
          </div>
        @endif    

        <div id="content">
            <div id="my-tab-content" class="tab-content">
                
                <!-- 26B-control-panel-buyer-ORDERS -->
                <div class="tab-pane active" id="completed">
                    <div class="order-status">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="title-orderstatus">
                                    Order Stats
                                </div>
                            </div>
                        </div>
                        <div class="order-details">
                            <div class="col-sm-3 no-padding">
                                <div class="right-boderstyle">
                                    ORDERS PLACED
                                    <p> {{$total['order']}} </p>
                                </div>
                            </div>
                            <div class="col-sm-3 no-padding">
                                <div class="right-boderstyle">
                                    ORDER AMOUNT
                                    <p> £{{$total['orderamt']}}</p>
                                </div>
                            </div>
                            <div class="col-sm-3 no-padding">
                                <div class="right-boderstyle">
                                    CANCELLED ORDERS
                                    <p> {{$total['cancelled']}} </p>
                                </div>
                            </div>
                            <div class="col-sm-3 no-padding">
                                <div class="right-boderstyle last-chlidhead">
                                    ACTIVE ORDERS 
                                    <p> {{$total['active']}} </p>
                                </div>
                            </div>
                        </div>
                        <div class="contant-table">
                            <div class="table-responsive">
                            <table class="table table-hover table-orderdetails">
                                <thead>
                                    <tr>
                                        <th>TITLE</th>
                                        <th>ORDER DATE</th>
                                        <th>DUE ON </th>
                                        <th>TOTAL</th>
                                        <th>STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                @if(isset($data) && count($data))
                                    
                                    @foreach($data as $k=>$v)
                                        <tr>
                                            <td>
                                                <a href="{{url('buyer/orders/detail')}}/{{$v->id}}"> 
                                                {{isset($v->v_profile_title) ? $v->v_profile_title : ''}}
                                                </a>
                                            </td>
                                            <td>{{isset($v->d_date) ? date("d M Y",strtotime($v->d_date)) : ''}}</td>
                                            
                                            @php
                                                $startdate = $v->d_date;   
                                                $duedate="";
                                                if(isset($v->v_order_start) && isset($v->d_order_start_date) && $v->v_order_start=="yes"){
                                                    $startdate = $v->d_order_start_date;
                                                }
                                                if(isset($v->v_duration_in) && $v->v_duration_in=="hours"){
                                                    $duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' hours',strtotime($startdate)));
                                                }else if(isset($v->v_duration_in) && $v->v_duration_in=="day"){
                                                    $duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' days',strtotime($startdate)));
                                                }else if(isset($v->v_duration_in) && $v->v_duration_in=="month"){
                                                    $duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' months',strtotime($startdate)));
                                                }else if(isset($v->v_duration_in) && $v->v_duration_in=="year"){
                                                    $duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' years',strtotime($startdate)));
                                                }
                                            @endphp
                                            
                                            <td>{{date("d M Y",strtotime($duedate))}}</td>
                                            <td>£{{isset($v->v_amount) ? $v->v_amount : ''}}</td>
                                            <td>{{isset($v->v_order_status) ? ucfirst($v->v_order_status) : ''}}</td>
                                        </tr>

                                    @endforeach
                                
                                @else
                                    <tr>
                                        <td colspan="5">No orders found.</td>
                                    </tr>
                                @endif
                              
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                    

                    

                </div>
                <!-- End 26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS-resolution-center-W1 -->

            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS -->

    


@stop
@section('js')
<script type="text/javascript">
  
</script>
@stop

