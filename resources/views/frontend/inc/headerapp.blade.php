@php
	$path = Route::getFacadeRoot()->current()->uri();
@endphp
    <header id="header">
        <div class="container">
            <div class="row">
                <nav class="navbar nav-main @if(!auth()->guard('web')->check()) without_login @endif">
                    <div class="container-fluid">
                        <div class="navbar-header navbar-header-final">
                            <button type="button" class="navbar-toggle icon-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand logo" href="javascript:;">
                                @php
                                      $logosrc= App\Helpers\General::SiteLogo();
                                @endphp  
                                <img src="{{$logosrc}}" alt="images">
                            </a>
                        </div>
                        <div class="collapse navbar-collapse sub-manu" id="myNavbar">
                            <div class="navbarmenu">
                            <ul class="nav navbar-nav navbar-right manu-nav">
                                {{-- <a class="btn-post-job_a" href="{{url('buyer/post-job')}}"> --}}
                                    <!-- <a class="btn-post-job_a" href="{{url('account/seller')}}">
                                    <li>
                                        <button class="btn btn-post-job">
                                            Freelancer?{{-- Post a Job --}}
                                        </button>
                                    </li>
                                </a>
                                
                                <li class="cart_btn"><a href="{{url('cart/data')}}"><i class="zmdi zmdi-shopping-cart"></i><span>Cart</span>
                                @if(sizeof(Cart::content()) > 0)    
                                <b class="badge">{{count(Cart::content())}}</b>
                                @endif
                                </a></li>  -->
                            </ul>

                            <form class="horizontal-form" role="form" method="get" name="searchmaster" id="searchmaster" action="{{url('search')}}" >

                            <ul class="nav navbar-nav manu-nav nav-find searchdataclass ">
                                

                            </ul>

                        </form>
                        </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>

   