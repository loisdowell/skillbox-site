@php
    $footerdata = App\Helpers\General::footerMenu();
@endphp

<!--footer-->
<div class="footer-custam">
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-heading1">
                        <div class="footer-heading">
                            
                            <span>
                                <a href="{{url('account/buyer')}}" style="color:#e8128c">Join Skillbox</a>
                            </span>
                        </div>
                        <div class="row">
                            <?php /*
                            <div class="col-sm-8">
                                <div class="col-xs-4">
                                    <div class="row">
                                        <div class="Skillbox">
                                            <ul>
                                                <li><a href="{{url('cms/about-us')}}"> About Us</a></li>
                                                <li><a href="{{url('blog')}}"> Blog & Education </a> </li>
                                                <li><a href="{{url('cms/feedback')}}"> Feedback</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="row">
                                        <div class="Skillbox">
                                            <ul>
                                                <li><a href="{{url('cms/help-support')}}"> Help & Support</a></li>
                                                <li><a href="{{url('join-our-team')}}"> Skillbox Careers</a></li>
                                                <li><a href="{{url('cms/trust-safety')}}"> Trust & Safety</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="row">
                                        <div class="Skillbox">
                                            <ul>
                                                <li><a href="{{url('skillbox-guide')}}"> How to Skillbox guide</a></li>
                                                <li><a href="{{url('cms/terms-of-service')}}"> Terms of Service</a></li>
                                                <li><a href="{{url('cms/privacy-policy')}}">Privacy & Security</a></li>
                                                <!-- <li><a target="_blank" href="https://www.mangopay.com/terms/PSP/PSP_MANGOPAY_EN.pdf">MangoPay T&C</a></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            */ ?>

                            <div class="col-sm-8">
                                <div class="col-xs-4">
                                    <div class="row">
                                        <div class="Skillbox">
                                            <ul>
                                                <li><a href="{{url('blog')}}"> Blog & Education </a> </li>
                                                @if(isset($footerdata) && count($footerdata))
                                                    @foreach($footerdata as $key=>$val)
                                                        @if($val->v_column=="1")    
                                                        @if(count($val->hasPage()) && isset($val->hasPage()->v_key))
                                                        
                                                        @if($val->hasPage()->v_key=="help-support")
                                                            <li><a href="{{url('support')}}"> {{$val->v_title}} </a> </li>
                                                        @else
                                                            <li><a href="{{url('pages/'.$val->hasPage()->v_key)}}"> {{$val->v_title}} </a> </li>
                                                        @endif    

                                                        @endif    
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="row">
                                        <div class="Skillbox">
                                            <ul>
                                                <li><a href="{{url('join-our-team')}}"> Skillbox Careers</a></li>
                                                 @if(isset($footerdata) && count($footerdata))
                                                    @foreach($footerdata as $key=>$val)
                                                        @if($val->v_column=="2")    
                                                        @if(count($val->hasPage()) && isset($val->hasPage()->v_key))
                                                        

                                                        @if($val->hasPage()->v_key=="help-support")
                                                            <li><a href="{{url('support')}}"> {{$val->v_title}} </a> </li>
                                                        @else
                                                            <li><a href="{{url('pages/'.$val->hasPage()->v_key)}}"> {{$val->v_title}} </a> </li>
                                                        @endif    


                                                        @endif    
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="row">
                                        <div class="Skillbox">
                                            <ul>
                                                <li><a href="{{url('skillbox-faqs')}}">Skillbox FAQ's</a></li>
                                               @if(isset($footerdata) && count($footerdata))
                                                    @foreach($footerdata as $key=>$val)
                                                        @if($val->v_column=="3")    
                                                        @if(count($val->hasPage()) && isset($val->hasPage()->v_key))

                                                        @if($val->hasPage()->v_key=="help-support")
                                                            <li><a href="{{url('support')}}"> {{$val->v_title}} </a> </li>
                                                        @else
                                                            <li><a href="{{url('pages/'.$val->hasPage()->v_key)}}"> {{$val->v_title}} </a> </li>
                                                        @endif    


                                                        @endif    
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <!-- <li><a target="_blank" href="https://www.mangopay.com/terms/PSP/PSP_MANGOPAY_EN.pdf">MangoPay T&C</a></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <!-- <div class="download-app">
                                    <span>Download Skillbox Mobile App</span>
                                </div>
                                <div class="app-download">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="app-store">
                                                <img src="{{url('public/Assets/frontend/images/app-store.png')}}" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="app-store">
                                                <img src="{{url('public/Assets/frontend/images/google-play.png')}}" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="clearfix"></div>
                                <div class="mangopay_logo">
                                    <div class="Skillbox">
                                        <ul>
                                            @if(isset($footerdata) && count($footerdata))
                                                @foreach($footerdata as $key=>$val)
                                                    @if($val->v_column=="4")    
                                                    @if(count($val->hasPage()) && isset($val->hasPage()->v_key))
                                                        
                                                        @if($val->hasPage()->v_key=="help-support")
                                                            <li><a href="{{url('support')}}"> {{$val->v_title}} </a> </li>
                                                        @else
                                                            <li><a href="{{url('pages/'.$val->hasPage()->v_key)}}"> {{$val->v_title}} </a> </li>
                                                        @endif    

                                                    @endif    
                                                    @endif
                                                @endforeach
                                            @endif
                                            <li><a target="_blank" href="https://www.mangopay.com/terms/PSP/PSP_MANGOPAY_EN.pdf">MangoPay T&C</a></li>
                                        </ul>
                                    </div>
                                    <img src="{{url('public/Assets/frontend/images/powered-by-mangopay.png')}}" alt="mangopay">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-6 social-link1">
                            <div class="social-link">
                                <ul>
                                    <li>
                                        <a target="_blank" href="{{App\Helpers\General::getSiteSetting('FACEBOOK_SOCIAL_LINK')}}"> <i class="zmdi zmdi-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="{{App\Helpers\General::getSiteSetting('TEITTER_SOCIAL_LINK')}}"> <i class="zmdi zmdi-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="{{App\Helpers\General::getSiteSetting('LINKEDIN_SOCIAL_LINK')}}"> <i class="zmdi zmdi-linkedin"></i></a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="{{App\Helpers\General::getSiteSetting('YOUTUBE_SOCIAL_LINK')}}"> <i class="zmdi zmdi-youtube"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-6 social-link1">
                            <div class="copy-right">
                                <a>{{App\Helpers\General::getSiteSetting('SITE_FOOTER_TEXT')}}</a>
                            </div>

                            <div class="copy-right">
                                <a style="margin-top: 7px;" href="https://www.altagency.co.uk/"> Website design & development by ALT Agency</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<script>
       
jQuery(document).ready(function(){
            jQuery( "#header" ).addClass('sticky');
  jQuery(window).scroll(function() {
    var height = jQuery(window).scrollTop();
        if(height  > 80 ) {
            jQuery('#header').css('box-shadow', '0px 4px 5px #cbcbcb');
          }
          else{
            jQuery('#header').css('box-shadow', 'none');
          }
        });
});

</script>
</div>    


<div id="catapult-cookie-bar" class="cookie_bar" >
    <div class="ctcc-inner container">
        <span class="ctcc-left-side">This website uses cookies to ensure you get the best experience on our website: 
            <a class="ctcc-more-info-link" tabindex="0" target="_blank" href="{{url('pages/privacy-policy')}}">Learn more.</a>
        </span>
        <span class="ctcc-right-side">
            <button id="" onclick="setCookiedata()" tabindex="0" onclick="">Okay, thanks</button>
        </span>
    </div>
</div>

<script>
    
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    
    function setCookiedata(){
        setCookie("alertbar", "Set", 1);
        //$(".cookie_bar").css("display","none");
        $(".cookie_bar").remove();
        $('footer').css('bottom', "0");
    }
    var username = getCookie("alertbar");
    
    if (username!= "") {
       //$(".cookie_bar").css("display","none");
       $(".cookie_bar").remove();
       $('footer').css('bottom', "0");
    }else{
        $(".cookie_bar").css("display","block");
    }

</script>
<!--end-footer-->