@extends('layouts.frontend')

@section('content')

<!--02B-reset-password -->
    <div class="container">
        <div class="title-welcome">
            <h1>Reset Password</h1>
        </div>
        <div class="row">
            <div class="col-md-12">

                @if ($success = Session::get('success'))
                  <div class="alert alert-success alert-big alert-dismissable br-5">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;success</strong>&nbsp;&nbsp;{{ $success }}
                  </div>
                @endif
                @if ($warning = Session::get('warning'))
                  <div class="alert alert-info alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;{{ $warning }}
                  </div>
                @endif

                <div class="reset-main">
                    <div class="sign-in">
                        
                        <form class="horizontal-form" role="form" method="POST" name="signupform" id="signupform" action="{{url('forgot-password')}}" data-parsley-validate enctype="multipart/form-data" >
                        {{ csrf_field() }}

                            <div class="mail-pass">
                                <input type="email" name="v_email" class="form-control form-mail" placeholder="Email" required>
                            </div>  
                            <div class="clearfix"></div>
                            <div class="signin-bottom">
                                
                                <button type="submit" class="btn btn-block btn-signin">Reset Password</button>

                                
                            </div>


                        </form>

                    </div>
                    <div class="join-us">
                        Not have an account ?
                        <br>
                        <span class="join">
                            <a href="{{url('account/buyer')}}">
                                Join us
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-02B-reset-password -->


@stop
@section('js')
<script type="text/javascript">
</script>

@stop

