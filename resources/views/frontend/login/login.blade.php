@extends('layouts.frontend')

@section('content')

<!--01B-sign-in-->
    <div class="container">
        <div class="title-welcome">
            <h1>Log in</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                
                @if ($success = Session::get('success'))
                  <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 30px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
                  </div>
                @endif
                @if ($warning = Session::get('warning'))
                  <div class="alert alert-info alert-danger" style="margin-top: 30px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
                  </div>
                @endif

                <div class="sign-main">
                    
                    <div class="sign-in">
                        
                        <a href="{{url('facebook/login')}}">
                        {{-- <a href="javascript:;"> --}}
                        <button type="button" class="btn  btn-block btn-signin">
                            <span class="facebook-img">
                                <img src="{{url('public/Assets/frontend/images/facebook.png')}}" alt="facebook-button" class="img-responsive img-sign" />
                            </span> Sign in with Facebook
                        </button>
                        </a>
                        
                        <form class="horizontal-form" role="form" method="POST" name="signupform" id="signupform" action="{{url('login')}}" data-parsley-validate enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <h2 class="login-option"><span> or </span></h2>
                        <div class="mail-pass">
                            <input type="email" name="v_email" class="form-control form-mail" placeholder="Email Address" required>
                            <input type="password" name="password" class="form-control form-pass" placeholder="Password" required>
                        </div>

                        <div class="clearfix">
                            <a href="{{url('forgot-password')}}">
                                <div class="forget-password">
                                    Forgot Password
                                </div>
                            </a>
                        </div>
                        <div class="signin-bottom">
                            <button type="submit" class="btn btn-block btn-signin">Sign in</button>
                            {{--
                            <a href="02B-sign-up-3-1.html" class="btn btn-block btn-signin">Sign in</a>
                            --}}
                        </div>
                        </form>
                    </div>
                    <div class="join-us">
                        Not have an account ?
                        <br>
                        <span class="join">
                        <a href="{{url('account/buyer')}}">
                        Join us
                        </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-01B-sign-in-->

@stop
@section('js')
<script type="text/javascript">
</script>

@stop

