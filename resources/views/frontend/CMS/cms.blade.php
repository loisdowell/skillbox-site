@extends('layouts.frontend')

@section('content')

    <link href="{{url('public/Assets/frontend')}}/vactor-map/dist/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{url('public/Assets/frontend')}}/vactor-map/dist/jquery.vmap.js"></script>
    <script type="text/javascript" src="{{url('public/Assets/frontend')}}/vactor-map/dist/maps/jquery.vmap.world.js" charset="utf-8"></script>
    <script type="text/javascript" src="{{url('public/Assets/frontend')}}/vactor-map/jquery.vmap.sampledata.js"></script>

    <?php
        $bannertext="";    
        if(isset($cmsdata->v_banner_text)){
            $bannertext = $cmsdata->v_banner_text;
        }
    ?>

    <!-- 31B-feedback-1 -->
    <div class="container-fluid">
        <div class="row">
            
            <div class="header-img">
                <img src="{{$metaDetails['v_image'] or ''}}" class="img-responsive" alt="" />
                <div class="center-containt">
                    <div class="container">
                        <div class="applybox-notification">
                            <h1 style="color: #fff;">{{$bannertext}}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
             @if ($success = Session::get('success'))
              <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 30px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
              </div>
            @endif
            @if ($warning = Session::get('warning'))
              <div class="alert alert-info alert-danger" style="margin-top: 30px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
              </div>
            @endif
            </div>

        </div>
    </div>

    <?php
            
            if(isset($cmsdata->l_description)){
                echo $cmsdata->l_description;
            }else{
                echo "";
            }
    ?>

@stop
@section('js')

<script type="text/javascript">

      function buyerSignup(){
          var formValidFalg = $("#buyersignupform").parsley().validate('');
          if(formValidFalg){
              var l = Ladda.create(document.getElementById('bsignup'));
              l.start();
              $("#buyersignupform").submit()
          } 
      }

</script>

<script>
        jQuery(document).ready(function() {
            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: '#ffffff',
                color: '#e8128c',

                selectedColor: '#e8128c',
                enableZoom: true,
                showTooltip: true,
                scaleColors: ['#e8128c', '#e8128c'],
                values: sample_data,
                normalizeFunction: 'polynomial'
            });
        });
    </script>

@stop

