@php
	$path = Route::getFacadeRoot()->current()->uri();
@endphp

  <header class="main-header">
                  
    <a href="{{url('admin')}}" class="logo">
      <span class="logo-mini"><b>SB</b></span>
      <!-- <span class="logo-lg"><img class="logo-default" alt="logo" src="{{url('public/Assets/admin/img')}}/bls-logo.png" style="max-width: 130px"></span> -->
      <span class="logo-lg" style="text-align:left;">
        @php
          $logosrc= App\Helpers\General::SiteLogo();
        @endphp  
        <img src="{{$logosrc}}" alt="images" style="margin-top: 5px">
      </span> 
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="height:50px;">
              @php
                $user_profile = auth()->guard('admin')->user()->v_profile;
              @endphp
              @if(isset($user_profile) && $user_profile != '')
                @php
                    $imgdata="";
                    if(Storage::disk('s3')->exists($user_profile)){
                        $imgdata = \Storage::cloud()->url($user_profile);      
                    }
                @endphp 
                <img src="{{$imgdata}}" class="user-image" alt="User Image">
              @else
                 @php
                    $imgdata = \Storage::cloud()->url('common/no-image.png');      
                 @endphp 
                <img src="{{$imgdata}}" class="user-image" alt="User Image">
              @endif
              <span class="hidden-xs">{{auth()->guard('admin')->user()->v_name}}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="{{$imgdata}}" class="img-circle" alt="User Image">
                <p>
                  {{auth()->guard('admin')->user()->v_name}}
                  <small>Member since {{date("M-Y",strtotime(auth()->guard('admin')->user()->d_added))}} </small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  @php $profile_url = url('admin/admin/edit/'.auth()->guard('admin')->user()->id); @endphp
                  <a href="{{$profile_url}}" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{url('admin/getLogout')}}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>


  <aside class="main-sidebar">
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
           @if(isset($user_profile) && $user_profile != '')
            @php
                $imgdata="";
                if(Storage::disk('s3')->exists($user_profile)){
                    $imgdata = \Storage::cloud()->url($user_profile);      
                }
            @endphp 
            <img src="{{$imgdata}}" class="img-circle" alt="User Image">
          @else
             @php
                $imgdata = \Storage::cloud()->url('common/no-image.png');      
             @endphp 
            <img src="{{$imgdata}}" class="img-circle" alt="User Image">
          @endif
        </div>
        <div class="pull-left info">
          <p>{{auth()->guard('admin')->user()->v_name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        
        <li class="@if( $path == 'admin/dashboard' ) active @endif treeview">
          <a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>


        
        <li class="@if( $path == 'admin/categories' || $path == 'admin/categories/add/{id}' || $path == 'admin/categories/edit/{id}' || $path == 'admin/skills' || $path == 'admin/skills/add/{id}' || $path == 'admin/skills/edit/{id}' || $path == 'admin/levels' || $path == 'admin/levels/add/{id}' || $path == 'admin/levels/edit/{id}' || $path == 'admin/country' || $path == 'admin/country/add/{id}' || $path == 'admin/country/edit/{id}' || $path == 'admin/state' || $path == 'admin/state/add/{id}' || $path == 'admin/state/edit/{id}' || $path == 'admin/city' || $path == 'admin/city/add/{id}' || $path == 'admin/state/city/{id}' || $path == 'admin/sitesetting' || $path == 'admin/search' || $path == 'admin/search/add/{id}' || $path == 'admin/search/edit/{id}' || $path == 'admin/sponsorfees' || $path == 'admin/sponsorfees/add/{id}' || $path == 'admin/sponsorfees/edit/{id}' || $path == 'admin/email-template' || $path == 'admin/email-template/add/{id}' || $path == 'admin/email-template/edit/{id}' || $path == 'admin/email-template-header' || $path == 'admin/email-template-header/add/{id}' || $path == 'admin/email-template-header/edit/{id}' || $path == 'admin/email-template-footer' || $path == 'admin/email-template-footer/add/{id}' || $path == 'admin/email-template-footer/edit/{id}' || $path == 'admin/company-ethos' || $path == 'admin/company-ethos/add/{id}' || $path == 'admin/company-ethos/edit/{id}' || $path == 'admin/price-filter' || $path == 'admin/price-filter/add/{id}' || $path == 'admin/price-filter/edit/{id}' || $path == 'admin/manage-meta-field' || $path == 'admin/manage-meta-field/add/{id}' || $path == 'admin/manage-meta-field/edit/{id}' || $path == 'admin/admin' || $path == 'admin/admin/add/{id}' || $path == 'admin/admin/edit/{id}' || $path == 'admin/buyer-how-its-work' || $path == 'admin/buyer-how-its-work/add/{id}' || $path == 'admin/buyer-how-its-work/edit/{id}' || $path == 'admin/seller-how-its-work' || $path == 'admin/seller-how-its-work/add/{id}' || $path == 'admin/seller-how-its-work/edit/{id}' || $path == 'admin/footer' || $path == 'admin/footer/add/{id}' || $path == 'admin/footer/edit/{id}' || $path == 'admin/guide' || $path == 'admin/guide/add/{id}' || $path == 'admin/guide/edit/{id}') active @endif treeview" >
            
            <a href="#">
            	<i class="fa fa-cogs "></i> <span>Settings</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          	</a>

            <ul class="treeview-menu">
	            <li @if( $path == 'admin/sitesetting' )class="active" @endif >
                <a href="{{url('admin/sitesetting')}}"><i class="fa fa-cog"></i> Site Settings</a>
              </li>
              <li @if( $path == 'admin/admin' || $path == 'admin/admin/add/{id}' || $path == 'admin/admin/edit/{id}' )class="active" @endif >
                 <a href="{{url('admin/admin')}}"><i class="fa fa-user"></i>Admins</a>
              </li>
              <li @if( $path == 'admin/categories' || $path == 'admin/categories/add/{id}' || $path == 'admin/categories/edit/{id}' )class="active" @endif >
                 <a href="{{url('admin/categories')}}"><i class="fa fa-user"></i>Categories</a>
              </li>
              <li @if( $path == 'admin/skills' || $path == 'admin/skills/add/{id}' || $path == 'admin/skills/edit/{id}' )class="active" @endif >
                 <a href="{{url('admin/skills')}}"><i class="fa fa-user"></i>skills</a>
              </li>

              <li @if( $path == 'admin/levels' || $path == 'admin/levels/add/{id}' || $path == 'admin/levels/edit/{id}' )class="active" @endif >
                 <a href="{{url('admin/levels')}}"><i class="fa fa-user"></i>Levels</a>
              </li>

              <li @if( $path == 'admin/country' || $path == 'admin/country/add/{id}' || $path == 'admin/country/edit/{id}' )class="active" @endif >
                 <a href="{{url('admin/country')}}"><i class="fa fa-globe"></i>Country</a>
              </li>

             

              {{-- <li @if( $path == 'admin/state' || $path == 'admin/state/add/{id}' || $path == 'admin/state/edit/{id}' )class="active" @endif >
                 <a href="{{url('admin/state')}}"><i class="fa fa-globe"></i>State</a>
              </li> --}}

              {{-- <li @if( $path == 'admin/city' || $path == 'admin/city/add/{id}' || $path == 'admin/city/edit/{id}' )class="active" @endif >
                 <a href="{{url('admin/city')}}"><i class="fa fa-globe"></i>City</a>
              </li> --}}

              <li @if( $path == 'admin/search' || $path == 'admin/search/add/{id}' || $path == 'admin/search/edit/{id}' )class="active" @endif >
                 <a href="{{url('admin/search')}}"><i class="fa fa-globe"></i>Search</a>
              </li>

              {{-- <li @if( $path == 'admin/sponsorfees' || $path == 'admin/sponsorfees/add/{id}' || $path == 'admin/sponsorfees/edit/{id}' )class="active" @endif >
                 <a href="{{url('admin/sponsorfees')}}"><i class="fa fa-globe"></i>Sponsor Fees</a>
              </li> --}}

               <li @if( $path == 'admin/guide' || $path == 'admin/guide/add/{id}' || $path == 'admin/guide/edit/{id}' )class="active" @endif >
                 <a href="{{url('admin/guide')}}"><i class="fa fa-globe"></i>How to Guide</a>
              </li>
              
              <li @if( $path == 'admin/company-ethos' || $path == 'admin/company-ethos/add/{id}' || $path == 'admin/company-ethos/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/company-ethos')}}"><i class="fa fa-file"></i>Company Ethos</a>
              </li>

              <li @if( $path == 'admin/price-filter' || $path == 'admin/price-filter/add/{id}' || $path == 'admin/price-filter/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/price-filter')}}"><i class="fa fa-file"></i>Price filter</a>
              </li>

              <li @if( $path == 'admin/footer' || $path == 'admin/footer/add/{id}' || $path == 'admin/footer/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/footer')}}"><i class="fa fa-sitemap"></i>Footer Menu</a>
              </li>

              <li @if( $path == 'admin/manage-meta-field' || $path == 'admin/manage-meta-field/add/{id}' || $path == 'admin/manage-meta-field/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/manage-meta-field')}}"><i class="fa fa-sitemap"></i>SEO Fields</a>
              </li>

              <li @if( $path == 'admin/buyer-how-its-work' || $path == 'admin/buyer-how-its-work/add/{id}' || $path == 'admin/buyer-how-its-work/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/buyer-how-its-work')}}"><i class="fa fa-sitemap"></i>How its Work(Buyer)</a>
              </li>

              <li @if( $path == 'admin/seller-how-its-work' || $path == 'admin/seller-how-its-work/add/{id}' || $path == 'admin/seller-how-its-work/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/seller-how-its-work')}}"><i class="fa fa-sitemap"></i>How its Work(Seller)</a>
              </li>

              <li @if( $path == 'admin/email-template' || $path == 'admin/email-template/add/{id}' || $path == 'admin/email-template/edit/{id}')class="active" @endif >
                <a href="{{url('admin/email-template')}}"><i class="fa fa-envelope-o"></i> Email Templates</a>
              </li>

              <li @if( $path == 'admin/email-template-header' || $path == 'admin/email-template-header/add/{id}' || $path == 'admin/email-template-header/edit/{id}')class="active" @endif >
                <a href="{{url('admin/email-template-header')}}"><i class="fa fa-envelope-o"></i> Email Template Header</a>
              </li>

              <li @if( $path == 'admin/email-template-footer' || $path == 'admin/email-template-footer/add/{id}' || $path == 'admin/email-template-footer/edit/{id}')class="active" @endif >
                <a href="{{url('admin/email-template-footer')}}"><i class="fa fa-envelope-o"></i> Email Template Footer</a>
              </li>

              <li @if( $path == 'admin/sitemap')class="active" @endif >
                <a href="{{url('admin/sitemap')}}"><i class="fa fa-sitemap"></i>Generate Sitemap</a>
              </li>


	          </ul>
        </li>

       
        <li class="@if( $path == 'admin/users' || $path == 'admin/users/add/{id}' || $path == 'admin/users/edit/{id}' || $path == 'admin/users/edit/password/{id}' || $path == 'admin/users/edit/billing/{id}' || $path == 'admin/users/edit/seller' || $path == 'admin/users/seller-reviews/{_id}' || $path == 'admin/users/buyer-reviews/{_id}' || $path == 'admin/users/seller-orders/{_id}' || $path == 'admin/users/buyer-orders/{_id}' || $path == 'admin/users/edit/sellerinperson/{_id}' || $path == 'admin/users/edit/selleronline/{_id}' ) active @endif treeview" >
            <a href="#">
            	<i class="fa fa-users"></i> <span> Users Management</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          	</a>
	          <ul class="treeview-menu">
	            <li @if( $path == 'admin/users' || $path == 'admin/users/add/{id}' || $path == 'admin/users/edit/{id}' || $path == 'admin/users/edit/password/{id}' || $path == 'admin/users/edit/billing/{id}' || $path == 'admin/users/edit/seller' || $path == 'admin/users/seller-reviews/{_id}' || $path == 'admin/users/buyer-reviews/{_id}' || $path == 'admin/users/seller-orders/{_id}' || $path == 'admin/users/buyer-orders/{_id}') class="active" @endif>
                  <a href="{{url('admin/users')}}"><i class="fa fa-users"></i> Users</a>
              </li>
              
            </ul>
        </li>
        
        <li class="@if( $path == 'admin/job' || $path == 'admin/job/add/{id}' || $path == 'admin/job/edit/{id}' ) active @endif treeview">
          <a href="{{url('admin/job')}}"><i class="fa fa-tasks"></i> <span>Job Management</span></a>
        </li>

         <li class="@if($path == 'admin/courses-language' || $path == 'admin/courses-language/add/{id}' || $path == 'admin/courses-language/edit/{id}' || $path == 'admin/courses-category' || $path == 'admin/courses-category/add/{id}' || $path == 'admin/courses-category/edit/{id}' || $path == 'admin/courses-level' || $path == 'admin/courses-level/add/{id}' || $path == 'admin/courses-level/edit/{id}' ||             $path == 'admin/courses' || $path == 'admin/courses/add/{id}' || $path == 'admin/courses/edit/{id}' || $path == 'admin/coursechapters' || $path == 'admin/coursechapters/add/{id}' || $path == 'admin/coursechapters/edit/{id}' || $path == 'admin/coursefaqs' || $path == 'admin/coursefaqs/add/{id}' || $path == 'admin/coursefaqs/edit/{id}' || $path == 'admin/coursecomments' || $path == 'admin/coursecomments/add/{id}' || $path == 'admin/coursecomments/edit/{id}') active @endif treeview">
            <a href="#">
              <i class="fa fa-file"></i> <span>Course Management</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
               
              <li @if( $path == 'admin/courses-language' || $path == 'admin/courses-language/add/{id}' || $path == 'admin/courses-language/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/courses-language')}}"><i class="fa fa-file"></i>Courses Languages</a>
              </li>

              <li @if( $path == 'admin/courses-category' || $path == 'admin/courses-category/add/{id}' || $path == 'admin/courses-category/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/courses-category')}}"><i class="fa fa-file"></i>Courses Category</a>
              </li>

              <li @if( $path == 'admin/courses-level' || $path == 'admin/courses-level/add/{id}' || $path == 'admin/courses-level/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/courses-level')}}"><i class="fa fa-file"></i>Courses Level</a>
              </li> 

              <li @if( $path == 'admin/courses' || $path == 'admin/courses/add/{id}' || $path == 'admin/courses/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/courses')}}"><i class="fa fa-file"></i> Courses</a>
              </li>
            </ul>
        </li>

        <li class="@if($path == 'admin/order/courses' || $path == 'admin/order/courses/edit/{id}' || $path == 'admin/order/skill' || $path == 'admin/order/skill/edit/{id}' || $path == 'admin/order/user-plan' || $path == 'admin/order/user-plan/edit/{id}' || $path == 'admin/order/resolution-center' || $path == 'admin/order/resolution-center/edit/{id}') active @endif treeview">
            <a href="#">
              <i class="fa fa-file"></i> <span>Order Management</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
             
              <li @if( $path == 'admin/order/user-plan' || $path == 'admin/order/user-plan/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/order/user-plan')}}"><i class="fa fa-file"></i>User subscription</a>
              </li>

              <li @if( $path == 'admin/order/courses' || $path == 'admin/order/courses/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/order/courses')}}"><i class="fa fa-file"></i>Courses Order</a>
              </li>

              <li @if( $path == 'admin/order/skill' || $path == 'admin/order/skill/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/order/skill')}}"><i class="fa fa-file"></i>Skill Order</a>
              </li>

              <li @if( $path == 'admin/order/resolution-center' || $path == 'admin/order/resolution-center/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/order/resolution-center')}}"><i class="fa fa-file"></i>Resolution Center</a>
              </li>

            </ul>
        </li>

        <li class="@if( $path == 'admin/plan' || $path == 'admin/plan/add/{id}' || $path == 'admin/plan/edit/{id}' ) active @endif treeview">
          <a href="{{url('admin/plan')}}"><i class="fa fa-file-text"></i> <span>Plan Management</span></a>
        </li>

        <li class="@if( $path == 'admin/blogs' || $path == 'admin/blogs/add/{id}' || $path == 'admin/blogs/edit/{id}' ) active @endif treeview">
          <a href="{{url('admin/blogs')}}"><i class="fa fa-file-text"></i> <span>Blog Management</span></a>
        </li>

        <li class="@if( $path == 'admin/pages' || $path == 'admin/pages/add/{id}' || $path == 'admin/pages/edit/{id}' ) active @endif treeview">
          <a href="{{url('admin/pages')}}"><i class="fa fa-file-text"></i> <span>Pages Management</span></a>
        </li>

        <li class="@if( $path == 'admin/pages-template' || $path == 'admin/pages-template/add/{id}' || $path == 'admin/pages-template/edit/{id}' ) active @endif treeview">
          <a href="{{url('admin/pages-template')}}"><i class="fa fa-file-text"></i> <span>Pages Templates</span></a>
        </li>
        
        
        <li class="@if( $path == 'admin/newsletterusers' || $path == 'admin/newsletterusers/add/{id}' || $path == 'admin/newsletterusers/edit/{id}' || $path == 'admin/sendnewsletters' || $path == 'admin/sendnewsletters/add/{id}' || $path == 'admin/sendnewsletters/edit/{id}' ) active @endif treeview">
            <a href="#">
              <i class="fa fa-hacker-news"></i> <span>Newsletter Management</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              <li @if( $path == 'admin/newsletterusers' || $path == 'admin/newsletterusers/add/{id}' || $path == 'admin/newsletterusers/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/newsletterusers')}}"><i class="fa fa-male"></i> Newsletter Users</a>
              </li>

              <!-- <li @if( $path == 'admin/sendnewsletters' || $path == 'admin/sendnewsletters/add/{id}' || $path == 'admin/sendnewsletters/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/sendnewsletters')}}"><i class="fa fa-newspaper-o"></i> Send Newsletter</a>
              </li> -->

            </ul>
        </li>

       
        

        <li class="@if( $path == 'admin/feedback' || $path == 'admin/feedback/add/{id}' || $path == 'admin/feedback/edit/{id}' ) active @endif treeview">
          <a href="{{url('admin/feedback')}}"><i class="fa fa-file-text"></i> <span>Feedback Management</span></a>
        </li>


        <li class="@if( $path == 'admin/supportquery' || $path == 'admin/supportquery/add/{id}' || $path == 'admin/supportquery/edit/{id}' || $path == 'admin/support' || $path == 'admin/support/add/{id}' || $path == 'admin/support/edit/{id}' ) active @endif treeview">
            <a href="#">
              <i class="fa fa-question-circle"></i> <span>Support Management</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              
              <li @if( $path == 'admin/supportquery' || $path == 'admin/supportquery/add/{id}' || $path == 'admin/supportcategory/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/supportquery')}}"><i class="fa fa-question-circle"></i> Support Query</a>
              </li>

              <li @if( $path == 'admin/support' || $path == 'admin/support/add/{id}' || $path == 'admin/support/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/support')}}"><i class="fa fa-question-circle"></i> Support</a>
              </li>

            </ul>
        </li>

       
        {{-- <li class="@if( $path == 'admin/messages' || $path == 'admin/messages/view/{id}' || $path == 'admin/reviews' || $path == 'admin/reviews/add/{id}' || $path == 'admin/reviews/edit/{id}') active @endif treeview">
            <a href="#">
              <i class="fa fa-comments"></i> <span>Messaging & Reviews</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              <li @if( $path == 'admin/messages' || $path == 'admin/messages/view/{id}') class="active" @endif>
                  <a href="{{url('admin/messages')}}"><i class="fa fa-comment"></i>Messages</a>
              </li>

              <li @if( $path == 'admin/reviews' || $path == 'admin/reviews/add/{id}' || $path == 'admin/reviews/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/reviews')}}"><i class="fa fa-commenting"></i>Reviews</a>
              </li>

            </ul>
        </li> --}}

         <li class="@if( $path == 'admin/faqs' || $path == 'admin/faqs/add/{id}' || $path == 'admin/faqs/edit/{id}' || $path == 'admin/faqs' || $path == 'admin/faqs/add/{id}' || $path == 'admin/faqs/edit/{id}' ) active @endif treeview">
            <a href="#">
              <i class="fa fa-file-text"></i> <span>FAQs Management</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              <li @if( $path == 'admin/faqs' || $path == 'admin/faqs/add/{id}' || $path == 'admin/faqs/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/faqs')}}"><i class="fa fa-file-text"></i> FAQs</a>
              </li>
            </ul>
        </li>

        <li class="@if( $path == 'admin/skillbox-job-category' || $path == 'admin/skillbox-job-category/add/{id}' || $path == 'admin/skillbox-job-category/edit/{id}' || $path == 'admin/skillbox-job' || $path == 'admin/skillbox-job/add/{id}' || $path == 'admin/skillbox-job/edit/{id}' ) active @endif treeview">
            <a href="#">
              <i class="fa fa-file-text"></i> <span>Skillbox Job Management</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              <li @if( $path == 'admin/skillbox-job-category' || $path == 'admin/skillbox-job-category/add/{id}' || $path == 'admin/skillbox-job-category/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/skillbox-job-category')}}"><i class="fa fa-file-text"></i> Job Category</a>
              </li>
              <li @if( $path == 'admin/skillbox-job' || $path == 'admin/skillbox-job/add/{id}' || $path == 'admin/skillbox-job/edit/{id}') class="active" @endif>
                  <a href="{{url('admin/skillbox-job')}}"><i class="fa fa-file-text"></i> Job</a>
              </li>
            </ul>
        </li>

        <li class="@if( $path == 'admin/notify-user' || $path == 'admin/notify-user/add/{id}' || $path == 'admin/notify-user/edit/{id}' ) active @endif treeview">
          <a href="{{url('admin/notify-user')}}"><i class="fa fa-file-text"></i> <span>Notify Users</span></a>
        </li>


        <li class="@if( $path == 'admin/report' ) active @endif treeview">
          <a href="{{url('admin/report')}}"><i class="fa fa-file-text"></i> <span>Report Managment</span></a>
        </li>

        <li class="@if( $path == 'admin/block-ip' || $path == 'admin/block-ip/add/{id}' || $path == 'admin/block-ip/edit/{id}' ) active @endif treeview">
          <a href="{{url('admin/block-ip')}}"><i class="fa fa-file-text"></i> <span>Block IP Management</span></a>
        </li>

        
    
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>