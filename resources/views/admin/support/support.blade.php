@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {border: 1px solid #eee !important;}
  .chat_window{max-height: 400px;overflow-y: auto;padding: 10px;border: 1px solid #f1f1f1;border-radius: 4px;}
  .chat_window .media .media-object {
    border-radius: 50%;
    width: 70px;
    height: 70px;
    object-fit: cover;
    object-position: center;
  }
  .chat_window .media .media-body {
    background-color: #f1f1f1;
    padding: 10px 15px;
    border-radius: 10px;
    position: relative;
  }
  .chat_window .media{margin-bottom: 15px;}
  .chat_window .media .media-heading{font-weight: bold;}
  .chat_window .media_admin .media-body{background-color: #f1f1f1;text-align: left;}

</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Support
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">support</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} Support</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            
            <div class="box-body">
              <br/><br/>
               <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/support/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                
                <div class="row">
                    <div class="col-md-6">
                       
                        <div class="form-group">
                          <label>From user</label>
                          <select class="form-control" id="i_user_id" name="i_user_id" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                          @if(isset($users) && count($users))
                              @foreach($users as $key => $val)
                                <option value="{{$val->id}}" @if(isset($data->i_user_id) && $data->i_user_id == $val->id) selected @endif >{{$val->v_fname}} {{$val->v_lname}}</option>
                              @endforeach
                            @endif
                          </select>

                        </div>

                        <div class="form-group">
                          <label>Select the field regarding your query*</label>
                          <select class="form-control" id="e_query" name="e_query" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            @if(isset($supportquery) && count($supportquery))
                              @foreach($supportquery as $key => $val)
                                <option value="{{$key}}" @if(isset($data->e_query) && $data->e_query == $key) selected @endif >{{$val}}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Support category</label>
                            <select class="form-control" id="e_type" name="e_type" required=""  data-parsley-trigger="keyup">
                              <option value="">-select-</option>
                              <option value="inperson" @if( isset($data->e_type ) && $data->e_type == 'inperson') SELECTED @endif >In Person</option>
                              <option value="online" @if( isset($data->e_type ) && $data->e_type == 'online') SELECTED @endif >Online</option>
                              <option value="both" @if( isset($data->e_type) && $data->e_type == 'course' ) selected @endif>Course</option>
                            </select>
                        </div>

                        <div class="form-group">
                          <label>Priority</label>
                          <select class="form-control" id="e_priority" name="e_priority" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="low" @if( isset($data->e_priority ) && $data->e_priority == 'low') SELECTED @endif >Low</option>
                            <option value="medium" @if( isset($data->e_priority ) && $data->e_priority == 'medium') SELECTED @endif >Medium</option>
                             <option value="high" @if( isset($data->e_priority) && $data->e_priority == 'high' ) selected @endif>High</option>
                          </select>
                      </div>

                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Question</label>
                            <textarea name="l_question" id="l_question" class="form-control" required="" rows="5" data-parsley-id="9363">{{$data->l_question or old('l_question')}}</textarea>
                            <ul class="parsley-errors-list" id="parsley-id-9363"></ul>
                        </div>
                    </div>

                    @if(isset($data->v_document) && count($data->v_document))
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Attached Document</label>
                            <ul>
                            @foreach($data->v_document as $k=>$v)
                            @php
                                $docurl = '';
                                if(Storage::disk('s3')->exists($v)){
                                    $docurl = \Storage::cloud()->url($v);
                                }
                            @endphp  
                            <li><a href="{{$docurl}}" download target="_blank" >{{$v}}</a></li>
                            @endforeach
                        </ul>
                        </div>
                    </div>
                    @endif


                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Status<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="open" @if( isset($data->e_status) && $data->e_status == 'open' ) selected @endif>Open</option>
                            <option value="close" @if( isset($data->e_status ) && $data->e_status == 'close') SELECTED @endif >Closed</option>
                          </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Assign admin<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="i_admin_id" name="i_admin_id">
                              <option value="">- select -</option>
                              @if(isset($admin_list) && count($admin_list))
                                @foreach($admin_list as $k=>$v)
                                    <option value="{{$v->id}}" @if( isset($data->i_admin_id) && $data->i_admin_id == $v->id) selected @endif >{{$v->v_name}}</option>
                                @endforeach
                              @endif  
                          </select>
                        </div>
                    </div>

                 </div>

                  <div style="clear:both"></div>
                  <div class="row">
                    <div class="col-md-12">
                        <a href="{{url('admin/support')}}">
                        <button type="button" class="btn btn-warning">Back to List</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                 </form>       

                  <div class="clear:both"></div>                
                  </div>
                
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>

            <div class="box-body">
              <div class="row">
                
                <div class="col-md-12">
                   <div class="chat_window">
                        
                        @if(count($messagedata))
                            @foreach($messagedata as $k=>$v)
                              @if(isset($v->e_user_type) && $v->e_user_type=="admin")

                                   <?php 
                                      $userdata=array();
                                      if(count($v->hasAdmin())){
                                          $userdata=$v->hasAdmin();
                                      }
                                      $v_name="";
                                      if(count($v->hasAdmin()) && isset($v->hasAdmin()->v_name)){
                                          $v_name = $v->hasAdmin()->v_name;
                                      }
                                      $v_profile="";
                                      if(count($v->hasAdmin()) && isset($v->hasAdmin()->v_profile)){
                                          $v_profile = $v->hasAdmin()->v_profile;
                                          $v_profile = \Storage::cloud()->url($v_profile);
                                      }
                                      $imgdata = \App\Helpers\General::adminroundimage($userdata);
                                  ?>  
                                  <div class="media media_admin">
                                    <div class="media-body">
                                      <h4 class="media-heading">{{$v_name}}</h4>
                                      <p><?php 
                                          if(isset($v->l_question)){
                                              echo $v->l_question;
                                          }else{
                                              echo "";
                                          }
                                      ?></p>
                                    </div>
                                    <div class="media-right">
                                      <img src="{{$v_profile}}" class="media-object">
                                    </div>
                                  </div>
                              @else 

                                  <?php 
                                        $v_image="";
                                        if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
                                            $v_image=$v->hasUser()->v_image;
                                            $v_image = \Storage::cloud()->url($v_image);
                                        }
                                        $v_fname="";
                                        if(count($v->hasUser()) && isset($v->hasUser()->v_fname)){
                                            $v_fname=$v->hasUser()->v_fname;
                                        }
                                        $v_lname="";
                                        if(count($v->hasUser()) && isset($v->hasUser()->v_lname)){
                                            $v_lname=$v->hasUser()->v_lname;
                                        }
                                    ?>  

                                  <div class="media media_user">
                                    <div class="media-left">
                                      <img src="{{$v_image}}" class="media-object">
                                    </div>
                                    <div class="media-body">
                                      <h4 class="media-heading">{{ucfirst($v_fname)}} {{ucfirst($v_lname)}}</h4>
                                      <p><?php 
                                          if(isset($v->l_question)){
                                              echo $v->l_question;
                                          }else{
                                              echo "";
                                          }
                                      ?></p>
                                    </div>
                                  </div>
                              @endif
                          @endforeach
                      @endif        
                    </div>
                </div>

              </div>
            </div>
          


            <div class="box-body">
              <br/><br/>
               <form class="horizontal-form" role="form" method="POST" name="sendrep"  action="{{url('admin/support/action/sendreply')}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Send Reply<span class="asterisk_input">*</span></label>
                            <textarea name="l_question"  class="form-control" required="" rows="5"></textarea>
                            <ul class="parsley-errors-list" id="parsley-id-9363"></ul>
                        </div>
                    </div>
                 </div>
                 <div style="clear:both"></div>
                  <div class="row">
                    <div class="col-md-12">
                        <a href="{{url('admin/support')}}">
                        <button type="button" class="btn btn-warning">Back to List</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Send Reply</button>
                    </div>
                  </div>
                 </form>       
                  <div class="clear:both"></div>                
                </div>
          </div>
        </div>


      </section>      
      

    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <div class="box-header pull-right">
              <a href="{{url('admin/support/add/0')}}">
                 <button type="submit" class="btn btn-info">Add New Support</button>
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Username</th>
                  <th>Created Date</th>
                  <th>Satus</th>
                  <th>Query</th>
                  <th>Assigned to</th>
                  <th>Priority</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )
                    <tr>
                      <?php 
                          $v_fname="";
                          if(count($value->hasUser()) && isset($value->hasUser()->v_fname)){
                              $v_fname=$value->hasUser()->v_fname;
                          }

                          $v_lname="";
                          if(count($value->hasUser()) && isset($value->hasUser()->v_lname)){
                              $v_lname=$value->hasUser()->v_lname;
                          }
                      ?> 
                      <td> {{$count+1}}
                        {{-- {{$value->i_ticket_num or ''}} --}}
                      </td>
                      <td>{{ucfirst($v_fname)}} {{ucfirst($v_lname)}}</td>
                      <td>{{isset($value->d_added) ? date("d/m/Y",strtotime($value->d_added)):''}}</td>
                      
                      <td>
                        @if(isset($value->e_status) && $value->e_status=="open")
                            <span class="badge bg-green">Open</span>         
                        @else
                            <span class="badge bg-yellow">Closed</span>
                        @endif
                      </td>
                      <td>{{isset($supportquery[$value->e_query]) ? $supportquery[$value->e_query]:''}}</td>
                      <td>
                        @php
                        $v_name="";
                        if(count($value->hasAdminAssign()) && isset($value->hasAdminAssign()->v_name)){
                            $v_name = $value->hasAdminAssign()->v_name;
                        }
                        @endphp
                        {{$v_name}}
                      </td>
                      <td>{{isset($value->e_priority) ? $value->e_priority:''}}</td>
                      <td>
                        <a href="{{url('admin/support/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/support',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if( $value->e_status == 'close' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                        @php $a=url('admin/support/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','<?php echo $a ?>')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>

                    </tr>
                  @endforeach
              
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
</script>

<script type="text/javascript">

jQuery(document).ready(function(){
  var i = 0;
  jQuery(".add-more").click(function(){ 
    i++;
        jQuery("#copy").append('<div id="mainparent"><div class="control-group input-group" id="control'+i+'" style="margin-top:10px; width:100%; margin-bottom: 10px"><textarea name="l_answer[]" id="l_answer'+i+'" class="form-control" rows="5"></textarea></div><div class="input-group-btn"><button class="btn btn-danger remove" id="remove'+i+'" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button></div></div>');
    });

   jQuery("#copy").on("click",".remove",function(){ 
    var id=  jQuery(this).parent().parent().attr("id");
       jQuery("#"+id).remove();
   });

   jQuery("#copy1").on("click",".remove",function(){ 
    var id=  jQuery(this).parent().parent().attr("id");
       jQuery("#"+id).remove();
   });
}); 

</script>

@stop

