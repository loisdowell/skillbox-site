@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/public/Assets/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
.active-manu {
   background:  #2c4762 !important;
   color: #fff;
}
.active-manu a{
  color: #fff;  
}
.list-group-item{
  padding-left: 10px !important;
}
.user-avatar{
  border-radius: 50%;
  background-color: #df21a1;
  color: white;
  width: 40px;
  height: 40px;
  font-size: 22px;
  text-align: center;
  line-height: 40px;

}
.edit-user-avatar{
  border-radius: 50%;
  background-color: #df21a1;
  color: white;
  width: 180px;
  height: 180px;
  font-size: 120px;
  text-align: center;
  line-height: 180px;
  margin: auto;
}
</style>
@stop


@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Users
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/users')}}">Users</a></li>
        <li class="active">users</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif
      </div>  


      <section class="content">
          <div class="row">
              <div class="col-md-3">
                  <div class="box box-primary">
                      <div class="box-body box-profile">

                          @if($view=="add")
                          <img class="profile-user-img img-responsive img-circle" src="{{url('public/theme/images/no-image.png')}}" alt="profile picture" style="height: 250px;width: 250px">
                          @else
                              @if(isset($data->v_image) && !is_array($data->v_image) && $data->v_image != '' )
                                @php
                                  $imgdata = \Storage::cloud()->url($data->v_image);
                                @endphp 
                                <img class="profile-user-img img-responsive img-circle" src="{{$imgdata}}" alt="profile picture" style="height: 250px;width: 250px">
                              @else
                                @if(isset($data->v_fname) && $data->v_fname != '' || isset($data->v_lname) && $data->v_lname != '' )
                                  <div class="edit-user-avatar">
                                    {{strtoupper(substr($data->v_fname, 0, 1))}}{{strtoupper(substr($data->v_lname, 0, 1))}}
                                  </div>
                                @else
                                  @php
                                      $imgdata = \Storage::cloud()->url('users/img-upload.png');   
                                  @endphp
                                  <img class="profile-user-img img-responsive img-circle" src="{{$imgdata}}" alt="profile picture" style="height: 250px;width: 250px">
                                @endif  
                              @endif    

                          @endif 
                          
                          <h3 class="profile-username text-center">{{isset($data->v_fname) ? $data->v_fname:'User Name' }} {{isset($data->v_lname) ? $data->v_lname:'' }}</h3>

                          <p class="text-muted text-center">{{isset($data->v_email) ? $data->v_email:'User Email' }}</p>
                          <p class="text-muted text-center"><b>Email verified :</b> {{isset($data->e_email_confirm) ? $data->e_email_confirm:'no' }}</p>
                          
                          <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                  <b>Buyer Account Status</b>
                                  <ul class="list-group list-group-unbordered">
                                    <b>In Person :-</b> {{isset($data->v_buyer_inperson_service) ? $data->v_buyer_inperson_service : ''}}</br>
                                    <b>Online :-</b> {{isset($data->v_buyer_online_service) ? $data->v_buyer_online_service : ''}}
                                  </ul>
                              </li>
                              <li class="list-group-item">
                                  <b>Seller Account Status</b>
                                  <ul class="list-group list-group-unbordered">
                                    <b>In Person :-</b> {{isset($data->v_seller_inperson_service) ? $data->v_seller_inperson_service : ''}}</br>
                                    <b>Online :-</b> {{isset($data->v_seller_online_service) ? $data->v_seller_online_service : ''}}
                                  </ul>
                              </li>
                          </ul>
                      </div>
                  </div>

                  <div class="box box-primary">
                      <ul class="list-group list-group-unbordered">
                          <li class="list-group-item active-manu">
                              <a href="javascript:;">
                                <b>Edit Main profile</b>
                              </a>    
                          </li>

                          @if ($view!="add")

                          <li class="list-group-item">
                              <a href="{{url('admin/users/edit/sellerinperson')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Seller In Person Profile</b>
                              </a>
                          </li>

                          <li class="list-group-item">
                              <a href="{{url('admin/users/edit/selleronline')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Seller Online Profile</b>
                              </a>
                          </li>

                           <li class="list-group-item" >
                              <a href="{{url('admin/users/edit/password')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Change Password</b>
                              </a>
                          </li>

                          <li class="list-group-item" >
                              <a href="{{url('admin/users/buyer-orders')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Buyer Orders</b>
                              </a>
                          </li>

                          <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-orders')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Seller Orders</b>
                              </a>
                          </li>
                          <li class="list-group-item" >
                              <a href="{{url('admin/users/buyer-reviews')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Buyer Reviews</b>
                              </a>
                          </li>
                          
                          <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-reviews')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Seller Skill Reviews</b>
                              </a>
                          </li>

                          <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-reviews/course')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Seller Course Reviews</b>
                              </a>
                          </li>

                         
                         {{--  <li class="list-group-item">
                              <a href="{{url('admin/users/edit/billing')}}/{{isset($data->id) ? $data->id:'0'}}" target="_blank">
                                <b>Edit Billing Detail</b>
                              </a>
                          </li> --}}
                          
                          @endif
                           
                      </ul>
                  </div>
              </div>
              <!-- /.col -->


              <div class="col-md-9">
                  <div class="nav-tabs-custom">
                      
                      <a class="btn btn-block btn-social btn-tumblr">
                        <i class="fa fa-info"></i>Personal Information
                      </a>

                      <div class="box-body">
                        <div class="row">
                            
                            <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/users/update-user-profile')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            
                              <div class="col-md-6">
                                
                                <div class="form-group">
                                  <label>First Name<span class="asterisk_input">*</span></label>
                                  <input type="text" name="v_fname" id="v_fname" class="form-control" placeholder="Name" value="{{isset($data->v_fname)?$data->v_fname:''}}"" required="" data-parsley-trigger="keyup">
                                </div>

                                <div class="form-group">
                                  <label>Email<span class="asterisk_input">*</span></label>
                                  <input type="email" name="v_email" id="v_email" class="form-control" placeholder="Email" value="{{isset($data->v_email)?$data->v_email:''}}" required="" data-parsley-trigger="keyup">
                                </div>

                                <div class="form-group">
                                    <label>Country :<span class="asterisk_input">*</span></label>
                                    <select class="form-control" name="i_country_id" required="" >
                                      <option value="">- select -</option>
                                      @if(isset($country) && count($country))
                                          @foreach($country as $k=>$v)
                                              <option value="{{$v->id}}" @if( isset($data->i_country_id) && $data->i_country_id==$v->id) selected @endif>{{$v->v_title}}</option>
                                          @endforeach
                                      @endif    
                                    </select>
                                </div>

                                <div class="form-group">
                                  <label>Password @if($view=="add") <span class="asterisk_input">*</span> @endif </label>
                                  <input type="password" name="password" id="password" class="form-control" @if($view=="add") required @endif >
                                </div>

                                <div class="form-group">
                                  <label>Address</label>
                                  <input type="text" name="l_address" id="l_address" class="form-control" placeholder="Address" value="{{isset($data->l_address)?$data->l_address:''}}">
                                </div>

                                <div class="form-group">
                                  <label>Birthday:</label>
                                  <div class="input-group date">
                                    <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="d_birthday" class="form-control pull-right" id="datepicker" value="{{isset($data->d_birthday)?$data->d_birthday:''}}">
                                  </div>
                                </div>

                                <?php
                                    $compnaytype=array(
                                        'individual'=>'Individual',
                                        'sole proprietor'=>'Sole proprietor',
                                        'Public limited company'=>'Public limited company',
                                        'Private company limited'=>'Private company limited',
                                        'Limited liability partnership'=>'Limited liability partnership',
                                        'Community interest company'=>'Community interest company',
                                    );
                                    $daydata=array(
                                    'monday'=>'Monday',
                                    'tuesday'=>'Tuesday',
                                    'wednesday'=>'Wednesday',
                                    'thursday'=>'Thursday',
                                    'friday'=>'Friday',
                                    'saturday'=>'Saturday',
                                    'sunday'=>'Sunday',
                                );

                                ?>
                                <?php /*
                                <div class="col-md-6 form-group">
                                   <label>Availabel From :</label>
                                    <select class="form-control" id="v_avalibility_from" name="v_avalibility_from">
                                      <option value="">-Select-</option>
                                            @foreach($daydata as $k=>$v)
                                                <option value="{{$k}}" @if(isset($data->v_avalibility_from) && $data->v_avalibility_from==$k) selected @endif >{{$v}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                 <label> Availabel to :</label>
                                    <select class="form-control" id="v_avalibility_to" name="v_avalibility_to">
                                      <option value="">-Select-</option>
                                        @foreach($daydata as $k=>$v)
                                            <option value="{{$k}}" @if(isset($data->v_avalibility_to) && $data->v_avalibility_to==$k) selected @endif >{{$v}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                */ ?>

                                <div class="form-group">
                                    <label>Company type</label>
                                    <select class="form-control" id="v_company_type" name="v_company_type">
                                      <option value="">-Select-</option>
                                        @if(count($compnaytype))
                                            @foreach($compnaytype as $k=>$v)
                                                <option value="{{$k}}" @if(isset($data->v_company_type) && $data->v_company_type==$k) selected @endif >{{$v}}</option>
                                            @endforeach
                                       @endif  
                                    </select>
                                </div>

                                <div class="form-group">
                                  <label>VAT number</label>
                                  <input type="text" name="v_vat_number" class="form-control" placeholder="VAT number" value="{{isset($data->v_vat_number)?$data->v_vat_number:''}}">
                                </div>

                                @if ($view=="edit")
                                <div class="form-group">
                                  <div><label>Image</label></div>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                      <div class="fileinput-new thumbnail" style="width: 200px;">
                                        
                                        @if( isset($data->v_image) && !is_array($data->v_image) && $data->v_image != '' )
                                          
                                          @php
                                            $imgdata = \Storage::cloud()->url($data->v_image);   
                                          @endphp
                                          <img src="{{$imgdata}}" style="max-width: 150px" alt="" />

                                        @else

                                          @if(isset($data->v_fname) && $data->v_fname != '' || isset($data->v_lname) && $data->v_lname != '' )
                                            <div class="edit-user-avatar">
                                              {{strtoupper(substr($data->v_fname, 0, 1))}}{{strtoupper(substr($data->v_lname, 0, 1))}}
                                            </div>
                                          @else
                                            @php
                                                $imgdata = \Storage::cloud()->url('users/img-upload.png');   
                                            @endphp
                                            <img class="profile-user-img img-responsive img-circle" src="{{$imgdata}}" alt="profile picture" style="height: 250px;width: 250px">
                                          @endif
                                        @endif
                                      </div>
                                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                                      <div>
                                         <span class="btn default btn-file">
                                            <span class="fileinput-new"> Select Image </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="v_image" > </span>
                                         <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                      </div>
                                    </div>
                                </div>
                                @else
                                <div class="form-group">
                                  <div><label>Image</label></div>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                                      <div>
                                         <span class="btn default btn-file">
                                            <span class="fileinput-new"> Select Image </span>
                                            <span class="fileinput-exists"> Change </span>
                                            <input type="file" name="v_image" required> </span>
                                         <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                      </div>
                                    </div>
                                </div>
                                @endif
                              
                              </div>
                              
                              <div class="col-md-6">
                                
                                <div class="form-group">
                                  <label>Last Name<span class="asterisk_input">*</span></label>
                                  <input type="text" name="v_lname" id="v_lname" class="form-control" placeholder="Last Name" value="{{isset($data->v_lname)?$data->v_lname:''}}" required="" data-parsley-trigger="keyup">
                                </div>

                                <div class="form-group">
                                    <label>Contact Phone:</label>
                                    <input type="text" name="v_contact" id="v_contact" class="form-control" placeholder="Phone" value="{{$data->v_contact or old('v_contact')}}" data-parsley-type="digits" minlength="6" maxlength="10" data-parsley-trigger="keyup">
                                </div>

                                <div class="form-group">
                                    <label>City:<span class="asterisk_input">*</span></label>
                                    <input type="text" name="v_city" class="form-control" placeholder="City" value="{{$data->v_city or old('v_city')}}" required>
                                </div>

                                <div class="form-group">
                                    <label>Status<span class="asterisk_input">*</span></label>
                                    <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                                      <option value="">- select -</option>
                                      <option value="active" @if( isset($data->e_status) && $data->e_status == 'active' ) selected @endif>Active</option>
                                      <option value="inactive" @if( isset($data->e_status ) && $data->e_status == 'inactive') SELECTED @endif >Inactive</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                  <label>Postcode</label>
                                  <input type="text" name="v_postcode" id="v_postcode" class="form-control" placeholder="Postcode" value="{{isset($data->v_postcode)?$data->v_postcode:''}}">
                                </div>

                                <div class="form-group">
                                    <label>Gender<span class="asterisk_input">*</span></label>
                                    <select class="form-control" name="v_gender" required="">
                                      <option value="">- select -</option>
                                      <option value="male" @if( isset($data->v_gender) && $data->v_gender == 'male' ) selected @endif>Male</option>
                                      <option value="female" @if( isset($data->v_gender ) && $data->v_gender == 'female') selected @endif >Female</option>
                                    </select>
                                </div>
                                <?php /*
                                 <div class="col-md-6 form-group">
                                   <label>From time</label>
                                    <select class="form-control" id="v_avalibility_from_time" name="v_avalibility_from_time">
                                      <option value="">-Select-</option>
                                            <?php
                                            for($i=1;$i<=24;$i++){ ?>
                                                <option value="{{$i}}" @if(isset($data->v_avalibility_from_time) && $data->v_avalibility_from_time==$i) selected @endif>{{$i}}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                 <label> To time </label>
                                    <select class="form-control" id="v_avalibility_to_time" name="v_avalibility_to_time">
                                      <option value="">-Select-</option>
                                        <?php
                                            for($i=1;$i<=24;$i++){ ?>
                                                <option value="{{$i}}" @if(isset($data->v_avalibility_to_time) && $data->v_avalibility_to_time==$i) selected @endif>{{$i}}</option>
                                        <?php } ?>
                                    </select>
                                </div> */ ?>

                                <div class="form-group">
                                  <label>Company Name </label>
                                  <input type="text" name="v_company_name" id="v_company_name" class="form-control" placeholder="Company Name" value="{{isset($data->v_company_name)?$data->v_company_name:''}}">
                                </div>

                                <div class="form-group">
                                  <label>Company registration number </label>
                                  <input type="text" name="v_company_regnumber" id="v_company_regnumber" class="form-control" placeholder="Company registration number" value="{{isset($data->v_company_regnumber)?$data->v_company_regnumber:''}}">
                                </div>

                               <div class="form-group">
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" name="i_newsletter" @if(isset($data->i_newsletter) && $data->i_newsletter=="on") checked @endif>
                                    Newsletter & promotion
                                  </label>
                                </div>
                                </div>

                              </div>
                              <div class="col-md-12">
                                  <a href="{{url('admin/users')}}">
                                  <button type="button" class="btn btn-warning">Back to List</button>
                                  </a>
                                  <button type="submit" class="btn btn-primary">Save</button>
                              </div>
                              
                            </form>
                        </div>
                  </div>
              </div>

          </div>
      </section>

   
              
    @else
    

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        
        @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

          <div class="box">
            <div class="box-header pull-right">
              <a href="{{url('admin/users/add/0')}}">
                 <button type="submit" class="btn btn-info">Add New User</button>
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Account Status</th>
                  <th>Added Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )
                    <tr>
                      <td align="center">
                        @if(isset($value->v_image) && !is_array($value->v_image) && $value->v_image != '' )
                          @php
                              $imgdata = \Storage::cloud()->url($value->v_image);   
                          @endphp
                          <img src="{{$imgdata}}" height="50" width="50" alt="" />
                        @else
                          @if(isset($value->v_fname) && $value->v_fname != '' || isset($value->v_lname) && $value->v_lname != '' )
                            <div class="user-avatar">
                              {{strtoupper(substr($value->v_fname, 0, 1))}}{{strtoupper(substr($value->v_lname, 0, 1))}}
                            </div>
                          @else
                            @php
                                $imgdata = \Storage::cloud()->url('users/img-upload.png');   
                            @endphp
                            <img src="{{$imgdata}}" height="50" width="50" alt="" />  
                          @endif
                        @endif
                      </td>
                      <td>{{$value->v_fname or ''}} {{$value->v_lname or ''}}</td>
                      <td><a href="mailto:{{$value->v_email}}">{{$value->v_email or ''}}</a></td>
                      
                      <td align="center">
                        @if(isset($value->e_status) && $value->e_status=="active")
                            <span class="badge bg-green">Active</span>         
                        @else
                            <span class="badge bg-yellow">Inactive</span>
                        @endif
                      </td>
                      <td>{{ isset($value->d_added) ? date('M d Y' ,strtotime($value->d_added)) : '' }}</td>
                      <td>
                        <a href="{{url('admin/users/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/users',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if($value->e_status == 'inactive' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                        @php $a=url('admin/users/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','<?php echo $a; ?>')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                        <a href="{{url('admin/users/action/resetpassword')}}/{{$value->id}}" title="Send Reset password" class="btn btn-primary  btn-sm"><i class="fa fa-key"></i></a>

                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{url('/public/Assets/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });

   $('#datepicker').datepicker({
      autoclose: true
    })
  

  function selectSubCat(id,selectedid=""){

      if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/subCategories/')}}",
                data:'category_id='+id+"&sub_category_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    $("#i_sub_category_id").html(obj['optingStr'])
                }
            }); 
        }
    
  }

  // $('#i_parent_id').on('change',function(){
  //       var categoryID = $(this).val();
  //       if(categoryID){
  //           $.ajax({
  //               type:'get',
  //               url:"{{url('admin/users/subCategories/')}}",
  //               data:'category_id='+categoryID,
  //               success:function(data){
  //                   var obj1 = JSON.parse(data);
  //                   //alert(obj['status']);
  //                   $("#i_sub_category_id").html(obj1['optingStr'])
  //               }
  //           }); 
  //       }
  // });

  function selectSkill(id,selectedid=""){
        
        if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/skills/')}}",
                data:'category_id='+id+"&selected_skill_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    //alert(obj['status']);
                    $("#i_main_skill_id").html(obj['optingStr'])
                }
            }); 
        }
  }

  function selectState(id,selectedid=""){
        
        if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/state/')}}",
                data:'country_id='+id+"&selected_state_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    $("#i_state_id").html(obj['optingStr'])
                }
            }); 
        }
  } 

  function selectCity(id,selectedid=""){
        if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/city/')}}",
                data:'state_id='+id+"&selected_city_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    $("#i_city_id").html(obj['optingStr'])
                }
            }); 
        }
  }    

  $(document).ready(function(){
         $("#i_main_skill_id").select2();
        <?php 
            if($view == 'edit' && isset($data->l_other_skills ) && ($data->l_other_skills != '')){
        ?>
        $('#othes_skill').show();
        <?php }else {?>
         $('#othes_skill').hide();
        <?php 
            }
            if($view == 'edit' && isset($data->e_type ) && ($data->e_type == 'seller' || $data->e_type == 'both' )){
        ?>
          $('#seller_info').show();
          $('#seller_education').show();
          $('#seller_employment').show();
          $('#seller_packages').show();
          $('#company_detail').show();
          

        <?php }else{ ?>
          $('#seller_info').hide();
          $('#seller_education').hide();
          $('#seller_employment').hide();
          $('#seller_packages').hide();
          $('#company_detail').hide();
        
        <?php } ?>
        $('#e_type').on('change', function() {
          var valType = this.value;

          if(valType == 'seller' || valType == 'both' ){
             $('#seller_info').show();
             $('#seller_education').show();
             $('#company_detail').show();
             $('#seller_employment').show();
             $('#seller_packages').show();  
          }else{
            $('#seller_info').hide();
            $('#seller_education').hide();
            $('#seller_employment').hide();
            $('#company_detail').hide();
            $('#seller_packages').hide();
          }
         
      })

      $('#d_date').datepicker({
        
      });
  // $('#d_start_date').datepicker();
  // $('#d_start_date').on('change', function() {
  //   var start_date = $('#d_start_date').val();
  //   $('#d_end_date').datepicker({
  //     minDate: new Date(start_date)
  //   }); 
  // })

     $(function() {
      $("#d_start_date").datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 3,
          onClose: function( selectedDate ) {
              $("#d_end_date").datepicker( "option", "minDate", selectedDate );
          }
      });

      $("#d_end_date").datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 3,
          onClose: function( selectedDate ) {
              $("#d_start_date").datepicker("option", "maxDate", selectedDate);
          }
      });
    });
   
   
});

 // $('#i_main_skill_id').on('change click', function(){
 //      if($('#i_main_skill_id').val() == 'Other'){
 //        $('#othes_skill').show();
 //      }else{
 //        $('#othes_skill').hide();
 //      }
 //  });

 $('#i_main_skill_id').on('change', function() {
       var data = $(this).val();
    //  var data = $(".select2-hidden-accessible option:selected").text();
       if(jQuery.inArray( "Other", data ) != -1){
        $('#othes_skill').show();
      }else{
        $('#othes_skill').hide();
      }
    })
</script>

@stop

