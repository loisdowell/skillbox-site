@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/public/Assets/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}

.active-manu {
   background: #2c4762 !important;
   color: #fff;
}
.active-manu a{
  color: #fff;  
}
.list-group-item{
  padding-left: 10px !important;
}


</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Users
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/users')}}">Users</a></li>
        <li class="active">users</li>
      </ol>
    </section>

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif
      </div>  

      <section class="content">
         
          <div class="row">
              <div class="col-md-3">
                  <div class="box box-primary">
                      <div class="box-body box-profile">

                          @if($view=="add")
                          <img class="profile-user-img img-responsive img-circle" src="{{url('public/theme/images/no-image.png')}}" alt="profile picture" style="height: 250px;width: 250px">
                          @else
                              
                              @php
                                  $imgdata="";  
                                  if(isset($data->v_image) && $data->v_image!=""){
                                      $imgdata = \Storage::cloud()->url($data->v_image);     
                                  }else{
                                      $imgdata = \Storage::cloud()->url('users/img-upload.png');   
                                  }
                              @endphp 
                              <img class="profile-user-img img-responsive img-circle" src="{{$imgdata}}" alt="profile picture" style="height: 250px;width: 250px">    

                          @endif 
                          <h3 class="profile-username text-center">{{isset($data->v_fname) ? $data->v_fname:'User Name' }} {{isset($data->v_lname) ? $data->v_lname:'' }}</h3>

                          <p class="text-muted text-center">{{isset($data->v_email) ? $data->v_email:'User Email' }}</p>
                          <p class="text-muted text-center"><b>Email verified :</b> {{isset($data->e_email_confirm) ? $data->e_email_confirm:'no' }}</p>
                          
                          <ul class="list-group list-group-unbordered">
                              
                              <li class="list-group-item">
                                  <b>Buyer Account Status :-</b> {{isset($data->buyer['e_status']) ? $data->buyer['e_status'] : ''}}
                              </li>
                              
                              <li class="list-group-item">
                                  <b>Seller Account Status :-</b> {{isset($data->seller['e_status']) ? $data->seller['e_status'] : ''}}
                              </li>

                          </ul>
                          
                      </div>
                  </div>
                  <div class="box box-primary">
                      
                      <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                            <a href="{{url('admin/users/edit')}}/{{isset($data->id) ? $data->id:'0'}}">
                              <b>User profile</b>
                            </a>
                          </li>
                          <li class="list-group-item" >
                              <a href="{{url('admin/users/buyer-orders')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Buyer Orders</b>
                              </a>
                          </li>
                          <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-orders')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Seller Orders</b>
                              </a>
                          </li>
                           <li class="list-group-item" >
                              <a href="{{url('admin/users/buyer-reviews')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Buyer Reviews</b>
                              </a>
                          </li>
                          <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-reviews')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Seller Reviews</b>
                              </a>
                          </li>
                          
                          <li class="list-group-item" >
                              <a href="{{url('admin/users/edit/password')}}/{{isset($data->id) ? $data->id:'0'}}">
                              <b>Change Password</b>
                              </a>
                          </li>
                          
                          <li class="list-group-item active-manu">
                              <a href="{{url('admin/users/edit/billing')}}/{{isset($data->id) ? $data->id:'0'}}">
                              <b>Edit Billing Detail</b>
                              </a>
                          </li>
                          <li class="list-group-item">
                              <a href="{{url('admin/users/edit/seller')}}/{{isset($data->id) ? $data->id:'0'}}">
                                <b>Edit Seller Account Detail</b>
                              </a>
                          </li>

                      </ul>
                  </div>
              </div>
              <!-- /.col -->
              <div class="col-md-9">
                  <div class="nav-tabs-custom">
                      
                      

                      <a class="btn btn-block btn-social btn-tumblr">
                        <i class="fa fa-info"></i>Update Billing Inforamtion
                      </a>

                      <div class="box-body">
                        
                            
                            <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/users/update-user-billing')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                            {{ csrf_field() }}
                              
                              <div class="row">
                                <div class="col-md-12">
                                     <div class="form-group">
                                        <label>Select Type<span class="asterisk_input">*</span></label>
                                        <select name="billing_detail[v_type]" class="form-control" onchange="changebillingdata(this.value);">
                                          <option value="paypal" @if(isset($data->billing_detail['v_type']) && $data->billing_detail['v_type'] == 'paypal') selected  @endif>PayPal</option>
                                          <option value="bank" @if(isset($data->billing_detail['v_type']) && $data->billing_detail['v_type'] == 'bank') selected  @endif >Bank</option>
                                       </select> 
                                     </div>
                                </div>

                                <div class="col-md-12" id="paypal_detail_div" @if(isset($data->billing_detail['v_type']) && $data->billing_detail['v_type'] == 'bank') style="display: none" @endif>
                                  <div class="form-group">
                                    <label>Paypal Email<span class="asterisk_input">*</span></label>
                                    <input type="email" id="paypalemail" name="billing_detail[paypal_email]" required class="form-control" value="{{isset($data->billing_detail['paypal_email']) ? $data->billing_detail['paypal_email']:''}}">
                                  </div>
                                </div>

                              </div>  

                              <div class="row" id="bank_detail_div" @if(isset($data->billing_detail['v_type']) && $data->billing_detail['v_type'] == 'paypal') style="display: none" @endif>

                                  <div class="col-md-6" >
                                    <div class="form-group">
                                      <label>Enter bank name<span class="asterisk_input">*</span></label>
                                      <input type="text" class="form-control" name="billing_detail[v_bankname]" id="v_bankname" value="{{isset($data->billing_detail['v_bankname']) ? $data->billing_detail['v_bankname']:''}}">
                                    </div>

                                    <div class="form-group">
                                      <label>Enter Account No:<span class="asterisk_input">*</span></label>
                                      <input type="text" class="form-control" name="billing_detail[v_accountno]" id="v_accountno" value="{{isset($data->billing_detail['v_accountno']) ? $data->billing_detail['v_accountno']:''}}">
                                    </div>
                                  </div>
                                    
                                  <div class="col-md-6" >  
                                    <div class="form-group">
                                      <label>Enter IFSC Code:<span class="asterisk_input">*</span></label>
                                      <input type="text" class="form-control input-field" name="billing_detail[v_ifsc]" id="v_ifsc" value="{{isset($data->billing_detail['v_ifsc']) ? $data->billing_detail['v_ifsc']:''}}">
                                    </div>

                                    <div class="form-group">
                                      <label>Enter Account Holder name::<span class="asterisk_input">*</span></label>
                                      <input type="text" class="form-control input-field" name="billing_detail[v_account_holder_name]" id="v_account_holder_name" value="{{isset($data->billing_detail['v_account_holder_name']) ? $data->billing_detail['v_account_holder_name']:''}}">
                                    </div>
                                  </div>

                              </div>

                              <div class="col-md-12">
                                  <a href="{{url('admin/users')}}">
                                  <button type="button" class="btn btn-warning">Back to List</button>
                                  </a>
                                  <button type="submit" class="btn btn-primary">Save</button>
                              </div>
                              
                            </form>
                        
                  </div>
              </div>

          </div>
      </section>

  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{url('/public/Assets/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
  
  function changebillingdata(data){

        if(data=="bank"){
            $("#paypal_detail_div").css("display","none");    
            $("#bank_detail_div").css("display","block");  
            $("#paypalemail").removeAttr("required");

            $("#v_bankname").attr("required","true");
            $("#v_accountno").attr("required","true");
            $("#v_ifsc").attr("required","true");
            $("#v_account_holder_name").attr("required","true");
      
        }else{
            $("#paypal_detail_div").css("display","block");   
            $("#bank_detail_div").css("display","none");     

            $("#v_bankname").removeAttr("required");
            $("#v_accountno").removeAttr("required");
            $("#v_ifsc").removeAttr("required");
            $("#v_account_holder_name").removeAttr("required");
            $("#paypalemail").attr("required","true");


        }
  }
  

  
  
  
  
</script>

@stop

