@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/public/Assets/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Jobs
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/users')}}">Users</a></li>
        <li class="active">jobs</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} new Job</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            
            <div class="box-body">
              <br/><br/>
                <div class="row">
                   <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/jobs/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    
                    <div class="col-md-6">

                      <div class="form-group">
                        <label>User</label>
                        <select class="form-control" id="i_user_id" name="i_user_id">
                          <option value="">- select -</option>
                          @if( isset($users) && count($users) )
                            @foreach($users as $key => $val)

                              <option value="{{$val->_id}}" @if( isset($data->i_user_id) && $data->i_user_id == $val->_id) selected @endif >{{$val->v_name}}</option>
                            @endforeach
                          @endif
                        </select>
                      </div>
                      <div class="form-group">
                          <label>Title<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_title" id="v_title" class="form-control" placeholder="Title" value="{{$data->v_title or old('v_title')}}" required="" data-parsley-trigger="keyup">
                      </div>

                      <div class="form-group">
                          <label>What Budget do you have in mind?<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="v_budget_type" name="v_budget_type" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="active" @if( isset($data->v_budget_type) && $data->v_budget_type == 'active' ) selected @endif>Active</option>
                            <option value="inactive" @if( isset($data->v_budget_type ) && $data->v_budget_type == 'inactive') SELECTED @endif >Inactive</option>
                          </select>
                      </div>

                      <div class="form-group">
                          <label>Budget</label>
                          <input type="text" name="i_budget" id="i_budget" class="form-control" placeholder="Budget" value="{{$data->i_budget or old('i_budget')}}">
                      </div>

                      <div class="form-group">
                          <label>Phone</label>
                          <input type="text" name="v_phone" id="v_phone" class="form-control" placeholder="Phone" value="{{$data->v_phone or old('v_phone')}}">
                      </div>

                      <div class="form-group">
                          <label>Avaibility</label>
                          <input type="text" name="v_avaibility" id="v_avaibility" class="form-control" placeholder="Avaibility" value="{{$data->v_avaibility or old('v_avaibility')}}">
                      </div>

                      <div class="form-group">
                        <label>City</label>
                        <select class="form-control city-multiple" id="i_preferred_city_ids" name="i_preferred_city_ids[]" multiple="multiple">
                          <option value="">- select -</option>
                          <?php $cityID=array(); ?>
                          @if( isset($city) && count($city) )
                            @foreach($city as $key => $val)
                            <?php  
                              if($view == 'edit'){
                                $cityID = explode(',',$data->i_preferred_city_ids);
                              }
                            ?>
                              <option value="{{$val->_id}}" @if( in_array($val->_id,$cityID)) selected @endif > {{$val->v_title}} </option>
                            @endforeach
                          @endif
                        </select>
                      </div>
                      <div class="form-group">
                          <label>Sponsored</label>
                          <select class="form-control" id="i_sponsored" name="i_sponsored">
                            <option value="">- select -</option>
                            <option value="1" @if( isset($data->i_sponsored) && $data->i_sponsored == '1' ) selected @endif>Yes</option>
                            <option value="0" @if( isset($data->i_sponsored ) && $data->i_sponsored == '0') SELECTED @endif >No</option>
                          </select>
                      </div>
                      
                      
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Status<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="active" @if( isset($data->e_status) && $data->e_status == 'active' ) selected @endif>Active</option>
                            <option value="inactive" @if( isset($data->e_status ) && $data->e_status == 'inactive') SELECTED @endif >Inactive</option>
                          </select>
                      </div>

                      <div class="form-group">
                          <label>Description<span class="asterisk_input">*</span></label>
                          <textarea name="l_description" id="l_description" class="form-control" required="" rows="5" data-parsley-id="9363">{{$data->l_description or old('l_description')}}</textarea>
                          <ul class="parsley-errors-list" id="parsley-id-9363"></ul>
                      </div>

                      <div class="form-group">
                          <label>Start Date</label>
                          <input type="text" name="d_start_date" id="d_start_date" class="form-control" placeholder="Start Date" value="{{ $data->d_start_date or old('d_start_date')}}">
                      </div>

                      <div class="form-group">
                          <label>Time Frame</label>
                          <input type="text" name="v_timeframe" id="v_timeframe" class="form-control" placeholder="Time Frame" value="{{$data->v_timeframe or old('v_timeframe')}}">
                      </div>

                      <div class="form-group">
                          <label>Callouts</label>
                          <input type="text" name="v_callouts" id="v_callouts" class="form-control" placeholder="Callouts" value="{{$data->v_callouts or old('v_callouts')}}">
                      </div>

                    </div>
                  </div>
                  <div class="clear:both"></div>                
                        
                        <div class="row">
                          <div class="col-md-12">
                              <a href="{{url('admin/jobs')}}">
                              <button type="button" class="btn btn-warning">Back to List</button>
                              </a>
                              <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                      </div> 
                    </div>
                    </form>
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>
          </div>
        </div>
      </section>      
              
    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <div class="box-header pull-right">
              <a href="{{url('admin/jobs/add/0')}}">
                 <button type="submit" class="btn btn-info">Add New Job</button>
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User</th>
                  <th>Title</th>
                  <th>Budget</th>
                  <th>Phone</th>
                  <th>Avaibility</th>
                  <th>Start Date</th>
                  <th>Time Frame</th>
                  <th>Callouts</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      <td>{{ count( $value->hasUser() ) ? $value->hasUser()->v_name : ''}}</td>
                      <td>{{$value->v_title or ''}}</td>
                      <td>{{$value->v_budget or ''}}</td>
                      <td>{{$value->v_phone or ''}}</td>
                      <td>{{$value->v_avaibility or ''}}</td>
                      <td>{{$value->d_start_date or ''}}</td>
                      <td>{{$value->v_timeframe or ''}}</td>
                      <td> {{$value->v_callouts or ''}} </td>
                      
                      <td>
                        <a href="{{url('admin/jobs/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/jobs',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if( $value->e_status == 'inactive' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                        @php $a=url('admin/jobs/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" onclick="confirmDelete('{{ $value->id }}','<?php echo $a ?>')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{url('/public/Assets/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });

  $(document).ready(function(){
    $('#d_start_date').datepicker();
    $('#v_timeframe').datepicker();
    $(".city-multiple").select2();
  });  

</script>

@stop

