@extends('layouts.master')


@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
{{-- <link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" /> --}}
<link href="{{url('/public/Assets/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}

.active-manu {
   background:  #2c4762 !important;
   color: #fff;
}
.active-manu a{
  color: #fff;  
}
.list-group-item{
  padding-left: 10px !important;
}


</style>

<style type="text/css">
    .imgrem {position: relative;}
    .close-img{height: 15px; position: absolute;right: 15px;top: -30px;}
    .select2-selection__choice, #add_text button {background-color: #3c8dbc !important;}
</style>
@stop

@section('content')

@php
  $path = Route::getFacadeRoot()->current()->uri();
  
@endphp


  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Users
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/users')}}">Users</a></li>
        <li class="active">users</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif
      </div>  


      <section class="content">
          <div class="row">
              <div class="col-md-3">
                  <div class="box box-primary">
                      <div class="box-body box-profile">

                          @if($view=="add")
                          <img class="profile-user-img img-responsive img-circle" src="{{url('public/theme/images/no-image.png')}}" alt="profile picture" style="height: 250px;width: 250px">
                          @else
                              @php
                              $imgdata = '';
                              if(Storage::disk('s3')->exists($userdata->v_image)){
                                  $imgdata = \Storage::cloud()->url($userdata->v_image);
                              }else{
                                  $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                              }
                              @endphp 
                              <img class="profile-user-img img-responsive img-circle" src="{{$imgdata}}" alt="profile picture" style="height: 250px;width: 250px">    
                          @endif 

                          <h3 class="profile-username text-center">{{isset($userdata->v_fname) ? $userdata->v_fname:'User Name' }} {{isset($userdata->v_lname) ? $userdata->v_lname:'' }}</h3>

                          <p class="text-muted text-center">{{isset($userdata->v_email) ? $userdata->v_email:'User Email' }}</p>
                          <p class="text-muted text-center"><b>Email verified :</b> {{isset($userdata->e_email_confirm) ? $userdata->e_email_confirm:'no' }}</p>
                          
                          <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                  <b>Buyer Account Status</b>
                                  <ul class="list-group list-group-unbordered">
                                    <b>In Person :-</b> {{isset($userdata->v_buyer_inperson_service) ? $userdata->v_buyer_inperson_service : ''}}</br>
                                    <b>Online :-</b> {{isset($userdata->v_buyer_online_service) ? $userdata->v_buyer_online_service : ''}}
                                  </ul>
                              </li>
                              <li class="list-group-item">
                                  <b>Seller Account Status</b>
                                  <ul class="list-group list-group-unbordered">
                                    <b>In Person :-</b> {{isset($userdata->v_seller_inperson_service) ? $userdata->v_seller_inperson_service : ''}}</br>
                                    <b>Online :-</b> {{isset($userdata->v_seller_online_service) ? $userdata->v_seller_online_service : ''}}
                                  </ul>
                              </li>
                          </ul>
                          
                      </div>
                  </div>
                  <div class="box box-primary">
                      
                      <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                              <a href="{{url('admin/users/edit')}}/{{isset($userdata->id) ? $userdata->id:'0'}}">
                                <b>Edit Main profile</b>
                              </a>    
                          </li>
                          @if ($view!="add")

                           <li class="list-group-item @if($path=="admin/users/edit/sellerinperson/{_id}") active-manu @endif">
                              <a href="{{url('admin/users/edit/sellerinperson')}}/{{isset($userdata->id) ? $userdata->id:'0'}}">
                                <b>Seller In Person Profile</b>
                              </a>
                          </li>

                          <li class="list-group-item @if($path=="admin/users/edit/selleronline/{_id}") active-manu @endif">
                              <a href="{{url('admin/users/edit/selleronline')}}/{{isset($userdata->id) ? $userdata->id:'0'}}">
                                <b>Seller Online Profile</b>
                              </a>
                          </li>

                          <li class="list-group-item" >
                              <a href="{{url('admin/users/edit/password')}}/{{isset($userdata->id) ? $userdata->id:'0'}}">
                                <b>Change Password</b>
                              </a>
                          </li>


                          <li class="list-group-item" >
                              <a href="{{url('admin/users/buyer-orders')}}/{{isset($userdata->id) ? $userdata->id:'0'}}" target="_blank">
                                <b>Buyer Orders</b>
                              </a>
                          </li>
                          <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-orders')}}/{{isset($userdata->id) ? $userdata->id:'0'}}" target="_blank">
                                <b>Seller Orders</b>
                              </a>
                          </li>
                          <li class="list-group-item" >
                              <a href="{{url('admin/users/buyer-reviews')}}/{{isset($userdata->id) ? $userdata->id:'0'}}" target="_blank">
                                <b>Buyer Reviews</b>
                              </a>
                          </li>

                           <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-reviews')}}/{{isset($userdata->id) ? $userdata->id:'0'}}" target="_blank">
                                <b>Seller Skill Reviews</b>
                              </a>
                          </li>

                          <li class="list-group-item" >
                              <a href="{{url('admin/users/seller-reviews/course')}}/{{isset($userdata->id) ? $userdata->id:'0'}}" target="_blank">
                                <b>Seller Course Reviews</b>
                              </a>
                          </li>
                          

                          @endif
                          
                      </ul>
                  </div>
              </div>


              <!-- /.col -->
              <div class="col-md-9">
                  <div class="nav-tabs-custom">
                     
                      <a class="btn btn-block btn-social btn-tumblr">
                        <i class="fa fa-info"></i>Edit seller Details
                      </a>

                      <div class="box-body">
                        <div class="row">
                            @if($path=="admin/users/edit/sellerinperson/{_id}")
                            <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/users/update-user-sellerinperson')}}/{{isset($userdata->_id) ? $userdata->_id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                            @else
                            <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/users/update-user-selleronline')}}/{{isset($userdata->_id) ? $userdata->_id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                            @endif

                            {{ csrf_field() }}
                               <div class="col-md-6">
                                <div class="form-group">
                                  <label>Category<span class="asterisk_input">*</span></label>
                                  <select class="form-control" id="i_category_id" name="i_category_id" onchange="getSkill(this.value)" required >
                                    <option value="">-Select-</option>
                                      @if(isset($categories) && count($categories))
                                          @foreach($categories as $k=>$v)
                                              <option value="{{$v->id}}" @if(isset($data->i_category_id) && $data->i_category_id==$v->id) selected @endif >{{$v->v_name or ''}}</option>
                                          @endforeach
                                     @endif  
                                  </select>
                                </div>

                                <div class="form-group">
                                    <label>Other Skill<span class="asterisk_input">*</span></label>
                                      <select class="form-control" id="i_otherskill_id" name="i_otherskill_id[]" multiple="multiple" required>
                                         <option value="">-Select-</option>
                                         
                                         @if(isset($skilldata) && count($skilldata))
                                            @foreach($skilldata as $k=>$v)
                                            <option value = '{{$v->id}}' @if(in_array($v->id, $data->i_otherskill_id)) selected @endif>{{$v->v_name}}</option>
                                            @endforeach
                                         @endif
                                       </select>
                                </div>
                                <div class="form-group">
                                  <label>Profile title<span class="asterisk_input">*</span></label>
                                  <input type="text" name="v_profile_title" class="form-control" placeholder="Title" value="{{$data->v_profile_title or ''}}" required>
                                 </div>

                                <div class="form-group">
                                  <label>Website<span class="asterisk_input">*</span></label>
                                  <input type="text" name="v_website" class="form-control" placeholder="Website" data-parsley-type="url" value="{{$data->v_website or ''}}" required>
                                </div>
                               
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
                                    <label>Skill<span class="asterisk_input">*</span></label>
                                    <select class="form-control" id="i_mainskill_id" name="i_mainskill_id" required>
                                      <option value="">-Select-</option>
                                        @if(isset($skilldata) && count($skilldata))
                                            @foreach($skilldata as $k=>$v)
                                              <option value = '{{$v->id}}' @if(isset($data->i_mainskill_id) && $data->i_mainskill_id==$v->id) selected @endif>{{$v->v_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Experience Level<span class="asterisk_input">*</span></label>
                                    <select class="form-control" id="v_experience_level" name="v_experience_level" required>
                                      <option value="">-Select-</option>
                                      <option value="entry" @if(isset($data->v_experience_level) && $data->v_experience_level == 'entry' ) selected @endif>Entry</option>
                                      <option value="intermediate" @if(isset($data->v_experience_level) && $data->v_experience_level == 'intermediate' ) selected @endif>Intermediate</option>
                                      <option value="expert" @if(isset($data->v_experience_level) && $data->v_experience_level == 'expert' ) selected @endif>Expert</option>
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                     <label class="field-name">
                                        Hourly Rate
                                    </label>
                                    <input type="number" step="any" class="form-control input-field" name="v_hourly_rate" data-parsley-type="number" required value="@if(isset($data->v_hourly_rate)){{$data->v_hourly_rate}}@endif">
                                </div>
                                

                                  <div class="form-group">
                                      <label>Contact Phone:</label>
                                      <input type="text" name="v_contact_phone" id="v_contact_phone" class="form-control" placeholder="Phone" value="{{$data->v_contact_phone or old('v_contact_phone')}}" data-parsley-type="digits" minlength="6" maxlength="10" data-parsley-trigger="keyup">
                                  </div>
                              </div>

                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label>Short Description<span class="asterisk_input">*</span></label>
                                    <textarea name="l_short_description" class="form-control" rows="3" required>{{$data->l_short_description or ''}}</textarea>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label>Brief Description of the work you do<span class="asterisk_input">*</span></label>
                                    <textarea name="l_brief_description" rows="15" class="form-control textarea" required>{{$data->l_brief_description or ''}}</textarea>
                                  </div>
                              </div>


                              <div class="col-md-6">
                                <div class="form-group">
                                   <label>City<span class="asterisk_input">*</span></label>
                                      <input type="text" name="v_city" id="autocomplete" class="form-control" placeholder="City" value="{{$data->v_city or ''}}" required>
                                </div>
                              </div>
                               <input type="hidden" name="v_latitude" id="v_latitude" value="@if(isset($data->v_latitude)){{$data->v_latitude}}@endif">
                              <input type="hidden" name="v_longitude" id="v_longitude" value="@if(isset($data->v_longitude)){{$data->v_longitude}}@endif">

                              <div class="col-md-6">
                                <div class="form-group">
                                   <label>Postcode<span class="asterisk_input">*</span></label>
                                     <input type="text" name="v_pincode" id="v_pincode" class="form-control" placeholder="Pincode" value="{{$data->v_pincode or ''}}" required>
                                </div>
                              </div>
                              



                              <div class="col-md-12">
                                <div class="form-group">
                                    <label>English Proficiency<span class="asterisk_input">*</span></label>
                                    <select class="form-control" id="v_english_proficiency" name="v_english_proficiency" required>
                                      <option value="">-Select-</option>
                                      <option value="limited" @if(isset($data->v_english_proficiency) && $data->v_english_proficiency == 'limited' ) selected @endif>Limited</option>
                                      <option value="professional" @if(isset($data->v_english_proficiency) && $data->v_english_proficiency == 'professional' ) selected @endif>Professional</option>
                                      <option value="native" @if(isset($data->v_english_proficiency) && $data->v_english_proficiency == 'native' ) selected @endif>Native</option>
                                      <option value="billingual" @if(isset($data->v_english_proficiency) && $data->v_english_proficiency == 'billingual' ) selected @endif>Billingual</option>
                                    </select>

                                </div>
                              </div>
                              <div class="col-md-6">
   
                                <div class="form-group">
                                    <div class="open-browser">
                                                
                                      <label class="field-name">
                                          Add your work video (up to 50MB)
                                      </label>
                                      <input type="text" class="form-control input-field" id="work_video" disabled>
                                      <div class="text-right">
                                        <input type='file' id="workvideoid" name="work_video[]" onchange="workvideo()" multiple accept="video/mp4,video/x-m4v,video/*"/>
                                        <label id="fileupload" class="btn-editdetail" for="workvideoid">Browser</label>
                                      </div>
                                    </div>
                                  </div>

                                  @if(isset($data->v_work_video) && count($data->v_work_video))
                                      @foreach($data->v_work_video as $k=>$v)
                                        @php
                                            $videodata = \Storage::cloud()->url($v);     
                                        @endphp
                                         <span class="imgrem" id="workvideorem{{$k}}">
                                          <video width="150" height="150" controls>
                                            <source src="{{$videodata}}" type="video/mp4">
                                          </video> 
                                          <img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" onclick="removeworkvideo('{{$data->id}}','{{$v}}','workvideorem{{$k}}')" style="height: 15px;position: absolute;right: 4px;top: -113px;">
                                         </span>
                                      @endforeach   
                                  @endif


                                  <div class="form-group">
                                    <div class="open-browser">
                                        <label class="field-name">
                                            Add your introducy video (up to 50MB)
                                        </label>
                                        <input type="text" class="form-control input-field" id="introducy_video" disabled>
                                        <div class="text-right">
                                            <input type='file' id="introducyvideoid" accept="video/mp4,video/x-m4v,video/*"  onchange="introducyvideo();" name="introducy_video" style="display: none" />
                                            <label id="fileupload" class="btn-editdetail" for="introducyvideoid">Browser</label>
                                        </div>
                                    </div>
                                  </div>  

                                  @if(isset($data->v_introducy_video) && $data->v_introducy_video != '')
                                    @php
                                        $videodata = \Storage::cloud()->url($data->v_introducy_video);     
                                    @endphp

                                    <span class="imgrem" id="videorem">
                                        <video width="150" height="150" controls>
                                            <source src="{{$videodata}}" type="video/mp4">
                                        </video> 
                                        <img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" onclick="removevideojobpost('{{$data->id}}','{{$data->v_introducy_video}}','videorem')" style="height: 15px;position: absolute;right: 4px;top: -113px;">
                                   </span>
                                  @endif
                                

                                <script type="text/javascript">
                                      function workvideo(){
                                          var filename = $('#workvideoid').val().split('\\').pop();
                                          var lastIndex = filename.lastIndexOf("\\");   
                                          $('#work_video').val(filename);
                                      }
                                </script>
                              </div>  
                              
                              <div class="col-md-6">
                                <div class="form-group">
                                  <div class="open-browser">
                                      <label class="field-name">
                                          Add Your work photos (up to 10)
                                      </label>
                                      <input type="text" class="form-control input-field" id="workphotoid" disabled>
                                      <div class="text-right">
                                          <input type='file' id="fileupload-example-4" name="work_photo[]" style="display: none" onchange="workphoto();" multiple />
                                          <label id="fileupload" class="btn-editdetail" for="fileupload-example-4">Browser</label>
                                      </div>
                                  </div>
                                </div>

                                @if(isset($data->v_work_photos) && count($data->v_work_photos))

                                    @foreach($data->v_work_photos as $k=>$v)
                                        @php
                                            $imgdata = \Storage::cloud()->url($v);     
                                        @endphp
                                        <span class="imgrem" id="imgrem{{$k}}">
                                        <img src="{{$imgdata}}" style="height: 100px;width: 100px">
                                        <img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" onclick="removeimgjobpost('{{$data->id}}','{{$v}}','imgrem{{$k}}')" class="close-img">
                                       </span>                                            
                                    @endforeach
                                @endif

                                
                              </div> 
                              <script type="text/javascript">
                                      
                                function introducyvideo(){
                                    var filename = $('#introducyvideoid').val().split('\\').pop();
                                    var lastIndex = filename.lastIndexOf("\\");   
                                    $('#introducy_video').val(filename);
                                }

                                function workphoto(){

                                }
                              
                              </script>
                              <style type="text/css">
                                #workvideoid {
                                    height: 0;
                                    width: 0;
                                }
                              </style>



                              <div class="col-md-12">
                                   <h3 class="field-name">Add your Packages (max 3 packages)</h3>
                              </div>

                              <div class="col-md-6">
                                <h4 class="field-name">Basic Package</h4>
                                
                                <div class="form-group">
                                  <label>Package 1 title</label><br>
                                  <input type="text" class="form-control input-field" name="information[basic_package][v_title]" required value="@if(isset($data->information['basic_package']['v_title'])){{$data->information['basic_package']['v_title']}}@endif">
                                </div>

                                <div class="form-group">
                                  <div class="package-title" id="basicpackage_bullets">
                                    <label class="subfield-name">
                                        Package bullet
                                    </label>
                                    
                                    @if(isset($data->information['basic_package']['v_bullets']) && count($data->information['basic_package']['v_bullets']))
                                      @foreach($data->information['basic_package']['v_bullets'] as $k=>$v)
                                      <input type="text" class="form-control input-field" name="information[basic_package][v_bullets][]" value="{{$v}}">
                                      @endforeach
                                    @else
                                      <input type="text" class="form-control input-field" name="information[basic_package][v_bullets][]">
                                    @endif
                                    </div>
                                    <div class="text-right">
                                        <button name="button" type="button" class="btn btn-bullet" onclick="addBullets('information[basic_package][v_bullets][]','basicpackage_bullets')">Add Bullets
                                        </button>
                                    </div>
                                  
                                </div>

                                <?php   
                                     $delivery_type=array(
                                           'hours'=>'Hour(s)',
                                            'day'=>"Day(s)",
                                            'month'=>"Month(s)",
                                            'year'=>"Year(s)",
                                      ); 
                                  ?> 

                                  <div class="form-group">
                                    <div class="time-buttle">
                                          <label class="subfield-name">
                                              Delivery Time
                                          </label>
                                          <div class="time-concept">
                                              <div class="row">
                                                  <div class="col-sm-7">
                                                  <div class="dilivery-hours">
                                                      <input type="number" step="any" class="form-control input-field" name="information[basic_package][v_delivery_time]" required  value="@if(isset($data->information['basic_package']['v_delivery_time'])){{$data->information['basic_package']['v_delivery_time']}}@endif">
                                                      </div>
                                                  </div>

                                                  <div class="col-sm-5">
                                                  <div class="dilivery-time">
                                                      <select class="form-control form-sell" name="information[basic_package][v_delivery_type]" required>
                                                          @foreach($delivery_type as $k=>$v)
                                                              <option value="{{$k}}" @if(isset($data->information['basic_package']['v_delivery_type']) && $data->information['basic_package']['v_delivery_type']==$k) selected @endif >{{$v}}</option>
                                                          @endforeach
                                                      </select>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="buttet-option">
                                              <label class="buttet-heading">
                                                  Package price
                                              </label>
                                              <input type="number" step="any" class="form-control input-field" name="information[basic_package][v_price]" data-parsley-type="number" required value="@if(isset($data->information['basic_package']['v_price'])){{$data->information['basic_package']['v_price']}}@endif">
                                          </div>
                                    </div>
                                  </div> 

                                  <div class="form-group">

                                    <div class="more-package">
                                          <label class="buttet-money">
                                              Add-ons (Add extras to make more money e.g. £5.99 )
                                          </label>
                                          
                                          <div class="package-added" >
                                            <div id="basicpackageaddon">
                                              @if(isset($data->information['basic_package']['add_on']['title']) && count($data->information['basic_package']['add_on']['title']))
                                              @foreach($data->information['basic_package']['add_on']['title'] as $k=>$v)
                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on title (max 2 add-ons)
                                                  </label>
                                                  <input type="text" class="form-control input-field" name="information[basic_package][add_on][title][]"  value="{{$v}}">
                                              </div>

                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on price
                                                  </label>
                                                  <input type="number" step="any" class="form-control input-field" name="information[basic_package][add_on][v_price][]"  value="@if(isset($data->information['basic_package']['add_on']['v_price'][$k])){{$data->information['basic_package']['add_on']['v_price'][$k]}} @endif">
                                              </div>
                                              @endforeach
                                              @else
                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on title (max 2 add-ons)
                                                  </label>
                                                  <input type="text" class="form-control input-field" name="information[basic_package][add_on][title][]" >
                                              </div>
                                              
                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on price
                                                  </label>
                                                  <input type="number" class="form-control input-field" name="information[basic_package][add_on][v_price][]" >
                                              </div>

                                              @endif

                                            </div>
                                              
                                              <div class="text-right basicpackageaddon">
                                                  <button name="button" type="button" class="btn btn-editdetail" onclick="addAddon('information[basic_package]','basicpackageaddon')"> Add more </button>
                                              </div>

                                          </div>
                                      </div>
                                  </div> 
                              </div>
                              
                              <div class="col-md-6">
                                <h4 class="field-name">Standard Package</h4>

                                <div class="form-group">

                                  <label>Package 2 title</label><br>
                                  <input type="text" class="form-control input-field" name="information[standard_package][v_title]" value="@if(isset($data->information['standard_package']['v_title'])){{$data->information['standard_package']['v_title']}}@endif">
                                
                                </div>
                                
                                <div class="form-group">

                                  <div class="package-title" id="standardpackage_bullets">
                                      <label class="subfield-name">
                                          Package bullet
                                      </label>
                                      @if(isset($data->information['standard_package']['v_bullets']) && count($data->information['standard_package']['v_bullets']))
                                        @foreach($data->information['standard_package']['v_bullets'] as $k=>$v)
                                        <input type="text" class="form-control input-field" name="information[standard_package][v_bullets][]" value="{{$v}}">
                                        @endforeach
                                      @else
                                        <input type="text" class="form-control input-field" name="information[standard_package][v_bullets][]">
                                      @endif
                                  </div>
                                  <div class="text-right">
                                      <button name="button" type="button" class="btn btn-bullet" onclick="addBullets('information[standard_package][v_bullets][]','standardpackage_bullets')" > Add Bullets
                                      </button>
                                  </div>

                                </div>

                                <div class="form-group">

                                     <div class="time-buttle">
                                        <label class="subfield-name">
                                            Delivery Time
                                        </label>
                                    </div>
                                    <div class="time-concept">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <div class="dilivery-hours">
                                                    <input type="number" class="form-control input-field" name="information[standard_package][v_delivery_time]" value="@if(isset($data->information['standard_package']['v_delivery_time'])){{$data->information['standard_package']['v_delivery_time']}}@endif" >
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="dilivery-time">
                                                    <select class="form-control form-sell" name="information[standard_package][v_delivery_hours]">
                                                        @foreach($delivery_type as $k=>$v)
                                                            <option value="{{$k}}" @if(isset($data->information['standard_package']['v_delivery_type']) && $data->information['standard_package']['v_delivery_type']==$k) selected @endif >{{$v}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="buttet-option">
                                            <label class="buttet-heading">
                                                Package price
                                            </label>
                                            {{-- <select class="form-control form-sell" name="information[standard_package][v_price]">
                                                <option value="5.99"> £5.99 </option>
                                                <option value="6.99"> £6.99 </option>
                                            </select> --}}
                                            <input type="number" step="any" class="form-control input-field" name="information[standard_package][v_price]" data-parsley-type="number" value="@if(isset($data->information['standard_package']['v_price'])){{$data->information['standard_package']['v_price']}}@endif">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="more-package">
                                      <label class="buttet-money">
                                          Add-ons (Add extras to make more money e.g. £5.99 )
                                      </label>
                                      <div class="package-added">
                                        
                                        <div id="standardpackageaddon">
                                          
                                          @if(isset($data->information['standard_package']['add_on']['title']) && count($data->information['standard_package']['add_on']['title']))
                                          @foreach($data->information['standard_package']['add_on']['title'] as $k=>$v)
                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on title (max 2 add-ons)
                                                  </label>
                                                  <input type="text" class="form-control input-field" name="information[standard_package][add_on][title][]" value="{{$v}}">
                                              </div>
                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on price
                                                  </label>
                                                  <input type="number" step="any" class="form-control input-field" name="information[standard_package][add_on][v_price][]"  value="@if(isset($data->information['standard_package']['add_on']['v_price'][$k])){{$data->information['standard_package']['add_on']['v_price'][$k]}} @endif">
                                              </div>

                                          @endforeach
                                          
                                          @else
                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on title (max 2 add-ons)
                                                  </label>
                                                  <input type="text" class="form-control input-field" name="information[standard_package][add_on][title][]">
                                              </div>
                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on price
                                                  </label>
                                                  <input type="number" step="any" class="form-control input-field" name="information[standard_package][add_on][v_price][]">
                                              </div>
                                          
                                          @endif
                                          



                                        </div>  
                                        <div class="text-right standardpackageaddon">
                                              <button name="button" type="button" class="btn btn-editdetail" onclick="addAddon('information[standard_package]','standardpackageaddon')"> Add more </button>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col-md-6">
                                <h4 class="field-name">Premium Package</h4>

                                <div class="form-group">

                                    <label>Package 3 title</label><br>
                                    <input type="text" class="form-control input-field" name="information[premium_package][v_title]" value="@if(isset($data->information['premium_package']['v_title'])){{$data->information['premium_package']['v_title']}}@endif">

                                </div>
                                
                                <div class="form-group">

                                  <div class="package-title" id="premium_bullets">
                                      <label class="subfield-name">
                                          Package bullet
                                      </label>
                                      @if(isset($data->information['premium_package']['v_bullets']) && count($data->information['premium_package']['v_bullets']))
                                          @foreach($data->information['premium_package']['v_bullets'] as $k=>$v)
                                              <input type="text" class="form-control input-field" name="information[premium_package][v_bullets][]" value="{{$v}}">
                                          @endforeach
                                      @else    
                                      <input type="text" class="form-control input-field" name="information[premium_package][v_bullets][]">
                                      @endif
                                  </div>

                                  <div class="text-right">
                                      <button name="button"  type="button" class="btn btn-bullet" onclick="addBullets('information[premium_package][v_bullets][]','premium_bullets')"> Add Bullets
                                      </button>
                                  </div>
                                </div>

                                <div class="form-group">


                                    <div class="time-buttle">
                                        <label class="subfield-name">
                                            Delivery Time
                                        </label>
                                    </div>
                                    <div class="time-concept">
                                        <div class="row">
                                            
                                            <div class="col-sm-7">
                                            <div class="dilivery-hours">
                                                <input type="number" step="any" class="form-control input-field" name="information[premium_package][v_delivery_time]" value="@if(isset($data->information['premium_package']['v_delivery_time'])){{$data->information['premium_package']['v_delivery_time']}}@endif">
                                            </div>
                                            </div>

                                            <div class="col-sm-5">
                                            <div class="dilivery-time">
                                                <select class="form-control form-sell" name="information[premium_package][v_delivery_hours]">
                                                    @foreach($delivery_type as $k=>$v)
                                                        <option value="{{$k}}" @if(isset($data->information['premium_package']['v_delivery_type']) && $data->information['premium_package']['v_delivery_type']==$k) selected @endif >{{$v}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="buttet-option">
                                            
                                            <label class="buttet-heading">
                                                Package price
                                            </label>
                                            
                                            <input type="number" step="any" class="form-control input-field" name="information[premium_package][v_price]" data-parsley-type="number" value="@if(isset($data->information['premium_package']['v_price'])){{$data->information['premium_package']['v_price']}}@endif">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">

                                    <div class="more-package">
                                      <label class="buttet-money">
                                          Add-ons (Add extras to make more money e.g. £5.99 )
                                      </label>
                                      <div class="package-added">
                                        
                                        <div id="premiumpackageaddon">

                                          @if(isset($data->information['premium_package']['add_on']['title']) && count($data->information['premium_package']['add_on']['title']))
                                              @foreach($data->information['premium_package']['add_on']['title'] as $k=>$v)

                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on title (max 2 add-ons)
                                                  </label>
                                                  <input type="text" class="form-control input-field" name="information[premium_package][add_on][title][]" value="{{$v}}">
                                              </div>
                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on price
                                                  </label>
                                                  <input type="number" step="any" class="form-control input-field" name="information[premium_package][add_on][v_price][]" value="@if(isset($data->information['premium_package']['add_on']['v_price'][$k])){{$data->information['premium_package']['add_on']['v_price'][$k]}} @endif">
                                              </div>
                                              @endforeach
                                          @else

                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on title (max 2 add-ons)
                                                  </label>
                                                  <input type="text" class="form-control input-field" name="information[premium_package][add_on][title][]">
                                              </div>
                                              <div class="package-title">
                                                  <label class="buttet-heading">
                                                      Add-on price
                                                  </label>
                                                  <input type="number" step="any" class="form-control input-field" name="information[premium_package][add_on][v_price][]">
                                              </div>
                                         
                                          @endif  
                                          </div>
                                          
                                          <div class="text-right premiumpackageaddon">
                                              <button name="button" type="button" class="btn btn-editdetail" onclick="addAddon('information[premium_package]','premiumpackageaddon')" > Add more </button>
                                          </div>
                                      </div>
                                  </div>
                                </div>  
                              </div>
                              
                              <div class="col-md-6">
                              </div> 



                              <div style="clear: both"></div> 

                              <div class="col-md-12">
                                <h3>Visit me</h3>
                                 Please add your social handle only without @ or #. Social media platform will be added automatically.
                              </div>
                              <div class="banking">
                              
                                <div class="col-md-6">
                               
                                  <div class="form-group">
                                    <label class="subfield-name">
                                        Facebook
                                    </label>
                                    <input type="text" class="form-control input-field" name="v_facebook" value="{{$data->v_facebook or ''}}">
                                  </div>
                               
                                  <div class="form-group">
                                    <label class="subfield-name">
                                        Twitter
                                    </label>
                                    <input type="text" class="form-control input-field" name="v_twitter"  value="{{$data->v_twitter or ''}}">
                                  </div>
                               
                                </div>
                              
                                <div class="col-md-6">
                                   <div class="form-group">

                                    <label class="subfield-name">
                                        Pinterest
                                    </label>
                                    <input type="text" class="form-control input-field" name="v_pinterest" value="{{$data->v_pinterest or ''}}">
                                    </div>

                                    <div class="form-group">
                                      <div class="name-details">
                                        <label class="subfield-name">
                                            Instagram
                                        </label>
                                        <input type="text" class="form-control input-field" name="v_instagram" value="{{$data->v_instagram or ''}}">
                                      </div>
                                    </div>
                                </div>
                              </div>

                             
                              </div>

                              <div class="col-md-6">
                              </div>


                              <div style="clear: both"></div>

                              <div class="col-md-12">
                                   <h3 class="field-name">Order requirements</h3>
                              </div>

                              <div class="col-md-12">
                                <h4 class="subfield-name specify-orderspace">
                                  Please specify any order requirements you would like your customers to know before they contact you.
                                </h4>

                                <div class="form-group">
                                    {{-- <label>Short Description<span class="asterisk_input">*</span></label> --}}
                                    <textarea name="v_order_requirements" rows="5" class="form-control textarea" required>{{$data->v_order_requirements or ''}}</textarea>
                                </div>
                              </div>
                              

                              <div class="col-md-12">
                                 <div class="buttet-concept">
                                        <div class="name-details">
                                            <div class="searching-topspace">
                                                <label class="field-name">
                                                    Add search tags
                                                </label>
                                            </div>
                                        </div>
                                        <div class="name-details">
                                            <label class="subfield-name">
                                                Please add up to 3 search tags related to your service or job.
                                            </label>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="input-login">
                                                            <input class="form-control business-name-control" type="text" placeholder="Start typing ...." id="temp_text">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2 terms-add" id="hiddentags">
                                                        @if(isset($data->v_search_tags) && count($data->v_search_tags))
                                                            @foreach($data->v_search_tags as $k=>$v)
                                                                <input type="hidden" id="hidden-{{$v}}" name="v_search_tags[]" value="{{$v}}">
                                                            @endforeach
                                                        @endif    

                                                        <div class="login-btn" id="addtagbutton" @if(isset($data->v_search_tags) && count($data->v_search_tags)>=3)  style="display: none" @endif>
                                                            <button type="button" class="btn add-now" onclick="myFunction()">Add</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="selection-point" id="add_text">
                                                            @if(isset($data->v_search_tags) && count($data->v_search_tags))
                                                                @foreach($data->v_search_tags as $k=>$v)
                                                            <button class='auto-width' id='{{$v}}' onclick='removeButton(this.id)'><span>{{$v}}</span><img src="{{url('public/Assets/frontend/images/close1.png')}}"></button>
                                                                @endforeach
                                                            @endif    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                              </div>  
                              <div style="clear: both"></div>
                              <div class="col-md-12" style="margin-top: 35px">
                                  <a href="{{url('admin/users')}}">
                                  <button type="button" class="btn btn-warning">Back to List</button>
                                  </a>
                                  <button type="submit" class="btn btn-primary">Save</button>
                              </div>
                              
                            </form>
                        </div>
                  </div>
              </div>

          </div>
      </section>

   <div class="modal fade" id="deleteModalimg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font">Confirm Remove Image ?</h3>
            </div>
            <div class="modal-body" style="text-align:center">
            <i class="margin-top-10 fa fa-question fa-5x"></i>
            <h4>Delete !</h4>
            <p>Are you sure you want to delete this image?</p>
            </div>
           <form name="deleteimg" id="deleteimgjobpost" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="i_seller_id" id='i_seller_id_delete'>
            <input type="hidden" name="v_photo_name" id='v_photo_name_delete'>
            <input type="hidden" name="v_span" id='v_span_delete'>
           </form>      
            <div class="modal-footer" style="padding-top: 20px;">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" type="button" onclick="jobpostimagedelete();" ><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
                <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" type="button" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModalvidso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font">Confirm Remove Video ?</h3>
            </div>
            <div class="modal-body" style="text-align:center">
            <i class="margin-top-10 fa fa-question fa-5x"></i>
            <h4>Delete !</h4>
            <p>Are you sure you want to delete this video?</p>
            </div>
           <form name="deletevideo" id="deletevideojobpost" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="i_seller_id" id='i_seller_id_video'>
            <input type="hidden" name="v_video_name" id='v_video_name_video'>
            <input type="hidden" name="v_span" id='v_span_video'>
           </form>      
            <div class="modal-footer" style="padding-top: 20px;">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" type="button" onclick="jobpostvideodelete();" ><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
                <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" type="button" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModalworkvidso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font">Confirm Remove Video ?</h3>
            </div>
            <div class="modal-body" style="text-align:center">
            <i class="margin-top-10 fa fa-question fa-5x"></i>
            <h4>Delete !</h4>
            <p>Are you sure you want to delete this video?</p>
            </div>
           <form name="deletevideo" id="deletevideojobpost" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="i_seller_id" id='i_seller_id_video'>
            <input type="hidden" name="v_video_name" id='v_video_name_video'>
            <input type="hidden" name="v_span" id='v_span_video'>
           </form>      
            <div class="modal-footer" style="padding-top: 20px;">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" type="button" onclick="workvideodelete();" ><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
                <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" type="button" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
</div>
              
    @else
    

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        
        @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

          <div class="box">
            <div class="box-header pull-right">
              <a href="{{url('admin/users/add/0')}}">
                 <button type="submit" class="btn btn-info">Add New User</button>
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Buyer Account Status</th>
                  <th>Seller Account Status</th>
                  <th>Added Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      
                      <td>
                        @if(isset($value->v_image) && $value->v_image != '' )
                          <img src="{{url('public/uploads/userprofiles',$value->v_image)}}" height="50" width="50" alt="" />
                        @else
                          <img src="{{url('public/theme/images/no-image.png')}}" height="50" width="50" alt="" />  
                        @endif
                      </td>

                      <td>{{$value->v_fname or ''}} {{$value->v_lname or ''}}</td>
                      <td>{{$value->v_email or ''}}</td>
                      <td>{{isset($value->buyer['e_status']) ? $value->buyer['e_status'] : ''}}</td>
                      <td>{{isset($value->seller['e_status']) ? $value->seller['e_status'] : ''}}</td>
                      <td>{{ isset($value->d_added) ? date('M d Y' ,strtotime($value->d_added)) : '' }}</td>
                      
                      <td>
                        
                        <a href="{{url('admin/users/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/users',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if($value->e_status == 'inactive' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                        @php $a=url('admin/users/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" onclick="confirmDelete('{{ $value->id }}',{{$a}}) class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>

                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>

  <img src="http://192.168.0.228/projects/skillbox/public/Assets/frontend/images/pinkclose.png">
  
@stop

@section('js')
<style type="text/css">
  .rebovebullet{
    position: relative;
  }
 .rebovebullet img {
    position: absolute;
    right: 6px;
    top: 9px;
    max-width: 16px;
  }
  .rebovebullet input{
    padding-right: 20px;
  }

  .reboveaddon{
    position: relative;
  }
 .reboveaddon img {
    position: absolute;
    right: 6px;
    top: 9px;
    max-width: 16px;
  }
  .reboveaddon input{
    padding-right: 20px;
  }

</style>
<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
{{-- <script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script> --}}
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{url('/public/Assets/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
  
  $(document).on("click", ".removebullet", function() {
      $(this).parent().remove();
  });

  $(document).on("click", ".removeaddon", function() {
      $(this).parent().remove();
     
      if($("#premiumpackageaddon > div").length==2){
          $(".premiumpackageaddon").css("display","block");
      }
      if($("#basicpackageaddon > div").length==2){
          $(".basicpackageaddon").css("display","block");
      }
      if($("#standardpackageaddon > div").length==2){
          $(".standardpackageaddon").css("display","block");
      }
  });

  var closeimgurl = "{{url('public/Assets/frontend/images/pinkclose.png')}}";
  function addBullets(name,id){
      var strrappend = '<div class="rebovebullet"><input type="text" class="form-control input-field" name="'+name+'" style="margin-top:5px"><img src="'+closeimgurl+'" class="removebullet" ></div>';
      $("#"+id).append(strrappend);
  }

  function addAddon(name,id){
           
      var strrappend ='<div class="reboveaddon"><div class="package-title" style="margin-top:10px"><label class="buttet-heading">Add-on title (max 2 add-ons)</label><input type="text" class="form-control input-field" name="'+name+'[add_on][title][]"></div><div class="package-title"><label class="buttet-heading">Add-on price</label><input type="text" class="form-control input-field" name="'+name+'[add_on][v_price][]"></div><img src="'+closeimgurl+'" class="removeaddon" ></div>';
      
      $("#"+id).append(strrappend);
      $("."+id).css("display","none");

  }

  function selectSubCat(id,selectedid=""){

      if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/subCategories/')}}",
                data:'category_id='+id+"&sub_category_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    $("#i_sub_category_id").html(obj['optingStr'])
                }
            }); 
        }
    
  }

  // $('#i_parent_id').on('change',function(){
  //       var categoryID = $(this).val();
  //       if(categoryID){
  //           $.ajax({
  //               type:'get',
  //               url:"{{url('admin/users/subCategories/')}}",
  //               data:'category_id='+categoryID,
  //               success:function(data){
  //                   var obj1 = JSON.parse(data);
  //                   //alert(obj['status']);
  //                   $("#i_sub_category_id").html(obj1['optingStr'])
  //               }
  //           }); 
  //       }
  // });

  function selectSkill(id,selectedid=""){
        
        if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/skills/')}}",
                data:'category_id='+id+"&selected_skill_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    //alert(obj['status']);
                    $("#i_mainskill_id").html(obj['optingStr'])
                }
            }); 
        }
  }

  function selectState(id,selectedid=""){
        
        if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/state/')}}",
                data:'country_id='+id+"&selected_state_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    $("#i_state_id").html(obj['optingStr'])
                }
            }); 
        }
  } 

  function selectCity(id,selectedid=""){
        if(id){
            $.ajax({
                type:'get',
                url:"{{url('admin/users/city/')}}",
                data:'state_id='+id+"&selected_city_id="+selectedid,
                success:function(data){
                    var obj = JSON.parse(data);
                    $("#i_city_id").html(obj['optingStr'])
                }
            }); 
        }
  }    

  $(document).ready(function(){
        var cat_id = '{{isset($data->i_category_id) ? $data->i_category_id : ''}}';
        
        if(cat_id != ''){
           //getSelSkill(cat_id);
        }
        var v_type = '{{(isset($userdata->billing_detail['v_type']) && $userdata->billing_detail['v_type'] != '') ? $userdata->billing_detail['v_type'] : ''}}';

       
        if(v_type == 'paypal'){

          $("#paypalinformation").css("display","block");   
          $("#bankinformation").css("display","none");   

          $("#v_bankname").removeAttr("required");
          $("#v_accountno").removeAttr("required");
          $("#v_ifsc").removeAttr("required");
          $("#v_account_holder_name").removeAttr("required");
          $("#paypalemail").attr("required","true");


        } else if(v_type == 'bank'){

          $("#paypalinformation").css("display","none");    
          $("#bankinformation").css("display","block");  
          $("#paypalemail").removeAttr("required");

          $("#v_bankname").attr("required","true");
          $("#v_accountno").attr("required","true");
          $("#v_ifsc").attr("required","true");
          $("#v_account_holder_name").attr("required","true");

        }else if(v_type == ''){

          $("#paypalinformation").css("display","none");    
          $("#bankinformation").css("display","block");  
          $("#paypalemail").removeAttr("required");

          $("#v_bankname").attr("required","true");
          $("#v_accountno").attr("required","true");
          $("#v_ifsc").attr("required","true");
          $("#v_account_holder_name").attr("required","true");
          
          $("#v_type_bank").prop("checked",true);
        }
    
        
        $("#i_otherskill_id").select2();
        <?php 
            if($view == 'edit' && isset($data->l_other_skills ) && ($data->l_other_skills != '')){
        ?>
        $('#othes_skill').show();
        <?php }else {?>
         $('#othes_skill').hide();
        <?php 
            }
            if($view == 'edit' && isset($data->e_type ) && ($data->e_type == 'seller' || $data->e_type == 'both' )){
        ?>
          $('#seller_info').show();
          $('#seller_education').show();
          $('#seller_employment').show();
          $('#seller_packages').show();
          $('#company_detail').show();
          

        <?php }else{ ?>
          $('#seller_info').hide();
          $('#seller_education').hide();
          $('#seller_employment').hide();
          $('#seller_packages').hide();
          $('#company_detail').hide();
        
        <?php } ?>
        $('#e_type').on('change', function() {
          var valType = this.value;

          if(valType == 'seller' || valType == 'both' ){
             $('#seller_info').show();
             $('#seller_education').show();
             $('#company_detail').show();
             $('#seller_employment').show();
             $('#seller_packages').show();  
          }else{
            $('#seller_info').hide();
            $('#seller_education').hide();
            $('#seller_employment').hide();
            $('#company_detail').hide();
            $('#seller_packages').hide();
          }
         
      })

      $('#d_date').datepicker({
        
      });
  // $('#d_start_date').datepicker();
  // $('#d_start_date').on('change', function() {
  //   var start_date = $('#d_start_date').val();
  //   $('#d_end_date').datepicker({
  //     minDate: new Date(start_date)
  //   }); 
  // })

     $(function() {
      $("#d_start_date").datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 3,
          onClose: function( selectedDate ) {
              $("#d_end_date").datepicker( "option", "minDate", selectedDate );
          }
      });

      $("#d_end_date").datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 3,
          onClose: function( selectedDate ) {
              $("#d_start_date").datepicker("option", "maxDate", selectedDate);
          }
      });
    });
   
   
});

 // $('#i_mainskill_id').on('change click', function(){
 //      if($('#i_mainskill_id').val() == 'Other'){
 //        $('#othes_skill').show();
 //      }else{
 //        $('#othes_skill').hide();
 //      }
 //  });

 $('#i_mainskill_id').on('change', function() {
       var data = $(this).val();
    //  var data = $(".select2-hidden-accessible option:selected").text();
       if(jQuery.inArray( "Other", data ) != -1){
        $('#othes_skill').show();
      }else{
        $('#othes_skill').hide();
      }
    })

function getSkill(i_category_id){
            
    var formdata = "i_category_id="+i_category_id;
    var actionurl = "{{url('admin/job/getskill')}}";

    $.ajax({
          type    : "GET",
          url     : actionurl,
          data    : formdata,
          success : function( res ){
              var obj = jQuery.parseJSON(res);
              if(obj['status']==1){
                $("#i_mainskill_id").html(obj['optionstr']);
                $("#i_otherskill_id").html(obj['optionstr']);
              }
          },
          error: function ( jqXHR, exception ) {
              $("#someerror").show();
          }
      });    
} 

// function getSelSkill(i_category_id){

//     var formdata = "i_category_id="+i_category_id;
//     var actionurl = "{{url('admin/job/getskill')}}";

//     $.ajax({
//           type    : "GET",
//           url     : actionurl,
//           data    : formdata,
//           success : function( res ){
//               var obj = jQuery.parseJSON(res);
//               if(obj['status']==1){
//                 $("#i_mainskill_id").html(obj['optionstr']);
                
//                 $("#i_otherskill_id").html(obj['optionstr']);
//               }
//           },
//           error: function ( jqXHR, exception ) {
//               $("#someerror").show();
//           }
//       });    
// } 


function billing_detail(data){
          
    if(data=="bank"){
        
        $("#paypalinformation").css("display","none");    
        $("#bankinformation").css("display","block");  
        $("#paypalemail").removeAttr("required");

        $("#v_bankname").attr("required","true");
        $("#v_accountno").attr("required","true");
        $("#v_ifsc").attr("required","true");
        $("#v_account_holder_name").attr("required","true");
  
    }else{

        $("#paypalinformation").css("display","block");   
        $("#bankinformation").css("display","none");   

        $("#v_bankname").removeAttr("required");
        $("#v_accountno").removeAttr("required");
        $("#v_ifsc").removeAttr("required");
        $("#v_account_holder_name").removeAttr("required");
        $("#paypalemail").attr("required","true");

    }
} 

function myFunction() {
    
    var cnt = $('#add_text').children().length
    
    if(cnt>=3){
        $("#parsley-id-tags").css("display","block");
        jQuery('#temp_text').val("");
        return 0;
    }else{
        $("#parsley-id-tags").css("display","none");
    }

    var dt = new Date();
    var dynamic_id = dt.getDate() + "" + dt.getHours() + "" + dt.getMinutes() + "" + dt.getSeconds() + "" + dt.getMilliseconds();
    var y = jQuery('#temp_text').val();

     var y = jQuery('#temp_text').val();
      if(y==""){
          return 0;
      }
    
    var z = '<button>';
    jQuery('#add_text').append("<button class='auto-width' id='" + dynamic_id + "' onclick='removeButton(this.id)'><span>" + y + "</span><img src='{{url('public/Assets/frontend/images/close1.png')}}'></button>");

    var cnt = $('#add_text').children().length

            if(cnt==3){
                $("#addtagbutton").css("display","none");
            }

    var abval = $("#temp_text").val();
    var strhidden = '<input type="hidden" id="hidden-'+dynamic_id+'" name="v_search_tags[]" value="'+abval+'">';
    $("#hiddentags").append(strhidden);
    $("#temp_text").val("");
}

function removeButton(id) {
    jQuery("#" + id).remove();
    jQuery("#hidden-"+id).remove();

    var cnt = $('#add_text').children().length
    if(cnt<3){
        $("#addtagbutton").css("display","block");
    }
}

function removeimgjobpost(jid,data,remimg){
    $("#i_seller_id_delete").val(jid);
    $("#v_photo_name_delete").val(data);
    $("#v_span_delete").val(remimg);
    $("#deleteModalimg").modal("show");
}

function jobpostimagedelete(){
    
    var v_span = $("#v_span_delete").val();
    var formdata = $("#deleteimgjobpost").serialize();
    var actionurl = "{{url('admin/users/seller/remove-img')}}";
    // document.getElementById('load').style.visibility="visible";
    $.ajax({
          type    : "POST",
          url     : actionurl,
          data    : formdata,
          success : function( res ){
              var obj = jQuery.parseJSON(res);
              if(obj['status']==1){
                $("#"+v_span).remove();
                $("#deleteModalimg").modal("hide");
              }
              // document.getElementById('load').style.visibility='hidden';

          },
          error: function ( jqXHR, exception ) {
              $("#someerror").show();
              // document.getElementById('load').style.visibility='hidden';
          }
      });    
}

function removevideojobpost(jid,data,remimg){
    $("#i_seller_id_video").val(jid);
    $("#v_video_name_video").val(data);
    $("#v_span_video").val(remimg);
    $("#deleteModalvidso").modal("show");
}

function jobpostvideodelete(){
    
    var v_span = $("#v_span_video").val();
    var formdata = $("#deletevideojobpost").serialize();
    var actionurl = "{{url('admin/users/seller/remove-video')}}";
    // document.getElementById('load').style.visibility="visible";

    $.ajax({
          type    : "POST",
          url     : actionurl,
          data    : formdata,
          success : function( res ){
              var obj = jQuery.parseJSON(res);
              if(obj['status']==1){
                $("#"+v_span).remove();
                $("#deleteModalvidso").modal("hide");
              }
              // document.getElementById('load').style.visibility='hidden';
          },
          error: function ( jqXHR, exception ) {
              $("#someerror").show();
              // document.getElementById('load').style.visibility='hidden';
          }
      });    
}


function removeworkvideo(jid,data,remimg){

    $("#i_seller_id_video").val(jid);
    $("#v_video_name_video").val(data);
    $("#v_span_video").val(remimg);
    $("#deleteModalworkvidso").modal("show");
}

function workvideodelete(){
    
    var v_span = $("#v_span_video").val();
    var formdata = $("#deletevideojobpost").serialize();
    var actionurl = "{{url('admin/users/seller/remove-work-video')}}";
    // document.getElementById('load').style.visibility="visible";

    $.ajax({
          type    : "POST",
          url     : actionurl,
          data    : formdata,
          success : function( res ){
              var obj = jQuery.parseJSON(res);
              if(obj['status']==1){
                $("#"+v_span).remove();
                $("#deleteModalworkvidso").modal("hide");
              }
              // document.getElementById('load').style.visibility='hidden';
          },
          error: function ( jqXHR, exception ) {
              $("#someerror").show();
              // document.getElementById('load').style.visibility='hidden';
          }
      });    
}


</script>
<script type="text/javascript">
  $('.textarea').wysihtml5()
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeUrl5sq-j9P3aar5jKpOiTqralR5T5GE&libraries=places&callback=initAutocomplete" async defer></script>
<script type="text/javascript">

        var placeSearch, autocomplete;
        var componentForm = {
            premise: 'long_name',
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'long_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {

            var place = autocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            $("#v_latitude").val(lat);
            $("#v_longitude").val(lng);

            var premise = '';
            var street_number = '';
            var route = '';

            for ( var i = 0; i < place.address_components.length; i++ ) {

                var addressType = place.address_components[i].types[0];

                if ( componentForm[addressType] && addressType == 'premise') {
                    premise = place.address_components[i][componentForm[addressType]];
                }
                if ( componentForm[addressType] && addressType == 'street_number') {
                    street_number = place.address_components[i][componentForm[addressType]];
                }
                if ( componentForm[addressType] && addressType == 'route') {
                    route = place.address_components[i][componentForm[addressType]];
                }
                else if ( componentForm[addressType] && addressType == 'postal_code') {
                    $('#v_pincode').val( place.address_components[i][componentForm[addressType]] );
                }
            }

            var address = '';

            if( premise != '' ) {
                address += ' ' + premise;
            }
            else {
                address += premise;
            }

            if( street_number != '' ) {
                address += ' ' + street_number;
            }
            else {
                address += street_number;
            }

            if( route != '' ) {
                address += ' ' + route;
            }
            else {
                address += route;
            }
            $('#v_city').val(address);

        }

            function geolocate() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        var circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocomplete.setBounds(circle.getBounds());
                    });
                }
            }
            </script>

@stop

