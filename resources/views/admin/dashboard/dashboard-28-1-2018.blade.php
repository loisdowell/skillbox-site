@extends('layouts.master')
<style type="text/css">
  .letestuser{
        margin-bottom: 10px;
  }
  .iconcenter{
    margin-top: 22px;
  }
</style>


@section('content')

<style type="text/css">
  .users-list>li {
    width: 15%;
    float: none;
    padding: 10px;
    text-align: center;
    display: inline-block;
    vertical-align: top;
}
</style>

<link href="{{url('/public/Assets/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css" />
  <div class="content-wrapper">
   
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Dashboard panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    <form class="horizontal-form" role="form" method="GET" name="adminForm"  action="" data-parsley-validate enctype="multipart/form-data" >
       <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>Start date:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="d_start_date" class="form-control pull-right" id="datepickerstart" value="{{isset($d_start_date)?$d_start_date:''}}" required="">
                </div>
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label>End date:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="d_end_date" class="form-control pull-right" id="datepickerend" value="{{isset($d_end_date)?$d_end_date:''}}" required="">
                </div>
              </div>
            </div>
             <div class="col-md-2">
              <div class="form-group">
                <label>&nbsp;</label>
                <div class="input-group date">
                 <button type="submit" class="btn btn-primary">Search</button> 
                </div>
              </div>
            </div>
      </div>
      </form>

     

     <div class="row">

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-contact-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Users</span>
              <span class="info-box-number">{{$totaluser}}</span>
            </div>
          </div>
        </div>

         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-contact-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Basic Subscribers </span>
              <span class="info-box-number">{{$totalbasicuser}}</span>
            </div>
          </div>
        </div>

         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-contact-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Standard Subscribers </span>
              <span class="info-box-number">{{$totalstandarduser}}</span>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-contact-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Premium Subscribers</span>
              <span class="info-box-number">{{$totalpremiumuser}}</span>
            </div>
          </div>
        </div>



        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-graduation-cap iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Courses Live</span>
              <span class="info-box-number">{{$totalCoursesLive}}</span>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-graduation-cap iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Courses Sold</span>
              <span class="info-box-number">{{$totalCoursesSold}}</span>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-graduation-cap iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Courses Sales £</span>
              <span class="info-box-number">£{{number_format($totalCoursesSales,2)}}</span>
            </div>
          </div>
        </div>

         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Commission by Buyers</span>
              <span class="info-box-number">£{{number_format($totalCommissionbyBuyers,2)}}</span>
            </div>
          </div>
        </div>


        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Commission by Sellers </span>
              <span class="info-box-number">£{{number_format($totalCommissionbySellers,2)}}</span>
            </div>
          </div>
        </div>

         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-graduation-cap iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Commission Course Sales</span>
              <span class="info-box-number">£{{number_format($totalCommissionCourseSales,2)}}</span>
            </div>
          </div>
        </div>




        
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-suitcase iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Active Jobs</span>
              <span class="info-box-number">{{$totalActiveJobs}}</span>
            </div>
           
          </div>
        </div>


        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-ticket iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Ticket Open</span>
              <span class="info-box-number">{{$totalTicketOpen}}</span>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-ticket iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Ticket Closed</span>
              <span class="info-box-number">{{$totalTicketClosed}}</span>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-sort iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Jobs Applications</span>
              <span class="info-box-number">{{$totalJobsApplications}}</span>
            </div>
          </div>
        </div>
          
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-graduation-cap iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Jobs Applied</span>
              <span class="info-box-number">{{$totalJobsApplied}}</span>
            </div>
          </div>
        </div>

         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Sales</span>
              <span class="info-box-number">£{{number_format($totalsales,2)}}</span>
            </div>
          </div>
        </div>


         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Monthly Subscribers Sales</span>
              <span class="info-box-number">£{{number_format($totalmonthlysales,2)}}</span>
            </div>
          </div>
        </div>

         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Annual Subscribers Sales</span>
              <span class="info-box-number">£{{number_format($totalanualsales,2)}}</span>
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total processing Fee charged</span>
              <span class="info-box-number">£{{number_format($totalProccessing,2)}}</span>
            </div>
          </div>
        </div>
        
        <div class="clearfix visible-sm-block"></div>

        {{-- <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-first-order iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Order Skill</span>
              <span class="info-box-number">0</span>
            </div>
           
          </div>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-contact-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Order User Plan</span>
              <span class="info-box-number">0</span>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-bold iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Blog</span>
              <span class="info-box-number">0</span>
            </div>
           
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">New Members</span>
              <span class="info-box-number">2,000</span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Total Order</span>
              <span class="info-box-number">0</span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus iconcenter"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Order Courses</span>
              <span class="info-box-number">0</span>
            </div>
          </div>
        </div> --}}

      </div>
     <div style="clear: both"></div>
      <div class="col-md-12">
        <!-- USERS LIST -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Latest users</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <ul class="users-list clearfix">
              @if(isset($latestuser) && count($latestuser) )
               @foreach( $latestuser as $count => $value )
                <li>
                  @php
                      $imgdata="";
                      if($value->v_image!=""){
                        $imgdata = \Storage::cloud()->url($value->v_image);     
                      }else{
                        $imgdata = \Storage::cloud()->url('users/img-upload.png');
                      }
                  @endphp
                  <img src="{{$imgdata}}" alt="User Image" height="50" width="50">
                  <a href="{{url('admin/users/edit', $value->_id)}}" class="users-list-name" href="#">{{$value->v_fname or ''}} {{$value->v_lname or ''}}</a>
                  <span class="users-list-date">{{date("M d Y",strtotime($value->d_added))}}</span>
                </li>
               @endforeach
              @endif
            </ul>
            <!-- /.users-list -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer text-center">
            <a href="{{url('admin/users')}}" class="uppercase">View All Users</a>
          </div>
          <!-- /.box-footer -->
        </div>
        <!--/.box -->
      </div>
      
      {{-- <div class="col-md-6">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Latest Orders</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
              <tr>
                <th>Order ID</th>
                <th>Item</th>
                <th>Order Date</th>
              </tr>
              </thead>
              <tbody>
                @if( isset($latestorders) && count($latestorders) )
                  @foreach( $latestorders as $count => $value )
                    <tr>
                      <td>{{$value->i_order_no or ''}}</td>
                      <td width="50%">
                        @if(isset($value->v_order_detail) && count($value->v_order_detail))
                          @foreach($value->v_order_detail as $key => $valueDetail)
                            {{$valueDetail['name'] or ''}}
                          @endforeach
                        @endif
                      </td>
                      <td>{{date("d M Y",strtotime($value->d_added))}}</td>
                    </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <a href="{{url('admin/order/skill')}}" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
        </div>
        <!-- /.box-footer -->
      </div>
    </div> --}}

    </section>
  </div>
@stop
@section('js')
<script src="{{url('/public/Assets/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script type="text/javascript">
 $('#datepicker').datepicker({
      autoclose: true
 })
 
 $('#datepickerend').datepicker({
      autoclose: true
    })
 $('#datepickerstart').datepicker({
      autoclose: true
 })

</script>

@stop

