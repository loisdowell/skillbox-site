@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
.chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}

.chat li.left .chat-body
{
    margin-left: 60px;
}

.chat li.right .chat-body
{
    margin-right: 60px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}

.panel-body
{
/*    overflow-y: scroll;
    height: 250px;
*/
}
.panel-primary {
     border: none !important; 
}


</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Messages
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/messages')}}">Messages And Reviews</a></li>
        <li class="active">messages</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="view")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
         
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
               
                <div class="panel-body">
                 

                    <ul class="chat">
                        <li class="right clearfix"><span class="chat-img pull-right">
                            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    @php 
                                        $humanTime= App\Helpers\General::time_elapsed_string(strtotime($maindata->d_added));
                                    @endphp
                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>{{$humanTime}}
                                    </small>
                                    <strong class="pull-right primary-font">{{ count( $maindata->hasFromUser() ) ? $maindata->hasFromUser()->v_name : ''}}</strong>
                                </div>
                                <p>
                                    {{$maindata->l_message or ''}}
                                </p>
                            </div>
                        </li>
                    @if( isset($data) && count($data) )

                     @foreach( $data as $count => $value )
                     @if($maindata->i_from_user_id != $value->i_from_user_id)

                        <li class="left clearfix"><span class="chat-img pull-left">
                            <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">{{ count( $value->hasFromUser() ) ? $value->hasFromUser()->v_name : ''}}</strong> <small class="pull-right text-muted">
                                      @php 
                                          $humanTime= App\Helpers\General::time_elapsed_string(strtotime($value->d_added));
                                      @endphp
                                        <span class="glyphicon glyphicon-time"></span>{{$humanTime}}</small>
                                </div>
                                <p>
                                    {{$value->l_message or ''}}
                                </p>
                            </div>
                        </li>
                    @else    
                        <li class="right clearfix"><span class="chat-img pull-right">
                            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    @php 
                                          $humanTime= App\Helpers\General::time_elapsed_string(strtotime($value->d_added));
                                      @endphp
                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>{{$humanTime}}</small>
                                    <strong class="pull-right primary-font">{{ count( $value->hasFromUser() ) ? $value->hasFromUser()->v_name : ''}}</strong>
                                </div>
                                <p>
                                    {{$value->l_message or ''}}
                                </p>
                            </div>
                        </li>
                        
                      @endif 
                       @endforeach
            
                      @endif
                        
                    </ul>

                   <div class="row">
                          <div class="col-md-12">
                              <a href="{{url('admin/messages')}}">
                              <button type="button" class="btn btn-warning">Back to List</button>
                              </a>
                           </div>
                  </div>
                  </div>
                 
                
                </div>
            </div>
        </div>
    </div>
</div>

</div>
        </div>
    </div>

          </div>
        </div>
      </section>      
              
    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>From User</th>
                  <th>To User</th>
                  <th>subject</th>
                  <th>Message</th>
                  <th>Status</th>
                  <th>View Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      <td>{{ count( $value->hasFromUser() ) ? $value->hasFromUser()->v_name : ''}}</td>
                      <td>{{ count( $value->hasToUser() ) ? $value->hasToUser()->v_name : ''}}</td>
                      <td>{{$value->v_subject or ''}}</td>
                      <td>{{$value->l_message or ''}}</td>
                      <td>{{$value->e_status or ''}}</td>
                      <td>{{$value->e_view_status or ''}}</td>
                      <td>
                        <a href="{{url('admin/messages/view/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                        
                        @php $a=url('admin/messages/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" onclick="confirmDelete('{{ $value->id }}','<?php echo $a ?>')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="7" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
</script>

@stop

