@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
.chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}

.chat li.left .chat-body
{
    margin-left: 60px;
}

.chat li.right .chat-body
{
    margin-right: 60px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}

.panel-body
{
/*    overflow-y: scroll;
    height: 250px;
*/
}
.panel-primary {
     border: none !important; 
}


</style>
@stop






@section('content')

<div class="modal fade" id="commentdeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title custom-font">Confirm Delete Record ?</h3>
      </div>
      <div class="modal-body" style="text-align:center">
      <i class="margin-top-10 fa fa-question fa-5x"></i>
      <h4>Delete !</h4>
      <p>Are you sure you want to delete this record?</p>
      </div>
      <input type="hidden" name="commentdeleteId" id='commentdeleteId'>
      <div class="modal-footer" style="padding-top: 20px;">
        <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" onclick="commentdeleteAction()"><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
        <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
      </div>
    </div>
  </div>
</div>




  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Review And Comments
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/messages')}}">Messages And Reviews</a></li>
        <li class="active">reviews</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="view")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
         
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
               
                <div class="panel-body">
                 
                  <table class="table table-bordered table-striped">
                    <small class="pull-right text-muted">
                      @php 
                        $humanTime= App\Helpers\General::time_elapsed_string(strtotime($data->d_added));
                      @endphp
                      <span class="glyphicon glyphicon-time"></span>{{$humanTime}}
                                    </small>
                        <tr>
                          <td><b>User</b></td>
                          <td>{{ count( $data->hasFromUser() ) ? $data->hasFromUser()->v_name : ''}}</td>
                        </tr>
                        <tr valign="top">
                          <td><b>Type</b></td>
                          <td>{{$data->e_type or ''}}</td>
                        </tr>
                        <tr valign="top">
                          <td><b>Reference</b></td>
                          <td>
                            @if($data->e_type == 'seller' || $data->e_type == 'buyer')
                              {{ count( $data->hasReferenceUser() ) ? $data->hasReferenceUser()->v_name : ''}}
                            @elseif($data->e_type == 'job')
                              {{ count( $data->hasJob() ) ? $data->hasJob()->v_title : ''}}
                            @else
                              {{ count( $data->hasCourse() ) ? $data->hasCourse()->v_title : ''}}
                            @endif
                          </td>
                        </tr>
                        
                        <tr>
                          <td><b>Reivew</b></td>
                          <td>{{$data->l_message or ''}}</td>
                        </tr>

                  </table>


                    <ul class="chat">
                       
                     @if( isset($comment) && count($comment) )

                     @foreach( $comment as $count => $value )
                     @if($data->id == $value->i_review_id)

                        <li class="left clearfix" id="<?php echo $value->id; ?>-comment">
                        <span class="chat-img pull-left" >
                            <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">{{ count( $value->hasUser() ) ? $value->hasUser()->v_name : ''}}</strong> <small class="pull-right text-muted">
                                       @php 
                                          $humanTime= App\Helpers\General::time_elapsed_string(strtotime($value->d_added));
                                        @endphp
                                        <span class="glyphicon glyphicon-time"></span>{{$humanTime}}</small>
                                </div>
                                <p>
                                    {{$value->l_message or ''}}
                                </p>
                                
                                <a class="pull-right" href="javascript:;" onclick="deleteComment('<?php echo $value->id; ?>')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                            </div>
                        </li>

                    @endif 
                    @endforeach 
                    @endif   
                    
                        
                    </ul>

                   <div class="row">
                          <div class="col-md-12">
                              <a href="{{url('admin/reviews')}}">
                              <button type="button" class="btn btn-warning">Back to List</button>
                              </a>
                           </div>
                  </div>
                  </div>
                 
                
                </div>
            </div>
        </div>
    </div>
</div>

</div>
        </div>
    </div>

          </div>
        </div>
      </section>      
              
    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Type</th>
                  <th>From User</th>
                  <th>Reference</th>
                  <th>Star</th>
                  <th>Title</th>
                  <th>Message</th>
                  <th>Status</th>
                  <th>View Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      <td>{{$value->e_type or ''}}</td>
                      <td>{{ count( $value->hasFromUser() ) ? $value->hasFromUser()->v_name : ''}}</td>
                      <td>
                        @if($value->e_type == 'seller' || $value->e_type == 'buyer')
                          {{ count( $value->hasReferenceUser() ) ? $value->hasReferenceUser()->v_name : ''}}
                        @elseif($value->e_type == 'job')
                          {{ count( $value->hasJob() ) ? $value->hasJob()->v_title : ''}}
                        @else
                          {{ count( $value->hasCourse() ) ? $value->hasCourse()->v_title : ''}}
                        @endif
                      </td>
                      <td>{{$value->i_stars or ''}}</td>
                      <td>{{$value->v_title or ''}}</td>
                      <td>{{$value->l_message or ''}}</td>
                      <td>{{$value->e_status or ''}}</td>
                      <td>{{$value->e_view_status or ''}}</td>
                      <td>
                        <a href="{{url('admin/reviews/view/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                        
                        @php $a=url('admin/reviews/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" onclick="confirmDelete('{{ $value->id }}','<?php echo $a ?>')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="9" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });



  });

  function deleteComment(id){
      
      $("#commentdeleteId").val(id);
      $("#commentdeleteModal").modal("show");
  }


  function commentdeleteAction(){
      var cid = $("#commentdeleteId").val();
      $.ajax({
          type:'get',
          url:"{{url('admin/reviews/deletecomment/')}}",
          data:'comment_id='+cid,
          success:function(data){
            $('#'+cid+'-comment').html("");
            $('#'+cid+'-comment').remove();
            $("#commentdeleteModal").modal("hide");
          }
      });


  }



</script>

@stop

