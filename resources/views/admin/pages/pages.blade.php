@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop


@section('content')
  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Pages
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">pages</li>
      </ol>
    </section>

    <!-- Main content -->
    
    @if($view=="add" || $view=="edit")
      <div style="clear: both"></div>
      <div class="col-xs-12">         
          @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

    </div>  
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} new Page</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <div class="box-body">
              <br/><br/>
                <div class="row">
               
          <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/pages/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
            {{ csrf_field() }}
            
            <div class="col-md-6">
                
                <div class="form-group">
                  <label>Title<span class="asterisk_input">*</span></label>
                  <input type="text" name="v_title" id="v_title" class="form-control" placeholder="Name" value="{{$data->v_title or old('v_title')}}" required="" data-parsley-trigger="keyup">
                </div>

            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Slug<span class="asterisk_input">*</span></label>
                    <input type="text" name="v_key" id="v_key" class="form-control" placeholder="Slug" value="{{$data->v_key or old('v_key')}}" required="" data-parsley-trigger="keyup">
                </div>
            </div>

            <div class="col-md-6">
                
                <div class="form-group">
                    <label>Banner Text<span class="asterisk_input">*</span></label>
                    <input type="text" name="v_banner_text" class="form-control" value="{{$data->v_banner_text or old('v_banner_text')}}" required="" >
                </div>

            </div>

            <div class="col-md-6">

                  <div class="form-group">
                    <div><label>Banner Image [1494 X 260]<span class="asterisk_input">*</span></label></div>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                      <div class="fileinput-new thumbnail" style="width: 200px;">
                        
                        @if(isset($data->v_banner_img) && $data->v_banner_img != '' )
                           @php
                              $imgdata="";
                              if(Storage::disk('s3')->exists($data->v_banner_img)){
                                  $imgdata = \Storage::cloud()->url($data->v_banner_img);      
                              }
                           @endphp 
                          <img src="{{$imgdata}}" style="max-width: 150px" alt="" />
                        @else
                           @php
                              $imgdata = \Storage::cloud()->url('common/no-image.png');      
                           @endphp 
                          <img src="{{$imgdata}}" alt="" />  
                        @endif

                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                      <div>
                         <span class="btn default btn-file">
                            <span class="fileinput-new"> Select Image </span>
                            <span class="fileinput-exists"> Change </span>
                            <input type="file" name="v_banner_img" > </span>
                         <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                      </div>
                    </div>
                  </div>
            </div>

            {{--
            <div class="col-md-6">
                              
                    <div class="form-group">
                        <label>Title<span class="asterisk_input">*</span></label>
                        <input type="text" name="v_title" id="v_title" class="form-control" placeholder="Name" value="{{$data->v_title or old('v_title')}}" required="" data-parsley-trigger="keyup">
                    </div>
                      <div class="form-group">
                        <label>Slug<span class="asterisk_input">*</span></label>
                        <input type="text" name="v_key" id="v_key" class="form-control" placeholder="Slug" value="{{$data->v_key or old('v_key')}}" required="" data-parsley-trigger="keyup">
                      </div>
                    
                      <div class="form-group">
                        <div><label>Banner Image [1494 X 260]<span class="asterisk_input">*</span></label></div>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px;">
                            @if( isset($data->v_banner_img) && $data->v_banner_img != '' )
                              <img src="{{url('public/uploads/pages',$data->v_banner_img)}}" style="max-width: 200px" alt="" />
                            @else
                              <img src="{{url('public/theme/images/no-image.png')}}" alt="" />  
                            @endif
                          </div>
                          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                          <div>
                             <span class="btn default btn-file">
                                <span class="fileinput-new"> Select Image </span>
                                <span class="fileinput-exists"> Change </span>
                                <input type="file" name="v_banner_img" > </span>
                             <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                          </div>
                        </div>
                      </div>
              </div>
              

                  <div class="col-md-12">   
                      <div class="form-group">
                        <label>Short Description<span class="asterisk_input">*</span></label>
                        <textarea name="l_short_description" id="l_short_description" class="form-control" placeholder="Description(short)" required="" data-parsley-trigger="keyup">{{$data->l_short_description or old('l_short_description')}}</textarea>
                      </div>
                  </div>
                  --}}

                  <div class="col-md-12">   
                      <div class="form-group">
                        <label>Description<span class="asterisk_input">*</span></label>
                        <textarea name="l_description" id="l_description" class="form-control ckeditor" placeholder="Description" required="" data-parsley-trigger="keyup">{{$data->l_description or old('l_description')}}</textarea>
                      </div>
                  </div>

                 
                  <div class="col-md-12">
                      <div class="form-group">
                        <label>Meta Title<span class="asterisk_input">*</span></label>
                        <input type="text" name="v_meta_title" id="v_meta_title" class="form-control" placeholder="Name" value="{{$data->v_meta_title or old('v_meta_title')}}" required="" data-parsley-trigger="keyup">
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label>Meta Keywords<span class="asterisk_input">*</span></label>
                        <textarea name="v_meta_keywords" rows="4" id="v_meta_keywords" class="form-control" placeholder="Description" required="" data-parsley-trigger="keyup">{{$data->v_meta_keywords or old('v_meta_keywords')}}</textarea>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label>Meta Description<span class="asterisk_input">*</span></label>
                        <textarea name="l_meta_description" rows="4" id="l_meta_description" class="form-control" placeholder="Description" required="" data-parsley-trigger="keyup">{{$data->l_meta_description or old('l_meta_description')}}</textarea>
                      </div>
                  </div>
                  <div class="col-md-12">
                      <div class="form-group">
                <label>Status<span class="asterisk_input">*</span></label>
                <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                  <option value="">- select -</option>
                  <option value="active" @if( isset($data->e_status) && $data->e_status == 'active' ) selected @endif>Active</option>
                  <option value="inactive" @if( isset($data->e_status ) && $data->e_status == 'inactive') SELECTED @endif >Inactive</option>
                </select>
              </div>
                      
                 </div>
                
                </div>
          <div class="clear:both"></div>                
                
                <div class="row">
                  <div class="col-md-12">
                      <a href="{{url('admin/pages')}}">
                      <button type="button" class="btn btn-warning">Back to List</button>
                      </a>
                      <button type="submit" class="btn btn-primary">Save</button>
                  </div>
              </div> 
            </div>
            </form>
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>
          </div>
        </div>
      </section>      
              
    @else
      <section class="content">
        <div class="row">
        
        <div class="col-xs-12">         
              @if ($success = Session::get('success'))
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
            </div>
          @endif
          @if ($warning = Session::get('warning'))
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
            </div>
          @endif

        </div>  
        <div class="col-xs-12">
            <div class="box">
              
              <div class="box-header pull-right">
                <a href="{{url('admin/pages/add/0')}}">
                   <button type="submit" class="btn btn-info">Add New Page</button>
                </a>
              </div>
              <div style="clear: both;"></div>
              

              <div class="box-body" >
                <table id="adminlisttable" class="table table-striped table-bordered table-hover order-column dataTable ">
                  <thead>
                    <tr>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Status</th>
                        <th>Added Date</th>
                        <th>Actions</th>
                    </tr>
                  </thead>

                  <tbody>
                    @if( isset($data) && count($data) )
                      @foreach( $data as $count => $value )
                        <tr>
                          <td>{{$value->v_title or ''}}</td>
                          <td>{{$value->v_key or ''}}</td>
                           {{--  @if( isset($value->e_status) && $value->e_status != '' )
                              <td>{{ isset($value->e_status) ? ( $value->e_status == 'inactive' ? 'Pending' : ucfirst($value->e_status) ) : '' }}</td>
                            @else
                              <td></td>
                            @endif --}}
                          <td>
                            @if(isset($value->e_status) && $value->e_status=="active")
                                <span class="badge bg-green">Active</span>         
                            @else
                                <span class="badge bg-yellow">Inactive</span>
                            @endif
                          </td>  
                          <td>{{ isset($value->d_added) }}</td>
                          <td>
                            <a href="{{url('admin/pages/edit/')}}/{{$value->_id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a href="{{url('admin/pages',[$value->_id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                              @if( $value->e_status == 'inactive' )
                                <i title="Activate" class="fa fa-check"></i>
                              @else
                                <i title="Inactivate" class="fa fa-times"></i>
                              @endif
                            </a>
                            @php $a=url('admin/pages/action/delete/').'/'.$value->id; @endphp
                            <a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','<?php echo $a; ?>')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>

                          </td>
                        </tr>
                      @endforeach
                      @else
                        <tr>
                          <td colspan="5" align="center">@lang('message.common.noRecords')</td>
                        </tr>
                      @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    @endif

  
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#adminlisttable').DataTable({
      "paging": true,
    });
  });
</script>
          
<script>
  
  $('#v_title').change(function() {
      var string = $('#v_title').val();
      var $v_key = '';
      var trimmed = $.trim(string);

      $v_key = trimmed.replace(/[^a-z0-9-]/gi, '-')
              .replace(/-+/g, '-')
              .replace(/^-|-$/g, '');

      $('#v_key').val($v_key.toLowerCase());

      return true;
    });
</script>
@php
  $frontThemePath = url('public/Assets/frontend');
@endphp

<script type="text/javascript">
    $(document).ready(function(){
        CKEDITOR.replace( 'l_description',{
                  height: '500px',
                  contentsCss :[
                    '<?php echo $frontThemePath ?>/vactor-map/dist/jqvmap.css', 
                    '<?php echo $frontThemePath ?>/css/bootstrap.css',
                    '<?php echo $frontThemePath ?>/css/style.css', 
                    '<?php echo $frontThemePath ?>/css/slick.css', 
                    '<?php echo $frontThemePath ?>/css/slick-theme.css', 
                    '<?php echo $frontThemePath ?>/css/material-design-iconic-font.css', 
                    
                    
                    '<?php echo $frontThemePath ?>/star-rating/css/star-rating.css', 
                    
                    '<?php echo $frontThemePath ?>/css/font-awesome.min.css', 
                  ],
                  contentsJs :[
                    '<?php echo $frontThemePath ?>/js/jquery-3.2.0.min.js',
                    '<?php echo $frontThemePath ?>/js/bootstrap.js',
                    '<?php echo $frontThemePath ?>/star-rating/js/star-rating.js', 
                    '<?php echo $frontThemePath ?>/vactor-map/dist/jquery.vmap.js', 
                    '<?php echo $frontThemePath ?>/vactor-map/dist/maps/jquery.vmap.world.js', 
                    '<?php echo $frontThemePath ?>/vactor-map/jquery.vmap.sampledata.js',
                  ],
                  filebrowserBrowseUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/ckfinder.html?Type=Files') }}",
                  filebrowserImageBrowseUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/ckfinder.html?Type=Images') }}",
                  filebrowserFlashBrowseUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash') }}",
                  filebrowserUploadUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}",
                  filebrowserImageUploadUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/core/connctor/php/connector.php?command=QuickUpload&type=Images') }}",
                  filebrowserFlashUploadUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}",
        });
    })
</script>


   

<script type="text/javascript">
      // CKEDITOR.replace( 'l_description', {
      //   filebrowserBrowseUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/ckfinder.html?Type=Files') }}",
      //   filebrowserImageBrowseUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/ckfinder.html?Type=Images') }}",
      //   filebrowserFlashBrowseUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash') }}",
      //   filebrowserUploadUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}",
      //   filebrowserImageUploadUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/core/connctor/php/connector.php?command=QuickUpload&type=Images') }}",
      //   filebrowserFlashUploadUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}",
      // });
</script>





@stop

