@extends('layouts.master')

@section('title')
	{{$section or 'setting'}}
@stop

@section('css')
	<link href="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
	<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
	<style type="text/css">

		table.table-bordered thead tr{border: 1px solid #000 !important; }
		table.table-bordered tbody th, table.table-bordered tbody td{
		border: 1px solid #ebebeb;
		}
		.table-bordered{
			border: 1px solid #ddd !important;
		}
		.box-title {
			display: inline-block;
			font-size: 29px !important;
			line-height: 1;
			margin: 0;
		}
		.asterisk_input{
			color: #e32;
		}
	</style>
@stop

@section('model')
@stop

@section('content')
	<div class="content-wrapper">
		

	<section class="content-header">
	      <h1>
	        Email Templates Managment
	        <small></small>
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="{{url('admin/sitesetting')}}">Settings</a></li>
	        <li class="active">email template</li>
	      </ol>
    </section>

		<div style="clear: both"></div>
		<div class="col-xs-12">
			<div style="clear: both"></div>
			<div style="margin-top: 30px">         
			@if ($success = Session::get('success'))
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
				</div>
			@endif
			@if ($warning = Session::get('warning'))
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
				</div>
			@endif
			</div>

		</div>  
		<div style="clear: both"></div>     
		<section class="content">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Site Settings</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
					</div>
				</div>
				<?php /*
				@if ($errors->has())
					<div class="alert alert-danger">
						@foreach ($errors->all() as $error)
							{{ $error }}<br>
						@endforeach
					</div>
				@endif
				*/?>
				<div class="box-body">
					<br/><br/>
						
						<div class="row">
							<div class="col-md-12">
							  <!-- Custom Tabs -->
							  	<div class="nav-tabs-custom">
									
									<ul class="nav nav-tabs">
									  <li class="active"><a href="#tab_1" data-toggle="tab">General Settings</a></li>
									  <li><a href="#tab_4" data-toggle="tab">SMTP Settings For Email</a></li>
									  <li><a href="#tab_3" data-toggle="tab">Mangopay Settings</a></li>
									  <li><a href="#tab_5" data-toggle="tab">Alert Box</a></li>
									  <li><a href="#tab_6" data-toggle="tab">Homepage Banner Image</a></li>
									</ul>

									<div class="tab-content">
								  	<div class="tab-pane active" id="tab_1">

									  		<form class="horizontal-form" name="employment-edit" id="employment-edit" enctype="multipart/form-data" action="{{url('admin/sitesetting')}}" method="POST" data-parsley-validate>
											{{ csrf_field() }}
									
											<div class="row">
												<div class="col-md-4">
													
													<div class="form-group">
														<label>Site Name</label>
														<input type="text" name="SITE_NAME" id="SITE_NAME" class="form-control" placeholder="Site Name" value="{{$data['SITE_NAME'] or ''}}" required data-parsley-trigger="keyup">
													</div>
													<div class="form-group">
														<label>Plan Yearly discount%(In Percentage)</label>
														<input type="text" name="PLAN_YEALY_DISCOUNT" id="PLAN_YEALY_DISCOUNT" class="form-control"  value="{{$data['PLAN_YEALY_DISCOUNT'] or ''}}" required data-parsley-trigger="keyup">
													</div>
													<div class="form-group">
														<label>Site Footer</label>
														<input type="text" name="SITE_FOOTER_TEXT" id="SITE_FOOTER_TEXT" class="form-control"  value="{{$data['SITE_FOOTER_TEXT'] or ''}}" required data-parsley-trigger="keyup">
													</div>
													
													<div class="form-group">
														<label>Commission for Buyer %(In Percentage)</label>
														<input type="number" min="0" step="0.01" name="SITE_COMMISSION" id="SITE_COMMISSION" class="form-control" placeholder="Commission %(In Percentage)" value="{{$data['SITE_COMMISSION'] or ''}}" required data-parsley-trigger="keyup">
													</div>

													<div class="form-group">
														<label>Commission for Seller Basic plan %(In Percentage)</label>
														<input type="number" min="0" step="0.01" name="SITE_COMMISSION_SELLER_BASIC" id="SITE_COMMISSION_SELLER" class="form-control" placeholder="Commission %(In Percentage)" value="{{$data['SITE_COMMISSION_SELLER_BASIC'] or ''}}" required data-parsley-trigger="keyup">
													</div>

													<div class="form-group">
														<label>Commission for Seller Standard plan %(In Percentage)</label>
														<input type="number" min="0" step="0.01" name="SITE_COMMISSION_SELLER_STANDARD" id="SITE_COMMISSION_SELLER" class="form-control" placeholder="Commission %(In Percentage)" value="{{$data['SITE_COMMISSION_SELLER_STANDARD'] or ''}}" required data-parsley-trigger="keyup">
													</div>

													<div class="form-group">
														<label>Commission for Seller Premium plan %(In Percentage)</label>
														<input type="number" min="0" step="0.01" name="SITE_COMMISSION_SELLER_PREMIUM" id="SITE_COMMISSION_SELLER" class="form-control" placeholder="Commission %(In Percentage)" value="{{$data['SITE_COMMISSION_SELLER_PREMIUM'] or ''}}" required data-parsley-trigger="keyup">
													</div>


													<div class="form-group">
														<label>Setting per day sponsored ads impressions</label>
														<input type="number" min="0" step="0.01" name="SETTING_PER_DAY_IMPRESSIONS" id="SETTING_PER_DAY_IMPRESSIONS" class="form-control" value="{{$data['SETTING_PER_DAY_IMPRESSIONS'] or ''}}" required data-parsley-trigger="keyup">
													</div>

													<div class="form-group">
														<div><label>Site Logo</label></div>
														  <div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="fileinput-new thumbnail" style="width: 200px;">
																	@if( isset($data['SITE_LOGO']) && $data['SITE_LOGO'] != '' )
																		<img src="{{url('public/uploads/settings',$data['SITE_LOGO'])}}" style="max-width: 200px" alt= />
																	@else
																		<img src="{{url('public/theme/images/no-image.png')}}" alt= />  
																	@endif
																</div>
																<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
																<div>
																	 <span class="btn default btn-file">
																		  <span class="fileinput-new"> Select Image </span>
																		  <span class="fileinput-exists"> Change </span>
																		  <input type="file" name="SITE_LOGO" > </span>
																	 <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
																</div>
														  </div>
													</div>



												</div>
												<div class="col-md-4">

													{{-- site -email  --}}
													<div class="form-group">
														<label>Site Email</label>
														<input type="text" name="SITE_EMAIL" id="SITE_EMAIL" class="form-control" placeholder="Site Email" value="{{$data['SITE_EMAIL'] or ''}}" required data-parsley-trigger="keyup">
													</div>

													{{-- CURRENCY_FOR_PAYMENT  --}}
{{-- 													<div class="form-group">
														<label>Currenry For Payment</label>
														<input type="text" name="CURRENCY_FOR_PAYMENT" id="CURRENCY_FOR_PAYMENT" class="form-control" placeholder="Currncy For Payment" value="{{$data['CURRENCY_FOR_PAYMENT'] or '9.99'}}" required data-parsley-trigger="keyup">
													</div> --}}

													{{-- Service -fees  --}}
													{{-- <div class="form-group">
														<label>Service Fees(Below 15$)</label>
														<input type="text" name="SERVICE_FEES_BELOW" id="SERVICE_FEES_BELOW" class="form-control" placeholder="Service Fees" value="{{$data['SERVICE_FEES_BELOW'] or ''}}" required data-parsley-trigger="keyup">
													</div> --}}

													<div class="form-group">
														<label>Twitter Link</label>
														<input type="text" name="TEITTER_SOCIAL_LINK" id="TEITTER_SOCIAL_LINK" class="form-control"  value="{{$data['TEITTER_SOCIAL_LINK'] or ''}}" required data-parsley-trigger="keyup">
													</div>

													<div class="form-group">
														<label>Show alter messages</label>
														<select name="SHOW_ALERT_MESSAGE" id="SHOW_ALERT_MESSAGE" class="form-control" required>
															<option value="yes" @if( isset($data['SHOW_ALERT_MESSAGE']) && $data['SHOW_ALERT_MESSAGE'] == 'yes' ) selected @endif >Yes</option>
															<option value="no" @if( isset($data['SHOW_ALERT_MESSAGE']) && $data['SHOW_ALERT_MESSAGE'] == 'no' ) selected @endif>No</option>
														</select>
													</div>

													<div class="form-group">
														<label>Processing fee for Buyer</label>
														<input type="number" min="0" step="0.01" name="SITE_PROCESSING_FEE" id="SITE_PROCESSING_FEE" class="form-control"  value="{{$data['SITE_PROCESSING_FEE'] or ''}}" required data-parsley-trigger="keyup">
													</div>

													<div class="form-group">
														<label>Processing fee for Seller Basic plan</label>
														<input type="number" min="0" step="0.01" name="SITE_PROCESSING_FEE_SELLER_BASIC" id="SITE_PROCESSING_FEE_SELLER" class="form-control"  value="{{$data['SITE_PROCESSING_FEE_SELLER_BASIC'] or ''}}" required data-parsley-trigger="keyup">
													</div>
													<div class="form-group">
														<label>Processing fee for Seller Standard plan</label>
														<input type="number" min="0" step="0.01" name="SITE_PROCESSING_FEE_SELLER_STANDARD" id="SITE_PROCESSING_FEE_SELLER" class="form-control"  value="{{$data['SITE_PROCESSING_FEE_SELLER_STANDARD'] or ''}}" required data-parsley-trigger="keyup">
													</div>
													<div class="form-group">
														<label>Processing fee for Seller Premium plan</label>
														<input type="number" min="0" step="0.01" name="SITE_PROCESSING_FEE_SELLER_PREMIUM" id="SITE_PROCESSING_FEE_SELLER" class="form-control"  value="{{$data['SITE_PROCESSING_FEE_SELLER_PREMIUM'] or ''}}" required data-parsley-trigger="keyup">
													</div>

													<div class="form-group">
														<label>Order Cancel Admin Fee %(In Percentage)</label>
														<input type="number" min="0" step="0.01" name="ORDER_CANCEL_ADMIN_FEES" id="ORDER_CANCEL_ADMIN_FEES" class="form-control" value="{{$data['ORDER_CANCEL_ADMIN_FEES'] or ''}}" required data-parsley-trigger="keyup">
													</div>



													{{-- <div class="form-group">
														<label>Commission for Seller %(In Percentage)</label>
														<input type="number" min="0" step="0.01" name="SITE_COMMISSION_SELLER" id="SITE_COMMISSION_SELLER" class="form-control" placeholder="Commission %(In Percentage)" value="{{$data['SITE_COMMISSION_SELLER'] or ''}}" required data-parsley-trigger="keyup">
													</div> --}}

													


												</div>
												<div class="col-md-4">
												
													<div class="form-group">
														<label>Youtube Link</label>
														<input type="text" name="YOUTUBE_SOCIAL_LINK" id="YOUTUBE_SOCIAL_LINK" class="form-control" value="{{$data['YOUTUBE_SOCIAL_LINK'] or ''}}" required data-parsley-trigger="keyup">
													</div>
													<div class="form-group">
														<label>Address</label>
														<textarea name="SITE_ADDRESS" id="SITE_ADDRESS" class="form-control" placeholder="Address" rows="5" required data-parsley-trigger="keyup" >{{$data['SITE_ADDRESS'] or ''}}</textarea>
													</div>
													<div class="form-group">
														<label>Standard as Free Member</label>
														<select name="STANDARD_MEMBERSHIP_FREE" id="STANDARD_MEMBERSHIP_FREE" class="form-control">
															<option value="">Select</option>	
															<option value="1" @if( isset($data['STANDARD_MEMBERSHIP_FREE']) && $data['STANDARD_MEMBERSHIP_FREE'] == '1' ) selected @endif >1 Month</option>
															<option value="3" @if( isset($data['STANDARD_MEMBERSHIP_FREE']) && $data['STANDARD_MEMBERSHIP_FREE'] == '3' ) selected @endif>3 Months</option>
														</select>
													</div>

													<div class="form-group">
														<label>Facebook Link</label>
														<input type="text" name="FACEBOOK_SOCIAL_LINK" id="FACEBOOK_SOCIAL_LINK" class="form-control"  value="{{$data['FACEBOOK_SOCIAL_LINK'] or ''}}" required data-parsley-trigger="keyup">
													</div>
													<div class="form-group">
														<label>Linkedin Link</label>
														<input type="text" name="LINKEDIN_SOCIAL_LINK" id="LINKEDIN_SOCIAL_LINK" class="form-control"  value="{{$data['LINKEDIN_SOCIAL_LINK'] or ''}}" required data-parsley-trigger="keyup">
													</div>

													<div class="form-group">
														<label>Homepage overlay video url</label>
														<input type="url" name="HOME_PAGE_VIDEO_URL" id="HOME_PAGE_VIDEO_URL" class="form-control"  value="{{$data['HOME_PAGE_VIDEO_URL'] or ''}}" required data-parsley-trigger="keyup">
													</div>

												</div>
												<div class="col-md-3">
												</div>
												<div class="col-md-3">
												</div>
											</div>

											<a href="{{url('admin/sitesetting')}}">
											<button type="button" class="btn btn-warning">Cancel</button>
											</a>
											<button type="submit" class="btn btn-primary">Update</button>
								  		</div>

								  		<!-- /.tab-pane -->
										  <div class="tab-pane" id="tab_4">
										  		<form class="horizontal-form" name="employment-edit" id="employment-edit" enctype="multipart/form-data" action="{{url('admin/sitesetting')}}" method="POST" data-parsley-validate>
												{{ csrf_field() }}

												<div class="row">
													<div class="col-md-3">
														{{-- SMTP HOST  --}}
														<div class="form-group">
															<label>SMTP Host</label>
															<input type="text" name="SMTP_HOST" id="SMTP_HOST" class="form-control" placeholder="SMTP Host" value="{{$data['SMTP_HOST'] or ''}}" required data-parsley-trigger="keyup">
														   
														</div>
												
														{{-- SMTP FROM EMAIL  --}}
														<div class="form-group">
															<label>SMTP From Email</label>
															<input type="email" name="SMTP_FROM_EMAIL" id="SMTP_FROM_EMAIL" class="form-control" placeholder="SMTP From Email" value="{{$data['SMTP_FROM_EMAIL'] or ''}}" required data-parsley-trigger="keyup">
														   
														</div>
													</div>
													<div class="col-md-3">

														{{-- SMTP USERNAME  --}}
														<div class="form-group">
															<label>SMTP Username</label>
															<input type="text" name="SMTP_USERNAME" id="SMTP_USERNAME" class="form-control" placeholder="SMTP Username" value="{{$data['SMTP_USERNAME'] or ''}}" required data-parsley-trigger="keyup">
														</div>

														{{-- SMTP PORT  --}}
														<div class="form-group">
															<label>SMTP Port</label>
															<input type="text" name="SMTP_PORT" id="SMTP_PORT" class="form-control" placeholder="SMTP Port" value="{{$data['SMTP_PORT'] or ''}}" required data-parsley-trigger="keyup">
														   
														</div>
														
													</div>

													<div class="col-md-3">

														{{-- SMTP PROTOCOL  --}}
														<div class="form-group">
															<label>SMTP Protocol</label>
															<input type="text" name="SMTP_PROTOCOL" id="SMTP_PROTOCOL" class="form-control" placeholder="SMTP Protocol" value="{{$data['SMTP_PROTOCOL'] or ''}}" required data-parsley-trigger="keyup">
														   
														</div>

														{{-- SMTP FROM NAME  --}}
														<div class="form-group">
															<label>SMTP From Name </label>
															<input type="text" name="SMTP_FROM_NAME" id="SMTP_FROM_NAME" class="form-control" placeholder="SMTP From Name" value="{{$data['SMTP_FROM_NAME'] or ''}}" required data-parsley-trigger="keyup">
															
														</div>
													</div>
													<div class="col-md-3">

														{{-- SMTP PASSWORD  --}}
														<div class="form-group">
															<label>SMTP Password</label>
															<input type="text" name="SMTP_PASSWORD" id="SMTP_PASSWORD" class="form-control" placeholder="SMTP Password" value="{{$data['SMTP_PASSWORD'] or ''}}" required data-parsley-trigger="keyup">
															
														</div>
													</div>
												</div>
										  
										  		<a href="{{url('admin/sitesetting')}}">
												<button type="button" class="btn btn-warning">Cancel</button>
												</a>
												<button type="submit" class="btn btn-primary">Update</button>
										  </div>



										  <div class="tab-pane" id="tab_3">
										  		<form class="horizontal-form" name="employment-edit" id="employment-edit" enctype="multipart/form-data" action="{{url('admin/sitesetting')}}" method="POST" data-parsley-validate>
												{{ csrf_field() }}

												<div class="row">

													<div class="col-md-3">
														<div class="form-group">
															<label>Mangopay Admin UserId</label>
															<input type="text" name="MANGOPAY_USERID" id="MANGOPAY_USERID" class="form-control" placeholder="Mangopay user id" value="{{$data['MANGOPAY_USERID'] or ''}}" required data-parsley-trigger="keyup">
														</div>
													</div>

													<div class="col-md-3">
														<div class="form-group">
															<label>Mangopay Admin User Wallet Id</label>
															<input type="text" name="MANGOPAY_USER_WALLET_ID" id="MANGOPAY_USER_WALLET_ID" class="form-control" placeholder="Mangopay user Wallet id" value="{{$data['MANGOPAY_USER_WALLET_ID'] or ''}}" required data-parsley-trigger="keyup">
														</div>
													</div>

													{{--
													<div class="col-md-3">
														<div class="form-group">
															<label>Paypal mode</label>
															<select name="PAYPAL_MODE" id="PAYPAL_MODE" class="form-control" required>
																<option value="">-select-</option>
																<option value="sendbox" @if( isset($data['PAYPAL_MODE']) && $data['PAYPAL_MODE'] == 'sendbox' ) selected @endif >Sendbox</option>
																<option value="live" @if( isset($data['PAYPAL_MODE']) && $data['PAYPAL_MODE'] == 'live' ) selected @endif>Live</option>
															</select>
														   
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group">
															<label>Paypal Email</label>
															<input type="email" name="PAYPAL_EMIAL" id="PAYPAL_EMIAL" class="form-control" placeholder="Paypal Email" value="{{$data['PAYPAL_EMIAL'] or ''}}" required data-parsley-trigger="keyup">
														</div>
													</div>
													--}}

													
												</div>
										  
										  		<a href="{{url('admin/sitesetting')}}">
												<button type="button" class="btn btn-warning">Cancel</button>
												</a>
												<button type="submit" class="btn btn-primary">Update</button>
										  </div>
										   <div class="tab-pane" id="tab_5">
										  		<form class="horizontal-form" name="employment-edit" id="employment-edit" enctype="multipart/form-data" action="{{url('admin/sitesetting')}}" method="POST" data-parsley-validate>
												{{ csrf_field() }}

												<div class="row">
													<div class="col-md-3">
														<div class="form-group">
															<label>Alert box 1</label>
															<input type="text" name="ACTIVE_SUBSCRIBERS_ALERTBOX" id="ACTIVE_SUBSCRIBERS_ALERTBOX" class="form-control" placeholder="Active Subscribers Alertbox" value="{{$data['ACTIVE_SUBSCRIBERS_ALERTBOX'] or ''}}" required data-parsley-trigger="keyup">
														</div>
													</div>
													<div class="col-md-3">

														<div class="form-group">
															<label>Alert box 2</label>
															<input type="text" name="SKILLBOX_APP_ALERTBOX" id="SKILLBOX_APP_ALERTBOX" class="form-control" placeholder="Skillbox APP Alertbox" value="{{$data['SKILLBOX_APP_ALERTBOX'] or ''}}" required data-parsley-trigger="keyup">
														</div>

													</div>

													
												</div>
										  
										  		<a href="{{url('admin/sitesetting')}}">
												<button type="button" class="btn btn-warning">Cancel</button>
												</a>
												<button type="submit" class="btn btn-primary">Update</button>
										  </div>

										  <div class="tab-pane" id="tab_6">
										  		<form class="horizontal-form" name="employment-edit" enctype="multipart/form-data" action="{{url('admin/sitesetting')}}" method="POST" data-parsley-validate>
												{{ csrf_field() }}
										  		<div class="row">
										  			
										  			<div class="col-md-2">
										  			<div class="form-group">
														<div><label>Image 1 [1905*670]</label></div>
														  <div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="fileinput-new thumbnail" style="width: 200px;">
																	 

																	@if( isset($data['HOMEPAGE_IMAGE_1']) && $data['HOMEPAGE_IMAGE_1'] != '' )
																		@php
										                                    $imgdata="";
										                                    if(Storage::disk('s3')->exists($data['HOMEPAGE_IMAGE_1'])){
										                                        $imgdata = \Storage::cloud()->url($data['HOMEPAGE_IMAGE_1']);      
										                                    }
										                                 @endphp

																		<img src="{{$imgdata}}" style="max-width: 200px" alt= >
																	@else
																		<img src="{{url('public/theme/images/no-image.png')}}" alt= >  
																	@endif
																</div>
																<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
																<div>
																	 <span class="btn default btn-file">
																		  <span class="fileinput-new"> Select Image </span>
																		  <span class="fileinput-exists"> Change </span>
																		  <input type="file" name="HOMEPAGE_IMAGE_1" > </span>
																	 <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
																</div>
														  </div>
													</div>
													</div>

													<div class="col-md-2">
										  			<div class="form-group">
														<div><label>Image 2 [1905*670]</label></div>
														  <div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="fileinput-new thumbnail" style="width: 200px;">
																	@if( isset($data['HOMEPAGE_IMAGE_2']) && $data['HOMEPAGE_IMAGE_2'] != '' )
																		@php
										                                    $imgdata="";
										                                    if(Storage::disk('s3')->exists($data['HOMEPAGE_IMAGE_2'])){
										                                        $imgdata = \Storage::cloud()->url($data['HOMEPAGE_IMAGE_2']);      
										                                    }
										                                 @endphp
										                                <img src="{{$imgdata}}" style="max-width: 200px" alt= >
																	@else
																		<img src="{{url('public/theme/images/no-image.png')}}" alt= >  
																	@endif
																</div>
																<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
																<div>
																	 <span class="btn default btn-file">
																		  <span class="fileinput-new"> Select Image </span>
																		  <span class="fileinput-exists"> Change </span>
																		  <input type="file" name="HOMEPAGE_IMAGE_2" > </span>
																	 <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
																</div>
														  </div>
													</div>
													</div>

													<div class="col-md-2">
										  			<div class="form-group">
														<div><label>Image 3 [1905*670]</label></div>
														  <div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="fileinput-new thumbnail" style="width: 200px;">
																	@if( isset($data['HOMEPAGE_IMAGE_3']) && $data['HOMEPAGE_IMAGE_3'] != '' )
																		@php
										                                    $imgdata="";
										                                    if(Storage::disk('s3')->exists($data['HOMEPAGE_IMAGE_3'])){
										                                        $imgdata = \Storage::cloud()->url($data['HOMEPAGE_IMAGE_3']);      
										                                    }
										                                 @endphp
										                                <img src="{{$imgdata}}" style="max-width: 200px" alt= >
																	@else
																		<img src="{{url('public/theme/images/no-image.png')}}" alt= >  
																	@endif
																</div>
																<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
																<div>
																	 <span class="btn default btn-file">
																		  <span class="fileinput-new"> Select Image </span>
																		  <span class="fileinput-exists"> Change </span>
																		  <input type="file" name="HOMEPAGE_IMAGE_3" > </span>
																	 <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
																</div>
														  </div>
													</div>
													</div>

													<div class="col-md-2">
										  			<div class="form-group">
														<div><label>Image 4 [1905*670]</label></div>
														  <div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="fileinput-new thumbnail" style="width: 200px;">
																	@if( isset($data['HOMEPAGE_IMAGE_4']) && $data['HOMEPAGE_IMAGE_4'] != '' )
																		@php
										                                    $imgdata="";
										                                    if(Storage::disk('s3')->exists($data['HOMEPAGE_IMAGE_4'])){
										                                        $imgdata = \Storage::cloud()->url($data['HOMEPAGE_IMAGE_4']);      
										                                    }
										                                 @endphp
										                                <img src="{{$imgdata}}" style="max-width: 200px" alt= >
																	@else
																		<img src="{{url('public/theme/images/no-image.png')}}" alt= >  
																	@endif
																</div>
																<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
																<div>
																	 <span class="btn default btn-file">
																		  <span class="fileinput-new"> Select Image </span>
																		  <span class="fileinput-exists"> Change </span>
																		  <input type="file" name="HOMEPAGE_IMAGE_4" > </span>
																	 <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
																</div>
														  </div>
													</div>
													</div>
													<div class="col-md-2">
										  			<div class="form-group">
														<div><label>Image 5 [1905*670]</label></div>
														  <div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="fileinput-new thumbnail" style="width: 200px;">
																	@if( isset($data['HOMEPAGE_IMAGE_5']) && $data['HOMEPAGE_IMAGE_5'] != '' )
																		@php
										                                    $imgdata="";
										                                    if(Storage::disk('s3')->exists($data['HOMEPAGE_IMAGE_5'])){
										                                        $imgdata = \Storage::cloud()->url($data['HOMEPAGE_IMAGE_5']);      
										                                    }
										                                 @endphp
										                                <img src="{{$imgdata}}" style="max-width: 200px" alt= >
																	@else
																		<img src="{{url('public/theme/images/no-image.png')}}" alt=>  
																	@endif
																</div>
																<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
																<div>
																	 <span class="btn default btn-file">
																		  <span class="fileinput-new"> Select Image </span>
																		  <span class="fileinput-exists"> Change </span>
																		  <input type="file" name="HOMEPAGE_IMAGE_5" > </span>
																	 <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
																</div>
														  </div>
													</div>
													</div>

													<div style="clear: both"></div>
													<div class="col-md-2">
													<div class="form-group">
														<div><label>I Want to Buy Image [555*371]</label></div>
														  <div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="fileinput-new thumbnail" style="width: 200px;">
																	@if( isset($data['I_WANT_BUY_IMAGE']) && $data['I_WANT_BUY_IMAGE'] != '' )
																		@php
										                                    $imgdata="";
										                                    if(Storage::disk('s3')->exists($data['I_WANT_BUY_IMAGE'])){
										                                        $imgdata = \Storage::cloud()->url($data['I_WANT_BUY_IMAGE']);      
										                                    }
										                                 @endphp
										                                <img src="{{$imgdata}}" style="max-width: 200px" alt= >
																	@else
																		<img src="{{url('public/theme/images/no-image.png')}}" alt=>  
																	@endif
																</div>
																<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
																<div>
																	 <span class="btn default btn-file">
																		  <span class="fileinput-new"> Select Image </span>
																		  <span class="fileinput-exists"> Change </span>
																		  <input type="file" name="I_WANT_BUY_IMAGE" > </span>
																	 <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
																</div>
														  </div>
													</div>
													</div>

													<div class="col-md-2">
													<div class="form-group">
														<div><label>I Want to Sell Image [555*371]</label></div>
														  <div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="fileinput-new thumbnail" style="width: 200px;">
																	@if( isset($data['I_WANT_SELL_IMAGE']) && $data['I_WANT_SELL_IMAGE'] != '' )
																		@php
										                                    $imgdata="";
										                                    if(Storage::disk('s3')->exists($data['I_WANT_SELL_IMAGE'])){
										                                        $imgdata = \Storage::cloud()->url($data['I_WANT_SELL_IMAGE']);      
										                                    }
										                                 @endphp
										                                <img src="{{$imgdata}}" style="max-width: 200px" alt= >
																	@else
																		<img src="{{url('public/theme/images/no-image.png')}}" alt=>  
																	@endif
																</div>
																<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
																<div>
																	 <span class="btn default btn-file">
																		  <span class="fileinput-new"> Select Image </span>
																		  <span class="fileinput-exists"> Change </span>
																		  <input type="file" name="I_WANT_SELL_IMAGE" > </span>
																	 <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
																</div>
														  </div>
													</div>
													</div>

													<div style="clear: both"></div>
													<div class="col-md-6">

														<div class="form-group">
															<label>I Want to Buy Text</label>
															<input type="text" name="I_WANT_BUY_TEXT" id="I_WANT_BUY_TEXT" class="form-control" value="{{$data['I_WANT_BUY_TEXT'] or ''}}" required data-parsley-trigger="keyup">
														</div>


													</div>

													<div class="col-md-6">

														<div class="form-group">
															<label>I Want to Sell Text</label>
															<input type="text" name="I_WANT_SELL_TEXT" id="I_WANT_SELL_TEXT" class="form-control" value="{{$data['I_WANT_SELL_TEXT'] or ''}}" required data-parsley-trigger="keyup">
														</div>

													</div>




												</div>	


										  		<a href="{{url('admin/sitesetting')}}">
												<button type="button" class="btn btn-warning">Cancel</button>
												</a>
												<button type="submit" class="btn btn-primary">Update</button>
												</form>

										  </div>
										<!-- /.tab-content -->
									</div>
							  <!-- nav-tabs-custom -->
								</div>
								<!-- /.col -->
						  	</div>

							<div class="clear:both"></div>                  
							<!-- <div class="row">
								<div class="col-md-12">
									<a href="{{url('admin/sitesetting')}}">
										<button type="button" class="btn btn-warning">Cancel</button>
									</a>
									<button type="submit" class="btn btn-primary">Update</button>
								</div>
							</div>  -->
						</form>
						<div class="box-footer">
						   Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@stop

@section('js')
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>

<script>
  $(function () {
	$('#adminlisttable').DataTable({
	  "paging": true,
	  
	});
  });

  $('#v_title').change(function() {
	
	var string = $('#v_title').val();
	var $v_key = '';
	var trimmed = $.trim(string);

	$v_key = trimmed.replace(/[^a-z0-9-]/gi, '-')
					.replace(/-+/g, '-')
					.replace(/^-|-$/g, '');

	$('#v_key').val($v_key.toLowerCase());

	return true;
});

function setHiddden(dFormate){
	var dFormate = dFormate;
	$("#SITE_DATE_FORMATE_PHP").val(dFormate);
}
</script>

@stop