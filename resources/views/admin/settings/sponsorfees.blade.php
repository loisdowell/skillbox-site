@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Sponsor Ad Fees
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('admin/sitesetting')}}">Settings</a></li>
        <li class="active">sponosr fees</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} new Sponsor Fee</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            
            <div class="box-body">
              <br/><br/>
                <div class="row">
                   <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/sponsorfees/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Number<span class="asterisk_input">*</span></label>
                          <input type="text" name="i_number" id="i_number" class="form-control" placeholder="Name" value="{{$data->i_number or old('i_number')}}" required="" data-parsley-trigger="keyup">
                      </div>
                      <div class="form-group">
                        <label>Type<span class="asterisk_input">*</span></label>
                        <select class="form-control" id="e_type" name="e_type" required="" data-parsley-trigger="keyup">
                          <option value="">- select -</option>
                           <option value="days" @if( isset($data->e_type) && $data->e_type == 'days' ) selected @endif>Days</option>
                            <option value="months" @if( isset($data->e_type ) && $data->e_type == 'months') SELECTED @endif >Months</option>
                            <option value="year" @if( isset($data->e_type ) && $data->e_type == 'year') SELECTED @endif >Year</option>
                        </select>
                      </div>

                       <div class="form-group">
                          <label>Price</label>
                          <input type="text" name="f_price" id="f_price" class="form-control" placeholder="Price" value="{{$data->f_price or old('f_price')}}">
                      </div>

                      <div class="form-group">
                          <label>Impression Limit</label>
                          <input type="text" name="i_impression_limit" id="i_impression_limit" class="form-control" placeholder="Impression Limit" value="{{$data->i_impression_limit or old('i_impression_limit')}}">
                      </div>

                    </div>
                    <div class="col-md-6">

                      <div class="form-group">
                          <label>Until Connected</label>
                          <select class="form-control" id="i_until_connected" name="i_until_connected">
                            <option value="">- select -</option>
                            <option value="1" @if( isset($data->i_until_connected) && $data->i_until_connected == '1' ) selected @endif>Yes</option>
                            <option value="0" @if( isset($data->i_until_connected ) && $data->i_until_connected == '0') SELECTED @endif >No</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label>Order</label>
                          <input type="text" name="i_order" id="i_order" class="form-control" placeholder="Order" value="{{$data->i_order or old('i_order')}}">
                      </div>
                      
                      <div class="form-group">
                          <label>Status<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="active" @if( isset($data->e_status) && $data->e_status == 'active' ) selected @endif>Active</option>
                            <option value="inactive" @if( isset($data->e_status ) && $data->e_status == 'inactive') SELECTED @endif >Inactive</option>
                          </select>
                      </div>

                  </div>
                  </div>
                  <div class="clear:both"></div>                
                        
                        <div class="row">
                          <div class="col-md-12">
                              <a href="{{url('admin/sponsorfees')}}">
                              <button type="button" class="btn btn-warning">Back to List</button>
                              </a>
                              <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                      </div> 
                    </div>
                    </form>
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>
          </div>
        </div>
      </section>      
              
    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <div class="box-header pull-right">
              <a href="{{url('admin/sponsorfees/add/0')}}">
                 <button type="submit" class="btn btn-info">Add Sponsor Ad Fee</button>
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Number</th>
                  <th>Type</th>
                  <th>Price</th>
                  <th>Impression Limit</th>
                  <th>Untill Connected</th>
                  <th>Order</th>
                  <th>Added Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      <td>{{ $value->i_number or ''}}</td>
                      <td>{{ $value->e_type or '' }} </td>
                      <td>{{ $value->f_price or '' }} </td>
                      <td>{{ $value->i_impression_limit or '' }} </td>
                      <td>{{ $value->i_until_connected == 1 ? 'Yes' : 'No' }} </td>
                      <td>{{ $value->i_order or '' }} </td> 
                      <td>{{ isset($value->d_added) ? date('M d Y' ,strtotime($value->d_added)) : '' }}</td>
                      <td>
                        <a href="{{url('admin/sponsorfees/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/sponsorfees',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if( $value->e_status == 'inactive' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                        @php $a=url('admin/sponsorfees/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','<?php echo $a ?>')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
</script>

@stop

