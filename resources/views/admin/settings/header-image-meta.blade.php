@extends('layouts.master')

@section('title')
	{{$section or 'Admin'}}
@stop

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">

table.table-bordered thead tr{border: 1px solid #000 !important; }
table.table-bordered tbody th, table.table-bordered tbody td{
border: 1px solid #ebebeb;
}
.table-bordered{
	border: 1px solid #ddd !important;
}
.box-title {
    display: inline-block;
    font-size: 29px !important;
    line-height: 1;
    margin: 0;
}
.asterisk_input{
	color: #e32;
}
</style>
@stop

@section('model')
@stop

@section('content')
	
	<div class="content-wrapper">
        <section class="content-header">
	      <h1>
	        Meta Management
	        <small></small>
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="{{url('admin/sitesetting')}}">Settings</a></li>
	        <li class="active">Meta</li>
	      </ol>
	    </section>

    
    @if($view=="add" || $view=="edit")
    	<div class="col-xs-12">	      	
    	<div style="clear: both"></div>
      		@if ($success = Session::get('success'))
	          <div class="alert alert-success alert-dismissable">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
	          </div>
	        @endif
	        @if ($warning = Session::get('warning'))
	          <div class="alert alert-danger alert-dismissable">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
	          </div>
	        @endif

		</div>	
		<div style="clear: both"></div>		
    	<section class="content">
	   		
	   		<div class="box box-default">
		        <div class="box-header with-border">
	                <h3 class="box-title">{{ucfirst($view)}} new Meta</h3>
	                <div class="box-tools pull-right">
	                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
	                </div>
	            </div>
		        
		        <div class="box-body">
		         	<br/><br/>
		          	<div class="row">
		           
					<form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/manage-meta-field/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
						{{ csrf_field() }}
						
						<div class="col-md-6">
							<div class="form-group">
			                	<label>Title<span class="asterisk_input">*</span></label>
			                	<input type="text" name="v_title" id="v_title" class="form-control" placeholder="Title" value="{{$data->v_title or old('v_title')}}" required="" data-parsley-trigger="keyup">
			              	</div>
			            </div>

			            <div class="col-md-6">
			            	<div class="form-group">
			                	<label>Slug<span class="asterisk_input">*</span></label>
			                	<input type="text" name="v_key" id="v_key" class="form-control" placeholder="Slug" value="{{$data->v_key or old('v_key')}}" required="" data-parsley-trigger="keyup" @if($view=="edit") readonly @endif >
			              	</div>
			            </div>

			           <div class="col-md-12">
			            
			              	<div class="form-group">
			                	<label>Sub headline<span class="asterisk_input">*</span></label>
			                	<input type="text" name="v_sub_headline" id="v_sub_headline" class="form-control" placeholder="Sub headline" value="{{$data->v_sub_headline or old('v_sub_headline')}}" required="" data-parsley-trigger="keyup">
			              	</div>
			              	<div class="form-group">
								<div><label>Image<span class="asterisk_input">*</span></label></div>
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-new thumbnail" style="width: 200px;">
										@if(isset($data->v_image) && $data->v_image != '' )
		                                 @php
		                                    $imgdata="";
		                                    if(Storage::disk('s3')->exists($data->v_image)){
		                                        $imgdata = \Storage::cloud()->url($data->v_image);      
		                                    }
		                                 @endphp 
		                                <img src="{{$imgdata}}" style="max-width: 150px" alt="" />
		                              @else
		                                 @php
		                                    $imgdata = \Storage::cloud()->url('common/no-image.png');      
		                                 @endphp 
		                                <img src="{{$imgdata}}" alt="" />  
		                              @endif
									</div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
									<div>
										 <span class="btn default btn-file">
											  <span class="fileinput-new"> Select Image </span>
											  <span class="fileinput-exists"> Change </span>
											  <input type="file" name="v_image" > </span>
										 <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
									</div>
								</div>
							</div>
			              	<div class="form-group">
			                	<label>Meta Title<span class="asterisk_input">*</span></label>
			                 	<input type="text" name="v_meta_title" id="v_meta_title" class="form-control" placeholder="Name" value="{{$data->v_meta_title or old('v_meta_title')}}" required="" data-parsley-trigger="keyup">
			              	</div>
			           
			              	<div class="form-group">
			                	<label>Meta Keywords<span class="asterisk_input">*</span></label>
			                 	<textarea name="v_meta_keywords" rows="4" id="v_meta_keywords" class="form-control" placeholder="Description" required="" data-parsley-trigger="keyup">{{$data->v_meta_keywords or old('v_meta_keywords')}}</textarea>
			              	</div>
			           
			              	<div class="form-group">
			                	<label>Meta Description<span class="asterisk_input">*</span></label>
			                 	<textarea name="l_meta_description" rows="4" id="l_meta_description" class="form-control" placeholder="Description" required="" data-parsley-trigger="keyup">{{$data->l_meta_description or old('l_meta_description')}}</textarea>
			              	</div>
				            
			           </div>
		           	</div>
					<div class="clear:both"></div>		          	
		          	
		          	<div class="row">
			          	<div class="col-md-12">
			                <a href="{{url('admin/manage-meta-field')}}">
			                <button type="button" class="btn btn-warning">Back to List</button>
			                </a>
			                <button type="submit" class="btn btn-primary">Save</button>
			            </div>
			        </div> 
		        </div>
		        </form>
		        <div class="box-footer">
		           Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
		        </div>
		      </div>
		    </div>
	    </section>      
	            
    @else
	    <section class="content">
	      <div class="row">	
		    <div class="col-xs-12">
		    	<div style="clear: both"></div>
			        @if ($success = Session::get('success'))
			        <div class="alert alert-success alert-dismissable">
			          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
			        </div>
			      @endif
			      @if ($warning = Session::get('warning'))
			        <div class="alert alert-danger alert-dismissable">
			          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
			        </div>
			      @endif
	          <div class="box">
	            
	           {{--  <div class="box-header">
	              <h2 class="box-title"><b>List of Meta</b></h2>
	            </div> --}}

	            <div class="box-header pull-right">
					<a href="{{url('admin/manage-meta-field/add/0')}}">
					<button type="submit" class="btn btn-info">Add New Meta</button>
					</a>
				</div>
	            <div style="clear: both;"></div>
	            

	            <div class="box-body" >
	              <table id="adminlisttable" class="table table-striped table-bordered table-hover order-column dataTable ">
	                <thead>
		                <tr>
		                  	<th>Image</th>
		                  	<th>Title</th>
							<th>Status</th>
							<th>Added Date</th>
							<th>Actions</th>
		                </tr>
	                </thead>

	                <tbody>
	                	@if( isset($data) && count($data) )
							@foreach( $data as $count => $value )
								<tr>
									<td> 
										@if(isset($value->v_image) && $value->v_image != '' )
				                          @php
				                            $imgdata="";
				                            if(Storage::disk('s3')->exists($value->v_image)){
				                                $imgdata = \Storage::cloud()->url($value->v_image);      
				                            }
				                          @endphp 
				                          <img src="{{$imgdata}}" height="50" width="50" alt="" />
				                        @else
				                           @php
				                              $imgdata = \Storage::cloud()->url('common/no-image.png');      
				                           @endphp 
				                          <img src = "{{$imgdata}}" height="50" width="50" alt="" />  
				                        @endif</td>
									<td>{{$value->v_title or ''}}</td>
									@if( isset($value->e_status) && $value->e_status != '' )
										<td>{{ isset($value->e_status) ? ( $value->e_status == 'inactive' ? 'Pending' : ucfirst($value->e_status) ) : '' }}</td>
									@else
										<td></td>
									@endif
									<td>{{ isset($value->d_added) ? date('M d Y' ,strtotime($value->d_added)) : '' }}</td>
									<td>
										<a href="{{url('admin/manage-meta-field/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
										<a href="{{url('admin/manage-meta-field',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
											@if( $value->e_status == 'inactive' )
												<i title="Activate" class="fa fa-check"></i>
											@else
												<i title="Inactivate" class="fa fa-times"></i>
											@endif
										</a>
										@php $a=url('admin/manage-meta-field/action/delete/').'/'.$value->id; @endphp
										<a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','<?php echo $a ?>')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>

									</td>
								</tr>
							@endforeach
						
						@else
							<tr>
								<td colspan="6" align="center">@lang('message.common.noRecords')</td>
							</tr>
						@endif
	                
	                </tbody>
	                
	              </table>
	            </div>
	          </div>
	        </div>
	      </div>
	    </section>
    @endif


  </div>


@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>

<script>
  $(function () {
    $('#adminlisttable').DataTable({
      "paging": true,
      
    });
  });

 	//$('#v_title').change(function() {
  	// var string = $('#v_title').val();
	// var $v_key = '';
	// var trimmed = $.trim(string);
	// $v_key = trimmed.replace(/[^a-z0-9-]/gi, '-')
	// 				.replace(/-+/g, '-')
	// 				.replace(/^-|-$/g, '');
	// $('#v_key').val($v_key.toLowerCase());

	// return true;
	// });


</script>

@stop
