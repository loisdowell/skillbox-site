@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
.bootstrap-tagsinput{
  width: 100% !important;
}

</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Courses
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">courses</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} new Course</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            
            <div class="box-body">
              <br/><br/>
                <div class="row">
                   
                   <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/courses/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}

                    <div class="col-md-4">
                        
                        <div class="form-group">
                          <label>Title<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_title" id="v_title" class="form-control" placeholder="Title" value="{{$data->v_title or old('v_title')}}" required="" data-parsley-trigger="keyup">
                        </div>

                        <div class="form-group">
                          <label>Select User</label>
                          <select class="form-control" id="i_user_id" name="i_user_id">
                            <option value="">-select-</option>
                            @if( isset($usersLists) && count($usersLists) )
                              @foreach($usersLists as $key => $val)
                                <option value="{{$val->_id}}" @if( isset($data->i_user_id) && $data->i_user_id == $val->_id) selected @endif >{{$val->v_fname}} {{$val->v_lname}}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>

                        <div class="form-group">
                          <label>Select Level</label>
                          <select class="form-control" id="i_level_id" name="i_level_id">
                            <option value="">-select-</option>
                            @if( isset($coursesLevel) && count($coursesLevel) )
                              @foreach($coursesLevel as $key => $val)
                                <option value="{{$val->_id}}" @if( isset($data->i_level_id) && $data->i_level_id == $val->_id) selected @endif >{{$val->v_title}}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        
                        <div class="form-group">
                          <label>Subtitle<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_sub_title" id="v_sub_title" class="form-control" placeholder="Title" value="{{$data->v_sub_title or old('v_sub_title')}}" required="" data-parsley-trigger="keyup">
                        </div>

                        <div class="form-group">
                          <label>Select Courses Language</label>
                          <select class="form-control" id="i_language_id" name="i_language_id">
                            <option value="">-select-</option>
                            @if( isset($coursesLanguage) && count($coursesLanguage) )
                              @foreach($coursesLanguage as $key => $val)
                                <option value="{{$val->_id}}" @if( isset($data->i_language_id) && $data->i_language_id == $val->_id) selected @endif >{{$val->v_title}}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>

                        <div class="form-group">
                          <label>Courses Total Duration<span class="asterisk_input">*</span></label>
                          <input type="text" name="i_duration" id="i_duration" class="form-control" placeholder="Title" value="{{$data->i_duration or old('i_duration')}}" required="" data-parsley-trigger="keyup">
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="form-group">
                          <label>Author Name<span class="asterisk_input">*</span></label>
                          <input type="text" name="v_author_name" id="v_author_name" class="form-control" placeholder="Title" value="{{$data->v_author_name or old('v_author_name')}}" required="" data-parsley-trigger="keyup">
                        </div>

                        <div class="form-group">
                          <label>Select Courses Category</label>
                          <select class="form-control" id="i_category_id" name="i_category_id">
                            <option value="">-select-</option>
                            @if( isset($coursesCategory) && count($coursesCategory) )
                              @foreach($coursesCategory as $key => $val)
                                <option value="{{$val->_id}}" @if( isset($data->i_category_id) && $data->i_category_id == $val->_id) selected @endif >{{$val->v_title}}</option>
                              @endforeach
                            @endif
                          </select>
                        </div>


                    </div>
                    <div style="clear: both"></div>

                    <div class="col-md-6">
                        
                        <div class="form-group">
                            <label>Courses Requirements</label>
                            <textarea name="v_requirement" id="v_requirement" class="form-control" required="" rows="7" data-parsley-id="9363">{{$data->v_requirement or old('v_requirement')}}</textarea>
                        </div>

                    </div>

                    <div class="col-md-6">
                        
                        <div class="form-group">
                            <label>Courses Description</label>
                            <textarea name="l_description" id="l_description" class="form-control" required="" rows="7" data-parsley-id="9363">{{$data->l_description or old('l_description')}}</textarea>
                        </div>                          

                    </div>

                    <div style="clear: both"></div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Instructor Details</label>
                            <textarea name="l_instructor_details" id="l_instructor_details" class="form-control" required="" rows="7" data-parsley-id="9363">{{$data->l_instructor_details or old('l_instructor_details')}}</textarea>
                        </div>                          
                    </div>
                    <div style="clear: both"></div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <div><label>Instructor Profile </label></div>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                             <div class="fileinput-new thumbnail" style="width: 200px;">
                             
                              @if(isset($data->v_instructor_image) && $data->v_instructor_image != '' )
                                 @php
                                    $imgdata="";
                                    if(Storage::disk('s3')->exists($data->v_instructor_image)){
                                        $imgdata = \Storage::cloud()->url($data->v_instructor_image);      
                                    }
                                 @endphp 
                                <img src="{{$imgdata}}" style="max-width: 150px" alt="" />
                              @else
                                 @php
                                    $imgdata = \Storage::cloud()->url('common/no-image.png');      
                                 @endphp 
                                <img src="{{$imgdata}}" alt="" />  
                              @endif

                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> 
                            </div>
                            <div>
                               <span class="btn default btn-file">
                                  <span class="fileinput-new"> Select Image </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="file" name="v_instructor_image" @if($view!="edit") required @endif> </span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <div><label>Course Thumbnail Image</label></div>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                             <div class="fileinput-new thumbnail" style="width: 200px;">
                             
                              @if(isset($data->v_course_thumbnail) && $data->v_course_thumbnail != '' )
                                 @php
                                    $imgdata="";
                                    if(Storage::disk('s3')->exists($data->v_course_thumbnail)){
                                        $imgdata = \Storage::cloud()->url($data->v_course_thumbnail);      
                                    }
                                 @endphp 
                                <img src="{{$imgdata}}" style="max-width: 150px" alt="" />
                              @else
                                 @php
                                    $imgdata = \Storage::cloud()->url('common/no-image.png');      
                                 @endphp 
                                <img src="{{$imgdata}}" alt="" />  
                              @endif

                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> 
                            </div>
                            <div>
                               <span class="btn default btn-file">
                                  <span class="fileinput-new"> Select Image </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="file" name="v_course_thumbnail" @if($view!="edit") required @endif> </span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                      </div>
                    </div>
                   
                    <div class="col-md-4">
                    <div class="form-group">
                        <div><label> Upload Course Introduction Video</label></div>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                             <div class="fileinput-new thumbnail" style="width: 200px;">
                             
                              @if(isset($data->l_video) && $data->l_video != '' )
                                 @php
                                    $videodata="";
                                    if(Storage::disk('s3')->exists($data->l_video)){
                                        $videodata = \Storage::cloud()->url($data->l_video);      
                                    }
                                 @endphp 
                                 <video width="300" height="150" controls>
                                      <source src="{{$videodata}}" type="video/mp4">
                                    </video> 
                              @else
                                 @php
                                    $imgdata = \Storage::cloud()->url('common/no-image.png');      
                                 @endphp 
                                <img src="{{$imgdata}}" alt="" />  
                              @endif
                            </div>
                            
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> 
                            </div>
                            <div>
                               <span class="btn default btn-file">
                                  <span class="fileinput-new">Select Video</span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="file" name="l_video" @if($view!="edit") required @endif> </span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                      </div>
                    </div>

                    
                    <div style="clear: both"></div>

                    {{-- <div class="col-md-6">
                        @if ($view=="edit")
                          <div class="form-group">
                            <div><label>Instuctor Image</label></div>
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px;">
                                  @if( isset($data->v_instructor_image) && $data->v_instructor_image != '' )
                                    @php
                                        $imgdata = \Storage::cloud()->url($data->v_instructor_image);    
                                    @endphp  
                                    <img src="{{$imgdata}}" style="max-width: 150px" alt="" />
                                  @else
                                    <img src="{{url('public/theme/images/no-image.png')}}" alt="" />  
                                  @endif
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                                <div>
                                   <span class="btn default btn-file">
                                      <span class="fileinput-new"> Select Image </span>
                                      <span class="fileinput-exists"> Change </span>
                                      <input type="file" name="v_instructor_image" > </span>
                                   <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                              </div>
                          </div>
                          @else
                          <div class="form-group">
                            <div><label>Instuctor Image</label></div>
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                                <div>
                                   <span class="btn default btn-file">
                                      <span class="fileinput-new"> Select Image </span>
                                      <span class="fileinput-exists"> Change </span>
                                      <input type="file" name="v_instructor_image" > </span>
                                   <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                              </div>
                          </div>
                        @endif  
                    </div>
                    <div class="col-md-6">
                       
                        @if ($view=="edit")
                          <div class="form-group">
                            <div><label>Video</label></div>
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px;">
                                  @if(isset($data->l_video) && $data->l_video != '' )
                                    @php
                                        $videodata = \Storage::cloud()->url($data->l_video);    
                                    @endphp  
                                     <video width="400" height="200" controls>
                                      <source src="{{$videodata}}" type="video/mp4">
                                    </video>
                                  @else
                                    <img src="{{url('public/theme/images/no-image.png')}}" alt="" />  
                                  @endif
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                                <div>
                                   <span class="btn default btn-file">
                                      <span class="fileinput-new"> Select Video </span>
                                      <span class="fileinput-exists"> Change </span>
                                      <input type="file" name="l_video" > </span>
                                   <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                              </div>
                          </div>

                          @else
                          <div class="form-group">
                            <div><label>Video</label></div>
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                                <div>
                                   <span class="btn default btn-file">
                                      <span class="fileinput-new"> Select Video </span>
                                      <span class="fileinput-exists"> Change </span>
                                      <input type="file" name="l_video" > </span>
                                   <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                              </div>
                          </div>
                        @endif
                    </div> --}}


                     <div class="col-md-12">
                                 <div class="buttet-concept">
                                        <div class="name-details">
                                            <div class="searching-topspace">
                                                <label class="field-name">
                                                    Add search tags
                                                </label>
                                            </div>
                                        </div>
                                        <div class="name-details">
                                            <label class="subfield-name">
                                                Please add up to 3 search tags related to your service or job.
                                            </label>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="input-login">
                                                            <input class="form-control business-name-control" type="text" placeholder="Start typing ...." id="temp_text">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2 terms-add" id="hiddentags">
                                                        @if(isset($data->v_search_tags) && count($data->v_search_tags))
                                                            @foreach($data->v_search_tags as $k=>$v)
                                                                <input type="hidden" id="hidden-{{$v}}" name="v_search_tags[]" value="{{$v}}">
                                                            @endforeach
                                                        @endif    

                                                        <div class="login-btn" id="addtagbutton" @if(isset($data->v_search_tags) && count($data->v_search_tags)>=3)  style="display: none" @endif>
                                                            <button type="button" class="btn add-now" onclick="myFunction()">Add</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12" style="margin-top: 15px">
                                                        <div class="selection-point" id="add_text">
                                                            @if(isset($data->v_search_tags) && count($data->v_search_tags))
                                                                @foreach($data->v_search_tags as $k=>$v)
                                                            <button class='auto-width' id='{{$v}}' onclick='removeButton(this.id)'><span>{{$v}}</span><img src="{{url('public/Assets/frontend/images/close1.png')}}"></button>
                                                                @endforeach
                                                            @endif    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                              </div>  

                    <div style="clear: both"></div>
                    @php

                      $v_search_tags="";
                      if(isset($data->v_search_tags) && count($data->v_search_tags)){
                          $v_search_tags=implode(",", $data->v_search_tags);
                      }
                    @endphp

                    
                   {{--  <div class="col-md-11" style="padding-right:0px; ">              
                      <label>Search tags</label>
                      <div class="form-group">
                          <input data-role="tagsinput" class="form-control" id="v_search_tags" name="v_search_tags" type="text" value="{{$v_search_tags}}" placeholder="Start Typing ...." style="width: 100%" >
                          <span class="pull-left">Max 3 tags are allowed per courses. </span>
                      </div>
                    </div>
                    
                    <div class="col-md-1" style="padding: 0px;">
                      <label>&nbsp;</label>
                      <div class="form-group">
                        <button class="btn" type="button">Add</button>
                      </div>
                    </div>                                              --}} 

                    <div style="clear: both"></div>
                    <div class="col-md-12" style="margin-top: 10px">
                        <label>Courses Price<span class="asterisk_input">*</span></label>
                        <div class="form-group">
                        <input type="number" step="any" name="f_price" id="f_price" class="form-control" placeholder="Title" value="{{$data->f_price or old('f_price')}}" required="" data-parsley-trigger="keyup">
                        </div>
                    </div>      

                    <div style="clear: both"></div>
                    
                    <hr/>
                    <div class="col-md-12">
                        <h3>Courses Section</h3>    
                    </div>
                    <hr/>

                    @if($view=="edit")

                        @if(isset($data->section) && count($data->section))
                            @foreach($data->section as $key=>$val)

                              <div class="sectiondata">
                                  <div class="col-md-6">
                                      <label>Section 1 Title</label>
                                        <div class="form-group">
                                          <input type="text" name="section[{{$key}}][v_title]" class="form-control" required="" value="{{$val['v_title']}}">
                                        </div>
                                  </div>  
                                  <input type="hidden" name="section[{{$key}}][_id]" value="{{$val['_id']}}">
                                  <div style="clear: both"></div> 
                                  
                                  @if(isset($val['video']) && count($val['video']))
                                    @foreach($val['video'] as $k=>$v)
                                        <div class="videoData sectionvideoremove"> @if($k>0)<hr/>@endif
                                            <div class="col-md-6">
                                                <label>Section 1 Video Title</label>
                                                  <div class="form-group">
                                                    <input type="text" name="section[{{$key}}][video][{{$k}}][v_title]" class="form-control" required="" value="{{$v['v_title'] or ''}}">
                                                  </div>
                                            </div>
                                            <input type="hidden" name="section[{{$key}}][video][{{$k}}][_id]" value="{{$v['_id']}}">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <div class="open-browser">
                                                      <label class="field-name">
                                                          Section 1 Video
                                                      </label>
                                                      <input type="text" class="form-control input-field" id="videotexte{{$key}}{{$k}}" disabled="" value="{{$v['v_video'] or ''}}">
                                                      <div class="text-right">
                                                          <input type="file" accept=".mp4, .mkv, .avi, .mov" name="section[{{$key}}][video][{{$k}}][v_video]"  id="videouploade{{$key}}{{$k}}" style="display: none" onchange="sectionvideo('e{{$key}}{{$k}}');">
                                                          <label id="fileupload" class="btn-editdetail" for="videouploade{{$key}}{{$k}}">Browser</label>
                                                      </div>
                                                  </div>
                                                </div>
                                            </div>
                                            <div style="clear: both"></div> 
                                            <div class="col-md-2">
                                                <label>Video duration Minutes</label>
                                                  <div class="form-group">
                                                    <input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="section[{{$key}}][video][{{$k}}][v_duration_min]" class="form-control" required="" value="{{$v['v_duration_min'] or ''}}">
                                                  </div>
                                            </div>

                                            <div class="col-md-2">
                                                <label>Video duration Seconds</label>
                                                <div class="form-group">
                                                  <input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="section[{{$key}}][video][{{$k}}][v_duration_sec]" class="form-control" required="" value="{{$v['v_duration_sec'] or ''}}">
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <label>Preview this course</label>
                                                <div class="form-group">
                                                  <select name="section[{{$key}}][video][{{$k}}][i_preview]" class="form-control" required="" value="">
                                                    <option value="off" @if($v['i_preview']=="off") selected @endif >Off</option>
                                                    <option value="on" @if($v['i_preview']=="on") selected @endif>On</option>
                                                  </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <div class="open-browser">
                                                      <label class="field-name">
                                                           Section 1 Doc
                                                      </label>
                                                      <input type="text" class="form-control input-field" id="doctexte{{$key}}{{$k}}" disabled="" value="{{$v['v_doc'] or ''}}">
                                                      <div class="text-right">
                                                          <input type="file" id="docuploade{{$key}}{{$k}}" name="section[{{$key}}][video][{{$k}}][v_doc]" style="display: none" onchange="sectiondoc('e{{$key}}{{$k}}');" multiple="">
                                                          <label id="fileupload" class="btn-editdetail" for="docuploade{{$key}}{{$k}}">Browser</label>
                                                      </div>
                                                  </div>
                                                </div>
                                            </div>
                                            @if($k>0)
                                            <div class="col-md-12 ">
                                              <div class="form-group pull-right">
                                              <button type="button" class="btn btn-danger remsectionvideo">Remove Video</button>
                                              </div>
                                            </div>  
                                            @endif
                                        </div>

                                    @endforeach
                                  @endif      

                                  <div style="clear: both"></div> 
                                  <div class="moreVideo1" id="moreVideo1">
                                  
                                  </div>
                                 
                                  <input type="hidden" id="videouploadcnt1" value="1">
                                  
                                  <div class="col-md-12 ">
                                      <div class="form-group pull-right">
                                        <button type="button" class="btn btn-info" onclick="addmoreVideo('1')">Add New Video</button>
                                      </div>
                                  </div>  
                              </div>

                            @endforeach
                        @endif    

                    @else

                    <div class="sectiondata">
                        
                        <div class="col-md-6">
                            <label>Section 1 Title</label>
                              <div class="form-group">
                                <input type="text" name="section[0][v_title]" class="form-control" required="" value="">
                              </div>
                        </div>  
                        <div style="clear: both"></div> 
                        <div class="videoData">
                            <div class="col-md-6">
                                <label>Section 1 Video Title</label>
                                  <div class="form-group">
                                    <input type="text" name="section[0][video][0][v_title]" class="form-control" required="" value="">
                                  </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <div class="open-browser">
                                      <label class="field-name">
                                          Section 1 Video
                                      </label>
                                      <input type="text" class="form-control input-field" id="videotext1" disabled="">
                                      <div class="text-right">
                                          <input type="file" accept=".mp4, .mkv, .avi, .mov" name="section[0][video][0][v_video]" required id="videoupload1" style="display: none" onchange="sectionvideo('1');">
                                          <label id="fileupload" class="btn-editdetail" for="videoupload1">Browser</label>
                                      </div>
                                  </div>
                                </div>
                            </div>
                            <div style="clear: both"></div> 
                            <div class="col-md-2">
                                <label>Video duration Minutes</label>
                                  <div class="form-group">
                                    <input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="section[0][video][0][v_duration_min]" class="form-control" required="" value="">
                                  </div>
                            </div>

                            <div class="col-md-2">
                                <label>Video duration Seconds</label>
                                <div class="form-group">
                                  <input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="section[0][video][0][v_duration_sec]" class="form-control" required="" value="00">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <label>Preview this course</label>
                                <div class="form-group">
                                  <select name="section[0][video][0][i_preview]" class="form-control" required="" value="">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                  </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <div class="open-browser">
                                      <label class="field-name">
                                           Section 1 Doc
                                      </label>
                                      <input type="text" class="form-control input-field" id="doctext1" disabled="">
                                      <div class="text-right">
                                          <input type="file" id="docupload1" name="section[0][video][0][v_doc]" style="display: none" onchange="sectiondoc('1');" multiple="">
                                          <label id="fileupload" class="btn-editdetail" for="docupload1">Browser</label>
                                      </div>
                                  </div>
                                </div>
                            </div>
                        </div>

                        <div style="clear: both"></div> 
                        <div class="moreVideo1" id="moreVideo1">
                        
                        </div>

                        <input type="hidden" id="videouploadcnt1" value="1">
                        <div class="col-md-12 ">
                            <div class="form-group pull-right">
                              <button type="button" class="btn btn-info" onclick="addmoreVideo('1')">Add New Video</button>
                            </div>
                        </div>  
                    </div>
                    
                    @endif

                    <div style="clear: both"></div>   
                    <div class="moreSection" id="moreSection">
                    </div>
                    
                    
                    
                    @if($view=="edit")
                    @php
                        $sectioncnt=1;
                        if(isset($data->section) && count($data->section)){
                          $sectioncnt=count($data->section);
                        }
                    @endphp  
                    <input type="hidden" id="sectioncnt" value="{{$sectioncnt}}">
                    <div class="col-md-12 ">
                      <div class="form-group pull-right">
                        <button type="button" class="btn btn-info" onclick="addmoreSection('{{$sectioncnt}}')">Add New Section</button>
                      </div>
                    </div>  

                    @else
                    <input type="hidden" id="sectioncnt" value="1">
                    <div class="col-md-12 ">
                      <div class="form-group pull-right">
                        <button type="button" class="btn btn-info" onclick="addmoreSection('1')">Add New Section</button>
                      </div>
                    </div>  

                    @endif

                    
                    <script type="text/javascript">
                      function sectionvideo(data){
                          var filename = $('#videoupload'+data).val().split('\\').pop();
                          var lastIndex = filename.lastIndexOf("\\");   
                          $('#videotext'+data).val(filename);
                      }

                      function sectiondoc(data){
                          var filename = $('#docupload'+data).val().split('\\').pop();
                          var lastIndex = filename.lastIndexOf("\\");   
                          $('#doctext'+data).val(filename);
                      }
                    </script>

                    <div style="clear: both"></div>
                    <br>
                   {{--  <div class="col-md-12">
                        <h3>Courses Section</h3>    
                    </div>
                    <div style="clear: both"></div> --}}
                    


                    <?php /*
                    @if($view=="edit")
                        
                        @if(isset($data->section) && count($data->section))
                            @foreach($data->section as $key=>$val)
                                
                                <div id="coursessectiondiv" class="removesectionparents" style="border: 2px solid #222d32;margin-top: 30px">
                          
                                  @if($key>=1)
                                  <div class="col-md-12">
                                      <div class="form-group">
                                          <button type="button" class="btn btn-danger pull-right removesection">Remove</button>
                                      </div>
                                  </div>
                                  @endif
                                  
                                  <div class="col-md-12">
                                        <label>Section {{$key+1}} Title</label>
                                        <div class="form-group">
                                          <input type="text" name="section[{{$key}}][v_title]" class="form-control" required="" value="{{$val['v_title']}}">
                                        </div>
                                  </div>
                                  
                                  @if(isset($val['video']['v_title']) && count($val['video']['v_title']))          
                                    @foreach($val['video']['v_title'] as $k=>$v)

                                      <div class="removeeditvideo">
                                          <div class="col-md-3">
                                              <label>Section {{$key+1}} Video Title</label>
                                              <div class="form-group">
                                                <input type="text" name="section[{{$key}}][video][v_title][{{$k}}]" class="form-control" required="" value="{{$v}}" >
                                              </div>
                                          </div>
                                          <div class="col-md-3">
                                              <label>Section {{$key+1}} Video</label>
                                              <div class="form-group">
                                                <input type="file" name="section[{{$key}}][video][v_video][{{$k}}]" class="form-control">
                                              </div>
                                          </div>
                                          <div class="col-md-2">
                                              <label>&nbsp;</label>
                                              <div class="form-group">
                                                <input type="hidden" name="section[{{$key}}][video][i_preview][{{$k}}]" value="off">
                                                <input type="checkbox" name="section[{{$key}}][video][i_preview][{{$k}}]" @if(isset($val['video']['i_preview'][$key]) && $val['video']['i_preview'][$key]=="on") checked @endif style="margin-top:12px"><span style="margin-top: -2px"> &nbsp; Preview this video</span> 
                                              </div>
                                          </div>
                                          <div class="col-md-2">
                                              <div class="form-group">
                                                
                                                @php
                                                    $videodata="";
                                                    if(isset($val['video']['v_video'][$k])){
                                                      $videodata = \Storage::cloud()->url($val['video']['v_video'][$k]);      
                                                    }
                                                @endphp  
                                                 <video width="100" height="100" controls>
                                                  <source src="{{$videodata}}" type="video/mp4">
                                                </video>
                                              </div>
                                          </div>
                                          @if($k<1)
                                            <div class="col-md-2">
                                              <label>&nbsp;</label>
                                              <div class="form-group">
                                                <button class="btn btn-primary" type="button" onclick="addmorevideo('{{$key+1}}')">Add more video</button>
                                              </div>
                                            </div>
                                          @else
                                            <div class="col-md-2">
                                              <label>&nbsp;</label>
                                              <div class="form-group">
                                              <button type="button"class="btn btn-danger remedit">Remove</button>
                                            </div>
                                            </div>
                                          @endif
                                      </div>
                                      <div style="clear: both"></div>
                                    @endforeach
                                  @endif
                                  
                                  <div id="sectionvideo{{$key+1}}">
                                  </div>

                                  @if(isset($val['doc']['v_title']) && count($val['doc']['v_title']))          
                                    @foreach($val['doc']['v_title'] as $k=>$v)
                                        <div class="col-md-3">
                                            <label>Section {{$key+1}} Doc Title</label>
                                            <div class="form-group">
                                              <input type="text" name="section[{{$key}}][doc][v_title][{{$k}}]" class="form-control" required="" value="{{$v}}">
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <label>Section {{$key+1}} Doc</label>
                                            <div class="form-group">
                                              <input type="file" name="section[{{$key}}][doc][v_doc][{{$k}}]" class="form-control">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                         <label>&nbsp;</label>
                                            <div class="form-group">
                                              @php
                                                    $imgdata="";
                                                    if(isset($val['doc']['v_doc'][$k])){
                                                      $imgdata = \Storage::cloud()->url($val['doc']['v_doc'][$k]);      
                                                    }
                                                @endphp  
                                              <a href="{{$imgdata}}">{{$v}}</a>
                                            </div>
                                        </div>

                                        @if($k<1)
                                        <div class="col-md-3">
                                            <label>&nbsp;</label>
                                            <div class="form-group">
                                              <button class="btn btn-primary" type="button" onclick="addmoredoc('{{$key+1}}')" >Add more Doc</button>
                                            </div>
                                        </div>
                                        @else
                                        <div class="col-md-2">
                                          <label>&nbsp;</label>
                                          <div class="form-group">
                                          <button type="button"class="btn btn-danger rem">Remove</button>
                                        </div>
                                        </div>
                                        @endif
                                        <div style="clear: both"></div>
                                    @endforeach
                                  @endif
                                  <div id="sectiondoc{{$key+1}}">
                                  </div>
                              </div>
                            @endforeach
                        @endif    
                        <input type="hidden" id="sectioncnt" value="{{count($data->section)}}">
                    @endif
                    */ ?>

                    <?php /*
                    @if($view=="add")
                    <div id="coursessectiondiv"  style="border: 2px solid #222d32;">
                        
                        <div class="col-md-12">
                              <label>Section 1 Title</label>
                              <div class="form-group">
                                <input type="text" name="section[0][v_title]" class="form-control" required="" data-parsley-trigger="keyup">
                              </div>
                        </div>  
                        
                        <div class="col-md-3">
                            <label>Section 1 Video Title</label>
                            <div class="form-group">
                              <input type="text" name="section[0][video][v_title][0]" class="form-control" required="" data-parsley-trigger="keyup">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>Section 1 Video</label>
                            <div class="form-group">
                              <input type="file" name="section[0][video][v_video][0]" class="form-control" required="" data-parsley-trigger="keyup">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>&nbsp;</label>
                            <div class="form-group">
                              <input type="checkbox" name="section[0][video][i_preview][0]" style="margin-top:12px"><span style="margin-top: -2px"> &nbsp; Preview this video</span> 
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <div class="form-group">
                              <button class="btn btn-primary" type="button" onclick="addmorevideo('1')">Add more video</button>
                            </div>
                        </div>
                        <div style="clear: both"></div>
                        <div id="sectionvideo1">
                        </div>


                        <div class="col-md-3">
                            <label>Section 1 Doc Title</label>
                            <div class="form-group">
                              <input type="text" name="section[0][doc][v_title][0]" class="form-control" required="" data-parsley-trigger="keyup">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label>Section 1 Doc</label>
                            <div class="form-group">
                              <input type="file" name="section[0][doc][v_doc][0]" class="form-control" required="" data-parsley-trigger="keyup">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <div class="form-group">
                              <button class="btn btn-primary" type="button" onclick="addmoredoc('1')" >Add more Doc</button>
                            </div>
                        </div>
                        <div style="clear: both"></div>
                        <div id="sectiondoc1">
                        </div>
                    </div>
                    <input type="hidden" id="sectioncnt" value="1">
                    @endif
                    */ ?>

                   {{--  <div id="coursessectionmorediv">
                    </div>

                    <div class="col-md-12">
                        <label>&nbsp;</label>
                        <div class="form-group">
                          <button type="button" class="btn btn-info" onclick="addmoresection()">Add More Section</button>
                        </div>
                    </div> --}}

                    <div class="col-md-12">
                        <div class="form-group">
                          <label>Status<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                            <option value="">-select-</option>
                            <option value="published" @if( isset($data->e_status) && $data->e_status == 'published' ) selected @endif>Published</option>
                            <option value="unpublished" @if( isset($data->e_status ) && $data->e_status == 'unpublished') SELECTED @endif >Unpublished</option>
                            <option value="draft" @if( isset($data->e_status ) && $data->e_status == 'draft') SELECTED @endif >Draft</option>
                          </select>
                      </div>   
                    </div>

                  </div>
                  <div class="clear:both"></div>                
                        
                        <div class="row">
                          <div class="col-md-12">
                              <a href="{{url('admin/courses')}}">
                              <button type="button" class="btn btn-warning">Back to List</button>
                              </a>
                              <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                      </div> 
                    </div>
                    </form>
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>
          </div>
        </div>
      </section>      
              
    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <div class="box-header pull-right">
              <a href="{{url('admin/courses/add/0')}}">
                 <button type="submit" class="btn btn-info">Add New Course</button>
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User</th>
                  <th>Title</th>
                  <th>Sub Title</th>
                  <th>Level</th>
                  <th>Author</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )
                    <tr>
                      <td>{{count( $value->hasUser() ) ? $value->hasUser()->v_fname .' '.$value->hasUser()->v_lname : ''}}</td>
                      <td>{{$value->v_title or ''}}</td>
                      <td>{{$value->v_sub_title or ''}}</td>
                      <td>{{count( $value->hasLevel() ) ? $value->hasLevel()->v_title : ''}}</td>
                      <td>{{$value->v_author_name or ''}}</td>
                      <td>{{$value->e_status or ''}}</td>
                      <td>
                        <a href="{{url('admin/courses/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        @php $a=url('admin/courses/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','<?php echo $a ?>')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
              @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>

  function myFunction() {
    
    var cnt = $('#add_text').children().length
    
    if(cnt>=3){
        $("#parsley-id-tags").css("display","block");
        jQuery('#temp_text').val("");
        return 0;
    }else{
        $("#parsley-id-tags").css("display","none");
    }

    var dt = new Date();
    var dynamic_id = dt.getDate() + "" + dt.getHours() + "" + dt.getMinutes() + "" + dt.getSeconds() + "" + dt.getMilliseconds();
    var y = jQuery('#temp_text').val();

     var y = jQuery('#temp_text').val();
      if(y==""){
          return 0;
      }
    
    var z = '<button>';
    jQuery('#add_text').append("<button class='auto-width' id='" + dynamic_id + "' onclick='removeButton(this.id)'><span>" + y + "</span><img src='{{url('public/Assets/frontend/images/close1.png')}}'></button>");

    var cnt = $('#add_text').children().length

            if(cnt==3){
                $("#addtagbutton").css("display","none");
            }

    var abval = $("#temp_text").val();
    var strhidden = '<input type="hidden" id="hidden-'+dynamic_id+'" name="v_search_tags[]" value="'+abval+'">';
    $("#hiddentags").append(strhidden);
    $("#temp_text").val("");
}

function removeButton(id) {
    jQuery("#" + id).remove();
    jQuery("#hidden-"+id).remove();

    var cnt = $('#add_text').children().length
    if(cnt<3){
        $("#addtagbutton").css("display","block");
    }
}

  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });

  $(document).on("click", ".remedit", function() { 
      $(this).parents(".removeeditvideo").remove();
  });

  $(document).on("click", ".remsectionvideo", function() { 
      $(this).parents(".sectionvideoremove").remove();
  });



  function addmoreVideo(section=""){
      var videouploadcnt = $("#videouploadcnt"+section).val();
      $("#videouploadcnt"+section).val(parseInt(videouploadcnt)+1);
      var arrayindex = videouploadcnt;
      var uploaddata=section+videouploadcnt;
      var i = parseInt(section)-1;
      var appendstr="";
      appendstr+='<div style="clear: both"></div><div class="videoData sectionvideoremove"><hr/>';
      appendstr+='<div class="col-md-6">';
      appendstr+='<label>Section '+section+' Video Title</label>';
      appendstr+='<div class="form-group">';
      appendstr+='<input type="text" name="section['+i+'][video]['+arrayindex+'][v_title]" class="form-control" required="" value="">';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div class="col-md-6">';
      appendstr+='<div class="form-group">';
      appendstr+='<div class="open-browser">';
      appendstr+='<label class="field-name">';
      appendstr+='Section '+section+' Video';
      appendstr+='</label>';
      appendstr+='<input type="text" class="form-control input-field" id="videotext'+uploaddata+'" disabled="">';
      appendstr+='<div class="text-right">';

      appendstr+='<input type="file" accept=".mp4, .mkv, .avi, .mov" name="section['+i+'][video]['+arrayindex+'][v_video]" required id="videoupload'+uploaddata+'" style="display: none" onchange="sectionvideo('+uploaddata+');">';

      appendstr+='<label id="fileupload" class="btn-editdetail" for="videoupload'+uploaddata+'">Browser</label>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div style="clear: both"></div> ';
      appendstr+='<div class="col-md-2">';
      appendstr+='<label>Video duration Minutes</label>';
      appendstr+='<div class="form-group">';
      appendstr+='<input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="section['+i+'][video]['+arrayindex+'][v_duration_min]" class="form-control" required="" value="">';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div class="col-md-2">';
      appendstr+='<label>Video duration Seconds</label>';
      appendstr+='<div class="form-group">';

      appendstr+='<input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="section['+i+'][video]['+arrayindex+'][v_duration_sec]" class="form-control" required="" value="00">';
      appendstr+='</div>';
      appendstr+='</div>';

      appendstr+='<div class="col-md-2">';
      appendstr+='<label>Preview this course</label>';
      appendstr+='<div class="form-group">';
      appendstr+='<select name="section['+i+'][video]['+arrayindex+'][i_preview]" class="form-control" required="" value="">';
      appendstr+='<option value="off">Off</option>';
      appendstr+='<option value="on">On</option>';
      appendstr+='</select>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div class="col-md-6">';
      appendstr+='<div class="form-group">';
      appendstr+='<div class="open-browser">';
      appendstr+='<label class="field-name">';
      appendstr+='Section '+section+' Doc';
      appendstr+='</label>';
      appendstr+='<input type="text" class="form-control input-field" id="doctext'+uploaddata+'" disabled="">';
      appendstr+='<div class="text-right">';
      appendstr+='<input type="file" id="docupload'+uploaddata+'" name="section['+i+'][video]['+arrayindex+'][v_doc]" style="display: none" onchange="sectiondoc('+uploaddata+');">';
      appendstr+='<label id="fileupload" class="btn-editdetail" for="docupload'+uploaddata+'">Browser</label>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div class="col-md-12 ">';
      appendstr+='<div class="form-group pull-right">';
      appendstr+='<button type="button" class="btn btn-danger remsectionvideo">Remove Video</button>';
      appendstr+='</div>';
      appendstr+='</div>  ';
      appendstr+='</div>';
      $("#moreVideo"+section).append(appendstr);

  }

  $(document).on("click", ".remsection", function() { 
      $(this).parents(".sectionremove").remove();
  });

  function addmoreSection(section=""){

      var sectioncnt = $("#sectioncnt").val();
      $("#sectioncnt").val(parseInt(sectioncnt)+1);
      var sectiondata = parseInt(sectioncnt)+1;
      var arrayindex = 0;
      
      var uploaddata=sectiondata+sectioncnt;
      var i = parseInt(section)-1;
      var appendstr="";
  
      appendstr+='<div style="clear: both"></div><div class="sectiondata sectionremove"><hr/>';
      appendstr+='<div class="col-md-6">';
      appendstr+='<label>Section '+sectiondata+' Title</label>';
      appendstr+='<div class="form-group">';
      appendstr+='<input type="text" name="section['+sectioncnt+'][v_title]" class="form-control" required="" value="">';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div class="col-md-6">';
      appendstr+='<div class="form-group pull-right">';
      appendstr+='<button type="button" class="btn btn-danger remsection">Remove Section</button>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div style="clear: both"></div><div class="videoData sectionvideoremove">';
      appendstr+='<div class="col-md-6">';
      appendstr+='<label>Section '+sectiondata+' Video Title</label>';
      appendstr+='<div class="form-group">';
      appendstr+='<input type="text" name="section['+sectioncnt+'][video]['+arrayindex+'][v_title]" class="form-control" required="" value="">';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div class="col-md-6">';
      appendstr+='<div class="form-group">';
      appendstr+='<div class="open-browser">';
      appendstr+='<label class="field-name">';
      appendstr+='Section '+sectiondata+' Video';
      appendstr+='</label>';
      appendstr+='<input type="text" class="form-control input-field" id="videotext'+uploaddata+'" disabled="">';
      appendstr+='<div class="text-right">';

      appendstr+='<input type="file" accept=".mp4, .mkv, .avi, .mov" name="section['+sectioncnt+'][video]['+arrayindex+'][v_video]" required id="videoupload'+uploaddata+'" style="display: none" onchange="sectionvideo('+uploaddata+');">';

      appendstr+='<label id="fileupload" class="btn-editdetail" for="videoupload'+uploaddata+'">Browser</label>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div style="clear: both"></div> ';
      appendstr+='<div class="col-md-2">';
      appendstr+='<label>Video duration Minutes</label>';
      appendstr+='<div class="form-group">';
      appendstr+='<input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="section['+sectioncnt+'][video]['+arrayindex+'][v_duration_min]" class="form-control" required="" value="">';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div class="col-md-2">';
      appendstr+='<label>Video duration Seconds</label>';
      appendstr+='<div class="form-group">';

      appendstr+='<input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="section['+sectioncnt+'][video]['+arrayindex+'][v_duration_sec]" class="form-control" required="" value="00">';
      appendstr+='</div>';
      appendstr+='</div>';

      appendstr+='<div class="col-md-2">';
      appendstr+='<label>Preview this course</label>';
      appendstr+='<div class="form-group">';
      appendstr+='<select name="section['+sectioncnt+'][video]['+arrayindex+'][i_preview]" class="form-control" required="" value="">';
      appendstr+='<option value="off">Off</option>';
      appendstr+='<option value="on">On</option>';
      appendstr+='</select>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div class="col-md-6">';
      appendstr+='<div class="form-group">';
      appendstr+='<div class="open-browser">';
      appendstr+='<label class="field-name">';
      appendstr+='Section '+sectiondata+' Doc';
      appendstr+='</label>';
      appendstr+='<input type="text" class="form-control input-field" id="doctext'+uploaddata+'" disabled="">';
      appendstr+='<div class="text-right">';
      appendstr+='<input type="file" id="docupload'+uploaddata+'" name="section['+sectioncnt+'][video]['+arrayindex+'][v_doc]" style="display: none" onchange="sectiondoc('+uploaddata+');">';
      appendstr+='<label id="fileupload" class="btn-editdetail" for="docupload'+uploaddata+'">Browser</label>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='<div style="clear: both"></div> ';
      appendstr+='<div class="moreVideo1" id="moreVideo'+sectiondata+'">';
      appendstr+='</div>';
      appendstr+='<input type="hidden" id="videouploadcnt'+sectiondata+'" value="'+sectiondata+'">';
      appendstr+='<div class="col-md-12 ">';
      appendstr+='<div class="form-group pull-right">';
      appendstr+='<button type="button" class="btn btn-info" onclick="addmoreVideo('+sectiondata+')">Add More Video</button>';
      appendstr+='</div>';
      appendstr+='</div>';
      appendstr+='</div>';


      $("#moreSection").append(appendstr);

  }






  function addmoresection(){
      var b = $("#sectioncnt").val();
      
      var a="";
      var i=parseInt(b)+1;
      $("#sectioncnt").val(i);

      a+='<div id="coursessectiondiv'+i+'" class="removesectionparents" style="border: 2px solid #222d32;margin-top:20px">';
      a+='<div class="col-md-12">';
      a+='<div class="form-group">';
      a+='<button type="button" class="btn btn-danger pull-right removesection">Remove</button>';
      a+='</div>';
      a+='</div>';
      a+='<div class="col-md-12">';
      a+='<label>Section '+i+' Title</label>';
      a+='<div class="form-group">';
      a+='<input type="text" name="section['+b+'][v_title]" class="form-control" required="" data-parsley-trigger="keyup">';
      a+='</div>';
      a+='</div>  ';
      a+='<div class="col-md-3">';
      a+='<label>Section '+i+' Video Title</label>';
      a+='<div class="form-group">';
      a+='<input type="text" name="section['+b+'][video][v_title][]" class="form-control" required="" data-parsley-trigger="keyup">';
      a+='</div>';
      a+='</div>';
      a+='<div class="col-md-3">';
      a+='<label>Section '+i+' Video</label>';
      a+='<div class="form-group">';
      a+='<input type="file" name="section['+b+'][video][v_video][]" class="form-control" required="" data-parsley-trigger="keyup">';
      a+='</div>';
      a+='</div>';
      a+='<div class="col-md-2">';
      a+='<label>&nbsp;</label>';
      a+='<div class="form-group">';
      a+='<input type="hidden" name="section['+b+'][video][i_preview][]" value="off">';
      a+='<input type="checkbox" name="section['+b+'][video][i_preview][]" style="margin-top:12px"><span style="margin-top: -2px"> &nbsp; Preview this video</span> ';
      a+='</div>';
      a+='</div>';
      a+='<div class="col-md-3">';
      a+='<label>&nbsp;</label>';
      a+='<div class="form-group">';
      a+='<button class="btn btn-primary" type="button" onclick="addmorevideo('+i+')">Add more video</button>';
      a+='</div>';
      a+='</div>';
      a+='<div style="clear: both"></div>';
      a+='<div id="sectionvideo'+i+'">';
      a+='</div>';
      a+='<div class="col-md-3">';
      a+='<label>Section '+i+' Doc Title</label>';
      a+='<div class="form-group">';
      a+='<input type="text" name="section['+b+'][doc][v_title][]" class="form-control" required="" data-parsley-trigger="keyup">';
      a+='</div>';
      a+='</div>';
      a+='<div class="col-md-2">';
      a+='<label>Section '+i+' Doc</label>';
      a+='<div class="form-group">';
      a+='<input type="file" name="section['+b+'][doc][v_doc][]" class="form-control" required="" data-parsley-trigger="keyup">';
      a+='</div>';
      a+='</div>';
      a+='<div class="col-md-3">';
      a+='<label>&nbsp;</label>';
      a+='<div class="form-group">';
      a+='<button class="btn btn-primary" type="button" onclick="addmoredoc('+i+')" >Add more Doc</button>';
      a+='</div>';
      a+='</div>';
      a+='<div style="clear: both"></div>';
      a+='<div id="sectiondoc'+i+'">';
      a+='</div>';
      a+='</div>';
      $("#coursessectionmorediv").append(a);
  }

    
  $(document).on("click", ".rem", function() { 
      $(this).parents(".sectiondocremove").remove();
  });  
  $(document).on("click", ".remvideo", function() { 
      $(this).parents(".sectionvideoremove").remove();
  });  

  $(document).on("click", ".removesection", function() { 
      $(this).parents(".removesectionparents").remove();
  });  



  function addmoredoc(section=""){

      var a="";  
      var i=parseInt(section)-1;
      
      a+='<div class="sectiondocremove"><div class="col-md-3">';
      a+='<label>Section 1 Doc Title</label>';
      a+='<div class="form-group">';
      a+='<input type="text" name="section['+i+'][doc][v_title][]" class="form-control" >';
      a+='</div>';
      a+='</div>';
      a+='<div class="col-md-2">';
      a+='<label>Section 1 Doc</label>';
      a+='<div class="form-group">';
      a+='<input type="file" name="section['+i+'][doc][v_doc][]" class="form-control">';
      a+='</div>';
      a+='</div>';
      a+='<div class="col-md-3">';
      a+='<label>&nbsp;</label>';
      a+='<div class="form-group">';
      a+='<button type="button"class="btn btn-danger rem">Remove</button>';
      a+='</div>';
      a+='</div><div style="clear: both"></div></div>';
      
      $("#sectiondoc"+section).append(a);

  }

  function addmorevideo(section=""){

    var a="";  
    var i=parseInt(section)-1;
    a+='<div class="sectionvideoremove">';
    a+='<div class="col-md-3">';
    a+='<label>Section 1 Video Title</label>';
    a+='<div class="form-group">';
    a+='<input type="text" name="section['+i+'][video][v_title][]" class="form-control">';
    a+='</div>';
    a+='</div>';
    a+='<div class="col-md-3">';
    a+='<label>Section 1 Video</label>';
    a+='<div class="form-group">';
    a+='<input type="file" name="section['+i+'][video][v_video][]" class="form-control">';
    a+='</div>';
    a+='</div>';
    a+='<div class="col-md-2">';
    a+='<label>&nbsp;</label>';
    a+='<div class="form-group">';
    a+='<input type="hidden" name="section['+i+'][video][i_preview][]" value="off" >'; 
    a+='<input type="checkbox" name="section['+i+'][video][i_preview][]" style="margin-top:12px"><span style="margin-top: -2px"> &nbsp; Preview this video</span> ';
    a+='</div>';
    a+='</div> ';
    a+='<div class="col-md-3">';
    a+='<label>&nbsp;</label>';
    a+='<div class="form-group">';
    a+='<button type="button"class="btn btn-danger remvideo">Remove</button>';
    a+='</div>';
    a+='</div>';
    $("#sectionvideo"+section).append(a);

  } 
  

</script>

@stop

