@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
</style>
@stop

@section('content')

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Blogs
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">blogs</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div>   
      <section class="content">
        
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ucfirst($view)}} new Blogs</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            
            <div class="box-body">
              <br/><br/>
                <div class="row">
                   <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/blogs/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    
                  <div class="col-md-6">
                    <div class="form-group">
                        <label>Title<span class="asterisk_input">*</span></label>
                        <input type="text" name="v_name" id="v_name" class="form-control" placeholder="Title" value="{{$data->v_name or old('v_name')}}" required="" data-parsley-trigger="keyup">
                    </div>

                    <div class="form-group">
                        <label>Slug<span class="asterisk_input">*</span></label>
                        <input type="text" name="v_slug" id="v_slug" class="form-control" value="{{$data->v_slug or old('v_slug')}}" required="" data-parsley-trigger="keyup">
                    </div>

                  </div>
                  <div class="col-md-6">
                       <div class="form-group">
                        <label>Author Name<span class="asterisk_input">*</span></label>
                        <input type="text" name="v_author_name" id="v_author_name" class="form-control" placeholder="Name" value="{{$data->v_author_name or old('v_author_name')}}" required="" data-parsley-trigger="keyup">
                      </div>

                      <div class="form-group">
                        <label>Publish date</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="d_publish_date" class="form-control pull-right" id="datepicker" value="{{isset($data->d_publish_date)?$data->d_publish_date:''}}">
                        </div>
                      </div>


                  </div>
                    

                    <div class="col-md-12">

                       <div class="form-group">
                          <label>Short Description</label>
                          <textarea name="l_short_description" id="l_short_description" class="form-control"  rows="4" required="" data-parsley-trigger="keyup">{{$data->l_short_description or old('l_short_description')}}</textarea>
                      </div>
                      

                     


                    </div>
                  </div>
                  <div class="clear:both"></div>      
                  <div class="row">
                    <div class="col-md-12">
                      @if ($view=="edit")
                      <div class="form-group">
                        <div><label>Image</label></div>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px;">
                              
                              @if(isset($data->v_image) && $data->v_image != '' )
                                 @php
                                    $imgdata="";
                                    if(Storage::disk('s3')->exists($data->v_image)){
                                        $imgdata = \Storage::cloud()->url($data->v_image);      
                                    }
                                 @endphp 
                                <img src="{{$imgdata}}" style="max-width: 150px" alt="" />
                              @else
                                 @php
                                    $imgdata = \Storage::cloud()->url('common/no-image.png');      
                                 @endphp 
                                <img src="{{$imgdata}}" alt="" />  
                              @endif
                            </div>

                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                            <div>
                               <span class="btn default btn-file">
                                  <span class="fileinput-new"> Select Image </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="file" name="v_image" > </span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                      </div>
                      @else
                      <div class="form-group">
                        <div><label>Image</label></div>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"> </div>
                            <div>
                               <span class="btn default btn-file">
                                  <span class="fileinput-new"> Select Image </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="file" name="v_image" required="" data-parsley-trigger="keyup"> </span>
                               <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                          </div>
                      </div>
                      @endif
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" rows="10" name="l_long_description" id="l_long_description" aria-required="true" required="" data-parsley-trigger="keyup">{{$data->l_long_description or old('l_long_description')}}</textarea>
                    </div>
                   </div>

                  <div class="col-md-12">
                      <div class="form-group">
                        <label>Meta Title</label>
                        <input type="text" name="v_meta_title" id="v_meta_title" class="form-control" placeholder="Name" value="{{$data->v_meta_title or old('v_meta_title')}}" required="" data-parsley-trigger="keyup">
                      </div>
                  </div>

                  <div class="col-md-6">
                      <div class="form-group">
                        <label>Meta Keywords</label>
                        <textarea name="v_meta_keywords" rows="4" id="v_meta_keywords" class="form-control" placeholder="Description" required="" data-parsley-trigger="keyup">{{$data->v_meta_keywords or old('v_meta_keywords')}}</textarea>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label>Meta Description</label>
                        <textarea name="l_meta_description" rows="4" id="l_meta_description" class="form-control" placeholder="Description" required="" data-parsley-trigger="keyup">{{$data->l_meta_description or old('l_meta_description')}}</textarea>
                      </div>
                  </div>

                 
                    <div class="col-md-12">
                      <div class="form-group">
                          <label>Status<span class="asterisk_input">*</span></label>
                          <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                            <option value="">- select -</option>
                            <option value="active" @if( isset($data->e_status) && $data->e_status == 'active' ) selected @endif>Active</option>
                            <option value="inactive" @if( isset($data->e_status ) && $data->e_status == 'inactive') SELECTED @endif >Inactive</option>
                          </select>
                      </div>
                    </div>
                 
                  </div>
                        <div class="row">
                          <div class="col-md-12">
                              <a href="{{url('admin/blogs')}}">
                              <button type="button" class="btn btn-warning">Back to List</button>
                              </a>
                              <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                      </div> 
                    </div>
                    </form>
            <div class="box-footer">
               Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
            </div>
          </div>
        </div>
      </section>      
              
    @else
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div style="clear: both"></div>
        @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
        </div>
      @endif
      @if ($warning = Session::get('warning'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
        </div>
      @endif

          <div class="box">
         <!--  <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <div class="box-header pull-right">
              <a href="{{url('admin/blogs/add/0')}}">
                 <button type="submit" class="btn btn-info">Add New Blog</button>
              </a>
             </div>
            
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="categoryListing" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if( isset($data) && count($data) )
                     @foreach( $data as $count => $value )

                    <tr>
                      <td>{{$value->v_name or ''}}</td>
                      <td>


                        @if(isset($value->v_image) && $value->v_image != '' )
                          @php
                            $imgdata="";
                            if(Storage::disk('s3')->exists($value->v_image)){
                                $imgdata = \Storage::cloud()->url($value->v_image);      
                            }
                          @endphp 
                          <img src="{{$imgdata}}" height="50" width="50" alt="" />
                        @else
                           @php
                              $imgdata = \Storage::cloud()->url('common/no-image.png');      
                           @endphp 
                          <img src = "{{$imgdata}}" height="50" width="50" alt="" />  
                        @endif

                      </td>
                      <td>
                      <?php 
                      // strip tags to avoid breaking any html
                        $string = $value->l_short_description;
                        $string = strip_tags($string);

                        if (strlen($string) > 170) {

                            // truncate string
                            $stringCut = substr($string, 0, 170);

                            // make sure it ends in a word so assassinate doesn't become ass...
                            $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="'. url('admin/blogs/edit/').'/'.$value->id .'">Read More</a>'; 
                        }
                        echo $string;

                      ?>
                      </td>
                      <td>
                        @if(isset($value->e_status) && $value->e_status=="active")
                            <span class="badge bg-green">Active</span>         
                        @else
                            <span class="badge bg-yellow">Inactive</span>
                        @endif
                      </td>
                      <td>
                        <a href="{{url('admin/blogs/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{url('admin/blogs',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if( $value->e_status == 'inactive' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                        @php $a=url('admin/blogs/action/delete/').'/'.$value->id; @endphp
                        <a href="javascript:;" onclick="confirmDelete('{{ $value->id }}','{{$a}}')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
            
                  @else
                  <tr>
                    <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                  </tr>
            @endif
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
     @endif
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script>
  $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
    })
  $('#v_name').change(function() {
      var string = $('#v_name').val();
      var $v_key = '';
      var trimmed = $.trim(string);
      $v_key = trimmed.replace(/[^a-z0-9-]/gi, '-')
              .replace(/-+/g, '-')
              .replace(/^-|-$/g, '');
      $('#v_slug').val($v_key.toLowerCase());
      return true;
    });
</script>


<script>
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });
</script>

<script type="text/javascript">
      CKEDITOR.replace( 'l_long_description', {
        filebrowserBrowseUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/ckfinder.html?Type=Files') }}",
        filebrowserImageBrowseUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/ckfinder.html?Type=Images') }}",
        filebrowserFlashBrowseUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash') }}",
        filebrowserUploadUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}",
        filebrowserImageUploadUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/core/connctor/php/connector.php?command=QuickUpload&type=Images') }}",
        filebrowserFlashUploadUrl: "{{ url('public/Assets/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}",
      });
    </script>

@stop

