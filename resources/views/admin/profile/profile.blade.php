@extends('layouts.master')
<style type="text/css">
  .letestuser{
        margin-bottom: 10px;
  }
  .error{
    color: #ff0000;
  }
</style>
@section('content')
 <div class="content-wrapper">
    <section class="content-header">
      <h1>
        @if(isset($view) && $view == 'edit' )
          User Profile
        @else
          Admins
        @endif  
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    @if(isset($view) && $view == 'add' || $view == 'edit' )
      <section class="content">
        @if(isset($view) && $view == 'edit' )
          <div class="row">
            <div class="col-md-12">

              <div class="box box-primary">
                <div class="box-body box-profile">
                  @if(isset($admin->v_profile) && $admin->v_profile != '')
                    @php
                        $imgdata="";
                        if(Storage::disk('s3')->exists($admin->v_profile)){
                            $imgdata = \Storage::cloud()->url($admin->v_profile);      
                        }
                    @endphp
                    <img src="{{$imgdata}}" class="profile-user-img img-responsive img-circle" alt="User Image">
                  @else
                     @php
                        $imgdata = \Storage::cloud()->url('common/no-image.png');      
                     @endphp 
                     <img src="{{$imgdata}}" class="profile-user-img img-responsive img-circle" alt="User Image">                   
                  @endif  
                  <h3 class="profile-username text-center">{{$admin->v_name}}</h3>
                </div>
              </div>
             
            </div>
         </div> 
        @endif 
       <div class="row">  
          <!-- /.col -->
          <div class="col-md-12">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                @if(isset($view) && $view == 'edit' )
                  <li class="active" style="width: 20%;"><a href="#activity" data-toggle="tab">View Profile</a></li>
                  <li style="width: 20%;"><a href="#settings" data-toggle="tab">Edit Profile</a></li>
                @else
                  <li class="active" style="width: 20%;"><a href="#settings" data-toggle="tab">Add New Admin</a></li>
                @endif  
              </ul>
              <div class="tab-content">
                @if(isset($view) && $view == 'edit' )
                  <div class="active tab-pane" id="activity">
                    
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                              <label class="col-sm-2 control-label">Name</label>
                              <div class="col-sm-10">
                                <p class="form-control-static">{{$admin->v_name}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Email</label>
                          <div class="col-sm-10">
                            <p class="form-control-static">{{$admin->v_email}}</p>
                        </div>
                      </div>

                        @if(isset($admin->v_profile) && $admin->v_profile != '')
                          <div class="form-group">
                              <label class="col-sm-2 control-label">Profile Image</label>
                              <div class="col-sm-10">
                                @php
                                    $imgdata="";
                                    if(Storage::disk('s3')->exists($admin->v_profile)){
                                        $imgdata = \Storage::cloud()->url($admin->v_profile);      
                                    }
                                @endphp 
                                <img src="{{$imgdata}}" style="width:110px">
                              </div>
                          </div>
                        @else
                           @php
                              $imgdata = \Storage::cloud()->url('common/no-image.png');      
                           @endphp 
                          <div class="form-group">
                              <label class="col-sm-2 control-label">Profile Image</label>
                              <div class="col-sm-10">
                                <img src="{{$imgdata}}" style="width:110px">
                              </div>
                          </div>
                        @endif

                   </form>
                 </div>
                @endif 
               
                <div class="@if(isset($view) && $view == 'add' )active @endif tab-pane" id="settings">
                @php
                    if(isset($view) && $view == 'add' ){
                      $profile_url = url('/admin/admin/add-profile/0');  
                    }else{
                      $profile_url = url('/admin/admin/update-profile/'.$admin->_id);
                    } 
                @endphp
                  
                  <form class="form-horizontal" id="edit_profile_form" method="post" action="{{$profile_url}}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                    
                    <div class="form-group">
                      <label for="inputName" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="v_name" id="v_name" placeholder="Name" value="{{$admin->v_name or ''}}">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="v_email" name="v_email" placeholder="Email" value="{{$admin->v_email or ''}}">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="inputExperience" class="col-sm-2 control-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                      </div>
                    </div>
                   
                    <div class="form-group">
                      <label for="inputSkills" class="col-sm-2 control-label">Re-enter Password</label>
                      <div class="col-sm-10">
                        <input type="password" id="repassword" name="repassword" class="form-control" placeholder="Re-enter Password">
                      </div>
                    </div>

                     <div class="form-group">
                      <label for="img" class="col-sm-2 control-label"> Profile Image</label>
                       <div class="col-sm-10">
                          <div class="" data-provides="fileinput">
                              <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                  <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Choose</span><input type="hidden"><input type="hidden"><input type="file" id="v_image" name="v_profile" accept="image/*" style="padding:0px;border:none"></span>
                                  
                                  <div class="form-control" data-trigger="fileinput"><span class="fileinput-filename"></span></div>
                              </div>
                          </div>
                          <div id="file_icon_UploadFile1">
                          </div>
                          </div>
                    </div>
                    <div class="form-group">
                    <label for="img" class="col-sm-2 control-label"></label> 
                    <div class="col-sm-10">
                     @php
                       //dd( $admin->v_profile);
                     @endphp
                     @if(isset($admin->v_profile) && $admin->v_profile != '')
                       <input type="hidden" id="ori_image" name="ori_image" value="{{ $admin->v_profile }}">
                         @php
                            $imgdata="";
                            if(Storage::disk('s3')->exists($admin->v_profile)){
                                $imgdata = \Storage::cloud()->url($admin->v_profile);      
                            }
                        @endphp
                       <img src="{{$imgdata}}" id="ori_pro_img" style="width:110px">
                       <img src="" id="new_image">
                      @else
                          @php
                              $imgdata = \Storage::cloud()->url('common/no-image.png');      
                           @endphp 
                          <img src="{{$imgdata}}" id="new_image" style="width:110px">
                      @endif
                    </div>
                    </div>

                    <div class="form-group">
                      <label for="inputSkills" class="col-sm-2 control-label">Status</label>
                      <div class="col-sm-10">
                        <select class="form-control" id="e_status" name="e_status" required="" data-parsley-trigger="keyup">
                          <option value="">- select -</option>
                          <option value="active" @if( isset($admin->e_status) && $admin->e_status == 'active' ) selected @endif>Active</option>
                          <option value="inactive" @if( isset($admin->e_status ) && $admin->e_status == 'inactive') SELECTED @endif >Inactive</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-danger">Submit</button>
                      </div>
                    </div>
                  </form>

                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

      </section>
    @else
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
          <div style="clear: both"></div>
          @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

            <div class="box">
           <!--  <div class="box-header">
                <h3 class="box-title"></h3>
              </div> -->
              <div class="box-header pull-right">
                <a href="{{url('admin/admin/add/0')}}">
                   <button type="submit" class="btn btn-info">Add New Admin</button>
                </a>
               </div>
              
              <div style="clear: both;"></div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="categoryListing" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email Address</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @if( isset($admin) && count($admin) )
                       @foreach( $admin as $count => $value )

                      <tr>
                        <td>{{$value->v_name or ''}}</td>
                        <td>{{$value->v_email or ''}}</td>
                        <td>{{$value->e_status or ''}}</td>
                        <td>
                          <a href="{{url('admin/admin/edit/')}}/{{$value->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                          <a href="{{url('admin/admin',[$value->id, $value->e_status, 'status'])}}" class="btn btn-info btn-sm">
                          @if( $value->e_status == 'inactive' )
                            <i title="Activate" class="fa fa-check"></i>
                          @else
                            <i title="Inactivate" class="fa fa-times"></i>
                          @endif
                        </a>
                          @php $a=url('admin/admin/delete/').'/'.$value->id; @endphp
                          <a href="javascript:;" title="Delete" onclick="confirmDelete('{{ $value->id }}','<?php echo $a ?>')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                    @endforeach
              
                    @else
                    <tr>
                      <td colspan="6" align="center">@lang('message.common.noRecords')</td>
                    </tr>
              @endif
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section> 
    @endif
    <!-- /.content -->
  </div>
@stop
@section('js')
<script type="text/javascript">

!function($) {
    "use strict";

    var FormValidator = function() {
      this.$edit_profile_form = $("#edit_profile_form");
    };

    $.validator.addMethod("loginRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");

    $.validator.addMethod("phoneRegex", function(value, element) {
        return this.optional(element) || /^[0-9 *#+]+$/i.test(value);
    }, "numbers, space and special characters only.");

    $.validator.addMethod("string", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");

    $.validator.addMethod("emailregx", function(value, element) {
        return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
    }, "Please Enter valid Email Address.");

    $.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[.a-zA-Z\s]+$/);
    });

    jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 && 
        phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
    }, "Please specify a valid phone number");

    FormValidator.prototype.init = function() {
      this.$edit_profile_form.validate({
            rules: {
                v_name: {
                  required: true,
                  maxlength: 50,
                  alpha: true,
                },
                
                v_email: {
                  emailregx: true,
                  required: true,
                },
                password: {
                    minlength: 6
                },
                repassword: {
                    minlength: 6,
                    equalTo: "#password"
                },
            },
            messages: {
                v_name: {
                    required:  "Please enter your name.",
                    maxlength: "maxlength is 50 character.",
                    alpha:    "Value for First name can only have String values.",
                },
                v_email: {
                    required: "Please enter your Email.",
                    emailregx:   "Please Enter Valid Email Address.",
                },
                password: {
                    minlength: "Your password must be at least 6 characters long"
                },
                repassword: {
                    minlength: "Your password must be at least 6 characters long",
                    equalTo: "Please enter the same password as above"
                }
            }
        });
    }
$.FormValidator = new FormValidator, $.FormValidator.Constructor = FormValidator
}(window.jQuery),
function($) {
    "use strict";
    $.FormValidator.init()
}(window.jQuery);

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#new_image').attr('src', e.target.result);
       $('#new_image').attr('width', 110);
      //alert(e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#v_image").change(function() {
  $('#ori_pro_img').hide();
  readURL(this);
});
</script>

@stop

