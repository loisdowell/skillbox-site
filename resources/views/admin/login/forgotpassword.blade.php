<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Admin Login</title>
		<link rel="stylesheet" href="{{url('/public/Assets/bootstrap/css/bootstrap.min.css')}}">
	  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	  	<link rel="stylesheet" href="{{url('/public/Assets/admin/css/AdminLTE.min.css')}}">
	  	<link rel="stylesheet" href="{{url('/public/Assets/plugins/iCheck/square/blue.css')}}">
	  	<link href="{{url('/public/Assets/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
	  	<style type="text/css">
	  	 .copyright {
		    text-align: center;
		    margin: 0 auto 30px 0;
		    padding: 10px;
		    color: #7a8ca5;
		    font-size: 13px;
		}
		.login-box-msg{
			color: #3c8dbc;
		}
	  	</style>
	</head>
	<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
    	<a href="{{url('admin/login')}}"><b>Skill</b>BOX</a>
  		</div>
  		  @if ($success = Session::get('success'))
		    <div class="alert alert-success" style="margin-top:20px;">
		      <i class="fa fa-check-square-o margin-right-10"></i>&nbsp;&nbsp;&nbsp;{{$success}}
		    </div>
		  @endif
		  @if ($warning = Session::get('warning'))
		    <div class="alert alert-danger" style="margin-top:20px;">
		      <i class="fa fa-warning margin-right-10"></i>&nbsp;&nbsp;&nbsp;{{$warning}}
		    </div>
		  @endif
		<div class="login-box-body" style="margin-top: 0px;">
			<form class="login-form" id="forgotpassword-form" name="forgotpassword-form" action="{{url('admin/forgotpassword')}}" method="POST">
				{{ csrf_field() }}
				<h3 class="form-title login-box-msg">Forgot Password</h3>
				<div class="form-group">
					<p>Please enter your email address. You will receive a link to create a new password via email.</p>
					
					<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email Address" name="v_email" required="">
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Get Email</button>
				</div>
				<div class="form-actions " >
					<a href="{{url('admin/login')}}" class="pull-right">
						<label>
							Login
						</label>
					</a>
				</div>
			</form>
			
		</div>

		<div class="copyright">{{date('Y')}} &copy; Skillbox</div>
	</div>
		<script src="{{url('/public/Assets/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
		<script src="{{url('/public/Assets/bootstrap/js/bootstrap.min.js')}}"></script>
		<script src="{{url('/public/Assets/plugins/iCheck/icheck.min.js')}}"></script>
		<script src="{{url('/public/Assets/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
		
	</body>
</html>