<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Admin Login</title>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="{{url('/public/theme/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('/public/theme/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('/public/theme/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('/public/theme/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('/public/theme/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('/public/theme/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />


		<link href="{{url('/public/theme/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />

		<link href="{{url('/public/theme/pages/css/login.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('/public/theme/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	<body class="login">
		<div class="logo" style="padding-bottom: 10px;">
			<a href="{{url('/')}}">
			<img src="{{url('public/theme/pages/img/logo.png')}}" alt="" /> </a>
		</div>

		<div class="content" style="margin-top: 0px;">
			<form class="login-form" id="confirm-new-password-form" name="confirm-new-password-form" action="{{url('admin/confirm-new-password')}}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="admin_id" value="{{$admin_id or '0'}}">
				<input type="hidden" name="reset_password_token" value="{{$reset_password_token or '0'}}">
				<input type="hidden" name="link" value="{{$link or ''}}">
				
				<h3 class="form-title font-green">Reset Your Password</h3>
				<div class="form-group"></div>
				<div class="form-group">
					<label class="control-label visible-ie8 visible-ie9">New Password</label>
					<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" required="" />
				</div>
				<div class="form-group">
					<label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
					<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Confirm Password" name="confirm_password" required="" />
				</div>
				<div class="form-actions">
					<button type="submit" class="btn green uppercase">Save</button>
				</div>
				<div class="form-actions " >
					<a href="{{url('admin/login')}}" class="pull-right">
						<label>
							Login
						</label>
					</a>
				</div>

			</form>
			@if ($success = Session::get('success'))
				<div class="alert alert-success">
					<i class="fa fa-check-square-o margin-right-10"></i>{{$success}}
				</div>
			@endif
			@if ($warning = Session::get('warning'))
				<div class="alert alert-danger">
					<i class="fa fa-warning margin-right-10"></i>{{$warning}}
				</div>
			@endif
		</div>
		<div class="copyright">{{date('Y')}} &copy; Charity</div>
		<script src="{{url('/public/theme/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
		<script src="{{url('/public/theme/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
		<script src="{{url('/public/theme/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
		<script src="{{url('/public/theme/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
		<script src="{{url('/public/theme/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
		<script src="{{url('/public/theme/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
		<script src="{{url('/public/theme/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
		<script src="{{url('/public/theme/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
		<script src="{{url('/public/theme/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
		<script src="{{url('/public/theme/global/scripts/app.min.js')}}" type="text/javascript"></script>
		<script src="{{url('/public/theme/pages/scripts/login.min.js')}}" type="text/javascript"></script>
	</body>
</html>