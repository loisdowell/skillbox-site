
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Skillbox| Log in</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <link rel="stylesheet" href="{{url('/public/Assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{url('/public/Assets/admin/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{url('/public/Assets/plugins/iCheck/square/blue.css')}}">
  <link href="{{url('/public/Assets/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />

</head>
<body class="hold-transition login-page">

<div class="login-box">
  <div class="login-logo">
    <a href="{{url('admin/login')}}">
    @php
          $logosrc= App\Helpers\General::SiteLogo();
    @endphp  
    <img src="{{$logosrc}}" alt="images">
    </a>
  </div>
  @if ($success = Session::get('success'))
    <div class="alert alert-success" style="margin-top:20px;">
      <i class="fa fa-check-square-o margin-right-10"></i>&nbsp;&nbsp;&nbsp;{{$success}}
    </div>
  @endif
  @if ($warning = Session::get('warning'))
    <div class="alert alert-danger" style="margin-top:20px;">
      <i class="fa fa-warning margin-right-10"></i>&nbsp;&nbsp;&nbsp;{{$warning}}
    </div>
  @endif

  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <form class="login-form" id="login-form" name="login-form" action="{{url('admin/login')}}" method="POST" data-parsley-validate enctype="multipart/form-data">
				{{ csrf_field() }}
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="v_email" required="">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" required="">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-6">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember"> Remember Me
            </label>
          </div>
        </div>
        <div class="col-xs-6">
        </div>
      </div>
    <div class="social-auth-links text-center">
    	<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
    </div>
    </form>
    <a href="{{url('admin/forgotpassword')}}">I forgot my password</a><br>
  </div>
</div>

<script src="{{url('/public/Assets/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script src="{{url('/public/Assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('/public/Assets/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{url('/public/Assets/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

