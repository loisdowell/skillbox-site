@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.css')}}">
<link href="{{url('/public/theme/global/plugins/parsley/parsley.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/public/Assets/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee !important;
}
.bootstrap-tagsinput{
  width: 100% !important;
}

.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}


</style>
<style type="text/css">
    .imgrem {position: relative;}
    .close-img{height: 15px; position: absolute;right: 4px;top: -36px;cursor: pointer}
    .select2-selection__choice, #add_text button {background-color: #3c8dbc !important;}
</style>

@stop

@section('content')
<div class="modal fade" id="deleteModalimg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font">Confirm Remove Image ?</h3>
            </div>
            <div class="modal-body" style="text-align:center">
            <i class="margin-top-10 fa fa-question fa-5x"></i>
            <h4>Delete !</h4>
            <p>Are you sure you want to delete this image?</p>
            </div>
           <form name="deleteimg" id="deleteimgjobpost" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="i_job_id" id='i_job_id_delete'>
            <input type="hidden" name="v_photo_name" id='v_photo_name_delete'>
            <input type="hidden" name="v_span" id='v_span_delete'>
           </form>      
            <div class="modal-footer" style="padding-top: 20px;">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" type="button" onclick="jobpostimagedelete();" ><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
                <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" type="button" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModalvidso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font">Confirm Remove Video ?</h3>
            </div>
            <div class="modal-body" style="text-align:center">
            <i class="margin-top-10 fa fa-question fa-5x"></i>
            <h4>Delete !</h4>
            <p>Are you sure you want to delete this video?</p>
            </div>
           <form name="deletevideo" id="deletevideojobpost" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="i_job_id" id='i_job_id_video'>
            <input type="hidden" name="v_video_name" id='v_video_name_video'>
            <input type="hidden" name="v_span" id='v_span_video'>
           </form>      
            <div class="modal-footer" style="padding-top: 20px;">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" type="button" onclick="jobpostvideodelete();" ><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
                <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" type="button" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
            </div>
        </div>
    </div>
</div>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Jobpost Managment
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">plan</li>
      </ol>
    </section>

    <!-- Main content -->
    @if($view=="add" || $view=="edit")

      <div style="clear: both"></div>
      <div class="col-xs-12">    
        <div style="clear: both"></div>     
            @if ($success = Session::get('success'))
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
          </div>
        @endif

      </div>  
    
    <div style="clear: both"></div> 



      <section class="content">
        
          <div class="box box-default">
              <div class="box-header with-border">
                  <h3 class="box-title">{{ucfirst($view)}} new Job</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                  </div>
              </div>
              
              <div class="box-body">
                  <br/><br/>
                
                <form class="horizontal-form" role="form" method="POST" name="adminForm" id="adminForm" action="{{url('admin/job/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" data-parsley-validate enctype="multipart/form-data" >
                  {{ csrf_field() }}
                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                      <div class="row">
                          
                         <div class="col-md-4">
                              <div class="form-group">
                                  <label>Select User<span class="asterisk_input">*</span></label>
                                  <select class="form-control" id="i_user_id" name="i_user_id" required>
                                    <option value="">-select-</option>
                                    @if( isset($usersLists) && count($usersLists) )
                                      @foreach($usersLists as $key => $val)
                                        <option value="{{$val->_id}}" @if( isset($data->i_user_id) && $data->i_user_id == $val->_id) selected @endif >{{$val->v_fname}} {{$val->v_lname}}</option>
                                      @endforeach
                                    @endif
                                  </select>
                              </div>
                          </div>    

                          <div class="col-md-4">
                            <div class="form-group">
                                  <label>Service<span class="asterisk_input">*</span></label>
                                  <select class="form-control" name="v_service" required >
                                    <option value="">-Select-</option>
                                    <option value="online" @if(isset($data->v_service) && $data->v_service=='online') selected @endif >Online</option>
                                    <option value="inperson" @if(isset($data->v_service) && $data->v_service=='inperson') selected @endif >In Person</option>
                                  </select>
                              </div>
                          </div>

                          <div class="col-md-4">
                               <div class="form-group">
                                    <label>Category<span class="asterisk_input">*</span></label>
                                    <select class="form-control" id="i_category_id" name="i_category_id" onchange="getSkill(this.value)" required >
                                      <option value="">-Select-</option>
                                        @if(isset($category) && count($category))
                                            @foreach($category as $k=>$v)
                                                <option value="{{$v->id}}" @if(isset($data->i_category_id) && $data->i_category_id==$v->id) selected @endif >{{$v->v_name or ''}}</option>
                                            @endforeach
                                       @endif  
                                    </select>

                                </div>
                          </div>

                          <div class="col-md-4">

                               <div class="form-group">
                                  <label>Skill<span class="asterisk_input">*</span></label>
                                  <select class="form-control" id="i_mailskill_id" name="i_mailskill_id" required>
                                    <option value="">-Select-</option>
                                     @if(isset($skilldata) && count($skilldata))
                                          @foreach($skilldata as $k=>$v)
                                            @if(isset($data->i_mailskill_id) && $data->i_mailskill_id==$v->id)
                                                    <option value = '{{$v->id}}' selected >{{$v->v_name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                  </select>
                              </div>

                          </div>

                          <div class="col-md-4">

                              <div class="form-group">
                                  <label>Other Skill<span class="asterisk_input">*</span></label>
                                  <select class="form-control" id="i_otherskill_id" name="i_otherskill_id[]" multiple="multiple" required>
                                     <option value="">-Select-</option>
                                     
                                     @if(isset($skilldata) && count($skilldata))
                                        @foreach($skilldata as $k=>$v)
                                        <option value = '{{$v->id}}' @if(in_array($v->id, $data->i_otherskill_id)) selected @endif>{{$v->v_name}}</option>
                                        @endforeach
                                     @endif
                                   </select>
                              </div>

                          </div>

                          <div class="col-md-4">
                              <div class="form-group">
                                  <label>Job Title<span class="asterisk_input">*</span></label>
                                  <input type="text" name="v_job_title" class="form-control" placeholder="Title" value="{{$data->v_job_title or ''}}" data-parsley-trigger="keyup" required>
                              </div>

                          </div>
                          <div class="col-md-4">
                               <div class="form-group">
                                  <label>Budget Type<span class="asterisk_input">*</span></label>
                                  <select class="form-control" id="v_budget_type" name="v_budget_type" required onchange="budgetdata(this.value)">
                                    <option value="">-Select-</option>
                                     <option value="total_budget" @if( isset($data->v_budget_type) && $data->v_budget_type == 'total_budget' ) selected @endif>Total Budget</option>
                                     <option value="hourly_rate" @if( isset($data->v_budget_type ) && $data->v_budget_type == 'hourly_rate') SELECTED @endif >Hourly Rate</option>
                                     <option value="open_to_offers" @if( isset($data->v_budget_type) && $data->v_budget_type == 'open_to_offers' ) selected @endif>Open to Offers</option>
                                  </select>
                              </div>

                               <script type="text/javascript">
                                    function budgetdata(data){
                                        if(data=="open_to_offers"){
                                            $("#Specifybudget").css("display",'none');
                                            $("#v_budget_amt").removeAttr("required");
                                        }else{
                                            if(data=="hourly_rate"){
                                                $("#budgettext").html("Specify your hourly rate <span class='asterisk_input'>*</span>");
                                            }else{
                                                $("#budgettext").html("Specify your budget <span class='asterisk_input'>*</span>");
                                            }
                                            $("#Specifybudget").css("display",'block');
                                            $("#v_budget_amt").attr("required");
                                        }
                                    }
                                </script>
                          </div>

                          <div class="col-md-4">
                              <div class="form-group" id="Specifybudget" @if(isset($data->v_budget_type) && $data->v_budget_type=='open_to_offers') style="display: none" @endif>
                                  <label id="budgettext">Specify Your Budget<span class="asterisk_input">*</span></label>
                                  <input type="number" min="0" onkeydown="javascript: return event.keyCode == 69 ? false : true" data-parsley-trigger="keyup" name="v_budget_amt" id="v_budget_amt" class="form-control" value="{{$data->v_budget_amt or ''}}" data-parsley-type="digits" minlength="1" maxlength="10" data-parsley-trigger="keyup" @if(isset($data->v_budget_type) && $data->v_budget_type!='open_to_offers') required @endif>
                              </div>
                          </div>

                          <div class="col-md-4">
                               <div class="form-group">
                                <label>Contact phone</label>
                                <input type="text" name="v_contact" class="form-control" placeholder="Contact" value="{{$data->v_contact or ''}}" data-parsley-type="number" name="v_contact" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" data-parsley-trigger="keyup">
                              </div>


                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                <label>Website</label>
                                <input type="text" name="v_website" class="form-control" placeholder="Website" data-parsley-type="url" value="{{$data->v_website or ''}}">
                              </div>

                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label>Skill Level<span class="asterisk_input">*</span></label>
                                 
                                  <select class="form-control" id="v_skill_level_looking" name="v_skill_level_looking" required>
                                    <option value="">-Select-</option>
                                    <option value="entry" @if(isset($data->v_skill_level_looking) && $data->v_skill_level_looking == 'entry' ) selected @endif>Entry</option>
                                    <option value="intermediate" @if(isset($data->v_skill_level_looking) && $data->v_skill_level_looking == 'intermediate' ) selected @endif>Intermediate</option>
                                    <option value="expert" @if(isset($data->v_skill_level_looking) && $data->v_skill_level_looking == 'expert' ) selected @endif>Expert</option>
                                  </select>

                              </div>


                          </div>

                          <div class="col-md-4">
                          </div>

                      </div>
                      <div style="clear: both;"></div>
                      
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                <label>Describe the job<span class="asterisk_input">*</span></label>
                                <textarea name="l_job_description" class="form-control" required>{{$data->l_job_description or ''}}</textarea>
                              </div>
                          </div>
                      </div>

                      <?php
                           $daydata=array(
                                'monday'=>'Monday',
                                'tuesday'=>'Tuesday',
                                'wednesday'=>'Wednesday',
                                'thursday'=>'Thursday',
                                'friday'=>'Friday',
                                'saturday'=>'Saturday',
                                'sunday'=>'Sunday',
                            );

                          $monthdata=array(
                              '1'=>'January',
                              '2'=>'February',
                              '3'=>'March',
                              '4'=>'April',
                              '5'=>'May',
                              '6'=>'June',
                              '7'=>'July',
                              '8'=>'August',
                              '9'=>'September',
                              '10'=>'Octomber',
                              '11'=>'November',
                              '12'=>'December',
                          );
                      ?>
                      
                      <div class="row">
                            <div class="col-md-2">
                              <div class="form-group">
                                <label>Your Availability From<span class="asterisk_input">*</span></label>
                                <select class="form-control" name="v_avalibility_from"  required>
                                  <option  value="">-Select-</option>
                                  @foreach($daydata as $k=>$v)
                                      <option value="{{$k}}" @if(isset($data->v_avalibility_from) && $data->v_avalibility_from==$k) selected @endif >{{$v}}</option>
                                  @endforeach
 
                                </select>  
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-group">
                                <label>Your Availability To<span class="asterisk_input">*</span></label>
                                <select class="form-control" name="v_avalibility_to" required>
                                   <option  value="">-Select-</option>
                                    @foreach($daydata as $k=>$v)
                                        <option value="{{$k}}" @if(isset($data->v_avalibility_to) && $data->v_avalibility_to==$k) selected @endif >{{$v}}</option>
                                    @endforeach
                                </select>  
                              </div>
                            </div>

                            <div class="col-md-2">
                              <div class="form-group">
                               <label>Your Availability From time<span class="asterisk_input">*</span></label>
                               <select class="form-control" name="v_avalibilitytime_from"  required>
                                   <?php
                                      for($i=1;$i<=24;$i++){ ?>
                                          <option value="{{$i}}" @if(isset($data->v_avalibilitytime_from) && $data->v_avalibilitytime_from==$i) selected @endif >{{$i}}</option>
                                  <?php } ?>

                               </select>
                            </div> 
                            </div> 
                            <div class="col-md-2">
                              <div class="form-group">
                              <label>Your Availability To time<span class="asterisk_input">*</span></label>
                              <select class="form-control" name="v_avalibilitytime_to"  required>
                              <?php
                                  for($i=1;$i<=24;$i++){ ?>
                                      <option value="{{$i}}" @if(isset($data->v_avalibilitytime_to) && $data->v_avalibilitytime_to==$i) selected @endif>{{$i}}</option>
                              <?php } ?>
                              </select> 
                              </div>
                            </div>

                             <div class="col-md-4">
                          
                           <div class="form-group">
                           <label>When would you like the job to start?<span class="asterisk_input">*</span></label><br>
                              <div class="input-group date">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="d_start_date" class="form-control pull-right " id="datepicker" value="{{isset($data->d_start_date)?$data->d_start_date:''}}">
                              </div>
                            </div>
                          </div>

                        </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Add Your Videos (unlimited)</label><br>
                            <input type="file" id="work_video" name="work_video[]" multiple accept="video/mp4,video/x-m4v,video/*">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Add Your Photos (up to 10)</label><br>
                            <input type="file" id="photos" name="photos[]" multiple accept=".jpeg,.jpg,.png">
                          </div>
                        </div>
                      </div>
                      
                      {{-- <div class="row">
                        <div class="col-md-6" id="adddd">
                        </div>
                        <div class="col-md-6">
                        </div>
                      </div>   --}}
                      @if($view=="add")
                      <script>
                         $(function(){
                            $("#photos").on("change", function(e) {
                                 var files = e.target.files,
                                 filesLength = files.length;
                                 if(filesLength>10){
                                    alert("You are only allowed to upload a maximum of 10 files"); 
                                    $("#photos").val("");
                                 }
                            });
                         });
                      </script>
                      @else
                        <script type="text/javascript">
                        $(function(){

                            var totalupload = '{{count($data->v_photos)}}';
                            var total = 10-parseInt(totalupload);
                            $("#photos").on("change", function(e) {
                                 var files = e.target.files,
                                 filesLength = files.length;
                                 if(filesLength>total){
                                    alert("You are only allowed to upload a maximum of 10 files"); 
                                    $("#photos").val("");
                                 }
                            });
                         });
                        </script>
                      @endif  


                      {{-- <script type="text/javascript">
                        $(document).ready(function() {
                              
                                $("#work_video").on("change", function(e) {
                                 alert("fff");
                                  var files = e.target.files,
                                    filesLength = files.length;
                                  for (var i = 0; i < filesLength; i++) {
                                    var f = files[i]
                                    var fileReader = new FileReader();
                                    fileReader.onload = (function(e) {
                                      var file = e.target;
                                      var aa="<span class=\"pip\">" +
                                        "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                                        "<br/><span class=\"remove\">Remove image</span>" +
                                        "</span>";
                                      alert(aa);  
                                      $("#adddd").append(aa);

                                      $("<span class=\"pip\">" +
                                        "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                                        "<br/><span class=\"remove\">Remove image</span>" +
                                        "</span>").insertAfter("#files");

                                      $(".remove").click(function(){
                                        $(this).parent(".pip").remove();
                                      });
                                      
                                    });
                                    fileReader.readAsDataURL(f);
                                  }
                                });
                            });
                      </script> --}}

                      <div class="row">
                          <div class="col-md-6">
                          @if(isset($data->v_video) && count($data->v_video))
                              @foreach($data->v_video as $k=>$v)
                                  @php
                                      $videodata = \Storage::cloud()->url($v);     
                                  @endphp
                                 <span class="imgrem" id="videorem{{$k}}" style="margin-right: 10px;">
                                  <video width="100" height="100" controls style="border: 2px solid gray;">
                                    <source src="{{$videodata}}" type="video/mp4">
                                  </video> 
                                 <img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" onclick="removevideojobpost('{{$data->id}}','{{$v}}','videorem{{$k}}')" style="height: 15px;position: absolute;right: 4px;top: -82px;border: 2px solid gray;cursor: pointer;">
                                 </span>
                              @endforeach
                          @endif
                          </div>
                          <div class="col-md-6">
                          @if(isset($data->v_photos) && count($data->v_photos))
                              @foreach($data->v_photos as $k=>$v)
                                  @php
                                      $imgdata = \Storage::cloud()->url($v);     
                                  @endphp
                                 <span class="imgrem" id="imgrem{{$k}}" style="margin-right: 10px;">
                                 <img src="{{$imgdata}}" style="height: 100px;width: 100px;border: 2px solid gray;">
                                 <img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" onclick="removeimgjobpost('{{$data->id}}','{{$v}}','imgrem{{$k}}')" class="close-img" style="border: 2px solid gray;">
                                 </span>
                              @endforeach
                          @endif

                          </div>
                      </div> 
                       
                      <?php
                             $timeschedule=array(
                                  'hours'=>'Hour(s)',
                                  'day'=>"Day(s)",
                                  'months'=>"Month(s)",
                                  'year'=>"Year(s)",
                              ); 
                        ?>



                      <div class = "row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>How long do you except the job at last? <span class="asterisk_input">*</span></label>
                            <input type="text" name="i_long_expect_duration" class="form-control" value="{{$data->i_long_expect_duration or '' }}" data-parsley-type="digits" minlength="1" maxlength="10" data-parsley-trigger="keyup" required>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                          
                            <select class="form-control form-sell" name="v_long_expect_duration_type" style="margin-top: 25px;" required>
                                @foreach($timeschedule as $k=>$v)
                                    <option value="{{$k}}" @if(isset($data->v_long_expect_duration_type) && $data->v_long_expect_duration_type==$k) selected @endif >{{$v}}</option>
                                @endforeach
                            </select>
                          </div>
                        </div>
                         <div class="col-md-2">
                          <div class="form-group">
                            <input checked="checked"  type="checkbox" name="i_permanent_role" value="selected" style="margin-top: 30px;" >Permanent role
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                             <label>City<span class="asterisk_input">*</span></label>
                                <input type="text" name="v_city" id="autocomplete" class="form-control" placeholder="City" value="{{$data->v_city or ''}}" required>
                          </div>
                        </div>
                         <input type="hidden" name="v_latitude" id="v_latitude" value="@if(isset($data->v_latitude)){{$data->v_latitude}}@endif">
                        <input type="hidden" name="v_longitude" id="v_longitude" value="@if(isset($data->v_longitude)){{$data->v_longitude}}@endif">

                        <div class="col-md-6">
                          <div class="form-group">
                             <label>Postcode<span class="asterisk_input">*</span></label>
                               <input type="text" name="v_pincode" id="v_pincode" class="form-control" placeholder="Pincode" value="{{$data->v_pincode or ''}}" required>
                          </div>
                        </div>
                      </div>

                     {{--  @php
                        $v_search_tags="";
                        if(isset($data->v_search_tags) && $data->v_search_tags != ''){
                            $v_search_tags=implode(",", $data->v_search_tags);
                        }
                      @endphp --}}
                      <div class="row">
                       <div class="col-md-12">
                           <div class="buttet-concept">
                                  <div class="name-details">
                                      <div class="searching-topspace">
                                          <label class="field-name">
                                              Add search tags
                                          </label>
                                      </div>
                                  </div>
                                  <div class="name-details">
                                      <label class="subfield-name">
                                          Please add up to 3 search tags related to your service or job.
                                      </label>
                                  </div>
                                  <div class="row">
                                      <div class="col-xs-12">
                                          <div class="row">
                                              <div class="col-xs-10">
                                                  <div class="input-login">
                                                      <input class="form-control business-name-control" type="text" placeholder="Start typing ...." id="temp_text">
                                                  </div>
                                              </div>
                                              <div class="col-xs-2 terms-add" id="hiddentags">
                                                  @if(isset($data->v_search_tags) && count($data->v_search_tags))
                                                      @foreach($data->v_search_tags as $k=>$v)
                                                          <input type="hidden" id="hidden-{{$v}}" name="v_search_tags[]" value="{{$v}}">
                                                      @endforeach
                                                  @endif    

                                                  <div class="login-btn" id="addtagbutton" @if(isset($data->v_search_tags) && count($data->v_search_tags)>=3)  style="display: none" @endif>
                                                      <button type="button" class="btn add-now" onclick="myFunction()">Add</button>
                                                  </div>
                                              </div>
                                              <div class="col-xs-12">
                                                  <div class="selection-point" id="add_text" style="margin-top: 20px">
                                                      @if(isset($data->v_search_tags) && count($data->v_search_tags))
                                                          @foreach($data->v_search_tags as $k=>$v)
                                                      <button class='auto-width' id='{{$v}}' onclick='removeButton(this.id)'><span>{{$v}}</span><img src="{{url('public/Assets/frontend/images/close1.png')}}"></button>
                                                          @endforeach
                                                      @endif    
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                        </div>  
                        </div>
                      <?php /*
                      <div class="row">
                          <div style="clear: both"></div>
                          <div class="col-md-11" style="padding-right:0px; ">              
                            <label>Search tags</label>
                            <div class="form-group">
                                <input data-role="tagsinput" class="form-control" id="v_search_tags" name="v_search_tags" type="text"  placeholder="Start Typing ...." style="width: 100%" value="{{$v_search_tags}}">
                                <span class="pull-left">Max 3 tags are allowed per courses. </span>
                            </div>
                          </div>
                           <div class="col-md-1" style="padding: 0px;">
                            <label>&nbsp;</label>
                            <div class="form-group">
                              <button class="btn" type="button">Add</button>
                            </div>
                          </div>  
                      </div>
                      */ ?>
                      <div style="clear: both"></div>

                      <br/>

                      <div class="clear:both"></div> 
                        <div class="row">
                          <div class="col-md-12">
                              <a href="{{url('admin/job')}}">
                              <button type="button" class="btn btn-warning">Back to List</button>
                              </a>
                              <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                        </div> 
                      </div>

                </form>
              
                  <div class="box-footer">
                    Fill Above Information and click Save or update. <span class="asterisk_input">*</span> Indicate required filed.
                  </div>
              </div>
        </div>
      </section>      
              
    @else



    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div style="clear: both"></div>
                    @if ($success = Session::get('success'))
                      <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="fa fa-check-square-o margin-right-10"></i>&nbsp; &nbsp;{{$success}}
                      </div>
                    @endif
                    @if ($warning = Session::get('warning'))
                      <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="fa fa-warning margin-right-10"></i>&nbsp; &nbsp;{{$warning}}
                      </div>
                    @endif

                    <div class="box">
         
                        <div class="box-header pull-right">
                          <a href="{{url('admin/job/add/0')}}">
                             <button type="submit" class="btn btn-info">Add New Jobpost</button>
                          </a>
                        </div>
                        
                        <div style="clear: both;"></div>
              
                        <div class="box-body">
                            <table id="categoryListing" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                      <th>Username</th>
                                      <th>Title</th>
                                      <th>Service</th>
                                      <th>Category</th>
                                      <th>Posted Date</th>
                                      <th>Status</th>
                                      <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($data))
                                        @foreach($data as $key=>$val)

                                          <tr>  
                                            <td>
                                            @if(count($val->hasUser()) && isset($val->hasUser()->v_fname) && isset($val->hasUser()->v_lname))
                                                {{$val->hasUser()->v_fname.' '.$val->hasUser()->v_lname}}
                                            @endif  
                                            </td>
                                            <td>{{$val->v_job_title or ''}}</td>
                                            <td>{{$val->v_service or ''}}</td>
                                            <td>
                                              @if(count($val->hasCategory()) && isset($val->hasCategory()->v_name))
                                                {{$val->hasCategory()->v_name}}
                                              @endif
                                            </td>
                                            <td>{{date("Y-m-d",strtotime($val->d_added))}}</td>
                                            
                                            <td>
                                            @php
                                              $currentdate = date("Y-m-d");  
                                            @endphp
                                            @if(isset($val->e_status) && $val->e_status=="active")
                                                 @if(isset($val->d_expiry_date) && strtotime($val->d_expiry_date)<strtotime($currentdate))
                                                    <span class="badge bg-red">Expired</span>
                                                 @else
                                                     <span class="badge bg-green">Active</span>         
                                                 @endif
                                            @else
                                                  <span class="badge bg-yellow">Inactive</span>
                                            @endif
                                            </td>
                                            
                                            <td><a href="{{url('admin/job/edit/')}}/{{$val->id}}" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-pencil"></i></a>
                                           
                                            @php $a=url('admin/job/action/delete/').'/'.$val->id; @endphp
                                             <a href="javascript:;" onclick="confirmDelete('{{ $val->id }}','{{$a}}')" class="btn btn-danger  btn-sm"><i class="fa fa-trash"></i></a>
                                            </td>
                                          </tr>  
                                        @endforeach
                                  @else
                                      <tr>  
                                        <td colspan="7">There is not found any jobpost.</td>
                                      </tr>  
                                  @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
              </div>
          </div>
    </section>
    
    @endif
  
  </div>
  
@stop

@section('js')

<script src="{{url('public/Assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{url('/public/theme/global/plugins/parsley/parsley.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/theme/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{url('/public/Assets/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

 <script type="text/javascript">
                            // $(".form_datetime2").datetimepicker({
                            //     pickTime: false,
                            //     minView: 2,
                            //     format: 'yyyy-mm-dd',
                            //     startDate: '-0m',
                            //     //endDate: '+0d',
                            //     autoclose: true,
                            // });
                        </script>


<script>
  
  $(function () {
    $('#categoryListing').DataTable({
      "paging": true,
    });
  });

 


  $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      startDate: '-0m',
    })

</script>

<script type="text/javascript">

jQuery(document).ready(function(){
  var i = 0;
  var j = 0;
  
  $("#i_otherskill_id").select2();

  jQuery(".add-more").click(function(){ 
    i++;
        jQuery("#copy").append('<div class="control-group input-group" id="control'+i+'" style="margin-top:10px"><input type="text" name="l_bullet[]" id="l_bullet'+i+'" class="form-control" value=""><div class="input-group-btn"><button class="btn btn-danger remove" id="remove'+i+'" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button></div></div>');
    });

   jQuery("#copy").on("click",".remove",function(){ 
    var id=  jQuery(this).parent().parent().attr("id");
       jQuery("#"+id).remove();
   });

    jQuery("#copy1").on("click",".remove",function(){ 
    var id=  jQuery(this).parent().parent().attr("id");
       jQuery("#"+id).remove();
   });




    jQuery(".add-more-addon").click(function(){ 
        j++;

        var a= '<div class="input-group control-group after-add-more"  id="controladdon" style="margin-top: 10px"><input type="text" name="l_addon[text][]" id="AddOntext'+i+'" class="form-control" value="" placeholder="AddOn Text"><input type="text" name="l_addon[price][]" id="AddOnprice'+i+'" class="form-control" value="" placeholder="AddOn Price"><div class="input-group-btn"><button class="btn btn-danger remove" id="remove'+i+'" type="button"><i class="glyphicon glyphicon-remove"></i>Remove</button></div></div>'
        jQuery("#copyaddon").append(a);

    });


    jQuery("#copyaddon").on("click",".remove",function(){ 
      var id=  jQuery(this).parent().parent().attr("id");
         jQuery("#"+id).remove();
    });

    jQuery("#copyaddon1").on("click",".remove",function(){ 
      alert("sssss");  
      var id=  jQuery(this).parent().parent().attr("id");
         jQuery("#"+id).remove();
    });
  }); 

        function getSkill(i_category_id){
            
            var formdata = "i_category_id="+i_category_id;
            var actionurl = "{{url('admin/job/getskill')}}";

            $.ajax({
                  type    : "GET",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#i_mailskill_id").html(obj['optionstr']);
                        $("#i_otherskill_id").html(obj['optionstr']);
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                  }
              });    
        }


   function removeimgjobpost(jid,data,remimg){
            $("#i_job_id_delete").val(jid);
            $("#v_photo_name_delete").val(data);
            $("#v_span_delete").val(remimg);
            $("#deleteModalimg").modal("show");
        }
       
        function jobpostimagedelete(){
            
            var v_span = $("#v_span_delete").val();
            var formdata = $("#deleteimgjobpost").serialize();
            var actionurl = "{{url('admin/job/removeimage')}}";

            $.ajax({
                  type    : "POST",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#"+v_span).remove();
                        $("#deleteModalimg").modal("hide");
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                  }
              });    
        }

        function removevideojobpost(jid,data,remimg){
            $("#i_job_id_video").val(jid);
            $("#v_video_name_video").val(data);
            $("#v_span_video").val(remimg);
            $("#deleteModalvidso").modal("show");
        }

        function jobpostvideodelete(){
            
            var v_span = $("#v_span_video").val();
            var formdata = $("#deletevideojobpost").serialize();
            var actionurl = "{{url('admin/job/removevideo')}}";

            $.ajax({
                  type    : "POST",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#"+v_span).remove();
                        $("#deleteModalvidso").modal("hide");
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                  }
            });    
        }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeUrl5sq-j9P3aar5jKpOiTqralR5T5GE&libraries=places&callback=initAutocomplete" async defer></script>
<script>

        var placeSearch, autocomplete;
        var componentForm = {
            premise: 'long_name',
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'long_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {

            var place = autocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            $("#v_latitude").val(lat);
            $("#v_longitude").val(lng);

            var premise = '';
            var street_number = '';
            var route = '';

            for ( var i = 0; i < place.address_components.length; i++ ) {

                var addressType = place.address_components[i].types[0];

                if ( componentForm[addressType] && addressType == 'premise') {
                    premise = place.address_components[i][componentForm[addressType]];
                }
                if ( componentForm[addressType] && addressType == 'street_number') {
                    street_number = place.address_components[i][componentForm[addressType]];
                }
                if ( componentForm[addressType] && addressType == 'route') {
                    route = place.address_components[i][componentForm[addressType]];
                }
                else if ( componentForm[addressType] && addressType == 'postal_code') {
                    $('#v_pincode').val( place.address_components[i][componentForm[addressType]] );
                }
            }

            var address = '';

            if( premise != '' ) {
                address += ' ' + premise;
            }
            else {
                address += premise;
            }

            if( street_number != '' ) {
                address += ' ' + street_number;
            }
            else {
                address += street_number;
            }

            if( route != '' ) {
                address += ' ' + route;
            }
            else {
                address += route;
            }
            $('#v_city').val(address);

        }

            function geolocate() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        var circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocomplete.setBounds(circle.getBounds());
                    });
                }
            }


            function myFunction() {
    
    var cnt = $('#add_text').children().length
    
    if(cnt>=3){
        $("#parsley-id-tags").css("display","block");
        jQuery('#temp_text').val("");
        return 0;
    }else{
        $("#parsley-id-tags").css("display","none");
    }

    var dt = new Date();
    var dynamic_id = dt.getDate() + "" + dt.getHours() + "" + dt.getMinutes() + "" + dt.getSeconds() + "" + dt.getMilliseconds();
    var y = jQuery('#temp_text').val();

     var y = jQuery('#temp_text').val();
      if(y==""){
          return 0;
      }
    
    var z = '<button>';
    jQuery('#add_text').append("<button class='auto-width' id='" + dynamic_id + "' onclick='removeButton(this.id)'><span>" + y + "</span><img src='{{url('public/Assets/frontend/images/close1.png')}}'></button>");

    var cnt = $('#add_text').children().length

            if(cnt==3){
                $("#addtagbutton").css("display","none");
            }

    var abval = $("#temp_text").val();
    var strhidden = '<input type="hidden" id="hidden-'+dynamic_id+'" name="v_search_tags[]" value="'+abval+'">';
    $("#hiddentags").append(strhidden);
    $("#temp_text").val("");
}

function removeButton(id) {
    jQuery("#" + id).remove();
    jQuery("#hidden-"+id).remove();

    var cnt = $('#add_text').children().length
    if(cnt<3){
        $("#addtagbutton").css("display","block");
    }
}
             
</script>

@stop

