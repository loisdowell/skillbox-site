@extends('layouts.frontend')


@section('content')
    <!--slide-containt-->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">

                 @php
                    $v_banner_image="";
                    if($metaDetails['v_image'] && $metaDetails['v_image']!=""){
                        if(Storage::disk('s3')->exists($metaDetails['v_image'])){
                            $v_banner_image = \Storage::cloud()->url($metaDetails['v_image']);      
                        }                
                    }else{
                    }
                    $v_banner_image = url('public/Assets/frontend/images/skills-jobs.png');      
                @endphp
                
                <img src="{{$v_banner_image}}" class="img-responsive" alt="" />
                <div class="center-containt">
                    <div class="container">
                        <div class="applybox-notification blog-page">
                            @if($metaDetails['v_title'] && $metaDetails['v_title']!="")
                            <h1>{{$metaDetails['v_title']}}</h1>
                            @else
                            <h1> SKILLBOX JOBS </h1>
                            @endif
                            @if($metaDetails['v_sub_headline'] && $metaDetails['v_sub_headline']!="")
                            <p style="margin-top: 25px;">{{$metaDetails['v_sub_headline']}}</p>
                            @else
                            <p style="margin-top: 25px;">Follow our blog and be the first in touch with our latest articles!</p>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--end slide-containt-->

    <!--28B-skillbox-jobs-apply-form-4 -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                 @if ($success = Session::get('success'))
                  <div class="alert alert-success alert-big alert-dismissable br-5">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
                  </div>
                @endif
                @if ($warning = Session::get('warning'))
                  <div class="alert alert-info alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
                  </div>
                @endif

                <div class="main-applyform">
                    <div class="partner-manager">
                        <div class="manager-name">
                            <h3>  {{$data->v_title or ''}} </h3>
                        </div>
                        <div class="manager-address">
                            <p> {{$data->v_country or ''}}, {{$data->v_city or ''}}</p>
                            <p> {{$data->v_short_description or ''}} </p>
                            <p> {{$data->v_job_time or ''}} </p>
                        </div>
                    </div>
                    <form class="horizontal-form" role="form" method="POST" action="{{url('join-our-team/apply-job', $data->id)}}" data-parsley-validate enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <div class="form-field">
                            <div class="row-form-field">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <label class="field-name">
                                            First Name
                                        </label>
                                        <input type="text" name="v_fname" required="" class="form-control input-field">
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <label class="field-name">
                                            Last Name
                                        </label>
                                        <input type="text" name="v_lname" required="" class="form-control input-field">
                                    </div>
                                </div>
                            </div>
                            <div class="row-form-field">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <label class="field-name">
                                            Email
                                        </label>
                                        <input type="email" name="v_email" required="" class="form-control input-field">
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <label class="field-name">
                                            Phone Number
                                        </label>
                                        <input type="tel" name="v_phone_number" class="form-control input-field">
                                    </div>
                                </div>
                            </div>
                            <div class="row-form-field">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <label class="field-name">
                                            Cover letter
                                        </label>
                                        <textarea name="v_cover_letter" required="" rows="6" class="form-control cover-letter"></textarea>
                                        {{-- <input type="file"  class="btn btn-attach-cv"> --}}
                                        
                                        <input type='file' name="v_attach_cv" id="fileupload-down" required="" style="display: none;" onchange="docuploaddata();" />
                                        <label id="fileupload-down" for="fileupload-down">+ Attach CV</label>

                                            
                                    </div>
                                    <ul class="" id="docupload">
                                    </ul>
                                </div>
                            </div>
                            <button class="btn btn-apply-now"> Apply Now </button>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
    <!--end-28B-skillbox-jobs-apply-form-4 -->
@stop
@section('js')
<script type="text/javascript">
    function docuploaddata(){
        var input = document.getElementById('fileupload-down');
        var list = document.getElementById('docupload');
        var strappend="";
        for (var x = 0; x < input.files.length; x++) {
            strappend = strappend+"<li>"+input.files[x].name+"</li>";
        }
        $("#docupload").html(strappend);
    }
</script>
@stop

