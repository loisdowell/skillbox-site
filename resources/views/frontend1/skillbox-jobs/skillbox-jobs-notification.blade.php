@extends('layouts.frontend')


@section('content')
    <!--slide-containt-->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">
                <img src="{{url('public/Assets/frontend/images/skills-jobs.png')}}" class="img-responsive" alt="" />
                <div class="center-containt">
                    <div class="container">
                        <div class="applybox-notification">
                            <h1>SKILLBOX JOBS</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end slide-containt-->

    <!--28B-skillbox-jobs-apply-form-notification-2-->
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-cv-containt">
                    <div class="cv-containt">
                        <h3> Thank You For Your Interest!</h3>
                        <p> We have received your CV and if it gets shortlisted we will get in-touch with you.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-28B-skillbox-jobs-apply-form-notification-2-->
@stop
@section('js')

@stop

