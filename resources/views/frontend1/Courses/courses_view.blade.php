@extends('layouts.frontend')

@section('content')
<style type="text/css">
    .overview-sellerheading.selling-new{display: inherit !important;}
    .text-new {font-size: 16px !important;color: #000;}
</style>
<style type="text/css">
        .letter-text-myprifile {
            text-align: center;
            font-size: 34px;
            padding: 10px 0px;
            color: rgb(231, 14, 138);
        }
        .new-seller-find {border-radius: 20px;
        margin-right: 10px;}
        .new-submit-live {height: 34px;}
        .new-submit-comment{display: inline-flex;width: 100%}
    </style>

    <!--26b-control-panel-my-courses-overviwe-->
    <div class="title-controlpanel controlpanel_setting_icon">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> My Courses Details </h1>
                </div>

                <div class="right-symbol">
                    <a href="{{url('courses/settings')}}/{{$data->id}}" class="setting-course">
                        <img src="{{url('public/Assets/frontend/images/setting-course.png')}}" class="setting-course" alt="" />
                    </a>
                    <a href="{{url('courses/messages')}}/{{$data->id}}"> <img src="{{url('public/Assets/frontend/images/mail.png')}}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-detail tabdetailplace">
        <div class="container">
            <ul class="nav nav-pills nav-pills-new">
                <li class="active"><a href="{{url('courses/view')}}/{{$data->id}}">Overview </a></li>
                <li><a href="{{url('courses/section')}}/{{$data->id}}">Course Sections </a></li>
                <li><a href="{{url('courses/qa')}}/{{$data->id}}">Q&A<span class="place-badge">{{isset($total['qa']) ? $total['qa'] : 0}}</span> </a></li>
                <li><a href="{{url('courses/reviews')}}/{{$data->id}}"> Course Reviews <span class="place-course">{{isset($total['reviews']) ? $total['reviews'] : 0}}</span> </a></li>
                <li><a href="{{url('courses/announcements')}}/{{$data->id}}">Announcements <span class="place-annou">{{isset($total['announcements']) ? $total['announcements'] : 0}}</span> </a></li>

            </ul>
        </div>
    </div>


    <div class="container">

        <!-- 26B-control-panel-my-courses-overview-Seller -->
        <div class="Overview-box Overview_box2">
            <div class="row">
                <div class="col-sm-8 text-center-xs">
                    
                    <div class="overview-sellerheading selling-new">
                        <h3>{{isset($data->v_title) ? $data->v_title : ''}} <span class="text-new"><img src="{{url('public/Assets/frontend/images/clock1.png')}}" alt="" />{{isset($data->i_duration) ? $data->i_duration:''}} Hours</span></h3>
                    </div>

                    <div class="lecture">
                        <div class="lecture-button">
                            <div class="rating-seller">
                                <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="@if(isset($data->i_total_avg_review)){{$data->i_total_avg_review}} @else 0 @endif" readonly>
                                <span class="star-no"> {{number_format($data->i_total_avg_review,1)}} ({{$data->i_total_review}})</span>
                            </div>
                        </div>
                    </div>
                </div>
                    
                <div class="col-sm-4">
                    <div class="overview-radiospace">
                        <div class="wrap selected-radiocourse">
                            <form>
                                <fieldset>
                                     <div class="toggle">
                                        <input type="radio" class="toggle-input" name="joincreate" onchange="coursesStatus(this.value,'{{$data->id}}')" value="published" id="join" @if(isset($data->e_status) && $data->e_status=='published') checked @endif >
                                        <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Published</label>
                                        <input type="radio" class="toggle-input" name="joincreate" onchange="coursesStatus(this.value,'{{$data->id}}')" value="unpublished" id="create" @if(isset($data->e_status) && $data->e_status=='unpublished') checked @endif>
                                        <label for="create" class="toggle-label toggle-label-on" id="createLabel">Unpublished</label>
                                        <span class="toggle-selection"></span>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div id="commonmsg"></div>


        <div class="row">
            
            <div class="col-xs-12">
                <div class="active-recent">
                    <h3>Recent Activity</h3>
                </div>
            </div>
            <div class="clearfix"> </div>
            
            
            <div class="col-sm-6">
                
                
                 @if(isset($coursesQA) && count($coursesQA))        
                    
                    <div class="recent-question">
                        <span class="header-recent">
                            Recent Questions
                        </span>
                    </div>

                    @foreach($coursesQA as $key=>$val)
                        <div class="recent-question">
                            <div class="conversation-find conversation-find-over">
                                <div class="conversation-man">
                                    <?php
                                        $imgdata="";
                                        if(isset($userList[$val->i_user_id])){
                                            $imgdata = \App\Helpers\General::userroundimage($userList[$val->i_user_id]);
                                        }
                                        echo $imgdata;
                                    ?>
                                </div>
                                <div class="conversation-name2">
                                    <p>{{$val->l_questions}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <a href="{{url('courses/qa')}}/{{$data->id}}" class="recent-link">
                        <div class="recent-question">
                            Browse all questions
                        </div>
                    </a>
                @else
                     
                    <div class="recent-question">
                        <span class="header-recent">
                            No Recent Questions
                        </span>
                    </div>

                @endif        

                
               
            </div>

            <div class="col-sm-6">
                

                @if(isset($coursesAnnouncements) && count($coursesAnnouncements))
                    
                    <div class="recent-question">
                        <span class="header-recent">
                            Recent Instructor Announcements
                        </span>
                    </div>

                    @foreach($coursesAnnouncements as $k=>$v)
                        <div class="recent-question">
                            <div class="conversation-find conversation-find-over">
                                <div class="conversation-man">
                                   <?php
                                        $imgdata = \App\Helpers\General::userroundimage($curentuser);
                                        echo $imgdata;
                                    ?> 
                                </div>
                                <div class="conversation-name2">
                                    <p class="name-over"><b>{{$curentuser['v_name']}}</b> </p>
                                    <p>{{isset($v->l_announcements) ? $v->l_announcements : ''}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach  
                    
                    <a href="{{url('courses/announcements')}}/{{$data->id}}" class="recent-link">
                        <div class="recent-question">
                            Browse all announcements
                        </div>
                    </a>
                @else
                    
                    <div class="recent-question">
                        <span class="header-recent">
                           No Recent Instructor Announcements.
                        </span>
                    </div>

                @endif
               
            </div>

            <div class="col-xs-12">
                <div class="Description-heading">
                    <h4>Description</h4>
                    <p><?php 
                            if(isset($data->l_description)){
                                echo $data->l_description;    
                            }
                        ?>
                    </p>

                </div>
            </div>
            
            <div class="col-xs-12">
                <div class="Description-heading2">
                    <h4>About the Instructor</h4>
                </div>
            </div>

            <?php 
                $v_image="";
                if(count($data->hasUser()) && isset($data->hasUser()->v_image)){
                    $v_image=$data->hasUser()->v_image;
                }
                $v_fname="";
                if(count($data->hasUser()) && isset($data->hasUser()->v_fname)){
                    $v_fname=$data->hasUser()->v_fname;
                }

                $v_lname="";
                if(count($data->hasUser()) && isset($data->hasUser()->v_lname)){
                    $v_lname=$data->hasUser()->v_lname;
                }

                $v_verified="";
                $i_verified=0;
                
                if(count($data->hasUser()) && isset($data->hasUser()->e_email_confirm)){
                    $v_verified= ($data->hasUser()->e_email_confirm  == 'yes') ? 'verified' :  'not verified' ;
                    if($data->hasUser()->e_email_confirm  == 'yes'){
                        $i_verified=1;      
                    }
                }

                $i_course_total_avg_review="0";
                if(count($data->hasUser()) && isset($data->hasUser()->i_course_total_avg_review)){
                    $i_course_total_avg_review=$data->hasUser()->i_course_total_avg_review;
                }
                $i_course_total_review="0";
                if(count($data->hasUser()) && isset($data->hasUser()->i_course_total_review)){
                    $i_course_total_review=$data->hasUser()->i_course_total_review;
                }
               

                $v_replies_time="";
                if(count($data->hasUser()) && isset($data->hasUser()->v_replies_time)){
                    $v_replies_time = $data->hasUser()->v_replies_time;
                }
             ?> 
            
            <div class="col-xs-2">
                <div class="let-profile-fianl">
                    
                    <div class="let-profile">
                        @php
                            $v_image="";
                            if(isset($data->v_instructor_image) && $data->v_instructor_image!=""){
                                $v_image = \Storage::cloud()->url($data->v_instructor_image);
                            }else{
                                $v_image =  \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                            }
                        @endphp
                        <img src="{{$v_image}}" alt="" />
                    </div>
                    <div class="all-time-free">
                        <img src="{{url('public/Assets/frontend/images/clock-add.png')}}" alt="" style="max-width: 42px;"/>
                        <p>Replies in {{$v_replies_time}}</p>
                    </div>
                    <div class="all-time-free">
                        @if($i_verified)
                         <img src="{{url('public/Assets/frontend/images/right1.png')}}" alt="" style="max-width: 42px;"/>
                         @else
                         <img src="{{url('public/Assets/frontend/images/close-lot.png')}}" alt="" style="max-width: 42px;"/>
                        @endif
                        <p>Profile verified</p>
                    </div>
                    <div class="all-time-free">
                        <img src="{{url('public/Assets/frontend/images/classification.png')}}" alt="" style="max-width: 42px;" />
                        @php
                          $start =strtotime($data->hasUser()->d_added);
                          $end =  time();
                          $diff=$end - $start;
                          $days_between = round($diff / 86400);
                          if($days_between>30){
                              $days_between = round($days_between / 30);
                              $days_between = $days_between." Months";
                          }else{

                                if($days_between>1){
                                    $days_between = round($days_between)." days";
                                }else{
                                    $days_between = "1 day";
                                }
                          }
                         @endphp
                        <p>With Skillbox ({{$days_between}})</p>
                    </div>
                </div>
            </div>

            
            <div class="col-xs-10">
                <div class="about-instuctor">
                    <h4>{{isset($data->v_author_name) ? ucfirst($data->v_author_name):''}}</h4>
                    <p>&nbsp;</p>
                    {{-- <div class="rating-buyer">
                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$i_course_total_avg_review}}" readonly">
                        <span class="star-no"> {{number_format($i_course_total_avg_review, 1)}} <span class="review-point">( {{$i_course_total_review}} ) </span> </span>
                    </div> --}}
                    <div class="instuctor-text" style="margin: 00px 0px;">
                        <p>
                        <?php 
                            if(isset($data->l_instructor_details)){
                                echo nl2br($data->l_instructor_details);    
                            }
                        ?>
                        </p>
                    </div>

                </div>
            </div>
        </div>
        <!-- 26B-control-panel-my-courses-overview-Seller -->
    </div>
    <!--26b-control-panel-my-courses-overviwe-->

   

@stop
@section('js')

<script type="text/javascript">

function coursesStatus(data="",id=""){
      
        var actionurl = "{{url('courses/updatestatus')}}";
        var formdata = "id="+id+"&e_status="+data;
      
        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
               
                var obj = jQuery.parseJSON(res);
                
                if(obj['status']==1){
                    $("#commonmsg").html(obj['msg']);
                }else{
                    $('#commonerr').css("display","block");
                    $('#commonerrmsg').html(obj['msg']);
                    $("#commonmsg").html(obj['msg']);
                    $('#join').prop('checked', false);
                    $('#create').prop('checked', true);
                }
            },
            error: function ( jqXHR, exception ) {
                $('#commonerr').css("display","block");
            }
        });
    }

</script>

@stop

