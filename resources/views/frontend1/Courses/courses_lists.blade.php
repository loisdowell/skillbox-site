@extends('layouts.frontend')
@section('content')
<style type="text/css">
    .video-play .ytp-swatch-background-color { background-color: #e70e8a !important; }
    .category-img {    background: #000; border-bottom: 0px solid transparent; position: relative; top: auto;}
.category-img > img {height: 194px; width: 100%;}
.category-img > video {height: 194px; width: 100%;}
.category-img > video source {height: 194px; width: 100%; }
.controlerimg { position: absolute; width: 100%; left: 0%; top: 33%; text-align: center; cursor: pointer; }
.my-courses > div > div .full-size:nth-child(4n + 5){clear:both;}
@media(max-width:991px){
    .my-courses > div > div .full-size:nth-child(4n + 5){clear:unset;}
    .my-courses > div > div .full-size:nth-child(3n + 4){clear:both;}
}
@media(max-width:767px){
    .my-courses > div > div .full-size:nth-child(2n + 3){clear:both;}
    .my-courses > div > div .full-size:nth-child(3n + 4){clear:unset;}
}

</style>
    <!-- 26B-control-panel-buyer-account-freelance-online-EDIT-shortlisted-1 -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('seller/dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> My Courses </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="sort-for">
            <div class="sort-forep">
                <a href="javascript:;" onclick="viewchange('grid')">
                    <img  id="gridicon" src="{{url('public/Assets/frontend/images/fore.png')}}" class="fore-all" alt="" />
                </a>
                <a href="javascript:;" onclick="viewchange('list')">
                    <img  id="listicon" src="{{url('public/Assets/frontend/images/fore2.png')}}" alt="" />
                </a>
            </div>
        </div>
        <div class="btn-new-link">
            <a href="{{url('courses/add')}}" class="btn btn-primary">
                Create New Course
            </a>
        </div>
    </div>

    <div class="container">
        <div class="my-courses">
            
            <div class="row">

                @if ($success = Session::get('success'))
                  <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 15px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;success</strong>&nbsp;&nbsp;{{ $success }}
                  </div>
                @endif
                @if ($warning = Session::get('warning'))
                  <div class="alert alert-info alert-danger" style="margin-top: 15px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;{{ $warning }}
                  </div>
                @endif

                <div id="gridview" class="my-courses_listing">
                @if(count($data))
                    @foreach($data as $key=>$val)
                        <div class="col-xs-6 col-sm-4 col-md-3 full-size">
                            <div class="all-category all-category-final">
                                <a href="{{url('courses/view')}}/{{$val->id}}">
                                <div class="category-img" onclick="playPausevideogrid('{{$val->id}}')">
                                    @php
                                        $imgdata="";

                                        if(isset($val->v_course_thumbnail) && $val->v_course_thumbnail!=""){
                                            $imgdata = \Storage::cloud()->url($val->v_course_thumbnail); 
                                        }else{
                                            $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                        }
                                    @endphp
                                    <img src="{{$imgdata}}" alt="" />
                                </div>
                                </a>
                                
                                <div class="category-text">
                                    <a href="{{url('courses/view')}}/{{$val->id}}"><h2 style="min-height:36px;max-height:36px;overflow: hidden;">{{$val->v_title}}</h2></a>
                                    <p class="my_cs_listing_txt" style="min-height: 60px;max-height: 60px;overflow: hidden;">
                                        @if(isset($val->v_sub_title))
                                            @if(strlen($val->v_sub_title)>75)
                                                {{substr($val->v_sub_title,0,75)}}...
                                            @else
                                                {{$val->v_sub_title}}
                                            @endif
                                        @endif
                                     </p>   
                                    <div class="star-category">
                                        <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                                        <p class="text-star">
                                        {{number_format($val->i_total_avg_review,1)}}
                                           <span>({{$val->i_total_review}})
                                        </span></p>
                                    </div>
                                </div>
                                <div class="starting-status">
                                    <div class="status-bottom">
                                        @if(isset($val->e_status) & $val->e_status!="")
                                        {{$val->e_status}}
                                        @else
                                        Unpublished
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                <div class="all-leval">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-courses">
                                    <div class="row">
                                         <div class="col-xs-12 no-padding">
                                            <div class="center-courses-text">
                                                <center>
                                                <span>Currently, there are no courses available.</span>
                                                </center>
                                            </div>
                                          </div>      
                                    </div>
                                </div>        
                            </div>
                        </div>
                    </div>        
                @endif  
                </div>

                <div style="clear: both"></div>
                <div id="listview" class="listview-container" style="display: none;">
                    @if(count($data))
                        @foreach($data as $key=>$val)
                            <div class="all-leval " style="overflow: hidden;">
                                <div class="row height_container">
                                    <div class="col-sm-3">
                                       <a href="{{url('courses/view')}}/{{$val->id}}">
                                        <div class="category-img" onclick="playPausevideo('{{$val->id}}')">
                                            @php
                                                $imgdata="";

                                                if(isset($val->v_course_thumbnail) && $val->v_course_thumbnail!=""){
                                                    $imgdata = \Storage::cloud()->url($val->v_course_thumbnail); 
                                                }else{
                                                    $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                                }
                                            @endphp
                                            <img class="height_div" src="{{$imgdata}}" alt="" />
                                        </div>
                                        </a>
                                    </div>
                                    
                                    <div class="col-sm-9 height_div">
                                        <div class="center-courses">
                                            <div class="row">
                                                <div class="col-xs-12 no-padding">
                                                    <div class="center-courses-text">
                                                        <a href="{{url('courses/view')}}/{{$val->id}}">
                                                            <span>{{$val->v_title}}</span>
                                                            <p>&nbsp;</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-8 no-padding">
                                                    <div class="center-courses-text">
                                                        <p>
                                                            @if(isset($val->v_sub_title))
                                                                @if(strlen($val->v_sub_title)>150)
                                                                    {{substr($val->v_sub_title,0,150)}}...
                                                                @else
                                                                    {{$val->v_sub_title}}
                                                                @endif
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4">
                                                    <div class="rating-leval rating_inline">
                                                        <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$val->i_total_avg_review}}" readonly>
                                                        <p><span class="point">{{number_format($val->i_total_avg_review,1)}}
                                                           ({{$val->i_total_review}})
                                                         </span></p>
                                                    </div>
                                                    <button class="btn" style="float: right;width: 100%;max-width: 145px;background: #e8128c;color: #fff;font-weight: 900;">@if(isset($val->e_status) & $val->e_status!="")
                                                        {{$val->e_status}}
                                                        @else
                                                        Unpublished
                                                        @endif</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 no-padding">
                                                    <ul class="list-leval">
                                                        <li><span>{{isset($val->section) ? count($val->section):'0'}}</span> Lectures</li>
                                                        |
                                                        <li><span>{{$val->i_duration}}</span> Hours Duration</li>
                                                        |
                                                        <li><span>{{count( $val->hasLevel() ) ? $val->hasLevel()->v_title : ''}}</span> Level</li>
                                                        |
                                                        <li>{{count( $val->hasCategory() ) ? $val->hasCategory()->v_title : ''}} </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                   <div class="all-leval">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="center-courses">
                                    <div class="row">
                                         <div class="col-xs-12 no-padding">
                                            <div class="center-courses-text">
                                                <center>
                                                <span>Currently, there are no courses available.</span>
                                                </center>
                                            </div>
                                          </div>      
                                    </div>
                                </div>        
                            </div>
                        </div>
                    </div>        


                    @endif    
                </div>
            
            </div>
        </div>

        <div class="col-xs-12">
            <div class="center">
                {{ $data->links() }}
            </div>
        </div>
        
  
    </div>

   

@stop
@section('js')

<script type="text/javascript">

</script>

 <script type="text/javascript">
        
        
        
        function playPausevideogrid(data) { 
           
            var video = document.getElementById("videoPlayergrid"+data);

            if (video.paused)
            {
                video.play();
                if (video.play)
                {
                    $("#videoControlergrid"+data).hide();
                }
            }
            else
            {
                video.pause();
                if (video.pause)
                {
                    $("#videoControlergrid"+data).show();
                }
            }

        }


        function playPausevideo(data) { 
            var video = document.getElementById("videoPlayerlist"+data);
            
            if (video.paused)
            {
                video.play();
                if (video.play)
                {
                    $("#videoControlerlist"+data).hide();
                }
            }
            else
            {
                video.pause();
                if (video.pause)
                {
                    $("#videoControlerlist"+data).show();
                }
            }
        }

        function viewchange(data) {
            
            var imgpath = "{{url('public/Assets/frontend/images')}}";

            if(data=="grid"){
                $("#listview").css("display","none");
                $("#gridview").css("display","inline");

                var gridicon = imgpath+'/fore.png';
                var listicon = imgpath+'/fore2.png';
                $("#gridicon").attr("src",gridicon);
                $("#listicon").attr("src",listicon);

            }else{  
                
                $("#listview").css("display","block");
                $("#gridview").css("display","none");

                var gridicon = imgpath+'/fore33.png';
                var listicon = imgpath+'/fore22.png';
                $("#gridicon").attr("src",gridicon);
                $("#listicon").attr("src",listicon);
            }

        }


    </script>

@stop

