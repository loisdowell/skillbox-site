@extends('layouts.frontend')

@section('content')

    <style type="text/css">
        .letter-text-myprifile {
            text-align: center;
            font-size: 34px;
            padding: 10px 0px;
            color: rgb(231, 14, 138);
        }
        .new-seller-find {border-radius: 20px;
        margin-right: 10px;}
        .new-submit-live {height: 34px;margin-top: auto;}
        .new-submit-comment{display: inline-flex;width: 100%}
    </style>


    <!--26b-control-panel-my-courses-overviwe-->
    <div class="title-controlpanel controlpanel_setting_icon">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> My Courses Details </h1>
                </div>
                <div class="right-symbol">
                    <a href="{{url('courses/settings')}}/{{$data->id}}" class="setting-course">
                        <img src="{{url('public/Assets/frontend/images/setting-course.png')}}" class="setting-course" alt="" />
                    </a>
                    <a href="{{url('courses/messages')}}/{{$data->id}}"> <img src="{{url('public/Assets/frontend/images/mail.png')}}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-detail tabdetailplace">
        <div class="container">
            <ul class="nav nav-pills nav-pills-new">
                <li><a href="{{url('courses/view')}}/{{$data->id}}">Overview </a></li>
                <li><a href="{{url('courses/section')}}/{{$data->id}}">Course Sections </a></li>
                <li><a href="{{url('courses/qa')}}/{{$data->id}}">Q&A <span class="place-badge">{{isset($total['qa']) ? $total['qa'] : 0}}</span> </a></li>
                <li class="active"><a href="{{url('courses/reviews')}}/{{$data->id}}"> Course Reviews <span class="place-course">{{isset($total['reviews']) ? $total['reviews'] : 0}}</span> </a></li>
                <li><a href="{{url('courses/announcements')}}/{{$data->id}}">Announcements <span class="place-annou">{{isset($total['announcements']) ? $total['announcements'] : 0}}</span> </a></li>
            </ul>
        </div>
    </div>
    <!-- Tab panes -->

    <div class="container">
        <!-- 26B-control-panel-my-courses-leave-a-review-seller -->
        <div class="row">
            <div class="col-sm-6 ">
                <div class="Course-ans">
                    <div class="Course-final">
                        <div class="Course-text">
                            <h3>Course Reviews <span></span></h3>
                        </div>
                        <div class="rating-Course">
                            <div class="rating-buyer">
                                <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$totalavg}}">
                                <span class="star-no"> {{number_format($totalavg,1)}}  ( {{count($coursesReviews)}} )</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" col-sm-6">
                <div class="course-choise">
                    <div class="wrap selected-radiocourse">
                        <form>
                            <fieldset>
                                 <div class="toggle">
                                    <input type="radio" class="toggle-input" name="joincreate" onchange="coursesStatus(this.value,'{{$data->id}}')" value="published" id="join" @if(isset($data->e_status) && $data->e_status=='published') checked @endif >
                                    <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Published</label>
                                    <input type="radio" class="toggle-input" name="joincreate" onchange="coursesStatus(this.value,'{{$data->id}}')" value="unpublished" id="create" @if(isset($data->e_status) && $data->e_status=='unpublished') checked @endif>
                                    <label for="create" class="toggle-label toggle-label-on" id="createLabel">Unpublished</label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        @php 

        $totalInstructions=0;
        $totalNavigate=0;
        $totalrecommend=0;
        
        if(isset($coursesReviews) && count($coursesReviews)){
            foreach($coursesReviews as $key=>$val){
                $totalInstructions = $totalInstructions+$val->f_rate_instruction;
                $totalNavigate = $totalNavigate+$val->f_rate_navigate;
                $totalrecommend = $totalrecommend+$val->f_rate_reccommend;

            }  
            $totalInstructions = $totalInstructions/count($coursesReviews);
            $totalNavigate = $totalNavigate/count($coursesReviews);
            $totalrecommend = $totalrecommend/count($coursesReviews);
        }        
        @endphp            
        
        <div class="row">
        <div class="col-sm-2 text-center">
            Clear Instructions
            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalInstructions}}" readonly>
        </div>
        
        <div class="col-sm-2 text-center">
            Easy to Navigate
            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalNavigate}}" readonly>
        </div>
        <div class="col-sm-2 text-center">
            Would recommend
            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalrecommend}}" readonly>
        </div>
        </div>
        
        <div style="clear: both"></div>
        <br>

        <div id="commonmsg"></div>
        <div class="Course-Reviews">
            @if(isset($coursesReviews) && count($coursesReviews))        
                @foreach($coursesReviews as $key=>$val)
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="conversation-find">
                                <?php
                                    $imgdata="";
                                    if(isset($userList[$val->i_user_id])){
                                        $imgdata = \App\Helpers\General::userroundimage($userList[$val->i_user_id]);
                                    }
                                ?>
                                <div class="conversation-man">
                                    <?php 
                                        echo $imgdata;
                                    ?>    
                                </div>
                                <div class="conversation-name">
                                    <p>{{count($val->hasUser()) ? $val->hasUser()->v_fname.' '.$val->hasUser()->v_lname : '-'}}<span class="rating-buyer rating-Course-one">
                                     @php
                                    $ratedata = ($val->f_rate_instruction+$val->f_rate_navigate+$val->f_rate_reccommend)/3;
                                    @endphp
                                    <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$ratedata}}" ></span> {{number_format($ratedata,1)}}  </p>
                                    <p><span><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($val->d_added))->diffForHumans(); ?></span></p>
                                </div>
                            </div>
                            <div class="conversation-text">
                                <div class="course-review-all">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <p>{{isset($val->l_comment) ? $val->l_comment:''}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="commentstrans{{$val->id}}">
                            @if(isset($val->l_answers) && count($val->l_answers))
                            <div class="conversation-text extra-margin">
                                <div class="conversation-find conversation-find-course">
                                    
                                    <div class="conversation-man">
                                        <?php
                                            $imgdata="";
                                            if(isset($userList[$val->l_answers['i_user_id']])){
                                                $imgdata = \App\Helpers\General::userroundimage($userList[$val->l_answers['i_user_id']]);
                                            }
                                            echo $imgdata;
                                        ?>  
                                    </div>

                                    <div class="conversation-name">
                                        <p>
                                            <?php
                                                $username="";
                                                if(isset($userList[$val->l_answers['i_user_id']])){
                                                    $username = $userList[$val->l_answers['i_user_id']]['v_name'];
                                                }
                                                echo $username;
                                            ?>   
                                        <span class="rating-buyer rating-Course-one">
                                        </span></p>
                                        <p><span><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($val->l_answers['d_added']))->diffForHumans(); ?></span></p>
                                    </div>
                                </div>

                                <div class="conversation-text conversation-text-final">
                                    <div class="conversation-text-all conversation-text-course">
                                        <p class="response-seller"> {{isset($val->l_answers['l_comment']) ? $val->l_answers['l_comment'] :'' }} </p>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="col-xs-12">
                                <div class="conversation-second-man second-course">
                                    <div class="conversation-find courses-reviews-noborder">
                                        <div class="conversation-man">
                                            <?php
                                                $imgdata = \App\Helpers\General::userroundimage($curentuser);
                                                echo $imgdata;
                                            ?> 
                                        </div>
                                        
                                        <form method="POST" name="commnetform{{$val->id}}" id="commnetform{{$val->id}}" data-parsley-validate enctype="multipart/form-data" style="width: 100%" onsubmit="submitcomment('{{$val->id}}')" action="javascript:;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="i_course_reviews_id" value="{{$val->id}}">
                                        <div class="conversation-name1 conversation-name-q">
                                            <p>{{$curentuser['v_name']}}</p>
                                            <div class="new-submit-comment">
                                            <input type="text" class="form-control comment-seller new-seller-find" name="l_comment" placeholder="Enter Your Comment">
                                            <button type="button" onclick="submitcomment('{{$val->id}}')" class="btn btn-convarsation-submit new-submit-live"> Submit </button>
                                            </div>
                                        </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            @endif
                            </div>

                        </div>
                    </div>
                @endforeach
            @else
                
                <div class="row">
                    <div class="col-md-6">
                          No Reviews.
                    </div>
                </div>

            @endif
        </div>
        <!-- 26B-control-panel-my-courses-leave-a-review-seller -->
    </div>
    <!--26b-control-panel-my-courses-overviwe-->

   

@stop
@section('js')

<script type="text/javascript">

    $(document).ready(function() {
            $('.accordion').find('.accordion-toggle1').click(function() {
                $(this).next().slideToggle('600');
                $(".accordion-content").not($(this).next()).slideUp('600');
            });
            $('.accordion-toggle1').on('click', function() {
                $(this).toggleClass('active').siblings().removeClass('active');
            });
        });

    function submitcomment(id=""){
       
        var actionurl = "{{url('courses/post-reviews')}}";
        var formValidFalg = $("#commnetform"+id).parsley().validate('');

        if(formValidFalg){
              document.getElementById('load').style.visibility="visible";           
            var formdata = $("#commnetform"+id).serialize();

            $.ajax({
                type    : "POST",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    document.getElementById('load').style.visibility="hidden";
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $('#commentstrans'+id).html(obj['commentstr']);
                    }else{
                        $('#commonerr').css("display","block");
                        $('#commonerrmsg').html(obj['msg']);
                    }
                    
                },
                
                error: function ( jqXHR, exception ) {
                     document.getElementById('load').style.visibility="hidden";
                    $('#commonerr').css("display","block");
                }

            });
        }
    }

    function coursesStatus(data="",id=""){
      
        var actionurl = "{{url('courses/updatestatus')}}";
        var formdata = "id="+id+"&e_status="+data;
       document.getElementById('load').style.visibility="visible";
        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                document.getElementById('load').style.visibility="hidden";
                var obj = jQuery.parseJSON(res);
                
                if(obj['status']==1){
                    $("#commonmsg").html(obj['msg']);
                }else{
                    $('#commonerr').css("display","block");
                    $('#commonerrmsg').html(obj['msg']);
                    $("#commonmsg").html(obj['msg']);
                    $('#join').prop('checked', false);
                    $('#create').prop('checked', true);
                }
            },
            error: function ( jqXHR, exception ) {
                 document.getElementById('load').style.visibility="hidden";
                $('#commonerr').css("display","block");
            }
        });
    }

</script>

@stop

