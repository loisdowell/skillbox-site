@extends('layouts.frontend')

@section('content')

 <style type="text/css">
        .letter-text-myprifile {
            text-align: center;
            font-size: 34px;
            padding: 10px 0px;
            color: rgb(231, 14, 138);
        }
        .new-seller-find {border-radius: 20px;
        margin-right: 10px;}
        .new-submit-live {height: 34px;}
        .new-submit-comment{display: inline-flex;width: 100%}
    </style>


  <?php 
        $ciurseuserdata="";
        if(count($data->hasUser()) && isset($data->hasUser()->v_image)){
            $ciurseuserdata =$data->hasUser();
        }
        
        $v_fname="";
        if(count($data->hasUser()) && isset($data->hasUser()->v_fname)){
            $v_fname=$data->hasUser()->v_fname;
        }

        $v_lname="";
        if(count($data->hasUser()) && isset($data->hasUser()->v_lname)){
            $v_lname=$data->hasUser()->v_lname;
        }

    ?> 

   

   <!--26b-control-panel-my-courses-overviwe-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> My Courses Details </h1>
                </div>
            </div>
        </div>
    </div>


    <div class="tab-detail">
        <div class="container">
            <ul class="nav nav-pills nav-pills-new">
                <li><a href="{{url('buyer/courses/overview')}}/{{$buyercoursedata->id}}">Overview </a></li>
                <li><a href="{{url('buyer/courses/section')}}/{{$buyercoursedata->id}}">Course Content</a></li>
                @if(isset($data->e_qa_enabled) && $data->e_qa_enabled=="on")
                <li><a href="{{url('buyer/courses/qa')}}/{{$buyercoursedata->id}}">Q&A</a></li>
                @endif
                
                @if(isset($data->e_reviews_enabled) && $data->e_reviews_enabled=="on")
                <li><a href="{{url('buyer/courses/reviews')}}/{{$buyercoursedata->id}}"> Course Reviews</a></li>
                @endif

                @if(isset($data->e_announcements_enabled) && $data->e_announcements_enabled=="on")
                <li class="active"><a href="{{url('buyer/courses/announcements')}}/{{$buyercoursedata->id}}">Announcements</a></li>
                @endif

            </ul>
        </div>
    </div>

    <div class="container">
        <div class="timeline-title">
            <div class="row">
                <div class="col-sm-4">
                    <h3>Announcements Timeline</h3>
                </div>
            </div>
        </div>
        <div id="commonmsg"></div>
         <div class="alert alert-info alert-danger" style="display: none;" id="commonerr">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;Something went wrong.Please try again after sometime.
          </div>            

        <div class="Announcements-content">
            @if(isset($coursesAnnouncements) && count($coursesAnnouncements))
                @foreach($coursesAnnouncements as $k=>$v)
                    <div class="accordion-final-announc">
                        <div class="accordion accordion-announcement">
                            <div class="accordion-toggle1 @if($k==0) active @endif">
                                <div class="Announcements-find edit-position">
                                    <div class="Announcements-man">
                                        <?php
                                            $imgdata = \App\Helpers\General::userroundimage($ciurseuserdata);
                                            echo $imgdata;
                                        ?> 
                                    </div>
                                    <div class="Announcements-name">
                                        <p>{{$v_fname}} {{$v_lname}}</p>
                                        <p><span><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($v->d_added))->diffForHumans(); ?></span></p>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-content" @if($k==0) style="display: block" @endif>

                                <div class="Announcements-text Announcements-space" id="announcementscomments{{$v->id}}">
                                    
                                    <div class="Announcements-text-all">
                                        <h3 id="v_title_{{$v->id}}" >{{isset($v->v_title) ? $v->v_title : ''}}</h3>
                                        <p id="l_announcements_{{$v->id}}">{{isset($v->l_announcements) ? $v->l_announcements : ''}}</p>
                                    </div>
                                        
                                    @if(isset($v->l_comments) && count($v->l_comments))        
                                        @foreach($v->l_comments as $key=>$val)
                                            <div class="Announcements-second-man">
                                                <div class="Announcements-find">
                                                    <div class="Announcements-man">
                                                        <?php
                                                            $imgdata="";
                                                            if(isset($userList[$val['i_user_id']])){
                                                                $imgdata = \App\Helpers\General::userroundimage($userList[$val['i_user_id']]);
                                                            }
                                                            echo $imgdata;
                                                        ?>    
                                                    </div>
                                                    <div class="Announcements-name">
                                                        <p>
                                                        @php
                                                            $username="";
                                                            if(isset($userList[$val['i_user_id']])){
                                                                $username = $userList[$val['i_user_id']]['v_name'];
                                                            }
                                                            echo $username;
                                                        @endphp
                                                        </p>
                                                        <p><span><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($val['d_added']))->diffForHumans(); ?></span></p>
                                                    </div>
                                                </div>

                                                <div class="Announcements-text">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="Announcements-text-seller">
                                                                <p >
                                                                   {{$val['v_text']}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif        

                                </div>

                                <form role="form" method="POST" name="commnetform{{$v->id}}" id="commnetform{{$v->id}}" data-parsley-validate enctype="multipart/form-data" onsubmit="submitcomment('{{$v->id}}')" action="javascript:;">
                                {{ csrf_field() }}
                                <input type="hidden" name="i_announcements_id" value="{{$v->id}}">
                                <div class="Announcements-input">
                                    <div class="Announcements-find Announcements-find-q">
                                        <div class="Announcements-man">
                                            <?php
                                                $imgdata = \App\Helpers\General::userroundimage($curentuser);
                                                echo $imgdata;
                                            ?> 
                                        </div>
                                        <div class="Announcements-name Announcements-name-q">
                                            <p>{{$curentuser['v_name']}}</p>
                                            <div class="new-submit-comment">
                                            <input type="text" class="form-control comment-seller new-seller-find " name="v_text" id="v_text{{$v->id}}" placeholder="Enter your comment" required>
                                            <button type="button" onclick="submitcomment('{{$v->id}}')" class="btn btn-convarsation-submit new-submit-live"> Submit </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>


                            </div>
                        </div>
                    </div>

                    


                @endforeach

            @else
                    <div class="Course-Reviews">
                         <div class="row">
                            <div class="col-md-6">
                                 There are no announcements.
                            </div>
                        </div>
                    </div>    
            @endif        

        </div>
        <!-- 26B-control-panel-my-courses-announcement-seller -->
    </div>
    <!--26b-control-panel-my-courses-overviwe-->   

@stop
@section('js')

<script type="text/javascript">

        $(document).ready(function() {
            $('.accordion').find('.accordion-toggle1').click(function() {
                $(this).next().slideToggle('600');
                $(".accordion-content").not($(this).next()).slideUp('600');
            });
            $('.accordion-toggle1').on('click', function() {
                $(this).toggleClass('active').siblings().removeClass('active');
            });
        });

            
       function submitcomment(id=""){
       
        var actionurl = "{{url('buyer/courses/comments-announcements')}}";
        var formValidFalg = $("#commnetform"+id).parsley().validate('');
        
        if(formValidFalg){
            
            document.getElementById('load').style.visibility="visible";            
            var formdata = $("#commnetform"+id).serialize();

            $.ajax({
                type    : "POST",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                   document.getElementById('load').style.visibility="hidden";
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $('#announcementscomments'+id).append(obj['commentstr']);
                        $("#v_text"+id).val("");
                    }else{
                        $('#commonerr').css("display","block");
                        $('#commonerrmsg').html(obj['msg']);
                    }
                },
                
                error: function ( jqXHR, exception ) {
                    $('#commonerr').css("display","block");
                }

            });
        }
    }

</script>

@stop

