@extends('layouts.frontend')

@section('content')
<style type="text/css">
    .issue-img{
        height: auto !important;
    }
    .issue-rightimg{
        height: auto !important;
    }
</style>
@php
    
    $issue = App\Helpers\General::ResolutionProblem();    
@endphp

    <!-- 26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                   <a href="{{url('buyer/orders/detail')}}/{{$data->id}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Order Detail</button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> Resolution Center </h1>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="tab-detail">
        <div class="container">
            <ul class="nav nav-pills nav-resolution-new">
                <li class="">
                    <a href="26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS-resolution-center-W1.html"> 1. SELECT ISSUE </a>
                </li>
                <li class="active">
                    <a href="26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS-resolution-center-W2.html"> 2. SELECT RESOLUTION </a>
                </li>
                <li>
                    <a href="26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS-resolution-center-W3.html"> 3. SUBMIT RESOLUTION </a>
                </li>
            </ul>
        </div>
    </div>

    <!-- Tab panes -->
    <div class="container">
        <div id="content">
            <div id="my-tab-content" class="tab-content">

                <!-- 26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS-resolution-center-W1 -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="outer-border">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="resolve-issue">
                                        <p>
                                            The issue You've Selected Is : {{$issue[$stepdata['i_issue_id']]}}
                                        </p>
                                    </div>
                                    <form class="horizontal-form" role="form" method="POST" name="submitreq" id="submitreq" action="{{url('resolution/step3')}}" data-parsley-validate enctype="multipart/form-data" >
                                    {{ csrf_field() }}
                                    <input type="hidden" name="i_order_id" value="{{$data->id}}"> 
                                    <input type="hidden" name="i_issue_id" value="{{$stepdata['i_issue_id']}}"> 
                                    <div class="order-issue">
                                        <span>
                                            How would you like to resolve this issue?
                                        </span>

                                            <ul class="list-unstyled listed-status">
                                                <li>
                                                    <div class="reg">
                                                        <bdo dir="ltr">
                                                            <input type="radio" value="1" name="i_issue_step">
                                                            <span></span>
                                                            <abbr> Ask the seller to cancel this order </abbr>
                                                        </bdo>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="reg">
                                                        <bdo dir="ltr">
                                                            <input type="radio" value="0" name="i_issue_step">
                                                            <span></span>
                                                            <abbr> Contact customer support </abbr>
                                                        </bdo>
                                                    </div>
                                                </li>
                                            </ul>
                                    </div>

                                    <div class="right-btn">
                                        <button type="submit" class="btn btn-nextorder" >Continue</button>
                                    </div>
                                    
                                    <div class="clearfix">
                                    </div>
                                    </form>

                                </div>
                                
                                <div class="col-sm-4">
                                    <div class="issue-right">
                                        <div class="issue-img">
                                            @php
                                                $imgdata="";
                                            @endphp
                                            @if(isset($sellerprofile) && count($sellerprofile))
                                                @if(isset($sellerprofile->v_work_photos) && isset($sellerprofile->v_work_photos[0]))
                                                    @php
                                                        $imgdata = \Storage::cloud()->url($sellerprofile->v_work_photos[0]);    
                                                    @endphp
                                                @else
                                                    @php
                                                        $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');    
                                                    @endphp
                                                @endif
                                            @else
                                                @php
                                                    $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');    
                                                @endphp    
                                            @endif    
                                            <img class="img-responsive issue-rightimg" src="{{$imgdata}}" alt="" />
                                        </div>
                                        
                                        <div class="status-order">
                                            <div class="magnetic-word">
                                                {{isset($data->v_profile_title) ? $data->v_profile_title : ''}}
                                            </div>

                                            <div class="magnetic-option">
                                                <ul class="list-unstyled">
                                                    

                                                    @if(isset($sellerprofile->information[$package]['v_bullets']) && count($sellerprofile->information[$package]['v_bullets']))

                                                        @foreach($sellerprofile->information[$package]['v_bullets'] as $k=>$v)
                                                                <li><img src="{{url('public/Assets/frontend/images/right-issue.png')}}" class="img-issueright" alt="" />{{$v}}</li>
                                                        @endforeach

                                                    @endif

                                                    
                                                </ul>
                                            </div>

                                            <div class="overview-issue">
                                                <div class="info-aboutstatus">
                                                    <div class="status-grp">
                                                        Status
                                                    </div>
                                                    <span class="delivered-color">{{isset($data->v_order_status) ? $data->v_order_status : ''}}</span>
                                                </div>
                                                <div class="info-aboutstatus">
                                                    <div class="status-grp">
                                                        Order
                                                    </div>
                                                    <span class="statusinfo-color"> #{{isset($data->i_order_no) ? $data->i_order_no : ''}}</span>
                                                </div>
                                                <div class="info-aboutstatus">
                                                    <div class="status-grp">
                                                        Order Date
                                                    </div>
                                                    <span class="statusinfo-color">{{isset($data->d_date) ? date("M d, Y ",strtotime($data->d_date)) : ''}} </span>
                                                </div>
                                                
                                                <div class="info-aboutstatus">
                                                    <div class="status-grp">
                                                        Quantity
                                                    </div>
                                                    
                                                    @if(isset($data->v_order_detail[0]) && count($data->v_order_detail[0]))
                                                    <span class="statusinfo-color">{{$data->v_order_detail[0]['qty']}}</span>
                                                    @endif

                                                </div>
                                                <div class="info-aboutstatus">
                                                    <div class="status-grp">
                                                        Price
                                                    </div>
                                                    @if(isset($data->v_order_detail[0]) && count($data->v_order_detail[0]))
                                                    <span class="statusinfo-color"> £{{$data->v_order_detail[0]['price']}}</span>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS -->

     
        
   

@stop
@section('js')
<script type="text/javascript">
  
</script>
@stop

