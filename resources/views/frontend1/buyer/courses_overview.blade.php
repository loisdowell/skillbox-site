@extends('layouts.frontend')

@section('content')

   <!--26B-control-panel-my-courses-1 -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                     <a href="{{url('buyer/courses')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to My Courses </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> My Courses </h1>
                </div>
            </div>
        </div>
    </div>

     <div class="tab-detail">
        <div class="container">
            <ul class="nav nav-pills nav-pills-new">
                <li class="active"><a href="{{url('buyer/courses/overview')}}/{{$data->id}}">Overview </a></li>
                <li><a href="{{url('buyer/courses/section')}}/{{$data->id}}">Course Content</a></li>
                
                @if(isset($cdata->e_qa_enabled) && $cdata->e_qa_enabled=="on")
                <li><a href="{{url('buyer/courses/qa')}}/{{$data->id}}">Q&A</a></li>
                @endif
                
                @if(isset($cdata->e_reviews_enabled) && $cdata->e_reviews_enabled=="on")
                <li><a href="{{url('buyer/courses/reviews')}}/{{$data->id}}"> Course Reviews</a></li>
                @endif

                @if(isset($cdata->e_announcements_enabled) && $cdata->e_announcements_enabled=="on")
                <li><a href="{{url('buyer/courses/announcements')}}/{{$data->id}}">Announcements</a></li>
                @endif
                
             </ul>
        </div>
    </div>

    <!-- Tab panes -->
    <div class="container">

        <!--26b-control-panel-my-courses-overview-->
        <div class="Overview-box">
            <div class="row">
                <div class="col-sm-6 text-center-xs">
                    <div class="Overview-heading">
                        <h3>{{count( $data->hasCourse() ) ? $data->hasCourse()->v_title : ''}}</h3><span><img src="{{url('public/Assets/frontend/images/clock1.png')}}" alt="" />{{count( $data->hasCourse() ) ? $data->hasCourse()->i_duration : ''}} Hours</span>
                    </div>
                    
                    <div class="lecture">
                        <div class="lecture-button">
                            
                            @if($data->v_total_lectures>$data->v_complete_lectures)
                            <a href="{{url('buyer/courses/section')}}/{{$data->id}}?lact={{$data->v_complete_lectures+1}}">
                            <button class="btn btn-lecture">Continue to Lecture {{$data->v_complete_lectures+1}}</button>
                            </a>
                            @endif

                            <div class="rating-buyer">
                                @php
                                    $avgreview=0;
                                    if($total['reviews']!=0){
                                        $avgreview = $total['ratedata']/$total['reviews'];
                                    }
                                @endphp 
                                <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{count( $data->hasCourse() ) ? $data->hasCourse()->review['avgtotal'] : ''}}" readonly>
                                <span class="star-no"> {{count( $data->hasCourse() ) ? number_format($data->hasCourse()->i_total_avg_review,1) : ''}} ( {{count( $data->hasCourse() ) ? $data->hasCourse()->i_total_review : ''}} )
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 text-center-xs">
                    <div class="cup-page">
                        <img src="{{url('public/Assets/frontend/images/cup1.png')}}" alt="" />
                    </div>
                     @php
                            $percent = 0;
                            if( $data->v_complete_lectures ){
                                $percent = ceil( $data->v_complete_lectures * 100  / $data->v_total_lectures );
                            } 
                    @endphp

                    <div class="progress-data1">
                        <div class="progress-data ">
                            <div id="myBar" class="prosess-progress" style="width:{{$percent}}%"></div>
                        </div>
                        <p>{{$data->v_complete_lectures}} of {{ $data->v_total_lectures}} Lectures Completed</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12">
                <div class="active-recent-fianl">
                    <div class="active-recent">
                        <h3>Recent Activity</h3>
                    </div>
                    <div class="active-recent1">
                        <a href="{{url('buyer/invoice/course/download')}}/{{$data->v_order_id}}">
                            <img src="{{url('public/Assets/frontend/images/download-invoice.png')}}" alt="image"><span> DOWNLOAD INVOICE </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                
                <div class="recent-question">
                    Recent Questions
                </div>

                @if(isset($coursesQA) && count($coursesQA))
                    @foreach($coursesQA as $key=>$val)
                        
                         <div class="recent-question">
                            <div class="conversation-find conversation-find-over">
                                <div class="conversation-man">
                                    <?php
                                        $imgdata="";
                                        if(isset($userList[$val->i_user_id])){
                                            $imgdata = \App\Helpers\General::userroundimage($userList[$val->i_user_id]);
                                        }
                                        echo $imgdata;
                                    ?>

                                </div>
                                <div class="conversation-name2">
                                    <p>{{$val->l_questions}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <a href="{{url('buyer/courses/qa')}}/{{$data->id}}" class="recent-link">
                        <div class="recent-question">
                            Browse all questions
                        </div>
                    </a>

                 @else
                     
                    <div class="recent-question">
                        <span class="header-recent">
                            No Recent Questions
                        </span>
                    </div>
                        
                @endif    

            </div>
            <div class="col-sm-6">
                
                <div class="recent-question">
                    Recent Instructor Announcements
                </div>
                
                @if(isset($coursesAnnouncements) && count($coursesAnnouncements))
                    @foreach($coursesAnnouncements as $k=>$v)
                        
                        <div class="recent-question">
                            <div class="conversation-find conversation-find-over">
                                <div class="conversation-man">
                                    
                                    @php
                                        $imgdata="";
                                        if(isset($userList[$v['i_user_id']])){
                                            $imgdata = \App\Helpers\General::userroundimage($userList[$v['i_user_id']]);
                                        }
                                        echo $imgdata;
                                    @endphp    
                                    
                                </div>
                                <div class="conversation-name2">
                                    <p class="name-over"><b>{{$v->v_title}}</b> </p>
                                    <p>{{$v->l_announcements}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach  
                        <a href="{{url('buyer/courses/announcements')}}/{{$data->id}}" class="recent-link">
                            <div class="recent-question">
                                Browse all announcements
                            </div>
                        </a>
                @else 

                         
                    <div class="recent-question">
                        <span class="header-recent">
                           No Recent Instructor Announcements.
                        </span>
                    </div>

                @endif
                
            </div>
            


            <div class="col-xs-12">
                <div class="Description-heading">
                    <h4>Description</h4>
                     <p>
                        <?php 
                            if(count( $data->hasCourse()) && isset($data->hasCourse()->l_description)){
                                echo $data->hasCourse()->l_description; 
                            }
                        ?>
                    </p>

                </div>
            </div>

             <?php 
                $v_image="";
                if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_image)){
                    $v_image=$cdata->hasUser()->v_image;
                }
                $v_fname="";
                if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_fname)){
                    $v_fname=$cdata->hasUser()->v_fname;
                }

                $v_lname="";
                if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_lname)){
                    $v_lname=$cdata->hasUser()->v_lname;
                }

                $i_course_total_avg_review="0";
                if(count($cdata->hasUser()) && isset($cdata->hasUser()->i_course_total_avg_review)){
                    $i_course_total_avg_review=$cdata->hasUser()->i_course_total_avg_review;
                }
                $i_course_total_review="0";
                if(count($cdata->hasUser()) && isset($cdata->hasUser()->i_course_total_review)){
                    $i_course_total_review=$cdata->hasUser()->i_course_total_review;
                }


                $v_replies_time="";
                if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_replies_time)){
                    $v_replies_time = $cdata->hasUser()->v_replies_time;
                }
               
            ?> 

            <div class="col-xs-12">
                <div class="Description-heading2">
                    <h4>About the Instructor</h4>
                </div>
            </div>

            <div class="col-xs-2">
                <div class="let-profile-fianl">
                    <div class="let-profile">

                        @php
                            $imgins="";
                            if(count($data->hasCourse()) && isset($data->hasCourse()->v_course_thumbnail) && $data->hasCourse()->v_course_thumbnail!=""){
                                $imgins = $data->hasCourse()->v_course_thumbnail;
                            }else{
                                $imgins = 'jobpost/photos/No_Image_Available.jpg';
                            }
                            $imgdata = \Storage::cloud()->url($imgins);
                            $profileData = App\Helpers\General::getProfileData($data->hasCourse()->i_user_id);
                        @endphp
                        <img src="{{$imgdata}}" />
                    </div>
                    
                    <div class="all-time-free">
                        <img src="{{url('public/Assets/frontend/images/clock-add.png')}}" alt="" style="max-width: 42px;"/>
                        <p>Replies in {{$v_replies_time}}</p>
                    </div>
                    
                    <div class="all-time-free">
                        @if($profileData['profile']=="yes")
                        <img src="{{url('public/Assets/frontend/images/right-lot.png')}}" alt="" style="max-width: 42px;"/>
                        <p>Profile verified</p>
                        @else
                        <img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" alt="" style="max-width: 42px;"/>
                        <p>Profile not verified</p>
                        @endif
                    </div>

                    <div class="all-time-free">
                        <img src="{{url('public/Assets/frontend/images/classification.png')}}" alt="" style="max-width: 42px;"/>
                        <p>With Skillbox ({{$profileData['withskillbox']}})</p>
                    </div>

                </div>
            </div>

            @php
                $reviewseller = App\Helpers\General::getCourseSellerReview($data->hasCourse()->i_user_id);
            @endphp
           
            <div class="col-xs-10">
                <div class="about-instuctor">
                    <h4>{{count( $data->hasCourse() ) ? ucfirst($data->hasCourse()->v_author_name) : ''}}</h4>
                    <p>&nbsp;</p>
                    {{-- <div class="rating-buyer">
                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$i_course_total_avg_review}}" readonly">
                        <span class="star-no"> {{number_format($i_course_total_avg_review, 1)}} <span class="review-point">( {{$i_course_total_review}} ) </span> </span>
                    </div> --}}
                    <div class="instuctor-text" style="margin: 0px 0px; ">
                        <p>
                            <?php 
                                if(count( $data->hasCourse()) && isset($data->hasCourse()->l_instructor_details)){
                                    echo $data->hasCourse()->l_instructor_details; 
                                }
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--End 26b-control-panel-my-courses-overview-->
    </div>


@stop
@section('js')
<script type="text/javascript">


function move() {
    
    var elem = document.getElementById("myBar");
    var width = 101;
    var id = setInterval(frame, 10);

    function frame() {
        if (width >= 100) {
            clearInterval(id);
        } else {
            width++;
            elem.style.width = width + '%';
            document.getElementById("demo").innerHTML = width * 1 + '%';
        }
    }

}
  
</script>
@stop

