@extends('layouts.frontend')

<?php
// echo "<pre>";
// print_r($jobs_data);
// exit;
 
?>

@section('content')

<style type="text/css">
    .highimgshortlist{
        max-width: 26px;
        margin-top: -4px;
    }
</style>
<div class="modal fade" id="sendMessage" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 600px">
            <div class="modal-content modal-content-1">
                <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> Contact Seller</h4>
                </div>
                <form role="form" method="POST" name="sendMessageForm" id="sendMessageForm" data-parsley-validate enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="i_to_id" id="i_to_id_message" value="">
                <div class="modal-body">
                    <div class="modal-body-1" style="padding: 0px 30px !important;" id="contactsellerdetail">
                        <div class="row">
                            <div id="popupcommonmsg"></div>
                            <div class="col-xs-12">
                              <div class="text-box-containt">
                                    <label> Subject </label>
                                    <input type="text" class="form-control popup-letter" name="v_subject_title" required>
                                </div>
                            </div>    
                            <div class="col-xs-12">
                                <div class="text-box-containt">
                                    <label> Enter Your Message </label>
                                    <textarea rows="3" name="l_message" class="form-control popup-letter" required></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button  type="button" onclick="submitMessage()" class="btn btn-Submit-pop">Send Message</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
</div>


<div id="sendMessageSuccess" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;">Message</h4>
      </div>
      <div class="modal-body">
        <p id="popupmessage" style="text-align: center;"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




{{-- <div class="modal fade" id="sendMessageSuccess" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-content-1">
            <button type="button" class="close closemodal" data-dismiss="modal"></button>
            <div class="modal-header modal-header-1">
                <h4 class="modal-title" id="popupmessage">Your message has sent successfully.</h4>
            </div>
        </div>
    </div>
</div> --}}



   <!-- 26B-control-panel-buyer-account-shortlisted-skills-1 -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/dashboard')}}"><button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button></a>
                </div>
                <div class="title-support">
                    <h1> Applied Jobs </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="main-shortlisted">
            <div id="commonmsg" style="margin-top: 20px"></div>
            <?php 
             //echo "<pre>";
             //print_r($seller_data);
             //exit;
            ?>
            @if(isset($appliedJobs) && count($appliedJobs))
                <div class="edit-btn">
                    <button type="button" class="btn edit-short" onclick="fn_delete()"> Delete Selected </button>
                    <button type="button" class="btn btn-selectall" onclick="fn_select_all()"> Select All </button>
                </div>
                @foreach($appliedJobs as $k=>$v)
                    <?php 
                    $v_image="";
                    if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
                        $v_image=$v->hasUser()->v_image;
                    }

                    $sellerdata=array();
                    if(count($v->hasSellerProfile())){
                        $sellerdata = $v->hasSellerProfile();
                    }

                    $jobdata=array();
                    if(count($v->hasJob())){
                        $jobdata = $v->hasJob();
                    }

                    ?>

                    <div class="editshort-list" id="editshort-list_{{$v->_id}}">
                        <div class="border-space">
                            <div class="row">
                                <div class="col-sm-3 col-xs-12">
                                    <div class="header-dirservice-find">
                                        <div class="Shortlisted-img-person">
                                            @php
                                            if(Storage::disk('s3')->exists($v_image)){
                                                $imgdata = \Storage::cloud()->url($v_image);
                                            }else{
                                                $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                            }
                                            @endphp  
                                            <img src="{{$imgdata}}" class="img-category" alt="" />
                                        </div>
                                        <div class="squaredChk">
                                            <input type="checkbox" id="squaredChk_{{$v->_id}}" name="shortlisted_ids[]" value="{{$v->_id}}" />
                                            <label for="squaredChk_{{$v->_id}}"></label>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-9 col-xs-12">
                                    <div class="category-heading ">
                                        <div class="row">
                                            <div class="col-sm-8 col-xs-12 ">
                                                <h4>{{isset($sellerdata->v_profile_title) ? $sellerdata->v_profile_title:''}}</h4>
                                                <img src="{{url('public/Assets/frontend/images/lira.png')}}" class="img-category-list highimgshortlist" alt="" />
                                                
                                                @if(isset($sellerdata->v_hourly_rate) && $sellerdata->v_hourly_rate!="")
                                                <span class="info"> £{{isset($sellerdata->v_hourly_rate) ? $sellerdata->v_hourly_rate:''}}  HOURLY RATE</span>
                                                @endif

                                                <img src="{{url('public/Assets/frontend/images/strength.png')}}" class="img-category-list highimgshortlist" alt="" />
                                                <span class="info"> Experience Level:<strong style="text-transform: uppercase;"> {{isset($sellerdata->v_experience_level) ? $sellerdata->v_experience_level:''}}
                                            </strong> </span>
                                            </div>
                                            <div class="col-sm-4 col-xs-12 ">
                                                <div class="heading-right">
                                                    <span class="sponsored"> {{( isset($sellerdata->e_sponsor_status) && $sellerdata->e_sponsor_status == 'yes') ? 'Sponsored Post' : ''}} </span>

                                                    <button type="button" class="btn btn-sponsored" onclick="SendMessage('{{$v->i_user_id}}','{{$sellerdata->id}}')">CONTACT</button>
                                                    {{-- <button type="button" class="btn btn-sponsored">
                                                        CONTACT</button> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Shortlisted-text">
                                        <p>
                                        <?php 
                                        $l_short_description = substr(isset($sellerdata['l_short_description']) ? $sellerdata['l_short_description'] : $sellerdata['l_short_description'] , 0, 210);
                                        ?>
                                        {{$l_short_description or ''}}    
                                        </p>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="info" style="margin-left: 16px;margin-top: 10px;padding-top: 10px;display: inline-block;">Applied for: {{isset($jobdata->v_job_title) ? $jobdata->v_job_title:''}}</span>
                        @php
                            $title =App\Helpers\General::TitleSlug($sellerdata->v_profile_title);
                        @endphp
                        <a href="{{url('online')}}/{{$sellerdata->id}}/{{$title}}?id={{$sellerdata->id}}">
                        <button type="button" class="btn btn-detail"> Details </button></a>
                        
                        <div class="clearfix">
                        </div>
                    </div>

                @endforeach
            @else
             <div class="editshort-list">
                    <div class="border-space">
                        <div class="row" style="text-align: center;">
                            No applied jobs found.
                        </div>
                    </div>    
                </div>     

            @endif    
            
        </div>
    </div>
    <!-- End 26B-control-panel-buyer-account-shortlisted-skills-1 -->

<!--- start delete model -->
<div class="modal fade" id="commentdeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title custom-font">Confirm Delete Record ?</h3>
      </div>
      <div class="modal-body" style="text-align:center">
          <i class="margin-top-10 fa fa-question fa-5x"></i>
          <h4>Delete !</h4>
          <p>Are you sure you want to delete this record?</p>
      </div>
      <input type="hidden" name="commentdeleteId[]" id='commentdeleteId'>
      <div class="modal-footer" style="padding-top: 20px;">
        <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" onclick="commentdeleteAction()"><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
        <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
      </div>
    </div>
  </div>
</div>
 
 <!--- end delete model -->   
@stop


@section('js')

<script type="text/javascript">


function SendMessage(id="",pid){

    var actionurl = "{{url('message/userLogin/check')}}";
    //var formData = "i_to_id="+id+"&from=buyer";
    var formData = "i_to_id="+id+"&from=buyer&pid="+pid;
    $.ajax({
        processData: false,
        contentType: false,
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            document.getElementById('load').style.visibility='hidden';
            var obj = jQuery.parseJSON(res);
            if(obj['status']==1){
                $("#i_to_id_message").val(id);
                $("#contactsellerdetail").html(obj['responsestr']);
                $("#sendMessage").modal("show");
            }else{
              window.location = "{{url('login')}}";
            }
        },
        error: function ( jqXHR, exception ) {
            document.getElementById('load').style.visibility='hidden';
            $("#popupmessage").html("Something went wrong.please try again after sometime.");
            $("#sendMessageSuccess").modal("show");
            $("#sendMessage").modal("hide");
        }
    });
  }


          function submitMessage(type=""){
            
              var actionurl = "{{url('message/sendMessageSeller')}}";
              var formValidFalg = $("#sendMessageForm").parsley().validate('');
             
              if(formValidFalg){
                
                  document.getElementById('load').style.visibility="visible";  
                  var formData = new FormData($('#sendMessageForm')[0]);
                  
                  $.ajax({
                      processData: false,
                      contentType: false,
                      type    : "POST",
                      url     : actionurl,
                      data    : formData,
                      success : function( res ){
                          document.getElementById('load').style.visibility='hidden';
                          var obj = jQuery.parseJSON(res);
                          if(obj['status']==1){
                            $("#popupmessage").html(obj['msg']);
                            $("#sendMessageSuccess").modal("show");
                            $("#sendMessage").modal("hide");

                            
                          }else{
                            $("#popupmessage").html(obj['msg']);
                            $("#sendMessageSuccess").modal("show");
                            $("#sendMessage").modal("hide");
                          }
                          
                      },
                      error: function ( jqXHR, exception ) {
                          document.getElementById('load').style.visibility='hidden';
                          $("#popupmessage").html("Something went wrong.please try again after sometime.");
                          $("#sendMessageSuccess").modal("show");
                          $("#sendMessage").modal("hide");
                      }
                  });

              }    
          }


function fn_delete(){
      
    var id = [];
       
    $("input[name='shortlisted_ids[]']:checked").each(function(i){
        id.push($(this).val());
    });
      
    $("#commentdeleteId").val(id);
    $("#commentdeleteModal").modal("show");
}

function commentdeleteAction(){
   var cid = $("#commentdeleteId").val();
   var actionurl = "{{url('job-postings/delete-applied-jobs')}}";
   var formdata = "Ids="+cid; 
   var array_cid = cid.split(",");
   document.getElementById('load').style.visibility="visible"; 
   
    $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){

                document.getElementById('load').style.visibility='hidden';     
                var obj = jQuery.parseJSON(res);
                  
                if(obj['status']==1){
                    $("#commentdeleteModal").modal("hide");

                    for (var i = 0; i < array_cid.length; i++) {
                       $("#editshort-list_"+array_cid[i]).hide();
                    }
                    $('#commonmsg').html(obj['msg']);     

                }else{
                    $("#commentdeleteModal").modal("hide");
                    $('#commonmsg').html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
            
            }
            
        });

}

function fn_select_all(){

    var is_checked = $("input[name='shortlisted_ids[]']:checked").length;
   
    if (!is_checked) {
        $("input[name='shortlisted_ids[]").prop('checked', true); 
    }else {
        $("input[name='shortlisted_ids[]").prop('checked', false);
    } 
   
}
</script>

@stop

