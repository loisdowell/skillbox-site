@extends('layouts.frontend')
@section('content')
<style type="text/css">
    .highimgshortlist{
        max-width: 26px;
        margin-top: -4px;
    }
</style>

   <!-- 26B-control-panel-buyer-account-shortlisted-skills-1 -->
    <div class="shortlisted_custom">
        <div class="title-controlpanel">
            <div class="container">
                <div class="title-control-postion">
                    <div class="left-supportbtn">
                        <a href="{{url('buyer/dashboard')}}"><button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button></a>
                    </div>
                    <div class="title-support">
                        <h1> Shortlisted Skills </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="main-shortlisted">
                
                <div id="commonmsg" style="margin-top: 20px"></div>
                
                @if(isset($skills_data) && count($skills_data))
                    <div class="edit-btn">
                        <button type="button" class="btn edit-short" onclick="fn_delete()"> Delete Selected </button>
                        <button type="button" class="btn btn-selectall" onclick="fn_select_all()"> Select All </button>
                    </div>

                    @foreach($skills_data as $k=>$v)
                        <div class="editshort-list_{{$v->_id}}">
                            <div class="border-space">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="header-dirservice-find">
                                            <div class="Shortlisted-img-person">
                                                @php
                                                    $v_image="";
                                                    if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
                                                        $v_image=$v->hasUser()->v_image;
                                                    }
                                                    $imgdata = \Storage::cloud()->url($v_image);
                                                @endphp
                                                <img src="{{$imgdata}}" class="img-responsive img-category" alt="" />
                                            </div>
                                            <div class="squaredChk">
                                                <input type="checkbox" id="squaredChk_{{$v->_id}}" name="shortlisted_ids[]" value="{{$v->_id}}" />
                                                <label for="squaredChk_{{$v->_id}}"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 col-xs-12">
                                        <div class="category-heading ">
                                            <div class="row">
                                                <div class="col-sm-8 col-xs-12 ">
                                                    <h4>{{isset($v->v_profile_title) ? $v->v_profile_title:''}}</h4>
                                                    <img src="{{url('public/Assets/frontend/images/lira.png')}}" class="img-category-list highimgshortlist" alt="" />
                                                    <span class="info"> £{{isset($v->v_hourly_rate) ? $v->v_hourly_rate:''}} Per hour</span>
                                                    <img src="{{url('public/Assets/frontend/images/strength.png')}}" class="img-category-list highimgshortlist" alt="" />
                                                    <span class="info"> Skill Level:<strong style="text-transform: uppercase;"> {{isset($v->v_experience_level) ? $v->v_experience_level:''}}
                                                </strong> </span>
                                                </div>
                                                <div class="col-sm-4 col-xs-12 ">
                                                    <div class="heading-right">
                                                        <span class="sponsored"> {{( isset($v->e_sponsor) && $v->e_sponsor == 'yes') ? 'Sponsored Seller' : ''}} </span>
                                                         
                                                        @php
                                                            $title =App\Helpers\General::TitleSlug($v->v_profile_title);
                                                        @endphp
                                                         <a href="{{url('online')}}/{{$v->_id}}/{{$title}}?id={{$v->_id}}">
                                                            <button type="button" class="btn btn-sponsored">HIRE NOW</button>
                                                         </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Shortlisted-text">
                                            <p>
                                            <?php 

                                            $l_short_description = substr(isset($v['l_short_description']) ? $v['l_short_description'] : $v['l_job_description'] , 0, 210);
                                            ?>
                                            {{$l_short_description or ''}}    
                                            </p>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <a href="{{url('online')}}/{{$v->_id}}"  target="_blank" ><button type="button"class="btn btn-detail"> Details </button></a> --}}
                            <div class="clearfix">
                            </div>
                        </div>

                    @endforeach
                @else
                
                    <div class="editshort-list">
                        <div class="border-space">
                            <div class="row" style="text-align: center;">
                                No shortlisted skills found.
                            </div>
                        </div>    
                    </div>    
                @endif    
                
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-buyer-account-shortlisted-skills-1 -->

<!--- start delete model -->
<div class="modal fade" id="commentdeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title custom-font">Confirm Delete Record ?</h3>
      </div>
      <div class="modal-body" style="text-align:center">
          <i class="margin-top-10 fa fa-question fa-5x"></i>
          <h4>Delete !</h4>
          <p>Are you sure you want to delete this record?</p>
      </div>
      <input type="hidden" name="commentdeleteId[]" id='commentdeleteId'>
      <div class="modal-footer" style="padding-top: 20px;">
        <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" onclick="commentdeleteAction()"><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
        <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
      </div>
    </div>
  </div>
</div>
 
 <!--- end delete model -->   
@stop


@section('js')

<script type="text/javascript">
  
function fn_delete(){
      
    var id = new Array();

    $("input[name='shortlisted_ids[]']:checked").each(function(i){
        id.push($(this).val());
    });
      
    $("#commentdeleteId").val(id);
    $("#commentdeleteModal").modal("show");
}


function commentdeleteAction(){
   var cid = $("#commentdeleteId").val();
   var actionurl = "{{url('buyer/delete-shortlisted-skill')}}";
   var formdata = "shortlistIds="+cid; 
   var array_cid = cid.split(",");
   document.getElementById('load').style.visibility="visible"; 
    $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                document.getElementById('load').style.visibility='hidden'; 
                var obj = jQuery.parseJSON(res);
                  
                if(obj['status']==1){
                    $("#commentdeleteModal").modal("hide");

                    for (var i = 0; i < array_cid.length; i++) {
                       $("#editshort-list_"+array_cid[i]).hide();
                    }
                    $('#commonmsg').html(obj['msg']);
                    window.location = "{{url('buyer/shortlisted-skills')}}";
                        
                }else{
                    $("#commentdeleteModal").modal("hide");
                    $('#commonmsg').html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                // $("#errormsg").show();
             }
        });

}

function fn_select_all(){

    var is_checked = $("input[name='shortlisted_ids[]']:checked").length;
  
    if (!is_checked) {
        $("input[name='shortlisted_ids[]").prop('checked', true); 
    }else {
        $("input[name='shortlisted_ids[]").prop('checked', false);
    } 
   
}
</script>

@stop

