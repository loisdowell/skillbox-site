@extends('layouts.frontend')
@section('content')
<style type="text/css">
   .modal-new .modal-backdrop {z-index: 0;}
</style>

<div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="title-support">
                    <h1> Payment with PayPal </h1>
                </div>
            </div>
        </div>
    </div>

<div class="container">

    <div class="row" style="margin: 5px 0px;">
        <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
           <label class="field-edit-name" style="margin-top: 5px;">
            <h2><span>Processing Payment...</span></h2>
           </label>
        </div>
    </div>

    <form action="{{$paypalURL}}" method="post" name="paypal_form" id="paypal_form">
          
          <input type="hidden" name="cmd" value="_cart">
          <input type="hidden" name="upload" value="1">
          <input type="hidden" name="business" value="{{$paypalID}}">
          <input type="hidden" name="currency_code" value="{{$currency_code}}">
          <input type='hidden' name='cancel_return' value='{{$cancel_return}}'>
          <input type='hidden' name='return' value='{{$return_url}}'>
          <input type="hidden" name="rm" value="2" />
          <input type='hidden' name='notify_url' value='{{$return_url}}'>
          <input type="hidden" name="lc" value="AE" />

          @if(isset($itemdata) && count($itemdata))
            @foreach($itemdata as $k=>$v)
                <input type="hidden" name="item_name_{{$k+1}}" value="{{$v['name']}}">
                <input type="hidden" name="item_number_{{$k+1}}" value="{{$v['id']}}">
                <input type="hidden" name="amount_{{$k+1}}" value="{{$v['price']}}">
            @endforeach
          @endif
          
   </form>

</div>

@stop

@section('js')

<script type="text/javascript">
  document.getElementById('load').style.visibility="visible";
  $(document).ready(function() {
      $("#paypal_form").submit();
  });

</script>
@stop