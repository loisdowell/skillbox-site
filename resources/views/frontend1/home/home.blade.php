@extends('layouts.frontend')
@section('content')

@php
    $videourl = App\Helpers\General::getSiteSetting("HOME_PAGE_VIDEO_URL");    
    $videourldata=array();
    if($videourl!=""){
        $videourldata=explode("=", $videourl);
    }
    if(isset($videourldata[1])){
        $videourl="https://www.youtube.com/embed/".$videourldata[1].'?rel=0&autoplay=1';   
    }else{
        $videourl = $videourl.'?rel=0&autoplay=1';
    }
@endphp

<div class="modal video_modal fade" id="video1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="CloseVideo()">&times;</button>
        </div>
        <div class="modal-body">
         <iframe id="videoplay" src="" target="_parent" width="100%" height="500px">
        </iframe>
        </div>
      </div>
    </div>
</div>
   
<div class="container-fluid">
    <div class="row">
        <div class="header-img _main_home">
            @php
                $v_banner_image="";
                if(isset($homepage) && count($homepage) && isset($homepage[0])){
                    $v_banner_image = $_S3_PATH.'/'.$homepage[0];
                }  
                else{
                    $v_banner_image = url('public/Assets/frontend/images/header.png');
                }  
            @endphp
            <img src="{{$v_banner_image}}" class="img-responsive" alt="banner image" />
            <div class="center-containt">
                <div class="container">
                    <div class="slide-containt">
                        <p>Discover<span> . </span>Select<span> . </span>Connect<span> . </span></p>
                    </div>
                    <div class="free-lancer">
                        <h1>Find your Perfect FREELANCER</h1>
                    </div>
                    <!-- <div class="free-lancer" onclick="videoPlay('{{$videourl}}')" style="cursor: pointer;"> -->
                        <!-- <p>Feel the Freedom <span class="spcamera">
                        </span></p> -->
                        <div class="free-lancer">
                        <a href="{{url('buyer/post-job')}}">
                        <button type="button" class="btn btn-guide-job-home">Post a Job for Free!</button>
                        </a>
                    </div>

                    <form class="horizontal-form" role="form" method="get" name="searchmaster" id="searchmaster" action="{{url('search')}}" >
                    
                    <div class="input-box-slide-second">
                        <div class="new-location1">
                            <div class="new-location">
                                <div class="select-box-find">
                                    <select id="generalsearch" name="e_type" onchange="searchhomepage(this.value)" >
                                        <option value="inperson">In Person</option>
                                        <option value="online">Online</option>
                                        <option value="courses">Courses</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div style="display: inline-flex;" id="searchcontent"> 
                               
                                <div class="search-to-find" id="inpersonmasterskill">
                                    <input type="text" list="browsers" class="form-control" name="keyword" placeholder="Find your perfect skill or job" onkeyup="inpersonssearch(this.value)">
                                </div>
                                <datalist id="browsers">
                                </datalist>
                                <div class="Location-line" id="inpersonmasterlocation">
                                    <input type="text" class="form-control" name="text" id="autocomplete" placeholder="Location"  autocomplete="off" oninvalid="this.setCustomValidity('Enter a location to see results close by')" oninput="setCustomValidity('')">
                                </div>
                                <input type="hidden" name="v_latitude" id="v_latitude">
                                <input type="hidden" name="v_longitude" id="v_longitude">

                            </div>
                            
                            <div class="btn-perfect1">
                                <button type="submit" class="btn" >Get Started</button>
                            </div>

                        </div>
                    </div>
                    </form>
                    <div class="guide">
                        <a href="{{url('pages/how-to-guide')}}">
                        <button type="button" class="btn btn-guide">how to guide?</button>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
    
    @php
        $cids=array();
    @endphp
    <div id="skillcategory" style="margin-bottom: 18px">
    </div>

    <!--Unlimited-Access-to-Skills-->
    <div class="container-fluid">
        <div class="all-skills" id="skillcategory1">
            <h2>Unlimited Access To Skills</h2>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="direct-services">
                            <img src="{{url('public/Assets/frontend/images/direct-service.png')}}" alt="Direct Service Iamge" style="max-width: 50px" />
                            <p><strong>IN PERSON  SERVICES</strong></p>
                            <div class="clearfix"></div>
                        </div>
                        <div class="all-services person_services_boxs" id="directservicemore">
                        @if(isset($inperson))
                        @foreach($inperson as $val)

                            @php
                                $cids[]=$val->id;
                            @endphp
                            <div class="col-sm-6">
                                <div class="home-skill">
                                    <a href="{{url('search')}}?e_type=inperson&keyword=&i_category_id={{$val->id}}&v_type_skill=on">
                                    <div class="skills-img">
                                        @php
                                            $imgdata="";
                                            $imgdata=$_S3_PATH.'/'.$val->v_image;
                                        @endphp
                                        <img src="{{$imgdata}}" class="img-responsive" alt="Category Image" />
                                        <p>{{$val->v_name}}</p>
                                    </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="direct-services">
                            <img src="{{url('public/Assets/frontend/images/glasses.png')}}" alt="Online Service Image" style="max-width: 50px"/>
                            <p><strong>online services</strong></p>
                        </div>
                        
                        <div class="all-services online_services_boxs" id="onlineservicemore">
                        @if(isset($online))
                        @foreach($online as $val)
                            @php
                                $cids[]=$val->id;
                            @endphp
                            <div class="col-sm-6">
                                <div class="home-skill">
                                    <a href="{{url('search')}}?e_type=online&keyword=&i_category_id={{$val->id}}&v_type_skill=on">
                                    <div class="skills-img">
                                        @php
                                            $imgdata="";
                                            $imgdata=$_S3_PATH.'/'.$val->v_image;
                                        @endphp 
                                        <img src="{{$imgdata}}" class="img-responsive" alt="Categories Iamge" />
                                        <p>{{$val->v_name}}</p>
                                    </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        @endif 
                        </div>
                    </div>
                </div>

                @php
                    $cids = implode(',',$cids);
                @endphp
                <input type="hidden" name="cids" id="cids" value="{{$cids}}">
                @if($moreCategoryButton>=1)
                <div class="guide" id="btnstatus">
                    <button type="button" onclick="moreCategory('{{$cids}}')" class="btn btn-guide btn-Categories">More Categories</button>
                </div>
                @endif

            </div>
        </div>
    </div>
    <!--end-Unlimited-Access-to-Skills-->

    <!--Access Your Service-->
    <div class="access_service_custom">
        <div class="container">
            <div class="all-skills">
                <h2 class="text_2a2a2a">Access Your Service</h2>
            </div>
            <div class="Access-service">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{url('account/buyer')}}">
                        <div class="hire-work">
                            <img src="{{$signupdata['buy_image']}}" alt="I want to buy image"  />
                            <div class="services-tital">
                                <p>{{$signupdata['buy_text']}}</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        </a>
                        <div class="guide">
                            <a href="{{url('account/buyer')}}"><button class="btn btn-guide btn-hire">I want to Buy</button></a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <a href="{{url('account/seller')}}">
                        <div class="hire-work">
                            <img src="{{$signupdata['sell_image']}}" alt="i want to sell image" />
                            <div class="services-tital">
                                <p>{{$signupdata['sell_text']}}</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        </a>
                        <div class="guide">
                            <a href="{{url('account/seller')}}"><button class="btn btn-guide btn-hire">I want to Sell</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-Access Your Service-->

    <!--How to guide-->
    <div class="container">
        <div class="all-skills">
            <h2 class="text_2a2a2a">How to guide</h2>
        </div>
        <div class="container">
            <div class="guide-all">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="progress-circle">
                            <div class="circle" id="circle-a">
                                <strong class="spsearch">
                                {{-- <img src="{{url('public/Assets/frontend/images/skill1.png')}}" alt="skill icon" /> --}}
                                </strong>
                            </div>
                            <div class="progress-search">
                                <span>{{$guidedatalist['SEARCH']['title'] or 'SEARCH'}}</span>
                            </div>
                            <div class="progress-text">
                                <p>{{$guidedatalist['SEARCH']['desc'] or 'Our database is full of talented young minded freelancers search to enjoy!'}} </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle" id="circle-b">
                            <strong class="spnetwork">
                            {{-- <img src="{{url('public/Assets/frontend/images/time.png')}}" alt="time icon" /> --}}
                            </strong>
                        </div>
                        <div class="progress-search">
                            <span>{{$guidedatalist['CONNECT']['title'] or 'CONNECT'}}</span>
                        </div>
                        <div class="progress-text">
                        <p>{{$guidedatalist['CONNECT']['desc'] or 'Join the fun experience both Online and in Person to connect with others.'}} </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle" id="circle-c">
                            <strong class="spthumsup">
                            {{-- <img src="{{url('public/Assets/frontend/images/heand.png')}}" alt="Hend icon" /> --}}
                            </strong>
                        </div>
                        <div class="progress-search">
                            <span>{{$guidedatalist['SKILL UP']['title'] or 'SKILL UP'}}</span>
                        </div>
                        <div class="progress-text">
                        <p>{{$guidedatalist['SKILL UP']['desc'] or 'Gain instant access to the skilled community, submit Projects or learn New skills.'}} </p>
                            {{-- <p>Gain instant access to the skilled community, submit Projects or learn New skills </p> --}}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="circle" id="circle-d">
                            <strong class="spprice">
                            {{-- <img src="{{url('public/Assets/frontend/images/price.png')}}" alt="Price Icon" /> --}}
                            </strong>
                        </div>
                        <div class="progress-search">
                            <span>{{$guidedatalist['PAY']['title'] or 'PAY'}}</span>
                        </div>
                        <div class="progress-text">
                            <p>{{$guidedatalist['PAY']['desc'] or 'Simply pay when the job is done!'}} </p>
                        </div>
                    </div>
                </div>
                <div class="progress-link">
                    <a href="{{url('pages/learn-the-way')}}">Learn the way</a>
                </div>
            </div>
        </div>
    </div>
    <!--end-How to guide-->


    @if(count($featuredProfile))

    <!--Featured-Freelancers-->
    <div class="Featured-tab">
        <div class="container">
            <div class="all-skills">
                <h2 class="text_2a2a2a">Featured Freelancers</h2>
            </div>
            
            <div class="responsive autoplay1">
               
                @foreach($featuredProfile as $k=>$v)
                    @php
                        $v_fname="";
                        if(count($v->hasUser()) && isset($v->hasUser()->v_fname)){
                            $v_fname=$v->hasUser()->v_fname;
                        }

                        $v_lname="";
                        if(count($v->hasUser()) && isset($v->hasUser()->v_lname)){
                            $v_lname=$v->hasUser()->v_lname;
                        }

                    @endphp
                    @if($v_fname!="")
                    <div class="slider-play1">
                        <div class="slider-play">

                            <a href="{{url('online')}}/{{$v['_id']}}/{{$v['v_profile_title']}}?id={{$v['_id']}}">
                            <div class="slider-img">
                                @php
                                $v_image_profile="";
                                if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
                                    $v_image_profile = $v->hasUser()->v_image;
                                }
                                $imgdata = '';
                                
                                $imgdata=$_S3_PATH.'/'.$v_image_profile;
                                @endphp
                                <img src="{{$imgdata}}" alt="Profile Image" />
                            </div>
                            </a>
                            <div class="devloper-name">
                                <a href="{{url('online')}}/{{$v['_id']}}/{{$v['v_profile_title']}}?id={{$v['_id']}}" style="color: #000000">
                                <button class="btn btn-name">{{$v_fname}} {{$v_lname}}</button>
                                </a>
                            </div>
                        </div>

                        <div class="name-satus">
                            <a href="{{url('online')}}/{{$v['_id']}}/{{$v['v_profile_title']}}?id={{$v['_id']}}" style="color: #000000">
                            <p>{{$v->v_profile_title or ''}}</p>
                            </a>
                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$v->i_total_avg_review or 0}}" readonly>
                        </div>
                        <div class="guide">
                            <a href="{{url('online')}}/{{$v['_id']}}/{{$v['v_profile_title']}}?id={{$v['_id']}}">
                            <button class="btn btn-guide btn-Profile">View Profile</button>
                            </a>
                        </div>
                        
                        <div class="devloper-alltext">
                            <p>
                                @if(isset($v->l_short_description))
                                    @if(strlen($v->l_short_description)>50)
                                        {{substr($v->l_short_description,0,50)}}...
                                    @else
                                        {{$v->l_short_description}}
                                    @endif
                               @endif
                            </p>
                        </div>
                    </div>
                    @endif
                    
                @endforeach  

            </div>
        </div>
    </div>
    <!--end-Featured-Freelancers-->
    @endif

    <!--Our Company Ethos-->
    @if(isset($companyEthos) && count($companyEthos))
        <div class="container">
            <div class="all-skills">
                <h2 class="text_1a1a1a">Our Company Ethos</h2>
            </div>
            <div class="Company-Ethos">
                <div class="row">
                    @foreach($companyEthos as $key => $val)
                        <div class="col-sm-3">
                            <div class="our-company">
                                <div class="our-logo">
                                     @if(isset($val->v_image) && $val->v_image != '' )
                                         @php
                                            $imgdata="";
                                            $imgdata=$_S3_PATH.'/'.$val->v_image;
                                         @endphp 
                                        <img src="{{$imgdata}}" style="max-width: 150px" alt="Compnay Ethos image" />
                                      @else
                                         @php
                                            $imgdata=$_S3_PATH.'/common/no-image.png';
                                         @endphp 
                                        <img src="{{$imgdata}}" alt="No Image" />  
                                      @endif
                                </div>
                                <span>{{$val->v_title or ''}}</span>
                                <div class="purose-text">
                                    <p>{{$val->v_description or ''}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    <!--end-Our Company Ethos-->

@stop
@section('js')

{{-- <script src="{{url('public/Assets/frontend/js/homepage.js')}}"></script> --}}
<script type="text/javascript">
    
    function  searchhomepage(data) {
    var searchstr="";
    if(data=="inperson"){
        searchstr+='<div class="search-to-find" id="inpersonmasterskill">';
        searchstr+='<input type="text" list="browsers" class="form-control" name="text" placeholder="Find your perfect skill or job" onkeyup="inpersonssearch(this.value)">';
        searchstr+='<datalist id="onlinesearch"></datalist>';
        searchstr+='</div>';
        searchstr+='<div class="Location-line" id="inpersonmasterlocation">';
        searchstr+='<input type="text" class="form-control" name="text" id="autocomplete" placeholder="Location"  autocomplete="off"';
        searchstr += 'oninvalid="this.setCustomValidity';
        searchstr += "('Enter a location to see results close by')";
        searchstr += '" oninput="setCustomValidity()" >';
        searchstr+='<input type="hidden" name="v_latitude" id="v_latitude">';
        searchstr+='<input type="hidden" name="v_longitude" id="v_longitude">';
        searchstr+='</div>';
    }else if(data=="online"){
        searchstr+='<div class="search-to-find" id="onlinemaster">';
        searchstr+='<input type="text" list="onlinesearch" onkeyup="onlinesearch(this.value)" class="form-control form-online" name="keyword" placeholder="Find your perfect skill or job">';
        searchstr+='<datalist id="onlinesearch"></datalist>';
        searchstr+='</div>';
    
    }else{
        searchstr+='<div class="search-to-find" id="coursesmaster">';
        searchstr+='<input type="text" list="coursesearch" onkeyup="coursesearch(this.value)" class="form-control form-online" name="keyword" placeholder="Find your perfect course">';
        searchstr+='<datalist id="coursesearch"></datalist>';
        searchstr+='</div>';
    }
    $("#searchcontent").html(searchstr);
    initAutocomplete();
}




function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

function moreCategory(data){

    document.getElementById('load').style.visibility="visible"; 
    var data = $("#cids").val();
    var actionurl = base_path+"/more-category";
    var formData = "ids="+data;
    
    $.ajax({
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            
            var obj = jQuery.parseJSON(res);
            $("#directservicemore").append(obj['inpersonstr']);
            $("#onlineservicemore").append(obj['onlinestr']);
            $("#cids").val(obj['ids']);
            if(obj['onlinestr']==0){
                $("#btnstatus").html("");
            }
            document.getElementById('load').style.visibility='hidden';
        },
        error: function ( jqXHR, exception ) {
        }
    });
}


    function inpersonssearch(data){
        
        var keystr = data.length;

        if(keystr>3){

            var actionurl = base_path+"/inperson-suggest";
            var formData = "keyword="+data;
            
            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#browsers").html(obj['searchstr']);  
                    }
                },
            });
        }
    }

    function onlinesearch(data){
        
        var keystr = data.length;

        if(keystr>3){

            var actionurl = base_path+"/online-suggest";
            var formData = "keyword="+data;
            
            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#onlinesearch").html(obj['searchstr']);  
                    }
                },
            });
        }
    }

    function coursesearch(data){
        
        var keystr = data.length;

        if(keystr>3){

            var actionurl = base_path+"/course-suggest";
            var formData = "keyword="+data;
            
            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#coursesearch").html(obj['searchstr']);  
                    }
                },
            });
        }
    }

    function videoPlay(data=""){
        $("#video1").modal("show");
        $("#videoplay").attr("src",data);
    }
    function CloseVideo(){
        $("#video1").modal("hide");
        $("#videoplay").attr("src","");
    }
</script>


@stop

