      <link rel="stylesheet" href="{{url('public/Assets/frontend/star-rating/css/star-rating.css')}}" type="text/css" media="all">
      <script src="{{url('public/Assets/frontend/star-rating/js/star-rating.js')}}"></script> 
      
      @if(isset($reviewdata) && count($reviewdata))
        @foreach($reviewdata as $k=>$v)
            @php
                $v_image="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
                    $v_image=$v->hasUser()->v_image;
                }
                $v_fname="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_fname)){
                    $v_fname=$v->hasUser()->v_fname;
                }

                $v_lname="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_lname)){
                    $v_lname=$v->hasUser()->v_lname;
                }
            @endphp
            <div class="all-viwe-man">
                <div class="Review-profile-all Review-profile-fine">
                   <div class="Review-profile">
                      <div class="Review-img">

                          @php
                          $imgdata = '';
                          if(Storage::disk('s3')->exists($v_image)){
                              $imgdata = \Storage::cloud()->url($v_image);
                          }else{
                              $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                          }
                          @endphp  
                         <img src="{{$imgdata}}" alt="" />
                      </div>

                      @php
                          $totalavg = $v->i_communication_star+$v->i_described_star+$v->i_recommend_star;
                          $totalavg = $totalavg/3;

                      @endphp

                      <div class="Review-name">
                         <h3>{{$v_fname}} {{$v_lname}}</h3>
                         
                         <div class="Review-name-final Review-name-fine review_align review_align_left mt_3">
                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalavg}}" readonly>&nbsp;<span class="review_number">{{number_format($totalavg,1)}}</span>
                         </div>
                         <p>{{$v->l_comment}}</p>
                      </div>
                   </div>

                   @php
                      $v_image="";
                      if(count($v->hasSeller()) && isset($v->hasSeller()->v_image)){
                          $v_image=$v->hasSeller()->v_image;
                      }
                      $v_fname="";
                      if(count($v->hasSeller()) && isset($v->hasSeller()->v_fname)){
                          $v_fname=$v->hasSeller()->v_fname;
                      }

                      $v_lname="";
                      if(count($v->hasSeller()) && isset($v->hasSeller()->v_lname)){
                          $v_lname=$v->hasSeller()->v_lname;
                      }
                  @endphp
                   
                   @if(isset($v->l_answers) && count($v->l_answers))

                     <div class="Review-profile1">
                        <div class="Review-img1">
                           @php
                              $imgdata = '';
                              if(Storage::disk('s3')->exists($v_image)){
                                  $imgdata = \Storage::cloud()->url($v_image);
                              }else{
                                  $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                              }
                              @endphp  
                             <img src="{{$imgdata}}" alt="" />
                        </div>
                        <div class="Review-name">
                           <h3>{{$v_fname}} {{$v_lname}}</h3>
                           <div class="Review-name-final Review-name-fine">
                           </div>
                           <p>{{isset($v->l_answers['l_comment']) ? $v->l_answers['l_comment'] : ''}}</p>
                        </div>
                     </div>
                  @endif   

                </div>
            </div>
        @endforeach
      @endif      
      
      <script type="text/javascript">
        var src1 = "{{url('public/Assets/frontend/images/star.png')}}";
        var src2 = "{{url('public/Assets/frontend/images/star1.png')}}";
        $("#star-input-1,#star-input-2,#star-input-3,#star-input-4,#star-input-5").rating({
            min: 0,
            max: 5,
            step: 0.5,
            showClear: false,
            showCaption: false,
            theme: 'krajee-fa',
            filledStar: '<i class="fa"><img src="'+src1+'"></i>',
            emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
        });

        $("#f_rate_instruction,#f_rate_navigate,#f_rate_reccommend").rating({
            min: 0,
            max: 5,
            step: 0.5,
            showClear: false,
            showCaption: false,
            theme: 'krajee-fa',
            filledStar: '<i class="fa"><img src="'+src1+'"></i>',
            emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
        });
     </script>
