<link rel="stylesheet" href="{{url('public/Assets/frontend/star-rating/css/star-rating.css')}}" type="text/css" media="all">
<script src="{{url('public/Assets/frontend/star-rating/js/star-rating.js')}}"></script> 

@if(isset($type) && $type=="list")
    
    @if(isset($courses) && count($courses))
    @foreach($courses as $k=>$v)

        @php
            $title =App\Helpers\General::TitleSlug($v->v_title);
        @endphp

        <div class="col-xs-12">
            <div class="all-leval @if(isset($v->e_sponsor_status) && $v->e_sponsor_status == 'start') listviewbordersponsered @endif ">
                <div class="row height_container">
                    <div class="col-sm-4">
                        <div class="leval-img-sortng">
                     <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}">
                            @php
                             $imgins="";
                            if(isset($v->v_course_thumbnail) && $v->v_course_thumbnail!=""){
                                $imgins = $v->v_course_thumbnail;
                            }
                            $imgdata=$_S3_PATH.'/'.$imgins
                            // if(Storage::disk('s3')->exists($imgins)){
                            //     $imgdata = \Storage::cloud()->url($imgins);
                            // }else{
                            //     $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                            // }
                         @endphp   
                            <img class="height_div" src="{{$imgdata}}"  alt="" />
                        </a>

                            @if(isset($v->e_sponsor_status) && $v->e_sponsor_status == 'start')
                                <div class="sponsored-course" style="top: 87%;">
                                    <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}" class="btn btn-course">Sponsored AD</a>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-8 height_div">
                        <div class="center-courses">
                        
                            <div class="row">
                                <div class="col-xs-10 no-padding">
                                    <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}">
                                    <div class="center-courses-text">
                                        <span>{{isset($v->v_title) ? $v->v_title:''}}</span>
                                        <p>{{isset($v->v_author_name) ? $v->v_author_name:''}}</p>
                                    </div>
                                    </a>
                                </div>
                                <div class="col-xs-2">
                                    <div class="courses-icon">
                                        @if(in_array($v->_id, $s_course))
                                            <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['_id']}}" onclick="shortlist_listview('{{$v['_id']}}')" >
                                            <img src="{{url('public/Assets/frontend/images/hartA.png')}}" id="bg_listview_{{$v['_id']}}" /></a>
                                            <input type="hidden" name="img_click_listview_{{$v['_id']}}" id="img_click_listview_{{$v['_id']}}" value="1">

                                        @else
                                           <a href="javascript:;" title="shortlist" class="menulink menut{{$v['_id']}}" onclick="shortlist_listview('{{$v['_id']}}')" >
                                        <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_listview_{{$v['_id']}}" /></a>
                                        <input type="hidden" name="img_click_listview_{{$v['_id']}}" id="img_click_listview_{{$v['_id']}}" value="0">

                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 no-padding">
                                    <div class="center-courses-text">
                                        <p>
                                            @if(isset($v->v_sub_title))
                                                @if(strlen($v->v_sub_title)>150)
                                                    {{substr($v->v_sub_title,0,150)}}...
                                                @else
                                                    {{$v->v_sub_title}}
                                                @endif
                                           @endif

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 no-padding">
                                    <ul class="list-leval">
                                        <li><span>{{isset($v->section) ? count($v->section):'0'}}</span>Lectures</li>
                                        |
                                        <li><span>{{isset($v->i_duration) ? $v->i_duration:''}}</span>Hours Duration</li>
                                        |
                                        <li><span>{{count( $v->hasLevel() ) ? $v->hasLevel()->v_title : ''}}</span>Level</li>
                                        |
                                        <li>{{count( $v->hasCategory() ) ? $v->hasCategory()->v_title : ''}} </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="courses-icon course-pricevalue">
                                        @if(isset($v->f_price) && $v->f_price!="0")
                                        <P> COURSE PRICE 
                                        <span>£{{isset($v->f_price) ? $v->f_price:''}}</span>
                                        </P>
                                        @else
                                        <P>  
                                        FREE COURSE
                                        @endif
                                        </P>
                                    </div>
                                    <div class="rating-leval">

                                        <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$v->i_total_avg_review}}" required>
                                        <p> {{number_format($v->i_total_avg_review,1)}}
                                        <span class="point">({{$v->i_total_review}})
                                          </span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
     @endforeach

@endif


@else
@php
  $knt=0;      
@endphp
@if(isset($courses) && count($courses))
    @foreach($courses as $k=>$v)
       
       @php
            $title =App\Helpers\General::TitleSlug($v->v_title);
        @endphp

        @php $cidsarray[]=$v->id; @endphp                
        <div class="col-sm-4 @if($knt==0) fresult @endif">
            <div class="all-category @if(isset($v->e_sponsor_status) && $v->e_sponsor_status == 'start') all-cate-results @endif">
                <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}">
                <div class="category-img category-img-results">
                     @php
                        $imgins="";
                        if(isset($v->v_course_thumbnail) && $v->v_course_thumbnail!=""){
                            $imgins = $v->v_course_thumbnail;
                        }
                        $imgdata=$_S3_PATH.'/'.$imgins
                        // if(Storage::disk('s3')->exists($imgins)){
                        //     $imgdata = \Storage::cloud()->url($imgins);
                        // }else{
                        //     $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                        // }
                     @endphp   
                    <img src="{{$imgdata}}" alt="" />
                </div>
                </a>
                <div class="category-text">
                    <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}">
                    <h2 style="min-height: 36px;max-height: 36px;overflow: hidden;">{{isset($v->v_title) ? $v->v_title:''}}</h2>
                    </a>
                    <p style="min-height: 45px;max-height: 45px;overflow: hidden;">
                       @if(isset($v->v_sub_title))
                            @if(strlen($v->v_sub_title)>55)
                                {{substr($v->v_sub_title,0,55)}}...
                            @else
                                {{$v->v_sub_title}}
                            @endif
                       @endif
                    <div class="star-category">
                        @if($v->i_total_avg_review>0)
                        <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                        @else
                        <img src="{{url('public/Assets/frontend/images/star-gray.png')}}" alt="" />  
                        @endif
                        <p class="text-star">{{number_format($v->i_total_avg_review,1)}}   
                                &nbsp;<span>({{$v->i_total_review}})</span></p>
                    </div>
                </div>  
                @if(isset($v->e_sponsor_status) && $v->e_sponsor_status == 'start')
                <div class="sponsored-course">
                    <a href="{{url('course')}}/{{$v->id}}/{{$title}}?id={{$v->id}}" class="btn btn-course">Sponsored AD</a>
                </div>
                @endif

                <div class="starting-price">
                    <div class="pull-left">
                        @if(in_array($v->_id, $s_course))
                            <a href="javascript:;" title="shortlisted" class="menulink menut{{$v['_id']}}" onclick="shortlist('{{$v['_id']}}')" >
                            <img src="{{url('public/Assets/frontend/images/yellow-heart.png')}}" id="bg_{{$v['_id']}}" /></a>
                            <input type="hidden" name="img_click_{{$v['_id']}}" id="img_click_{{$v['_id']}}" value="1">

                        @else
                           <a href="javascript:;" title="shortlist" class="menulink menut{{$v['_id']}}" onclick="shortlist('{{$v['_id']}}')" >
                            <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg_{{$v['_id']}}" /></a>
                            <input type="hidden" name="img_click_{{$v['_id']}}" id="img_click_{{$v['_id']}}" value="0">

                        @endif
                    </div>
                    <div class="pull-right">
                        @if(isset($v->f_price) && $v->f_price!="0")
                        <P> COURSE PRICE 
                        <span>£{{isset($v->f_price) ? $v->f_price:''}}</span>
                        @else
                        <P>  
                        FREE COURSE
                        @endif
                        </P>
                    </div>
                </div>

            </div>
        </div>
         @php
          $knt=$knt+1;      
        @endphp
    @endforeach
@endif

@endif

<script type="text/javascript">
    var src1 = "{{url('public/Assets/frontend/images/star.png')}}";
    var src2 = "{{url('public/Assets/frontend/images/star1.png')}}";
    $("#star-input-1,#star-input-2,#star-input-3,#star-input-4,#star-input-5").rating({
        min: 0,
        max: 5,
        step: 0.5,
        showClear: false,
        showCaption: false,
        theme: 'krajee-fa',
        filledStar: '<i class="fa"><img src="'+src1+'"></i>',
        emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
    });

    $("#f_rate_instruction,#f_rate_navigate,#f_rate_reccommend").rating({
        min: 0,
        max: 5,
        step: 0.5,
        showClear: false,
        showCaption: false,
        theme: 'krajee-fa',
        filledStar: '<i class="fa"><img src="'+src1+'"></i>',
        emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
    });
 </script>