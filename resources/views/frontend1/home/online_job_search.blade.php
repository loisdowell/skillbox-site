@extends('layouts.frontend')


@section('content')

<style type="text/css">
    .clearboth:nth-child(3n+1){
        clear: both;
    }

</style>

   <!--slide-containt-->
    <div class="container-fluid">
        <div class="row">
            <div class="header-img">
                <img src="{{url('public/Assets/frontend/images/filtaring.png')}}" class="img-responsive" alt="" />
                <div class="center-containt">
                    <div class="container">
                        <div class="filtaring-page">
                            <h1>gardening</h1>
                            <p>You’ve got the idea, now make it official with the perfect garden designs</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--end-slide-containt-->

    <!-- 04B-filtering-results-in person-list view-jobss -->
    <div class="container">
       <div class="sort-for">
            <div class="sort-forep">
                
                <a href="javascript:;" onclick="viewchange('grid')">
                    <img src="{{url('public/Assets/frontend/images/fore.png')}}" class="fore-all" alt="" />
                </a>
                <a href="javascript:;" onclick="viewchange('list')">
                    <img src="{{url('public/Assets/frontend/images/fore2.png')}}" alt="" />
                </a>

            </div>
        </div>
        <div class="filtaring-main">
            <div class="row">
                <div class="col-sm-3">
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Type</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox" type="checkbox" checked="">
                            <label for="checkbox">
                                jobss (<span>6</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox1" type="checkbox" checked="">
                            <label for="checkbox1">
                                Jobs (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning" style="visibility: hidden;">
                            <input id="checkbox2" type="checkbox" checked="">
                            <label for="checkbox2">
                                Courses (<span>1</span>)
                            </label>
                        </div>
                    </div>
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Experience</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox3" type="checkbox" checked="">
                            <label for="checkbox3">
                                Entry (<span>6</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox4" type="checkbox" checked="">
                            <label for="checkbox4">
                                Intermediate (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox5" type="checkbox" checked="">
                            <label for="checkbox5">
                                Expert (<span>1</span>)
                            </label>
                        </div>
                    </div>
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Language</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox6" type="checkbox" checked="">
                            <label for="checkbox6">
                                English (<span>6</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox7" type="checkbox" checked="">
                            <label for="checkbox7">
                                Spanish (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox8" type="checkbox" checked="">
                            <label for="checkbox8">
                                French (<span>1</span>)
                            </label>
                            <select class="more-info">
                                <option>More</option>
                            </select>
                        </div>
                    </div>
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Location</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox9" type="checkbox" checked="">
                            <label for="checkbox9">
                                Birmingham (<span>6</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox10" type="checkbox" checked="">
                            <label for="checkbox10">
                                Oxford (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox11" type="checkbox" checked="">
                            <label for="checkbox11">
                                Birmingham (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox12" type="checkbox" checked="">
                            <label for="checkbox12">
                                Wiltshire (<span>5</span>)
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox13" type="checkbox" checked="">
                            <label for="checkbox13">
                                French (<span>1</span>)
                            </label>
                            <select class="more-info">
                                <option>More</option>
                            </select>
                        </div>
                    </div>
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Price</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox14" type="checkbox" checked="">
                            <label for="checkbox14">
                                Under £50
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox15" type="checkbox" checked="">
                            <label for="checkbox15">
                                £50 - £100
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox16" type="checkbox" checked="">
                            <label for="checkbox16">
                                £100 - £200
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox17" type="checkbox" checked="">
                            <label for="checkbox17">
                                £250 - £350
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox18" type="checkbox" checked="">
                            <label for="checkbox18">
                                £400 +
                            </label>
                        </div>
                    </div>
                    <div class="all-type">
                        <div class="name-of-type">
                            <span>Radius</span>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox19" type="checkbox" checked="">
                            <label for="checkbox19">
                                5 Miles
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox20" type="checkbox" checked="">
                            <label for="checkbox20">
                                10 Miles
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox21" type="checkbox" checked="">
                            <label for="checkbox21">
                                15 Miles
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox22" type="checkbox" checked="">
                            <label for="checkbox22">
                                25 Miles
                            </label>
                        </div>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox23" type="checkbox" checked="">
                            <label for="checkbox23">
                                50 Miles
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 no-padding">
                    <div class="select-box">
                        <div class="col-sm-4">
                            <select class="form-control form-sel">
                                <option>Select Category</option>
                                <option>Select-1</option>
                                <option>Select-2</option>
                                <option>Select-3</option>
                                <option>Select-4</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control form-sel">
                                <option>Select Subcategory</option>
                                <option>Select-1</option>
                                <option>Select-2</option>
                                <option>Select-3</option>
                                <option>Select-4</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control form-sel">
                                <option>Sort by Relevance</option>
                                <option>Select-1</option>
                                <option>Select-2</option>
                                <option>Select-3</option>
                                <option>Select-4</option>
                            </select>
                        </div>
                    </div>
                    <?php 

                    // echo "<pre>";
                    // print_r($jobs);
                    // exit;
                    ?>
                 

                     <div id="gridview" >
                        <div class="box-content-find">
                            @if(isset($jobs) && count($jobs))
                                @foreach($jobs as $k=>$v)
                                    
                                     <div class="col-sm-4">
                                        <div class="all-category all-cate-results">
                                            <div class="category-img category-img-results">
                                                {{-- <img src="{{url('public/uploads/courses')}}/{{isset($v->v_instructor_image) ? $v->v_instructor_image:''}}" alt="" /> --}}
                                                <img src="{{url('public/Assets/frontend/images/leval1.png')}}" alt="" />
                                            </div>
                                            <div class="category-text">
                                                <a href="{{url('course')}}/{{$v->id}}">
                                                <h2>{{isset($v->v_job_title) ? $v->v_job_title:''}}</h2>
                                                </a>
                                                <p> 
                                                <?php 
                                                $l_job_description = substr($v->l_job_description, 0, 150);
                                                ?>
                                                {{$l_job_description or ''}}    </p>
                                                <div class="star-category">
                                                    <img src="{{url('public/Assets/frontend/images/star-final.png')}}" alt="" />
                                                    <p class="text-star">4.9 <span>( 34 )</span></p>
                                                </div>
                                            </div>

                                            <div class="sponsored-course">
                                                <a href="#" class="btn btn-course">Sponsored AD</a>
                                            </div>


                                            <div class="starting-price">
                                                <div class="pull-left">
                                                    <img src="{{url('public/Assets/frontend/images/hart.png')}}" alt="" />
                                                </div>
                                                <div class="pull-right">
                                                    <P> COURSE PRICE <span>£{{isset($v->v_budget_amt) ? $v->v_budget_amt:''}}</span></P>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                @endforeach
                            @else
                                <div class="col-sm-12">
                                    <div class="all-category all-cate-results">
                                        
                                        <div class="category-text">
                                            <h2 style="text-align:center;">Courses data can not found.</h2>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>

                            @endif
                        </div>
                    </div>

                    <div id="listview" style="display: none;">
                          @if(count($jobs))
                            @foreach($jobs as $k=>$v)
                                
                                <div class="col-xs-12">
                                    <div class="all-leval">
                                        <div class="row">
                                            
                                            <div class="col-sm-4">
                                                <div class="leval-img-sortng">
                                                    <img src="{{url('public/Assets/frontend/images/leval1.png')}}" alt="" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-8">
                                                <div class="center-courses">
                                                    <div class="row">
                                                        <div class="col-xs-10 no-padding">
                                                            <div class="center-courses-text">
                                                                <a href="{{url('online-job')}}/{{$v->id}}"><span>{{$v->v_job_title or ''}}</span></a>
                                                                <p>{{count( $v->hasUser() && isset($v->hasUser()->v_fname) ) ? $v->hasUser()->v_fname.' '.$v->hasUser()->v_lname : ''}}</p>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-2">
                                                            <div class="courses-icon">
                                                                <a href="" title="Switch" class="menulink">
                                                                <img src="{{url('public/Assets/frontend/images/hartB.png')}}" id="bg" /></a>
                                                                <input type="hidden" name="img_click" id="img_click" value="1">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 no-padding">
                                                            <div class="center-courses-text">
                                                                <p>
                                                                    <?php 
                                                                    $l_job_description = substr($v->l_job_description, 0, 150);
                                                                    ?>
                                                                    {{$l_job_description or ''}}    
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6 no-padding text-center-xs">
                                                            <label class="leval-all-seller">level 1 seller</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="courses-icon course-pricevalue">
                                                                <p>STARTING AT <span>£
                                                                 @if(isset($v->v_budget_amt))
                                                                      {{$v->v_budget_amt}}  
                                                                 @endif   
                                                                </span></p>
                                                            </div>
                                                            <div class="rating-leval">
                                                                <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="5" required>
                                                                <p>4.9 <span class="point">( 34 )</span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                         
                        @else
                             <div class="col-sm-12">
                                <div class="all-category all-cate-results">
                                    
                                    <div class="category-text">
                                        <h2 style="text-align:center;">Courses data can not found.</h2>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
   
                   
                </div>
            </div>
        </div>
    </div>
    <!--end-04B-filtering-results-in person-list view-jobss-->



@stop






@section('js')


<script type="text/javascript">
    $(function() {
     $('.menulink').click(function(e){
         e.preventDefault();
         var click_val = $('#img_click').val();
         if(click_val == 1){
                $("#bg").attr('src',"{{url('public/Assets/frontend/images/hartA.png')}}");
                $('#img_click').val(2);
         }
         if(click_val == 2){
                $("#bg").attr('src',"{{url('public/Assets/frontend/images/hartB.png')}}");
                $('#img_click').val(1);
         }
     });
    });
</script>

<script type="text/javascript">



function viewchange(data) {
   
    if(data=="grid"){
        $("#listview").css("display","none");
        $("#gridview").css("display","inline");
    }else{  
        $("#listview").css("display","block");
        $("#gridview").css("display","none");
    }


}

function masterfilter(){
    $("#searchmaster").submit();
}




</script>

@stop

