
@extends('layouts.frontend')

@section('content')


    
    @php
        $totalWork=0;
        $totalSupport=0;
        $totalGrowth=0;
        $totalWorkAgain=0;
        $totalavg=0;
        $totalWorkavg =0;
        $totalSupportavg =0;
        $totalGrowthavg =0;
        $totalWorkAgainavg=0;

        
        if(count($buyerreview)>0){
            foreach($buyerreview as $k=>$v){
                $totalWork = $totalWork+$v->i_work_star;
                $totalSupport = $totalSupport+$v->i_support_star;
                $totalGrowth = $totalGrowth+$v->i_opportunities_star;
                $totalWorkAgain = $totalWorkAgain+$v->i_work_again_star;
            }
            $totalWorkavg = $totalWork/count($buyerreview);
            $totalSupportavg = $totalSupport/count($buyerreview);
            $totalGrowthavg = $totalGrowth/count($buyerreview);
            $totalWorkAgainavg = $totalWorkAgain/count($buyerreview);

            $totalavg = $totalWorkavg+$totalSupportavg+$totalGrowthavg+$totalWorkAgainavg;
            $totalavg = $totalavg/4;

        }
        
        
    @endphp



<div class="modal fade" id="sendMessage" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 600px">
            <div class="modal-content modal-content-1">
                <button type="button" class="close closemodal" data-dismiss="modal"></button>
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> Contact Buyer </h4>
                </div>
                <form role="form" method="POST" name="sendMessageForm" id="sendMessageForm" data-parsley-validate enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="i_to_id" value="{{$jobs->hasUser()->id}}">
                <input type="hidden" name="i_job_id" value="{{$jobs->id}}">
                
                <div class="modal-body">
                    <div class="modal-body-1" style="padding: 0px 30px !important;" id="contactsellerdetail">
                        <div class="row">
                            <div id="popupcommonmsg"></div>
                            <div class="col-xs-12">
                              <div class="text-box-containt">
                                    <label> Subject </label>
                                    <input type="text" class="form-control popup-letter" name="v_subject_title" required>
                                </div>
                            </div>    
                            <div class="col-xs-12">
                                <div class="text-box-containt">
                                    <label> Enter Your Message </label>
                                    <textarea rows="3" name="l_message" class="form-control popup-letter" required></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button  type="button" onclick="submitMessage()" class="btn btn-Submit-pop">Send Message</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
</div>

<div id="sendMessageSuccess" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;">Message</h4>
      </div>
      <div class="modal-body">
        <p id="popupmessage" style="text-align: center;"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

{{-- <div class="modal fade" id="sendMessageSuccess" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content modal-content-1">
                
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title"> Message </h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <div id="successmsgpop" style="display: none;">
                              <div class="alert alert-success alert-big alert-dismissable br-5">
                                <center>
                                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>succesfully send message<span></span>
                                </center>
                              </div>
                            </div>

                            <div id="errormsgpop" style="display: none;">
                              <div class="alert alert-danger alert-big alert-dismissable br-5">
                                <center>
                                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>Something went wrong.please try again after sometime<span></span>
                                </center>
                              </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="stb-btn">
                                    <center>
                                    <button  type="button" data-dismiss="modal" class="btn btn-Submit-pop">Close</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
</div> --}}

<?php
    $v_cat="";
    if(count($jobs->hasCategory()) && isset($jobs->hasCategory()->v_name)){
        $v_cat=$jobs->hasCategory()->v_name;
    }
    $v_name_main_skill="";
    if(count($jobs->hasSkill()) && isset($jobs->hasSkill()->v_name)){
        $v_name_main_skill =$jobs->hasSkill()->v_name;
    }
   
    $v_name_other_skill="";
    if(count($jobs->hasOtherSkill()) && isset($jobs->hasOtherSkill()->v_name)){
        $v_name_other_skill=$jobs->hasOtherSkill()->v_name;
    }
    $i_long_expect_duration = isset($jobs->i_long_expect_duration) ? explode(' ', $jobs->i_long_expect_duration) : '';

    $v_image="";
    if(count($jobs->hasUser()) && isset($jobs->hasUser()->v_image)){
        $v_image=$jobs->hasUser()->v_image;
    }

    $v_plan="";
    if(count($jobs->hasUser()) && isset($jobs->hasUser()->v_plan)){
        $v_plan=$jobs->hasUser()->v_plan;
    }
    $useridbuyer="";
    if(count($jobs->hasUser()) && isset($jobs->hasUser()->id)){
        $useridbuyer = $jobs->hasUser()->id;
    }
?>
    
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content modal-content-1">
                <div class="modal-header modal-header-1">
                    <h4 class="modal-title">Leave Your Review</h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body-1">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="modal-body-2">
                                    <span>Review rating</span>
                                </div>
                                <div class="rating-buyer">
                                    <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="3" required>
                                </div>
                                <div class="text-box-containt">
                                    <label>Enter your review</label>
                                    <textarea rows="7" class="form-control"></textarea>
                                </div>
                                <div class="stb-btn">
                                    <button class="btn btn-Submit-pop">Submit Review</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-1">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->
    <div class="responsive_sections">
        <!--25B-buyer-more-info-->
        <div class="container">
            <div style="clear: both"></div><br>
            <div id="commonmsg"></div>
            <div class="job-postion-info">
                <h1>{{isset($jobs->v_job_title) ? $jobs->v_job_title : ''}}</h1>
                <div class="info-job">
                    <span class="info">
                    <img src="{{url('public/Assets/frontend/images/lira.png')}}" style="max-width: 25px;" class="img-category-list" alt="" />
                    @if(isset($jobs->v_budget_type) && $jobs->v_budget_type=='total_budget')
                        Total Budget
                    @elseif(isset($jobs->v_budget_type) && $jobs->v_budget_type=='hourly_rate') 
                        Hourly Rate   
                    @elseif(isset($jobs->v_budget_type) && $jobs->v_budget_type=='open_to_offers')    
                        <b> Open to Offers </b>
                    @endif
                    @if(isset($jobs->v_budget_type) && $jobs->v_budget_type!='open_to_offers')
                    - £<span>@if(isset($jobs->v_budget_amt) && $jobs->v_budget_amt!=""){{$jobs->v_budget_amt}}@endif    
                    @endif
                    </span></span>
                    @if(isset($jobs->v_city) && $jobs->v_city!="")
                    
                    <span class="info"><img src="{{url('public/Assets/frontend/images/location.png')}}" style="max-width: 25px;" class="img-category-list1" alt="" />{{$jobs->v_city}}</span>
                    @endif
                </div>
                
                <div class="info-job-shortlist">
                    
                    @if(!auth()->guard('web')->check() || auth()->guard('web')->user()->id!=$jobs->i_user_id)
                    <a href="javascript:;" onclick="shortlist('{{$jobs->_id}}')">
                    <span class="info" id="shortlist_text">
                    <img src="{{url('public/Assets/frontend/images/shortlist.png')}}" class="img-category-list" alt="" />
                    @if(isset($is_shortlisted) && $is_shortlisted == 1)    
                        Shortlisted
                    @else
                        Add to Shortlist
                    @endif
                    </span>
                    </a>
                    @endif
                </div>
            </div>
        </div>

       

        <div class="container">
            <div class="all-detail-job">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="job-person">
                            @php
                            $imgdata = '';
                            
                            if(isset($jobs->v_photos) && count($jobs->v_photos)){
                                if(Storage::disk('s3')->exists($jobs->v_photos[0])){
                                    $imgdata = \Storage::cloud()->url($jobs->v_photos[0]);
                                }else{
                                    $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                }
                            }else{
                                $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                            }
                            @endphp 
                            <img src="{{$imgdata}}" alt="" />
                        </div>
                    </div>

                   
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="job-maintent">
                                    <h2>JOB POSTED IN:</h2>
                                    @if(isset($jobs->v_service) && $jobs->v_service == "inperson")
                                     <a href="{{url('courses-search')}}?e_type=inperson&keyword=&i_category_id={{$jobs->i_category_id}}&v_type_skill=on&v_type_job=on"> <b> {{$v_cat or ''}} </b> </a>
                                    @else
                                     <a href="{{url('courses-search')}}?e_type=online&keyword=&i_category_id={{$jobs->i_category_id}}&v_type_skill=on&v_type_job=on"> <b> {{$v_cat or ''}} </b> </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="job-maintent">
                                    <h2>MAIN SKILL:</h2>
                                    <span style="color: #337ab7"> {{$v_name_main_skill or ''}} </span>
                                </div>
                            </div>
                            
                            @if(isset($jobs->d_start_date) && $jobs->d_start_date!="")
                            <div class="col-xs-6">
                                <div class="job-maintent">
                                    <h2>JOB START DATE:</h2>
                                    <span style="color: #337ab7">
                                    {{$jobs->d_start_date or ''}}
                                    </span>
                                </div>
                            </div>
                            @endif

                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="job-maintent">
                                    <h2> DESIRABLE SKILLS:</h2>
                                    <span style="color: #337ab7"> {{$other_skills_data or ''}}</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="job-maintent">
                                    <h2> JOB TIME PERIOD:</h2>
                                    <span style="color: #337ab7"> 
                                    @if($jobs->i_permanent_role=="on")
                                        Permanent
                                    @else
                                        {{$i_long_expect_duration[0]}} {{$jobs->v_long_expect_duration_type}} {{isset($jobs->i_permanent_role) && $jobs->i_permanent_role == 'selected' ? 'or Permanent' : ''}}
                                    @endif
                                     </span>
                                </div>
                            </div>
                        </div>
                         @if(isset($jobs->v_skill_level_looking) && $jobs->v_skill_level_looking!="")
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="job-maintent">
                                    <h2> SKILL LEVEL REQUIRED:</h2>
                                    <span style="color: #337ab7"> {{ucfirst($jobs->v_skill_level_looking)}} </span>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="col-sm-3">
                        <div class="review-final">
                            <h2 class="color-font-black"> REVIEW BY SELLERS: </h2>
                            
                            <p class="rating-point  review_by_sellers">
                                <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalavg}}" readonly>&nbsp;&nbsp;<a href="#reviewblock_job" style="color: #000000 !important">{{number_format($totalavg,1)}}
                                <span class="number-space color-font-black">({{count($buyerreview)}})</span></a>
                            </p>


 

                            <p class="complited-job color-font-black">Total Jobs Posted : <span class="color-font-black number-space">{{$totaljob}}</span></p>
                            {{-- <ul class="list-unstyled online">
                                <li> <img src="{{url('public/Assets/frontend/images/green-round.png')}}" alt="tick"> ONLINE </li>
                            </ul> --}}
                            @if(!auth()->guard('web')->check() || auth()->guard('web')->user()->id!=$jobs->i_user_id)
                            <button type="button" class="btn btn-final-job" onclick="SendMessage('{{$useridbuyer}}')">Contact Me</button>
                            @if(isset($is_applied) && $is_applied == 1)
                                <button type="button" class="btn btn-final-job" id="apply_btn_{{$jobs->_id}}" value="" disabled="disabled"> APPLIED </button>
                            @else
                                <button type="button" class="btn btn-final-job" onclick="jobApply('{{$jobs->_id}}')" id="apply_btn_{{$jobs->_id}}" value=""> APPLY NOW </button>
                            @endif
                            @else

                            <button type="button" class="btn btn-final-job">Contact Me</button>
                            @if(isset($is_applied) && $is_applied == 1)
                                <button type="button" class="btn btn-final-job" id="apply_btn_{{$jobs->_id}}" value="" disabled="disabled"> APPLIED </button>
                            @else
                                <button type="button" class="btn btn-final-job" id="apply_btn_{{$jobs->_id}}" value=""> APPLY NOW </button>
                            @endif


                            @endif
                          
                            <div class="mo-number color-font-black">
                                @if(isset($jobs->v_contact) && $jobs->v_contact!="")
                                <img src="{{url('public/Assets/frontend/images/fon.png')}}" alt="" /> <span class="color-font-black">
                                @if(isset($v_plan['id']) && $v_plan['id']!="5a65b48cd3e812a4253c9869")
                                {{$jobs->v_contact}}
                                @else
                                    @php
                                        echo substr($jobs->v_contact, 0, -4).'XXXX';              
                                    @endphp
                                @endif
                                </span>
                                @endif

                                @if(isset($v_plan['id']) && $v_plan['id']!="5a65b48cd3e812a4253c9869")
                                    @if(isset($jobs->v_website) && $jobs->v_website!="")
                                    <br/>
                                    <span class="color-font-black">{{$jobs->v_website}}</span>
                                    @endif
                                @endif
                            </div>
                            @if(isset($jobs->v_avalibility_from) && $jobs->v_avalibility_from!="" && isset($jobs->v_avalibilitytime_from) && $jobs->v_avalibilitytime_from!="")
                            
                            <div class="avalibility">
                                <p> Availability </p>
                                 <p class="change-color day-time">
                                    @if(isset($jobs->v_avalibility_from) && $jobs->v_avalibility_from!="")
                                        {{ucfirst(substr($jobs->v_avalibility_from, 0, 3))}}-
                                    @endif
                                    @if(isset($jobs->v_avalibility_to) && $jobs->v_avalibility_to!="")
                                        {{ucfirst(substr($jobs->v_avalibility_to, 0, 3))}} 
                                    @endif
                                    @if(isset($jobs->v_avalibilitytime_from) && $jobs->v_avalibilitytime_from!="")
                                        / {{date("h a",strtotime($jobs->v_avalibilitytime_from.':00'))}} -
                                    @endif
                                     @if(isset($jobs->v_avalibilitytime_to) && $jobs->v_avalibilitytime_to!="")
                                        {{date("h a",strtotime($jobs->v_avalibilitytime_to.':00'))}} 
                                    @endif
                                </p>
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="bullet-box">
                @php
                    if(isset($jobs->l_job_description)){
                        echo $jobs->l_job_description;
                    }    
                @endphp
            </div>
        </div>

        <!--slider-->
        <div class="container">
            <div class="Media-slider">
                <h3>Media</h3>
            </div>
        </div>
         
        <div class="container">
            <div class="online_job_slider responsive_online_job_slider">
                <div class="slider-nav">
                    
                    @if(isset($jobs->v_video) && count($jobs->v_video))
                        @foreach($jobs->v_video as $k=>$v)
                            @php
                            $videodata = \Storage::cloud()->url($v); 
                            @endphp
                             <div class="slider-look">
                                <div class="slide-img-final play_button" onclick="playPausevideogrid('{{$k}}')">
                                    <img class="play_button_icon" src="{{url('public/Assets/frontend/images/play_button.png')}}" class="imgcontroler" id="videoControlergrid"  alt="" />
                                    <span class="imgrem" id="workvideorem">
                                        <video class="slide-img-final">
                                          <source src="{{$videodata}}" type="video/mp4">
                                        </video> 
                                    </span>
                                </div>
                            </div>
                      @endforeach 
                    @endif


                    @if(isset($jobs->v_photos))
                        @foreach($jobs->v_photos as $k=>$v) 
                        @php
                        $imgdata = '';
                        if(Storage::disk('s3')->exists($v)){
                            $imgdata = \Storage::cloud()->url($v);
                        }else{
                            $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                        }
                        @endphp 
                            <div class="slider-look">
                              <div class="slide-img-final">
                                  <img src="{{$imgdata}}" class="pausevideo" alt="" />
                              </div>
                            </div>
                        @endforeach
                   @endif
                    
                    
                </div>
                <div class="slider-for">
                    
                    @if(isset($jobs->v_video) && count($jobs->v_video))
                        @foreach($jobs->v_video as $k=>$v)
                            @php
                            $videodata = \Storage::cloud()->url($v); 
                            @endphp
                                <div class="slide-location play_button" onclick="playPausevideogrid('{{$k}}')">
                                <img class="play_button_icon" src="{{url('public/Assets/frontend/images/play_button.png')}}" class="imgcontroler" id="videoControlergrid{{$k}}" alt="" />
                                    <span class="imgrem" id="workvideorem">
                                        <video width="100%" id="videoPlayergrid{{$k}}">
                                          <source src="{{$videodata}}" type="video/mp4">
                                        </video> 
                                    </span>
                                </div>    
                        @endforeach 
                    @endif


                    @if(isset($jobs->v_photos))
                        @foreach($jobs->v_photos as $k=>$v)
                        @php
                        $imgdata = '';
                        if(Storage::disk('s3')->exists($v)){
                            $imgdata = \Storage::cloud()->url($v);
                        }else{
                            $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                        }
                        @endphp  
                            <div class="slide-location ">
                              <img src="{{$imgdata}}" class="img-responsive" alt="" />
                            </div>
                        @endforeach
                    @endif

                      
                </div>
            </div>
        </div>
    </div>
    
    @if(count($buyerreview)>0)
    
    @php
        $totalWork=0;
        $totalSupport=0;
        $totalGrowth=0;
        $totalWorkAgain=0;
        
        foreach($buyerreview as $k=>$v){
            $totalWork = $totalWork+$v->i_work_star;
            $totalSupport = $totalSupport+$v->i_support_star;
            $totalGrowth = $totalGrowth+$v->i_opportunities_star;
            $totalWorkAgain = $totalWorkAgain+$v->i_work_again_star;
        }

        $totalWorkavg = $totalWork/count($buyerreview);
        $totalSupportavg = $totalSupport/count($buyerreview);
        $totalGrowthavg = $totalGrowth/count($buyerreview);
        $totalWorkAgainavg = $totalWorkAgain/count($buyerreview);

        $totalavg = $totalWorkavg+$totalSupportavg+$totalGrowthavg+$totalWorkAgainavg;
        $totalavg = $totalavg/4;
        
    @endphp

<div class="online_reviewblock" id="reviewblock_job">    
    <div class="container" >
        <div class="slideall-text review_align">
            <h3 style="width: auto;"> Reviews </h3> &nbsp;
            <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$totalavg}}" readonly>
            <span class="star-no"> &nbsp;&nbsp;{{number_format($totalavg,1)}} &nbsp;({{count($buyerreview)}})</span>
        </div>

        <div style="clear: both;"></div>
        
        <div class="row">
         <div class="col-sm-2 text-center">
             Person You Work For?
            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalWorkavg}}" readonly >
         </div>
         <div class="col-sm-2 text-center">
            Support You Get
            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalSupportavg}}" readonly >
         </div>
        <div class="col-sm-2 text-center">
            Growth opportunities
            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalGrowthavg}}" readonly >
         </div>

         <div class="col-sm-2 text-center">
            Would You Work Again
            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalWorkAgainavg}}" readonly >
         </div>
      </div>
      <br/>
       
        <div class="buyer-reviews">
            
            <div id="jobreviewdata">
            </div>    
            
            <input type="hidden" name="reviewpage" id="reviewpage" value="1" autocomplete="off" />
            <div id="showmorebtn">
            <div class="load-profile">
                <button class="btn btn-load pull-left" onclick="jobReview('{{$jobs->id}}');">
                    Load More Reviews (<span>{{count($buyerreview)}}</span>)
                </button>
            </div>
            </div>
        </div>


    </div>
    </div>
    @endif
    <!--25B-buyer-more-info-->
@stop

@section('js')

    <script type="text/javascript">
       
        function playPausevideogrid(data) { 
            var video = document.getElementById("videoPlayergrid"+data);
            if (video.paused)
            {
                video.play();
                if (video.play)
                {
                    $("#videoControlergrid"+data).hide();
                }
            }
            else
            {   video.pause();
                if (video.pause)
                {
                    $("#videoControlergrid"+data).show();
                }
            }
        }


        $(document).on('click', '.slick-next', function () {
              $("video").each(function () { this.pause() });
              $(".play_button_icon").show();
          });

          $(document).on('click', '.pausevideo', function () {
              $("video").each(function () { this.pause() });
              $(".play_button_icon").show();
          });

          

          $(document).on('click', '.slick-prev', function () {
              $("video").each(function () { this.pause() }); 
              $(".play_button_icon").show();   
          });

    </script>

    <script type="text/javascript">
        function SendMessage(id=""){

            var actionurl = "{{url('message/userLogin/check')}}";
            var formData = "i_to_id="+id+"&from=seller";

            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    document.getElementById('load').style.visibility='hidden';
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#contactsellerdetail").html(obj['responsestr']);
                        $("#sendMessage").modal("show");
                    }else{
                      window.location = "{{url('login')}}";
                    }
                },
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                    // $("#successmsgpop").css("display","none");
                    // $("#errormsgpop").css("display","block");
                    $("#popupmessage").html("Something went wrong.please try again after sometime.");
                    $("#sendMessageSuccess").modal("show");
                    $("#sendMessage").modal("hide");
                }
            });
          }

          function submitMessage(type=""){
            
              var actionurl = "{{url('message/sendMessageSeller')}}";
              var formValidFalg = $("#sendMessageForm").parsley().validate('');
             
              if(formValidFalg){
                
                  document.getElementById('load').style.visibility="visible";  
                  var formData = new FormData($('#sendMessageForm')[0]);
                  
                  $.ajax({
                      processData: false,
                      contentType: false,
                      type    : "POST",
                      url     : actionurl,
                      data    : formData,
                      success : function( res ){
                          document.getElementById('load').style.visibility='hidden';
                          var obj = jQuery.parseJSON(res);
                          if(obj['status']==1){
                            $("#popupmessage").html(obj['msg']);
                            $("#sendMessageSuccess").modal("show");
                            $("#sendMessage").modal("hide");
                          }else{
                            $("#popupmessage").html(obj['msg']);
                            $("#sendMessageSuccess").modal("show");
                            $("#sendMessage").modal("hide");
                          }
                      },
                      error: function ( jqXHR, exception ) {
                          document.getElementById('load').style.visibility='hidden';
                          $("#popupmessage").html("Something went wrong.please try again after sometime.");
                          $("#sendMessageSuccess").modal("show");
                          $("#sendMessage").modal("hide");
                      }
                  });

              }    
          }
    </script>

  <script type="text/javascript">
        $(document).ready(function() {
            $('.resizing_select').change(function() {
                $("#width_tmp_option").html($('.resizing_select option:selected').text());
                $(this).width($("#width_tmp_select").width());
            });
        });
    </script>

  

    <script type="text/javascript">
        $(document).ready(function(){
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            // asNavFor: '.slider-nav',
            adaptiveHeight: true
        });
        $('.slider-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            // centerMode: true,
            focusOnSelect: true,
            prevArrow: "<a href='#' class='prev1'></a>",
            nextArrow: "<a href='#' class='next2'></a>",
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5,
                    }
                }, {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                }, {
                    breakpoint: 575,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
         });
    </script>

    <script>
        function move() {
            var elem = document.getElementById("myBar");
            var width = 50;
            var id = setInterval(frame, 10);

            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                    document.getElementById("demo").innerHTML = width * 1 + '%';
                }
            }
        }
    </script>

    <script type="text/javascript">
        $(".one").click(function() {
            $(".section1").addClass("intro");
        });
    </script>

    <script type="text/javascript">


     function shortlist(id=""){
        
        var actionurl = "{{url('shortlist')}}";
        var formdata = "_id="+id+"&type=jobs"; 
        document.getElementById('load').style.visibility="visible";  
        $.ajax({
            
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                document.getElementById('load').style.visibility='hidden';    
                var obj = jQuery.parseJSON(res);
                if(obj['status']==1){
                    $("#shortlist_text").html("Shortlisted");
                    $("#commonmsg").html(obj['msg']);
                }    
                else if(obj['status']==2){
                    $("#shortlist_text").html("Add to Shortlist");
                    $("#commonmsg").html(obj['msg']);
                }
                else if(obj['status']==3){
                   window.location = "{{'login'}}";
                }else{
                    $("#commonmsg").html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
            }
        });
    }

    function jobApply(id=""){
        var actionurl = "{{url('apply-job')}}";
        var formdata = "_id="+id;   
        document.getElementById('load').style.visibility="visible";  

        $.ajax({
            
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){

                document.getElementById('load').style.visibility='hidden';
                var obj = jQuery.parseJSON(res);
                if(obj['status']==1){
                    $('#apply_btn_'+id).text('Applied');
                    $('#apply_btn_'+id).prop('disabled', true);
                    $("#commonmsg").html(obj['msg']);
                    // $("#popupmessage").html(obj['msg']);
                    // $("#sendMessageSuccess").modal("show");
                }    
                else if(obj['status']==3){
                   window.location = "{{url('login')}}";
                }else{
                    $("#commonmsg").html(obj['msg']);
                    // $("#popupmessage").html(obj['msg']);
                    // $("#sendMessageSuccess").modal("show");
                }
            },
            error: function ( jqXHR, exception ) {
            }
        });
    }


    function jobReview(id=""){

          var page = $("#reviewpage").val();
          var actionurl = "{{url('job/review')}}";
          var formdata = "id="+id+"&page="+page; 
          page=parseInt(page)+1;
          $("#reviewpage").val(page)
          document.getElementById('load').style.visibility="visible"; 
          
          $.ajax({
              type    : "GET",
              url     : actionurl,
              data    : formdata,
              success : function( res ){
                var obj = jQuery.parseJSON(res);
                document.getElementById('load').style.visibility='hidden';
                if(obj['status']==1){
                    
                    $("#jobreviewdata").append(obj['responsestr']);
                    if(obj['btnstatus']==0){
                        $("#showmorebtn").html("");
                    }
                }    
                else{
                    $("#commonmsg").html(obj['msg']);
                }
              },
              error: function ( jqXHR, exception ) {
              }
          });

    }
      jobReview('{{$jobs->id}}');



      
    </script>

    <script type="text/javascript">
    $('a[href^="#reviewblock_job"]').on('click', function(event) {
        var target = $(this.getAttribute('href'));
        if( target.length ) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 2000);
        }
    });
</script>
   
@stop 
