@extends('layouts.frontend')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                @if ($success = Session::get('success'))
                  <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 15px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
                  </div>
                @endif
                @if ($warning = Session::get('warning'))
                  <div class="alert alert-info alert-danger" style="margin-top: 15px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
                  </div>
                @endif
            </div>
            
            @if(isset($issuccess) && $issuccess==1)
                
                @if(isset($page) && $page=="order")
                    <div class="col-xs-12">
                      <div class="thank_you">
                        <h1>Thank You for ordering with SKILLBOX</h1>
                        <h2>Your order has been successfully placed.</h2>
                        <p style="color: #000;">Click the button below to view your order details.</p>
                        <a href="{{$rurl}}" class="btn btn-default">Continue to Order Details</a>
                      </div>
                    </div>
                @elseif(isset($page) && $page=="plan")    
                  <div class="col-xs-12">
                      <div class="thank_you">
                        <h1>Thank you for subscribing with SKILLBOX.</h1>
                        <p style="color: #000;">Please click the button below to access your seller account.</p>
                        <a href="{{$rurl}}" class="btn btn-default">Continue to account</a>
                      </div>
                  </div>
                @else
                  <div class="col-xs-12">
                    <div class="thank_you">
                      <h1>Thank You for ordering with SKILLBOX</h1>
                      <p style="color: #000;">your payment has been completed successfully.</p>
                      <a href="{{$rurl}}" class="btn btn-default">Continue Dashboard</a>
                    </div>
                  </div>
                @endif
            @else
                <div class="col-xs-12">
                  <div class="thank_you">
                    <h1>You've cancelled this order. Please click the button below to continue to homepage.</h1>
                    <a href="{{url('/')}}" class="btn btn-default">Go to homepage</a>
                  </div>
                </div>
            @endif
          
        </div>
    </div>
    <!-- End 26B-control-panel-support-ticketing listing -->
 
@stop
@section('js')

{{-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127610087-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-127610087-1');
</script> --}}

<script type="text/javascript">

var transactiondata = '{{$trans}}';

transactiondata = transactiondata.replace(/\\n/g, "\\n").replace(/\\'/g, "\\'").replace(/\\"/g, '\\"').replace(/\\&/g, "\\&").replace(/\\r/g, "\\r").replace(/\\t/g, "\\t").replace(/\\b/g, "\\b").replace(/\\f/g, "\\f").replace(/&quot;/g, '"');
transactiondata = transactiondata.replace(/[\u0000-\u0019]+/g,""); 

var transactiondata = JSON.parse(transactiondata)

var itemdata = '{{$items}}';

itemdata = itemdata.replace(/\\n/g, "\\n").replace(/\\'/g, "\\'").replace(/\\"/g, '\\"').replace(/\\&/g, "\\&").replace(/\\r/g, "\\r").replace(/\\t/g, "\\t").replace(/\\b/g, "\\b").replace(/\\f/g, "\\f").replace(/&quot;/g, '"');
itemdata = itemdata.replace(/[\u0000-\u0019]+/g,""); 

var itemdata = JSON.parse(itemdata)


if("id" in transactiondata && transactiondata.id!="" && itemdata.length>0){
  

  // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  //   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  //   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  //   })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  //   ga('create', 'UA-127610087-1', 'auto');
  //   ga('send', 'pageview');
  //   ga('require', 'ecommerce');

  //   ga('ecommerce:addTransaction', transactiondata);
  //   ga('ecommerce:addItem', itemdata);
  //   ga('ecommerce:send');
   
}



</script>

@stop

