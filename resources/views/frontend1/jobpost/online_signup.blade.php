@extends('layouts.frontend')

@section('content')
  
   <!-- Navigation Bar -->
    <div class="navigation-bar">
        <div class="container">
            <div id="content">
                <ul id="tabs" class="nav nav-tabs nav-dirservice" data-tabs="tabs">
                    <li class="active"><a href="#step1" data-toggle="tab" class="tab-name"> 1. Step </a></li>
                    <li><a href="#step2" data-toggle="tab" class="tab-name"> 2. Step </a></li>
                    <li><a href="#step3" data-toggle="tab" class="tab-name"> 3. Step </a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Navigation Bar -->

    <!-- 02B-sign-up-post-a-job-online-W1 -->
    <div class="container">
        <div id="my-tab-content" class="tab-content">
            
            <div class="tab-pane active" id="step1">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-dirservice">
                            <div class="step1-dirservices">
                                <div class="online-service">
                                    <h1>{{ucfirst($v_service)}} Services </h1>
                                    <p>Post A Job </p>
                                </div>
                                
                                <form class="horizontal-form" role="form" method="POST" name="signupste1pform" id="signupste1pform" data-parsley-validate enctype="multipart/form-data" >

                                    <input type="hidden" name="v_service" value="{{$v_service}}">
                                    {{ csrf_field() }}
                                    <div class="dropdown">
                                        <label class="sel"> Select your desired category </label>
                                        <select class="form-control form-sel" name="i_category_id" onchange="getSkill(this.value)" required >
                                            <option value="">-Select-</option>
                                            @if(isset($category) && count($category))
                                                @foreach($category as $k=>$v)
                                                    @if(isset($v->v_name) && isset($v->id))
                                                    <option value = '{{$v->id}}'>{{$v->v_name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif                                                  
                                        </select>
                                    </div>
                                    
                                    <div class="dropdown">
                                        <label class="sel"> Main Skill You are looking for </label>
                                        <select class="form-control form-sel" name="i_mailskill_id" id="mainskill" required>
                                            <option value="">-Select-</option>
                                        </select>
                                    </div>
                                    
                                    <div class="multiple-category">
                                        <label class="sel"> What other desirable skills you're looking into the Freelancer? </label>
                                        <select class="form-control form-sel" name="i_otherskill_id" id="otherskill" required>
                                            <option value="">-Select-</option>
                                        </select>
                                    </div>


                                    <div class="radio-choise">
                                        <label class="sel"> What type of skill level you're looking for?
                                        </label>
                                        <div class="reg">
                                            <bdo dir="ltr">
                                                <input type="radio" checked class="one" value="entry" name="v_skill_level_looking">
                                                <span></span>
                                                <abbr>Entry</abbr>
                                            </bdo>
                                            <bdo dir="ltr">
                                                <input type="radio" class="one" value="intermediate" name="v_skill_level_looking">
                                                <span></span>
                                                <abbr>Intermediate</abbr>
                                            </bdo>
                                            <bdo dir="ltr">
                                                <input type="radio" class="one" value="expert" name="v_skill_level_looking">
                                                <span></span>
                                                <abbr>Expert</abbr>
                                            </bdo>
                                        </div>
                                    </div>
                                  
                                  <hr class="hr-line">
                                  <div class="bottom-btn">

                                      <button type="button" class="btn form-save-exit ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="saveandexit1" onclick="saveStep1('saveandexit1')">Save &amp; Exit</button>
                                        
                                      <button type="button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savenext2" class="btn form-next ladda-button" onclick="saveStep1('savenext2')">Next</button>
                                  </div>

                                </form>
                                
                            </div>
                        </div>
                        
                        <div class="alert alert-info alert-danger" id="errormsg" style="display: none;">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;<span id="errortxt">Something went wrong.</span>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End 02B-sign-up-post-a-job-online-W1 -->


            <!-- 02B-sign-up-post-a-job-online-W2 -->
            <div class="tab-pane" id="step2">
                <div class=" row">
                    <div class="col-sm-12">
                        <div class="main-dirservice2">
                            <div class="step2-dirservice">
                                <div class="step2-containt">
                                    <div class="title-dirservice">
                                        <h2> {{ucfirst($v_service)}} Services </h2>
                                        <p> Post A Job </p>
                                    </div>
                                    
                                    <form class="horizontal-form" role="form" method="POST" name="signupste2pform" id="signupste2pform" data-parsley-validate enctype="multipart/form-data" >
                                    {{ csrf_field() }}
                                    <input type="hidden" name="v_job_id" id="v_job_id">

                                    <div class="dirservice-form">
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Job Title
                                                        </label>
                                                        <input type="text" class="form-control input-field" name="v_job_title" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="radio-choise">
                                                    <div class="col-xs-12">
                                                        <label class="field-name">
                                                            What budget do you have in mind?
                                                        </label>
                                                        <div class="">
                                                            <div class="reg">
                                                                <bdo dir="ltr">
                                                                    <input type="radio" value="total_budget" name="v_budget_type">
                                                                    <span></span>
                                                                    <abbr> Total Budget </abbr>
                                                                </bdo>
                                                            </div>
                                                            <div class="reg">
                                                                <bdo dir="ltr">
                                                                    <input type="radio" value="hourly_rate" name="v_budget_type">
                                                                    <span></span>
                                                                    <abbr> Hourly Rate </abbr>
                                                                </bdo>
                                                            </div>
                                                            <div class="reg">
                                                                <bdo dir="ltr">
                                                                    <input type="radio" value="open_to_offers" name="v_budget_type">
                                                                    <span></span>
                                                                    <abbr> Open to Offers </abbr>
                                                                </bdo>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Specify your budget
                                                        </label>
                                                        <input type="text" class="form-control input-field" data-parsley-type="number" name="v_budget_amt" required>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    
                                                    <div class="textbox-description">
                                                        <label class="field-name">
                                                            Describe the Job
                                                        </label>
                                                        <textarea name="l_job_description" required></textarea>
                                                    </div>
                                                    <script>
                                                        CKEDITOR.replace('l_job_description');
                                                    </script>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Website
                                                        </label>
                                                        <input type="text" class="form-control input-field" name="v_website" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Contact Phone
                                                        </label>
                                                        <input type="text" class="form-control input-field" data-parsley-type="number" name="v_contact" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <label class="field-name">
                                                        Your availability
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-sm-5 col-xs-12">
                                                            <div class="timepicker-responsive">
                                                                <div class="dirservice-deshline">
                                                                    <div class="dirservice-desh">
                                                                        
                                                                        <select class="form-control date-concept" name="v_avalibility_from" required>
                                                                            <option value="monday">Monday</option>
                                                                            <option value="tuesday">Tuesday</option>
                                                                            <option value="wednesday">Wednesday</option>
                                                                            <option value="thursday">Thursday</option>
                                                                            <option value="Friday">Friday</option>
                                                                            <option value="saturday">Saturday</option>
                                                                            <option value="sunday">Sunday</option>
                                                                        </select>

                                                                    </div>
                                                                    <select class="form-control date-concept" name="v_avalibility_to" required>
                                                                            <option value="monday">Monday</option>
                                                                            <option value="tuesday">Tuesday</option>
                                                                            <option value="wednesday">Wednesday</option>
                                                                            <option value="thursday">Thursday</option>
                                                                            <option value="Friday">Friday</option>
                                                                            <option value="saturday">Saturday</option>
                                                                            <option value="sunday">Sunday</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-5 col-xs-12">
                                                            <div class="timepicker-responsive">
                                                                <div class="time">
                                                                    <div class="control-group">
                                                                        <input size="16" type="text" value="HH:mm" class="form-control form_datetime1  date-concept" name="v_avalibilitytime_from" required>
                                                                        <div class="timepicker-desh">-</div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <input size="16" type="text" value="HH:mm" class="form_datetime form-control date-concept" name="v_avalibilitytime_to" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <label class="field-name">
                                                        When would you like the job to start?
                                                    </label>
                                                </div>
                                                <div class="col-sm-8 col-xs-12">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-xs-12">
                                                            <div class="dirservice-slashline">
                                                                <div class="dirservice-slash">
                                                                    
                                                                    <select class="form-control date-concept" name="v_start_month" required>
                                                                        <option value="1">January</option>
                                                                        <option value="2">February</option>
                                                                        <option value="3">March</option>
                                                                        <option value="4">April</option>
                                                                        <option value="5">May</option>
                                                                        <option value="6">June</option>
                                                                        <option value="7">July</option>
                                                                        <option value="8">August</option>
                                                                        <option value="9">September</option>
                                                                        <option value="10">Octomber</option>
                                                                        <option value="11">November</option>
                                                                        <option value="12">December</option>
                                                                    </select>

                                                                </div>
                                                                <div class="dirservice-slash">
                                                                    <select class="form-control date-concept" name="v_start_date" required>
                                                                        @for($i=1;$i<=31;$i++)
                                                                             <option value='{{$i}}'>{{$i}}</option> 
                                                                        @endfor
                                                                    </select>
                                                                </div>
                                                                <select class="form-control date-concept" id="selectElementId" name="v_start_year" required>
                                                                    <?php $year1 =  date("Y"); ?>
                                                                    <option value='{{$year1}}'>{{$year1}}</option> 
                                                                    @for($i=1;$i<=2;$i++)
                                                                       <?php $year = $year1+$i; ?>
                                                                        <option value='{{$year}}'>{{$year}}</option> 
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <style type="text/css">
                                          #workvideoid {
                                              height: 0;
                                              width: 0;
                                          }
                                        </style>
                                        <div class="row">
                                            
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="open-browser">
                                                    <label class="field-name">
                                                        Add your videos (unlimited)
                                                    </label>
                                                    <input type="text" class="form-control input-field" id="work_video" disabled>
                                                    <div class="text-right">
                                                        <input type='file'  id="workvideoid" onchange="workvideo();" name="work_video" />
                                                        <label id="fileupload"  class="btn-editdetail" for="workvideoid">Browser</label>

                                                    </div>
                                                </div>
                                            </div>

                                             <script type="text/javascript">
                                        
                                              function workvideo(){
                                                  var filename = $('#workvideoid').val().split('\\').pop();
                                                  var lastIndex = filename.lastIndexOf("\\");   
                                                  $('#work_video').val(filename);
                                              }
                                            
                                            </script>
                                            <style type="text/css">
                                                  #workphotoid {
                                                      height: 0;
                                                      width: 0;
                                                  }
                                            </style>      
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="open-browser">
                                                    <label class="field-name">
                                                        Add your photos (up to 10)
                                                    </label>
                                                    <input type="text" class="form-control input-field" id="work_photo" disabled>
                                                    <div class="text-right">
                                                        <input type='file' id="workphotoid" onchange="workvideo();" name="work_photo" />
                                                        <label id="fileupload" class="btn-editdetail" for="workphotoid">Browser</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <label class="subfield-name">
                                                        How long do you expect the job to last?
                                                    </label>
                                                    <div class="row">
                                                        <div class="col-sm-7">
                                                            <div class="dilivery-hours">
                                                                <input type="text" class="form-control input-field" name="i_long_expect_duration" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <div class="dilivery-time">
                                                                <select class="form-control form-sell" name="v_long_expect_duration_type" required>
                                                                    <option value="hours">Hours</option>
                                                                    <option value="day">Day</option>
                                                                    <option value="months">Months</option>
                                                                    <option value="year">Years</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="checkbox-space">
                                                        <label class="checkbox">
                                                            <input type="checkbox" name="i_permanent_role" required>
                                                            <span class="subfield-name"> 
                                                                Permanent role
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="dirservice-field">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name" >
                                                            City
                                                        </label>
                                                        <input type="text" class="form-control input-field" name="v_city" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="your-field">
                                                        <label class="field-name">
                                                            Pincode
                                                        </label>
                                                        <input type="text" class="form-control input-field" name="v_pincode" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="buttet-concept">
                                            <div class="name-details">
                                                <label class="field-name">
                                                    Add search tags
                                                </label>
                                            </div>
                                            <div class="name-details">
                                                <label class="subfield-name">
                                                    Please add up to 3 search tags related to your service or job.
                                                </label>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-10">
                                                            <div class="input-login">
                                                                <input class="form-control business-name-control" type="text" placeholder="Start typing ...." id="temp_text">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 terms-add" id="hiddentags">
                                                            <div class="login-btn">
                                                                <button type="button" class="btn add-now" onclick="myFunction()">Add</button>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="selection-point" id="add_text">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="btn-backnext">

                                            <button type="button" class="btn form-save-exit ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="saveback1" onclick="saveStep2('saveback1')"> Save & Back</button>

                                            <button type="button" class="btn form-next ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="savenext3" onclick="saveStep2('savenext3')"> Next </button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                       
                        <div style="clear: both"></div>    
                        <div class="alert alert-info alert-danger" id="errormsgstep2" style="display: none;margin-top: 20px;">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;<span id="errortxtstep2">Something went wrong.</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End 02B-sign-up-post-a-job-online-W2 -->


            <!-- 02B-sign-up-post-a-job-online-W3 -->
            <div class="tab-pane" id="step3">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main-dirservice3">
                            

                            <form class="horizontal-form" role="form" method="POST" name="planform" id="planform" action="{{url('job-post/plan-data')}}" data-parsley-validate enctype="multipart/form-data" >
                            {{ csrf_field() }}

                            <div class="step3-dirservice">
                                <div class="title-dirservice">
                                    <h2> {{ucfirst($v_service)}} Services </h2>
                                    <p> Post A Job </p>
                                </div>

                                <div class="foll-option">
                                    Please choose the plan that best fits your needs
                                </div>
                                <div class="wrap">
                                        <fieldset>
                                            <div class="toggle">
                                                <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join" checked onchange="plandurationchange('monthly')" >
                                                <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                                <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create" onchange="plandurationchange('yearly')">
                                                <label for="create" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                                <span class="toggle-selection"></span>
                                            </div>
                                        </fieldset>
                                </div>
                            </div>

                            <div class="createLabel">
                                
                            @if(count($plandata))
                                @foreach($plandata as $key=>$val)

                                <div class="section">
                                    <div class="row">
                                        
                                        <div class="col-sm-3 col-xs-12">
                                            <div class="@if($key==0)left-choise @else left-side @endif">
                                                <div class="reg">
                                                    <bdo dir="ltr">
                                                        <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required>
                                                        <span class="select-seller-job"></span>
                                                        <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                    </bdo>
                                                </div>
                                                <div class="choise-que">{{$val['v_subtitle']}}</div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-3 col-xs-12">
                                            <ul class="list-unstyled choise-avalible">
                                                
                                                <?php 
                                                    $lcnt=1;
                                                ?>
                                                @if(count($val['l_bullet']))
                                                    @foreach($val['l_bullet'] as $k=>$v) 
                                                        @if($k<3)
                                                        <li class="right-mark"><img src="images/tick.png" class="tick" alt="" />{{$v}}</li>
                                                        <?php $lcnt = $lcnt+1; ?>
                                                        @endif
                                                    @endforeach
                                                @endif       
                                                
                                            </ul>
                                        </div>
                                        
                                        <div class="col-sm-4 col-xs-12">
                                            <div class="right-display">
                                                
                                                <ul class="list-unstyled choise-avalible">
                                                    <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k>=3)
                                                            <li class="right-mark"><img src="images/tick.png" class="tick" alt="" />{{$v}}</li>
                                                            @endif
                                                            <?php $lcnt = $lcnt+1; ?>
                                                        @endforeach
                                                    @endif  
                                                </ul>

                                                @if(count($val['l_addon']['text']))
                                                    @foreach($val['l_addon']['text'] as $k=>$v) 
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="{{$val['l_addon']['price'][$k]}}" name="v_plan[{{$v}}]">
                                                            <span class="urgent-offer"> 
                                                                {{$v}}
                                                            </span>
                                                        </label>
                                                    @endforeach
                                                @endif  

                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-2 col-xs-12 same-height-1">
                                            <div class="@if($key==0) free-price @else final-price @endif">
                                                <div class="all-in-data">
                                                    
                                                <div  class="monthyprice">
                                                       @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>{{$val['f_monthly_price']}}</del>
                                                            </div>
                                                            offer {{$val['f_monthly_dis_price']}} per month
                                                       @endif  
                                                </div>  
                                                
                                                <div class="yealryprice" style="display: none;">
                                                       @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                            <div class="final-price_text">
                                                            FREE
                                                            </div>
                                                       @else 
                                                            <div class="final-price_text">
                                                            <del>{{$val['f_yealry_price']}}</del>
                                                            </div>
                                                            offer {{$val['f_yealry_dis_price']}} per yearly
                                                       @endif  
                                                </div>     

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                
                                @endforeach
                            @endif    
                            </div>
                            
                            <button type="submit" class="btn btn-payment">
                                Proceed
                            </button>
                            </form>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End 02B-sign-up-i-want-to-sell-in-person-SELLER-W3 -->
    </div>
    <!-- End step 1- Direct Services -->
  

@stop
@section('js')


<script type="text/javascript">

        function myFunction() {
            var dt = new Date();
            var dynamic_id = dt.getDate() + "" + dt.getHours() + "" + dt.getMinutes() + "" + dt.getSeconds() + "" + dt.getMilliseconds();
            var y = jQuery('#temp_text').val();
            var z = '<button>';
            jQuery('#add_text').append("<button class='auto-width' id='" + dynamic_id + "' onclick='removeButton(this.id)'><span>" + y + "</span><img src='{{url('public/Assets/frontend/images/close1.png')}}'></button>");

            var abval = $("#temp_text").val();
            var strhidden = '<input type="hidden" id="hidden-'+dynamic_id+'" name="v_search_tags[]" value="'+abval+'">';
            $("#hiddentags").append(strhidden);
            $("#temp_text").val("");
        }

        function removeButton(id) {
            jQuery("#" + id).remove();
            jQuery("#hidden-"+id).remove();
        }
         
      function getSkill(i_category_id){
            
            var formdata = "i_category_id="+i_category_id;
            var actionurl = "{{url('seller-signup/get-skill')}}";

            $.ajax({
                  type    : "GET",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){
                        $("#mainskill").html(obj['optionstr']);
                        $("#otherskill").html(obj['optionstr']);
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      $("#someerror").show();
                  }
              });    
        }

         function saveStep1(type=""){
          
              var actionurl = "{{url('job-post/online-update-step1')}}";
              var formValidFalg = $("#signupste1pform").parsley().validate('');

                if(formValidFalg){
                        
                    var l = Ladda.create(document.getElementById(type));
                    l.start();  
                    var formdata = $("#signupste1pform").serialize();

                    $.ajax({
                        type    : "POST",
                        url     : actionurl,
                        data    : formdata,
                        success : function( res ){
                            l.stop();
                            var obj = jQuery.parseJSON(res);
                          
                            if(obj['status']==1){
                                
                                $('#v_job_id').val(obj['jobid']);
                                if(type=="savenext2"){
                                    $('.nav-tabs a[href="#step2"]').tab('show');
                                }

                            }else{
                                $("#errormsg").show();
                                $("#errortxt").html(obj['msg']);
                            }
                        },
                        error: function ( jqXHR, exception ) {
                            l.stop();
                            $("#errormsg").show();
                        }
                    });
                }    
        }


        function saveStep2(type=""){
          
          var actionurl = "{{url('job-post/online-update-step2')}}";
          var formValidFalg = $("#signupste2pform").parsley().validate('');

          if(formValidFalg){
              
              var l = Ladda.create(document.getElementById(type));
              l.start();
              
              var formData = new FormData($('#signupste2pform')[0]);

              $.ajax({
                  processData: false,
                  contentType: false,
                  type    : "POST",
                  url     : actionurl,
                  data    : formData,
                  success : function( res ){
                      l.stop();
                      var obj = jQuery.parseJSON(res);
                      
                      if(obj['status']==1){
                        if(type == "savenext3"){
                            $('.nav-tabs a[href="#step3"]').tab('show');
                        }else{
                            $('.nav-tabs a[href="#step1"]').tab('show');
                        }
                      }else{
                        $("#errormsgstep2").show();
                        $("#errortxtstep2").html(obj['msg']);
                      }

                  },
                  error: function ( jqXHR, exception ) {
                      l.stop();
                      $("#errormsgstep2").show();
                  }
              });

          }    
        }


        //Plan duration change
        function plandurationchange(data=""){
            
            if(data=="monthly"){
                $(".monthyprice").css("display","block");
                $(".yealryprice").css("display","none");

            }else if(data=="yearly"){
                $(".yealryprice").css("display","block");
                $(".monthyprice").css("display","none");
            }
        }

</script>

@stop

