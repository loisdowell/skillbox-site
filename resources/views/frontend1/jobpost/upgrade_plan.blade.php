@extends('layouts.frontend')

@section('content')
    
 <style type="text/css">
    .modal-new .modal-backdrop {z-index: 0;}
    /*footer{position: fixed;width: 100%;bottom: 0}*/
</style> 

    @php
        $standardplanFree= App\Helpers\General::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
    @endphp  

<!--modal-->
    <div id="upgradePlanModal" class="modal1 upgradePlanModal_custom" role="dialog">
        <div class="modal-content2">
            <div class="final-containt">
                <span class="close2"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" alt=""  onclick="upgradePlanModalClose()" /></span>
            </div>
            <div class="container">
                <div class="main-dirservice-popup">
                    
                    <form class="horizontal-form" role="form" method="POST" name="planform" id="planform" action="{{url('job-post/update-user-plan')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}

                    <div class="popup-dirservice">
                        <div class="title-dirservice">
                            <h4> Upgrade Your Plan </h4>
                        </div>
                        <div class="foll-option-popup">
                            You're currently a {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }} member. Please upgrade your Plan.
                        </div>
                        
                        <div class="wrap">
                            <fieldset>
                                <div class="toggle">
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="join" checked onchange="plandurationchange('monthly')" >
                                    <label for="join" class="toggle-label toggle-label-off" id="joinLabel">Monthly</label>
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="create" onchange="plandurationchange('yearly')">
                                    <label for="create" class="toggle-label toggle-label-on" id="createLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    @php
                       $selected="";

                    @endphp
                    @if(count($plandata))
                        @foreach($plandata as $key=>$val)
                        
                            @if($val['id'] == $userplan['id'])
                                @php $selected =1; @endphp
                            @endif   

                            @if($selected==1)
                                <div class="final-leval-popup">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-12">
                                            <div class="@if($key==0)left-choise @else left-side @endif">
                                                <div class="reg">
                                                    <bdo dir="ltr">
                                                        <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required @if($val['id'] == $userplandata['v_plan_id']) checked  @endif >
                                                        <span class="select-seller-job"></span>
                                                        <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                    </bdo>
                                                </div>
                                                <div class="choise-que">{{$val['v_subtitle']}}</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-xs-12">
                                            
                                            <ul class="list-unstyled choise-avalible">
                                                <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k<3)
                                                            <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                            <?php $lcnt = $lcnt+1; ?>
                                                            @endif
                                                        @endforeach
                                                    @endif  
                                            </ul>

                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <div class="right-display">
                                                <ul class="list-unstyled choise-avalible">
                                                    <?php 
                                                            $lcnt=1;
                                                        ?>
                                                        @if(count($val['l_bullet']))
                                                            @foreach($val['l_bullet'] as $k=>$v) 
                                                                @if($k>=3)
                                                                <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                                @endif
                                                                <?php $lcnt = $lcnt+1; ?>
                                                            @endforeach
                                                        @endif  

                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 same-height-1">
                                            <div class="@if($key==0) free-price @else final-price @endif">
                                                <div class="all-in-data">
                                                    <div  class="monthyprice">
                                                           @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_monthly_price']}}</del>
                                                                </div>
                                                                Only £{{$val['f_monthly_dis_price']}} p/m
                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif
                                                           @endif  
                                                    </div>  
                                                    
                                                    <div class="yealryprice" style="display: none;">
                                                           @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_yealry_price']}}</del>
                                                                </div>
                                                                Only £{{$val['f_yealry_dis_price']}} p/a

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif
                                                           @endif  
                                                    </div>    


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif        

                    <button type="submit" class="btn btn-payment">
                        Proceed
                    </button>

                    </form>

                </div>
                <!-- End 26B-my-profile-with-sponsor-button-POP UP-in-person-or-online -->
            </div>
        </div>
    </div>
<!--end-modal-->


   <!-- 26B-job-posting -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="title-support">
                    <h1> Upgrade your plan </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="content">
          
            <div class="tab-pane active" id="completed">
                <div class="order-status">
                    <div class="alert alert-info alert-success" id="sponsorsuccmsg" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span>Thank you, your message is now sponsored.</span>
                    </div>

                    <div class="alert alert-info alert-danger" id="sponsorerrmsg" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span>Something went wrong.please try again after some time.</span>
                    </div>

                    <div id="commonmsg"></div>
                    
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">

                            @if ($success = Session::get('success'))
                              <div class="alert alert-success alert-big alert-dismissable br-5">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;Done</strong>&nbsp;&nbsp;{{ $success }}
                              </div>
                            @endif
                            @if ($warning = Session::get('warning'))
                              <div class="alert alert-info alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;{{ $warning }}
                              </div>
                            @endif

                            @php
                                $jobpost=0;
                                if($userplandata['v_plan_id']=="5a65b48cd3e812a4253c9869"){
                                    $jobpost=5;    
                                }else if($userplandata['v_plan_id']=="5a65b757d3e8125e323c986a"){
                                    $jobpost=10;    
                                }else{
                                    $jobpost=30;    
                                }
                            @endphp
                            @if($userplandata['v_plan_id']=="5a65b757d3e8125e323c986a")

                                <h3>Your current plan is a {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }}. You cannot post more than {{$jobpost}} job postings.</h3>
                            @else
                            
                            <h3>Your current plan is a {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }}. You cannot post more than {{$jobpost}} job postings under {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }} at anyone time. Please upgrade your plan.</h3>

                            <div class="navigation-bar-postiion">
                                <div class="" style="    text-align: center;margin-top: 35px;">
                                    <button type="button" class="btn btn-upgrade" onclick="upgradeplanmodal()">UPGRADE</button>
                                </div>
                            </div>

                            @endif
                            
                             
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- End 26B-job-posting -->

        </div>
    </div>
  
@stop
@section('js')

<script type="text/javascript">

    var modal1 = document.getElementById('upgradePlanModal');
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close2")[0];
    btn.onclick = function() {
        modal1.style.display = "block";
    }
    span.onclick = function() {
        modal1.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal1) {
            modal1.style.display = "none";
        }
    }
    
    function upgradeplanmodal(){
        $("#upgradePlanModal").modal("show");
    }

    function plandurationchange(data=""){
            
            if(data=="monthly"){
                $(".monthyprice").css("display","block");
                $(".yealryprice").css("display","none");

            }else if(data=="yearly"){
                $(".yealryprice").css("display","block");
                $(".monthyprice").css("display","none");
            }
    }

    function upgradePlanModalClose(){
        $("#upgradePlanModal").modal("hide");
    }


</script>

<script type="text/javascript">

    function buyerSignup(){
          var formValidFalg = $("#buyersignupform").parsley().validate('');
          if(formValidFalg){
              var l = Ladda.create(document.getElementById('bsignup'));
              l.start();
              $("#buyersignupform").submit()
          } 
    }

    function SponsorNowModal(jid=""){
            $("#sjid").val(jid);
            $("#sponsorNow").modal("show");
    }

    function SponsorNow(){
            
            var actionurl = "{{url('job-post/sponsor-now')}}";
            var job_id = $("#sjid").val();    
            var formdata = "job_id="+job_id;

            var l = Ladda.create(document.getElementById('SponsorNowJob'));
            l.start();  

            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    l.stop();
                    
                    var obj = jQuery.parseJSON(res);
                    
                    $("#sponsorNow").modal("hide");
                    if(obj['status']==1){
                        
                        $("#sponsorsuccmsg").show();
                        $("#sponsorsucmsg").html(obj['msg']);
                        
                        $("#sponsor_"+job_id).html("");
                        $("#sponsor_"+job_id).html("<span class='positive-status'>YES</span>");


                    }else{
                        $("#sponsorerrmsg").show();
                        $("#sponsorerrmsg").html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    l.stop();
                    $("#sponsorerrmsg").show();
                }

            });

    }

    function MarkUrgentModal(jid=""){
        $("#sjid").val(jid);
        $("#markUrgent").modal("show");
    }

    function markUrgent(){
            
        var actionurl = "{{url('job-post/mark-urgent')}}";
        var job_id = $("#sjid").val();    
        var formdata = "job_id="+job_id;

        var l = Ladda.create(document.getElementById('MarkUrgentJob'));
        l.start();  

        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                l.stop();
                
                var obj = jQuery.parseJSON(res);
                
                $("#markUrgent").modal("hide");
                if(obj['status']==1){
                    
                    $('#commonmsg').html(obj['msg']);
                    
                    $("#urgent_"+job_id).html("");
                    $("#urgent_"+job_id).html("<span class='positive-status'>YES</span>");


                }else if(obj['status']==2){
                    $("#upgradeurgent").modal("show");
                }else{
                    $("#sponsorerrmsg").show();
                    $("#sponsorerrmsg").html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                l.stop();
                $("#sponsorerrmsg").show();
            }

        });

    }


    function NotifyModal(jid=""){
        $("#sjid").val(jid);
        $("#notify").modal("show");
    }

    function Notify(){
            
        var actionurl = "{{url('job-post/notify')}}";
        var job_id = $("#sjid").val();    
        var formdata = "job_id="+job_id;

        var l = Ladda.create(document.getElementById('NotifyJob'));
        l.start();  

        $.ajax({
            type    : "GET",
            url     : actionurl,
            data    : formdata,
            success : function( res ){
                l.stop();
                
                var obj = jQuery.parseJSON(res);
                
                $("#notify").modal("hide");
                if(obj['status']==1){
                    
                    $('#commonmsg').html(obj['msg']);
                    
                    $("#notify_"+job_id).html("");
                    $("#notify_"+job_id).html("<span class='positive-status'>YES</span>");

                }else if(obj['status']==2){
                    $("#upgradenotify").modal("show");
                }
                else{
                    $('#commonmsg').html(obj['msg']);
                }
            },
            error: function ( jqXHR, exception ) {
                l.stop();
                $("#sponsorerrmsg").show();
            }

        });

    }


</script>

@stop

