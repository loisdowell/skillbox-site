@extends('layouts.frontend')
@section('content')
<style type="text/css">
   .modal-new .modal-backdrop {z-index: 0;}
   .form-save-exit{width: 250px !important;}
</style>

<div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="title-support">
                    <h1> Order Summery </h1>
                </div>
            </div>
        </div>
    </div>

<div class="container">
   <div class="main-edit-profile">
      <div class="edit-details" style="margin-top: 50px;">
         <div class="row">
               
               <div class="col-xs-12">
                  <div class="upload-details" style="margin: 0px auto;">
                     
                    
                    @if(isset($data['v_plan_charge']) && $data['v_plan_charge']=="yes")
                      <div class="row" style="margin: 5px 0px;">
                          <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
                             <label class="field-edit-name" style="margin-top: 5px;">
                              <b>User Plan subscription </b>: {{isset($data['v_plan_name']) ? $data['v_plan_name'] : ''}}
                             </label>
                          </div>
                      </div>
                    @endif

                    @if(isset($data['v_addon']) && count($data['v_addon']))
                          @foreach($data['v_addon'] as $k=>$v)
                          <div class="row" style="margin: 2px 0px;">
                              <div class="col-sm-12 col-xs-12" style="margin: 5px 0px; text-align: center;">
                                 <label class="field-edit-name" style="margin-top: 5px;">
                                  
                                  <b>
                                  @if($k=="jobpostnotify")
                                  Notify sellers
                                  @else
                                  Mark as urgent
                                  @endif    
                                  </b>: £{{$v}}
                                 </label>
                              </div>
                          </div>
                          @endforeach
                     @endif 

                    <div class="row" style="margin: 5px 0px;">
                        <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
                           <label class="field-edit-name" style="margin-top: 5px;">
                            <b>Total Amount Payable </b>: £{{isset($data['v_total_amount']) ? $data['v_total_amount'] : ''}}
                           </label>
                        </div>
                    </div>
                    
                    @if(isset($data['v_plan_charge']) && $data['v_plan_charge']=="yes")
                    <div class="row" style="margin: 5px 0px;">
                        <div class="col-sm-12 col-xs-12" style="margin: 15px 0px; text-align: center;">
                           <label class="field-edit-name" style="margin-top: 5px;">
                            <b>Plan Duration</b>: {{isset($data['v_plan_duration']) ? $data['v_plan_duration'] : ''}}
                           </label>
                        </div>
                    </div>
                    @endif

                    <div style="margin: 15px 0px; text-align: center;">

                         <a href="{{url('job-post/payment/mangopay')}}">
                         <button type="button" class="btn form-save-exit"> Proceed to payment </button>
                         </a>

                        
                        {{--
                         <a href="{{url('job-post/payment/paypal')}}">
                         <button type="button" class="btn form-save-exit">Pay with PayPal</button>
                         </a>
                         <button type="button" class="btn form-next">Pay with Escrow</button>
                        --}}

                    </div>

                  </div>
               </div>
            
         </div>
      </div>
      
   </div>
</div>
@stop
@section('js')
<script type="text/javascript">

</script>
@stop