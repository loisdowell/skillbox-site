@php
	$path = Route::getFacadeRoot()->current()->uri();
@endphp
    <header id="header">
        <div class="container">
            <div class="row">
                <nav class="navbar nav-main @if(!auth()->guard('web')->check()) without_login @endif">
                    <div class="container-fluid">
                        <div class="navbar-header navbar-header-final">
                            <button type="button" class="navbar-toggle icon-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand logo" href="{{url('')}}">
                                @php
                                      $logosrc= App\Helpers\General::SiteLogo();
                                @endphp  
                                <img src="{{$logosrc}}" alt="images">
                            </a>
                        </div>
                        <div class="collapse navbar-collapse sub-manu" id="myNavbar">
                            <div class="navbarmenu">
                            <ul class="nav navbar-nav navbar-right manu-nav">
                                {{-- <a class="btn-post-job_a" href="{{url('buyer/post-job')}}"> --}}
                                    <a class="btn-post-job_a" href="{{url('account/seller')}}">
                                    <li>
                                        <button class="btn btn-post-job">
                                            Freelancer?{{-- Post a Job --}}
                                        </button>
                                    </li>
                                </a>
                                
                                <li class="cart_btn"><a href="{{url('cart/data')}}"><i class="zmdi zmdi-shopping-cart"></i><span>Cart</span>
                                @if(sizeof(Cart::content()) > 0)    
                                <b class="badge">{{count(Cart::content())}}</b>
                                @endif
                                </a></li> 
                            </ul>

                            <form class="horizontal-form" role="form" method="get" name="searchmaster" id="searchmaster" action="{{url('search')}}" >

                            <ul class="nav navbar-nav manu-nav nav-find searchdataclass ">
                                <li>
                                    <div class="new" id="my-live-data">
                                        <!-- <img src=" {{url('public/Assets/frontend/images/search.png')}}" class="set-img"> -->
                                        <img src="{{url('public/Assets/frontend/images/down-arrow.png')}}" class="d-arrow">
                                        <ul class="my-live" style="display: none;">
                                            <li class="option-new"><a href="javascript:;" onclick="masterchange('inperson')">In Person</a></li>
                                            <li class="option-new"><a href="javascript:;" onclick="masterchange('online')">Online</a></li>
                                            <li class="option-new"><a href="javascript:;" onclick="masterchange('courses')">Courses</a></li>
                                        </ul>

                                        <input type="text" list="browserstop" name="keyword" class="input-new" placeholder="In Person" id="mastersearchtext" onkeyup="inpersonssearchtop(this.value)" >
                                        <datalist id="browserstop">
                                        </datalist>
                                        <input type="hidden" name="e_type" class="input-new" placeholder="" id="e_type" value="inperson">

                                    </div>
                                </li>
                                
                                <li id="mastersearchlocation">
                                    <input type="text" name="text" id="autocomplete_head" autocomplete="off"  class="form-control manu-map" placeholder="Location" oninvalid="this.setCustomValidity('Enter a location to see results close by')" oninput="setCustomValidity('')" >
                                    <input type="hidden" name="v_latitude" id="v_latitude_head">
                                    <input type="hidden" name="v_longitude" id="v_longitude_head">
                                </li> 

                                <li class="src_btn_hrdr">
                                    <button class="spsearchhead">
                                        {{-- <img src=" {{url('public/Assets/frontend/images/search.png')}}" /> --}}
                                    </button>
                                </li>
                                <li>
                                    {{-- <button type="button" class="manu-img browser-cust" @if($path=="/") onclick="categoryviewdata()" @else onclick="categoryviewdatahref()"  @endif>    
                                        <div class="home-img">
                                            <img src="{{url('public/Assets/frontend/images/browser.png')}}" alt="" />
                                        </div>
                                        <div class="home-img1">
                                            <img src="{{url('public/Assets/frontend/images/browser1.png')}}" alt="" />
                                        </div>
                                        Browse 
                                    </button> --}}

                                    <button type="button" class="manu-img browser-cust" @if($path=="/") onclick="categoryviewdata()" @else onclick="categoryviewdatahref()"  @endif>    
                                        <div class="home-img spbrowse1" >
                                            <img class = "topiconhighres" src="{{url('public/Assets/frontend/images/eye_pink.png')}}" alt="" />
                                        </div>
                                        <div class="home-img1 spbrowsewhite1">
                                            <img class = "topiconhighres" src="{{url('public/Assets/frontend/images/eye_white.png')}}" alt="" />
                                        </div>
                                        Browse 
                                    </button>

                                
                                </li>
                                
                                <li style="display: none;">
                                    <a href="16B-my-profile-home.html" class="manu-img">
                                        <div class="home-img">
                                            <img src="{{url('public/Assets/frontend/images/mobile.png')}}" class="mobile-icon" alt="" />
                                        </div>
                                        <div class="home-img1">
                                            <img src="{{url('public/Assets/frontend/images/mobile1.png')}}" class="mobile-icon" alt="" />
                                        </div>
                                        Get the app
                                    </a>
                                </li>

                                @if(!auth()->guard('web')->check())
                                <li>
                                   {{--  <a href="{{url('account/buyer')}}" class="manu-img">
                                        <div class="home-img">
                                            <img  src="{{url('public/Assets/frontend/images/join.png')}}" alt="" />
                                        </div>
                                        <div class="home-img1">
                                            <img src="{{url('public/Assets/frontend/images/join1.png')}}" alt="" />
                                        </div>
                                        Join
                                    </a> --}}

                                    <a href="{{url('account/buyer')}}" class="manu-img">
                                        <div class="home-img">
                                            <img  class = "topiconhighresuser" src="{{url('public/Assets/frontend/images/plus_pink.png')}}" alt="" />
                                        </div>
                                        <div class="home-img1">
                                            <img class = "topiconhighresuser" src="{{url('public/Assets/frontend/images/plus_white.png')}}" alt="" />
                                        </div>
                                        Join
                                    </a>


                                </li>
                                <li>
                                    {{-- <a href="{{url('login')}}" class="manu-img">
                                        <div class="home-img">
                                            <img src="{{url('public/Assets/frontend/images/user.png')}}" class="" alt="" />
                                        </div>
                                        <div class="home-img1">
                                            <img src="{{url('public/Assets/frontend/images/user2.png')}}" alt="" />
                                        </div>
                                        Log in
                                    </a> --}}

                                    <a href="{{url('login')}}" class="manu-img">
                                        <div class="home-img">
                                            <img class = "topiconhighresuser" src="{{url('public/Assets/frontend/images/login_pink.png')}}" class="" alt="" />
                                        </div>
                                        <div class="home-img1">
                                            <img class = "topiconhighresuser" src="{{url('public/Assets/frontend/images/login_white.png')}}" alt="" />
                                        </div>
                                        Log in
                                    </a>

                                </li>
                                @else
                                <li class="@if($path=='dashboard') active-manu @endif">
                                    {{-- <a href="{{url('dashboard')}}" class="manu-img">
                                        <div class="home-img">
                                            <img src="{{url('public/Assets/frontend/images/user.png')}}" alt="" />
                                        </div>
                                        <div class="home-img1">
                                            <img src="{{url('public/Assets/frontend/images/user2.png')}}" alt="" />
                                        </div>
                                        My Profile
                                    </a> --}}

                                    <a href="{{url('dashboard')}}" class="manu-img">
                                        <div class="home-img">
                                            <img class = "topiconhighresuser" src="{{url('public/Assets/frontend/images/login_pink.png')}}" alt="" />
                                        </div>
                                        <div class="home-img1">
                                            <img class = "topiconhighresuser" src="{{url('public/Assets/frontend/images/login_white.png')}}" alt="" />
                                        </div>
                                        My Profile
                                    </a>


                                </li>

                                @endif

                            </ul>

                        </form>
                        </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>

   