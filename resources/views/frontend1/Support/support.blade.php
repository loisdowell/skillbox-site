@extends('layouts.frontend')

@section('content')
    
   
 <!-- 26B-control-panel-support-ticketing listing -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1> Support </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                @if ($success = Session::get('success'))
                  <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 15px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
                  </div>
                @endif
                @if ($warning = Session::get('warning'))
                  <div class="alert alert-info alert-danger" style="margin-top: 15px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
                  </div>
                @endif

                <div class="main-support-ticket">
                    <div class="support-ticket">
                        Ticket Centre
                        <a href="{{url('support/add')}}" class="btn btn-new-ticket"> Create New Ticket</button> </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="contant-table">
                        <table class="table table-hover table-jobposting">
                            <thead>
                                <tr>
                                    <th> # </th>
                                    <th>CREATE DATE</th>
                                    <th>STATUS</th>
                                    <th>QUERY </th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                              @if(count($supportdata))
                                
                                @foreach($supportdata as $k=>$v)
                                <tr>
                                    <td> {{$k+1}}
                                    {{-- {{isset($v->i_ticket_num) ? $v->i_ticket_num : ''}}  --}}
                                    </td>
                                    <td> {{isset($v->d_added) ? date("M d, Y",strtotime($v->d_added)) : ''}}</td>
                                    <td> 
                                        @if(isset($v->e_status) && $v->e_status=="close" )
                                            Closed
                                        @else
                                            Open
                                        @endif
                                        {{-- {{isset($v->e_status) ? $v->e_status : ''}}  --}}
                                    </td>
                                    <td> {{isset($supportquery[$v->e_query]) ? $supportquery[$v->e_query] : ''}}</td>
                                    <td>
                                        <a href="{{url('support/view')}}/{{$v->id}}">
                                        <button type="button" class="btn btn-view"> View </button>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                              
                              @else
                                <tr>
                                    <td colspan="5">No tickets found.</td>
                                </tr>
                              @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-support-ticketing listing -->
   

@stop
@section('js')

<script type="text/javascript">

</script>

@stop

