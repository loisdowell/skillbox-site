@extends('layouts.frontend')

@section('content')
    <style type="text/css">
        .modal-new .modal-backdrop {z-index: 0;}
    </style> 

    <div id="viewdatajobs" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align: center;">Sponsored Ad Stats</h4>
          </div>
          <div class="modal-body">
            <p id="popupmessage" style="text-align: center;"></p>
            
            <div class="contant-table">
                <div class="table-responsive">
                    <table class="table table-hover table-sponsoreads s">
                        <thead>
                            <tr>
                                <th>TOTAL AD IMPRESSIONS</th>
                                <th>TOTAL AD CONTACTS</th>
                                <th>TOTAL SPONSORED CLICKS</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="IMPRESSIONSPOP">0</td>
                            <td id="CONTACTSPOP">0</td>
                            <td id="CLICKSPOP">0</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="upgradebutton" role="dialog">
        <div class="modal-dialog modal-dialog-popup" style="max-width:35% !important">
            <div class="modal-content modal-content-popup">
                <div class="modal-header modal-header-bottom">
                </div>
                <div class="modal-body">
                    <div class="popup-sponsore">
                        <div class="title-control-postion">
                            
                            <div class="title-popup-table">
                                <h3>Upgrade Plan</h3>
                            </div>

                             <div style="clear: both"></div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <p>You're on our Basic Plan. If you would like to sponsor the job please upgrade to our Standard or Premium account.</p>
                                </div>
                            </div>
                            <br/>
                            
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel & Close</button>
                                    <button type="button" class="btn btn-success" onclick="upgradeUserPlan()">Upgrade plan</button>
                                </div>
                            </div> 

                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-top">
                </div>
            </div>
        </div>
    </div>

    @php
        $standardplanFree= App\Helpers\General::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
    @endphp  

    <div id="sponserUpgradePlanModal" class="modal1 upgradePlanModal_custom" role="dialog">
        <div class="modal-content2">
            <div class="final-containt">
                <span class="close2"><img src="{{url('public/Assets/frontend/images/pinkclose.png')}}" alt=""  onclick="sponserUpgradePlanModalClose()" /></span>
            </div>
            <div class="container">
                <div class="main-dirservice-popup">
                    
                    <form class="horizontal-form" role="form" method="POST" name="userplanform" id="userplanform" action="{{url('job-post/sponser-plan-data')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    
                    <input type="hidden" name="i_job_id" id="i_job_id_sponser">
                    <div class="popup-dirservice">
                        <div class="title-dirservice">
                            <h4> Upgrade Your Plan </h4>
                        </div>
                        <div class="foll-option-popup">
                            You're currently a {{isset($userplandata['v_plan_name']) ? $userplandata['v_plan_name']:'' }} member. Please upgrade your Plan.
                        </div>
                        
                        <div class="wrap">
                            <fieldset>
                                <div class="toggle">
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="monthly" id="sponserplanmonth" checked onchange="plandurationchange('monthly')" >
                                    <label for="sponserplanmonth" class="toggle-label toggle-label-off" id="sponserjoinLabel">Monthly</label>
                                    <input type="radio" class="toggle-input" name="v_plan[duration]" value="yearly" id="sponserplanyear" onchange="plandurationchange('yearly')">
                                    <label for="sponserplanyear" class="toggle-label toggle-label-on" id="sponsercreateLabel">Yearly <sup class="year-noti">{{$discount}}% off</sup></label>
                                    <span class="toggle-selection"></span>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    @if(count($plandata))
                        @foreach($plandata as $key=>$val)
                        
                            @if($key!=0)
                                <div class="final-leval-popup">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-12">
                                            <div class="@if($key==0)left-choise @else left-side @endif">
                                                <div class="reg">
                                                    <bdo dir="ltr">
                                                        <input type="radio" class="one" value="{{$val['id']}}" name="v_plan[id]" required >
                                                        <span class="select-seller-job"></span>
                                                        <abbr class="choise-name">{{$val['v_name']}}</abbr>
                                                    </bdo>
                                                </div>
                                                <div class="choise-que">{{$val['v_subtitle']}}</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-xs-12">
                                            
                                            <ul class="list-unstyled choise-avalible">
                                                <?php 
                                                        $lcnt=1;
                                                    ?>
                                                    @if(count($val['l_bullet']))
                                                        @foreach($val['l_bullet'] as $k=>$v) 
                                                            @if($k<3)
                                                            <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                            <?php $lcnt = $lcnt+1; ?>
                                                            @endif
                                                        @endforeach
                                                    @endif  
                                            </ul>

                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <div class="right-display">
                                                <ul class="list-unstyled choise-avalible">
                                                    <?php 
                                                            $lcnt=1;
                                                        ?>
                                                        @if(count($val['l_bullet']))
                                                            @foreach($val['l_bullet'] as $k=>$v) 
                                                                @if($k>=3)
                                                                <li class="right-mark"><img src="{{url('public/Assets/frontend/images/tick.png')}}" class="tick" alt="" />{{$v}}</li>
                                                                @endif
                                                                <?php $lcnt = $lcnt+1; ?>
                                                            @endforeach
                                                        @endif  

                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12 same-height-1">
                                            <div class="@if($key==0) free-price @else final-price @endif">
                                                <div class="all-in-data">
                                                    <div  class="monthyprice">
                                                           @if($val['f_monthly_price']==0 || $val['f_monthly_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_monthly_price']}}</del>
                                                                </div>
                                                                offer £{{$val['f_monthly_dis_price']}} per month

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif

                                                           @endif  
                                                    </div>  
                                                    
                                                    <div class="yealryprice" style="display: none;">
                                                           @if($val['f_yealry_price']==0 || $val['f_yealry_price']=="")
                                                                <div class="final-price_text">
                                                                FREE
                                                                </div>
                                                           @else 
                                                                <div class="final-price_text">
                                                                <del>£{{$val['f_yealry_price']}}</del>
                                                                </div>
                                                                offer £{{$val['f_yealry_dis_price']}} per yearly

                                                                @if($val['id']=="5a65b757d3e8125e323c986a")
                                                                @if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null)
                                                                <br>First {{$standardplanFree}} @if($standardplanFree!="3") month @else months @endif absolutely FREE
                                                                @endif
                                                                @endif

                                                                
                                                           @endif  
                                                    </div>    

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif        

                    <button type="submit" class="btn btn-payment">
                        Proceed
                    </button>
                    </form>
                </div>
                <!-- End 26B-my-profile-with-sponsor-button-POP UP-in-person-or-online -->
            </div>
        </div>
    </div>



    <div class="modal fade" id="sponsorNow" role="dialog">
        <div class="modal-dialog modal-dialog-popup" style="max-width:35% !important">
            <!-- Modal content-->
            <div class="modal-content modal-content-popup">
                <div class="modal-header modal-header-bottom">
                </div>
                <div class="modal-body">
                    <div class="popup-sponsore">
                        <div class="title-control-postion">
                            <div class="title-popup-table">
                                <h3>Are you sure you want to sponsor this job posting? </h3>
                            </div>
                            <input type="hidden" name="sjid" id="sjid">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel & Close</button> &nbsp;&nbsp;&nbsp;&nbsp;                                         
                                    <button type="button" class="btn btn-success ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" id="SponsorNowJob" onclick="SponsorNow()">Sponsor Now</button>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-top">
                </div>
            </div>
        </div>
    </div>

   
    <!-- Modal -->
    <div class="modal fade" id="addNewSponsoredAds" role="dialog">
        <div class="modal-dialog modal-dialog-popup">
            <!-- Modal content-->
            <div class="modal-content modal-content-popup">
                <div class="modal-header modal-header-bottom">
                </div>
                <div class="modal-body">
                    <div class="popup-sponsore">
                        <div class="title-control-postion">
                            <div class="title-popup-table">
                                <h3> Add New Sponsored Ads </h3>
                            </div>
                            <div class="contant-table">
                                <table class="table table-hover table-sponsoreads">
                                    <thead>
                                        <tr>
                                            <th>JOB TITLE</th>
                                            <th>POSTING DATE</th>
                                            <th>EXPIRY DATE</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody id="newsponsoradsdata">
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-top">
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

    <!-- 26B-sponsored-ads -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('buyer/dashboard')}}"><button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button></a>
                </div>
                <div class="title-support">
                    <h1> Sponsored Ads </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="content">
            <div class="tab-pane active" id="completed">
                <div class="order-status">

                    <div id="sponsermsg"></div>

                    @if(Session::get('success'))
                        <div class="alert alert-info alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span>{{ Session::get('success') }}</span>
                        </div>
                    @endif

                    <div class="alert alert-info alert-danger" id="sponsorerrmsg" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span>Something went wrong.please try again after some time.</span>
                    </div>

                    <div class="alert alert-success alert-big alert-dismissable br-5" id="commonsucc" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span id="commonsuccmsg">Thank you, your message is now sponsored.</span>
                    </div>

                    <div class="alert alert-info alert-danger" id="commonerr" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong><i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span id="commonerrmsg">Something went wrong.please try again after some time.</span>
                    </div>


                    
                    <div class="title-sponsoreads">
                        Sponsored Ads Stats
                        <button type="button" class="btn btn-new-sponsore" onclick="NewSponsoredAds()"> Add New Sponsored Ad
                        </button>
                        <div class="clearfix"></div>
                    </div>
                    @php
                        
                        $IMPRESSIONS=0;
                        $CONTACTS = 0;
                        $CLICKS=0;
                        if(count($joblist)){
                            foreach($joblist as $key=>$val){
                                if(isset($val->i_impression)){
                                    $IMPRESSIONS = $IMPRESSIONS+$val->i_impression;            
                                }

                                if(isset($val->i_contact)){
                                    $CONTACTS = $CONTACTS+$val->i_contact;            
                                }

                                if(isset($val->i_clicks)){
                                    $CLICKS = $CLICKS+$val->i_clicks;            
                                }
                            }   
                        }


                    @endphp

                    @if(count($joblist))
                        @foreach($joblist as $key=>$val)

                        @endforeach
                    @endif    


                    
                    <div class="order-details order-details2">
                       {{--  <div class="col-sm-3 no-padding">
                            <div class="right-boderstyle">
                                TOTAL SPONSORED ADS SALES
                                <p> £200</p>
                            </div>
                        </div> --}}
                        <div class="col-sm-3 no-padding">
                            <div class="right-boderstyle">
                                TOTAL ADS IMPRESSIONS
                                <p class="extra-pad"> {{$IMPRESSIONS}} </p>
                            </div>
                        </div>
                        <div class="col-sm-3 no-padding">
                            <div class="right-boderstyle">
                                TOTAL ADS CONTACTS
                                <p class="extra-pad"> {{$CONTACTS}} </p>
                            </div>
                        </div>
                        <div class="col-sm-3 no-padding">
                            <div class="right-boderstyle last-chlidhead">
                                TOTAL SPONSORED CLICKS
                                <p class="extra-pad"> {{$CLICKS}} </p>
                            </div>
                        </div>

                    </div>

                    <div class="contant-table">
                        <div class="table-responsive">
                            <table class="table table-hover table-sponsoreads s">
                                <thead>
                                    <tr>
                                        <th>JOB TITLE</th>
                                        <th>POSTING DATE</th>
                                        <th>EXPIRY DATE</th>
                                        <th>SPONSORED</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>

                                <tbody>
                                @if(count($joblist))
                                    @foreach($joblist as $key=>$val)
                                    <tr>
                                        <td><a href="javascript:;" onclick="ViewData('{{$val->i_impression or 0}}','{{$val->i_contact or 0}}','{{$val->i_clicks or 0}}')">{{isset($val->v_job_title) ? $val->v_job_title:''}} </a></td>

                                        <td>{{isset($val->d_added) ? date("d/m/Y",strtotime($val->d_added)):''}}</td>
                                        <td>{{isset($val->d_expiry_date) ? date("d/m/Y",strtotime($val->d_expiry_date)):''}}</td>
                                        <td>
                                            <div id="statusdivid{{$val->id}}">    
                                            @if(isset($val->e_sponsor_status) && $val->e_sponsor_status=="start")
                                                <button type="button" class="btn btn-pause" onclick="SponsoreStatusChange('{{$val->id}}','pause')" > Pause </button>
                                            @else
                                                <button type="button" class="btn btn-start" onclick="SponsoreStatusChange('{{$val->id}}','start')"> Start </button>
                                            @endif
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{url('job-post/edit')}}/{{$val->id}}">
                                            <button type="button" class="btn btn-jobpost">Edit Job Post</button>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr> 
                                        <td colspan="5">No Sponsored Ads Found.</td>
                                    </tr>
                                @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End 26B-sponsored-ads -->

        </div>
    </div> 
   


@stop
@section('js')

<script type="text/javascript">

        function ViewData(IMPRESSIONS=0,CONTACT=0,CLICKS=0){
            $("#IMPRESSIONSPOP").html(IMPRESSIONS);
            $("#CONTACTSPOP").html(CONTACT);                    
            $("#CLICKSPOP").html(CLICKS);
            $("#viewdatajobs").modal("show");
        } 
      function NewSponsoredAds(){
            
            var actionurl = "{{url('sponsor-ads/addsponsorads')}}";
            var formdata = "";
            document.getElementById('load').style.visibility="visible";
            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    document.getElementById('load').style.visibility='hidden';
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#newsponsoradsdata").html(obj['htmlstr']);
                        $("#addNewSponsoredAds").modal("show");
                    
                    }else if(obj['status']==2){
                        $("#upgradebutton").modal("show");
                    }else{
                        $("#sponsorerrmsg").show();
                        $("#sponsorerrmsg").html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                    $("#sponsorerrmsg").show();
                }
            });
      }

      function upgradeUserPlan(){
            $("#upgradebutton").modal("hide");
            $("#sponserUpgradePlanModal").modal("show");

      }

      function SponsorNowModal(jid=""){
            $("#addNewSponsoredAds").modal("hide");
            $("#sjid").val(jid);
            $("#sponsorNow").modal("show");
      }

      function SponsorNow(){
            
            var actionurl = "{{url('job-post/sponsor-now')}}";
            var job_id = $("#sjid").val();    
            var formdata = "job_id="+job_id;
            document.getElementById('load').style.visibility="visible";
            var l = Ladda.create(document.getElementById('SponsorNowJob'));
            l.start();  

            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    document.getElementById('load').style.visibility='hidden';
                    l.stop();
                    var obj = jQuery.parseJSON(res);
                    $("#sponsorNow").modal("hide");
                    if(obj['status']==1){
                        window.location = "{{url('sponsor-ads')}}";
                    }else{
                        $("#sponsorerrmsg").show();
                        $("#sponsorerrmsg").html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                    l.stop();
                    $("#sponsorerrmsg").show();
                }
            });

      }


      function SponsoreStatusChange(jid="",status=""){
            
            var actionurl = "{{url('sponsor-ads/sponsoradsstatus')}}";
            var formdata = "job_id="+jid+"&status="+status;
            document.getElementById('load').style.visibility="visible";

            $.ajax({
                type    : "GET",
                url     : actionurl,
                data    : formdata,
                success : function( res ){
                    document.getElementById('load').style.visibility='hidden';
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        
                        $("#sponsermsg").html(obj['msg']);
                        //$("#commonsucc").show();
                        //$("#commonsuccmsg").html(obj['msg']);
                        $("#statusdivid"+jid).html(obj['btnstr']);
                    }else{
                        
                        $("#sponsermsg").html(obj['msg']);
                        //$("#commonerr").show();
                        //$("#commonerrmsg").html(obj['msg']);
                    }

                },
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                    $("#commonerr").show();
                    $("#commonerrmsg").html(obj['msg']);
                }
            });

    }
    
    function sponserUpgradePlanModalClose(){
        $("#sponserUpgradePlanModal").modal("hide");
    }

</script>

@stop

