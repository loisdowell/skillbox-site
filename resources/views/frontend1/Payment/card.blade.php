@extends('layouts.frontend')
@section('content')
<style type="text/css">
   .modal-new .modal-backdrop {z-index: 0;}
   .form-save-exit{width: 250px !important;}
</style>

<div class="title-controlpanel">
    <div class="container">
        <div class="title-control-postion">
            <div class="title-support">
                <h1> Order Summery </h1>
            </div>
        </div>
    </div>
</div>


<form class="horizontal-form" role="form" method="POST"  action="{{$result->CardRegistrationURL}}" data-parsley-validate="" enctype="application/x-www-form-urlencoded" novalidate="">
{{ csrf_field() }}
<input type="hidden" name="data" value="{{$result->PreregistrationData}}" />
<input type="hidden" name="accessKeyRef" value="{{$result->AccessKey}}" />
<input type="hidden" name="returnURL" value="{{$returnurl}}" />
<input type="hidden" name="cardExpirationDate" id="cardExpirationDate" value="" />

<div class="container">
  <div class="mangopay_pymt_card">
      <p class="mangopay_pymt_card_top_text">You are on a page with approved bank security. All your personal information is encrypted and secured with mangopay.</p>
      <div class="mangopay_pymt_card_form">
        <h1>Amount to pay : {{number_format($data['v_total_amount'],2)}} GBP</h1>
        <div class="mangopay_card_form">

            <div class="form-group card_nm">
              <label for="number">Card Number</label>
              <input type="number" class="form-control" name="cardNumber" onkeydown="javascript: return event.keyCode == 69 ? false : true" data-parsley-maxlength="19" required="required" data-parsley-trigger="keyup change focusin focusout">
            </div>
            <div class="form-group">
             <label for=""> Expiration date </label>
              <select autocomplete="off" name="cardExpirationDateMonth" id="cardExpirationDateMonth" required onchange="MonthChange(this.value)">
                <option value="">Month</option>
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
              @php
                  $year = date("Y",strtotime("-1 year"));  
                  $yearval = date("y",strtotime("-1 year"));  
              @endphp  
              <select autocomplete="off" name="cardExpirationDateYear" id="cardExpirationDateYear" required onchange="YearChange(this.value)">
                <option value="">Year</option>  
                @php
                  for($i=1;$i<10;$i++){  
                    $year = $year+1;  
                    $yearval = $yearval+1;  
                @endphp
                    <option value="{{$yearval}}">{{$year}}</option>
                @php
                  }
                @endphp
              </select>
            </div>
            <div class="form-group card_cv">
              <label>Card Security code </label>
              <input required="required" type="number" data-parsley-type="number"  data-parsley-trigger="keyup change focusin focusout" onkeydown="javascript: return event.keyCode == 69 ? false : true"  class="form-control"  name="cardCvx"> <img src="<?php echo url('public/Assets/frontend/images/help.png');?>">
            </div>
      </div>
    </div>
    <button type="submit">Validate</button>
  </div>
</div>
</form>

<div class="card_footer_section">
  <div class="card_footer_section_inr">
    <div class="pull-left">
      <p class="lock_icon">Payment secured by MANGOPAY</p>
    </div>
    <div class="pull-right">
      <ul>
        <li><img src="<?php echo url('public/Assets/frontend/images/ssl-logo.png');?>"></li>
        <li><img src="<?php echo url('public/Assets/frontend/images/powered-by-mangopay.png');?>"></li>
      </ul>
    </div>
  </div>
</div>

@stop
@section('js')
<script type="text/javascript">
    function MonthChange(data) {
        var month = $("#cardExpirationDateMonth").val();
        var year =  $("#cardExpirationDateYear").val();
        $("#cardExpirationDate").val(month+year);
    }

    function YearChange(data) {
        var month = $("#cardExpirationDateMonth").val();
        var year =  $("#cardExpirationDateYear").val();
        $("#cardExpirationDate").val(month+year);
    }

</script>
@stop