@extends('layouts.frontend')
@section('content')
<style type="text/css">
   .modal-new .modal-backdrop {z-index: 0;}
   .form-save-exit{width: 250px !important;}
</style>

<div class="title-controlpanel">
    <div class="container">
        <div class="title-control-postion">
            <div class="title-support">
                <h1> Payment Option </h1>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
  .radio-choise .reg bdo span{top: 7px;}
  .mangopay_pymt_card button{margin-bottom: 20px;float: none;margin-left: auto;margin-right: auto;min-width: 1px;max-width: 230px;width: 100%;display: block;}
</style>

<form class="horizontal-form" role="form" method="POST"  action="{{url('payment/mangopay-option')}}" data-parsley-validate="" enctype="application/x-www-form-urlencoded" novalidate="">
{{ csrf_field() }}


<div class="container">
  <div class="mangopay_pymt_card">
        
      <div class="mangopay_pymt_card_form">
        <h1 style="text-align: center;">Select Your Payment Option</h1>
        
        <div class="radio-choise text-center">
          <div class="reg">
          <label style="font-weight: normal;">
              <bdo dir="ltr">
                 <input type="radio" checked class="one" value="exist" name="v_card_option" >
                 <span></span>
                 <abbr> Use my card Details on file with MangoPay </abbr>
              </bdo>
           </label>   
           
          </div>
      </div>

      <div class="radio-choise text-center">
          <div class="reg">
          <label style="font-weight: normal;">
              <bdo dir="ltr">
                 <input type="radio" class="one" value="new" name="v_card_option">
                 <span></span>
                 <abbr>I would like to use new card details</abbr>
              </bdo>
            </label>
           </div>
      </div>

      <div class="clearfix"></div>
      <button type="submit">Proceed</button>
      <div class="clearfix"></div>

    </div>
  </div>
</div>
</form>

@stop
@section('js')
<script type="text/javascript">
    function MonthChange(data) {
        var month = $("#cardExpirationDateMonth").val();
        var year =  $("#cardExpirationDateYear").val();
        $("#cardExpirationDate").val(month+year);
    }

    function YearChange(data) {
        var month = $("#cardExpirationDateMonth").val();
        var year =  $("#cardExpirationDateYear").val();
        $("#cardExpirationDate").val(month+year);
    }

</script>
@stop