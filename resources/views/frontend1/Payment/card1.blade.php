@extends('layouts.frontend')
@section('content')
<style type="text/css">
   .modal-new .modal-backdrop {z-index: 0;}
   .form-save-exit{width: 250px !important;}
</style>

<div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="title-support">
                    <h1> Order Summery </h1>
                </div>
            </div>
        </div>
    </div>
<div class="container">
   <div class="main-edit-profile">
      <div class="edit-details" style="margin-top: 50px;">
         <div class="row">
               
               <form class="horizontal-form" role="form" method="POST"  action="{{$result->CardRegistrationURL}}" data-parsley-validate="" enctype="application/x-www-form-urlencoded" novalidate="">
                {{ csrf_field() }}
                <input type="hidden" name="data" value="{{$result->PreregistrationData}}" />
                <input type="hidden" name="accessKeyRef" value="{{$result->AccessKey}}" />
                <input type="hidden" name="returnURL" value="{{$returnurl}}" />

                <label for="cardNumber">Card Number</label>
                <input type="text" name="cardNumber" value="" />
                <div class="clear"></div>
                <label for="cardExpirationDate">Expiration Date</label>
                <input type="text" name="cardExpirationDate" value="" />
                <div class="clear"></div>

                <label for="cardCvx">CVV</label>
                <input type="text" name="cardCvx" value="" />
                <div class="clear"></div>

                <div class="col-xs-12">
                  <div class="upload-details" style="margin: 0px auto;">
                    <div style="margin: 15px 0px; text-align: center;">
                         <button type="submit" class="btn form-save-exit"> Pay </button>
                    </div>
                  </div>
               </div>

            </form>
         </div>
      </div>
      
   </div>
</div>
@stop
@section('js')
<script type="text/javascript">

</script>
@stop