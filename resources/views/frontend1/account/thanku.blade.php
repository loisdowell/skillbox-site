@extends('layouts.frontend')

@section('content')

  <!-- 02B-sign-up-3-buyer -->
    <div class="container">
        <div class="title-welcome">
            <h1>Thank you for select plan</h1>
        </div>
        <div style="clear: both"></div>

        @if ($success = Session::get('success'))
          <div class="alert alert-success alert-big alert-dismissable br-5">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;success</strong>&nbsp;&nbsp;{{ $success }}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-info alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;{{ $warning }}
          </div>
        @endif
        
    </div>
  <!-- End 02B-sign-up-3-buyer -->


@stop
@section('js')

<script type="text/javascript">
</script>

@stop

