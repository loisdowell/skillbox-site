@extends('layouts.frontend')

@section('content')
@php
    $disabled =""
@endphp

@if(isset($userdata->i_bank_id) && $userdata->i_bank_id!="")
    @php
        $disabled ="disabled";
    @endphp
@endif

@php
     $countrylist = App\Helpers\General::countrysortcode();    
@endphp


 <!-- 26B-control-panel-edit-billing-details-2-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1>Edit Refunds / Payments </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">


            @if ($success = Session::get('success'))
              <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 20px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
              </div>
            @endif
            
            @if ($warning = Session::get('warning'))
              <div class="alert alert-info alert-danger" style="margin-top: 20px">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
              </div>
            @endif

                    
            <div class="col-xs-12">
                <div class="main-support">

                <p>To receive refunds from sellers or payments from buyers please provide your bank details below. We don't save your bank details at Skillbox. Your details get pushed to MangoPay secure payment system when you click "save changes".</p>

                <p>Without KYC verification you will not be able to withdraw funds or refunds. Please click 'Identification Documents' to upload necessary documents. This is a one-time process.</p>    

                <center>
                    <a href="{{url('profile/identification-documents')}}">
                    <button type="button" class="btn form-next" style="width: 225px;">Identification Documents</button>
                    </a>
                </center>

                <form class="horizontal-form" role="form" method="POST" name="editbillingform" id="editbillingform" action="{{url('profile/billing-update')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}    

                    @if(isset($_REQUEST['wallet']) && $_REQUEST['wallet']!="")
                        <input type="hidden" name="wallet" value="{{$_REQUEST['wallet']}}">
                    @endif
                    <p>&nbsp;</p>
                    {{-- <p> We don't save your billing details at Skillbox. Your details get pushed to MangoPay secure payment system when you click "save changes".</p> --}}

                    <div class="billing-detail">
                        <div class="row">
                            
                        </div>
                        
                        <div id="bank_detail_div" >
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="field-support">
                                        Bank Details
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row-field-support">
                                <div class="row">
                                    
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Enter Account Holder name:
                                            </div>
                                            <input type="text" class="form-control input-field" value="{{isset($userdata->billing_detail['v_OwnerName']) ? $userdata->billing_detail['v_OwnerName']:''}}" name="billing_detail[v_OwnerName]" id="v_OwnerName" required {{$disabled}}>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Enter Account No:
                                            </div>
                                            <input type="number" min="0" step="any" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" value="{{isset($userdata->billing_detail['v_AccountNumber']) ? $userdata->billing_detail['v_AccountNumber']:''}}" name="billing_detail[v_AccountNumber]" id="v_AccountNumber" required {{$disabled}} data-parsley-trigger="keyup focusin focusout">
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Enter Sort code:
                                            </div>
                                            <input type="number" min="0" step="any" onkeydown="javascript: return event.keyCode == 69 ? false : true" class="form-control input-field" value="{{isset($userdata->billing_detail['v_SortCode']) ? $userdata->billing_detail['v_SortCode']:''}}" name="billing_detail[v_SortCode]" id="v_SortCode" required {{$disabled}} data-parsley-trigger="keyup focusin focusout">
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Address Line 1
                                            </div>
                                            <input type="text" class="form-control input-field" value="{{isset($userdata->billing_detail['v_AddressLine1']) ? $userdata->billing_detail['v_AddressLine1']:''}}" name="billing_detail[v_AddressLine1]" id="v_AddressLine1" required {{$disabled}}>
                                        </div>
                                    </div>
                                    <div style="clear: both"></div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Address Line 2
                                            </div>
                                            <input type="text" class="form-control input-field" value="{{isset($userdata->billing_detail['v_AddressLine2']) ? $userdata->billing_detail['v_AddressLine2']:''}}" name="billing_detail[v_AddressLine2]" id="AddressLine2" {{$disabled}}>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                City
                                            </div>
                                            <input type="text" class="form-control input-field" value="{{isset($userdata->billing_detail['v_City']) ? $userdata->billing_detail['v_City']:''}}" name="billing_detail[v_City]" id="v_City" required {{$disabled}}>
                                        </div>
                                    </div>

                                    <input type="hidden" value="GB" name="billing_detail[v_Country]">
                                   
                                     <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Country 
                                            </div>
                                           
                                           {{--  <select class="form-control form-sell" name="billing_detail[v_Country]" required {{$disabled}}>
                                                    <option value="">-select-</option>    
                                            @if(isset($countrylist) && count($countrylist))
                                                @foreach($countrylist as $key=>$val)
                                                    <option value="{{$key}}">{{$val}}</option>
                                                @endforeach
                                            @endif    
                                            </select> --}}

                                            <input type="text" class="form-control input-field" value="United Kingdom (Great Britain)" id="v_Country" {{$disabled}}>

                                        </div>
                                    </div>

                                     <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Postcode
                                            </div>
                                            <input type="text" class="form-control input-field" value="{{isset($userdata->billing_detail['v_PostalCode']) ? $userdata->billing_detail['v_PostalCode']:''}}" name="billing_detail[v_PostalCode]" id="v_PostalCode" required {{$disabled}}>
                                        </div>
                                    </div>

                                    {{--  <div class="col-sm-6 col-xs-12">
                                        <div class="your-field">
                                            <div class="payment-method">
                                                Region
                                            </div>
                                            <input type="text" class="form-control input-field" value="{{isset($userdata->billing_detail['v_Region']) ? $userdata->billing_detail['v_Region']:''}}" name="billing_detail[v_Region]" id="v_Region" required {{$disabled}}>
                                        </div>
                                    </div> --}}


                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="top-btnspace">
                        {{-- <a href="{{url('dashboard')}}">
                        <button type="button" class="btn form-save-exit"> Discard </button>
                        </a> --}}

                        <button type="submit" class="btn form-next" id="sub" @if(isset($userdata->i_bank_id) && $userdata->i_bank_id!="") style="display: none;"  @endif >Save Changes</button>

                        @if(isset($userdata->i_bank_id) && $userdata->i_bank_id!="")
                        <button type="button" onclick="Editdata()" id="editch" class="btn form-next">Edit Details</button>
                        @endif

                    </div>

                 </form>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-edit-billing-details-2 -->

@stop

@section('js')
     <script type="text/javascript">
        
     function Editdata(){
        $("input").removeAttr("disabled");
        $("select").removeAttr("disabled");
        $("#sub").css("display","inline");
        $("#editch").css("display","none");
    }

        function billingdetail(data) {
            
            if(data=="bank"){
                $("#paypal_detail_div").css("display","none");    
                $("#bank_detail_div").css("display","block");  
                $("#paypalemail").removeAttr("required");

                $("#v_bankname").attr("required","true");
                $("#v_accountno").attr("required","true");
                $("#v_ifsc").attr("required","true");
                $("#v_account_holder_name").attr("required","true");
          
            }else{
                $("#paypal_detail_div").css("display","block");   
                $("#bank_detail_div").css("display","none");     

                $("#v_bankname").removeAttr("required");
                $("#v_accountno").removeAttr("required");
                $("#v_ifsc").removeAttr("required");
                $("#v_account_holder_name").removeAttr("required");
                $("#paypalemail").attr("required","true");


            }

        }




    </script>
@stop

