@extends('layouts.frontend')

@section('content')
  <style type="text/css">
    .usr_icon select {
        padding-left: 44px;
        font-size: 16px;
    }

  </style>
  <!-- 02B-sign-up-3-buyer -->
    <div class="container">
        <div class="title-welcome">
            <h1>Welcome</h1>
        </div>
        <div style="clear: both"></div>

        @if ($success = Session::get('success'))
          <div class="alert alert-success alert-big alert-dismissable br-5">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-info alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
          </div>
        @endif

        <div class="alert alert-danger" role="alert" style="display:none" id="someerror">
            Something went wrong,Please try again after some time.!
        </div>
        @php
        $path = Route::getFacadeRoot()->current()->uri();
        @endphp

        <div class="row">
            <div class="col-xs-12">
                <div class="Welcome-page-all">
                    <div class="Welcome-page">
                      <form class="horizontal-form" role="form" method="POST" name="signupform" id="signupform" action="{{url('account/signup')}}" data-parsley-validate enctype="multipart/form-data" >

                        {{ csrf_field() }}
                        
                        @if(isset($v_upper_service) && $v_upper_service=="no")
                          <input type="hidden" name="v_service" value="{{$v_service}}">
                        @else
                        <div class="service">
                            <div class="service-interest">
                                @if(isset($usertype) && $usertype=="seller") 
                                <!-- What would you like to sell? Choose most appropriate -->
                                What would you like to sell?<br> Choose most appropriate
                                @else
                                {{-- Which service is right for you? Choose most appropriate --}}
                                <!-- Which service would you like to buy? Choose most appropriate. -->
                                @if($path=="account/buyer")
                                  What would you like to buy?
                                @else
                                  Post a job for free! 
                                @endif  
                                <br>Choose most appropriate.
                                @endif
                            </div>
                            
                            <ul class="list-unstyled">
                                <li>
                                    <div class="reg reg_fill">
                                        <bdo dir="ltr">
                                           <input type="radio" value="online" onfocus="javascript:checkNullProf()" name="v_service" checked>
                                           <span></span>
                                           <abbr> Online Services </abbr>
                                           </bdo>
                                      </div>
                                </li>
                                <li>
                                    <div class="reg reg_fill">
                                        <bdo dir="ltr">
                                           <input type="radio" value="inperson" onfocus="javascript:checkNullProf()" name="v_service">
                                           <span></span>
                                           <abbr> In Person Services </abbr>
                                        </bdo>
                                    </div>
                                </li>
                               @if(isset($usertype) && $usertype!="jobpost") 
                                <li>
                                    <div class="reg reg_fill">
                                        <bdo dir="ltr">
                                           <input type="radio" value="courses" onfocus="javascript:checkNullProf()" name="v_service">
                                           <span></span>
                                           <abbr> Courses </abbr>
                                        </bdo>
                                    </div>
                                </li>
                               @endif 
                            </ul>
                        </div>
                        @endif
                        
                        <input type="hidden" name="e_type" value="{{$usertype}}">
                        
                        <div class="Welcome-box">
                          <style type="text/css">
                            .add_fr_ls .form-control{border: 2px solid #cccccc;height: 35px;border-radius: 50px;margin-bottom: 15px;}
                            /*.Welcome-page{max-width: 400px;}*/
                            input.singup-checkup[type="checkbox"] + span.freemail-newsletter{padding-top: 2px;padding-bottom: 2px;}
                            .add_fr_ls .row{margin-left:-15px;margin-right:-15px; }
                            .add_fr_ls .row .col-sm-6{padding-left:15px;padding-right:15px;}
                          </style>
                          <div class="add_fr_ls">
                            <div class="row">
                              <div class="col-sm-12 col-xs-12">
                                <div class="usr_icon">
                                <input class="form-control" type="text" name="v_fname" placeholder="First name" required>
                                </div>
                              </div>
                              <div class="col-sm-12 col-xs-12">
                                <div class="usr_icon">
                                <input class="form-control" type="text" name="v_lname" placeholder="Last name" required>
                                </div>
                              </div>

                              @if(isset($usertype) && $usertype=="seller") 
                               <div class="col-sm-12 col-xs-12">
                                <div class="usr_icon">
                                 
                                      <select class="form-control form-sel" required name="v_company_type">
                                        <option value="">Seller Type</option>
                                        <option value="INDIVIDUAL">Individual</option>
                                        <option value="SOLETRADER">Sole Trader</option>
                                        <option value="BUSINESS">Business</option>
                                        <option value="ORGANIZATION">Organisation</option>
                                      </select>
                                  
                                </div>
                              </div>
                              @else
                              <input type="hidden" name="v_company_type" value="INDIVIDUAL"> 
                              @endif
                            </div>
                          </div>
                           

                            <input type="email" name="v_email" class="form-control form-mail1 fontcls" placeholder="Email Address" required>
                            <div class="msg">
                                <span class="password-msg">
                                  Your password is not long enough. <br> Please enter at least 9 characters.
                                </span>
                            </div>
                            
                            <input type="password" name="password" id="password" class="form-control form-right-sign fontcls" data-parsley-minlength="8" data-parsley-pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" data-parsley-pattern-message="Your password must contain at least a numeric, special character and uppercase letter." placeholder="Password" required>
                            <input type="password" name="repassword" class="form-control form-pass fontcls" placeholder="Retype Password" data-parsley-equalto="#password" required>
                            
                            <label class="checkbox wc-checkbox-space">
                            <input type="checkbox" class="singup-checkup" name="i_newsletter">
                                <span class="freemail-newsletter"> 
                                    I want to subscribe to the FREE email newsletter 
                                  </span>
                            </label>

                            <button onclick="buyerSignup()" type="button" id="bsignup" class="btn signup-wc ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" >Get Started</button>
                            <p class="already-account"> Already have an account?<a href="{{url('login')}}" class="welcome-link"> Log in</a></p>
                            
                            <div class="term-policy">
                                By clicking Get Started, I agree to the
                                <a target="_blank" href="{{url('pages/terms-of-service')}}" class="welcome-link"> Terms of Services  </a> and
                                <a target="_blank" href="{{url('pages/privacy-policy')}}" class="welcome-link">Privacy Policy </a>
                            </div>

                        </div>

                      </form>    
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
  <!-- End 02B-sign-up-3-buyer -->


@stop
@section('js')

<script type="text/javascript">

      function buyerSignup(){

          var formValidFalg = $("#signupform").parsley().validate('');
          if(formValidFalg){
              var l = Ladda.create(document.getElementById('bsignup'));
              l.start();
              $("#signupform").submit()
          }
      }

</script>

@stop

