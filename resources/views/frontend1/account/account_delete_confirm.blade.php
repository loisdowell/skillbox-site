@extends('layouts.frontend')

@section('content')



<div id="cndowngrade" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;">Confirmation</h4>
      </div>
      <div class="modal-body">
        <h3 style="text-align: center;">Are you sure you want to downgrade your account to basic? </h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <a href="{{url('account/downgrade-account')}}">
        <button type="button" class="btn btn-success">Yes</button>
        </a>
      </div>
    </div>
  </div>
</div>


<div id="cndeactivate" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;">Confirmation</h4>
      </div>
      <div class="modal-body">
        <h3 style="text-align: center;">Are you sure you want to cancel your Skillbox account?</h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <a href="{{url('account/delete-account')}}">
        <button type="button" class="btn btn-success">Yes</button>
        </a>
      </div>
    </div>
  </div>
</div>

 <!-- 26B-control-panel-edit-billing-details-2-->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                
                <div class="left-supportbtn">
                    <a href="{{url('dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    @if(isset($userplandata['v_plan_id']) && $userplandata['v_plan_id']=="5a65b48cd3e812a4253c9869")
                    <h1>Cancel Account</h1>
                    @else
                    <h1>Downgrade or Cancel Account</h1>    
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            @if(isset($userplandata['v_plan_id']) && $userplandata['v_plan_id']=="5a65b48cd3e812a4253c9869")           
            <div class="col-xs-12" style="text-align: center;">
                <br>
                <h3>Are you sure you want to cancel your Skillbox account?</h3>
                <br>    
                <h4>Cancelling your account will delete your complete account. All your buyer and seller history will be deleted from our system.</h4>
                <br>
                <a href="{{url('dashboard')}}"><button type="button" class="btn form-save-exit"> No</button></a>
                <a href="{{url('account/delete-account')}}"><button type="button" class="btn form-next"> Yes </button></a>
            </div>
            @else

            <div class="col-xs-12" style="text-align: center;">
                <br>
                <h3>Are you sure you want to Downgrade or Cancel your account?</h3>
                <br>    
                <div class="account_downgrade">
                <div class="row height_container">
                       
                       <div class="col-md-6 "> 
                            <p class="height_div">Your account will be downgraded to our Basic (Free) Plan and your monthly or annual subscription will be cancelled automatically. All the perks that come with our Standard and Premium account will be removed.</p> 
                            <a href="javascript:;" onclick="ConfirmDowngrade()">Downgrade My Account to Basic</a>                  
                       </div>

                       <div class="col-md-6">    
                            <p class="height_div">Cancelling your account will delete your complete account. All your buyer and seller history will be deleted from our system.    </p>
                            <a href="javascript:;" onclick="ConfirmDeactivate()">Cancel My Account Completely</a>             
                       </div>

                </div> 
                </div>

            </div>

            @endif



        </div>
    </div>
    <!-- End 26B-control-panel-edit-billing-details-2 -->

@stop

@section('js')
     <script type="text/javascript">
        function ConfirmDowngrade(){
            $("#cndowngrade").modal("show");    
        }
        function ConfirmDeactivate(){
            $("#cndeactivate").modal("show");    
        }
        
        function billingdetail(data) {
            
            if(data=="bank"){
                $("#paypal_detail_div").css("display","none");    
                $("#bank_detail_div").css("display","block");  
                $("#paypalemail").removeAttr("required");

                $("#v_bankname").attr("required","true");
                $("#v_accountno").attr("required","true");
                $("#v_ifsc").attr("required","true");
                $("#v_account_holder_name").attr("required","true");
          
            }else{
                $("#paypal_detail_div").css("display","block");   
                $("#bank_detail_div").css("display","none");     

                $("#v_bankname").removeAttr("required");
                $("#v_accountno").removeAttr("required");
                $("#v_ifsc").removeAttr("required");
                $("#v_account_holder_name").removeAttr("required");
                $("#paypalemail").attr("required","true");


            }

        }




    </script>
@stop

