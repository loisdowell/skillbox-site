<link rel="stylesheet" href="{{url('public/Assets/frontend/star-rating/css/star-rating.css')}}" type="text/css" media="all">
<script src="{{url('public/Assets/frontend/star-rating/js/star-rating.js')}}"></script> 


@php
    $imag="";
    if(isset($userdata->v_image) && $userdata->v_image!=""){
        $imag=\Storage::cloud()->url($userdata->v_image);    
    }else{
        $imag=\Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
    }

    $totalReview=0;
    $totalAvgReview=0;
    if(isset($post_data['from']) && $post_data['from']=="buyer"){
        if(count($sellerdata)){
            $totalReview = $sellerdata->i_total_review;
            $totalAvgReview = $sellerdata->i_total_avg_review;
        }
    }else{
        if(isset($userdata->i_job_total_review) && isset($userdata->i_job_total_avg_review)){
            $totalReview=$userdata->i_job_total_review;
            $totalAvgReview=$userdata->i_job_total_avg_review;
        }
    }
    
@endphp      
      
<div class="row">
 <div class="final-point">
    <div class="buyer-name">
        <img src="{{$imag}}" alt="">
    </div>
    <div class="buyer-tital">
        <h5 style="margin-left: 5px">{{ucfirst($userdata->v_fname)}} {{ucfirst($userdata->v_lname)}}</h5>
        <div class="rating-buyer rating-link rating_counts" style="margin-top: 6px !important;margin-bottom: 6px !important">
        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$totalAvgReview}}" readonly>
        <span class="star-no">&nbsp;{{number_format($totalAvgReview,1)}}&nbsp;({{$totalReview}})</span>
        </div>
        <br>
        @if(isset($userdata->v_city) && $userdata->v_city!="")
        <span style="margin-left: 5px">Location : {{$userdata->v_city}}</span>
        @endif
    </div>
</div>

<div id="popupcommonmsg"></div>
<div class="col-xs-12">
  <div class="text-box-containt">
        <label> Subject </label>
        <input type="text" class="form-control input-field" name="v_subject_title" required>
    </div>
    <div class="text-box-containt">
        <label> Enter Your Message </label>
        <textarea rows="3" name="l_message" class="form-control popup-letter" required></textarea>
    </div>
    <div class="stb-btn">
        <button  type="button" onclick="submitMessage()" class="btn btn-Submit-pop">Send Message</button>
    </div>
</div>    
<div class="col-xs-12">    
</div>
</div>


   

<script type="text/javascript">
var src1 = "{{url('public/Assets/frontend/images/star.png')}}";
var src2 = "{{url('public/Assets/frontend/images/star1.png')}}";
$("#star-input-1,#star-input-2,#star-input-3,#star-input-4,#star-input-5").rating({
    min: 0,
    max: 5,
    step: 0.5,
    showClear: false,
    showCaption: false,
    theme: 'krajee-fa',
    filledStar: '<i class="fa"><img src="'+src1+'"></i>',
    emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
});

$("#f_rate_instruction,#f_rate_navigate,#f_rate_reccommend").rating({
    min: 0,
    max: 5,
    step: 0.5,
    showClear: false,
    showCaption: false,
    theme: 'krajee-fa',
    filledStar: '<i class="fa"><img src="'+src1+'"></i>',
    emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
});
</script>
