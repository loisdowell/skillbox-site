@extends('layouts.frontend')

@section('content')

  <!-- 02B-sign-up-seller -->
    <div class="container">
        <div class="title-welcome">
            <h1>Welcome</h1>
        </div>
        <div style="clear: both"></div>

        @if ($success = Session::get('success'))
          <div class="alert alert-success alert-big alert-dismissable br-5">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;success</strong>&nbsp;&nbsp;{{ $success }}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-info alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;warning</strong>&nbsp;&nbsp;{{ $warning }}
          </div>
        @endif

        <div class="alert alert-danger" role="alert" style="display:none" id="someerror">
            Something went wrong,Please try again after some time.!
        </div>
        
        <div class="row">
            <div class="col-xs-12">
                <div class="Welcome-page-all">
                    <div class="Welcome-page">
                      
                      <form class="horizontal-form" role="form" method="POST" name="sellersignupform" id="sellersignupform" action="{{url('seller-signup/signup')}}" data-parsley-validate enctype="multipart/form-data" >

                        {{ csrf_field() }}
                        <div class="service">
                            <div class="service-interest">
                                What would you like to buy? Choose most appropriate
                            </div>
                            <ul class="list-unstyled">
                                <li>
                                
                                    <div class="reg">
                                        <label style="font-weight: normal;">
                                        <bdo dir="ltr">
                                           <input type="radio" value="Online" onfocus="javascript:checkNullProf()" name="v_service" checked>
                                           <span></span>
                                           <abbr> Online Services </abbr>
                                           </bdo>
                                        </label>
                                      </div>
                                      
                                </li>
                                <li>
                                
                                    <div class="reg">
                                        <label style="font-weight: normal;">
                                        <bdo dir="ltr">
                                           <input type="radio" value="inperson" onfocus="javascript:checkNullProf()" name="v_service">
                                           <span></span>
                                           <abbr> In Person Services </abbr>
                                        </bdo>
                                        </label>
                                    </div>
                                
                                </li>
                                <li>
                                
                                    <div class="reg">
                                        <label style="font-weight: normal;">
                                        <bdo dir="ltr">
                                           <input type="radio" value="courses" onfocus="javascript:checkNullProf()" name="v_service">
                                           <span></span>
                                           <abbr> Courses </abbr>
                                        </bdo>
                                        </label>
                                    </div>
                                
                                </li>
                            </ul>
                        </div>
                        
                        <div class="Welcome-box">
                            
                            <input type="email" name="v_email" class="form-control form-mail1" placeholder="Email Address" required>

                            <div class="msg">
                                <span class="password-msg">
                                  Your password is not long enough. <br> Please enter at least 9 characters.
                                </span>
                            </div>
                            
                            <input type="password" name="password" id="password" class="form-control form-right-sign" data-parsley-minlength="8" data-parsley-pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" data-parsley-pattern-message="Your password must contain at least a numeric, special character and uppercase letter." placeholder="Password" required>
                            <input type="password" name="repassword" class="form-control form-pass" placeholder="Retype Password" data-parsley-equalto="#password" required>
                            
                            <label class="checkbox wc-checkbox-space">
                            <input type="checkbox" class="singup-checkup" name="i_newsletter">
                                <span class="freemail-newsletter"> 
                                    I want to subscribe to the FREE email newsletter 
                                  </span>
                            </label>

                            <button onclick="buyerSignup()" type="button" id="bsignup" class="btn signup-wc ladda-button" data-style="zoom-in" data-spinner-color="#e70e8a" >Get Started</button>

                            <p class="already-account"> Already have an account?<a href="{{url('login')}}" class="welcome-link"> Log in</a></p>

                            <div class="term-policy">
                                By clicking Get Started, I agree to the
                                <a href="08B-terms-of-service-3.html" class="welcome-link"> Terms of Services  </a> and
                                <a href="08B-privacy-policy-5.html" class="welcome-link">Privacy policy </a>
                            </div>
                        </div>

                      </form>    
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End 02B-sign-up-seller3 -->


@stop
@section('js')
<script type="text/javascript">

      function buyerSignup(){

          var formValidFalg = $("#sellersignupform").parsley().validate('');
          var l = Ladda.create(document.getElementById('bsignup'));
          l.start();  

          if(formValidFalg){
              $("#sellersignupform").submit()
          }else{
              l.stop();              
          }

      }

</script>

@stop

