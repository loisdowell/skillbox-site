     <div class="mainorder" id="mainordermessage">      
      @if(isset($buyermessage) && count($buyermessage))
        @foreach($buyermessage as $k=>$v)
           

            @php
                $cnt = $k++;
                $v_fromimage="";
                if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_image)){
                    $v_fromimage=$v->hasFromUser()->v_image;
                }
               
                if($v_fromimage!=""){
                    $v_fromimage = \Storage::cloud()->url($v_fromimage);
                }else{
                    $v_fromimage = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                }
                
                $v_fromfname="";
                if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_fname)){
                    $v_fromfname=$v->hasFromUser()->v_fname;
                }
                $v_fromlname="";
                if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_lname)){
                    $v_fromlname=$v->hasFromUser()->v_lname;
                }

            @endphp

            <div class="media media_msg p-3 dashed_border message-first ">
                <h2 style="display: none;">{{strtotime($v->d_added)}}</h2>
                <div class="media-left">
                  <img src="{{$v_fromimage}}" alt="image">
                </div>
                <div class="media-body">
                  <a href="{{url('message/seller/message-centre')}}?mid={{$v->id}}"> 
                  <h4 class="media-heading">{{$v->v_subject_title}}</h4>
                  </a>
                  <p><span>From: </span>{{$v_fromfname}} {{$v_fromlname}}<span class="time-buyer">{{date("h:i a",strtotime($v->d_added))}} (<?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($v->d_added))->diffForHumans(); ?>)</span></p>
                  <div class="buyer-tital-text">
                        <p>
                            @if(isset($v->l_message))
                                @if(strlen($v->l_message)>150)
                                    {{substr($v->l_message,0,150)}}...
                                @else
                                    {{$v->l_message}}
                                @endif
                            @endif

                        </p>
                </div>
                </div>
            </div>

            <!-- <div class="buyer-first message-first">
                <h2 style="display: none;">{{strtotime($v->d_added)}}</h2>
                <div class="final-point">
                    <div class="buyer-name">
                        <img src="{{$v_fromimage}}" alt="">
                    </div>
                    <div class="buyer-tital">
                        <a href="{{url('message/seller/chatHistory')}}"> 
                        <h5>{{$v->v_subject_title}}</h5>
                        </a>
                        <p><span>From: </span>{{$v_fromfname}} {{$v_fromlname}}<span class="time-buyer">{{date("h:i a",strtotime($v->d_added))}} (<?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($v->d_added))->diffForHumans(); ?>)</span></p>

                        <div class="buyer-tital-text">
                            <p>{{$v->l_message}}</p>
                        </div>

                    </div>
                </div>
               
            </div> -->

           @endforeach
           
           @else

            <div class="buyer-first message-first">
                <h2 style="display: none;">1</h2>
                <div class="buyer-rating">
                    <center><h3>No message found.</h3></center>
                </div>
            </div>
          
        @endif  
        </div> 
