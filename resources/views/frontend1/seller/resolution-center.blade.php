@extends('layouts.frontend')

@section('content')

<!-- Modal -->
<div class="modal  fade" id="CancelOrderModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content modal-content-1">
            <div class="modal-header modal-header-1">
                <h4 class="modal-title"> Cancel Order </h4>
            </div>
            <div class="modal-body">
                <div class="modal-body-center">
                    <p>Are you sure you want to cancel this order?</p>
                    <form class="horizontal-form" role="form" method="POST" name="cancelOrderForm" id="cancelOrderForm" action="{{url('seller/cancel-order')}}" data-parsley-validate enctype="multipart/form-data" >
                    {{ csrf_field() }}
                        <div class="btn-popdown">
                            <div class="col-xs-6">
                                <input type="hidden" name="cancelOrderId" id='cancelOrderId'>
                                <input type="hidden" name="rid" id='rid'>
                                <button class="btn btn-yes" type="button" onclick="cancelAction()">Yes</button>
                            </div>
                            <div class="col-xs-6">
                                <button data-dismiss="modal" class="btn btn-no">Cancel</button>
                            </div>
                        </div>
                    </form>    
                </div>
            </div>
            <div class="modal-footer modal-footer-1">
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->
<div class="title-controlpanel">
    <div class="container">
        <div class="title-control-postion">
            <div class="left-supportbtn">
                <a href="{{url('seller/orders/active')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Orders </button>
                </a>
            </div>
            <div class="title-support">
                <h1> Resolution Centre </h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="title-orderstatus">
        <h2> Messages </h2>
    </div>
    <div class="message-system">
        {{-- This is one way communication system . Buyer will be notified of your replay via email. Buyer cannot replay you back. If this requires longer communication please use our <a href="#"> messaging system </a> --}}
    </div>
    <div id="commonmsg"></div>
    <div class="Resolution-page">
        <div class="Resolution-page-line">
            <div class="row">
                <div class="col-xs-3">
                    <div class="Resolution-center">
                        ACTION
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="Resolution-center">
                        NAME
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="Resolution-center">
                        ORDER NO
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="Resolution-center">
                        SUBJECT
                    </div>
                </div>
            </div>
        </div>


        <div class="accordion-tab">
            @if(isset($data) && count($data))
                @foreach($data as $key => $val)
                    <div class="accordion-toggle-tab">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="Resolution-center1 btn-cancelled{{$val->i_order_id}}" id="btn-cancelled{{$val->id}}">
                                    @if(isset($val->hasOrder()->v_order_status) && $val->hasOrder()->v_order_status == 'cancelled')
                                        <button class="btn btn-Cancel-Order" type="button">Order Cancelled</button>
                                    @else
                                        <button class="btn btn-Cancel-Order" type="button" onclick="confirmCancelOrder('{{$val->i_order_id}}','{{$val->id}}')">Cancel Order</button>
                                   @endif 
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="Resolution-center1">
                                    <p>{{$val->hasUser()->v_fname or ''}} {{$val->hasUser()->v_lname or ''}}</p>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="Resolution-center1">
                                    <p>#{{$val->hasOrder()->i_order_no or ''}}</p>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="Resolution-center1">
                                    <p>{{$val->v_subject or ''}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-content-tab">
                        <div class="Resolution-contant">
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="Original-data">
                                        <h3>Original message by {{$val->hasUser()->v_fname or ''}} {{$val->hasUser()->v_lname or ''}}</h3>
                                        <p>{{isset($val->d_added) ? date("F d, Y. h:i:s A",strtotime($val->d_added)) : ''}}</p>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                
                                <form  method="POST" name="Commnetform_{{$val->id}}" id="Commnetform_{{$val->id}}" data-parsley-validate enctype="multipart/form-data" onsubmit="replayBuyer('{{$val->id}}')" action="javascript:;" >
                                    {{ csrf_field() }}
                                    <div class="Original-second">
                                        <p><strong>{{$val->v_subject or ''}}</strong></p>
                                        <p>{{$val->l_message or ''}}</p>
                                    </div>
                                    <div class="Original-second" id="replybox_{{$val->id}}">
                                        @if(!isset($val->l_reply))
                                        <input type="hidden" name="rid" value="{{$val->id}}">
                                        <input type="text" class="form-control" name="l_reply" placeholder="Write reply">
                                        <button type="button" onclick="replayBuyer('{{$val->id}}')" class="btn btn-convarsation-submit">Submit</button>
                                        @endif
                                    </div>
                                 </form>
                                    
                                </div>
                            </div>

                            <div id="replystr_{{$val->id}}" style="margin-top: 15px;border-top: 1px solid #b7b7b7;">
                            @if(isset($val->l_reply) && $val->l_reply!="")
                            <div class="row" style="margin-top: 15px;">
                                
                                <div class="col-xs-3">
                                    <div class="Original-data">
                                        <h3>Original message by Me</h3>
                                        <p>{{isset($val->d_reply_date) ? date("F d, Y. h:i:s A",strtotime($val->d_reply_date)) : ''}}</p>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <div class="Original-second">
                                        <p>{{$val->l_reply or ''}}</p>
                                    </div>
                                </div>
                            </div>
                            @endif
                            </div>
                           

                        </div>
                        
                        {{--
                        <div class="Resolution-contant">
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="Original-data">
                                        <h3>Original message by John Doe</h3>
                                        <p>November 15, 2017. 6:21AM</p>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <div class="Original-second">
                                        <p>Niche Site Creations believes in helping individuals succeed with niche site affiliate marketing. From keyword research and product selection to site creation and promotion, it's all an important part of the process that the Niche Site Creations team strive to help with. Niche Site Creations believes in helping individuals succeed with niche site affiliate marketing. From keyword research and product selection to site creation and promotion, it's all an important part of the process that the Niche Site Creations team strive to help with. </p>
                                        <input type="text" class="form-control" name="text" placeholder="Write reply">
                                    </div>
                                </div>
                            </div>
                        </div>
                        --}}    

                    </div>
                    
                @endforeach
            @else
               <div class="Resolution-page-line">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="Resolution-center">
                                No resolution found.
                            </div>
                        </div>
                    </div>
                </div>     
            @endif
        </div>
    </div>
</div>
@stop
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('.accordion-tab').find('.accordion-toggle-tab').click(function() {
            $(this).next().slideToggle('600');
            $(".accordion-content-tab").not($(this).next()).slideUp('600');
        });
        $('.accordion-toggle-tab').on('click', function() {
            $(this).toggleClass('active').siblings().removeClass('active');
        });
    });

    function confirmCancelOrder( id,rid ) {
      $('#cancelOrderId').val(id);
      $('#rid').val(rid);
      $('#CancelOrderModal').modal('show');
    }
    
    function replayBuyer(id){
        
        var url = "{{url('seller/reply-order-resolution')}}";     
        var formValidFalg = $("#Commnetform_"+id).parsley().validate('');

        if(formValidFalg){

            document.getElementById('load').style.visibility="visible";
            var formdata = $("#Commnetform_"+id).serialize();

            $.ajax({
                url: url,
                type: "POST",
                data: formdata,
                success : function( res ){
                 
                    document.getElementById('load').style.visibility='hidden';
                    var obj = jQuery.parseJSON(res);
                    
                    if(obj['status']==1){
                        $('#replystr_'+id).html(obj['replystr']);
                        $('#replybox_'+id).html("");
                        $('#commonmsg').html(obj['msg']);
                    }else{
                        $('#commonmsg').html(obj['msg']);
                    }
                },
                error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                    $('#commonmsg').html(obj['msg']);
                }
            });




        }



    }

function cancelAction(id=""){
    
    var url = "{{url('seller/cancel-order')}}";     
    var formValidFalg = $("#cancelOrderForm").parsley().validate('');
    
    if(formValidFalg){
       
       document.getElementById('load').style.visibility="visible";
       var formdata = $("#cancelOrderForm").serialize();
       var rid = $('#cancelOrderId').val();
       
        $.ajax({
          url: url,
          type: "POST",
          data: formdata,
          success : function( res ){
             
             document.getElementById('load').style.visibility='hidden';
             var obj = jQuery.parseJSON(res);

             if(obj['status']==1){
                $('.btn-cancelled'+rid).html('<button class="btn btn-Cancel-Order">Order Cancelled</button>');
                $('#commonmsg').html(obj['msg']);
                $("#CancelOrderModal").modal("hide");
             }else{
                $('#commonmsg').html(obj['msg']);
             }
          },
          error: function ( jqXHR, exception ) {
                document.getElementById('load').style.visibility='hidden';
                $('#commonmsg').html(obj['msg']);
          }
       });

    }


}
</script>
@stop

