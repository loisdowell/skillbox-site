@extends('layouts.frontend')

@section('content')
    
    <!-- 26B-control-panel-invoice-2 -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('seller/dashboard')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </button>
                    </a>
                </div>
                <div class="title-support">
                    <h1>My Sale Invoices</h1>
                </div>
            </div>
        </div>
    </div>  

    <div class="container">
        <div class="main-invoice-setting">
            <div class="invoice-history">
                <h5>
                    History
                </h5>
                <div class="invoice-table">
                    <div class="table-responsive">
                        <table class="table table-invoicedetail">
                            <thead>
                                <tr>
                                    <th>DATE</th>
                                    <th>TYPE</th>
                                    <th>FOR</th>
                                    <th>AMOUNT</th>
                                    <th>DOWNLOAD</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(isset($invoice) && count($invoice))
                                    @foreach($invoice as $key => $val)
                                        <tr>
                                            <td>{{date("M d, Y",strtotime($val->d_added))}}</td>
                                            <td>
                                                @if($val->e_transaction_type=="skill")
                                                    Skill
                                                @else
                                                    Course
                                                @endif    
                                            </td>
                                            <td>Payment for {{$val->v_order_detail[0]['name']}}</td>
                                            <td><span class="active-amount-color"> + £{{$val->v_amount}}</span></td>
                                            <td> 
                                                <a href="{{url('seller/invoice/course/download')}}/{{$val->id}}">    
                                                    Download Invoice <img src="{{url('public/Assets/frontend/images/download-logo.png')}}" alt="Download" class="download-logo">
                                                </a>
                                            </td>

                                        </tr> 
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="text-center">No invoices found.</td>
                                    </tr>    
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-invoice-2 -->

@stop

@section('js')
     <script type="text/javascript">
        
        function masterSearch(data){
            $("#searchrransaction").submit();
        }

    </script>
@stop

