@extends('layouts.frontend')

@section('content')
    <style type="text/css">
    .ans-qus-final-progress ul.parsley-errors-list{
        margin-left: 106px !important;
    }
</style>
    <script src="{{url('public/Assets/plugins/countdown.js')}}" type="text/javascript"></script>
    <style type="text/css">
        .countdown_dashboard {margin: 0px 0px 89px;}
    </style>

    <!-- 26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS-in progress -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                <a href="{{url('seller/orders/active')}}">
                    <button type="button" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Orders </button>
                </a>    
                </div>
                <div class="title-support">
                    <h1> Order Details </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-detail">
        <div class="container">
            <ul class="nav nav-pills nav-order-detail">
                <li><a href="#"><i class="zmdi zmdi-hc-fw"></i> PAYMENT COLLECTED</a></li>
                
                @if(isset($data->v_order_requirements_submited) && $data->v_order_requirements_submited=="yes")    
                <li><a href="#"><i class="zmdi zmdi-hc-fw"></i> BUYER SUBMITTED REQUIREMENTS </a></li>
                @endif
                
                <li class="active"><a href="#"><i class="zmdi zmdi-hc-fw"></i> ORDER  
                    @if($data->v_order_status=="active")
                    IN PROGRESS
                    @elseif($data->v_order_status=="delivered")
                        @if($data->v_order_status=="delivered" && !isset($data->v_buyer_deliver_status))
                            DELIVERED
                        @else
                            COMPLETED
                        @endif
                    @elseif($data->v_order_status=="completed")
                    COMPLETED
                    @elseif($data->v_order_status=="cancelled")
                    CANCELLED
                    @endif
                </a></li>
                @if(isset($data->v_order_status) && $data->v_order_status!="completed" && $data->v_order_status!="delivered" && !isset($data->v_buyer_deliver_status))
                <li class="pull-right deliverbtn">
                    <form  method="POST" name="deleiverorder"  action="{{url('seller/orders/delivery-order-header')}}" enctype="multipart/form-data" >
                        <input type="hidden" name="v_order_id" value="{{$data->id}}">
                        <input type="hidden" name="v_order_deliver" id="v_order_deliver_final" value="1">
                        {{ csrf_field() }}
                        {{-- <button type="submit" class="btn btn-delivery-order"> Deliver Your Order</button> --}}
                    </form>
                </li>
                @endif

            </ul>
        </div>
    </div>

    <?php 
        
        $v_fname="";
        if(count($data->hasUser()) && isset($data->hasUser()->v_fname)){
            $v_fname=$data->hasUser()->v_fname;
        }

        $v_lname="";
        if(count($data->hasUser()) && isset($data->hasUser()->v_lname)){
            $v_lname=$data->hasUser()->v_lname;
        }
     ?> 

    <!-- Tab panes -->
    <div class="container">

        @if ($success = Session::get('success'))
          <div class="alert alert-success alert-big alert-dismissable br-5" style="margin-top: 10px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-info alert-danger" style="margin-top: 10px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
          </div>
        @endif


        <div class="order-progress">
            <div class="row">
                

                 <div class="col-sm-9">
                    <div class="active-btn-t">
                        <button class="btn btn-active-btn-t">ACTIVITY</button>
                    </div>
                    <div class="download-invoice-final">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="list-heading list-heading-two">
                                    <ul>
                                        <li><b>Buyer :</b> {{ucfirst($v_fname)}} {{ucfirst($v_lname)}}</li>
                                        |
                                        <li>Order: #{{isset($data->i_order_no) ? $data->i_order_no : ''}} </li>
                                        |
                                        <li> {{isset($data->d_date) ? date("M d, Y ",strtotime($data->d_date)) : ''}}.</li>
                                    </ul>
                                    <span>£{{isset($data->v_amount) ? $data->v_amount : ''}}</span>
                                </div>
                            </div>
                            
                            <div class="col-xs-12">
                                <div class="grand-total">
                                    <table class="table table-condensed">
                                        <tr>
                                            <th style="width: 40%;">TITLE</th>
                                            <th>QUANTITY</th>
                                            <th>DURATION</th>
                                            <th>AMOUNT</th>
                                        </tr>
                                        @php
                                            $subtotal=0;
                                        @endphp

                                        @if(isset($data->v_order_detail) && count($data->v_order_detail))
                                            @foreach($data->v_order_detail as $k=>$v)
                                                @php
                                                    $subtotal = $subtotal+($v['price']*$v['qty']);
                                                @endphp
                                                <tr>
                                                    <td class="Subtotal">{{isset($v['name']) ? $v['name'] : ''}}</td>
                                                    <td>{{isset($v['qty']) ? $v['qty'] : ''}}</td>
                                                    @if($k>0)
                                                    <td>-</td>    
                                                    @else
                                                    <td>{{isset($data->v_duration_time) ? $data->v_duration_time : ''}} {{isset($data->v_duration_in) ? $data->v_duration_in : ''}}</td>
                                                    @endif
                                                    <td>£{{ $v['price']*$v['qty'] }}</td>
                                                </tr>
                                            @endforeach
                                        @endif 
                                    </table>
                                </div>
                                <div class="grand-viwe grand-viwe-fianl">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="Subtotal-point">
                                                <b>Subtotal</b>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="sub-total-leval">
                                                <b>@if($data->v_order_status=="cancelled") - @endif £{{$subtotal}}</b>
                                            </div>
                                        </div>

                                        @php
                                             $CommissionCharge=  $subtotal*$comission/100; 
                                        @endphp

                                        <div class="col-xs-8">
                                            <div class="Subtotal-point">
                                                <b>Skillbox Commission</b>
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="sub-total-leval">
                                                <b>-£{{number_format($CommissionCharge,2)}}</b>
                                            </div>
                                        </div>

                                         <div class="col-xs-8">
                                            <div class="Subtotal-point">
                                                <b>Processing fees</b>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="sub-total-leval">
                                                <b>-£{{number_format($processing,2)}}</b>
                                            </div>
                                        </div>
                                        @php
                                            $charge=$CommissionCharge+$processing;
                                            $total = $subtotal-$charge; 
                                        @endphp
                                        </div>
                                           
                                        <div class="grand-viwe">
                                        <div class="row">
                                        <div class="col-xs-8">
                                            <div class="Subtotal-point">
                                                <b>Total</b>
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="sub-total-leval" >
                                                <b> @if($data->v_order_status=="cancelled") - @endif £{{number_format($total,2)}}</b>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                @php
                    
                    $hours=0;
                    $day=0;
                    $month=0;
                    $year=0;
                    $min=0;
                    $sec=0;
                    $hours=0;
       
                    $enddate = $data->d_added;
                    if(isset($data->v_order_start) && isset($data->d_order_start_date) && $data->v_order_start=="yes"){
                        $enddate = $data->d_order_start_date;
                    }

                    if(isset($data->v_duration_in) && $data->v_duration_in=="hours"){
                        
                        if(isset($data->v_duration_time)){
                            $hours = (int)$data->v_duration_time;
                        }
                       
                        $cdate = date("Y-m-d H:i:s");
                        if(isset($data->v_order_start) && $data->v_order_start!="yes"){
                            if(isset($data->v_order_deliver_date) && $data->v_order_deliver_date!=""){
                                $cdate = $data->v_order_deliver_date;    
                            }else{
                                $cdate = $data->d_added;    
                            }
                        }else{
                            $cdate=date("Y-m-d H:i:s");
                        }
                        
                        $seconds = strtotime($cdate)-strtotime($enddate);
                        $totalsecond = $hours*3600;

                        if($totalsecond>$seconds){
                            $seconds = $totalsecond - $seconds;    
                        }else{
                            $seconds = 0;    
                        }
                        
                        $hours=0;
                        $hours=0;
                        $min=0;
                        $sec=$seconds;

                    }

                    if(isset($data->v_duration_in) && $data->v_duration_in=="day"){
                            
                        if(isset($data->v_duration_time)){
                            $day = $data->v_duration_time;
                        }

                        $cdate = date("Y-m-d H:i:s");
                        if(isset($data->v_order_start) && $data->v_order_start!="yes"){
                            if(isset($data->v_order_deliver_date) && $data->v_order_deliver_date!=""){
                                $cdate = $data->v_order_deliver_date;    
                            }else{
                                $cdate = $data->d_added;    
                            }
                        }else{
                            $cdate=date("Y-m-d H:i:s");
                        }

                        //$cdate=date("Y-m-d H:i:s");
                        $odate = $enddate;
                        $seconds = strtotime($cdate)-strtotime($odate);
                        $daydiff = ceil(($seconds / (60 * 60 * 24)));
                        $totalsecond = $day*86400;
                        $seconds = $totalsecond - $seconds;
                        $day=0;
                        $day = 0;  
                        $hours =0;
                        $min = 0;
                        $sec = $seconds;
                    }    
                    
                    if(isset($data->v_duration_in) && $data->v_duration_in=="month"){

                            if(isset($data->v_duration_time)){
                                $day = $data->v_duration_time*30;
                            }

                            $cdate = date("Y-m-d H:i:s");
                            if(isset($data->v_order_start) && $data->v_order_start!="yes"){
                                if(isset($data->v_order_deliver_date) && $data->v_order_deliver_date!=""){
                                    $cdate = $data->v_order_deliver_date;    
                                }else{
                                    $cdate = $data->d_added;    
                                }
                            }else{
                                $cdate=date("Y-m-d H:i:s");
                            }
                            //$cdate=date("Y-m-d H:i:s");
                            
                            $odate = $enddate;
                            $seconds = strtotime($cdate)-strtotime($odate);
                            $daydiff = ceil($seconds / (60 * 60 * 24));

                            $totalsecond = $day*86400;
                            $seconds = $totalsecond - $seconds;
                            $day=0;
                            $day = 0;  
                            $hours =0;
                            $min = 0;
                            $sec = $seconds;
                        }

                    if(isset($data->v_duration_in) && $data->v_duration_in=="year"){

                        if(isset($data->v_duration_time)){
                            $day = $data->v_duration_time*288;
                        }

                        $cdate = date("Y-m-d H:i:s");
                        if(isset($data->v_order_start) && $data->v_order_start!="yes"){
                            if(isset($data->v_order_deliver_date) && $data->v_order_deliver_date!=""){
                                $cdate = $data->v_order_deliver_date;    
                            }else{
                                $cdate = $data->d_added;    
                            }
                        }else{
                            $cdate=date("Y-m-d H:i:s");
                        }
                        //$cdate=date("Y-m-d H:i:s");
                        
                        $seconds = strtotime($cdate)-strtotime($enddate);
                        $daytotal = $seconds / (60 * 60 * 24);

                        $totalsecond = $day*86400;
                        $seconds = $totalsecond - $seconds;
                        $day=0;
                        $day = 0;  
                        $hours =0;
                        $min = 0;
                        $sec = $seconds;
                    }

                    // $enddate = $data->d_added;
                    // if(isset($data->v_order_start) && isset($data->d_order_start_date) && $data->v_order_start=="yes"){
                    //     $enddate = $data->d_order_start_date;
                    // }

                    // if(isset($data->v_duration_in) && $data->v_duration_in=="hours"){
                        
                    //     if(isset($data->v_duration_time)){
                    //         $hours = (int)$data->v_duration_time;
                    //     }
                    //     $cdate=date("Y-m-d H:i:s");
                    //     $seconds = strtotime($cdate)-strtotime($enddate);
                    //     $hourstotal = $seconds / 60 / 60;
                        
                    //     $totalduration = $data->v_duration_time.":00:00";
                    //     if($hours>$hourstotal){
                            
                    //         $a = strtotime($totalduration)-$seconds;
                    //         $hours = date("H",$a);
                    //         $min = date("i",$a);
                    //         $sec = date("s",$a);
                      
                    //     }else{
                    //         $hours=0;
                    //         $hours = 0;
                    //         $min = 0;
                    //         $sec = 0;
                    //     }

                    // }

                    // if(isset($data->v_duration_in) && $data->v_duration_in=="day"){
                        
                    //     if(isset($data->v_duration_time)){
                    //         $day = $data->v_duration_time;
                    //     }

                    //     $cdate=date("Y-m-d H:i:s");
                    //     $odate = $enddate;
                    //     $seconds = strtotime($cdate)-strtotime($odate);
                    //     $daydiff = ceil($seconds / (60 * 60 * 24));
                        
                    //     if($day>$daydiff){
                    //         $day = $day-$daydiff;  
                    //         $hours = date("H",$seconds);
                    //         //$min = date("i",$seconds);
                    //         //$sec = date("s",$seconds);
                    //     }

                    // }

                    // if(isset($data->v_duration_in) && $data->v_duration_in=="month"){

                       
                    //     if(isset($data->v_duration_time)){
                    //         $day = $data->v_duration_time*30;
                    //     }

                    //     $cdate=date("Y-m-d H:i:s");
                    //     $odate = $enddate;
                    //     $seconds = strtotime($cdate)-strtotime($odate);
                    //     $daydiff = ceil($seconds / (60 * 60 * 24));
                        
                    //     if($day>$daydiff){
                    //         $day = $day-$daydiff;  
                    //         $hours = date("H",$seconds);
                    //         $min = date("i",$seconds);
                    //         $sec = date("s",$seconds);
                    //     }

                    // }

                    // if(isset($data->v_duration_in) && $data->v_duration_in=="year"){

                    //     if(isset($data->v_duration_time)){
                    //         $day = $data->v_duration_time*288;
                    //     }

                    //     $cdate=date("Y-m-d H:i:s");
                    //     $seconds = strtotime($cdate)-strtotime($enddate);
                    //     $daytotal = $seconds / (60 * 60 * 24);

                    //     if($day>$daytotal){

                    //         $a = strtotime($totalduration)-$seconds;
                    //         $hours = date("H",$a);
                    //         $min = date("i",$a);
                    //         $sec = date("s",$a);
                    //     }else{
                    //         $day=0;
                    //     }

                    // }
                @endphp

                @if(isset($data->v_order_start) && $data->v_order_start=="yes")
                <div class="col-sm-3">
                    <div class="timer-date" style="margin:0px 0px !important;">
                    <div class="margin-t">
                        <div class="row">
                        <div class="countdown_dashboard">
                            <div class="col-xs-12 date-time-section2">
                                <div class="timer-box-b dash day_dash">
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="text-days-text">
                                        DAYS
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 no-padding-order date-time-section2 ">
                                <div class="timer-box-b dash hour_dash">
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="text-days-text">
                                        HOURS
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 no-padding-order date-time-section2 ">
                                <div class="timer-box-b dash min_dash">
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="text-days-text">
                                        MINUTES
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-xs-12 no-padding-order no-padding-right date-time-section2 ">
                                
                                <div class="timer-box-b last-child-b dash dash sec_dash">
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="text-days-text">
                                        SECONDS
                                    </div>
                                </div>
                            </div>

                        </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="col-sm-3">
                    <div class="timer-date" style="margin:0px 0px !important;">
                    <div class="margin-t">
                        <div class="row">
                        <div class="countdown_dashboard">
                            <div class="col-xs-12 date-time-section2">
                                <div class="timer-box-b dash day_dash">
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="text-days-text">
                                        DAYS
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 no-padding-order date-time-section2 ">
                                <div class="timer-box-b dash hour_dash">
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="text-days-text">
                                        HOURS
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 no-padding-order date-time-section2 ">
                                <div class="timer-box-b dash min_dash">
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="text-days-text">
                                        MINUTES
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-xs-12 no-padding-order no-padding-right date-time-section2 ">
                                
                                <div class="timer-box-b last-child-b dash dash sec_dash">
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="timer-date-box digit">
                                        0
                                    </div>
                                    <div class="text-days-text">
                                        SECONDS
                                    </div>
                                </div>
                            </div>

                        </div>
                        </div>
                    </div>
                </div>
                @endif

                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="container">

        {{-- @if ($success = Session::get('success'))
          <div class="alert alert-success alert-big alert-dismissable br-5">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $success }}
          </div>
        @endif
        @if ($warning = Session::get('warning'))
          <div class="alert alert-info alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;{{ $warning }}
          </div>
        @endif --}}

        <div class="accordion-final-progress">
            <div class="accordion accordion-next">
                <h4 class="accordion-toggle-progress">ORDER REQUIREMENTS
                    <p>You’ve filled out the requirements</p>
                </h4>
                <div class="accordion-content-progress">
                    
                    <div id="reqconv">
                    @if(isset($orderRequirements) && count($orderRequirements))
                        @foreach($orderRequirements as $k=>$v)

                            @if($v->e_type == "order_deliver")
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>Order delivered</p>
                                    <p class="litral-text">You have delivered this order.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i M d Y",strtotime($v->d_added)) : ''}}.</p>
                                </div>
                            @elseif($v->e_type == "not_satisfied")
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>I'm not Satisfied</p>
                                    <p class="litral-text">Buyer is not satisfied.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i M d Y",strtotime($v->d_added)) : ''}}.</p>
                                </div>

                             @elseif($v->e_type == "order_cancel_req")
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>Order Cancel request</p>
                                    <p class="litral-text">Buyer requested to cancel the order.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i M d Y",strtotime($v->d_added)) : ''}}.</p>
                                </div>     
                            
                             @elseif($v->e_type == "order_cancelled")
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>Order Cancelled</p>
                                    <p class="litral-text">You have cancelled this order.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i M d Y",strtotime($v->d_added)) : ''}}.</p>
                                </div> 

                            @elseif($v->e_type == "req_posted")
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>Order Requirements </p>
                                    <p class="litral-text">Buyer has submitted the order requirements.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i M d Y",strtotime($v->d_added)) : ''}}.</p>
                                </div> 

                            @elseif($v->e_type == "extendtime")

                                @if($v->v_extend_resquest_status == "pending")
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>Extend Order Request</p>
                                    <p class="litral-text">You have requested to extend the order delivery by {{isset($v->v_duration_time) ? $v->v_duration_time : ''}} {{isset($v->v_duration_in) ? $v->v_duration_in : ''}}.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i M d Y",strtotime($v->d_added)) : ''}}.</p>
                                </div>
                                @elseif($v->v_extend_resquest_status == "reject")
                                    <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                        <p>Extend Order Request</p>
                                        <p class="litral-text">buyer rejected your request to extend the order delivery by {{isset($v->v_duration_time) ? $v->v_duration_time : ''}} {{isset($v->v_duration_in) ? $v->v_duration_in : ''}}.</p>
                                        <p class="litral-text">{{isset($v->d_added) ? date("H:i M d Y",strtotime($v->d_added)) : ''}}.</p>
                                    </div>
                                @else
                                
                                <div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">
                                    <p>Order Extended</p>
                                    <p class="litral-text">buyer accepted your request to extend the order delivery by {{isset($v->v_duration_time) ? $v->v_duration_time : ''}} {{isset($v->v_duration_in) ? $v->v_duration_in : ''}}.</p>
                                    <p class="litral-text">{{isset($v->d_added) ? date("H:i M d Y") : ''}}.</p>
                                </div>
                                @endif

                            @elseif($v->e_type == "requirements")
                                <div class="accordion-content-progress1">
                                    <div class="ans-qus-final">
                                        @php
                                            if(isset($v->l_message)){
                                                echo $v->l_message;
                                            }
                                        @endphp
                                    </div>
                                </div>
                            @else
                                <div class="accordion-content-progress1">
                                    <div class="ans-qus-final">
                                        <div class="Announcements-find-progress">
                                            
                                            <div class="Announcements-man">
                                                @php
                                                    $user=array();
                                                    if(count($v->hasUser())){
                                                        $user=$v->hasUser();
                                                    }

                                                    $imgdata = \App\Helpers\General::userroundimage($user);
                                                    echo $imgdata;
                                                @endphp
                                            </div>
                                            <div class="Announcements-p-tag">
                                                <p>
                                                    @php
                                                        if(isset($user['v_fname']) && $user['v_lname']){
                                                            echo $user['v_fname'].' '.$user['v_lname'];
                                                        }    
                                                    @endphp    
                                                </p>
                                                
                                                <span class="span-text">
                                                    @php
                                                        if(isset($v->l_message)){
                                                            echo $v->l_message;
                                                        }
                                                    @endphp
                                                </span>
                                                @if(isset($v->v_document) && $v->v_document!="" && count($v->v_document))
                                                    @foreach($v->v_document as $key=>$val)

                                                        @php
                                                            $durl = \Storage::cloud()->url($val);
                                                            $docname="";
                                                            if($v!=""){
                                                                $docname = explode("/", $val);
                                                            }
                                                            $docname = str_replace("=", '.', $key);        
                                                        @endphp
                                                        <p>
                                                        <a href="{{$durl}}" target="_blank">    
                                                        <img src="{{url('public/Assets/frontend/images/link-save.png')}}" alt="" /><span style="margin-left: 5px">{{$docname}}</span></p>
                                                        </a>
                                                            
                                                    @endforeach
                                                @endif
                                                <p class="time-p-tag"> {{date("H:i M d, Y",strtotime($v->d_added))}} </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                    </div>    

                    @if(isset($data->v_order_status) && $data->v_order_status=="delivered" && isset($data->v_buyer_deliver_status) && $data->v_buyer_deliver_status == "accept")

                        @php
                            $compdate = $data->d_order_completed_date;
                            $currentdate = date("Y-m-d H:i:s",strtotime("+ 1 day"));
                        @endphp

                        @if(isset($sellerreview) && count($sellerreview))
                            <div class="accordion-content-progress1">
                                <div class="ans-qus-final text-center">
                                    <div class="accepted-msg1">
                                        <p data-toggle="modal" data-target="#leaveReview">ORDER REVIEWS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-content-progress1">
                                <div class="ans-qus-final">
                                    <div class="Announcements-find-progress">
                                        <div class="Announcements-man">

                                            @php
                                                $v_image="";
                                                if(count($sellerreview->hasUser()) && isset($sellerreview->hasUser()->v_image)){
                                                    $v_image=$sellerreview->hasUser()->v_image;
                                                }
                                                $v_fname="";
                                                if(count($sellerreview->hasUser()) && isset($sellerreview->hasUser()->v_fname)){
                                                    $v_fname=$sellerreview->hasUser()->v_fname;
                                                }

                                                $v_lname="";
                                                if(count($sellerreview->hasUser()) && isset($sellerreview->hasUser()->v_lname)){
                                                    $v_lname=$sellerreview->hasUser()->v_lname;
                                                }
                                            @endphp
                                            @php
                                                  $imgdata = '';
                                                  if(Storage::disk('s3')->exists($v_image)){
                                                      $imgdata = \Storage::cloud()->url($v_image);
                                                  }else{
                                                      $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                                                  }
                                            @endphp  
                                           <img src="{{$imgdata}}" alt="" />
                                        </div>
                                        
                                        <div class="Announcements-p-tag">
                                            <p>{{$v_fname}} {{$v_lname}}</p>
                                            <div class="all-in-one-star">
                                                <span class="span-text">
                                                    {{isset($sellerreview->l_comment) ? $sellerreview->l_comment : ''}}
                                                </span>
                                            </div>
                                            
                                            <div class="all-in-one-star">
                                                <span class="span-text">
                                                    Communication with seller
                                                </span>
                                                <div class="rating-Course">
                                                    <div class="rating-buyer">
                                                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" value="{{isset($sellerreview->i_communication_star) ? $sellerreview->i_communication_star : ''}}" readonly>
                                                        <span class="star-no">( {{$sellerreview->i_communication_star}} )</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all-in-one-star">
                                                <span class="span-text">
                                                    Service as Described
                                                </span>
                                                <div class="rating-Course">
                                                    <div class="rating-buyer">
                                                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{isset($sellerreview->i_described_star) ? $sellerreview->i_described_star : ''}}" readonly>
                                                        <span class="star-no">( {{$sellerreview->i_described_star}} )</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all-in-one-star">
                                                <span class="span-text">
                                                    Buy Again or Recommend
                                                </span>
                                                <div class="rating-Course">
                                                    <div class="rating-buyer">
                                                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{isset($sellerreview->i_recommend_star) ? $sellerreview->i_recommend_star : ''}}" readonly>
                                                        <span class="star-no">( {{$sellerreview->i_recommend_star}} )</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(isset($sellerreview->l_answers) && count($sellerreview->l_answers))

                                <div class="accordion-content-progress1">
                                    <div class="ans-qus-final">
                                        <div class="Announcements-find Announcements-find-q">
                                            <div class="Announcements-man">
                                                @php
                                                    $imgdata = \App\Helpers\General::userroundimage($userdata);
                                                    echo $imgdata;
                                                @endphp
                                            </div>
                                            <div class="Announcements-name Announcements-name-q">
                                                <p>Me</p>
                                                <p>{{isset($sellerreview->l_answers['l_comment']) ? $sellerreview->l_answers['l_comment'] : ''}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                            
                            @if(strtotime($compdate)<strtotime($currentdate))
                            <div id="rcommentdata">
                            <div class="accordion-content-progress1">
                                <div class="ans-qus-final">
                                    <div class="Announcements-find Announcements-find-q">
                                        <div class="Announcements-man">
                                            @php
                                                $imgdata = \App\Helpers\General::userroundimage($userdata);
                                                echo $imgdata;
                                            @endphp
                                        </div>
                                        <div class="Announcements-name Announcements-name-q">
                                            <p>Me</p>
                                            <form  method="POST" name="reviewcmt" id="reviewcmt" data-parsley-validate enctype="multipart/form-data" >
                                               <input type="hidden" name="i_review_id" value="{{$sellerreview->id}}"> 
                                               {{ csrf_field() }}
                                                
                                                <div class="input-group" >
                                                    <input type="text" id="l_commentrev" class="form-control comment-seller pull-left" name="l_comment" placeholder="Enter your comment" required>
                                                    <span class="input-group-addon"><button type="button" onclick="submitcomment()" class="btn btn-convarsation-submit pull-right">Submit</button></span>
                                                </div>

                                                {{-- <div>
                                                <input type="text" class="form-control comment-seller pull-left" name="l_comment" placeholder="Enter your comment" required>
                                                <button type="button" onclick="submitcomment()" class="btn btn-convarsation-submit pull-right">Submit</button>
                                                </div> --}}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                            @endif

                            @endif

                        @endif    

                   {{--  @endif --}}
                   @else
                    {{-- @if(isset($data->v_order_status) && $data->v_order_status=="active" && !isset($data->v_buyer_deliver_status) ) --}}
                    @if(isset($data->v_order_status) && $data->v_order_status!="cancelled")
                    <form class="horizontal-form" role="form" method="POST" name="submitreq" id="submitreq" action="{{url('seller/orders/post-requirement')}}" data-parsley-validate enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <input type="hidden" name="v_order_id" value="{{$data->id}}">
                        <input type="hidden" name="v_order_deliver" id="v_order_deliver" value="0">
                        <div class="ans-qus-final-progress">
                            <div class="ans-qus-final-counter">
                                <textarea cols="200" name="l_message" rows="8" class="char-counter" placeholder="Type your message here...." required></textarea>
                            </div>
                            <div class="link-img">
                                <input type='file' name="v_document_order[]" multiple id="fileupload-down" style="display: none;" onchange="docuploaddata();" />
                                <label id="fileupload-down" for="fileupload-down"><img src="{{url('public/Assets/frontend/images/link-save.png')}}" alt="" /></label>
                            </div>
                        </div>

                        <div class="ans-qus-final-progress2 pull-right">
                            <ul class="pull-right" id="docupload">
                            </ul>
                        </div>
                        <script type="text/javascript">
                            function docuploaddata(){
                                var input = document.getElementById('fileupload-down');
                                var list = document.getElementById('docupload');
                                var strappend="";
                                for (var x = 0; x < input.files.length; x++) {
                                    strappend = strappend+"<li>"+input.files[x].name+"</li>";
                                }
                                $("#docupload").html(strappend);
                            }
                        </script>
                        
                        <div class="ans-qus-final-progress2">
                            <div class="btn-textarea-text">
                                <button type="submit" class="btn btn-post-message">Post Message</button>
                                
                                @if(isset($data->v_order_status) && !isset($data->v_buyer_deliver_status))
                                @if($data->v_order_status!="delivered")
                                <button type="button" class="btn btn-post-message" id="display_order_popup">Deliver This Order</button>
                                <div class="popup_orderbox">
                                    <p class="sure-deliver">Are you sure you want to deliver this order?
                                    </p>
                                    <div class="text-center">
                                        <input type="button" class="close_button" value="Cancel">
                                        <input type="button" onclick="deliverOrder()" class="yes_button" value="Yes">
                                    </div>
                                </div>
                                @endif
                                @endif


                            </div>
                        </div>
                    </form>
                    @endif

                    @endif
                        
                    @if(isset($data->v_order_status) && $data->v_order_status=="active")
                        
                        <form class="horizontal-form" role="form" method="POST" name="extendreq" id="extendreq" data-parsley-validate enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            <div class="ans-qus-final-progress2">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 text-center-xs">
                                        <div class="extend-time">
                                            <p> Extend Delivery Time?</p>
                                            <span>
                                                Concerned? Won't able to deliver on time? Request client to extend delivery time.
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 no-padding">
                                        <label class="subfield-name">
                                            Delivery Time
                                        </label>
                                        <input type="hidden" name="v_order_id" value="{{$data->id}}">
                                        <div class="time-concept">
                                            <div class="row">
                                                <div class="col-sm-7">
                                                    <input type="number" min="1" class="form-control input-field" name="v_duration_time" id="v_duration_time" required="">
                                                    <p> Use only numbers</p>
                                                </div>
                                                <div class="col-sm-5 no-padding">
                                                    <select class="form-control form-sell" name="v_duration_in" required>
                                                        <option value="hours">Hours</option>
                                                        <option value="days">Day's</option>
                                                        <option value="months">Month's</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-4 text-center-xs">
                                        <button type="button" class="btn btn-request-sent" id="display_popup" onclick="ExtendRequestTime()">Send Request to Client</button>
                                        <div class="popup_box">
                                            <input type="button" class="cancel_button" value="X">
                                            <p class="info_text">Your order request successfully sent to buyer.
                                            </p>
                                            <p class="info_text">The order delivery date will automatically changed to new date once its been accepted.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endif

                </div>
            </div>
        </div>
        <br>
        <div id="footercommonmsg"></div>
    </div>

    <!-- 26B-control-panel-buyer-SINGLE-ITEM-ORDER-DETAILS-in progress -->

@stop
@section('js')
<script type="text/javascript">
  
</script>

 <script type="text/javascript">
        $(document).ready(function() {
            $(".accordion-content-progress").show();
            $('.accordion').find('.accordion-toggle-progress').click(function() {
                $(this).next().slideToggle('600');
                $(".accordion-content-progress").not($(this).next()).slideUp('600');
            });
            $('.accordion-toggle-progress').on('click', function() {
                $(this).toggleClass('active').siblings().removeClass('active');
            });
        });


        function submitcomment(){

            var actionurl = "{{url('seller/orders/post-review-comment')}}";
            var formValidFalg = $("#reviewcmt").parsley().validate('');
            document.getElementById('load').style.visibility="visible";  
            if(formValidFalg){
             
                var formData = $('#reviewcmt').serialize();
                $.ajax({
                    type    : "POST",
                    url     : actionurl,
                    data    : formData,
                    success : function( res ){
                      document.getElementById('load').style.visibility='hidden';
                        var obj = jQuery.parseJSON(res);
                        if(obj['status']==1){

                            $("#rcommentdata").html(obj['btnstr']);  
                            $("#footercommonmsg").html(obj['msg']);  
                            $("#l_commentrev").val("");
                        }else{
                            $("#footercommonmsg").html(obj['msg']);  
                        }

                    },
                    error: function ( jqXHR, exception ) {
                   
                    }
                });
            }    

        }
    </script>
    
    <script type="text/javascript">
        (function($) {
            "use strict";

            $.fn.charCounter = function(options) {
                String.prototype.format = function() {
                    var content = this;
                    for (var i = 0; i < arguments.length; i++) {
                        var replacement = '{' + i + '}';
                        content = content.replace(replacement, arguments[i]);
                    }
                    return content;
                };

                var options = $.extend({
                    backgroundColor: "#FFFFFF",
                    position: {
                        bottom: -10,
                        left: 20
                    },
                    font: {
                        size: 16,
                        color: "#08090a"
                    },
                    limit: 2500
                }, options);

                return this.each(function() {
                    var el = $(this),
                        wrapper = $("<div/>").css("position", "relative").css("display", "inline-block"),
                        label = $("<span/>").text("{0}/{1}".format(0, options.limit)).css({
                            "backgroundColor": options.backgroundColor,
                            "position": "absolute",
                            "font-size": options.font.size,
                            "color": options.font.color
                        }).css(options.position);

                    el.prop("maxlength", options.limit);
                    el.wrap(wrapper);
                    el.before(label);
                    el.on("keyup", updateLabel).on("keypress", updateLabel).on('keydown', updateLabel);

                    function updateLabel(e) {
                        label.text("{0}/{1}".format($(this).val().length, options.limit));
                    }
                });
            }
        })(jQuery);

        $(document).ready(function() {
            $(".char-counter").charCounter();
        });
    </script>

    <script type="text/javascript">
         
        function ExtendRequestTime(){
            
            var actionurl = "{{url('seller/orders/extend-request')}}";
            var formValidFalg = $("#extendreq").parsley().validate('');

            if(formValidFalg){
             
              var formData = $('#extendreq').serialize();
              document.getElementById('load').style.visibility="visible";  
              $.ajax({
                  type    : "POST",
                  url     : actionurl,
                  data    : formData,
                  success : function( res ){
                      document.getElementById('load').style.visibility='hidden';
                      var obj = jQuery.parseJSON(res);
                      if(obj['status']==1){

                          $("#v_duration_time").val("");  
                          $("#reqconv").append(obj['btnstr']);  
                          $("#footercommonmsg").html(obj['msg']);  
                          $("#display_popup").click(function() {
                                showpopup();
                            });
                            $(".cancel_button").click(function() {
                                hidepopup();
                            });

                      }else{
                        $("#footercommonmsg").html(obj['msg']);  
                      }

                  },
                  error: function ( jqXHR, exception ) {
                    document.getElementById('load').style.visibility='hidden';
                      $("#errormsgstep2").show();
                  }
              });
          }    
        }
    </script>
    
    <script type="text/javascript">

        function deliverOrder(){

            $("#v_order_deliver").val(1);
            var formValidFalg = $("#submitreq").parsley().validate('');
            
            if(formValidFalg){
                $("#submitreq").submit();
            }

            
        }

        // function deliverOrderHeader(){
            
        // }
        
    </script>

    <script type="text/javascript">
        function showpopup() {
            $(".popup_box").fadeToggle();
            $(".popup_box").css({
                "visibility": "visible",
                "display": "block"
            });
        }

        function hidepopup() {
            $(".popup_box").fadeToggle();
            $(".popup_box").css({
                "visibility": "hidden",
                "display": "none"
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#display_order_popup").click(function() {
                showorderpopup();
            });
            // $(".cancel_button").click(function(){
            //  hideorderpopup();
            // });
            $(".close_button").click(function() {
                hideorderpopup();
            });
        });
    </script>

    <script type="text/javascript">
        function showorderpopup() {
            $(".popup_orderbox").fadeToggle();
            $(".popup_orderbox").css({
                "visibility": "visible",
                "display": "block"
            });
        }

        function hideorderpopup() {
            $(".popup_orderbox").fadeToggle();
            $(".popup_orderbox").css({
                "visibility": "hidden",
                "display": "none"
            });
        }

        var sec = '{{$sec}}';    
        var min = '{{$min}}';    
        var hourstotal = '{{$hours}}';
        var daytotal = '{{$day}}';    
        

        $('.countdown_dashboard').countdown();
        $('.countdown_dashboard').data('countdown').update({
          day: parseInt(daytotal),
          hour: parseInt(hourstotal),
          min: parseInt(min),
          sec: parseInt(sec)
        });

        var timet='{{$data->v_order_start}}';
        if(timet!="yes"){
            setTimeout(function(){ 
                $('.countdown_dashboard').data('countdown').stop({
                  day: parseInt(daytotal),
                  hour: parseInt(hourstotal),
                  min: parseInt(min),
                  sec: parseInt(sec)
                });
             }, 1500);
        }


        @if(isset($data->v_order_status) && $data->v_order_status=="cancelled")

            setTimeout(function(){ 
                $('.countdown_dashboard').data('countdown').stop({
                  day: parseInt(daytotal),
                  hour: parseInt(hourstotal),
                  min: parseInt(min),
                  sec: parseInt(sec)
                });
             }, 1500);


        @endif
        




        // var hourstotal = '{{$hours}}';
        // var daytotal = '{{$day}}';    

        // $('.countdown_dashboard').countdown();
        // $('.countdown_dashboard').data('countdown').update({
        //   day: 10,
        //   hour: 5,
        //   min: 15,
        //   sec: 10
        // });


    </script>

@stop

