<div class="clearfix"></div>    
    @if(isset($data) && count($data))
        @foreach($data as $k=>$v)
            @php

                $v_image="";
                $v_fname="";
                $v_lname="";
                
                if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_image)){
                    $v_image=$v->hasFromUser()->v_image;
                }
                if($v_image!=""){
                    $v_image = \Storage::cloud()->url($v_image);
                }else{
                    $v_image = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
                }
                if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_fname)){
                    $v_fname=$v->hasFromUser()->v_fname;
                }
                if(count($v->hasFromUser()) && isset($v->hasFromUser()->v_lname)){
                    $v_lname=$v->hasFromUser()->v_lname;
                }
            @endphp
            <div class="chat-point">
                <div class="buyer-name">
                    <img src="{{$v_image}}" alt="" />
                </div>
                <div class="chat-tital">
                    <span>{{$v->l_message}}</span><br>
                    <span>{{date("h:i a",strtotime($v->d_added))}}</span><span> (<?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($v->d_added))->diffForHumans(); ?>)</span>
                </div>
            </div>

        @endforeach
    @else
        <div class="chat-point">
            <div class="chat-tital">
                <span>There is no message</span>
            </div>
        </div>    
    @endif
<div class="clearfix"></div>