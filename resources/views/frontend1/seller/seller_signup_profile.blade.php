@extends('layouts.frontend')

@section('css')
<link href="{{url('public/Assets/frontend/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />
<script src="{{url('public/Assets/frontend/js/fileinput.js')}}" type="text/javascript"></script>
@stop

@section('content')
   
   <!-- 02B-sign-up-add profile photo-1 -->
    <div class="container">
        <div class="profile-skills">
            <h1>Set Your Profile Image</h1>
        </div>
        <div class="profile-main">
            <div class="profile-img">
                
                <form class="horizontal-form" role="form" method="POST" name="buyersignupprofileform" id="buyersignupprofileform" action="{{url('seller-signup/update-profile')}}" data-parsley-validate enctype="multipart/form-data" >
                  {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-6">
                        <div class="image-peve">
                            <img src="{{url('public/Assets/frontend/images/user1.png')}}" id="edit-images" alt="" />
                            <input type='file' name="v_image" id="fileupload-example-4" onchange="readURL(this);" />
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="upload-img">
                            <input type='file' id="fileupload-example-4" onchange="readURL(this);" />
                            <label class="profile-upload" for="fileupload-example-4">Upload a Photo
                            </label>
                            <p>From your computer</p>
                        </div>
                        
                        <div class="upload-img1">
                            <a href="#">Take a Photo</a>
                            <p>With your webcam</p>
                        </div>
                        <div class="btn-allskip">
                            <div class="col-xs-6">
                                <div class="guide-img">
                                  <a href="{{url('single-search')}}">
                                    <button class="btn btn-guide btn-Skip">Skip</button>
                                  </a>  
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="guide-img">
                                    <button type="submit" class="btn btn-guide btn-Continue">Continue</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </form>

            </div>
        </div>
    </div>
    <!--end- 02B-sign-up-add profile photo-1 -->

@stop


@section('js')



<script type="text/javascript">

      function readURL(input) {

          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function(e) {
                  $('#edit-images')
                      .attr('src', e.target.result)
                      .width(200)
                      .height(200);
              };
              reader.readAsDataURL(input.files[0]);
          }

        }


      function buyerSignup(){

          var actionurl = "{{url('buyer-signup/signup')}}";
          var formValidFalg = $("#buyersignupform").parsley().validate('');

          if(formValidFalg){
              
              var l = Ladda.create(document.getElementById('bsignup'));
              l.start();

              var formdata = $("#buyersignupform").serialize();

              $.ajax({
                  type    : "POST",
                  url     : actionurl,
                  data    : formdata,
                  success : function( res ){
                      l.stop();
                      var obj = jQuery.parseJSON(res);
                      
                      if(obj['status']==1){
                        $("#someerror").show();
                        $("#someerror").html(obj['msg']);
                      }else{
                        $("#someerror").show();
                        $("#someerror").html(obj['msg']);
                      }
                  },
                  error: function ( jqXHR, exception ) {
                      l.stop();
                      $("#someerror").show();
                  }
              });    

          }
      }

</script>

@stop

