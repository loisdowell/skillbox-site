@extends('layouts.frontend')

@section('content')

    <!-- 26B-control-panel-your-buyer-reviews-1 -->
    <div class="title-controlpanel">
        <div class="container">
            <div class="title-control-postion">
                <div class="left-supportbtn">
                    <a href="{{url('seller/dashboard')}}" class="btn btn-back"> <img src="{{url('public/Assets/frontend/images/left-arrow.png')}}" alt="image"> Back to Control Panel </a>
                </div>
                <div class="title-support">
                    <h1> Review Centre </h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="main-buyeraccount">
            
            <div class="buyer-reviews">


                <div class="buyer-first">
                      <div class="buyer-profile">
                          <div class="buyer-profile">
                              <h1>Reviews by Buyers</h1>
                              <div class="select-filter1">
                                  <div class="select-filter">
                                      <span>Filter by:</span>&nbsp;
                                      <select style="width: auto !important" class="resizing_select1 arr_img" name="v_order_by" id="v_order_by_review" onchange="dashboardReviewOrder()">
                                          <option value="DESC" selected>Latest First</option>
                                          <option value="ASC">Oldest First</option>
                                      </select>
                                      {{-- <select id="width_tmp_select1">
                                          <option id="width_tmp_option1"></option>
                                      </select>
                                      <div class="Filter-img">
                                          <img src="{{url('public/Assets/frontend/images/select.png')}}" alt="" />
                                      </div> --}}
                                  </div>
                              </div>
                          </div>
                      </div>

                       @php
                          $totalcommunication=0;
                          $totalrecommend=0;
                          $totaldescribed=0;

                          $totalcommunicationavg=0;
                          $totalrecommendavg=0;
                          $totaldescribedavg=0;
                          
                          $totalavg=0;
                          if(count($buyerreview)){

                              foreach($buyerreview as $k=>$v){
                                  $totalcommunication = $totalcommunication+$v->i_communication_star;
                                  $totalrecommend = $totalrecommend+$v->i_recommend_star;
                                  $totaldescribed = $totaldescribed+$v->i_described_star;
                              }
                              
                              $totalcommunicationavg = $totalcommunication/count($buyerreview);
                              $totalrecommendavg = $totalrecommend/count($buyerreview);
                              $totaldescribedavg = $totaldescribed/count($buyerreview);
                              $totalavg = $totalcommunication+$totalrecommend+$totaldescribed;
                              $totalavg = $totalavg/3;
                          }
                      ?>

                      <div class="row">
                         <div class="col-sm-4 text-center">
                            Seller Communication
                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalcommunicationavg}}" readonly >
                         </div>
                         <div class="col-sm-4 text-center">
                            Service As Described
                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totaldescribedavg}}" readonly >
                         </div>
                          <div class="col-sm-4 text-center">
                            Would Recommend
                            <input type="text" name="f_rate_appearance" id="star-input-1" class="rating" title="" value="{{$totalrecommendavg}}" readonly >
                         </div>
                      </div>
                  
                  </div>



                <div id="jobreviewdata">
                </div>    
                <input type="hidden" name="reviewpage" id="reviewpage" value="1" autocomplete="off" />
                <div id="showmorebtn">
                <div class="load-profile">
                    <button class="btn btn-load" onclick="jobReview();">
                        Load More Reviews (<span>{{count($buyerreview)}}</span>)
                    </button>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End 26B-control-panel-your-buyer-reviews-1 -->
@stop

@section('js')
     <script type="text/javascript">
      function dashboardReviewOrder(){
            
            var v_order_by = $("#v_order_by_review").val();
            
            if(v_order_by=="ASC"){
                var $divs = $("div.review-first");
                var numericallyOrderedDivs = $divs.sort(function (a, b) {
                    return $(a).find("h2").text() > $(b).find("h2").text();
                });
                $("#mainorderreview").html(numericallyOrderedDivs);
            }else{
                var $divs = $("div.review-first");
                var numericallyOrderedDivs = $divs.sort(function (a, b) {
                    return $(a).find("h2").text() < $(b).find("h2").text();
                });
                $("#mainorderreview").html(numericallyOrderedDivs);
            }
        }
        
    </script>
    
    <script type="text/javascript">
        
        function jobReview(){

          var page = $("#reviewpage").val();
          var actionurl = "{{url('seller/user-review')}}";
          var formdata = "&page="+page; 
          page=parseInt(page)+1;
          $("#reviewpage").val(page)
          document.getElementById('load').style.visibility="visible"; 
          
          $.ajax({
              type    : "GET",
              url     : actionurl,
              data    : formdata,
              success : function( res ){
                var obj = jQuery.parseJSON(res);
                document.getElementById('load').style.visibility='hidden';
                if(obj['status']==1){
                    $("#jobreviewdata").append(obj['responsestr']);
                    if(obj['btnstatus']==0){
                        $("#showmorebtn").html("");
                    }
                }    
                else{
                    $("#commonmsg").html(obj['msg']);
                }
              },
              error: function ( jqXHR, exception ) {
              }
          });

    }
      jobReview();
    </script>     
@stop

