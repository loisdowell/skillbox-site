      <link rel="stylesheet" href="{{url('public/Assets/frontend/star-rating/css/star-rating.css')}}" type="text/css" media="all">
      <script src="{{url('public/Assets/frontend/star-rating/js/star-rating.js')}}"></script> 
      <div id="mainorderreview">
      @if(isset($buyerreview) && count($buyerreview))
        @foreach($buyerreview as $k=>$v)
           
            @php
                $cnt = $k++;
                $v_image="";
                $v_fromimage="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
                    $v_image=$v->hasUser()->v_image;
                    $v_fromimage = \Storage::cloud()->url($v_image);
                }

                $v_fname="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_fname)){
                    $v_fname=$v->hasUser()->v_fname;
                }

                $v_lname="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_lname)){
                    $v_lname=$v->hasUser()->v_lname;
                }

                $totalavg=0;
                $totalavg = ($v['i_communication_star']+$v['i_described_star']+$v['i_recommend_star'])/3;
                
            @endphp

            <div class="buyer-first review-first">
                <h2 style="display: none;">{{strtotime($v->d_added)}}</h2>
                <div class="buyer-rating">
                    <div class="buyer-name pull-left">
                        <img src="{{$v_fromimage}}" alt="">
                    </div>
                    <div class="buyer-tital pull-left">
                    <h3>{{$v_fname}} {{$v_lname}}</h3>
                    <div class="rating-buyer">
                        <input type="text" name="f_rate_appearance" id="star-input-4" class="rating" title="" value="{{$totalavg}}" readonly>
                        <span class="star-no">{{number_format($totalavg,1)}}</span>
                    </div>
                    <div class="text-buyer" style="margin-top: 6px">
                        <p>
                          @php
                            if(isset($v->l_comment)){
                              echo $v->l_comment;
                            }
                          @endphp
                        </p>
                    </div>

                    </div>
                    {{-- <div class="clearfix"></div> --}}
                    
                </div>
            </div>
           @endforeach
           
           @else
           <div class="buyer-first review-first">
                <h2 style="display: none;">1</h2>
                <div class="buyer-profile">
                    <div class="buyer-rating">
                        <center><h3>No review found.</h3></center>
                    </div>
                </div>
                
            </div>
      @endif   
      </div>

      <script type="text/javascript">
        var src1 = "{{url('public/Assets/frontend/images/star.png')}}";
        var src2 = "{{url('public/Assets/frontend/images/star1.png')}}";
        $("#star-input-1,#star-input-2,#star-input-3,#star-input-4,#star-input-5").rating({
            min: 0,
            max: 5,
            step: 0.5,
            showClear: false,
            showCaption: false,
            theme: 'krajee-fa',
            filledStar: '<i class="fa"><img src="'+src1+'"></i>',
            emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
        });

        $("#f_rate_instruction,#f_rate_navigate,#f_rate_reccommend").rating({
            min: 0,
            max: 5,
            step: 0.5,
            showClear: false,
            showCaption: false,
            theme: 'krajee-fa',
            filledStar: '<i class="fa"><img src="'+src1+'"></i>',
            emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
        });
     </script>
