    
@if(isset($data) && count($data))
<div class="col-xs-12">
@if(isset($data['general']) && count($data['general']))
<div class="heading-title" id="general">
    <h1 class="title-faq" style="color: #000"> General Questions </h1>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach($data['general'] as $key => $val)
            <div class="panel panel-default panel-nonestyle" id="heading{{$val->id or ''}}">
                <div class="panel-heading panel-heading-css" role="tab" id="heading1{{$val->id or ''}}">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#{{$val->id or ''}}" aria-expanded="false" aria-controls="collapseOne">
                            {{$val->v_question or ''}}
                        </a>
                    </h4>
                </div>
                <div id="{{$val->id or ''}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$val->id or ''}}">
                    <div class="panel-body">
                        <p style="font-size: 18px">
                        @php
                            $ans = str_replace("\r\n\r\n", "<br><span style='height: 30px;display: inline-block;'></span>", $val->l_answer);
                        @endphp    
                        <?php echo nl2br($ans); ?>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endif
@if(isset($data['seller']) && count($data['seller']))
<div class="heading-title" id="seller">
    <h1 class="title-faq" style="color: #000"> Seller Questions </h1>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach($data['seller'] as $key => $val)
            <div class="panel panel-default panel-nonestyle" id="heading{{$val->id or ''}}">
                <div class="panel-heading panel-heading-css" role="tab" id="heading1{{$val->id or ''}}">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#{{$val->id or ''}}" aria-expanded="false" aria-controls="collapseOne">
                            {{$val->v_question or ''}}
                        </a>
                    </h4>
                </div>
                <div id="{{$val->id or ''}}" class="panel-collapse collapse @if(isset($_REQUEST['id']) && $_REQUEST['id'] == $val->id) in @endif" role="tabpanel" aria-labelledby="heading{{$val->id or ''}}">
                    <div class="panel-body">
                        <p style="font-size: 18px">
                        @php
                            $ans = str_replace("\r\n\r\n", "<br><span style='height: 30px;display: inline-block;'></span>", $val->l_answer);
                        @endphp    
                        <?php echo nl2br($ans); ?>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endif
@if(isset($data['buyer']) && count($data['buyer']))
<div class="heading-title" id="buyer">
    <h1 class="title-faq" style="color: #000"> Buyer Questions </h1>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach($data['buyer'] as $key => $val)
            <div class="panel panel-default panel-nonestyle">
                <div class="panel-heading panel-heading-css" role="tab" id="heading{{$val->id or ''}}">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#{{$val->id or ''}}" aria-expanded="false" aria-controls="collapseOne">
                            {{$val->v_question or ''}}
                        </a>
                    </h4>
                </div>
                <div id="{{$val->id or ''}}" class="panel-collapse collapse @if(isset($_REQUEST['id']) && $_REQUEST['id'] == $val->id) in @endif" role="tabpanel" aria-labelledby="heading{{$val->id or ''}}">
                    <div class="panel-body">
                        <p style="font-size: 18px">
                        @php
                            $ans = str_replace("\r\n\r\n", "<br><span style='height: 30px;display: inline-block;'></span>", $val->l_answer);
                        @endphp    
                        <?php echo nl2br($ans); ?>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endif
</div>
@else
<h2 class="text-center title-faq"> No result found. </h2>
@endif
        


