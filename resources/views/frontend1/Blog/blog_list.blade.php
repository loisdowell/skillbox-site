@extends('layouts.frontend')

@section('content')
  
   <!--13B-blog-1-->

    <div class="container-fluid">
        <div class="row">
            <div class="header-img">
                @php
                    $v_banner_image="";
                    if($metaDetails['v_image'] && $metaDetails['v_image']!=""){
                        if(Storage::disk('s3')->exists($metaDetails['v_image'])){
                            $v_banner_image = \Storage::cloud()->url($metaDetails['v_image']);      
                        }else{
                            $v_banner_image = url('public/Assets/frontend/images/blog-img.png');      
                        }                
                    }else{
                        $v_banner_image = url('public/Assets/frontend/images/blog-img.png');      
                    }
                @endphp
                <img src="{{$v_banner_image}}" class="img-responsive" alt="" />
                <div class="center-containt">
                    <div class="container">
                        <div class="blog-page">
                             @if($metaDetails['v_title'] && $metaDetails['v_title']!="")
                            <h1>{{$metaDetails['v_title']}}</h1>
                            @else
                            <h1> SKILLBOX BLOG </h1>
                            @endif

                            @if($metaDetails['v_sub_headline'] && $metaDetails['v_sub_headline']!="")
                            <p style="margin-top: 25px;">{{$metaDetails['v_sub_headline']}}</p>
                            @else
                            <p style="margin-top: 25px;">Follow our blog and be the first in touch with our latest articles!</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="main-blogpage">
                    
                    @if(count($blog))
                        @foreach($blog as $k=>$v)

                            <div class="b-blog">
                                <div class="img-withright-discription">
                                    <div class="row">
                                        <div class=" col-sm-4 col-xs-12 ">
                                            <div class="left-containt">
                                            @php  $imagepath=""; @endphp
                                            @if(isset($v->v_image) && $v->v_image != '' )

                                                @php
                                                    $imgdata="";
                                                    if(Storage::disk('s3')->exists($v->v_image)){
                                                        $imgdata = \Storage::cloud()->url($v->v_image);      
                                                    }
                                                 @endphp 
                                                <a href="{{url('blog')}}/{{$v->v_slug}}">
                                                <img src="{{$imgdata}}" class="img-responsive imgmedia-blog " alt="" />
                                                </a>
                                             @else
                                                 @php
                                                    $imgdata = \Storage::cloud()->url('common/no-image.png');      
                                                 @endphp 
                                                <a href="{{url('blog')}}/{{$v->v_slug}}">
                                                <img src="{{$imgdata}}" class="img-responsive imgmedia-blog " alt="" />  
                                                </a>
                                              @endif
                                  
                                            </div>
                                        </div>

                                        @php
                                            $publishdate = $v->d_added;
                                            if(isset($v->d_publish_date) && $v->d_publish_date!=""){
                                                $publishdate=$v->d_publish_date;
                                            }    
                                        @endphp


                                        <div class=" col-sm-8 col-xs-12 ">
                                            <div class="body-containt">
                                                <span class="date">{{date("M d, Y",strtotime($publishdate))}}</span>
                                                <span class="author"> by {{isset($v->v_author_name)?$v->v_author_name:""}} </span>
                                                <a href="{{url('blog')}}/{{$v->v_slug}}">
                                                <div class="title-containt">
                                                    <h5>{{isset($v->v_name)?$v->v_name:""}}</h5>
                                                </div>
                                                </a>
                                                <div class="body-msg" style="font-size: 16px">
                                                    {{isset($v->l_short_description)?$v->l_short_description:""}}
                                                </div>
                                                <a href="{{url('blog')}}/{{$v->v_slug}}">
                                                <button type="button" class="btn btn-readmore">Read more </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    @endif        
                 
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="center">
                                {{ $blog->links() }}
                            </div>
                        </div>
                    </div>            

               
                </div>
            </div>
        </div>
    </div>
    <!--end-13B-blog-1-->
  
@stop
@section('js')

<script type="text/javascript">

      function buyerSignup(){
          var formValidFalg = $("#buyersignupform").parsley().validate('');
          if(formValidFalg){
              var l = Ladda.create(document.getElementById('bsignup'));
              l.start();
              $("#buyersignupform").submit()
          } 
      }

</script>

@stop

