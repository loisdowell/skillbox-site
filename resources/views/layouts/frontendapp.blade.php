<!DOCTYPE html>
<html>
<head>
  @php
    if( !isset($metaDetails['v_meta_title']) ) $metaDetails['v_meta_title'] = 'Skillbox';
    if( !isset($metaDetails['v_meta_keywords']) ) $metaDetails['v_meta_keywords'] = 'Skillbox';
    if( !isset($metaDetails['l_meta_description']) ) $metaDetails['l_meta_description'] = 'Skillbox';
  @endphp
 
 <title>@yield('title', 'Skillbox') - Skillbox</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="title" content="@yield('meta-title', $metaDetails['v_meta_title'] )">
  <meta name="keywords" content="@yield('meta-keyword', $metaDetails['v_meta_keywords'] )">
  <meta name="description" content="@yield('meta-description', $metaDetails['l_meta_description'] )">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="index, follow">
  
  <link rel="shortcut icon" type="image/png" href="{{url('public/Assets/frontend/images/favicon.ico')}}"/>
  <link rel="stylesheet" href="{{url('public/Assets/frontend/css/font-awesome.min.css')}}" type="text/css" media="all">
  <link rel="stylesheet" href="{{url('public/Assets/frontend/css/bootsrap.min.css')}}" type="text/css" media="all">

  {{-- <link rel="stylesheet" href="{{url('public/Assets/frontend/css/style.css')}}" type="text/css" media="all"> --}}
  {{-- <link rel="stylesheet" href="{{url('public/Assets/frontend/css/slick.css')}}" type="text/css" media="all"> --}}
  {{-- <link rel="stylesheet" href="{{url('public/Assets/frontend/css/slick-theme.css')}}" type="text/css" media="all"> --}}
  {{-- <link rel="stylesheet" href="{{url('public/Assets/frontend/star-rating/css/star-rating.css')}}" type="text/css" media="all"> --}}

  <link rel="stylesheet" href="{{url('public/Assets/frontend/css/styleopt.css')}}" type="text/css" media="all">
  <link rel="stylesheet" href="{{url('public/Assets/frontend/css/material-design-iconic-font.css')}}" type="text/css" media="all">
  <link rel="stylesheet" href="{{url('public/Assets/plugins/ladda/ladda-themeless.min.css')}}">
  <link rel="stylesheet" href="{{url('public/Assets/frontend/css/bootstrap-datetimepicker.css')}}" type="text/css" media="all">

  {{-- <link rel="stylesheet" href="{{url('public/Assets/frontend/css/bootstrap.css')}}" type="text/css" media="all"> --}}
  {{-- <link rel="stylesheet" href="{{url('public/Assets/plugins/select2/select2.min.css')}}" type="text/css" media="all"> --}}
  {{-- <link rel="stylesheet" href="{{url('public/Assets/frontend/css/stylereponsive.css')}}" type="text/css" media="all"> --}}
  {{-- <link rel="stylesheet" href="{{url('public/Assets/frontend/css/style-alex.css')}}" type="text/css" media="all"> --}}

  <script src="{{url('public/Assets/frontend/js/jquery-3.2.0.min.js')}}"></script>
  <script src="{{url('public/Assets/frontend/js/bootstrap.min.js')}}" ></script>
  
  {{-- <style type="text/css">
    #load {
      width: 100%;
      height: 100%;
      position: fixed;
      z-index: 9999;
      visibility: hidden;
      background: url("{{url('public/Assets/frontend/images/skill-box.svg')}}") no-repeat center center rgba(0,0,0,0.25);
    }
   
    .browser-cust{    display: inline-flex;
        padding: 27px 10px;
        border: 0px;
        margin-left: 10px;
        font-size: 18px;
        letter-spacing: 1px;
        background: transparent;
    }
    .browser-cust:hover {background: #e70e8a; color: #fff;}
    .browser-cust a{
        color: #333;
    }
    .browser-cust:hover a{
        color: #fff;
    }
  </style> --}}

  @yield('css')
</head>

<body class="hold-transition skin-blue sidebar-mini modal-new">
	<div class="load" id="load"></div>
    
    @if(isset($_REQUEST['preview']) && $_REQUEST['preview']=="profile")
      <div class="msg-alert-preview">
          <div class="container">
              <div class="email-msg-preview">
                  <p><strong>Profile Preview Mode</strong></p>
                  <p>You are viewing your profile in a preview mode. If your wish to view your profile live, simply logout from your account and search for your skill.</p>
              </div>
          </div>
      </div>
     @endif 

     @if(isset($_REQUEST['preview']) && $_REQUEST['preview']=="job")
      <div class="msg-alert-preview">
          <div class="container">
              <div class="email-msg-preview">
                  <p><strong>Job Preview Mode</strong></p>
                  <p>You are viewing your job in a preview mode. If your wish to view your job live, simply logout from your account and search for your job posting.</p>
              </div>
          </div>
      </div>
     @endif 


    @if(auth()->guard('web')->check() && auth()->guard('web')->user()->e_email_confirm=="no")
      <div class="msg-alert">
          <div class="container">
              <div class="email-msg">
                  Welcome to Skillbox! Please check your email to confirm your account. <a href="javascript:;" onclick="resendEmail();"  id="cnemailid">Resend Email</a>
              </div>
          </div>
      </div>
    @endif

    @if(isset(auth()->guard('web')->user()->e_email_confirm_message) && auth()->guard('web')->user()->e_email_confirm_message=="yes")
    <div class="msg-alert">
          <div class="container">
              <div class="email-msg">
                  Welcome to Skillbox! You have successfully verified your account.
              </div>
          </div>
    </div>
    @endif


  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title custom-font">Confirm Delete Record ?</h3>
          </div>
          <div class="modal-body" style="text-align:center">
          <i class="margin-top-10 fa fa-question fa-5x"></i>
          <h4>Delete !</h4>
          <p>Are you sure you want to delete this record?</p>
          </div>
          <input type="hidden" name="deleteId" id='deleteId'>
          <input type="hidden" name="deleteSlug" id='deleteSlug'>
          <div class="modal-footer" style="padding-top: 20px;">
            <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" onclick="deleteAction()"><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
            <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
          </div>
        </div>
      </div>
    </div>
	@yield('model')
	<div class="wrapper">
		
		@include('frontend.inc.headerapp')
   	    @yield('content')
    
	</div>

@yield('js')
<script type="text/javascript">
  var base_path = "{{url('/')}}";
</script>

<script src="{{url('public/Assets/frontend/js/slick.min.js')}}" ></script>
{{-- <script src="{{url('public/Assets/frontend/js/circle-progress.js')}}" ></script> --}}
<script src="{{url('public/Assets/frontend/js/circle-progress.min.js')}}" ></script>
<script src="{{url('public/Assets/frontend/js/bootstrap-datetimepicker.min.js')}}" ></script>
<script src="{{url('public/Assets/plugins/ckeditor1/ckeditor.js')}}" ></script>
<script src="{{url('public/Assets/frontend/star-rating/js/star-rating.min.js')}}" ></script> 
<script src="{{url('public/Assets/plugins/parsley/parsley.min.js')}}" ></script>
<script src="{{url('public/Assets/plugins/ladda/spin.min.js')}}" ></script> 
<script src="{{url('public/Assets/plugins/ladda/ladda.min.js')}}" ></script>
<script src="{{url('public/Assets/frontend/js/jquery.scrollbar.min.js')}}" ></script>

{{-- <script src="{{url('public/Assets/frontend/js/scrollbar.js')}}" ></script> --}}
{{-- <script src="{{url('public/Assets/frontend/star-rating/js/star-rating.js')}}" ></script>  --}}
{{-- <script src="{{url('public/Assets/plugins/select2/select2.min.js')}}" ></script> --}}


<script type="text/javascript">

$('.responsive').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 991,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});


var progressBarOptions = {
    startAngle: -1.55,
    size: 180,
    value: 0.75,
    fill: {
        color: '#e70e8a'
    }
}

$('.circle').circleProgress(progressBarOptions).on('circle-animation-progress', function(event, progress, stepValue) {
    //$(this).find('strong').text(String(stepValue.toFixed(2)).substr(1));
});
$('#circle-a').circleProgress({
    value: 0.25,
    fill: {
        color: '#e70e8a'
    }
});

$('#circle-b').circleProgress({
    value: 0.50,
    fill: {
        color: '#e70e8a'
    }
});

$('#circle-c').circleProgress({
    value: 0.75,
    fill: {
        color: '#e70e8a'
    }
});

$('#circle-d').circleProgress({
    value: 1.0,
    fill: {
        color: '#e70e8a'
    }
});


var src1 = base_path+"/public/Assets/frontend/images/star.png";
var src2 = base_path+"/public/Assets/frontend/images/star1.png";
   
$("#star-input-1,#star-input-2,#star-input-3,#star-input-4,#star-input-5").rating({
    min: 0,
    max: 5,
    step: 0.5,
    showClear: false,
    showCaption: false,
    theme: 'krajee-fa',
    filledStar: '<i class="fa"><img src="'+src1+'"></i>',
    emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
});

$("#f_rate_instruction,#f_rate_navigate,#f_rate_reccommend").rating({
    min: 0,
    max: 5,
    step: 0.5,
    showClear: false,
    showCaption: false,
    theme: 'krajee-fa',
    filledStar: '<i class="fa"><img src="'+src1+'"></i>',
    emptyStar: '<i class="fa"><img src="'+src2+'"></i>'
});


$(document).ready(function(){

  $(".d-arrow").click(function(){
      $(".my-live").css('display','block');
      $(".option-new").css('display','block');
  });
  $(".option-new").click(function(){
      $(".option-new").hide();
  });
});




function masterchange(data){
    
    var txt = '<input type="text" name="text" id="autocomplete_head" class="form-control manu-map" placeholder="Location"';
     txt += 'oninvalid="this.setCustomValidity';
     txt += "('Enter a location to see results close by')";
     txt += '" oninput="setCustomValidity()">';
     txt+='<input type="hidden" name="v_latitude" id="v_latitude_head">';
     txt+='<input type="hidden" name="v_longitude" id="v_longitude_head">';

  if(data=="online"){
    
    $("#mastersearchlocation").html("");
    $(".new").css("width","419px");
    $(".input-new").css("width","319px");
    $("#mastersearchtext").attr("placeholder", "Online");
    $("#mastersearchtext").attr("onkeyup", "onlinesearchtop(this.value)");
    $("#e_type").attr("value", "online");
    $(".searchdataclass").addClass("onlinecourseclass");
    


  }else if(data=="inperson"){
    
    $(".new").css("width","258px");
    $(".input-new").css("width","79%");
    $("#mastersearchlocation").html(txt);
    $("#mastersearchtext").attr("placeholder", "In Person");
    $("#mastersearchtext").attr("onkeyup", "inpersonssearchtop(this.value)");

    $("#e_type").attr("value", "inperson");
    $(".searchdataclass").removeClass("onlinecourseclass");
    initAutocomplete();
  
  }else if(data=="courses"){

    $(".new").css("width","419px");
    $(".input-new").css("width","319px");
    $("#mastersearchlocation").html("");
    $("#mastersearchtext").attr("placeholder", "Courses");
    $("#mastersearchtext").attr("onkeyup", "coursesearchtop(this.value)");

    $("#e_type").attr("value", "courses");
    $(".searchdataclass").addClass("onlinecourseclass");
 
  }

}


function confirmDelete( id , slug ) {
  $('#deleteId').val(id);
  $('#deleteSlug').val(slug);
  $('#deleteModal').modal('show');
}

function deleteAction() {
  var deleteId= $('#deleteId').val();
  var deleteSlug= $('#deleteSlug').val();
  window.location.href = deleteSlug;
}

function resendEmail(){
    var actionurl = base_path+"/account/resend-email";
    var formData = "";
    document.getElementById('load').style.visibility="visible"; 
    $.ajax({
        
        processData: false,
        contentType: false,
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            var obj = jQuery.parseJSON(res);
            document.getElementById('load').style.visibility='hidden';
            if(obj['status']==1){
                $("#cnemailid").html("Email sent.");  
            }
        },
        error: function ( jqXHR, exception ) {
            $("#commonerrmsg").css("display","inline");
        }
    });

}
    
function confirmMessage(){
    var actionurl = base_path+"/account/email-confirm-message";
    var formData = "";
    document.getElementById('load').style.visibility="visible"; 
    $.ajax({
        processData: false,
        contentType: false,
        type    : "GET",
        url     : actionurl,
        data    : formData,
        success : function( res ){
            document.getElementById('load').style.visibility='hidden';
        },
        error: function ( jqXHR, exception ) {
            document.getElementById('load').style.visibility='hidden';
        }
    });

}
   



function footerAlign() {
  $('footer').css('display', 'block');
  $('footer').css('height', 'auto');
  var footerHeight = $('footer').outerHeight();
  $('body').css('padding-bottom', footerHeight);
  $('footer').css('height', footerHeight);
}


$(document).ready(function(){
  footerAlign();
});

$( window ).resize(function() {
  footerAlign();
});

$(document).ready( function() {
    /* Check width on page load*/
    if ( $(window).width() < 1199) {
        function matchHeights() {
          $('.matchHeights').each(function() {
            var elements = $(this).find('.matchheight_div');
            var height = 0;
            elements.css('min-height','0px');
            elements.each(function() {
              height = $(this).height() > height ? $(this).height() : height;
            });
            elements.css('min-height',height+'px');
          });
        };

        $(document).ready(function() {
          matchHeights();
        });

        $(window).resize(function() {
          matchHeights();
        });
    }
    else {}
 });

 function height_container() {
  $('.height_container').each(function() {
    var elements = $(this).find('.height_div');
    var height = 0;
    elements.css('min-height','0px');
    elements.each(function() {
      height = $(this).height() > height ? $(this).height() : height;
    });
    elements.css('min-height',height+'px');
  });
};


$(document).ready( function() {
    if ( $(window).width() > 768) {


       

        height_container();
        setInterval(function(){        
            height_container();
        },1000);


        // $(document).ready(function() {
        //   height_container();
        // setInterval(function(){        
        //     height_container();
        //   },1000);
        // });

        $(window).resize(function() {
          height_container();
          setInterval(function(){
            height_container();
          },1000);
        });
    }
    else {}
 });

$(document).ready( function() {
    if ( $(window).width() > 768) {
        function height_container_text() {
          $('.height_container_text').each(function() {
            var divHeight = $(".height_text").height();
            $(".height_img").css( "height", divHeight +"px");
          });
        };

        $(document).ready(function() {
          height_container_text();
      setInterval(function(){        
            height_container_text();
          },1000);
        });

        $(window).resize(function() {
          height_container();
          setInterval(function(){
            height_container();
          },1000);
        });
    }
    else {}
 });



(function( jQuery, undefined ) {

    jQuery.fn.extend({
        customScroll: function() {

            // measure the default scrollbar width.
            var tdv = jQuery( "<div><div></div></div>" )
                .css({
                    position: "absolute",
                    left: -1000,
                    width: 300,
                    overflow: "scroll"
                })
                .appendTo( "body" ),
                twi = tdv.width() - tdv.find( "div" ).width();
            tdv.remove();

            return this.each(function() {

                // decorate the element with a scrollbar
                var that = jQuery( this ),
                    he = that.outerHeight(),
                    wi = that.outerWidth();

                var sbp = that.css("direction") === "rtl" ? "left" : "right",
                    sbf = that.css("direction") === "rtl" ? "right" : "left",
                    scroller = jQuery( "<div>" )
                        .addClass( "phancy-scroller" )
                        .css({
                            overflow: "hidden",
                            position: "relative",
                            height: he,
                            width: wi,
                            marginTop: that.css( "margin-top" ),
                            marginBottom: that.css( "margin-bottom" ),
                            marginLeft: that.css( "margin-left" ),
                            marginRight: that.css( "margin-right" ),
                            float: that.css( "float" ),
                        }),
                    scrollarea = jQuery( "<div>" )
                        .css({
                            overflow: "scroll",
                            position: "relative",
                            height: he + twi,
                            width: wi + twi
                        })
                        .appendTo( scroller ),
                    scrollareax = jQuery( "<div>" )
                        .css({ float: sbf })
                        .appendTo( scrollarea );

                that
                    .css({
                        overflow: "visible",
                        height: "auto",
                        margin: 0,
                        float: ""
                    })
                    .after( scroller )
                    .appendTo( scrollareax );

                var nhe = scrollareax.outerHeight( true ),
                    ratio = Math.min( 1, he / nhe );
                if ( ratio >= 1 ) {
                    return;
                }

                // create scrollbars
                var scrollbar = jQuery( "<div>" )
                        .addClass( "phancy-scrollbar" )
                        .css({ height: he })
                        .css( sbp, 0 )
                        .appendTo( scroller ),

                    scrollbarbutton = jQuery( "<div>" )
                        .addClass( "phancy-scrollbarbutton" )
                        .css({ height: he * ratio })
                        .css( sbp, 0 )
                        .appendTo( scrollbar );
                that.css({ width: "-=" + scrollbar.css("width") });

                // bind events
                scroller.scroll(function() {
                    scroller.scrollLeft( 0 ).scrollTop( 0 ); /* http://stackoverflow.com/q/10036044 */
                });
                scrollarea.scroll(function() {
                    scrollbarbutton.css({
                        top: scrollarea.scrollTop() * ratio,
                        height: he * ratio
                    });
                });

                (function() {
                    var dragging = false,
                        pageY = null,
                        pageX = null,
                        top = null,
                        timer = null;

                    // scroll by clicking on scrollbar itself (page up and down).
                    scrollbar.on( "mousedown", function( e ) {
                        if ( e.which !== 1 || jQuery( e.target ).hasClass( "scrollbarbutton" ) )
                        {
                            return;
                        }
                        top = parseInt( scrollbarbutton.css( "top" ), 10  ) + ( he * ratio * ( e.pageY > scrollbarbutton.offset().top ? 1 : -1 ));
                        clearTimeout( timer );
                        timer = setTimeout(function() {
                            top = Math.min( Math.max( 0, e.pageY - scrollbar.offset().top ) - he * ratio / 2, he - ( he * ratio ) );
                            scrollbarbutton.css({ top: top });
                            scrollarea.scrollTop( Math.round( top / ratio ) );
                        }, 300);
                        scrollbarbutton.css({ top: top });
                        scrollarea.scrollTop( Math.round( top / ratio ) );
                        return false;
                    });

                    scrollbar.on("mouseup", function() {
                        clearTimeout( timer );
                    });

                    // scroll by clicking on scrollbar button (dragging).
                    scrollbarbutton.on("mousedown", function( e ) {
                        if ( e.which !== 1 )
                        {
                            return;
                        }
                        dragging = true;
                        pageY = e.pageY;
                        pageX = e.pageX;
                        top = parseInt( scrollbarbutton.css( "top" ), 10 );
                        jQuery( document ).on( "mousemove", function( e ) {
                            if ( dragging ) {
                                if ( Math.abs( e.pageX - pageX ) < 50 ) {
                                    var newtop = Math.min( Math.max( 0, top + e.pageY - pageY ), he - he * ratio );
                                    scrollbarbutton.css( "top", newtop );
                                    scrollarea.scrollTop( Math.round( newtop / ratio ) );
                                }
                                else {
                                    scrollarea.scrollTop( Math.round( top / ratio ) );
                                    scrollbarbutton.css({ top: top });
                                }
                                return false;
                            }
                            else {
                                jQuery( document ).unbind( "mousemove" );
                            }
                        });
                        return false;
                    });

                    jQuery( document ).on( "mouseup", function() {
                        if ( dragging ) {
                            dragging = false;
                            jQuery( document ).unbind( "mousemove" );
                            return false;
                        }
                    });
                })();
            });
        }
    });

})( jQuery );

function categoryviewdata(){
    $('html, body').animate({
        scrollTop: $("#skillcategory").offset().top
    }, 1000);
}


@if(Session::has('e_type'))
    masterchange('{{Session::get('e_type')}}');
@endif

@if(auth()->guard('web')->check())
confirmMessage();
@endif

function categoryviewdatahref(){
    window.location = "{{url('/')}}#skillcategory";
}

var csrfToken = $('[name="_token"]').attr('content');
function refreshToken(){
    $.get("{{url('refresh-csrf')}}").done(function(data){
        csrfToken = data; // the new token
        $('[name="_token"]').val(data);
    });
}
setInterval(refreshToken, 120000); // 2 Min //120000

  
var placeSearch, autocomplete;
var componentForm = {
    premise: 'long_name',
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'long_name',
    country: 'long_name',
    postal_code: 'short_name'
};


function initAutocomplete() {
    autocomplete = new google.maps.places.Autocomplete(
    (document.getElementById('autocomplete')),
    {types: ['geocode'],componentRestrictions: {country: "uk"} });
    autocomplete.addListener('place_changed', fillInAddress);

    autocomplete2 = new google.maps.places.Autocomplete(
    (document.getElementById('autocomplete_head')),
    {types: ['geocode'],componentRestrictions: {country: "uk"}});
    autocomplete2.addListener('place_changed', fillInAddress2);
}

//

function fillInAddress2() {

    var place = autocomplete2.getPlace();
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    $("#v_latitude_head").val(lat);
    $("#v_longitude_head").val(lng);

    var premise = '';
    var street_number = '';
    var route = '';

    for ( var i = 0; i < place.address_components.length; i++ ) {

        var addressType = place.address_components[i].types[0];

        if ( componentForm[addressType] && addressType == 'premise') {
            premise = place.address_components[i][componentForm[addressType]];
        }
        if ( componentForm[addressType] && addressType == 'street_number') {
            street_number = place.address_components[i][componentForm[addressType]];
        }
        if ( componentForm[addressType] && addressType == 'route') {
            route = place.address_components[i][componentForm[addressType]];
        }
        else if ( componentForm[addressType] && addressType == 'postal_code') {
            $('#v_pincode').val( place.address_components[i][componentForm[addressType]] );
        }
    }
    var address = '';
    
    if( premise != '' ) {
        address += ' ' + premise;
    }
    else {
        address += premise;
    }

    if( street_number != '' ) {
        address += ' ' + street_number;
    }
    else {
        address += street_number;
    }

    if( route != '' ) {
        address += ' ' + route;
    }
    else {
        address += route;
    }
}

function fillInAddress() {

    var place = autocomplete.getPlace();
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    $("#v_latitude").val(lat);
    $("#v_longitude").val(lng);

    var premise = '';
    var street_number = '';
    var route = '';

    for ( var i = 0; i < place.address_components.length; i++ ) {

        var addressType = place.address_components[i].types[0];

        if ( componentForm[addressType] && addressType == 'premise') {
            premise = place.address_components[i][componentForm[addressType]];
        }
        if ( componentForm[addressType] && addressType == 'street_number') {
            street_number = place.address_components[i][componentForm[addressType]];
        }
        if ( componentForm[addressType] && addressType == 'route') {
            route = place.address_components[i][componentForm[addressType]];
        }
        else if ( componentForm[addressType] && addressType == 'postal_code') {
            $('#v_pincode').val( place.address_components[i][componentForm[addressType]] );
        }
    }

    var address = '';

    if( premise != '' ) {
        address += ' ' + premise;
    }
    else {
        address += premise;
    }

    if( street_number != '' ) {
        address += ' ' + street_number;
    }
    else {
        address += street_number;
    }

    if( route != '' ) {
        address += ' ' + route;
    }
    else {
        address += route;
    }
}

@if(env('GOOGLE_ANALYTICS'))
    //alert("GOOGLE ANALYTICS CODE");
@endif


$(document).mouseup(function (e){
    
    var container = new Array();
    container.push($('#my-live-data'));
    $.each(container, function(key, value) {
        if (!$(value).is(e.target) && $(value).has(e.target).length === 0) 
        {
            $(".my-live").hide();
        }
    });
});


function inpersonssearchtop(data){
        
    var keystr = data.length;
    if(keystr>3){

        var actionurl = base_path+"/inperson-suggest";
        var formData = "keyword="+data;
        
        $.ajax({
            processData: false,
            contentType: false,
            type    : "GET",
            url     : actionurl,
            data    : formData,
            success : function( res ){
                var obj = jQuery.parseJSON(res);
                if(obj['status']==1){
                    $("#browserstop").html(obj['searchstr']);  
                }
            },
        });
    }
}


function coursesearchtop(data){
        
        var keystr = data.length;

        if(keystr>3){

            var actionurl = base_path+"/course-suggest";
            var formData = "keyword="+data;
            
            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#browserstop").html(obj['searchstr']);  
                    }
                },
            });
        }
    }


function onlinesearchtop(data){
        
        var keystr = data.length;

        if(keystr>3){

            var actionurl = base_path+"/online-suggest";
            var formData = "keyword="+data;
            
            $.ajax({
                processData: false,
                contentType: false,
                type    : "GET",
                url     : actionurl,
                data    : formData,
                success : function( res ){
                    var obj = jQuery.parseJSON(res);
                    if(obj['status']==1){
                        $("#browserstop").html(obj['searchstr']);  
                    }
                },
            });
        }
    }



</script>

</body>
</html>	
