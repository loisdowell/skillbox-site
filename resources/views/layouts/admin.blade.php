<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>@yield('title', 'Skillbox') - Skillbox</title>
		{{-- GLOBAL CSS --}}
		@include('inc.style')

		{{-- PAGE SPECIFIC CSS --}}
		@yield('css')

	<!-- <script type="text/javascript">
		$.fn.dataTable.ext.errMode = 'none';
	</script> -->	
	</head>

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">

		<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title custom-font">Confirm Delete Record ?</h3>
					</div>
					<div class="modal-body" style="text-align:center">
					<i class="margin-top-10 fa fa-question fa-5x"></i>
					<h4>Delete !</h4>
					<p>Are you sure you want to delete this record?</p>
					</div>
					<input type="hidden" name="deleteId" id='deleteId'>
					<input type="hidden" name="deleteSlug" id='deleteSlug'>
					<div class="modal-footer" style="padding-top: 20px;">
						<button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" onclick="deleteAction()"><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
						<button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
					</div>
				</div>
			</div>
		</div>

		@yield('model')

		<div class="page-wrapper">

			{{-- HEADER --}}
			@include('inc.header')

			{{-- CONTAINER --}}
			<div class="page-container">

				{{-- SIDEBAR --}}
				@include('inc.sidebar')

				{{-- CONTENT --}}
				<div class="page-content-wrapper">
					<div class="page-content">
						@yield('content')
					</div>
				</div>

			</div>

			{{-- FOOTER --}}
			@include('inc.footer')

		</div>

		{{-- GLOBAL SCRIPTS --}}
		@include('inc.scripts')

		{{-- PAGE SPECIFIC SCRIPTS --}}
		@yield('jspage')

		<script type="text/javascript">

			// Delete Model START

			function confirmDelete( id , slug ) {

				$('#deleteId').val(id);
				$('#deleteSlug').val(slug);
				$('#deleteModal').modal('show');
			}

			function deleteAction() {

				var deleteId= $('#deleteId').val();
				var deleteSlug= $('#deleteSlug').val();
				window.location.href = deleteSlug;
			}

			// Delete Model ENDS

		</script>

	</body>
</html>