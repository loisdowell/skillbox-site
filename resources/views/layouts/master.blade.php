<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title', 'Skillbox') - Skillbox</title>

  <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name=”viewport” content=”width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;” />
  <meta name=”viewport” content=”width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=false;” />
  <meta name=”viewport” content=”width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;” />
 
  <link rel="stylesheet" href="{{url('public/Assets/bootstrap/css/bootstrap.min.css')}}">

  <link rel="stylesheet" href="{{url('public/Assets/bootstrap/css/bootstrap-tagsinput.css')}}">

  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"> -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->

  <link rel="stylesheet" href="{{url('public/Assets/admin/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{url('public/Assets/admin/css/skins/_all-skins.min.css')}}">
  <link rel="stylesheet" href="{{url('public/Assets/plugins/iCheck/flat/blue.css')}}">
  <link rel="stylesheet" href="{{url('public/Assets/plugins/morris/morris.css')}}">
  <link rel="stylesheet" href="{{url('public/Assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
  <link rel="stylesheet" href="{{url('public/Assets/plugins/datepicker/datepicker3.css')}}">
  <link rel="stylesheet" href="{{url('public/Assets/plugins/daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{url('public/Assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <link href="{{url('/public/Assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{url('public/Assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}">
  <link rel="stylesheet" href="{{url('public/Assets/plugins/parsley/parsley.css')}}" type="text/css" media="all">



  
  @yield('css')

  <link rel="stylesheet" href="{{url('public/Assets/admin/css/font-awesome.css')}}">
  <link rel="stylesheet" href="{{url('public/Assets/admin/css/ionicons.css')}}">
  <style type="text/css">
    .asterisk_input{
      color: #e32;
    }
  </style>
  <script src="{{url('public/Assets/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	
  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title custom-font">Confirm Delete Record ?</h3>
          </div>
          <div class="modal-body" style="text-align:center">
          <i class="margin-top-10 fa fa-question fa-5x"></i>
          <h4>Delete !</h4>
          <p>Are you sure you want to delete this record?</p>
          </div>
          <input type="hidden" name="deleteId" id='deleteId'>
          <input type="hidden" name="deleteSlug" id='deleteSlug'>
          <div class="modal-footer" style="padding-top: 20px;">
            <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c br-50" onclick="deleteAction()"><i class="fa fa-long-arrow-right"></i>&nbsp;Delete</button>
            <button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c br-50" data-dismiss="modal"><i class="fa fa-long-arrow-left"></i>&nbsp;Cancel</button>
          </div>
        </div>
      </div>
    </div>
	@yield('model')
	<div class="wrapper">
		
		@include('admin.inc.header')
		
		@yield('content')
		
		@include('admin.inc.footer')
	</div>


<script src="{{url('public/Assets/admin/js/jquery-ui.min.js')}}"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{{url('public/Assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('public/Assets/bootstrap/js/bootstrap-tagsinput.js')}}"></script>
<script src="{{url('public/Assets/plugins/parsley/parsley.min.js')}}"></script>


<script src="{{url('public/Assets/plugins/raphael/raphael-min.js')}}"></script>
<script src="{{url('public/Assets/plugins/morris/morris.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{url('public/Assets/plugins/knob/jquery.knob.js')}}"></script>
<script src="{{url('public/Assets/plugins/raphael/moment.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{url('public/Assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{url('public/Assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{url('/public/Assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{url('public/Assets/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>

<script src="{{url('public/Assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{url('public/Assets/plugins/fastclick/fastclick.js')}}"></script>
<script src="{{url('public/Assets/admin/js/app.min.js')}}"></script>
<script src="{{url('public/Assets/admin/js/demo.js')}}"></script>
<script src="{{url('public/Assets/ckeditor/ckeditor.js')}}"></script> 
<script src="{{url('public/Assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>


@yield('js')


<script type="text/javascript">
     
      function confirmDelete( id , slug ) {
          $('#deleteId').val(id);
          $('#deleteSlug').val(slug);
          $('#deleteModal').modal('show');
      }
      
      function deleteAction() {
          var deleteId= $('#deleteId').val();
          var deleteSlug= $('#deleteSlug').val();
          window.location.href = deleteSlug;
      }

     $.fn.dataTable.ext.errMode = 'none';

</script>

</body>
</html>	
