<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// CORS --
// header('Access-Control-Allow-Origin: *');
// header('Content-Type: application/json');


Route::group([ 'prefix' => 'v1'], function () {

    Route::post('login', 'Api\V1\Login@index');
    Route::post('signupCheckEmail', 'Api\V1\Signup@checkEmail');
    Route::post('signup', 'Api\V1\Signup@index');
    Route::post('support/category', 'Api\V1\Options\Support@categoryList');
    Route::post('socail-login', 'Api\V1\SocailLogin@index');
    Route::post('forgot-password', 'Api\V1\Login@forgotPassword');

});

Route::group([ 'prefix' => 'v1','middleware' => ['apiauth'] ], function () {

    Route::post('dashboard', 'Api\V1\Options\Dashboard@index');
    Route::post('support', 'Api\V1\Options\Support@index');
    Route::post('support/add', 'Api\V1\Options\Support@addSupport');
    Route::post('logout', 'Api\V1\Login@logOut');
    Route::post('applied-jobs', 'Api\V1\Options\AppliedJobs@index');
    Route::post('applied-jobs/delete', 'Api\V1\Options\AppliedJobs@deleteAppliedJobs');

    Route::post('my-courses', 'Api\V1\Options\MyCourses@index');
    Route::post('my-courses/detail', 'Api\V1\Options\MyCourses@detail');
    Route::post('profile/detail', 'Api\V1\Options\Profile@index');
    Route::post('profile/update', 'Api\V1\Options\Profile@updateDetail');
    Route::post('profile/Image', 'Api\V1\Options\Profile@updateProfileImage');

    Route::get('message/list', 'Api\V1\Messages\Messages@index');
    Route::post('message/detail', 'Api\V1\Messages\Messages@detail');
    Route::post('message/send', 'Api\V1\Messages\Messages@sendMessage');

    Route::post('my-jobs', 'Api\V1\Jobs\MyJobs@index');
    Route::post('my-jobs/detail', 'Api\V1\Jobs\MyJobs@detail');
    Route::post('my-jobs/endjob', 'Api\V1\Jobs\MyJobs@endJob');
    Route::post('my-jobs/shortlist', 'Api\V1\Jobs\MyJobs@addToShortList');
    Route::post('my-jobs/apply', 'Api\V1\Jobs\MyJobs@applyJob');
    Route::post('my-jobs/send-message', 'Api\V1\Jobs\MyJobs@ContactSelller');



    Route::post('get-category', 'Api\V1\Jobs\Postjob@index');
    Route::post('get-category-skill', 'Api\V1\Jobs\Postjob@getCategorySkill');
    Route::post('post-job', 'Api\V1\Jobs\Postjob@postJob');

    Route::post('job/edit', 'Api\V1\Jobs\Postjob@edit');
    Route::post('job/update', 'Api\V1\Jobs\Postjob@updateJob');


    Route::post('my-money', 'Api\V1\Options\MyMoney@index');
    Route::get('shortlisted-skills', 'Api\V1\Options\ShortListSkill@index');
    Route::get('shortlisted-jobs', 'Api\V1\Options\ShortListJob@index');

    Route::post('shortlisted-jobs/delete', 'Api\V1\Options\ShortListJob@deleteShortlistedJob');
    
    Route::post('shortlisted-skills/delete', 'Api\V1\Options\ShortListSkill@deleteShortlistedSkill');

    Route::post('my-orders', 'Api\V1\Options\MyOrders@index');
    Route::post('my-orders-seller', 'Api\V1\Options\MyOrders@sellerOrder');

    Route::post('my-plan', 'Api\V1\Options\UpgradePlan@index');

    Route::post('my-review', 'Api\V1\Options\Reviews@index');
    Route::post('my-review-buyer', 'Api\V1\Options\Reviews@buyerReview');

    Route::post('account-cancel', 'Api\V1\Options\Dashboard@accountCancel');

    Route::post('search', 'Api\V1\Search\Search@index');
    Route::get('skill/detail', 'Api\V1\Search\Search@skillDetail');
    Route::get('skill/review', 'Api\V1\Search\Search@SkillReview');
    Route::post('skill/shortlist', 'Api\V1\Search\Search@skillShortlist');
    Route::post('skill/send-message', 'Api\V1\Search\Search@ContactSelller');

    Route::get('course/detail', 'Api\V1\Search\Search@courseDetail');
    Route::get('course/review', 'Api\V1\Search\Search@CourseReview');
    Route::post('course/shortlist', 'Api\V1\Search\Search@coursesShortlist');

});
