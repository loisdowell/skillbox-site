<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return view('welcome');
// });
## Block IP for security ##

Route::get('refresh-csrf', function () {
    return csrf_token();
});


	// Route::post('course/buy-now-app1',function() {
	// 	echo "1"; exit;
	// });



$site_abuse = App\Models\BlockedIPaddress::all();
$ip = $_SERVER['REMOTE_ADDR'];
foreach($site_abuse as $rowSiteAbuse){
	if($rowSiteAbuse->v_ipaddress == $ip ){
		Route::group([ 'prefix' => '/*' ], function(){
			die('<h1>Sorry your ip address is blacklisted.</h1>');
		});
	}
}
Route::get('/', 'Frontend\Home\Home@index');
Route::get('/feed', 'Frontend\Feed\Feed@index');
Route::get('/more-category', 'Frontend\Home\Home@moreCategory');

//Route::get('/payment', 'Frontend\Home\Payment@index');
// Route::get('/', function () {
// 	dd("dddd");	
// 	//Route::get('/', 'Frontend\Home\Home@index');
//     //return view('welcome');
// });

Route::get('facebook/login', 'Frontend\Login\SocialAccounts@index');
Route::get('facebook/callback', 'Frontend\Login\SocialAccounts@facebookCallback');
Route::group([ 'prefix' => 'login'], function () {
	Route::get('/','Frontend\Login\Login@index');
	Route::post('/','Frontend\Login\Login@postlogin');
});
Route::get('logout','Frontend\Login\Login@getLogout');
Route::get('thanks','Frontend\Thanks@index');

Route::get('signup-withfacebook','Frontend\Login\Login@signupFacebook');
Route::get('facebook-callback','Frontend\Login\Login@getFacebookcallback');
Route::get('forgot-password','Frontend\Login\Login@forgotPassword');
Route::post('forgot-password','Frontend\Login\Login@postForgotPassword');
Route::get('reset-password/{id}','Frontend\Login\Login@resetPassword');
Route::post('update-password','Frontend\Login\Login@postUpdatePassword');


Route::get('feedback','Frontend\Feedback\Feedback@index');
Route::post('feedback','Frontend\Feedback\Feedback@postFeedback');

Route::get('pages/{slug}','Frontend\CMS\Cms@index');
Route::get('cms/{slug}','Frontend\CMS\Cms@index');
Route::get('confirm/account/{id}','Frontend\Account\User@confirmAccount');

Route::post('notifyformdata','Frontend\Feedback\Feedback@postNotify');
Route::get('unsubscribed-notify/{id}','Frontend\Feedback\Feedback@UnscubsribedNotify');
Route::get('unsubscribed/thanks','Frontend\Feedback\Feedback@UnscubsribedThanks');

//Route::get('support','Frontend\CMS\Cms@support');

/*
|--------------------------------------------------------------------------
|		Skillbox => Homepage search and search result and detail page
|--------------------------------------------------------------------------
*/

Route::get('inperson-suggest','Frontend\Home\Home@inpersonSuggest');
Route::get('online-suggest','Frontend\Home\Home@onlineSuggest');
Route::get('course-suggest','Frontend\Home\Home@courseSuggest');

Route::get('search','Frontend\Home\Home@Search');
Route::get('courses-search','Frontend\Home\Home@Search');


Route::get('Loadmorecourse','Frontend\Home\Home@LoadMoreCourseSearch');
Route::get('Loadmoreskilljob','Frontend\Home\Home@LoadMoreSkillandJobSearch');



Route::get('shortlist','Frontend\Home\Home@shortlistResult');
Route::get('apply-job','Frontend\Home\Home@applyJob');
Route::get('shortlist-course','Frontend\Home\Home@coursesShortlist');
Route::get('shortlist-job','Frontend\Home\Home@jobShortlist');

Route::get('course/{id}/{name}','Frontend\Home\Home@coursesDetail');
Route::get('online/{id}/{name}','Frontend\Home\Home@onlineDetail');
Route::get('online-job/{id}/{name}','Frontend\Home\Home@onlineJobDetail');

Route::get('skill/review','Frontend\Home\Home@SkillReview');
Route::get('job/review','Frontend\Home\Home@jobdetailReview');

Route::get('course-cart/buy-now/{id}','Frontend\Home\Home@buyNowCourses');
Route::get('course-cart/add/{id}','Frontend\Home\Home@addNowCourses');

Route::get('skill/buy-now/{id}/{package}','Frontend\Home\Home@buyNowSkill');
Route::post('skill/buy-now','Frontend\Home\Home@postBuyNowSkill');


// Route::get('skill/buy-now-app','Frontend\Home\Home@postBuyNowSkillApp');
// Route::get('course/buy-now-app','Frontend\Home\Home@buyNowCoursesApp');
Route::match(['GET','POST'],'/skill/buy-now-app', 'Frontend\Home\Home@postBuyNowSkillApp');
Route::match(['GET','POST'],'/course/buy-now-app', 'Frontend\Home\Home@buyNowCoursesApp');




// Route::post('skill/buy-now-app','Frontend\Home\Home@postBuyNowSkillApp');
// Route::post('course/buy-now-app','Frontend\Home\Home@buyNowCoursesApp');

Route::get('cart/data','Frontend\Cart\Course@cartCommon');

Route::get('cart/course','Frontend\Cart\Course@index');
Route::get('cart/skill','Frontend\Cart\Skill@index');

Route::group(['prefix'=>'cart', 'middleware' => ['webuser'] ], function () {
	
	// Route::get('/course','Frontend\Cart\Course@index');
	Route::get('/course/remove/{id}','Frontend\Cart\Course@removeRawItem');
	Route::get('/course/mangopay','Frontend\Cart\Payment@paymentWithMangoPay');
	Route::match(['GET','POST'],'/course/mangopay-return', 'Frontend\Cart\Payment@getMangoPaymentReturn');
	Route::get('/skill/mangopay','Frontend\Cart\SkillPayment@paymentWithMangoPay');
	Route::match(['GET','POST'],'/skill/mangopay-return', 'Frontend\Cart\SkillPayment@getMangoPaymentReturn');
	Route::get('/course/payment/paypal','Frontend\Cart\Payment@coursePaypal');
	Route::match(['GET','POST'],'/course/paypal-return', 'Frontend\Cart\Payment@getPaypalReturn');
	Route::match(['GET','POST'],'/course/paypal-cancel', 'Frontend\Cart\Payment@getPaypalCancel');
	// Route::get('/skill','Frontend\Cart\Skill@index');
	Route::get('/skill/remove/{id}','Frontend\Cart\Skill@RemoveRawItem');
	Route::post('/skill/update','Frontend\Cart\Skill@updateCart');

	Route::get('/skill/payment/paypal','Frontend\Cart\SkillPayment@skillPaypal');
	Route::match(['GET','POST'],'/skill/paypal-return', 'Frontend\Cart\SkillPayment@getPaypalReturn');
	Route::match(['GET','POST'],'/skill/paypal-cancel', 'Frontend\Cart\SkillPayment@getPaypalCancel');
});
Route::get('payment/mango/{id}','Frontend\Payment\Mangopayment@index');


// Route::get('cart/skill/mangopay-app','Frontend\Cart\SkillPayment@paymentWithMangoPayApp');


// Route::get('payment/card','Frontend\Payment\Card@index');
// Route::get('payment/card1','Frontend\Payment\Card@CreateCard');
// Route::match(['GET','POST'],'/payment/card2', 'Frontend\Payment\Card@PostCreateCard');
// Route::get('payment/card3','Frontend\Payment\Card@DirectPaymentUsingCard');
// Route::match(['GET','POST'],'/payment/card4', 'Frontend\Payment\Card@Card4');


Route::get('payment/mangopay-option','Frontend\Payment\Card@CardOption');
Route::post('payment/mangopay-option','Frontend\Payment\Card@postCardOption');

Route::group(['prefix'=>'payment', 'middleware' => ['webuser'] ], function () {
	
	Route::get('/','Frontend\Payment\Payment@index');
	Route::get('/mangopay','Frontend\Payment\Payment@paymentWithMangoPay');
	Route::match(['GET','POST'],'/mangopay-return', 'Frontend\Payment\Payment@getMangoPaymentReturn');
	Route::get('/paypal','Frontend\Payment\Payment@paymentWithPaypal');
	Route::match(['GET','POST'],'/paypal-return', 'Frontend\Payment\Payment@getPaypalReturn');
	Route::match(['GET','POST'],'/paypal-cancel', 'Frontend\Payment\Payment@getPaypalCancel');

	Route::get('card','Frontend\Payment\Card@index');
	Route::match(['GET','POST'],'/mangopaycreatecard', 'Frontend\Payment\Card@CreateCardMangopay');
	Route::match(['GET','POST'],'/mangopay/card', 'Frontend\Payment\Card@DirectPaymentUsingCardId');
	
	// Route::get('payment/card1','Frontend\Payment\Card@CreateCard');
	// Route::match(['GET','POST'],'/payment/card2', 'Frontend\Payment\Card@PostCreateCard');
	// Route::get('payment/card3','Frontend\Payment\Card@DirectPaymentUsingCard');
	// Route::match(['GET','POST'],'/payment/card4', 'Frontend\Payment\Card@Card4');
});


Route::group(['prefix'=>'profile', 'middleware' => ['webuser'] ], function () {
		Route::get('/identification-documents','Frontend\Account\User@identificationDocuments');
		Route::post('/identification-documents','Frontend\Account\User@uploadIdentificationDocuments');
		
		Route::post('/ajax/identity','Frontend\Account\User@uploadIdentity');
		Route::get('/ajax/remove/identity','Frontend\Account\User@removeIdentity');

		Route::post('/ajax/registration','Frontend\Account\User@uploadRegistration');
		Route::get('/ajax/remove/registration','Frontend\Account\User@removeRegistration');

		Route::post('/ajax/articles','Frontend\Account\User@uploadArticles');
		Route::get('/ajax/remove/articles','Frontend\Account\User@removeArticles');

		Route::post('/ajax/shareholder','Frontend\Account\User@uploadShareholder');
		Route::get('/ajax/remove/shareholder','Frontend\Account\User@removeShareholder');

});	


/*
|--------------------------------------------------------------------------
|		Skillbox => user Account
|--------------------------------------------------------------------------
*/

Route::group([ 'prefix' => 'account'], function () {	
	
	Route::get('/job-post','Frontend\Account\User@index');
	Route::get('/email-confirm-message','Frontend\Account\User@EmailConfirmMessage');
	
	Route::get('/seller','Frontend\Account\User@seller');
	Route::get('/buyer','Frontend\Account\User@buyer');
	Route::post('/signup','Frontend\Account\User@signup');
	Route::get('/get-skill','Frontend\Account\User@getSkill');
	Route::get('/upgrade-plan','Frontend\Account\Plan@index');
	Route::get('/plan-thanku','Frontend\Account\Plan@thanksPlan');
	Route::get('/cancel-account-confirm','Frontend\Account\Dashboard@cancelAccountConfirm');
	Route::get('/delete-account','Frontend\Account\Dashboard@deactivateAccount');
	Route::get('/downgrade-account','Frontend\Account\Dashboard@downgradeAccount');

});


Route::group([ 'prefix' => 'crone'], function () {	
	
	Route::get('/course-review','Crone\Coursereview@index');
	Route::get('/job-review','Crone\Jobreview@index');
	Route::get('/seller-profile-review','Crone\Sellerreview@index');
	Route::get('/job-expire-notification','Crone\Jobexpirenotification@index');
	Route::get('/order-completed','Crone\OrderCompleted@index');
	Route::get('/update-user','Crone\Userplanexpire@updateUser');
	Route::get('/recurring-payment','Crone\Recurringpayment@index');
	Route::get('/perday-impression','Crone\Impression@index');
	Route::get('/sendmail','Crone\Sendmail@index');
	Route::get('/deletesendmail','Crone\Sendmail@deleteSendMail');
	Route::get('/order-completed-payment','Crone\OrderCompleted@directPayment');

	Route::get('/signupdate','Crone\Jobapplied@signupdate');

	Route::get('/kyc-approved','Crone\Userplanexpire@mangopayKycApproved');
	Route::get('/kyc-refused','Crone\Userplanexpire@mangopayKycRefused');


	// Route::get('/seller-review','Crone\Sellerreview@index');
	// Route::get('/job-applied','Crone\Jobapplied@index');
	// Route::get('/job-expire-notification','Crone\Jobexpirenotification@index');
	// Route::get('/plan-expire','Crone\Userplanexpire@index');
	//Seller payment and buyer refund crone	
	// Route::get('/order-completed','Crone\OrderCompleted@index');
	// Route::get('/buyer_refund','Crone\OrderCompleted@refund');
	// Route::get('/update-user','Crone\Userplanexpire@updateUser');

});

Route::get('message/userLogin/check','Frontend\Account\Messages@userLogin');

Route::group(['prefix'=>'message', 'middleware' => ['webuser'] ], function () {
	
	Route::post('/sendMessageSeller','Frontend\Account\Messages@sendMessageSeller');
	Route::get('/dashboard/buyer','Frontend\Account\Messages@buyerDashboardMessage');
	Route::get('/dashboard/seller','Frontend\Account\Messages@sellerDashboardMessage');
	
	Route::get('/buyer/message-centre','Frontend\Account\Messages@buyerchatHistory');
	Route::get('/seller/message-centre','Frontend\Account\Messages@sellerchatHistory');

	Route::get('/buyer/chatHistory','Frontend\Account\Messages@buyerchatHistory');
	Route::get('/seller/chatHistory','Frontend\Account\Messages@sellerchatHistory');

	Route::get('/buyer/ajaxlist','Frontend\Account\Messages@buyerchatHistoryajaxtList');
	Route::get('/seller/ajaxlist','Frontend\Account\Messages@sellerchatHistoryajaxtList');
	Route::get('/detail','Frontend\Account\Messages@messageDetail');
	Route::post('/sendMessage/Seller','Frontend\Account\Messages@sendMessagetoSeller');
	Route::post('/sendMessage/Buyer','Frontend\Account\Messages@sendMessagetoBuyer');
	Route::post('/sendReportAdmin','Frontend\Account\Messages@sendListingReportAdmin');
	Route::post('/sendDefaultMessage/seller','Frontend\Account\Messages@saveDefaultMessage');
});


Route::group(['prefix'=>'account', 'middleware' => ['webuser'] ], function () {
	
	Route::get('/profile','Frontend\Account\User@profilePhoto');
	Route::get('/profile/webcame','Frontend\Account\User@profilePhotoWebcame');
	Route::post('/update-profile','Frontend\Account\User@updateProfilePhoto');
	Route::post('/update-profile/webcame','Frontend\Account\User@updateProfilePhotoWebcame');
	Route::get('/profile-url','Frontend\Account\User@WebcameUrl');
	Route::get('/resend-email','Frontend\Account\User@resendConfirmAccountEmail');
	Route::post('/dashbaord-image','Frontend\Account\Dashboard@updateProfileImage');
	Route::get('/deactivate','Frontend\Account\Dashboard@deactivateAccount');

	//Route::post('/dashbaord-box','Frontend\Account\Dashboard@alertboxclose');

});

Route::get('account/dashbaord-box-data','Frontend\Account\Dashboard@Alertboxclose');



Route::group([ 'prefix' => 'seller-signup'], function () {
		
	Route::get('/online','Frontend\Seller\Seller@online');
	Route::get('/inperson','Frontend\Seller\Seller@inperson');
	Route::post('/update-step1','Frontend\Seller\Seller@updateStep1');
	Route::post('/update-step2','Frontend\Seller\Seller@updateStep2');	
	Route::get('/get-skill','Frontend\Seller\Seller@getSkill');

});


Route::get('/orderscancel','Frontend\Seller\ResolutionCenter@sendEmailNotificationBuyer');


Route::group(['prefix'=>'checkpassword', 'middleware' => ['webuser'] ], function () {
	Route::get('/','Frontend\Seller\Mywallet@checkpassword');
});


Route::group(['prefix'=>'seller', 'middleware' => ['webuser'] ], function () {

	
	Route::get('/invoice/course','Frontend\Seller\Invoices@courseInvoice');
	Route::get('/invoice/course/download/{id}','Frontend\Seller\Invoices@downloadCourseInvoice');
	Route::get('/invoice/skill','Frontend\Seller\Invoices@skillInvoice');
	Route::get('/invoice/skill/download/{id}','Frontend\Seller\Invoices@downloadSkillInvoice');

	Route::get('/profile','Frontend\Seller\Seller@index');
	Route::post('/update-profile','Frontend\Seller\Seller@updateProfile');
	Route::get('/dashboard','Frontend\Seller\Seller@dashboard');
	Route::get('/update-service','Frontend\Seller\Seller@updateService');	
	Route::get('/activate-profile','Frontend\Seller\Seller@activateProfile');
	Route::post('/upgrade-user-plan','Frontend\Seller\Seller@upgradeUserPlan');
	Route::get('/my-profile','Frontend\Seller\Seller@myProfile');
	Route::get('/update-sponsor-profile','Frontend\Seller\Seller@updateSponsorProfileStatus');
	Route::get('/edit-my-profile','Frontend\Seller\Seller@editMyProfile');
	Route::post('/sponsor-profile','Frontend\Seller\Seller@sponsorProfile');
	Route::get('/edit-profile','Frontend\Seller\Seller@editProfile');
	Route::post('/activate-user-plan','Frontend\Seller\Seller@profileUpgradeUserPlan');
	Route::post('/plan-data','Frontend\Seller\Seller@updateUserPlan');
	Route::get('/shortlisted-jobs','Frontend\Seller\Seller@shortlistedJobs');
	Route::get('delete-shortlisted-jobs','Frontend\Seller\Seller@deleteShortlistedJobs');
	Route::post('/remove-img','Frontend\Seller\Seller@removeImage');
	Route::post('/remove-video','Frontend\Seller\Seller@removeVideo');
	Route::post('/remove-work-video','Frontend\Seller\Seller@removeWorkVideo');
	Route::get('/applied-jobs','Frontend\Seller\Seller@appliedJobs');
	Route::get('delete-applied-jobs','Frontend\Seller\Seller@deleteAppliedJobs');
	Route::get('/orders/{status}','Frontend\Seller\Order@index');
	Route::get('/orders/detail/{id}','Frontend\Seller\Order@detail');
	Route::post('/orders/extend-request','Frontend\Seller\Order@extendRequest');
	Route::post('/orders/post-requirement','Frontend\Seller\Order@postRequirement');
	
	Route::post('/orders/delivery-order-header','Frontend\Seller\Order@SendDeliverHeader');

	Route::post('/orders/post-review-comment','Frontend\Seller\Order@postReviewCommenr');
	Route::post('/applied-jobs/post-review','Frontend\Seller\Seller@appliedJobsReview');
	Route::get('/review-center','Frontend\Seller\Review@index');
	Route::get('/user-review','Frontend\Seller\Review@userReview');
	Route::get('/dashboard-review','Frontend\Seller\Review@dashboardReview');
	Route::get('/orders-resolution-center','Frontend\Seller\ResolutionCenter@index');
	Route::post('/cancel-order','Frontend\Seller\ResolutionCenter@cancelOrder');
	Route::post('/reply-order-resolution','Frontend\Seller\ResolutionCenter@replyOrderResolution');

	Route::get('/my-wallet','Frontend\Seller\Mywallet@index');
	Route::post('/my-wallet/withdraw','Frontend\Seller\Mywallet@withdraw');
	Route::post('ajax/upload/introducy_video/{id}','Frontend\Seller\Seller@sellerProfileIntoducyVideo');
	Route::get('ajax/remove/introducy_video/','Frontend\Seller\Seller@RemoveSellerProfileIntoducyVideo');

	Route::post('ajax/upload/work_video/{id}','Frontend\Seller\Seller@sellerProfileWorkVideo');
	Route::get('ajax/remove/work_video/','Frontend\Seller\Seller@RemoveSellerProfileWorkVideo');

	Route::post('ajax/upload/work_photos/{id}','Frontend\Seller\Seller@sellerProfileWorkPhotos');
	Route::get('ajax/remove/work_photos/','Frontend\Seller\Seller@RemoveSellerProfileWorkPhotos');

	Route::post('ajax/upload/doc/{id}','Frontend\Seller\Seller@sellerProfileDoc');
	Route::get('ajax/remove/doc/','Frontend\Seller\Seller@RemovesellerProfileDoc');

	Route::get('/update-profile-status','Frontend\Seller\Seller@updateProfileStatus');	
	

});


Route::group(['prefix'=>'buyer', 'middleware' => ['webuser'] ], function () {
	
	Route::get('/invoice/course','Frontend\Buyer\Invoices@courseInvoice');
	Route::get('/invoice/course/download/{id}','Frontend\Buyer\Invoices@downloadCourseInvoice');

	Route::get('/invoice/skill','Frontend\Buyer\Invoices@skillInvoice');
	Route::get('/invoice/skill/download/{id}','Frontend\Buyer\Invoices@downloadSkillInvoice');

	Route::get('/dashboard','Frontend\Buyer\Buyer@dashboard');
	Route::get('/shortlisted-skills','Frontend\Buyer\Buyer@shortlistedSkills');
	Route::get('/activate-profile','Frontend\Buyer\Buyer@activateProfile');
	Route::post('/upgrade-user-plan','Frontend\Buyer\Buyer@upgradeUserPlan');
	Route::get('/update-service','Frontend\Buyer\Buyer@updateService');
	Route::get('delete-shortlisted-skill','Frontend\Buyer\Buyer@deleteShortlistedSkill');
	Route::post('/activate-user-plan','Frontend\Buyer\Buyer@profileUpgradeUserPlan');
	
	//Buyer courses section
	Route::get('/courses','Frontend\Buyer\Courses@index');
	Route::get('/courses/overview/{id}','Frontend\Buyer\Courses@overView');
	Route::get('/courses/section/{id}','Frontend\Buyer\Courses@section');
	Route::get('/courses/qa/{id}','Frontend\Buyer\Courses@qa');
	Route::post('/courses/qa','Frontend\Buyer\Courses@postQA');
	Route::post('/courses/qacomment','Frontend\Buyer\Courses@postQAComment');
	Route::get('/courses/reviews/{id}','Frontend\Buyer\Courses@reviews');
	Route::post('/courses/reviews','Frontend\Buyer\Courses@postReviews');
	Route::post('/courses/reviews-comment','Frontend\Buyer\Courses@postReviewscomments');
	Route::post('/courses/reviews-update','Frontend\Buyer\Courses@postReviewsupdates');
	Route::get('/courses/announcements/{id}','Frontend\Buyer\Courses@announcements');
	Route::post('/courses/comments-announcements','Frontend\Buyer\Courses@postAnnouncementsComments');
	Route::get('/courses/view/section','Frontend\Buyer\Courses@viewSection');
	Route::get('/courses/invoice/{id}','Frontend\Buyer\Courses@downloadInvoice');
	Route::get('/orders/{status}','Frontend\Buyer\Order@index');
	Route::get('/orders/detail/{id}','Frontend\Buyer\Order@detail');
	Route::get('/orders-extend-request','Frontend\Buyer\Order@extendRequest');
	Route::post('/order-post-requirement','Frontend\Buyer\Order@postRequirement');
	Route::post('/order-accept','Frontend\Buyer\Order@acceptOrder');
	Route::post('/order-reviews','Frontend\Buyer\Order@orderReview');
	Route::get('/orders/invoice/{id}','Frontend\Buyer\Order@downloadInvoice');
	Route::get('/review-center','Frontend\Buyer\Review@index');
	Route::get('/user-review','Frontend\Buyer\Review@userReview');
	Route::get('/dashboard-review','Frontend\Buyer\Review@dashboardReview');
	Route::get('/appliedjobs','Frontend\Jobpost\Jobpost@appliedJobsDashboardList');
	Route::get('/my-wallet','Frontend\Buyer\Mywallet@index');
	Route::post('/my-wallet/withdraw','Frontend\Buyer\Mywallet@withdraw');

	Route::get('/qa-search','Frontend\Buyer\Courses@Qasearch');


});


Route::group(['prefix'=>'resolution', 'middleware' => ['webuser'] ], function () {
	Route::get('/{id}','Frontend\Buyer\Resolution@index');
	Route::post('step2','Frontend\Buyer\Resolution@Step2');
	Route::post('step3','Frontend\Buyer\Resolution@Step3');
	Route::post('post-request','Frontend\Buyer\Resolution@postRequest');

});


Route::group(['prefix'=>'invoice', 'middleware' => ['webuser'] ], function () {
	Route::get('/','Frontend\Account\Invoices@index');
	Route::get('/download/{id}','Frontend\Account\Invoices@downloadInvoice');
});



Route::get('buyer/post-job','Frontend\Jobpost\Jobpost@index');


Route::group(['prefix'=>'job-post', 'middleware' => ['webuser'] ], function () {
	
	Route::post('/online-update-step1','Frontend\Jobpost\Jobpost@updateStep1');
	Route::post('/online-update-step2','Frontend\Jobpost\Jobpost@updateStep2');

	Route::post('/update/data1','Frontend\Jobpost\Jobpost@updateStep1');
	Route::post('/update/data2','Frontend\Jobpost\Jobpost@updateStep2');
	
	Route::get('/edit/{id}','Frontend\Jobpost\Jobpost@editJobpost');
	Route::post('/update-step-1','Frontend\Jobpost\Jobpost@updateJobpost1');
	Route::post('/update-step-2','Frontend\Jobpost\Jobpost@updateJobpost2');
	Route::get('/sponsor-now','Frontend\Jobpost\Jobpost@sponsorNow');
	Route::post('/remove-img','Frontend\Jobpost\Jobpost@removeImage');	
	Route::post('/remove-video','Frontend\Jobpost\Jobpost@removeVideo');
	Route::get('/mark-urgent','Frontend\Jobpost\Jobpost@markUrgent');
	Route::get('/notify','Frontend\Jobpost\Jobpost@notify');
	Route::post('/upgrade-user-plan','Frontend\Jobpost\Jobpost@upgradeUserPlan');	
	Route::post('/plan-data','Frontend\Jobpost\Jobpost@updatePlan');		
	Route::post('/update-user-plan','Frontend\Jobpost\Jobpost@updateUserPlan');

	Route::post('plan-data-urget-notify','Frontend\Jobpost\Jobpost@updatePlanUrgentNotify');		
	
	Route::post('/add-job-update-user-plan','Frontend\Jobpost\Jobpost@updateUserPlanAddnewJoppost');		

	Route::get('/payment','Frontend\Jobpost\Payment@index');
	Route::get('/payment/paypal','Frontend\Jobpost\Payment@paymentWithPaypal');
	Route::match(['GET','POST'],'/payment/paypal-return', 'Frontend\Jobpost\Payment@getPaypalReturn');
	Route::match(['GET','POST'],'/payment/paypal-cancel', 'Frontend\Jobpost\Payment@getPaypalCancel');

	Route::get('/payment/mangopay','Frontend\Jobpost\Payment@paymentWithMangoPay');
	Route::match(['GET','POST'],'/payment/mangopay-return', 'Frontend\Jobpost\Payment@getMangoPaymentReturn');



	Route::post('/urgent-payment','Frontend\Jobpost\Jobpost@markUrgentPayment');		
	Route::post('/notify-payment','Frontend\Jobpost\Jobpost@sellerNotifyPayment');	

	Route::get('/check-sponsor-now','Frontend\Jobpost\Jobpost@checkSponsorNow');
	Route::post('/sponser-plan-data','Frontend\Jobpost\Jobpost@SponsorNowUpgradePlan');	

	Route::post('ajax/upload/video/{id}','Frontend\Jobpost\Jobpost@uploadVideo');	
	Route::get('ajax/remove/video','Frontend\Jobpost\Jobpost@removeAjaxVideo');	

	Route::post('ajax/upload/photos/{id}','Frontend\Jobpost\Jobpost@uploadPhotos');	
	Route::get('ajax/remove/photos','Frontend\Jobpost\Jobpost@removeAjaxPhotos');	
	
});


/*
|--------------------------------------------------------------------------
|		Skillbox => Login
|--------------------------------------------------------------------------
*/

Route::group([ 'prefix' => 'dashboard'], function () {
	
	Route::get('/','Frontend\Account\Dashboard@index');
	Route::get('/buyer','Frontend\Account\Dashboard@buyerDashboard');
	Route::get('/seller','Frontend\Account\Dashboard@sellerDashboard');
	Route::post('/upgrade-user-plan','Frontend\Account\Dashboard@upgradeUserPlan');
	// Route::get('/upgrade-user-plan-app','Frontend\Account\Dashboard@upgradeUserPlanApp');

});

Route::get('dashboard/upgrade-user-plan-app','Frontend\Account\Dashboard@upgradeUserPlanApp');

Route::group([ 'prefix' => 'job-postings'], function () {
	Route::get('/','Frontend\Jobpost\Jobpost@listJobpost');
	Route::get('/applied-for-jobs/{id}','Frontend\Jobpost\Jobpost@appliedJobs');
	Route::get('/delete-applied-jobs','Frontend\Jobpost\Jobpost@deleteAppliedJobs');

	Route::get('/endjobpost/{id}','Frontend\Jobpost\Jobpost@endJobPosting');

});

Route::group([ 'prefix' => 'blog'], function () {
	Route::get('/','Frontend\Blog\Blog@index');
	Route::get('/{id}','Frontend\Blog\Blog@detail');
});

Route::group([ 'prefix' => 'profile'], function () {
	Route::get('/edit','Frontend\Account\User@editProfile');
	Route::post('/update','Frontend\Account\User@updateProfile');
	Route::get('/billing','Frontend\Account\User@editBilling');
	Route::post('/billing-update','Frontend\Account\User@updateBilling');
	Route::get('/change-password','Frontend\Account\User@changePassword');
	Route::post('/update-password','Frontend\Account\User@updatePassword');
});


	// Route::post('/update','Frontend\Account\User@updateProfile');
	// Route::get('/billing','Frontend\Account\User@editBilling');
	// Route::post('/billing-update','Frontend\Account\User@updateBilling');



/*
|--------------------------------------------------------------------------
|		Skillbox => Job Post
|--------------------------------------------------------------------------
*/


Route::get('plan/thanku','Frontend\Jobpost\Jobpost@planFinal');

/*
|--------------------------------------------------------------------------
|		Skillbox => Sponsor ads
|--------------------------------------------------------------------------
*/

Route::group([ 'prefix' => 'sponsor-ads'], function () {
	
	Route::get('/','Frontend\Sponsorads\Sponsorads@index');
	Route::get('/addsponsorads','Frontend\Sponsorads\Sponsorads@addSponsorAds');
	Route::get('/sponsoradsstatus','Frontend\Sponsorads\Sponsorads@sponsorAdsStatus');
	
});

/*
|--------------------------------------------------------------------------
|		Skillbox => Buyer
|--------------------------------------------------------------------------
*/

Route::group([ 'prefix' => 'buyer-signup'], function () {
	
	Route::get('/online','Frontend\Buyer\Buyer@online');
	Route::get('/inperson','Frontend\Buyer\Buyer@inperson');

	Route::post('/online-update-step1','Frontend\Buyer\Buyer@updateStep1');
	Route::post('/online-update-step2','Frontend\Buyer\Buyer@updateStep2');

	// Route::get('/','Frontend\Buyer\Buyer@index');
	// Route::post('/signup','Frontend\Buyer\Buyer@signup');
	// Route::get('/profile','Frontend\Buyer\Buyer@profilePhoto');
	// Route::post('/update-profile','Frontend\Buyer\Buyer@updateProfilePhoto');
});

	
Route::get('single-search','Frontend\Buyer\Buyer@search');


/*
|--------------------------------------------------------------------------
|		Skillbox => Support for both seller and buyer
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'support', 'middleware' => ['webuser'] ], function () {
	Route::get('/', 'Frontend\Support\Support@index');
	Route::get('/add', 'Frontend\Support\Support@add');	
	Route::post('/add', 'Frontend\Support\Support@addData');	
	Route::get('/view/{id}', 'Frontend\Support\Support@view');	
	Route::post('/post-comment', 'Frontend\Support\Support@postComment');	
});


/*
|--------------------------------------------------------------------------
|		Skillbox => Seller Courses (Selling)
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'courses', 'middleware' => ['webuser'] ], function () {
		
	Route::get('/', 'Frontend\Courses\Courses@index');
	Route::get('/add', 'Frontend\Courses\Courses@add');	
	Route::post('/add', 'Frontend\Courses\Courses@addData');		
	Route::post('/add-update', 'Frontend\Courses\Courses@addDataUpdate');	
	Route::get('/view/{id}', 'Frontend\Courses\Courses@coursesView');
	Route::get('/section/{id}', 'Frontend\Courses\Courses@coursesSection');
	Route::get('/section-delete/{cid}/{sid}', 'Frontend\Courses\Courses@coursesSectionDelete');
	Route::get('/qa/{id}', 'Frontend\Courses\Courses@coursesQA');
	Route::post('/post-qa', 'Frontend\Courses\Courses@updateQA');
	Route::get('/reviews/{id}', 'Frontend\Courses\Courses@coursesReviews');
	Route::post('/post-reviews', 'Frontend\Courses\Courses@updateReviews');
	Route::get('/announcements/{id}', 'Frontend\Courses\Courses@coursesAnnouncements');
	Route::post('/announcements/add', 'Frontend\Courses\Courses@coursesAddAnnouncements');
	Route::get('get-announcements', 'Frontend\Courses\Courses@coursesGetAnnouncements');
	Route::post('update-announcements', 'Frontend\Courses\Courses@coursesUpdateAnnouncements');
	Route::post('comments-announcements', 'Frontend\Courses\Courses@coursesCommentsAnnouncements');
	Route::get('/updatestatus', 'Frontend\Courses\Courses@updateCoursesStatus');
	Route::get('/settings/{id}', 'Frontend\Courses\Courses@coursesSettings');
	Route::get('/messages/{id}', 'Frontend\Courses\Courses@coursesMessages');
	Route::post('/post-comment', 'Frontend\Courses\Courses@postComment');	
	Route::post('update-settings', 'Frontend\Courses\Courses@updateSettings');
	Route::post('update-messages', 'Frontend\Courses\Courses@updateMessages');
	Route::post('update-user-plan', 'Frontend\Courses\Courses@updateUserPlan');
	Route::post('/add-new-section', 'Frontend\Courses\Courses@addNewSection');	
	
	Route::get('/get-section', 'Frontend\Courses\Courses@getSection');
	Route::get('/edit-section-detail', 'Frontend\Courses\Courses@getSection');
	
	Route::post('/edit-section', 'Frontend\Courses\Courses@updateSection');
	Route::get('/get-course', 'Frontend\Courses\Courses@getCourse');
	Route::post('/update-course', 'Frontend\Courses\Courses@updateCourse');


	Route::get('/qa-search', 'Frontend\Courses\Courses@Qasearch');


});


/*
|--------------------------------------------------------------------------
|		Skillbox => Seller Courses sponsor
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'sponsor-courses', 'middleware' => ['webuser'] ], function () {
	
	Route::get('/', 'Frontend\Courses\SponsorCourses@index');
	Route::get('/new','Frontend\Courses\SponsorCourses@addSponsor');
	Route::get('/add-sponsor','Frontend\Courses\SponsorCourses@addSponsorCourses');
	Route::post('/upgrade-user-plan','Frontend\Courses\SponsorCourses@upgradeUserPlan');
	Route::get('/sponsor-status','Frontend\Courses\SponsorCourses@statusSponsorCourses');
	Route::get('/get-view-data','Frontend\Courses\SponsorCourses@Viewdata');


});

Route::group([ 'prefix' => 'admin' ], function () {
	
	Route::get('/', 'Admin\Login@getLogin');				
	Route::get('/login', 'Admin\Login@getLogin');			
	Route::post('/login', 'Admin\Login@postLogin');			
	Route::get('/getLogout', 'Admin\Login@getLogout');		
	Route::get('/forgotpassword', 'Admin\Login@getForgotPassword');
	Route::post('/forgotpassword', 'Admin\Login@postForgotPassword');
	Route::get('/new-password', 'Admin\Login@newPassword');
	Route::get('/confirm-new-password', 'Admin\Login@confirmNewPassword');
	Route::post('/confirm-new-password', 'Admin\Login@postConfirmNewPassword');	
	Route::get('/admin', 'Admin\Profile\Profile@index');
	Route::get('/admin/add/{id}', 'Admin\Profile\Profile@Add');
	Route::get('/admin/edit/{id}', 'Admin\Profile\Profile@Edit');
	Route::post('/admin/add-profile/{id}', 'Admin\Profile\Profile@addProfile');
	Route::post('/admin/update-profile/{id}', 'Admin\Profile\Profile@updateProfile');	
	Route::get('/admin/delete/{id}', 'Admin\Profile\Profile@deleteProfile');
	Route::get('/admin/{id}/{status}/status','Admin\Profile\Profile@status');
});

Route::group([ 'prefix' => 'admin/dashboard', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Dashboard\Dashboard@index');
});


Route::group(['prefix'=>'admin/report', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Dashboard\Reportdata@index');
	
});


/*
|--------------------------------------------------------------------------
|		Settings => Sitesettings
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/sitesetting', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\Sitesetting@index');
	Route::post('/','Admin\Settings\Sitesetting@store');

});


Route::group(['prefix'=>'admin/buyer-how-its-work', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\BuyerHowitwork@index');
	Route::get('/add/{id}','Admin\Settings\BuyerHowitwork@Add');
	Route::get('/edit/{id}','Admin\Settings\BuyerHowitwork@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\BuyerHowitwork@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\BuyerHowitwork@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\BuyerHowitwork@status');

});

Route::group(['prefix'=>'admin/seller-how-its-work', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Settings\SellerHowitwork@index');
	Route::get('/add/{id}','Admin\Settings\SellerHowitwork@Add');
	Route::get('/edit/{id}','Admin\Settings\SellerHowitwork@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\SellerHowitwork@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\SellerHowitwork@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\SellerHowitwork@status');
});


/*
|--------------------------------------------------------------------------
|		Settings => Email Template
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/email-template', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\EmailTemplate@index');
	Route::get('/add/{id}','Admin\Settings\EmailTemplate@Add');
	Route::get('/edit/{id}','Admin\Settings\EmailTemplate@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\EmailTemplate@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\EmailTemplate@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\EmailTemplate@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => Email Template Header
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/email-template-header', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\EmailTemplateHeader@index');
	Route::get('/add/{id}','Admin\Settings\EmailTemplateHeader@Add');
	Route::get('/edit/{id}','Admin\Settings\EmailTemplateHeader@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\EmailTemplateHeader@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\EmailTemplateHeader@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\EmailTemplateHeader@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => Email Template Footer
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/email-template-footer', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\EmailTemplateFooter@index');
	Route::get('/add/{id}','Admin\Settings\EmailTemplateFooter@Add');
	Route::get('/edit/{id}','Admin\Settings\EmailTemplateFooter@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\EmailTemplateFooter@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\EmailTemplateFooter@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\EmailTemplateFooter@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => Company Ethos
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/company-ethos', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\CompanyEthos@index');
	Route::get('/add/{id}','Admin\Settings\CompanyEthos@Add');
	Route::get('/edit/{id}','Admin\Settings\CompanyEthos@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\CompanyEthos@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\CompanyEthos@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\CompanyEthos@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => Categories
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/categories', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Settings\Categories@index');
	Route::get('/add/{id}','Admin\Settings\Categories@Add');
	Route::get('/edit/{id}','Admin\Settings\Categories@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\Categories@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\Categories@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\Categories@status');
});

/*
|--------------------------------------------------------------------------
|		Settings => Skills
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/skills', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\Skills@index');
	Route::get('/add/{id}','Admin\Settings\Skills@Add');
	Route::get('/edit/{id}','Admin\Settings\Skills@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\Skills@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\Skills@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\Skills@status');
});

/*
|--------------------------------------------------------------------------
|		Settings => Levels
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/levels', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\Levels@index');
	Route::get('/add/{id}','Admin\Settings\Levels@Add');
	Route::get('/edit/{id}','Admin\Settings\Levels@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\Levels@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\Levels@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\Levels@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => Price filter
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/price-filter', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\Pricefilter@index');
	Route::get('/add/{id}','Admin\Settings\Pricefilter@Add');
	Route::get('/edit/{id}','Admin\Settings\Pricefilter@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\Pricefilter@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\Pricefilter@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\Pricefilter@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => NewsLetter
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/newsletterusers', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\NewsLetter\NewsLetter@index');
	Route::get('/add/{id}','Admin\NewsLetter\NewsLetter@Add');
	Route::get('/edit/{id}','Admin\NewsLetter\NewsLetter@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\NewsLetter\NewsLetter@Action');
	Route::get('/action/{action}/{_id}', 'Admin\NewsLetter\NewsLetter@Action');
	Route::get('/{id}/{status}/status','Admin\NewsLetter\NewsLetter@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => Faqs
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/faqs', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Faqs\Faqs@index');
	Route::get('/add/{id}','Admin\Faqs\Faqs@Add');
	Route::get('/edit/{id}','Admin\Faqs\Faqs@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Faqs\Faqs@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Faqs\Faqs@Action');
	Route::get('/{id}/{status}/status','Admin\Faqs\Faqs@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => Country
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/country', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\Country@index');
	Route::get('/add/{id}','Admin\Settings\Country@Add');
	Route::get('/edit/{id}','Admin\Settings\Country@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\Country@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\Country@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\Country@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => State
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/state', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Settings\State@index');
	Route::get('/add/{id}','Admin\Settings\State@Add');
	Route::get('/edit/{id}','Admin\Settings\State@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\State@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\State@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\State@status');
});

/*
|--------------------------------------------------------------------------
|		Settings => City
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/city', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Settings\City@index');
	Route::get('/add/{id}','Admin\Settings\City@Add');
	Route::get('/edit/{id}','Admin\Settings\City@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\City@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\City@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\City@status');
});

	
Route::group(['prefix'=>'admin/guide', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\Guide@index');
	Route::get('/add/{id}','Admin\Settings\Guide@Add');
	Route::get('/edit/{id}','Admin\Settings\Guide@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\Guide@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\Guide@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\Guide@status');

});




Route::group(['prefix'=>'admin/footer', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\Footer@index');
	Route::get('/add/{id}','Admin\Settings\Footer@Add');
	Route::get('/edit/{id}','Admin\Settings\Footer@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\Footer@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\Footer@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\Footer@status');

});


/*
|--------------------------------------------------------------------------
|		Settings => Search
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/search', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\Search@index');
	Route::get('/add/{id}','Admin\Settings\Search@Add');
	Route::get('/edit/{id}','Admin\Settings\Search@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\Search@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\Search@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\Search@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => Sponsor Fees
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/sponsorfees', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Settings\SponsorFees@index');
	Route::get('/add/{id}','Admin\Settings\SponsorFees@Add');
	Route::get('/edit/{id}','Admin\Settings\SponsorFees@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\SponsorFees@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\SponsorFees@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\SponsorFees@status');

});

/*
|--------------------------------------------------------------------------
|		Jobs Managment
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/job', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Job\Job@index');
	Route::get('/add/{id}','Admin\Job\Job@Add');
	Route::get('/edit/{id}','Admin\Job\Job@Edit');
	Route::post('/action/{action}/{_id}','Admin\Job\Job@Action');
	Route::get('/action/{action}/{_id}','Admin\Job\Job@Action');
	Route::get('/{id}/{status}/status','Admin\Job\Job@status');
	Route::get('/getskill','Admin\Job\Job@getSkill');

	Route::post('/removevideo','Admin\Job\Job@removeVideo');
	Route::post('/removeimage','Admin\Job\Job@removeImage');


});

/*
|--------------------------------------------------------------------------
|		Settings => Plan
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/plan', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Plan\Plan@index');
	Route::get('/add/{id}','Admin\Plan\Plan@Add');
	Route::get('/edit/{id}','Admin\Plan\Plan@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Plan\Plan@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Plan\Plan@Action');
	Route::get('/{id}/{status}/status','Admin\Plan\Plan@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => Blogs
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/blogs', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Blogs\Blogs@index');
	Route::get('/add/{id}','Admin\Blogs\Blogs@Add');
	Route::get('/edit/{id}','Admin\Blogs\Blogs@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Blogs\Blogs@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Blogs\Blogs@Action');
	Route::get('/{id}/{status}/status','Admin\Blogs\Blogs@status');

});

/*
|--------------------------------------------------------------------------
|		Settings => Pages
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/pages', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Pages\Pages@index');
	Route::get('/add/{id}','Admin\Pages\Pages@Add');
	Route::get('/edit/{id}','Admin\Pages\Pages@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Pages\Pages@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Pages\Pages@Action');
	Route::get('/{id}/{status}/status','Admin\Pages\Pages@status');

});

Route::group(['prefix'=>'admin/pages-template', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Pages\Pagestemplate@index');
	Route::get('/add/{id}','Admin\Pages\Pagestemplate@Add');
	Route::get('/edit/{id}','Admin\Pages\Pagestemplate@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Pages\Pagestemplate@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Pages\Pagestemplate@Action');
	Route::get('/{id}/{status}/status','Admin\Pages\Pagestemplate@status');
});

/*
|--------------------------------------------------------------------------
|		Settings => Pages
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/feedback', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Feedback\Feedback@index');
	Route::get('/add/{id}','Admin\Feedback\Feedback@Add');
	Route::get('/view/{id}','Admin\Feedback\Feedback@View');
	Route::post('/action/{action}/{_id}', 'Admin\Feedback\Feedback@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Feedback\Feedback@Action');
	Route::get('/{id}/{status}/status','Admin\Feedback\Feedback@status');
});



Route::group(['prefix'=>'admin/notify-user', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Notify\Notifyuser@index');
	Route::get('/add/{id}','Admin\Notify\Notifyuser@Add');
	Route::get('/edit/{id}','Admin\Notify\Notifyuser@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Notify\Notifyuser@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Notify\Notifyuser@Action');
	Route::get('/{id}/{status}/status','Admin\Notify\Notifyuser@status');

});




/*
|--------------------------------------------------------------------------
|		Settings => Users
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/users', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Users\Users@index');
	Route::get('/add/{id}','Admin\Users\Users@Add');
	Route::get('/edit/{id}','Admin\Users\Users@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Users\Users@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Users\Users@Action');
	Route::get('/{id}/{status}/status','Admin\Users\Users@status');
	Route::get('/subCategories','Admin\Users\Users@subCategories');
	Route::get('/skills','Admin\Users\Users@skills');
	Route::get('/state','Admin\Users\Users@state');
	Route::get('/city','Admin\Users\Users@city');

	Route::post('/update-user-profile/{action}/{_id}', 'Admin\Users\Users@updateUserProfile');

	Route::get('/edit/password/{id}','Admin\Users\Users@EditPassword');
	Route::post('/update-user-password/{action}/{_id}', 'Admin\Users\Users@updateUserPassword');

	Route::get('/edit/billing/{id}','Admin\Users\Users@EditBilling');
	Route::post('/update-user-billing/{action}/{_id}', 'Admin\Users\Users@updateUserBilling');

	
	Route::get('/edit/sellerinperson/{_id}','Admin\Users\Users@EditSellerInperson');
	Route::get('/edit/selleronline/{_id}','Admin\Users\Users@EditSellerOnline');
	Route::post('/update-user-sellerinperson/{_id}', 'Admin\Users\Users@updateUserSellerInperson');
	Route::post('/update-user-selleronline/{_id}', 'Admin\Users\Users@updateUserSellerOnline');
	


	Route::post('/seller/remove-img','Admin\Users\Users@removeImage');
	Route::post('/seller/remove-video', 'Admin\Users\Users@removeVideo');
	Route::post('/seller/remove-work-video','Admin\Users\Users@removeWorkVideo');

	Route::get('/buyer-orders/{_id}','Admin\Users\BuyerOrders@index');
	Route::get('/seller-orders/{_id}','Admin\Users\SellerOrders@index');
	Route::get('/buyer-reviews/{_id}','Admin\Users\BuyerReviews@index');
	Route::get('/seller-reviews/{_id}','Admin\Users\SellerReviews@index');

	Route::get('/seller-reviews/course/{_id}','Admin\Users\SellerReviews@CourseReview');


	// seller-reviews
	
	// buyer-reviews
	
	// seller-orders
	
	// buyer-orders
	
	



});

/*
|--------------------------------------------------------------------------
|		Settings => Jobs
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/jobs', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Users\Jobs@index');
	Route::get('/add/{id}','Admin\Users\Jobs@Add');
	Route::get('/edit/{id}','Admin\Users\Jobs@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Users\Jobs@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Users\Jobs@Action');
	Route::get('/{id}/{status}/status','Admin\Users\Jobs@status');
});

/*
|--------------------------------------------------------------------------
|		Settings => Seller Work
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/sellerwork', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Users\SellerWork@index');
	Route::get('/add/{id}','Admin\Users\SellerWork@Add');
	Route::get('/edit/{id}','Admin\Users\SellerWork@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Users\SellerWork@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Users\SellerWork@Action');
	Route::get('/{id}/{status}/status','Admin\Users\SellerWork@status');
});


Route::group(['prefix'=>'admin/sitemap', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Settings\Sitemap@index');
	Route::post('/generate','Admin\Settings\Sitemap@Generate');
});


/*
|--------------------------------------------------------------------------
|		Settings => Buyer Work
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/buyerwork', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Users\BuyerWork@index');
	Route::get('/add/{id}','Admin\Users\BuyerWork@Add');
	Route::get('/edit/{id}','Admin\Users\BuyerWork@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Users\BuyerWork@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Users\BuyerWork@Action');
	Route::get('/{id}/{status}/status','Admin\Users\BuyerWork@status');
});




/*
|--------------------------------------------------------------------------
|		Settings => Support Category
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/supportquery', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Support\SupportQuery@index');
	Route::get('/add/{id}','Admin\Support\SupportQuery@Add');
	Route::get('/edit/{id}','Admin\Support\SupportQuery@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Support\SupportQuery@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Support\SupportQuery@Action');
	Route::get('/{id}/{status}/status','Admin\Support\SupportQuery@status');
});

/*
|--------------------------------------------------------------------------
|		Settings => Support
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/support', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Support\Support@index');
	Route::get('/add/{id}','Admin\Support\Support@Add');
	Route::get('/edit/{id}','Admin\Support\Support@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Support\Support@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Support\Support@Action');
	Route::get('/{id}/{status}/status','Admin\Support\Support@status');
});


/*
|--------------------------------------------------------------------------
|		Settings => Courses
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/courses', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Courses\Courses@index');
	Route::get('/add/{id}','Admin\Courses\Courses@Add');
	Route::get('/edit/{id}','Admin\Courses\Courses@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Courses\Courses@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Courses\Courses@Action');
	Route::get('/{id}/{status}/status','Admin\Courses\Courses@status');
});


Route::group(['prefix'=>'admin/courses-language', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Courses\CoursesLanguage@index');
	Route::get('/add/{id}','Admin\Courses\CoursesLanguage@Add');
	Route::get('/edit/{id}','Admin\Courses\CoursesLanguage@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Courses\CoursesLanguage@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Courses\CoursesLanguage@Action');
	Route::get('/{id}/{status}/status','Admin\Courses\CoursesLanguage@status');

});

Route::group(['prefix'=>'admin/courses-category', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Courses\CoursesCategory@index');
	Route::get('/add/{id}','Admin\Courses\CoursesCategory@Add');
	Route::get('/edit/{id}','Admin\Courses\CoursesCategory@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Courses\CoursesCategory@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Courses\CoursesCategory@Action');
	Route::get('/{id}/{status}/status','Admin\Courses\CoursesCategory@status');

});

Route::group(['prefix'=>'admin/courses-level', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Courses\CoursesLevel@index');
	Route::get('/add/{id}','Admin\Courses\CoursesLevel@Add');
	Route::get('/edit/{id}','Admin\Courses\CoursesLevel@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Courses\CoursesLevel@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Courses\CoursesLevel@Action');
	Route::get('/{id}/{status}/status','Admin\Courses\CoursesLevel@status');

});




/*
|--------------------------------------------------------------------------
|		Settings => Course Chapters
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/coursechapters', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Courses\CourseChapters@index');
	Route::get('/add/{id}','Admin\Courses\CourseChapters@Add');
	Route::get('/edit/{id}','Admin\Courses\CourseChapters@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Courses\CourseChapters@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Courses\CourseChapters@Action');
	Route::get('/{id}/{status}/status','Admin\Courses\CourseChapters@status');
});





/*
|--------------------------------------------------------------------------
|		Settings => Course FAQs
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/coursefaqs', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Courses\CourseFaqs@index');
	Route::get('/add/{id}','Admin\Courses\CourseFaqs@Add');
	Route::get('/edit/{id}','Admin\Courses\CourseFaqs@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Courses\CourseFaqs@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Courses\CourseFaqs@Action');
	Route::get('/{id}/{status}/status','Admin\Courses\CourseFaqs@status');
});

/*
|--------------------------------------------------------------------------
|		Settings => Course Comments
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/coursecomments', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Courses\CourseComments@index');
	Route::get('/add/{id}','Admin\Courses\CourseComments@Add');
	Route::get('/edit/{id}','Admin\Courses\CourseComments@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Courses\CourseComments@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Courses\CourseComments@Action');
	Route::get('/{id}/{status}/status','Admin\Courses\CourseComments@status');
	Route::get('/title','Admin\Courses\CourseComments@GetTitle');
});

/*
|--------------------------------------------------------------------------
|		Settings => Messaging
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/messages', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\MessagingAndReviews\Messages@index');
	Route::get('/view/{id}/','Admin\MessagingAndReviews\Messages@View');
	Route::post('/action/{action}/{_id}', 'Admin\MessagingAndReviews\Messages@Action');
	Route::get('/action/{action}/{_id}', 'Admin\MessagingAndReviews\Messages@Action');
	
});

/*
|--------------------------------------------------------------------------
|		Settings => Reviews
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/reviews', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\MessagingAndReviews\Reviews@index');
	Route::get('/view/{id}/','Admin\MessagingAndReviews\Reviews@View');
	Route::post('/action/{action}/{_id}', 'Admin\MessagingAndReviews\Reviews@Action');
	Route::get('/action/{action}/{_id}', 'Admin\MessagingAndReviews\Reviews@Action');
	Route::get('/deletecomment','Admin\MessagingAndReviews\Reviews@deletecomment');
	
});


/*
|--------------------------------------------------------------------------
|		Order Management
|--------------------------------------------------------------------------
*/

	## Courses Order

Route::group(['prefix'=>'admin/order/courses', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Order\Courses@index');
	Route::get('/edit/{id}','Admin\Order\Courses@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Order\Courses@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Order\Courses@Action');
	Route::get('/{id}/{status}/status','Admin\Order\Courses@status');

});

	## Skill Order

Route::group(['prefix'=>'admin/order/skill', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Order\Skill@index');
	Route::get('/edit/{id}','Admin\Order\Skill@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Order\Skill@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Order\Skill@Action');
	Route::get('/{id}/{status}/status','Admin\Order\Skill@status');

});

	## User Plan Order

Route::group(['prefix'=>'admin/order/user-plan', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Order\UserPlan@index');
	Route::get('/edit/{id}','Admin\Order\UserPlan@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Order\UserPlan@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Order\UserPlan@Action');
	Route::get('/{id}/{status}/status','Admin\Order\UserPlan@status');

});

## Resolution Center

Route::group(['prefix'=>'admin/order/resolution-center', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\Order\ResolutionCenter@index');
	Route::get('/edit/{id}','Admin\Order\ResolutionCenter@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Order\ResolutionCenter@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Order\ResolutionCenter@Action');
	Route::get('/{id}/{status}/status','Admin\Order\ResolutionCenter@status');

});

/*
|--------------------------------------------------------------------------
|		Skillbox Job Management
|--------------------------------------------------------------------------
*/

	## Job Category

Route::group(['prefix'=>'admin/skillbox-job-category', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\SkillboxJob\SkillboxJobCategory@index');
	Route::get('/add/{id}','Admin\SkillboxJob\SkillboxJobCategory@Add');
	Route::get('/edit/{id}','Admin\SkillboxJob\SkillboxJobCategory@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\SkillboxJob\SkillboxJobCategory@Action');
	Route::get('/action/{action}/{_id}', 'Admin\SkillboxJob\SkillboxJobCategory@Action');
	Route::get('/{id}/{status}/status','Admin\SkillboxJob\SkillboxJobCategory@status');

});

	## Job

Route::group(['prefix'=>'admin/skillbox-job', 'middleware' => ['admin'] ], function () {
	
	Route::get('/','Admin\SkillboxJob\SkillboxJob@index');
	Route::get('/add/{id}','Admin\SkillboxJob\SkillboxJob@Add');
	Route::get('/edit/{id}','Admin\SkillboxJob\SkillboxJob@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\SkillboxJob\SkillboxJob@Action');
	Route::get('/action/{action}/{_id}', 'Admin\SkillboxJob\SkillboxJob@Action');
	Route::get('/{id}/{status}/status','Admin\SkillboxJob\SkillboxJob@status');

});

/*
|--------------------------------------------------------------------------
|		Meta
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/manage-meta-field', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Settings\ManageMetaField@index');
	Route::get('/add/{id}','Admin\Settings\ManageMetaField@Add');
	Route::get('/edit/{id}','Admin\Settings\ManageMetaField@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\Settings\ManageMetaField@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Settings\ManageMetaField@Action');
	Route::get('/{id}/{status}/status','Admin\Settings\ManageMetaField@status');
});

/*
|--------------------------------------------------------------------------
|		Block IP for security
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin/block-ip', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\BlockIP\BlockIP@index');
	Route::get('/add/{id}','Admin\BlockIP\BlockIP@Add');
	Route::get('/edit/{id}','Admin\BlockIP\BlockIP@Edit');
	Route::post('/action/{action}/{_id}', 'Admin\BlockIP\BlockIP@Action');
	Route::get('/action/{action}/{_id}', 'Admin\BlockIP\BlockIP@Action');
});



##Frontend Join Our Team

Route::get('join-our-team', 'Frontend\SkillboxJobs\SkillboxJobs@index');
Route::get('join-our-team/{id}', 'Frontend\SkillboxJobs\SkillboxJobs@detailsJob');
Route::get('join-our-team/apply-job/{id}', 'Frontend\SkillboxJobs\SkillboxJobs@applyJob');
Route::post('join-our-team/apply-job/{id}', 'Frontend\SkillboxJobs\SkillboxJobs@postApplyJob');
Route::get('jobs-apply-form-notification', 'Frontend\SkillboxJobs\SkillboxJobs@applyJobNotification');

##Frontend How to Skillbox guide
Route::get('skillbox-faqs', 'Frontend\SkillboxGuide\SkillboxGuide@index');
Route::get('skillbox-guide', 'Frontend\SkillboxGuide\SkillboxGuide@index');
Route::get('skillbox-guide-search', 'Frontend\SkillboxGuide\SkillboxGuide@SearchFaq');


