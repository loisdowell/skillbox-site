<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateWeb {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'web') {
	
		if (Auth::guard('web')->check()) {
			return $next($request);
		}
	
		return redirect('/')->with([
				'warning' => 'You must have to be logged in.',
		]);
	}   
}
