<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //

        "seller/ajax/*",
        "job-post/ajax/*",
        "profile/ajax/*",

    	'payment/paypal-cancel',
        'payment/paypal-return',
        'payment/card4',

        'job-post/payment/paypal-cancel',
        'job-post/payment/paypal-return',
        
        'cart/course/paypal-return',
        'cart/course/paypal-cancel',

        'cart/skill/paypal-return',
        'cart/skill/paypal-cancel',
        'account/update-profile/webcame',

        'payment/mangopay-return',
        'job-post/payment/mangopay-return',
        'feedback',


        'course/buy-now-app',
        'course/buy-now-app',
        

    ];
}
