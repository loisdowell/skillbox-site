<?php

namespace App\Http\Middleware;

use Closure, Session, URL,Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Users\Users as UsersModel;



class AuthenticateApi {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
    
    */
    public $attributes;
    
    public function handle($request, Closure $next, $guard = '') {

        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

        $serverdata = Request::all(); 

      	if(!isset($serverdata['token'])){
            $responseData=array(
                'response_code'=>401,
                'response_message'=>"Token is required",
                'response_data'=>array(),
            );
            return response()->json($responseData, 401);
        }
        $userToken = $serverdata['token'];
      
        $authdata = UsersModel::where('v_auth_token',$userToken)->first();
        if(!count($authdata)){
            $responseData=array(
                'response_code'=>401,
                'response_message'=>"Your session has expired. Please login again.",
                'response_data'=>array(),
            );
            return response()->json($responseData, 401);
        }   
        $request->attributes->add(['authdata' => $authdata]);
        return $next($request);
    }   



    

}

