<?php

namespace App\Http\Controllers\Api\V1\Messages;

use Request, Hash, Lang, Crypt, Session, Validator, Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Buyerreview as BuyerreviewModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Messages as MessagesModel;
use App\Models\Users\Users as UsersModel;



class Messages extends Controller
{


	public function index()
	{

		$postData = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;
		
		$dataLimit=20;
		$currentPage=1;

		$data=array();

		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			$q->where('i_to_id', $userid)->orWhere('i_from_id', $userid);
		});
		$query = $query->where('i_parent_id', 0)->orderBy("d_modified", "DESC");
		
		if(isset($postData['pagination']) && $postData['pagination']==1){
			$data = $query->paginate($dataLimit);	
		}else{
			$data = $query->get();
		}
		
		$finalList = array();
		if(count($data)) {
		
			foreach ($data as $k => $v) {
				
				$v_image = "";
				$v_fname = "";
				$v_lname = "";
				
				if ($v->i_from_id != $userid) {
					if (count($v->hasFromUser()) && isset($v->hasFromUser()->v_image)) {
						$v_image = $v->hasFromUser()->v_image;
					}
					if ($v_image != "") {
						$v_image = \Storage::cloud()->url($v_image);
					} else {
						$v_image = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
					}
					if (count($v->hasFromUser()) && isset($v->hasFromUser()->v_fname)) {
						$v_fname = $v->hasFromUser()->v_fname;
					}
					if (count($v->hasFromUser()) && isset($v->hasFromUser()->v_lname)) {
						$v_lname = $v->hasFromUser()->v_lname;
					}
				} else {

					if (count($v->hasToUser()) && isset($v->hasToUser()->v_image)) {
						$v_image = $v->hasToUser()->v_image;
					}
					if ($v_image != "") {
						$v_image = \Storage::cloud()->url($v_image);
					} else {
						$v_image = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
					}
					if (count($v->hasToUser()) && isset($v->hasToUser()->v_fname)) {
						$v_fname = $v->hasToUser()->v_fname;
					}
					if (count($v->hasToUser()) && isset($v->hasToUser()->v_lname)) {
						$v_lname = $v->hasToUser()->v_lname;
					}
				}
				$message = "";
				if(strlen($v->l_message)>200){
					$message = substr($v->l_message,0,200).'...';
				}else{
					$message = $v->l_message;
				}
				
				$finalList[] = array(
					'id' => $v->id,
					'v_image' => $v_image,
					'v_fname' => $v_fname,
					'v_lname' => $v_lname,
					'v_subject_title' => $v->v_subject_title,
					'l_message' => $message,
					'd_added' => date("h:m a", strtotime($v->d_added)),
					'v_human' => GeneralHelper::time_elapsed_string(strtotime($v->d_added)),
				);
			}
		}

		$resdata = GeneralHelper::apiPagination($data,$postData);
		$resdata['message_data'] = $finalList; 

		// $data = array(
		// 	'current_page' =>$currentPage,
		// 	'total_page' => $total_page,
		// 	'data_limit' => $dataLimit,
		// 	'message_data' => $finalList,
		// );

	
		$responseData = array(
			'code' => 200,
			'message' => Lang::get('api.message.list'),
			'data' => $resdata,
		);
		return response()->json($responseData, 200);
	}

	public function detail()
	{

		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;


		$query = MessagesModel::query();
		//$data = $query->where('i_parent_id', $post_data['id'])->orderBy("d_added", "ASC")->get();

		$query = MessagesModel::query();
		$query = $query->where(function($query) use($post_data){
			$query->where('i_parent_id', $post_data['id'])
				  ->orWhere('_id', $post_data['id']);
		});
		$data = $query->orderBy("d_added","ASC")->get();
	
		$finalList = array();
		if (count($data)) {
			foreach ($data as $k => $v) {

				$v_image = "";
				$v_fname = "";
				$v_lname = "";

				if (count($v->hasFromUser()) && isset($v->hasFromUser()->v_image)) {
					$v_image = $v->hasFromUser()->v_image;
				}
				if ($v_image != "") {
					$v_image = \Storage::cloud()->url($v_image);
				} else {
					$v_image = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
				}
				if (count($v->hasFromUser()) && isset($v->hasFromUser()->v_fname)) {
					$v_fname = $v->hasFromUser()->v_fname;
				}
				if (count($v->hasFromUser()) && isset($v->hasFromUser()->v_lname)) {
					$v_lname = $v->hasFromUser()->v_lname;
				}

				$finalList[] = array(
					'id' => $v->id,
					'i_parent_id' => $v->i_parent_id,
					'v_image' => $v_image,
					'v_fname' => $v_fname,
					'v_lname' => $v_lname,
					'v_subject_title' => $v->v_subject_title,
					'isSendByMe' => ($userid == $v->i_from_id) ? 1 : 0,
					'isRead' => ($v->e_view) == 'read' ? 1 : 0,
					'l_message' => $v->l_message,
					'd_added' => date("h:m a", strtotime($v->d_added)),
					'v_human' => GeneralHelper::time_elapsed_string(strtotime($v->d_added)),
				);
			}
		}

		$data = array(
			'message_data' => $finalList,
		);


		$responseData = array(
			'code' => 200,
			'message' => Lang::get('api.message.detail'),
			'data' => $data,
		);
		return response()->json($responseData, 200);
	}

	public function sendMessage()
	{

		$post_data = Request::all();

		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$rules = [
			'i_parent_id' => 'required',
			'l_message' => 'required',
		];

		$validator = Validator::make($post_data, $rules);
		if ($validator->fails()) {
			$responseData = array(
				'code' => 400,
				'message' => $validator->errors()->first(),
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		$mddata = MessagesModel::find($post_data['i_parent_id']);
		$mddata->d_modified = date("Y-m-d H:i:s");
		$mddata->save();

		$lastdata = MessagesModel::where('i_parent_id', $post_data['i_parent_id'])->orderBy('d_added', "DESC")->first();
		$totaldiff = 0;
		if (count($lastdata)) {
			$ctime = date("Y-m-d H:i:s");
			$ltime = $lastdata->d_added;
			$totaldiff = strtotime($ctime) - strtotime($ltime);
			$totaldiff = $totaldiff; //number_format($totaldiff/3600,2);
		}

		$toid = "";
		if ($mddata->i_to_id == $userid) {
			$toid = $mddata->i_from_id;
		} else {
			$toid = $mddata->i_to_id;
		}

		$data['i_parent_id'] = $post_data['i_parent_id'];
		$data['v_subject_title'] = "";
		$data['i_to_id'] = $toid;
		$data['i_from_id'] = $userid;
		$data['v_replay_time'] = $totaldiff;
		$data['l_message'] = $post_data['l_message'];
		$data['e_view'] = "unread";
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		$mData = MessagesModel::create($data);
		self::sendEmailNotificationForMessage($toid, $post_data['l_message']);

		$v_image = "";
		if (isset($authdata->v_image) && $authdata->v_image != "") {
			$v_image = \Storage::cloud()->url($authdata->v_image);
		} else {
			$v_image = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
		}

		$finalList = array(
			'id' => $mData->id,
			'v_image' => $v_image,
			'v_fname' => $authdata->v_fname,
			'v_lname' => $authdata->v_lname,
			'v_subject_title' => $mData->v_subject_title,
			'l_message' => $mData->l_message,
			'd_added' => date("h:m a", strtotime($mData->d_added)),
			'v_human' => GeneralHelper::time_elapsed_string(strtotime($mData->d_added)),
		);
		$data = array(
			'message_data' => $finalList,
		);

		$responseData = array(
			'code' => 200,
			'message' => Lang::get('api.message.add'),
			'data' => $data,
		);
		return response()->json($responseData, 200);
	}

	public function sendEmailNotificationForMessage($toid = "", $message = "")
	{

		if ($toid == "") {
			return 0;
		}

		$userdata = UsersModel::find($toid);
		if (!count($userdata)) {
			return 0;
		}
		$data['USER_FULL_NAME'] = "";
		$data['FROM_NAME'] = "";
		$data['MESSAGE'] = $message;
		$data['USER_EMAIL'] = "";

		if (isset(auth()->guard('web')->user()->v_fname)) {
			$data['FROM_NAME'] = auth()->guard('web')->user()->v_fname;
		}
		if (isset(auth()->guard('web')->user()->v_lname)) {
			$data['FROM_NAME'] .= ' ' . auth()->guard('web')->user()->v_lname;
		}

		if (isset($userdata->v_fname)) {
			$data['USER_FULL_NAME'] = $userdata->v_fname;
		}

		if (isset($userdata->v_lname)) {
			$data['USER_FULL_NAME'] .= ' ' . $userdata->v_lname;
		}
		if (isset($userdata->v_email)) {
			$data['USER_EMAIL'] = $userdata->v_email;
		}

		$mailcontent = EmailtemplateHelper::sendMessage($data);
		$subject = "Message";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5bc1990376fbae0d22218954");

		$mailids = array(
			$data['USER_FULL_NAME'] => $data['USER_EMAIL'],
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject, $mailcontent, $mailids);
	}
}
