<?php
namespace App\Http\Controllers\Api\V1;

// use Request, Hash, Lang,Crypt,Session,Validator,Auth;
use Request, Hash,Lang,Auth,Session,Redirect,Storage,File,Validator;

use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Users as UsersModel;

class SocailLogin extends Controller {

	protected $section;
	private $social_settings;
	
	public function __construct(\MangoPay\MangoPayApi $mangopay){
		$this->social_settings = array();
		$this->section = Lang::get('section.dashboard');
		$this->mangopay = $mangopay;
	}


	public function index() {
		
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		$data = Request::all(); 

		$rules = [
			'type' => 'required',
			'token' => 'required',
			'v_device_token' => 'required',
        ];
		
		$validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}


		if($data['type']=="google"){
			
			// from acces token
			// $url = "https://www.googleapis.com/plus/v1/people/me?access_token=".$data['token'];

			// from id token
			$url = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=".$data['token'];

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET"
				
			));
			$geocodeFromLatLong = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			$output = json_decode($geocodeFromLatLong);

			// print_r($output); exit;
			

			// ***********************************from access token ****************** //
			// if(!isset($output->id)){
			// 	$responseData=array(
			// 		'code'=>400,
			// 		'message'=>"Invalid Socail Media token",
			// 		'data'=>array(),
			// 	);
			// 	return response()->json($responseData, 400);
			// }

			// $authUser = UsersModel::where('v_facebook_id', $output->id)->first();

			// $email = "";
			// if(isset($output->emails['0']->value)){
			// 	$email = $output->emails['0']->value;	
			// }
			// $v_fname=$output->name->givenName;
			// $v_lname=$output->name->familyName;
			
			// if(count($authUser)){

			// 	$token = GeneralHelper::generateToken();
			// 	$userid = $authUser->id;
			// 	$update['v_device_token']=$data['v_device_token'];
			// 	$update['v_auth_token']=$token;
			// 	$update['v_fname'] = $output->name->givenName;
			// 	$update['v_lname'] = $output->name->familyName;
			// 	$update['v_email'] = $email;
			// 	$update['d_modified']=date("Y-m-d H:i:s");
			// 	UsersModel::find($userid)->update($update);
				
			// 	$resdata['user']['v_fname'] = $output->name->givenName;
			// 	$resdata['user']['v_lname'] = $output->name->familyName;
			// 	$resdata['user']['v_email'] = $email;
			// 	$resdata['user']['v_image'] = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/".$authUser->v_image;
			// 	$resdata['user']['v_auth_token'] = $token;
				
			// 	$responseData=array(
			// 		'code'=>200,
			// 		'message'=>Lang::get('api.login.loginSuccess'),
			// 		'data'=>$resdata,
			// 	);
			// 	return response()->json($responseData, 200);
			// }else{

			// 	$token = GeneralHelper::generateToken();
			// 	$update['v_device_token']=$data['v_device_token'];
			// 	$update['v_auth_token']=$token;
			// 	$update['v_fname'] = $output->name->givenName;
			// 	$update['v_lname'] = $output->name->familyName;
			// 	$update['v_email'] = $email;
			// 	$update['id'] = $output->id;
			// 	$res = self::loginWithSignup($update);

			// 	if(!count($res)){
			// 		$responseData=array(
			// 			'code'=>400,
			// 			'message'=>"Invalid Socail Media token",
			// 			'data'=>array(),
			// 		);
			// 		return response()->json($responseData, 400);
			// 	}
			// 	$resdata['user']['v_fname'] = $res->v_fname;
			// 	$resdata['user']['v_lname'] = $res->v_lname;
			// 	$resdata['user']['v_email'] = $res->v_email;
			// 	$resdata['user']['v_image'] = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/".$res->v_image;
			// 	$resdata['user']['v_auth_token'] = $res->v_auth_token;
				
			// 	$responseData=array(
			// 		'code'=>200,
			// 		'message'=>Lang::get('api.login.loginSuccess'),
			// 		'data'=>$resdata,
			// 	);
			// 	return response()->json($responseData, 200);
			// }


			// ***********************************from idtoken ****************** //
			if(!isset($output->sub)){
				$responseData=array(
					'code'=>400,
					'message'=>"Invalid Socail Media token",
					'data'=>array(),
				);
				return response()->json($responseData, 400);
			}

			$authUser = UsersModel::where('v_facebook_id', $output->sub)->first();

			$email = "";
			if(isset($output->email)){
				$email = $output->email;	
			}
			$v_fname=$output->given_name;
			$v_lname=$output->family_name;
			
			if(count($authUser)){

				$token = GeneralHelper::generateToken();
				$userid = $authUser->id;
				$update['v_device_token']=$data['v_device_token'];
				$update['v_auth_token']=$token;
				$update['v_fname'] = $v_fname;
				$update['v_lname'] = $v_lname;
				$update['v_email'] = $email;
				$update['d_modified']=date("Y-m-d H:i:s");
				UsersModel::find($userid)->update($update);
				
				$resdata['user']['v_fname'] = $v_fname;
				$resdata['user']['v_lname'] = $v_lname;
				$resdata['user']['v_email'] = $email;
				$resdata['user']['v_image'] = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/".$authUser->v_image;
				$resdata['user']['v_auth_token'] = $token;
				
				$responseData=array(
					'code'=>200,
					'message'=>Lang::get('api.login.loginSuccess'),
					'data'=>$resdata,
				);
				return response()->json($responseData, 200);
			}else{

				$token = GeneralHelper::generateToken();
				$update['v_device_token']=$data['v_device_token'];
				$update['v_auth_token']=$token;
				$update['v_fname'] = $v_fname;
				$update['v_lname'] = $v_lname;
				$update['v_email'] = $email;
				$update['id'] = $output->sub;
				$res = self::loginWithSignup($update);

				if(!count($res)){
					$responseData=array(
						'code'=>400,
						'message'=>"Invalid Socail Media token",
						'data'=>array(),
					);
					return response()->json($responseData, 400);
				}
				$resdata['user']['v_fname'] = $res->v_fname;
				$resdata['user']['v_lname'] = $res->v_lname;
				$resdata['user']['v_email'] = $res->v_email;
				$resdata['user']['v_image'] = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/".$res->v_image;
				$resdata['user']['v_auth_token'] = $res->v_auth_token;
				
				$responseData=array(
					'code'=>200,
					'message'=>Lang::get('api.login.loginSuccess'),
					'data'=>$resdata,
				);
				return response()->json($responseData, 200);
			}
		
		}else if($data['type']=="facebook"){


			$url = "https://graph.facebook.com/me?fields=id,name,email&access_token=".$data['token'];
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET"
				
			));
			$geocodeFromLatLong = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			$output = json_decode($geocodeFromLatLong);

			if(!isset($output->id)){
				$responseData=array(
					'code'=>400,
					'message'=>"Invalid Socail Media token",
					'data'=>array(),
				);
				return response()->json($responseData, 400);
			}
			$authUser = UsersModel::where('v_facebook_id', $output->id)->first();
			
			$email = "";
			if(isset($output->email) && $output->email!=""){
				$email = $output->email;	
			}

			$v_fname="";
			$v_lname="";
			if(isset($output->name) && $output->name!=""){
				$split = explode(" ",isset($output->name) ? $output->name : '');	
				if(isset($split[0])){
					$v_fname=$split[0];
				}
				if(isset($split[0])){
					$v_lname=$split[1];
				}
			}
			
			if(count($authUser)){
				
				$update=array();
				$update['v_fname']=$v_fname;
				$update['v_lname']=$v_lname;
				
				$token = GeneralHelper::generateToken();
				$userid = $authUser->id;
				$update['v_device_token']=$data['v_device_token'];
				$update['v_auth_token']=$token;
				$update['d_modified']=date("Y-m-d H:i:s");
				UsersModel::find($userid)->update($update);
				
				$resdata['user']['v_fname'] = $update['v_fname'];
				$resdata['user']['v_lname'] = $update['v_lname'];
				$resdata['user']['v_email'] = $email;
				$resdata['user']['v_image'] = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/".$authUser->v_image;
				$resdata['user']['v_auth_token'] = $token;
				
				$responseData=array(
					'code'=>200,
					'message'=>Lang::get('api.login.loginSuccess'),
					'data'=>$resdata,
				);
				return response()->json($responseData, 200);

			}else{

				
				$token = GeneralHelper::generateToken();
				$update['v_device_token']=$data['v_device_token'];
				$update['v_auth_token']=$token;
				$update['v_fname'] = $v_fname;
				$update['v_lname'] = $v_lname;
				$update['v_email'] = $email;
				$update['id'] = $output->id;
				$res = self::loginWithSignup($update);
				
				if(!count($res)){
					$responseData=array(
						'code'=>400,
						'message'=>"Invalid Socail Media token",
						'data'=>array(),
					);
					return response()->json($responseData, 400);
				}
				$resdata['user']['v_fname'] = $res->v_fname;
				$resdata['user']['v_lname'] = $res->v_lname;
				$resdata['user']['v_email'] = $res->v_email;
				$resdata['user']['v_image'] = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/".$res->v_image;
				$resdata['user']['v_auth_token'] = $res->v_auth_token;
				
				$responseData=array(
					'code'=>200,
					'message'=>Lang::get('api.login.loginSuccess'),
					'data'=>$resdata,
				);
				return response()->json($responseData, 200);
			}
		}
	}

	public function loginWithSignup($data) {

		$update=array();
		$update['v_fname']=$data['v_fname'];
		$update['v_lname']=$data['v_lname'];
		$uname = $data['v_fname'].''.$data['v_lname'];

		$avatar = new \LasseRafn\InitialAvatarGenerator\InitialAvatar();
		$image_avatar = $avatar->name($uname)
			->length(2)
			->fontSize(0.5)
			->size(250) 
			->background('#ddd')
			->color('#222')
			->generate()
			->stream('png', 100);

		$fileName = 'profile-'.time().'.png';
		$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, $image_avatar->__toString(), 'public');     
		$update['v_image']="users/".$fileName;
		
		if(isset($data['v_email']) && $data['v_email']!=""){

			$update['v_email'] = $data['v_email'];
			$res = self::createMangopayUser($update);
			$update['i_mangopay_id']= $res['i_mangopay_id'];
			$update['i_wallet_id']= $res['i_wallet_id'];
		}
		
		$update['buyer']['v_service']="online";
		$update['buyer']['e_status']="active";
		$update['seller']['v_service']="online";
		$update['seller']['e_status']="inactive";
		$update['v_plan']['id']="5a65b48cd3e812a4253c9869";
		$update['v_plan']['duration']="monthly";
		$update['v_plan']['d_date']=date("Y-m-d H:i:s");
		$update['v_plan']['d_start_date']=date("Y-m-d H:i:s");
		$update['v_plan']['d_end_date']=date('Y-m-d',strtotime('+4000 days'));
		$update['v_buyer_online_service'] = "active";
		$update['v_buyer_inperson_service'] = "inactive";
		$update['v_seller_online_service'] = "inactive";
		$update['v_seller_inperson_service'] = "inactive";
		$update['v_level']="0";
		$update['v_replies_time']="24 hours";
		$update['i_total_avg_review']=0;
		$update['i_total_review']=0;
		$update['i_course_total_review']=0;
		$update['i_course_total_avg_review']=0;
		$update['i_job_total_review']=0;
		$update['i_job_total_avg_review']=0;
		$update['v_company_type']="INDIVIDUAL";
		$update['e_login']="yes";
		$update['v_facebook_id'] = $data['id'];
		$update['i_newsletter'] = "on";
		$update['e_status'] = "active";
		$update['e_email_confirm'] = "yes";	
		$update['d_added'] = date('Y-m-d H:i:s');
		$update['d_modified'] = date('Y-m-d H:i:s');
		$update['v_device_token']=$data['v_device_token'];
		$update['v_auth_token']=$data['v_auth_token'];
		return UsersModel::create($update);

	}

	public function createMangopayUser($data){
		
		$response['i_mangopay_id']=0;
		$response['i_wallet_id']=0;

		if(!isset($data['v_fname'])){
			$data['v_fname']="First name";	
		}
		if(!isset($data['v_lname'])){
			$data['v_fname']="Last name";	
		}

		$UserNatural = new \MangoPay\UserNatural();
		$UserNatural->FirstName = $data['v_fname'];//"First Name";
		$UserNatural->LastName = $data['v_lname'];
		$UserNatural->Birthday = 1463496101;
		$UserNatural->Nationality = "GB";
		$UserNatural->Email = $data['v_email'];
		$UserNatural->CountryOfResidence = "GB";
		$result = $this->mangopay->Users->Create($UserNatural);

		if(count($result)){
			
			$mangopayid = $result->Id;
			$Wallet = new \MangoPay\Wallet();
			$Wallet->Tag = "custom meta";
			$Wallet->Owners = array ($result->Id);
			$Wallet->Description = "My skillbox project";
			$Wallet->Currency = "GBP";
			$resultWallet = $this->mangopay->Wallets->Create($Wallet);
			$response['i_mangopay_id']=$result->Id;
			$response['i_wallet_id']=$resultWallet->Id;
		
		}
		return $response;
	}

	

	

}