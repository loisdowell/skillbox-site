<?php
namespace App\Http\Controllers\Api\V1\Search;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage,File;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Support\SupportQuery as SupportQueryModel;
use App\Models\Country as CountryModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Sellerreview as SellerreviewModal;
use App\Models\CompanyEthos as CompanyEthosModal;
use App\Models\Users\Buyerreview as BuyerreviewModal;
use App\Models\Pricefilter as PricefilterModal;
use App\Models\ManageMetaField as ManageMetaFieldModel;
use App\Models\Users\Order as OrderModal;
use App\Models\Search as SearchModal;
use App\Models\Emailsend as Emailsend;
use App\Models\Sitesettings as SiteSettingsModel;
use App\Models\Guide as GuideModel;


class Search extends Controller {

	
	public function index() {
		
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
	 
		$data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;   

		$rules = [
			'e_type' => 'required',
			'e_type_data' => 'required',
		];
		
		$validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		if($data['e_type_data']=="skill"){
			$data['v_type_skill']="on";
		}
		if($data['e_type_data']=="job"){
			$data['v_type_job']="on";
		}
		
		if(isset($data['e_type']) && $data['e_type']=="online"){
			
			if(!isset($data['v_type_skill']) && !isset($data['v_type_job'])){
				$data['v_type_skill'] = "on";
			}

			$pricefilter = PricefilterModal::where("e_status","active")->orderBy("i_order")->get();
			$user_ids = $skill_users = $job_users = $s_skill = $s_job = array();
			$finalresult=array();
			$finalresultmob=array();
			$existskillids=array();
			$existjobids=array();


			$shortlistedSkill = ShortlistedSkillsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedSkill as $key => $value) {
				$s_skill[] = $value->i_shortlist_id;
			}

			$shortlistedJobs = ShortlistedJobsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedJobs as $key => $value) {
				$s_job[] = $value->i_shortlist_id;
			}


			$totaljob = 0;
			$totalskill = 0;

			//Bold Adds Skill
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
				
				$skilldata = GeneralHelper::SkillSearchBold($data,$pricefilter);
				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
						);
						if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){							
							$update=array();	
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;			
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}

							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}
						$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			
			  		}
			  	}
			}

		  	if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
		  		
		  		$jobsdata = GeneralHelper::JobSearchBold($data,$pricefilter);
		  	  	
		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {
						  
						
			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							
							$update['i_impression']=1;
							if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
			  			}
			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
						);
						$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob = $totaljob+1;
			  		}
			  	}
			}

			//Latest user data 24 hours
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
				
				$skilldata = GeneralHelper::SkillSearchNew($data,$existskillids,$pricefilter);
				
				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
						);

						if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){							
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;			
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}
						
						$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			

			  		}
			  	}
			}

			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
		  		
		  		$jobsdata = GeneralHelper::JobSearchNew($data,$pricefilter,$existjobids);
		  	  	
		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {
						  
						
			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							  $update['i_impression']=1;
							if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}

							JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
						  $totaljob =$totaljob+1;
						  
					}
			  	}
			}

			// urgent Job Listing
			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
		  		
		  		$jobsdata = GeneralHelper::JobSearchUrgent($data,$pricefilter,$existjobids);
		  	  	
		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {
						if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update['i_impression']=1;
							if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}

				  			JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
						  $totaljob =$totaljob+1;
						  
					}
			  	}
			}

			//Normal Listing
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
				
				$skilldata = GeneralHelper::SkillSearchNoraml($data,$existskillids,$pricefilter);
				
				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
						  );
						  
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;
						
						if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){							
							$update=array();	
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;			
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

						$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
						$totalskill = $totalskill+1;
						  

			  		}
			  	}
			}

			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
		  		
		  		$jobsdata = GeneralHelper::JobSearchNormal($data,$pricefilter,$existjobids);
		  	  	
		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {
						
			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update['i_impression']=1;
							if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
						  $totaljob =$totaljob+1;
						  
			  		}
			  	}
			}
		 
		  	$user_datalist = UsersModel::query()->whereIn('_id', $user_ids)->get();
		  	$user_data=array();
		  	foreach ($user_datalist as $key => $value) {
			  	$user_data[$value->_id] =  $value;
			}
			
			$categories = CategoriesModel::where("e_status","active")->where('v_type',"online")->orderBy('v_name')->get();
			$skills=array();
			if(isset($data['i_category_id']) && $data['i_category_id']!=""){	
				$skills = SkillsModel::where('i_category_id',$data['i_category_id'])->orderBy('v_name')->get();
			}

			$_S3_PATH = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/";//env("IMAP_HOSTNAME_TEST", "https://skillboxs3bucket.s3.us-east-2.amazonaws.com");
			
			$finalresultmob=array();
			if(count($finalresult)){
				foreach($finalresult as $k=>$v){
					
					if($v['type'] == "jobs"){

						$f_name="";
						$l_name="";
						$user_image="";
						if(isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])){
							if(isset($user_data[$v['data']['i_user_id']]['v_fname'])){
								$f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
							}
							if(isset($user_data[$v['data']['i_user_id']]['v_image'])){
								$user_image = $_S3_PATH.$user_data[$v['data']['i_user_id']]['v_image'];
							}
							if(isset($user_data[$v['data']['i_user_id']]['v_lname'])){
								$l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
							}
						} 
						
						$imgins="";
						if(isset($v['data']['v_photos'][0])){
							$imgins = $v['data']['v_photos'][0];
						} 
						$imgdata="";
						if($imgins!=""){
							$imgdata = $_S3_PATH.'/'.$imgins;    
						}else{
							$imgdata = $_S3_PATH.'/jobpost/photos/No_Image_Available.jpg';          
						}

						$e_sponsor_status=0;
			  			if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status']=="start"){
							$e_sponsor_status=1;
						}
						$is_shortlist=0;
						if(in_array($v['data']['_id'], $s_job)){
							$is_shortlist=1;
						}
						$is_urgent=0;
						if(isset($v['data']['e_urgent']) && $v['data']['e_urgent']=="yes"){
							$is_urgent=1;	
						}
						$finalresultmob[]=array(
							'type'=>"job",
							'id'=>$v['data']['_id'],
							'v_user_name'=>$f_name.' '.$l_name,
							'v_user_image'=>$user_image,
							'v_image'=>$imgdata,
							'v_title'=>$v['data']['v_job_title'],
							'i_user_id'=>$v['data']['i_user_id'],
							'l_short_description'=>$v['data']['l_short_description'],
							'i_total_avg_review'=>number_format($value['data']['i_total_avg_review'],2),
							'i_total_review'=>$v['data']['i_total_review'],
							'is_shortlist'=>$is_shortlist,
							'e_sponsor_status'=>$e_sponsor_status,
							'v_budget_amt'=>$v['data']['i_total_review'],
							'v_budget_type'=>$v['data']['v_budget_type'],
							'is_urgent'=>$is_urgent,
						);
					
					}else{

						$f_name="";
						$l_name="";
						$user_image="";
						if(isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])){
							if(isset($user_data[$v['data']['i_user_id']]['v_fname'])){
								$f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
							}
							if(isset($user_data[$v['data']['i_user_id']]['v_image'])){
								$user_image = $_S3_PATH.$user_data[$v['data']['i_user_id']]['v_image'];
							}
							if(isset($user_data[$v['data']['i_user_id']]['v_lname'])){
								$l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
							}
						} 

						$is_shortlist=0;
						if(in_array($v['data']['_id'], $s_skill)){
							$is_shortlist=1;
						}
						$e_sponsor_status=0;
						if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status']=="start"){							
							$e_sponsor_status=1;
						}

						$finalresultmob[]=array(
							'type'=>"skill",
							'id'=>$v['data']['_id'],
							'v_user_name'=>$f_name.' '.$l_name,
							'v_user_image'=>$user_image,
							'v_title'=>$v['data']['v_profile_title'],
							'i_user_id'=>$v['data']['i_user_id'],
							'l_short_description'=>$v['data']['l_short_description'],
							'i_total_avg_review'=>number_format($v['data']['i_total_avg_review'],2),
							'i_total_review'=>$v['data']['i_total_review'],
							'v_starting_at'=>$v['data']['information']['basic_package']['v_price'],
							'is_shortlist'=>$is_shortlist,
							'e_sponsor_status'=>$e_sponsor_status,
							'type'=>"skill",
						);


					}


				}
			}
			
			// $skillsidebar = GeneralHelper::SkillSearchBoldSidebar($data,$pricefilter);
			// $jobsidebar = GeneralHelper::JobSearchBoldSidebar($data,$pricefilter);

			// $skillsidebar1=array();	
			// if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
			// 	$skillsidebar1 = $skillsidebar;
			// }
			
			// $jobsidebar1=array();
			// if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
			// 	$jobsidebar1 = $jobsidebar;
			// }

			$_data=array(
				'v_service'=>"online",
				'data'=> $data,
				'list'=> $finalresultmob,
				'totaljob' => $totaljob,
				'totalskill' => $totalskill,
				'categories' => $categories,
				'skills' => $skills,
				// 'skillsidebar'=>$skillsidebar,
				// 'jobsidebar'=>$jobsidebar,
				// 'skillsidebar1'=>$skillsidebar1,
				// 'jobsidebar1'=>$jobsidebar1,
			);
			
			$responseData=array(
				'code'=>200,
				'message'=>"",
				'data'=>$_data,
			);
			return response()->json($responseData, 200);
			
		}

		else if(isset($data['e_type']) && $data['e_type']=="inperson"){

			if(!isset($data['v_type_skill']) && !isset($data['v_type_job'])){
				$data['v_type_skill'] = "on";
			}
			
			$pricefilter = PricefilterModal::where("e_status","active")->orderBy("i_order")->get();
			$milesfilter = array(
				'5'=>"5",
				'10'=>"10",
				'15'=>"15",
				'25'=>"25",
				'50'=>"50",
			);

			$user_ids = $skill_users = $job_users = $s_skill = $s_job = array();
			$finalresult=array();
			$existskillids=array();
			$existjobids=array();

			$totaljob = 0;
			$totalskill = 0;

			//Bold Adds Skill
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
				$skilldata = GeneralHelper::InpersonSkillSearchBold($data,$pricefilter);
				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){							
							$update=array();	
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;			
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

			  		}
			  	}
			}

		  	if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
		  		
		  		$jobsdata = GeneralHelper::InpersonJobSearchBold($data,$pricefilter);
		  	  	
		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {
			  			
			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
				  			JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

			//Latest user data 24 hours
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
				
				$skilldata = GeneralHelper::InpersonSkillSearchNew($data,$existskillids,$pricefilter);
				
				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){							
							$update=array();	
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;			
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

			  		}
			  	}
			}

			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
		  		
		  		$jobsdata = GeneralHelper::InpersonJobSearchNew($data,$pricefilter,$existjobids);
		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {
			  			
			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
				  			JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

			// urgent Job Listing
			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
		  		
		  		$jobsdata = GeneralHelper::InpersonJobSearchUrgent($data,$pricefilter,$existjobids);
		  	  	
		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {
			  			
			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}
			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

			//Normal Listing
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
				
				$skilldata = GeneralHelper::InpersonSkillSearchNoraml($data,$existskillids,$pricefilter);
				
				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){							
							$update=array();	
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;			
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

			  		}
			  	}
			}

			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
		  		
		  		$jobsdata = GeneralHelper::InpersonJobSearchNormal($data,$pricefilter,$existjobids);
		  	  	
		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {
			  			
			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;			
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;			
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

		  	$user_datalist = UsersModel::query()->whereIn('_id', $user_ids)->get();
		  	$user_data=array();
		  	foreach ($user_datalist as $key => $value) {
			  	$user_data[$value->_id] =  $value;
			}

		  	$shortlistedSkill = ShortlistedSkillsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedSkill as $key => $value) {
				$s_skill[] = $value->i_shortlist_id;
			}

			$shortlistedJobs = ShortlistedJobsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedJobs as $key => $value) {
				$s_job[] = $value->i_shortlist_id;
			}

			$categories = CategoriesModel::where("e_status","active")->where('v_type',"inperson")->orderBy('v_name')->get();
			$skills=array();
			if(isset($data['i_category_id']) && $data['i_category_id']!=""){	
				$skills = SkillsModel::where('i_category_id',$data['i_category_id'])->get();
			}

			// $skillsidebar = GeneralHelper::InpersonSkillSearchBoldSidebar($data,$pricefilter);
			// $jobsidebar = GeneralHelper::InpersonJobSearchBoldSidebar($data,$pricefilter);

			// $skillsidebar1=array();	
			// if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
			// 	$skillsidebar1 = $skillsidebar;
			// }
			
			// $jobsidebar1=array();
			// if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
			// 	$jobsidebar1 = $jobsidebar;
			// }
			

			$_S3_PATH = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/";//env("IMAP_HOSTNAME_TEST", "https://skillboxs3bucket.s3.us-east-2.amazonaws.com");
			
			$finalresultmob=array();
			if(count($finalresult)){
				foreach($finalresult as $k=>$v){
					
					if($v['type'] == "jobs"){

						$f_name="";
						$l_name="";
						$user_image="";
						if(isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])){
							if(isset($user_data[$v['data']['i_user_id']]['v_fname'])){
								$f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
							}
							if(isset($user_data[$v['data']['i_user_id']]['v_image'])){
								$user_image = $_S3_PATH.$user_data[$v['data']['i_user_id']]['v_image'];
							}
							if(isset($user_data[$v['data']['i_user_id']]['v_lname'])){
								$l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
							}
						} 
						
						$imgins="";
						if(isset($v['data']['v_photos'][0])){
							$imgins = $v['data']['v_photos'][0];
						} 

						$imgdata="";
						if($imgins!=""){
							$imgdata = $_S3_PATH.'/'.$imgins;    
						}else{
							$imgdata = $_S3_PATH.'/jobpost/photos/No_Image_Available.jpg';          
						}

						$e_sponsor_status=0;
			  			if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status']=="start"){
							$e_sponsor_status=1;
						}
						$is_shortlist=0;
						if(in_array($v['data']['_id'], $s_job)){
							$is_shortlist=1;
						}
						$is_urgent=0;
						if(isset($v['data']['e_urgent']) && $v['data']['e_urgent']=="yes"){
							$is_urgent=1;	
						}
						$finalresultmob[]=array(
							'type'=>"job",
							'id'=>$v['data']['_id'],
							'v_user_name'=>$f_name.' '.$l_name,
							'v_user_image'=>$user_image,
							'v_image'=>$imgdata,
							'v_title'=>$v['data']['v_job_title'],
							'i_user_id'=>$v['data']['i_user_id'],
							'l_short_description'=>$v['data']['l_short_description'],
							'i_total_avg_review'=>number_format($value['data']['i_total_avg_review'],2),
							'i_total_review'=>$v['data']['i_total_review'],
							'is_shortlist'=>$is_shortlist,
							'e_sponsor_status'=>$e_sponsor_status,
							'v_budget_amt'=>$v['data']['i_total_review'],
							'v_budget_type'=>$v['data']['v_budget_type'],
							'is_urgent'=>$is_urgent,
						);
					
					}else{

						$f_name="";
						$l_name="";
						$user_image="";
						if(isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])){
							if(isset($user_data[$v['data']['i_user_id']]['v_fname'])){
								$f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
							}
							if(isset($user_data[$v['data']['i_user_id']]['v_image'])){
								$user_image = $_S3_PATH.$user_data[$v['data']['i_user_id']]['v_image'];
							}
							if(isset($user_data[$v['data']['i_user_id']]['v_lname'])){
								$l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
							}
						} 

						$is_shortlist=0;
						if(in_array($v['data']['_id'], $s_skill)){
							$is_shortlist=1;
						}
						$e_sponsor_status=0;
						if(isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status']=="start"){							
							$e_sponsor_status=1;
						}

						$finalresultmob[]=array(
							'type'=>"skill",
							'id'=>$v['data']['_id'],
							'v_user_name'=>$f_name.' '.$l_name,
							'v_user_image'=>$user_image,
							'v_title'=>$v['data']['v_profile_title'],
							'i_user_id'=>$v['data']['i_user_id'],
							'l_short_description'=>$v['data']['l_short_description'],
							'i_total_avg_review'=>number_format($v['data']['i_total_avg_review'],2),
							'i_total_review'=>$v['data']['i_total_review'],
							'v_starting_at'=>$v['data']['information']['basic_package']['v_price'],
							'is_shortlist'=>$is_shortlist,
							'e_sponsor_status'=>$e_sponsor_status,
							'type'=>"skill",
						);


					}


				}
			}


			$_data=array(
				'v_service'=>"inperson",
				'data'=> $data,
				'list'=> $finalresultmob,
				'totaljob' => $totaljob,
				'totalskill' => $totalskill,
				'categories' => $categories,
				'skills' => $skills,
				// 'skillsidebar'=>$skillsidebar,
				// 'jobsidebar'=>$jobsidebar,
				// 'skillsidebar1'=>$skillsidebar1,
				// 'jobsidebar1'=>$jobsidebar1,
			);
			
			$responseData=array(
				'code'=>200,
				'message'=>"",
				'data'=>$_data,
			);
			return response()->json($responseData, 200);

		}

		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.supportList'),
			'data'=>$_data,
		);
		return response()->json($responseData, 200);
		
	}

	public function skillDetail(){
	
		$pdata = Request::all(); 
		$authdata = Request::get('authdata');

		$rules = [
			'v_skill_id' => 'required',
		];
		
		$validator = Validator::make($pdata, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		$id=$pdata['v_skill_id'];
		
		$sellerprofile = SellerprofileModel::find($id);

		if(!count($sellerprofile)){

			$responseData=array(
                'code'=>400,
                'message'=>"Something went wrong.",
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		
		if(isset($sellerprofile->i_delete) && $sellerprofile->i_delete=="1"){
			$responseData=array(
                'code'=>400,
                'message'=>"Something went wrong.",
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		
		if(isset($sellerprofile->e_sponsor_status) && $sellerprofile->e_sponsor_status == "start"){
			$update['i_clicks']=1;
			if(isset($sellerprofile->i_clicks)){
				$update['i_clicks'] = $sellerprofile->i_clicks+1;
			}
			SellerprofileModel::find($id)->update($update);
		}
		
		$res['v_introducy_video']="";
		$res['v_work_video']="";
		$res['v_work_photos']=array();

		$videoData=array();
		if(isset($sellerprofile->v_introducy_video) && $sellerprofile->v_introducy_video!=""){
			$res['v_introducy_video'] = \Storage::cloud()->url($sellerprofile->v_introducy_video); 
		}
		
		if(isset($sellerprofile->v_work_video) && count($sellerprofile->v_work_video)){
			foreach($sellerprofile->v_work_video as $k=>$v){
				$res['v_work_video'] = \Storage::cloud()->url($v); 
			}
		}
	
		$imagData=array();
		if(isset($sellerprofile->v_work_photos) && count($sellerprofile->v_work_photos)){
			foreach($sellerprofile->v_work_photos as $k=>$v){
				$imagData[] = \Storage::cloud()->url($v); 
			}
			$res['v_work_photos'] = $imagData;
		}

		$res['v_service']=$sellerprofile->v_service;
		$res['id']=$sellerprofile->id;
		$res['l_short_description']=$sellerprofile->l_short_description;
		$res['l_brief_description']=$sellerprofile->l_brief_description;
		$res['starting_at'] = isset($sellerprofile->information['basic_package']['v_price']) ? $sellerprofile->information['basic_package']['v_price']:'';
		$res['v_hourly_rate']=$sellerprofile->v_hourly_rate;

		$v_image="";
		if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_image)){
			$v_image=\Storage::cloud()->url($sellerprofile->hasUser()->v_image);
		}
		$res['v_user_image']=$v_image;

		$useridseller="";
		if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->id)){
			$useridseller = $sellerprofile->hasUser()->id;
		}


		$v_fname="";
		if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_fname)){
			$v_fname=$sellerprofile->hasUser()->v_fname;
		}
		$res['v_user_fname']=$v_fname;
		
		$res['i_user_id']=$sellerprofile->hasUser()->id;
		$v_lname="";
		if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_lname)){
			$v_lname=$sellerprofile->hasUser()->v_lname;
		}
		$res['v_user_lname']=$v_lname;

		$v_name_main_skill="";
		if(count($sellerprofile->hasSkill()) && isset($sellerprofile->hasSkill()->v_name)){
			$v_name_main_skill=$sellerprofile->hasSkill()->v_name;
		}
		$res['v_name_main_skill']=$v_name_main_skill;
		
		$v_name_other_skill="";
		if(count($sellerprofile->hasOtherSkill()) && isset($sellerprofile->hasOtherSkill()->v_name)){
			$v_name_other_skill=$sellerprofile->hasOtherSkill()->v_name;
		}
		$res['v_name_other_skill']=$v_name_other_skill;


		$other_skill_ids = array();
		if(isset($sellerprofile) && count($sellerprofile)){
			if(isset($sellerprofile->i_otherskill_id) && count($sellerprofile->i_otherskill_id)){
				foreach ($sellerprofile->i_otherskill_id as $key => $value) {
					$other_skill_ids[] = $value;
				}
			}	
		}	
		
		$other_skills_name = SkillsModel::whereIn("_id",$other_skill_ids)->get();
		if(isset($other_skills_name) && count($other_skills_name)){
			foreach($other_skills_name as $key => $value){
				$v_name[] = $value->v_name;
			}
		}
		$v_name = implode(', ', $v_name);
		$res['other_skills_name']=$v_name; 
		$res['v_english_proficiency']=isset($sellerprofile->v_english_proficiency) ? $sellerprofile->v_english_proficiency:''; 


		$v_verified="";
		$i_verified=0;
		
		if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->e_email_confirm)){
			$v_verified= ($sellerprofile->hasUser()->e_email_confirm  == 'yes') ? 'verified' :  'not verified' ;

			if($sellerprofile->hasUser()->e_email_confirm  == 'yes'){
				$i_verified=1;      
			}
		}
		$res['v_user_verified']=$i_verified;


		$availabel="";
		if(isset($sellerprofile->v_avalibility_from) && $sellerprofile->v_avalibility_from!=""){
		  $availabel = ucfirst(substr($sellerprofile->v_avalibility_from, 0, 3)) .' - ';
		}
		if(isset($sellerprofile->v_avalibility_to) && $sellerprofile->v_avalibility_to!=""){
		  $availabel .= ucfirst(substr($sellerprofile->v_avalibility_to, 0, 3)) .' / ';
		}

		if(isset($sellerprofile->v_avalibilitytime_from) && $sellerprofile->v_avalibilitytime_from!=""){
		  $availabel .= date("h a",strtotime($sellerprofile->v_avalibilitytime_from.':00')) .' - ';
		}
		
		if(isset($sellerprofile->v_avalibilitytime_to) && $sellerprofile->v_avalibilitytime_to!=""){
		  $availabel .= date("h a",strtotime($sellerprofile->v_avalibilitytime_to.':00')) ;
		}

		$v_replies_time="";
		if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_replies_time)){
			$v_replies_time = $sellerprofile->hasUser()->v_replies_time;
		}
		$res['v_replies_time']=$v_replies_time;

		$v_plan="";
		if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_plan)){
			$v_plan=$sellerprofile->hasUser()->v_plan;
		}

		$res['availabel']=$availabel;
		$res['city']="";

		if(isset($sellerprofile->v_service) && $sellerprofile->v_service=="inperson" && isset($sellerprofile->v_city) && $sellerprofile->v_city!=""){
			$res['city']=$sellerprofile->v_city;
		}

		$start =strtotime($sellerprofile->hasUser()->d_added);
		$end =  time();
		$diff=$end - $start;
		$days_between = round($diff / 86400);
		if($days_between>30){
			$days_between = round($days_between / 30);
			$days_between = $days_between." Month(s)";
		}else{
			if($days_between>1){
				$days_between = $days_between." day(s)";    
			}else{
				$days_between = "1 day";
			}
		}
		$res['with_skillbox']=$days_between;
		$res['v_website']=$sellerprofile->v_website;
		$res['v_contact_phone']=$sellerprofile->v_contact_phone;
		$res['package']=$sellerprofile->information;
		
		$userid = $authdata->id;   
		$shortlistedSkill = ShortlistedSkillsModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->first();

		$res['is_shortlisted'] = 0;
		if(count($shortlistedSkill) && !empty($shortlistedSkill)){
			$res['is_shortlisted'] = 1;
		}
		
		$leveldata=array();
		if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_level)){
            if($sellerprofile->hasUser()->v_level!="0"){
            	$leveldata = GeneralHelper::LevelData($sellerprofile->hasUser()->v_level);
            }
		}
		$res['leveldata'] = $leveldata;
		$resdata['detail'] = $res;

        $responseData=array(
			'code'=>200,
			'message'=>"",
			'data'=>$resdata,
		);
		return response()->json($responseData, 200);
	
	}

	public function SkillReview(){

		$pdata = Request::all(); 
		$authdata = Request::get('authdata');

		$rules = [
			'v_skill_id' => 'required',
		];
		
		$validator = Validator::make($pdata, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		$id =  $pdata['v_skill_id'];
		
		$sellerprofile = SellerprofileModel::find($id);
		if(!count($sellerprofile)){
			$responseData=array(
                'code'=>400,
                'message'=>"Something went wrong.",
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		$reviewdata = SellerreviewModal::where('i_seller_profile_id',$sellerprofile->id)->get();

		$reviewdataList = array();

		if(isset($reviewdata) && count($reviewdata)){
			foreach($reviewdata as $k=>$v){

				$v_image="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
                    $v_image=$v->hasUser()->v_image;
                }
                $v_fname="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_fname)){
                    $v_fname=$v->hasUser()->v_fname;
                }

                $v_lname="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_lname)){
                    $v_lname=$v->hasUser()->v_lname;
				}

				$imgdata = '';
				if(Storage::disk('s3')->exists($v_image)){
					$imgdata = \Storage::cloud()->url($v_image);
				}else{
					$imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
				}

				$totalavg = $v->i_communication_star+$v->i_described_star+$v->i_recommend_star;
				$totalavg = $totalavg/3;
				
				$vs_image="";
				if(count($v->hasSeller()) && isset($v->hasSeller()->v_image)){
					$vs_image=$v->hasSeller()->v_image;
				}
				$vs_fname="";
				if(count($v->hasSeller()) && isset($v->hasSeller()->v_fname)){
					$vs_fname=$v->hasSeller()->v_fname;
				}

				$vs_lname="";
				if(count($v->hasSeller()) && isset($v->hasSeller()->v_lname)){
					$vs_lname=$v->hasSeller()->v_lname;
				}

				$imgdatas = '';
				if(Storage::disk('s3')->exists($vs_image)){
					$imgdatas = \Storage::cloud()->url($vs_image);
				}else{
					$imgdatas = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
				}
				$seller_comment="";
				if(isset($v->l_answers) && count($v->l_answers)){
					$seller_comment = $v->l_answers['l_comment'];
				}

				$reviewdataList[]=array(
					'v_name'=>$v_fname.' '.$v_lname,
					'v_image'=>$imgdata,
					'totalavg'=>number_format($totalavg,1),
					'l_comment'=>$v->l_comment,
					'v_seller_name'=>$vs_fname.' '.$vs_lname,
					'v_seller_image'=>$imgdatas,
					'v_seller_comment'=>$seller_comment,
				);
			}
		}

		$_data=array(
			'list'=> $reviewdataList,
		);
		
		$responseData=array(
			'code'=>200,
			'message'=>"",
			'data'=>$_data,
		);
		return response()->json($responseData, 200);


	
		
	}

	public function ContactSelller(){

		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;
		
		$rules = [
			'i_job_id' => 'required',
			'v_subject_title' => 'required',
			'l_message'=>'required',
		];
		
		$validator = Validator::make($post_data, $rules);
    	if($validator->fails()) {
				$responseData=array(
						'code'=>400,
						'message'=>$validator->errors()->first(),
						'data'=>array(),
				);
				return response()->json($responseData, 400);
		}
		
		$jobdata = JobsModel::where('_id',$post_data['i_job_id'])->where('e_sponsor',"yes")->where('e_sponsor_status',"start")->first();
		if(count($jobdata)){
			$updatedata['i_contact'] = 1;
			if(isset($jobdata->i_contact)){
				$updatedata['i_contact'] = $jobdata->i_contact+1;
			}
			$a = JobsModel::find($jobdata->id)->update($updatedata);
		}
		$jobdata = JobsModel::find($post_data['i_job_id']);
		
		$data['i_parent_id']=0;
		$data['v_subject_title']=$post_data['v_subject_title'];
		$data['i_to_id'] = $jobdata->i_user_id;
		$data['i_from_id']=$userid;
		$data['l_message']=$post_data['l_message'];
		$data['e_view']="unread";
		$data['d_added']=date("Y-m-d H:i:s");
		$data['d_modified']=date("Y-m-d H:i:s");
		$mid = MessagesModel::create($data)->id;
		self::sendEmailNotificationSellerForMessage($data['i_to_id'],$post_data);	
		
		$sellerdata = UsersModel::find($jobdata->i_user_id);
		if(count($sellerdata)){
			if(isset($sellerdata->l_seller_auto_message) && $sellerdata->l_seller_auto_message!=""){
				$ctime=date("Y-m-d H:i:s");
				$ltime = $data['d_added'];
				$totaldiff =strtotime($ctime)-strtotime($ltime); 
				$totaldiff = $totaldiff;
				$insertreplay=array();
				$insertreplay['i_parent_id']=$mid;
				$insertreplay['v_subject_title']="";
				$insertreplay['i_to_id']=$userid;
				$insertreplay['i_from_id']=$jobdata->i_user_id;
				$insertreplay['l_message']=$sellerdata->l_seller_auto_message;
				$insertreplay['v_replay_time']=$totaldiff;
				$insertreplay['e_view']="unread";
				$insertreplay['d_added']=date("Y-m-d H:i:s");
				$insertreplay['d_modified']=date("Y-m-d H:i:s");
				$mid = MessagesModel::create($insertreplay)->id;
				self::sendEmailNotificationBuyerForMessage($jobdata->i_user_id,$userid,$sellerdata->l_seller_auto_message);	
			}
		}
		
		$finalResponse=array();
		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.messageSent'),
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);

	}

	public function skillShortlist(){


		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;
		
		$rules = [
			'i_skill_id' => 'required',
		];
		
		$validator = Validator::make($post_data, $rules);
    	if($validator->fails()) {
			$responseData=array(
					'code'=>400,
					'message'=>$validator->errors()->first(),
					'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$sData = ShortlistedSkillsModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->delete();
		$is_shortlisted=0;
		$msg="";

		if(count($sData)){
			ShortlistedSkillsModel::where('i_shortlist_id',$post_data['i_skill_id'])->where('i_user_id',$userid)->delete();
			$msg=Lang::get('api.jobs.removeToShortList');
		}else{

			$data['i_user_id']     = $userid;	
			$data['d_added']       = date("Y-m-d H:i:s");	
			$data['i_shortlist_id']= $post_data['i_skill_id'];	
			ShortlistedJobsModel::create($data);
			$is_shortlisted=1;
			$msg=Lang::get('api.jobs.addToShortList');
		}

		$finalResponse=array(
			'is_shortlisted'=>$is_shortlisted,
		);
		$responseData=array(
			'code'    => 200,
			'message' => $msg,
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);


	}

	
}