<?php
namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Users\Order as OrderModal;

class MyOrders extends Controller {

	
	public function index() {

		$postData = Request::all();
		$userData = Request::get('authdata');
		$userid = $userData->_id;

		$dataLimit=20;
		$currentPage=1;

		$rules = [
			'v_status' => 'required',
	    ];
		
		$validator = Validator::make($postData, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$status = $postData['v_status'];
	
		//Buyer Orders

		$query = OrderModal::query();
		$query = $query->where('i_user_id',$userid);
		
		if($status != "cancelled"){
			if($status == "all"){
				$query = $query->where(function($query ) use($userid){
		        $query->where('e_payment_status','success')
		              ->orWhere('e_payment_status','refund');
		    	});	
		   	}else{
				$query = $query->where('e_payment_status','success');	
			}
		}
		$query = $query->where('e_transaction_type','skill');
		if($status == "active"){
			$query = $query->where('v_order_status','active');			
		}
		else if($status == "delivered"){
			$query = $query->where('v_order_status','delivered');	
			$query = $query->where('v_buyer_deliver_status','!=','accept');	
		}
		else if($status == "completed"){
			$query = $query->where(function($query ) use($userid){
		        $query->where('v_order_status','completed')
		              ->orWhere('v_buyer_deliver_status','accept');
		    });	
		}
		else if($status == "cancelled"){
			$query = $query->where('v_order_status','cancelled');	
		}
		else if($status == "awaiting"){
			$query = $query->where('v_order_review','no');
		}

		// $data = $query->orderBy("d_added","DESC")->get(['d_order_start_date','d_date','v_duration_in','v_duration_time','v_order_start','v_order_status','v_amount']);
		$query = $query->orderBy("d_added","DESC");
	
		$data=array();
		if(isset($postData['pagination']) && $postData['pagination']==1){
			$data = $query->paginate($dataLimit);	
		}else{
			$data = $query->get(['d_order_start_date','d_date','v_duration_in','v_duration_time','v_order_start','v_order_status','v_amount']);
		}

		$query = OrderModal::query();
		$query = $query->where('i_user_id',$userid);
		//$query = $query->where('e_payment_status','success');
		$order = $query->where('e_transaction_type','skill')->get();

		$buyer['active']=0;
		$buyer['delivered']=0;
		$buyer['awaiting']=0;
		$buyer['cancelled']=0;
		$buyer['completed']=0;
		$buyer['order']=count($order);
		$buyer['orderamt']=0;

		if(count($order)){
			foreach ($order as $key => $value) {
				
				if($value->v_order_status=="cancelled"){
					$buyer['cancelled'] = $buyer['cancelled']+1;
				}
				
				if($value->e_payment_status=="success"){
					if($value->v_order_status=="active"){
						$buyer['active'] = $buyer['active']+1;
					}
					else if($value->v_order_status=="completed"){
						$buyer['completed'] = $buyer['completed']+1;
					}
					else if($value->v_order_status=="delivered"){
						if(isset($value->v_buyer_deliver_status) && $value->v_buyer_deliver_status=="accept"){
							$buyer['completed'] = $buyer['completed']+1;
						}else{
							$buyer['delivered'] = $buyer['delivered']+1;	
						}
					}
					if(isset($value->v_order_review) && $value->v_order_review=="no"){
						$buyer['awaiting'] = $buyer['awaiting']+1;
					}
					$buyer['orderamt']  =$buyer['orderamt']+$value->v_amount;
				}
			}	
		}
		
		$orderList=array();		
		if(count($data)){
			foreach($data as $k=>$v){

				$startdate = $v->d_date;   
				$duedate="";
				if(isset($v->v_order_start) && isset($v->d_order_start_date) && $v->v_order_start=="yes"){
					$startdate = $v->d_order_start_date;
				}
				if(isset($v->v_duration_in) && $v->v_duration_in=="hours"){
					$duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' hours',strtotime($startdate)));
				}else if(isset($v->v_duration_in) && $v->v_duration_in=="day"){
					$duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' days',strtotime($startdate)));
				}else if(isset($v->v_duration_in) && $v->v_duration_in=="month"){
					$duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' months',strtotime($startdate)));
				}else if(isset($v->v_duration_in) && $v->v_duration_in=="year"){
					$duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' years',strtotime($startdate)));
				}
												
				$orderList[]=array(
					'd_added'=>$startdate,
					'd_order_date'=>$startdate,
					'd_due_date'=>date("Y-m-d",strtotime($duedate)),
					'v_amount'=>$v->v_amount,
					'v_total'=>$v->v_amount,
					'v_order_status'=>$v->v_order_status,
					'v_profile_title'=>$v->v_profile_title
				);				
			}
		}
		$resData = GeneralHelper::apiPagination($data,$postData);

		$resData['current_page'] = $resData['current_page'] == 0 ? 1 : $resData['current_page'];
		$resData['total_page'] = $resData['total_page'] == 0 ? 1 : $resData['total_page'];


		$resData['buyer']=$buyer;
		$resData['buyer']['list']=$orderList;

		$resData['seller'] = array();
		$resData['seller']['list'] = array();
		
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.myMoney'),
			'data'=>$resData,
		);
		return response()->json($responseData, 200);
		
	}

	public function sellerOrder() {

		$postData = Request::all();
		$userData = Request::get('authdata');
		$userid = $userData->_id;

		$dataLimit=20;
		$currentPage=1;

		$rules = [
			'v_status' => 'required',
	    ];
		
		$validator = Validator::make($postData, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$status = $postData['v_status'];
	
		//Seller Order Data
		
		$query = OrderModal::query();
		$query = $query->where('i_seller_id',$userid);
		$query = $query->where('e_transaction_type','skill');
		
		if($status != "cancelled"){
			if($status == "all"){
				$query = $query->where(function($query ) use($userid){
		        $query->where('e_payment_status','success')
		              ->orWhere('e_payment_status','refund');
		    	});	
		   	}else{
				$query = $query->where('e_payment_status','success');	
			}
		}

		if($status == "active"){
			$query = $query->where('v_order_status','active');			
		}
		else if($status == "delivered"){
			$query = $query->where('v_order_status','delivered');	
			$query = $query->where('v_buyer_deliver_status','!=','accept');	
		}
		else if($status == "completed"){

			$query = $query->where(function($query ) use($userid){
		        $query->where('v_order_status','completed')
		              ->orWhere('v_buyer_deliver_status','accept');
		    });
		}
		else if($status == "cancelled"){
			$query = $query->where('v_order_status','cancelled');	
		}
		//$data = $query->orderBy("d_added","DESC")->get();

		// $query = $query->orderBy("d_added","DESC")->get(['d_order_start_date','d_date','v_duration_in','v_duration_time','v_order_start','v_order_status','v_amount']);
		
		$query = $query->orderBy("d_added","DESC");
		if(isset($postData['pagination']) && $postData['pagination']==1){
			$data = $query->paginate($dataLimit);	
		}else{
			$data = $query->get(['d_order_start_date','d_date','v_duration_in','v_duration_time','v_order_start','v_order_status','v_amount']);
		}
	
		$query = OrderModal::query();
		$query = $query->where('i_seller_id',$userid);
		//$query = $query->where('e_payment_status','success');
		$order = $query->where('e_transaction_type','skill')->get();

		$seller['active']=0;
		$seller['cancelled']=0;
		$seller['completed']=0;
		$seller['delivered']=0;
		$seller['order']=count($data);
		$seller['orderamt']=0;

		if(count($order)){
			foreach ($order as $key => $value) {
				
				if($value->v_order_status=="cancelled"){
						$seller['cancelled'] = $seller['cancelled']+1;
				}
				if($value->e_payment_status=="success"){
					if($value->v_order_status=="active"){
						$seller['active'] = $seller['active']+1;
					}
					else if($value->v_order_status=="completed"){
						$seller['completed'] = $seller['completed']+1;
					}
					else if($value->v_order_status=="delivered"){
						if(isset($value->v_buyer_deliver_status) && $value->v_buyer_deliver_status=="accept"){
							$seller['completed'] = $seller['completed']+1;
						}else{
							$seller['delivered'] = $seller['delivered']+1;	
						}
					}
					$seller['orderamt']  =$seller['orderamt']+$value->v_amount;
				}	
			}	
		}

		$orderList=array();		
		if(count($data)){
			foreach($data as $k=>$v){

				$startdate = $v->d_date;   
				$duedate="";
				if(isset($v->v_order_start) && isset($v->d_order_start_date) && $v->v_order_start=="yes"){
					$startdate = $v->d_order_start_date;
				}
				if(isset($v->v_duration_in) && $v->v_duration_in=="hours"){
					$duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' hours',strtotime($startdate)));
				}else if(isset($v->v_duration_in) && $v->v_duration_in=="day"){
					$duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' days',strtotime($startdate)));
				}else if(isset($v->v_duration_in) && $v->v_duration_in=="month"){
					$duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' months',strtotime($startdate)));
				}else if(isset($v->v_duration_in) && $v->v_duration_in=="year"){
					$duedate = date('Y-m-d H:i:s',strtotime('+'.$v->v_duration_time.' years',strtotime($startdate)));
				}
												
				$orderList[]=array(
					'd_added'=>$startdate,
					'd_order_date'=>$startdate,
					'd_due_date'=>date("Y-m-d",strtotime($duedate)),
					'v_amount'=>$v->v_amount,
					'v_total'=>$v->v_amount,
					'v_order_status'=>$v->v_order_status,
					'v_profile_title'=>$v->v_profile_title
				);				
			}
		}
		$resData = GeneralHelper::apiPagination($data,$postData);

		$resData['current_page'] = $resData['current_page'] == 0 ? 1 : $resData['current_page'];
		$resData['total_page'] = $resData['total_page'] == 0 ? 1 : $resData['total_page'];

		$resData['seller']=$seller;
		$resData['seller']['list']=$orderList;
		
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.myMoney'),
			'data'=>$resData,
		);
		return response()->json($responseData, 200);
		
	}
}