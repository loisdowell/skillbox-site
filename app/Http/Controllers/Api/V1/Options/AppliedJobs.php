<?php
namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Buyerreview as BuyerreviewModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Users\Jobs as JobsModel;

class AppliedJobs extends Controller {

	public function index() {

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');

		$postData = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$dataLimit=20;
		$currentPage=1;

		$appliedJobs=array();

		$query = AppliedJobModel::query();
		$query = $query->where('i_buyer_id',$userid);
		if(isset($postData['pagination']) && $postData['pagination']==1){
			$appliedJobs = $query->paginate($dataLimit);	
		}else{
			$appliedJobs = $query->get();
		}
		//$appliedJobs = AppliedJobModel::where('i_buyer_id',$userid)->get();

		$finalData=array();

		if(isset($appliedJobs) && count($appliedJobs)){
			foreach($appliedJobs as $k=>$v){
				
				$v_image="";
				if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
					$v_image=$v->hasUser()->v_image;
				}
				$imgdata="";
				if(Storage::disk('s3')->exists($v_image)){
					$imgdata = \Storage::cloud()->url($v_image);
				}else{
					$imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
				}

				$sellerdata=array();
				if(count($v->hasSellerProfile())){
					$sellerdata = $v->hasSellerProfile();
				}

				$jobdata=array();
				if(count($v->hasJob())){
					$jobdata = $v->hasJob();
				}

				$e_sponsor_status="";
				if(isset($sellerdata->e_sponsor_status) && $sellerdata->e_sponsor_status == 'yes'){
					$e_sponsor_status="Sponsored Post";
				}
				$finalData[]=array(
					'id'=>$v->id,
					'v_image'=>$imgdata,
					'v_seller_id'=>$sellerdata->id,
					'v_profile_title'=>$sellerdata->v_profile_title,
					'v_hourly_rate'=>$sellerdata->v_hourly_rate,
					'v_experience_level'=>$sellerdata->v_experience_level,
					'l_short_description'=>$sellerdata->l_short_description,
					'v_job_title'=>$jobdata->v_job_title,
					'v_job_id'=>$jobdata->id,
					'e_sponsor_status'=>$e_sponsor_status,
				);
			}
		}

		$resdata = GeneralHelper::apiPagination($appliedJobs,$postData);
		$resdata['jobs_data'] = $finalData;
			
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.appliedJob'),
			'data'=>$resdata,
		);
		return response()->json($responseData, 200);
		
	}


	public function deleteAppliedJobs(){
		
	
		$data = Request::all(); 
		$authdata = Request::get('authdata');
		$rules = [
			'Ids' => 'required',
	    ];
	
		$validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$Ids = explode(',', $data['Ids']);

		$userid = $authdata->id;
		AppliedJobModel::whereIn('_id',$Ids)->delete();
	
		$responseData=array(
			'code'=>200,
			'message'=>"",
			'data'=>array(),
		);
		return response()->json($responseData, 200);

	}




}