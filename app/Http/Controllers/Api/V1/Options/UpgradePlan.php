<?php
namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Users\Order as OrderModal;

class UpgradePlan extends Controller {

	
	public function index() {

		$postData = Request::all();
		$userData = Request::get('authdata');
		$userid = $userData->_id;

		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");
		$userplandata = GeneralHelper::userPlanDetail($userData);
		$finalData=array();
		$pcnt=0;
		if(count($plandata)){
			foreach($plandata as $key=>$val){
				
				if($userplandata['v_plan_id']=="5a65b757d3e8125e323c986a"){
					$pcnt=1;
				}
				if($userplandata['v_plan_id']=="5a65b9f4d3e8124f123c986c"){
					$pcnt=2;
				}
				if($key>=$pcnt){
					$finalData[]=$val;
				}
			}
		}
		$resData['v_user_plan_id'] = $userplandata['v_plan_id'];
		$resData['v_plan_duration'] = $userplandata['v_plan_duration'];
		$resData['v_plan_name'] = $userplandata['v_plan_name'];
		$resData['v_plan_list'] = $finalData;
		
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.myMoney'),
			'data'=>$resData,
		);
		return response()->json($responseData, 200);
		
	}
}