<?php
namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Support\SupportQuery as SupportQueryModel;

class Support extends Controller {

	
	public function index() {

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');

        $postData = Request::all();
		$authdata = Request::get('authdata');

		$dataLimit=20;
		$currentPage=1;

		$data=array();

		$query = SupportModel::query();
		$query = $query->where('i_user_id',$authdata->id)->where('i_parent_id',0)->orderBy('d_added',"DESC");
		
		if(isset($postData['pagination']) && $postData['pagination']==1){
			$data = $query->paginate($dataLimit);	
		}else{
			$data = $query->get();
		}
	
		$resdata = GeneralHelper::apiPagination($data,$postData);
		$finalList=array();
		if(count($data)){
			foreach($data as $k=>$v){
				$finalList[]=$v;
			}
		}
		$resdata['supportList'] = $finalList; 
		
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.supportList'),
			'data'=>$resdata,
		);
		return response()->json($responseData, 200);
		
		
	}

	public function categoryList() {

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		
		$postData = Request::all();
		$query = SupportQueryModel::query();
		$supportquery = $query->where('e_status','active')->orderBy('i_order')->get();
		
		$data=array(
			'supportFiledList'=>$supportquery,
		);	

		if($data){
			$responseData=array(
                'code'=>200,
                'message'=>Lang::get('api.moreOption.supportList'),
                'data'=>$data,
            );
            return response()->json($responseData, 200);
		}
	}

	public function addSupport() {

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		
   		$authdata = Request::get('authdata');
		
		$data = Request::all(); 
		$rules = [
			'e_query' => 'required',
			'e_type' => 'required',
			'l_question' => 'required',
        ];
		
		$validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$data['i_parent_id']=0;
		$data['e_user_type']="user";
		$data['i_user_id']=$authdata->id;
		$data['e_status']="open";
		$data['e_view_status']="unread";
		$data['i_ticket_num']=GeneralHelper::supportNumber();
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		$data['v_document']=array();

		if($data['file']){
			$imgname = self::uploadFileBase64($data['file'],'support');
			$data['v_document'] = $imgname;
		}
		
		// if(Request::hasFile('v_document')) {
		// 	$image = Request::file('v_document');
		// 	foreach($image as $file){
	 //        	$fileName = 'doc-'.time().'.'.$file->getClientOriginalExtension();
		// 		$fileName = str_replace(' ', '_', $fileName);
		// 		$data['v_document'][] = 'support/' . $fileName;
		// 		$uploadS3 = Storage::disk('s3')->put('/support/'.$fileName, File::get((string)$file), 'public');
		//     }
		// }
		SupportModel::create($data);	
		$supportdata = SupportModel::where('i_user_id',$authdata->id)->where('i_parent_id',0)->orderBy('d_added',"DESC")->get();
		
		$data=array(
			'supportList'=>$supportdata,
		);	
		
		if($data){
			$responseData=array(
                'code'=>200,
                'message'=>Lang::get('api.moreOption.supportList'),
                'data'=>$data,
            );
            return response()->json($responseData, 200);
		}
		
	}

	public function uploadFileBase64($file, string $remotePath){

        $data=$file;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);

        $diskName = env('S3_DISK') ? env('S3_DISK'):'public';
        $extension = "png";
        $fileName = str_replace('.', '', uniqid('', true)) . '.' . $extension;
        $fileName = $remotePath.'/'.$fileName;
        $uploadS3 = Storage::disk('s3')->put($fileName, $data, 'public');     
        // if ($uploadS3 === false) {
        //     throw new \Exception('File was not uploaded.');
        // }
        return $fileName;
    }
}