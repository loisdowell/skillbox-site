<?php

namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang, Crypt, Session, Validator, Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;

class ShortlistJobDelete extends Controller
{
  // public function index(){
    
  // }
  public function deleteShortlistedJob()
  {
    $data = Request::all();
    $authdata = Request::get('authdata');
    $rules = [
      'Ids' => 'required',
    ];

    $validator = Validator::make($data, $rules);
    if ($validator->fails()) {
      $responseData = array(
        'code' => 400,
        'message' => $validator->errors()->first(),
        'data' => array(),
      );
      return response()->json($responseData, 400);
    }

    $shortlistIds   = explode(',', $data['Ids']);
    $userId = $authdata->id;
    dd($userId);
    ShortlistedJobsModel::whereIn('i_shortlist_id', $shortlistIds)->where('i_user_id', $userId)->delete();

    $responseData = array(
      'code' => 200,
      'message' => "",
      'data' => array(),
    );
    return response()->json($responseData, 200);
  }
}
