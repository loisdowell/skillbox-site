<?php
namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Support\Support as SupportModel;

class MyMoney extends Controller {

	protected $section;
	private $mangopay;
	
	public function __construct(\MangoPay\MangoPayApi $mangopay){
		$this->section = Lang::get('section.Review center');
		$this->mangopay = $mangopay;
	}
	
	public function index() {

		$postData = Request::all();
		$userData = Request::get('authdata');
        
		$i_wallet_id = "";
		if(isset($userData->i_wallet_id)){
			$i_wallet_id = $userData->i_wallet_id;
		}	
		$result = $this->mangopay->Wallets->Get($i_wallet_id);	
		$kycdoc = $this->mangopay->Users->get($userData->i_mangopay_id);	
		
		$amt = 0;
		if(count($result)){
			$amt = $result->Balance->Amount;
			$amt = $amt/100;
		}
		$data=array(
			'wallet'=>number_format($amt,2),
			'kyc_status'=> ($kycdoc) ? $kycdoc->KYCLevel : '',
		);
		
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.myMoney'),
			'data'=>$data,
		);
		return response()->json($responseData, 200);
		
	}
}