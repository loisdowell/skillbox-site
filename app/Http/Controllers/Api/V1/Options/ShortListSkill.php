<?php
namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;

class ShortListSkill extends Controller {

	public function index() {

        $postData = Request::all();
        $authdata = Request::get('authdata');
		$userid = $authdata->id;
		
		$dataLimit=20;
		$currentPage=1;

		$s_skill = array();
		$shortlistedSkill=array();

		$query = ShortlistedSkillsModel::query();
		$query = $query->where('i_user_id',$userid);
		if(isset($postData['pagination']) && $postData['pagination']==1){
			$shortlistedSkill = $query->paginate($dataLimit);	
		}else{
			$shortlistedSkill = $query->get();
		}

		foreach ($shortlistedSkill as $key => $value) {
			$s_skill[] = $value->i_shortlist_id;
		}
		
		$skills_data=array();
		if(count($s_skill)){
			$skills_data = SellerprofileModel::whereIn('_id', $s_skill)->get();	
		}

		$_S3_PATH = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/";

		
		$finalData=array();
		if(isset($skills_data) && count($skills_data)){
			foreach($skills_data as $k=>$v){
				
				$v_image="";
				if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
					$v_image=$v->hasUser()->v_image;
				}
				$imgdata =  $_S3_PATH.$v_image;

				if($v_image!=""){
					$imgdata =  $_S3_PATH.$v_image;
				}else{
					$imgdata = $_S3_PATH.'jobpost/photos/No_Image_Available.jpg';          
				}

				$e_sponsor=0;
				if(isset($v->e_sponsor) && $v->e_sponsor == 'yes'){
					$e_sponsor="Sponsored Seller";
				}
				
				$finalData[]=array(
					'id'=>$v->id,
					'v_image'=>$imgdata,
					'v_profile_title'=>$v->v_profile_title,
					'v_hourly_rate'=>$v->v_hourly_rate,
					'v_experience_level'=>$v->v_experience_level,
					'e_sponsor'=> $e_sponsor,
					'l_short_description'=>$v->l_short_description,

				);
			}
		}

		$resdata = GeneralHelper::apiPagination($shortlistedSkill,$postData);
		$resdata['skills_data'] = $finalData;
	
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.shortListSkill'),
			'data'=>$resdata,
		);
		return response()->json($responseData, 200);
		
		

	}

	public function deleteShortlistedSkill(){
		
		$data = Request::all(); 
		$authdata = Request::get('authdata');
		$rules = [
			'Ids' => 'required',
	    ];
	
		$validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$shortlistIds   = explode(',', $data['Ids']);
		$userid = $authdata->id;
		ShortlistedSkillsModel::whereIn('i_shortlist_id',$shortlistIds)->where('i_user_id',$userid)->delete();

		$responseData=array(
			'code'=>200,
			'message'=>"",
			'data'=>array(),
		);
		return response()->json($responseData, 200);

	}
}