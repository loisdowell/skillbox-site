<?php
namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage,File;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Support\SupportQuery as SupportQueryModel;
use App\Models\Country as CountryModel;


class Profile extends Controller {

	
	public function index() {

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');

        $postData = Request::all();
		$authdata = Request::get('authdata');

		//$countrylist = CountryModel::where('e_status','active')->get();
	
		$data['v_fname']=$authdata->v_fname;
		$data['v_lname']=$authdata->v_lname;
		$data['v_email']=$authdata->v_email;
		$data['v_image']=\Storage::cloud()->url($authdata->v_image);
		$data['v_contact']=$authdata->v_contact;

		$data['i_country_id']=$authdata->i_country_id;
		$data['i_country_name']="";
		if(count($authdata->hasCountry()) && isset($authdata->hasCountry()->v_title)){
			$data['i_country_name'] = $authdata->hasCountry()->v_title;	
		}
		$data['v_city']=$authdata->v_city;
		$data['l_address']=$authdata->l_address;
		$data['v_postcode']=$authdata->v_postcode;
		$data['d_birthday']=$authdata->d_birthday;
		$data['v_gender']=$authdata->v_gender;

		$data['v_company_type'] = "";
		if(isset($authdata->v_company_type)){
			$data['v_company_type'] = $authdata->v_company_type;
		}
		$data['v_company_name'] = "";
		if(isset($authdata->v_company_name)){
			$data['v_company_name'] = $authdata->v_company_name;
		}
		$data['v_company_regnumber'] = "";
		if(isset($authdata->v_company_regnumber)){
			$data['v_company_regnumber'] = $authdata->v_company_regnumber;
		}
		$data['v_vat_number'] = "";
		if(isset($authdata->v_vat_number)){
			$data['v_vat_number'] = $authdata->v_vat_number;
		}
		$data['v_member_since']=date("d F Y", strtotime($authdata->d_added));
		$data['seller_status'] = $authdata['seller']['e_status'];
		$data['buyer_status'] = $authdata['buyer']['e_status'];
		$countrylist = CountryModel::where('e_status','active')->orderBy('v_title')->get();
	
		$_data=array(
			'user_data'=>$data,
			'countrylist'=>$countrylist,
		);	
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.supportList'),
			'data'=>$_data,
		);
		return response()->json($responseData, 200);
		
	}

	public function updateProfileImage() {

		$data = Request::all();
		$authdata = Request::get('authdata');

		$rules = [
			'v_image' => 'required',
		];
		
		$validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		$userid = $authdata->id;
		$imgname = self::uploadFileBase64($data['v_image'],'users');
		$update['v_image'] = $imgname;
		$a = UsersModel::find($userid)->update($update);



		$data=array(
			'v_image'=>\Storage::cloud()->url($imgname),
		);
		
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.updateProfile'),
			'data'=>$data,
		);
		return response()->json($responseData, 200);



		// $authdata = Request::get('authdata');
		// $rules = [
		// 	'v_image' => 'required',
		// ];
		// $validator = Validator::make($data, $rules);
  //       if($validator->fails()) {
  //           $responseData=array(
  //               'code'=>400,
  //               'message'=>$validator->errors()->first(),
  //               'data'=>array(),
		// 	);
		// 	return response()->json($responseData, 400);
		// }
		
		// $userid = $authdata->id;
		
		// $update=array();
		// if(Request::hasFile('v_image')) {
		
		// 	$image = Request::file('v_image');
		// 	$fileName = 'profile-'.time().'.'.$image->getClientOriginalExtension();
		// 	$fileName = str_replace(' ', '_', $fileName);
		// 	$update['v_image'] = 'users/' . $fileName;
		// 	$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$image), 'public');
		// 	$a = UsersModel::find($userid)->update($update);
		// }

		// $data=array(
		// 	'v_image'=>\Storage::cloud()->url($update['v_image']),
		// );
		
		// $responseData=array(
		// 	'code'=>200,
		// 	'message'=>Lang::get('api.moreOption.updateProfile'),
		// 	'data'=>$data,
		// );
		// return response()->json($responseData, 200);
		
	}

	public function uploadFileBase64($file, string $remotePath){

        $data=$file;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);

        $diskName = env('S3_DISK') ? env('S3_DISK'):'public';
        $extension = "png";
        $fileName = str_replace('.', '', uniqid('', true)) . '.' . $extension;
        $fileName = $remotePath.'/'.$fileName;
        $uploadS3 = Storage::disk('s3')->put($fileName, $data, 'public');     
        if ($uploadS3 === false) {
            throw new \Exception('File was not uploaded.');
        }
        return $fileName;
    }
	
	
	public function updateDetail() {

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		
   		$authdata = Request::get('authdata');
		
		$data = Request::all(); 
		$rules = [
			'v_fname' => 'required',
			'v_lname' => 'required',
			'v_email' => 'required|email',
			'v_contact' => 'required',
			'i_country_id' => 'required',
			'l_address' => 'required',
			'v_postcode' => 'required',
			'd_birthday' => 'required',
			'v_gender' => 'required',
			'v_city' => 'required',
		];
		
		$validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$userid = $authdata->id;
		$existemail = UsersModel::where("v_email",$data['v_email'])->where('_id','!=',$userid)->get();
		
		if(count($existemail)){

			$responseData=array(
                'code'=>400,
                'message'=>Lang::get('api.moreOption.emailExist'),
                'data'=>$data,
            );
			return response()->json($responseData, 400);
		}
		
		UsersModel::find($userid)->update($data);
		
		$_data=array(
			'data'=>$data,
		);	
		
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.updateProfile'),
			'data'=>$data,
		);
		return response()->json($responseData, 200);
	
		
	}
}