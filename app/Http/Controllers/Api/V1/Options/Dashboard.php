<?php
namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Users\Messages as MessagesModel;

class Dashboard extends Controller {

	
	public function index() {

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		
		$authdata = Request::get('authdata');
		
		$_data['summaryData'] = GeneralHelper::AccountSummary($authdata->id);
	
		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.moreOption.summary'),
			'data'    => $_data,
		);
		return response()->json($responseData, 200);
	}


	public function accountCancel() {

		$authdata = Request::get('authdata');

		$id = $authdata->id;
		self::deleteaccountnotification($authdata);
		$userdata = $authdata;
	
	    if(count($userdata)){
			$updateuser['e_login']="no";
        	$updateuser['i_delete']="1";
			$updateuser['v_email']="";
			$updateuser['v_auth_token']="";
			UsersModel::find($id)->update($updateuser);
			
			$update['i_delete']="1";
        	SellerprofileModel::where('i_user_id',$id)->update($update);
        	JobsModel::where('i_user_id',$id)->update($update);
        	CoursesModel::where('i_user_id',$id)->update($update);	
		}
		
		$_data=array();
		$responseData=array(
			'code'    => 200,
			'message' => "SKILLBOX Account Cancelled Successfully",
			'data'    => $_data,
		);
		return response()->json($responseData, 200);

	}

	public function deleteaccountnotification($authdata=array()){
		
		$v_email = $authdata->v_email;
        $name="";
        if(isset($authdata->v_fname) && $authdata->v_fname!=""){
            $name = $authdata->v_fname;
        }
        if(isset($authdata->v_lname) && $authdata->v_lname!=""){
            $name .= ' '.$authdata->v_lname;
		}
		
        $data=array(
            'USER_FULL_NAME'=>$name,
        );
		$mailcontent = EmailtemplateHelper::AccountDeleteNotification($data);
        $subject = "SKILLBOX Account Cancelled Successfully";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bb6f9f676fbae1787269862");
        $mailids=array(
            $name=>$v_email,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

}