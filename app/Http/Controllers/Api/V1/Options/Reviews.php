<?php
namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Users\Order as OrderModal;
use App\Models\Users\Buyerreview as BuyerreviewModel;
use App\Models\Users\Sellerreview as SellerreviewModel;



class Reviews extends Controller {

	
	public function index() {

		$postData = Request::all();
		$userData = Request::get('authdata');
		$userid = $userData->_id;

		// $buyerreview = BuyerreviewModel::where('i_buyer_id',$userid)->orderBy("d_added","DESC")->get();

		$dataLimit=20;
		$currentPage=1;

		$buyerreview=array();
		$query = BuyerreviewModel::query();
        $query = $query->where('i_buyer_id',$userid)->orderBy("d_added","DESC");
		if(isset($postData['pagination']) && $postData['pagination']==1){
			$buyerreview = $query->paginate($dataLimit);	
		}else{
			$buyerreview = $query->get();
		}

		$finalReview=array();
		if(isset($buyerreview) && count($buyerreview)){
        	foreach($buyerreview as $k=>$v){
           
                $cnt = $k++;
                $v_image="";
                $v_fromimage="";

                if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
                    $v_image=$v->hasUser()->v_image;
				    $v_fromimage = \Storage::cloud()->url($v_image);
                }
                $v_fname="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_fname)){
                    $v_fname=$v->hasUser()->v_fname;
                }

                $v_lname="";
                if(count($v->hasUser()) && isset($v->hasUser()->v_lname)){
                    $v_lname=$v->hasUser()->v_lname;
                }
			    $totalavg=0;
				$totalavg = ($v['i_work_star']+$v['i_support_star']+$v['i_opportunities_star']+$v['i_work_again_star'])/4;

				$finalReview[]=array(
					'v_name'=>$v_fname.' '.$v_lname,
					'v_image'=>$v_fromimage,
					'v_review'=>number_format($totalavg,1),
					'l_comment'=>$v->l_comment,
				);

			}
		}
		$resData = GeneralHelper::apiPagination($buyerreview,$postData);

		$resData['current_page'] = $resData['current_page'] == 0 ? 1 : $resData['current_page'];
		$resData['total_page'] = $resData['total_page'] == 0 ? 1 : $resData['total_page'];

		$resData['job'] = $finalReview;
		$resData['buyer'] = array();
		
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.myMoney'),
			'data'=>$resData,
		);
		return response()->json($responseData, 200);
		
	}

	public function buyerReview() {

		$postData = Request::all();
		$userData = Request::get('authdata');
		$userid = $userData->_id;

		$dataLimit=20;
		$currentPage=1;
		
		$sellerreview=array();
		
		$query = SellerreviewModel::query();
		$query = $query->where('i_seller_id',$userid)->orderBy("d_added","DESC");
		if(isset($postData['pagination']) && $postData['pagination']==1){
			$sellerreview = $query->paginate($dataLimit);	
		}else{
			$sellerreview = $query->get();
		}
		$finalReview=array();

		if(isset($sellerreview) && count($sellerreview)){
				foreach($sellerreview as $k=>$v){
					
					$cnt = $k++;
					$v_image="";
					$v_fromimage="";
					if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
						$v_image=$v->hasUser()->v_image;
						$v_fromimage = \Storage::cloud()->url($v_image);
					}
	
					$v_fname="";
					if(count($v->hasUser()) && isset($v->hasUser()->v_fname)){
						$v_fname=$v->hasUser()->v_fname;
					}
	
					$v_lname="";
					if(count($v->hasUser()) && isset($v->hasUser()->v_lname)){
						$v_lname=$v->hasUser()->v_lname;
					}
	
					$totalavg=0;
					$totalavg = ($v['i_communication_star']+$v['i_described_star']+$v['i_recommend_star'])/3;

					$finalReview[]=array(
						'v_name'=>$v_fname.' '.$v_lname,
						'v_image'=>$v_fromimage,
						'v_review'=>number_format($totalavg,1),
						'l_comment'=>$v->l_comment,
					);
				}
		}

		$resData = GeneralHelper::apiPagination($sellerreview,$postData);
		$resData['current_page'] = $resData['current_page'] == 0 ? 1 : $resData['current_page'];
		$resData['total_page'] = $resData['total_page'] == 0 ? 1 : $resData['total_page'];

		$resData['buyer'] = $finalReview;
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.moreOption.myMoney'),
			'data'=>$resData,
		);
		return response()->json($responseData, 200);
		
	}
}