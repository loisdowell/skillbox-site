<?php
namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Courses\BuyerCourse as BuyerCourseModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;

class MyCourses extends Controller {

	public function index() {

        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        
        $postData = Request::all();

        $authdata = Request::get('authdata');
        $userid = $authdata->id;

        $dataLimit=20;
        $currentPage=1;
        
        $cdata=array();

        $query = BuyerCourseModel::query();
        if(isset($data['e_status']) && $data['e_status']!=""){
			$query = $query->where('e_status',$data['e_status']);
		}
        $query = $query->where('i_user_id',$userid)->where('e_payment_status','success');

        if(isset($postData['pagination']) && $postData['pagination']==1){
			$cdata = $query->paginate($dataLimit);	
		}else{
			$cdata = $query->get();
        }
        
           
        $arrList = [];
        if(count($cdata)){
            foreach( $cdata as $val){
                $percent = 0;
                if(count($val->hasCourse()) && isset($val->hasCourse()->v_course_thumbnail) && $val->hasCourse()->v_course_thumbnail!=""){
                    $imgins = $val->hasCourse()->v_course_thumbnail;
                }else{
                    $imgins = 'jobpost/photos/No_Image_Available.jpg';
                }
                $imgdata = \Storage::cloud()->url($imgins);                
                if(count($val->hasCourse()) && isset($val->hasCourse()->v_sub_title)){
                    if(strlen($val->hasCourse()->v_sub_title)>75){
                        $v_sub_title = substr($val->hasCourse()->v_sub_title,0,75).'...';
                    } else{
                        $v_sub_title = $val->hasCourse()->v_sub_title;
                    }            
                }
     	        if( $val->v_complete_lectures ) $percent = ceil( $val->v_complete_lectures * 100  / $val->v_total_lectures );
                $arrList []=[
                    'i_course_id'  => $val->hasCourse()->id,
                    'imgdata'      => $imgdata,
                    'courseTitle'  => count( $val->hasCourse() ) ? $val->hasCourse()->v_title : '',
                    'v_sub_title'  => $v_sub_title,
                    'avg_review'   => count( $val->hasCourse() ) ? number_format($val->hasCourse()->i_total_avg_review,1) : '0',
                    'total_review' => count( $val->hasCourse() ) ? $val->hasCourse()->i_total_review : '',
                    'percent'      => $percent
                ];
            }
        }

        $resdata = GeneralHelper::apiPagination($cdata,$postData);
		$resdata['course_data'] = $arrList;
        // $data=array(
		// 	'course_data'=>$arrList,		
        // );

        $responseData=array(
            'code'    => 200,
            'message' => Lang::get('api.moreOption.myCourses'),
            'data'    => $resdata,
        );
        return response()->json($responseData, 200);
		
    }
    
    public function detail() {

        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');

        $data = Request::all(); 
		$rules = [
			'i_course_id' => 'required',
	    ];
        
        $validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
        }
        $authdata = Request::get('authdata');
        $userid = $authdata->id;
        $cData = CoursesModel::find($data['i_course_id']);
       
        $reviews = CoursesReviews::where("i_courses_id",$cData->id)->get();
		$ratedata=0;

		if(count($reviews)){
			foreach($reviews as $key => $value) {
				$ratedata = $ratedata+($value->f_rate_instruction+$value->f_rate_navigate+$value->f_rate_reccommend)/3; 				
			}
		}
		$total['reviews'] = count($reviews);
        $total['ratedata'] = $ratedata;

        $section=array();
        if(isset($cData->section)){
            $section = $cData->section;
        }

        $findalData['baseUrl']='https://skillboxs3bucket.s3.us-east-2.amazonaws.com/';
        $findalData['v_title']=$cData->v_title;
        $findalData['v_sub_title']=$cData->v_sub_title;
        $findalData['l_description']=$cData->l_description;
        $findalData['reviews']=count($reviews);
        $findalData['ratedata']=number_format($ratedata,1);
        $findalData['section']=$section;

        $data=array(
			'course_data'=>$findalData,		
        );
        
        $responseData=array(
            'code'    => 200,
            'message' => Lang::get('api.moreOption.myCourses'),
            'data'    => $data,
        );
        return response()->json($responseData, 200);
		
       
	}
}