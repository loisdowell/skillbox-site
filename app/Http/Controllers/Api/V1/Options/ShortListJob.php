<?php

namespace App\Http\Controllers\Api\V1\Options;

use Request, Hash, Lang, Crypt, Session, Validator, Auth, Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Users\ShortlistedJobs as shortlistedJobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Users\Jobs as JobsModel;

class ShortListJob extends Controller
{

	public function index()
	{
		$postData = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$dataLimit = 20;
		$currentPage = 1;

		$s_jobs = array();
		$a_jobs = array();
		$shortlistedJob = array();

		$query = shortlistedJobsModel::query();
		$query = $query->where('i_user_id', $userid);
		if (isset($postData['pagination']) && $postData['pagination'] == 1) {
			$shortlistedJob = $query->paginate($dataLimit);
		} else {
			$shortlistedJob = $query->get();
		}
		
		foreach ($shortlistedJob as $key => $value) {
			$s_jobs[] = $value->i_shortlist_id;
		}

		$appliedJob = AppliedJobModel::where('i_user_id',$userid)->get();

		foreach ($appliedJob as $key => $value) {
			$a_jobs[] = $value->i_applied_id;
		}

		$jobs_data = array();
		if (count($s_jobs)) {
			$jobs_data = JobsModel::whereIn('_id', $s_jobs)->get();
		}
		
		$_S3_PATH = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/";


		$finalData = array();
		if (isset($jobs_data) && count($jobs_data)) {
			foreach ($jobs_data as $k => $v) {

				$v_image = "";
				if(isset($v->v_photos[0])){
					$v_image = $v->v_photos[0];
				}

				$imgdata =  $_S3_PATH . $v_image;

				if ($v_image != "") {
					$imgdata =  $_S3_PATH . $v_image;
				} else {
					$imgdata = $_S3_PATH . 'jobpost/photos/No_Image_Available.jpg';
				}

				$e_sponsor = 0;
				if (isset($v->e_sponsor) && $v->e_sponsor == 'yes') {
					$e_sponsor = "Sponsored Seller";
				}
				
				$appied = 0;
				if(count($a_jobs) && in_array($v->id, $a_jobs)){
					$appied = 1;
				}

				$finalData[] = array(
					'id' => $v->_id,
					'v_image' => $imgdata,
					'v_profile_title' => $v->v_job_title,
					'v_hourly_rate' => $v->v_budget_amt,
					'v_experience_level' => $v->v_skill_level_looking,
					'e_sponsor' => $v->e_sponsor,
					'l_short_description' => $v->l_job_description,
					'applied' => $appied
				);
			}
		}

		$resdata = GeneralHelper::apiPagination($shortlistedJob, $postData);
		$resdata['jobs_data'] = $finalData;

		$responseData = array(
			'code' => 200,
			'message' => Lang::get('api.moreOption.shortListJob'),
			'data' => $resdata,
		);
		return response()->json($responseData, 200);
	}

	public function deleteShortlistedJob()
	{
		$data = Request::all();
		$authdata = Request::get('authdata');
		$rules = [
			'Ids' => 'required',
		];

		$validator = Validator::make($data, $rules);
		if ($validator->fails()) {
			$responseData = array(
				'code' => 400,
				'message' => $validator->errors()->first(),
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		$shortlistIds   = explode(',', $data['Ids']);
		$userId = $authdata->id;
		ShortlistedJobsModel::whereIn('i_shortlist_id', $shortlistIds)->where('i_user_id', $userId)->delete();

		$responseData = array(
			'code' => 200,
			'message' => "",
			'data' => array(),
		);
		return response()->json($responseData, 200);
	}
}
