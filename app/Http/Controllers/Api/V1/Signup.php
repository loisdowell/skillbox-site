<?php
namespace App\Http\Controllers\Api\V1;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage,File;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Country as CountryModel;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use League\Flysystem\Filesystem;

class Signup extends Controller {

	protected $section;
	private $mangopay;

	public function __construct(\MangoPay\MangoPayApi $mangopay){
		$this->section = Lang::get('section.dashboard');
		$this->mangopay = $mangopay;
	}
	
	public function index() {

		$post_data = Request::all();
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');

		$data = Request::all(); 
		$rules = [
			'e_type' => 'required',
			'v_service'=> 'required',
			'v_fname' => 'required',
			'v_lname' => 'required',
			'v_email' => 'required|email',
			'password' => 'required',
			'v_device_token' => 'required',
		];
		
		$validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
	
		$usertype="";
		$userservice="";
		
		if(isset($post_data['e_type'])){
			$usertype = $post_data['e_type'];
			unset($post_data['e_type']);
		}
		
		if(isset($post_data['v_service'])){
			$userservice = $post_data['v_service'];
			unset($post_data['v_service']);
		}
		
		$existEmail = UsersModel::where('v_email',$post_data['v_email'])->get();
		if(count($existEmail)){
			$responseData=array(
                'code'=>400,
                'message'=>"Email already exist, please use different email address.",
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		if(isset($post_data['password'])){
			$post_data['password'] = Hash::make($post_data['password']);
		}

		$data = GeneralHelper::baseRegistrationPlan();
		
		$post_data['v_buyer_online_service'] = "inactive";
		$post_data['v_buyer_inperson_service'] = "inactive";
		$post_data['v_seller_online_service'] = "inactive";
		$post_data['v_seller_inperson_service'] = "inactive";

		if($usertype=="seller"){
			if($userservice=="courses"){
				$post_data['seller']['v_service'] = "online";
				$post_data['seller']['e_temptype'] = "courses";
				$post_data['v_seller_online_service'] = "active";
			}else{
				if($userservice=="inperson"){
					$post_data['v_seller_inperson_service'] = "active";					
				}else{
					$post_data['v_seller_online_service'] = "active";	
				}
				$post_data['seller']['v_service'] = $userservice;
			}
			$post_data['seller']['e_status'] = "active";
			$post_data['buyer']['v_service'] = $post_data['seller']['v_service'];
			$post_data['buyer']['e_status'] = "inactive";
		}

		if($usertype=="buyer"){
			
			if($userservice=="courses"){
				$post_data['buyer']['v_service'] = "online";
				$post_data['seller']['v_service'] = "online";
				$post_data['v_buyer_online_service'] = "active";
			}else{
			
				if($userservice=="inperson"){
					$post_data['v_buyer_inperson_service'] = "active";					
				}else{
					$post_data['v_buyer_online_service'] = "active";	
				}
				$post_data['buyer']['v_service'] = $userservice;		
				$post_data['seller']['v_service'] = $userservice;
			}
		
			$post_data['buyer']['e_status'] = "active";
			$post_data['seller']['v_service'] = $userservice;
			$post_data['seller']['e_status'] = "inactive";

		}
		
		$post_data['v_plan']=array(
			'id'=>"5a65b48cd3e812a4253c9869",
			'duration'=>"monthly",
			'd_start_date'=>date("Y-m-d"),
			'd_end_date'=>date('Y-m-d',strtotime('+4000 days')),
		);
		
		$color=self::getAvatarColor();
		$user_name = $post_data['v_fname']." ".$post_data['v_lname'];
		$avatar = new \LasseRafn\InitialAvatarGenerator\InitialAvatar();
		$image_avatar = $avatar->name($user_name)
              ->length(2)
              ->fontSize(0.5)
              ->size(250) 
              ->background('#ddd')
              ->color('#222')
              ->generate()
              ->stream('png', 100);

        $fileName = 'profile-'.time().'.png';
	    $uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, $image_avatar->__toString(), 'public');     
	    $post_data['v_image']="users/".$fileName;
		
		$auth = auth()->guard('web');	
		$post_data['e_status']="active";
		$post_data['e_email_confirm']="no";
		$post_data['v_level']="0";
		$post_data['v_replies_time']="24 hours";
		$post_data['i_total_avg_review']=0;
		$post_data['i_total_review']=0;
		$post_data['i_course_total_review']=0;
		$post_data['i_course_total_avg_review']=0;
		$post_data['i_job_total_review']=0;
		$post_data['i_job_total_avg_review']=0;

		$post_data['e_login']="yes";
		$post_data['d_added']=date("Y-m-d H:i:s");
		$post_data['d_modified']=date("Y-m-d H:i:s");
		
		if(isset($post_data['v_company_type']) && $post_data['v_company_type']=="INDIVIDUAL"){
			$res = self::createMangopayUser($post_data);
			$post_data['i_mangopay_id']= $res['i_mangopay_id'];
			$post_data['i_wallet_id']= $res['i_wallet_id'];
		}else{
			$res = self::createMangopayUserLegal($post_data);
			$post_data['i_mangopay_id']= $res['i_mangopay_id'];
			$post_data['i_wallet_id']= $res['i_wallet_id'];
		}
		
		$token = GeneralHelper::generateToken();
		$post_data['v_device_token']=$post_data['v_device_token'];
		$post_data['v_auth_token']=$token;
		$userid = UsersModel::create($post_data)->id;

		$data=array(
			'name'=>$user_name,
			'confirm_account_link'=>url("confirm/account").'/'.$userid,
		);
		$mailcontent = EmailtemplateHelper::ConfirmUserAccount($data);
		$subject = "Confirm Email";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5ad81f45d3e812c30b3c9869");

		$mailids=array(
			$user_name=>$post_data['v_email']
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		
		$resdata['user']['v_fname'] = $post_data['v_fname'];
		$resdata['user']['v_lname'] = $post_data['v_lname'];
		$resdata['user']['v_email'] = $post_data['v_email'];
		$resdata['user']['v_image'] = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/".$post_data['v_image'];
		$resdata['user']['v_auth_token'] = $post_data['v_auth_token'];
		
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.signup.Success'),
			'data'=>$resdata,
		);
		return response()->json($responseData, 200);
		
	}

	public function getAvatarColor(){ 

		$a=array("900c3f","c70039","ff5733","ffc305","ff0554","7e0742","24900f","000","94d7df","7c543f","d21a1d","eb7322","fcd93c"); 
		$avatarColor=array_rand($a,1); 
		$Color = $a[$avatarColor]; 
		return $Color; 
	}

	public function createMangopayUserLegal($data){
		
		$response['i_mangopay_id']=0;
		$response['i_wallet_id']=0;

		if(!isset($data['v_company_name'])){
			$data['v_company_name']="Company name";
		}

		$UserLegal = new \MangoPay\UserLegal();
		$UserLegal->LegalPersonType=$data['v_company_type'];
		$UserLegal->Name = $data['v_company_name'];
		$UserLegal->LegalRepresentativeBirthday=1463496101;
		$UserLegal->LegalRepresentativeCountryOfResidence="GB";
		$UserLegal->LegalRepresentativeNationality="GB";
		$UserLegal->LegalRepresentativeFirstName=$data['v_fname'];
		$UserLegal->LegalRepresentativeLastName=$data['v_lname'];
		$UserLegal->Email=$data['v_email'];
		$result = $this->mangopay->Users->Create($UserLegal);
		
		if(count($result)){
			
			$mangopayid = $result->Id;
			$Wallet = new \MangoPay\Wallet();
			$Wallet->Tag = "custom meta";
			$Wallet->Owners = array ($result->Id);
			$Wallet->Description = "My skillbox project";
			$Wallet->Currency = "GBP";
			$resultWallet = $this->mangopay->Wallets->Create($Wallet);

			$response['i_mangopay_id']=$result->Id;
			$response['i_wallet_id']=$resultWallet->Id;
		}

		return $response;
	}

	public function createMangopayUser($data){
		
		$response['i_mangopay_id']=0;
		$response['i_wallet_id']=0;

		$UserNatural = new \MangoPay\UserNatural();
		$UserNatural->FirstName = $data['v_fname'];//"First Name";
		$UserNatural->LastName = $data['v_lname'];
		$UserNatural->Birthday = 1463496101;
		$UserNatural->Nationality = "GB";
		$UserNatural->Email = $data['v_email'];
		$UserNatural->CountryOfResidence = "GB";
		$result = $this->mangopay->Users->Create($UserNatural);
		
		if(count($result)){
			$mangopayid = $result->Id;
			$Wallet = new \MangoPay\Wallet();
			$Wallet->Tag = "custom meta";
			$Wallet->Owners = array ($result->Id);
			$Wallet->Description = "My skillbox project";
			$Wallet->Currency = "GBP";
			$resultWallet = $this->mangopay->Wallets->Create($Wallet);
			$response['i_mangopay_id']=$result->Id;
			$response['i_wallet_id']=$resultWallet->Id;
		}

		return $response;
	}


	public function checkEmail(){


		header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
		
		$post_data = Request::all();

		$rules = [
			'v_email' => 'required|email',
        ];
	
		$validator = Validator::make($post_data	, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}


		$existEmail = UsersModel::where('v_email',$post_data['v_email'])->get();
		if(count($existEmail)){
			$responseData=array(
                'code'=>400,
                'message'=>Lang::get('api.common.keyExist', [ 'section' => 'Email']),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}else{
			$responseData=array(
                'code'=>200,
                'message'=>'Success',
                'data'=>array(),
            );
            return response()->json($responseData, 200);
		}
	}
}