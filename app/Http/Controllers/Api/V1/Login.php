<?php
namespace App\Http\Controllers\Api\V1;

use Request, Hash, Lang,Crypt,Session,Validator,Auth;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Users as UsersModel;

class Login extends Controller {

	
	public function index() {
		
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');

		$data = Request::all(); 
		$rules = [
			'v_email' => 'required|email',
			'password' => 'required',
			'v_device_token' => 'required',
        ];
	
		$validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		
		$credentials = Request::only('v_email', 'password');
		$auth = auth()->guard('appauth');
		
		if($auth->attempt($credentials, false)) {
			
			$token = GeneralHelper::generateToken();
			$userid = $auth->user()->id;
			
			$update['v_device_token']=$data['v_device_token'];
			$update['v_auth_token']=$token;
			UsersModel::find($userid)->update($update);
			
			$resdata['user']['v_fname'] = $auth->user()->v_fname;
			$resdata['user']['v_lname'] = $auth->user()->v_lname; 
			$resdata['user']['v_email'] = $auth->user()->v_email;
			$resdata['user']['v_image'] = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/".$auth->user()->v_image;
			$resdata['user']['v_auth_token'] = $token;
			
			$responseData=array(
                'code'=>200,
                'message'=>Lang::get('api.login.loginSuccess'),
                'data'=>$resdata,
            );
            return response()->json($responseData, 200);

		}else{
			$responseData=array(
                'code'=>401,
                'message'=>Lang::get('api.login.invalidCredentials'),
                'data'=>array(),
			);
		    return response()->json($responseData, 401);
		}
	}
	
	public function forgotPassword() {


		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		
		$data = Request::all(); 
		$rules = [
			'v_email' => 'required|email',
	    ];
		
		$validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$userdata = UsersModel::where('v_email',$data['v_email'])->first();
		
		if(count($userdata)){

			$username = "";
			if(isset($userdata->v_fname)){
			   	$username = $userdata->v_fname;
			}
			if(isset($userdata->v_lname)){
			   	$username .= " ".$userdata->v_lname;
			}
			$encryptedid = Crypt::encrypt($userdata->id);
			$passwordreseturl= url('reset-password').'/'.$encryptedid;

			$useremail="";
			if(isset($userdata->v_email)){
			   	$useremail .= " ".$userdata->v_email;
			}
			$data=array(
				'name'=>$username,
				'passwordreseturl'=>$passwordreseturl,
			);
			$mailcontent = EmailtemplateHelper::ForgotPassword($data);
			$subject = EmailtemplateHelper::EmailTemplateSubject("5a60a0f1d3e812bc4b3c986a");
			$mailids=array(
				$username=>$useremail
			);
			$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
			$resdata=array();
			$responseData=array(
                'code'=>200,
                'message'=>Lang::get('api.login.forgotPasswordSuccess'),
                'data'=>$resdata,
            );
            return response()->json($responseData, 200);

		}else{
			$responseData=array(
                'code'=>401,
                'message'=>Lang::get('api.login.invalidEmail'),
                'data'=>array(),
			);
			return response()->json($responseData, 401);
			
		}
	}

	public function logOut() {
		
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		
		$authdata = Request::get('authdata');
		
		$update['v_auth_token']="";
		UsersModel::find($authdata->id)->update($update);
		$resdata=array();
		$responseData=array(
			'code'=>200,
			'message'=>Lang::get('api.login.logoutSuccess'),
			'data'=>$resdata,
		);
		return response()->json($responseData, 200);
		
	}

}