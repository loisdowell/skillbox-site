<?php

namespace App\Http\Controllers\Api\V1\Search;

use Request, Hash, Lang, Crypt, Session, Validator, Auth, Storage, File;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Support\SupportQuery as SupportQueryModel;
use App\Models\Country as CountryModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Sellerreview as SellerreviewModal;
use App\Models\CompanyEthos as CompanyEthosModal;
use App\Models\Users\Buyerreview as BuyerreviewModal;
use App\Models\Pricefilter as PricefilterModal;
use App\Models\ManageMetaField as ManageMetaFieldModel;
use App\Models\Users\Order as OrderModal;
use App\Models\Search as SearchModal;
use App\Models\Emailsend as Emailsend;
use App\Models\Sitesettings as SiteSettingsModel;
use App\Models\Guide as GuideModel;
use App\Models\Users\Messages as MessagesModel;
use App\Models\Courses\BuyerCourse as BuyerCourseModel;


class Search extends Controller
{


	public function index()
	{

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');

		$data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$rules = [
			'e_type' => 'required',
			'e_type_data' => 'required',
		];

		$validator = Validator::make($data, $rules);
		if ($validator->fails()) {
			$responseData = array(
				'code' => 400,
				'message' => $validator->errors()->first(),
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		if ($data['e_type_data'] == "skill") {
			$data['v_type_skill'] = "on";
		}
		if ($data['e_type_data'] == "job") {
			$data['v_type_job'] = "on";
		}
		
		if (isset($data['e_type']) && $data['e_type'] == "courses"){

			$coursesLanguage = CoursesLanguageModel::where("e_status","active")->get();
			$coursesCategory = CoursesCategoryModel::where("e_status","active")->where("i_delete","!=",'1')->orderBy('v_title')->get();
			$coursesLevel = CoursesLevelModel::where("e_status","active")->get();
			$pricefilter = array();
		
			$language=array();
			if(isset($coursesLanguage) && count($coursesLanguage)){
				foreach ($coursesLanguage as $k=>$v){
					$language[$v->id]['name']=$v->v_title;
					$language[$v->id]['cnt']=0;
				}   
			}
		

			$s_course=array();
			$shortlistedCourse = ShortlistedCourseModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedCourse as $key => $value) {
				$s_course[] = $value->i_shortlist_id;
			}
			$finalresultmob=array();

			$cids=array();
			if (isset($data['existcoursesids']) && $data['existcoursesids'] != "") {
				$cids = explode(",", $data['existcoursesids']);
			}
			if(isset($data['i_language_id']) && $data['i_language_id']!=""){
				$data['i_language_id'] = explode(",",$data['i_language_id']);
			}

			$_S3_PATH = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com"; //env("IMAP_HOSTNAME_TEST", "https://skillboxs3bucket.s3.us-east-2.amazonaws.com");

			$coursessidebar = GeneralHelper::CourseSearchNormalSidebar($data,$pricefilter);
			$coursessidebar1 = GeneralHelper::CourseSearchNormalSidebar1($data,$pricefilter);

			if(isset($coursessidebar1) && count($coursessidebar1)){
				foreach($coursessidebar1 as $k=>$v){
					if(isset($language[$v->i_language_id])){
						$language[$v->i_language_id]['cnt']=$language[$v->i_language_id]['cnt']+1;
					}
				}
			}
			
			$courses = GeneralHelper::CourseSearchBold($data,$pricefilter,$cids);
			if(count($courses)){
				foreach ($courses as $key => $value) {

					$imgins="";
					if(isset($value->v_course_thumbnail) && $value->v_course_thumbnail!=""){
						$imgins = $value->v_course_thumbnail;
					}
					$imgdata=$_S3_PATH.'/'.$imgins;

					$is_shortlist = 0;
					if (in_array($value->id, $s_course)) {
						$is_shortlist = 1;
					}
				
					$tempData=array(
						'id' => $value->id,
						'v_title' => $value->v_title,
						'v_sub_title' => $value->v_sub_title,
						'v_author_name' => $value->v_author_name,
						'f_price' => $value->f_price,
						'i_total_avg_review' => number_format($value->i_total_avg_review,1),
						'i_total_review' => number_format($value->i_total_review,1),
						'v_image' =>$imgdata,
						'is_shortlist' =>$is_shortlist,
					);
					$finalresultmob[] = $tempData;
					
					if(isset($value->e_sponsor_status) && $value->e_sponsor_status=="start"){
						$update=array();	
						if(isset($value['i_impression'])){
							$update['i_impression']=$value['i_impression']+1;			
						}else{
							$update['i_impression']=1;
						}
						if(isset($value['i_impression_perday'])){
							$update['i_impression_perday']=$value['i_impression_perday']+1;			
						}else{
							$update['i_impression_perday']=1;
						}
					
						if(isset($value['i_impression_day'])){
							$update['i_impression_day']=$value['i_impression_day']+1;			
						}else{
							$update['i_impression_day']=1;
						}
						CoursesModel::find($value['_id'])->update($update);
					}
					$cids[]	=$value->id;
				}
			
			}
		
			$courses = GeneralHelper::CourseSearchNew($data,$pricefilter,$cids);
			if(count($courses)){
				foreach ($courses as $key => $value) {

					$imgins="";
					if(isset($value->v_course_thumbnail) && $value->v_course_thumbnail!=""){
						$imgins = $value->v_course_thumbnail;
					}
					$imgdata=$_S3_PATH.'/'.$imgins;

					$is_shortlist = 0;
					if (in_array($value->id, $s_course)) {
						$is_shortlist = 1;
					}
				
					$tempData=array(
						'id' => $value->id,
						'v_title' => $value->v_title,
						'v_sub_title' => $value->v_sub_title,
						'v_author_name' => $value->v_author_name,
						'f_price' => $value->f_price,
						'i_total_avg_review' => number_format($value->i_total_avg_review,1),
						'i_total_review' => number_format($value->i_total_review,1),
						'v_image' =>$imgdata,
						'is_shortlist' =>$is_shortlist,
					);
					$finalresultmob[] = $tempData;

					if(isset($value->e_sponsor_status) && $value->e_sponsor_status=="start"){
						
						$update=array();	
						if(isset($value['i_impression'])){
							$update['i_impression']=$value['i_impression']+1;			
						}else{
							$update['i_impression']=1;
						}
						if(isset($value['i_impression_perday'])){
							$update['i_impression_perday']=$value['i_impression_perday']+1;			
						}else{
							$update['i_impression_perday']=1;
						}

						if(isset($value['i_impression_day'])){
							$update['i_impression_day']=$value['i_impression_day']+1;			
						}else{
							$update['i_impression_day']=1;
						}
						CoursesModel::find($value['_id'])->update($update);
					}else{
						$update=array();
						if(isset($value['i_impression_day'])){
							$update['i_impression_day']=$value['i_impression_day']+1;			
						}else{
							$update['i_impression_day']=1;
						}
						CoursesModel::find($value['_id'])->update($update);
					}

					$cids[]	=$value->id;
				}

				
			}

			$courses = GeneralHelper::CourseSearchNormal($data,$pricefilter,$cids);
			if(count($courses)){
				foreach ($courses as $key => $value) {

					$imgins="";
					if(isset($value->v_course_thumbnail) && $value->v_course_thumbnail!=""){
						$imgins = $value->v_course_thumbnail;
					}
					$imgdata=$_S3_PATH.'/'.$imgins;

					$is_shortlist = 0;
					if (in_array($value->id, $s_course)) {
						$is_shortlist = 1;
					}
				
					$tempData=array(
						'id' => $value->id,
						'v_title' => $value->v_title,
						'v_sub_title' => $value->v_sub_title,
						'v_author_name' => $value->v_author_name,
						'f_price' => $value->f_price,
						'i_total_avg_review' => number_format($value->i_total_avg_review,1),
						'i_total_review' => number_format($value->i_total_review,1),
						'v_image' =>$imgdata,
						'is_shortlist' =>$is_shortlist,
					);
					$finalresultmob[] = $tempData;

					if(isset($value->e_sponsor_status) && $value->e_sponsor_status=="start"){

						$update=array();	
						if(isset($value['i_impression'])){
							$update['i_impression']=$value['i_impression']+1;			
						}else{
							$update['i_impression']=1;
						}
						if(isset($value['i_impression_perday'])){
							$update['i_impression_perday']=$value['i_impression_perday']+1;			
						}else{
							$update['i_impression_perday']=1;
						}	
						if(isset($value['i_impression_day'])){
							$update['i_impression_day']=$value['i_impression_day']+1;			
						}else{
							$update['i_impression_day']=1;
						}
						CoursesModel::find($value['_id'])->update($update);
					}else{
						$update=array();
						if(isset($value['i_impression_day'])){
							$update['i_impression_day']=$value['i_impression_day']+1;			
						}else{
							$update['i_impression_day']=1;
						}
						CoursesModel::find($value['_id'])->update($update);
					}
					$cids[]	=$value->id;
				}
			}

			
			$response_exist['existcoursesids'] = "";
			if (count($cids)) {
				$response_exist['existcoursesids'] = implode(",", $cids);
			}

			$is_next_page = false;
			if (count($coursessidebar) > count($finalresultmob)) {
				$is_next_page = true;
			}

			$languageList=array();
			if(count($language)){
				foreach($language as $k=>$v){
					$languageList[]=array(
						'id'=>$k,
						'name'=>$v['name'],
						'cnt'=>$v['cnt'],
					);
				}
			}

			$relavance[]=array(
				'key'=>'latest',
				'value'=>'Newest First',
			);	
			
			$relavance[]=array(
				'key'=>'toprated',
				'value'=>'Top Rated',
			);	

			$relavance[]=array(
				'key'=>'lowtohigh',
				'value'=>'Price: Low to High',
			);	
			$relavance[]=array(
				'key'=>'hightolow',
				'value'=>'Price: High to Low',
			);	
			$relavance[]=array(
				'key'=>'sponsored',
				'value'=>'Sponsored',
			);	
			
			$_data = array(
				'data' => $data,
				'list' => $finalresultmob,
				'categories' => $coursesCategory,
				'existcoursesids' => $response_exist['existcoursesids'],
				'is_next_page' => $is_next_page,
				'language' => $languageList,
				'language' => $languageList,
				'relavance'=>$relavance,
			);
			
			$responseData = array(
				'code' => 200,
				'message' => "",
				'data' => $_data,
			);
			return response()->json($responseData, 200);


		
			
		}

		if (isset($data['e_type']) && $data['e_type'] == "online") {

			// if(!isset($data['v_type_skill']) && !isset($data['v_type_job'])){
			// 	$data['v_type_skill'] = "on";
			// }

			$pricefilter = PricefilterModal::where("e_status", "active")->orderBy("i_order")->get();
			$user_ids = $skill_users = $job_users = $s_skill = $s_job = array();
			$finalresult = array();
			$finalresultmob = array();
			$existskillids = array();
			$existjobids = array();


			$shortlistedSkill = ShortlistedSkillsModel::where('i_user_id', $userid)->get();
			foreach ($shortlistedSkill as $key => $value) {
				$s_skill[] = $value->i_shortlist_id;
			}

			$shortlistedJobs = ShortlistedJobsModel::where('i_user_id', $userid)->get();
			foreach ($shortlistedJobs as $key => $value) {
				$s_job[] = $value->i_shortlist_id;
			}

			if (isset($data['existskillids']) && $data['existskillids'] != "") {
				$existskillids = explode(",", $data['existskillids']);
			}
			if (isset($data['existjobids']) && $data['existjobids'] != "") {
				$existjobids = explode(",", $data['existjobids']);
			}
		
			$totaljob = 0;
			$totalskill = 0;

			//Bold Adds Skill
			if(isset($data['v_type_skill']) && $data['v_type_skill'] == "on") {

				$skilldata = array();
				if (count($existskillids) < 1) {
					$skilldata = GeneralHelper::SkillSearchBold($data, $pricefilter, $existskillids);
				} else {
					$skilldata = GeneralHelper::SkillSearchBoldLoad($data, $pricefilter, $existskillids);
				}

				if (count($skilldata)) {
					foreach ($skilldata as $key => $value) {
						$finalresult[] = array(
							'type' => "skill",
							'data' => $value,
						);
						if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
							$update = array();
							if (isset($value['i_impression'])) {
								$update['i_impression'] = $value['i_impression'] + 1;
							} else {
								$update['i_impression'] = 1;
							}
							if (isset($value['i_impression_perday'])) {
								$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
							} else {
								$update['i_impression_perday'] = 1;
							}

							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}
						$user_ids[] = $value['i_user_id'];
						$existskillids[] = $value['_id'];
						$totalskill = $totalskill + 1;
					}
				}
			}

			if (isset($data['v_type_job']) && $data['v_type_job'] == "on") {

				$jobsdata = GeneralHelper::JobSearchBold($data, $pricefilter, $existjobids);

				if (count($jobsdata)) {
					foreach ($jobsdata as $key => $value) {


						if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {

							$update['i_impression'] = 1;
							if (isset($value['i_impression'])) {
								$update['i_impression'] = $value['i_impression'] + 1;
							}
							if (isset($value['i_impression_perday'])) {
								$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
							} else {
								$update['i_impression_perday'] = 1;
							}

							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							JobsModel::find($value['_id'])->update($update);
						}
						$finalresult[] = array(
							'type' => "jobs",
							'data' => $value,
						);
						$user_ids[] = $value['i_user_id'];
						$existjobids[] = $value['_id'];
						$totaljob = $totaljob + 1;
					}
				}
			}




			//Latest user data 24 hours
			if (count($existskillids) < 1) {
				if (isset($data['v_type_skill']) && $data['v_type_skill'] == "on") {

					$skilldata = GeneralHelper::SkillSearchNew($data, $existskillids, $pricefilter);

					if (count($skilldata)) {
						foreach ($skilldata as $key => $value) {
							$finalresult[] = array(
								'type' => "skill",
								'data' => $value,
							);

							if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
								$update = array();
								if (isset($value['i_impression'])) {
									$update['i_impression'] = $value['i_impression'] + 1;
								} else {
									$update['i_impression'] = 1;
								}
								if (isset($value['i_impression_perday'])) {
									$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
								} else {
									$update['i_impression_perday'] = 1;
								}
								if (isset($value['i_impression_day'])) {
									$update['i_impression_day'] = $value['i_impression_day'] + 1;
								} else {
									$update['i_impression_day'] = 1;
								}
								SellerprofileModel::find($value['_id'])->update($update);
							} else {
								$update = array();
								if (isset($value['i_impression_day'])) {
									$update['i_impression_day'] = $value['i_impression_day'] + 1;
								} else {
									$update['i_impression_day'] = 1;
								}
								SellerprofileModel::find($value['_id'])->update($update);
							}

							$user_ids[] = $value['i_user_id'];
							$existskillids[] = $value['_id'];
							$totalskill = $totalskill + 1;
						}
					}
				}
			}

			if (count($existjobids) < 1) {
				if (isset($data['v_type_job']) && $data['v_type_job'] == "on") {

					$jobsdata = GeneralHelper::JobSearchNew($data, $pricefilter, $existjobids);

					if (count($jobsdata)) {
						foreach ($jobsdata as $key => $value) {


							if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
								$update['i_impression'] = 1;
								if (isset($value['i_impression'])) {
									$update['i_impression'] = $value['i_impression'] + 1;
								}
								if (isset($value['i_impression_perday'])) {
									$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
								} else {
									$update['i_impression_perday'] = 1;
								}
								if (isset($value['i_impression_day'])) {
									$update['i_impression_day'] = $value['i_impression_day'] + 1;
								} else {
									$update['i_impression_day'] = 1;
								}

								JobsModel::find($value['_id'])->update($update);
							} else {
								$update = array();
								if (isset($value['i_impression_day'])) {
									$update['i_impression_day'] = $value['i_impression_day'] + 1;
								} else {
									$update['i_impression_day'] = 1;
								}
								JobsModel::find($value['_id'])->update($update);
							}

							$finalresult[] = array(
								'type' => "jobs",
								'data' => $value,
							);
							$user_ids[] = $value['i_user_id'];
							$existjobids[] = $value['_id'];
							$totaljob = $totaljob + 1;
						}
					}
				}
			}

			// urgent Job Listing
			if (isset($data['v_type_job']) && $data['v_type_job'] == "on") {

				$jobsdata = GeneralHelper::JobSearchUrgent($data, $pricefilter, $existjobids);

				if (count($jobsdata)) {
					foreach ($jobsdata as $key => $value) {
						if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
							$update['i_impression'] = 1;
							if (isset($value['i_impression'])) {
								$update['i_impression'] = $value['i_impression'] + 1;
							}
							if (isset($value['i_impression_perday'])) {
								$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
							} else {
								$update['i_impression_perday'] = 1;
							}

							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}

							JobsModel::find($value['_id'])->update($update);
						} else {
							$update = array();
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

						$finalresult[] = array(
							'type' => "jobs",
							'data' => $value,
						);
						$user_ids[] = $value['i_user_id'];
						$existjobids[] = $value['_id'];
						$totaljob = $totaljob + 1;
					}
				}
			}

			//Normal Listing
			if (isset($data['v_type_skill']) && $data['v_type_skill'] == "on") {

				$skilldata = GeneralHelper::SkillSearchNoraml($data, $existskillids, $pricefilter);


				if (count($skilldata)) {
					foreach ($skilldata as $key => $value) {
						$finalresult[] = array(
							'type' => "skill",
							'data' => $value,
						);

						$user_ids[] = $value['i_user_id'];
						$existskillids[] = $value['_id'];
						$totalskill = $totalskill + 1;

						if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
							$update = array();
							if (isset($value['i_impression'])) {
								$update['i_impression'] = $value['i_impression'] + 1;
							} else {
								$update['i_impression'] = 1;
							}
							if (isset($value['i_impression_perday'])) {
								$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
							} else {
								$update['i_impression_perday'] = 1;
							}
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						} else {
							$update = array();
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

						$user_ids[] = $value['i_user_id'];
						// $existskillids[] = $value['_id'];
						$totalskill = $totalskill + 1;
					}
				}
			}

			if (isset($data['v_type_job']) && $data['v_type_job'] == "on") {

				$jobsdata = GeneralHelper::JobSearchNormal($data, $pricefilter, $existjobids);

				if (count($jobsdata)) {
					foreach ($jobsdata as $key => $value) {

						if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
							$update['i_impression'] = 1;
							if (isset($value['i_impression'])) {
								$update['i_impression'] = $value['i_impression'] + 1;
							}
							if (isset($value['i_impression_perday'])) {
								$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
							} else {
								$update['i_impression_perday'] = 1;
							}
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							JobsModel::find($value['_id'])->update($update);
						} else {
							$update = array();
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

						$finalresult[] = array(
							'type' => "jobs",
							'data' => $value,
						);
						$user_ids[] = $value['i_user_id'];
						$existjobids[] = $value['_id'];
						$totaljob = $totaljob + 1;
					}
				}
			}

			$user_datalist = UsersModel::query()->whereIn('_id', $user_ids)->get();
			$user_data = array();
			foreach ($user_datalist as $key => $value) {
				$user_data[$value->_id] =  $value;
			}

			$categories = CategoriesModel::where("e_status", "active")->where('v_type', "online")->orderBy('v_name')->get();
			$skills = array();
			if (isset($data['i_category_id']) && $data['i_category_id'] != "") {
				$skills = SkillsModel::whereIn('i_category_id', explode(',', $data['i_category_id']))->orderBy('v_name')->get();
			}

			$_S3_PATH = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/"; //env("IMAP_HOSTNAME_TEST", "https://skillboxs3bucket.s3.us-east-2.amazonaws.com");

			$finalresultmob = array();
			if (count($finalresult)) {
				foreach ($finalresult as $k => $v) {

					if ($v['type'] == "jobs") {

						// $existjobids[] = $v['data']['_id'];
						$f_name = "";
						$l_name = "";
						$user_image = "";
						if (isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])) {
							if (isset($user_data[$v['data']['i_user_id']]['v_fname'])) {
								$f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
							}
							if (isset($user_data[$v['data']['i_user_id']]['v_image'])) {
								$user_image = $_S3_PATH . $user_data[$v['data']['i_user_id']]['v_image'];
							}
							if (isset($user_data[$v['data']['i_user_id']]['v_lname'])) {
								$l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
							}
						}

						$imgins = "";
						if (isset($v['data']['v_photos'][0])) {
							$imgins = $v['data']['v_photos'][0];
						}
						$imgdata = "";
						if ($imgins != "") {
							$imgdata = $_S3_PATH . '/' . $imgins;
						} else {
							$imgdata = $_S3_PATH . '/jobpost/photos/No_Image_Available.jpg';
						}

						$e_sponsor_status = 0;
						if (isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == "start") {
							$e_sponsor_status = 1;
						}
						$is_shortlist = 0;
						if (in_array($v['data']['_id'], $s_job)) {
							$is_shortlist = 1;
						}
						$is_urgent = 0;
						if (isset($v['data']['e_urgent']) && $v['data']['e_urgent'] == "yes") {
							$is_urgent = 1;
						}
						$finalresultmob[] = array(
							'type' => "job",
							'id' => $v['data']['_id'],
							'v_user_name' => $f_name . ' ' . $l_name,
							'v_user_image' => $user_image,
							'v_image' => $imgdata,
							'v_title' => $v['data']['v_job_title'],
							'i_user_id' => $v['data']['i_user_id'],
							'l_short_description' => str_limit($v['data']['l_short_description'], 60, '...'),
							'i_total_avg_review' => number_format($value['data']['i_total_avg_review'], 2),
							'i_total_review' => $v['data']['i_total_review'],
							'is_shortlist' => $is_shortlist,
							'e_sponsor_status' => $e_sponsor_status,
							'v_budget_amt' => $v['data']['v_budget_amt'],
							'v_budget_type' => $v['data']['v_budget_type'],
							'is_urgent' => $is_urgent,
						);
					} else {

						// $existskillids[] = $v['data']['_id'];
						$f_name = "";
						$l_name = "";
						$user_image = "";

						if (isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])) {
							if (isset($user_data[$v['data']['i_user_id']]['v_fname'])) {
								$f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
							}
							if (isset($user_data[$v['data']['i_user_id']]['v_image'])) {
								$user_image = $_S3_PATH . $user_data[$v['data']['i_user_id']]['v_image'];
							}
							if (isset($user_data[$v['data']['i_user_id']]['v_lname'])) {
								$l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
							}
						}

						$is_shortlist = 0;
						if (in_array($v['data']['_id'], $s_skill)) {
							$is_shortlist = 1;
						}
						$e_sponsor_status = 0;
						if (isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == "start") {
							$e_sponsor_status = 1;
						}

						$finalresultmob[] = array(
							'type' => "skill",
							'id' => $v['data']['_id'],
							'v_user_name' => $f_name . ' ' . $l_name,
							'v_user_image' => $user_image,
							'v_title' => $v['data']['v_profile_title'],
							'i_user_id' => $v['data']['i_user_id'],
							// 'l_short_description' => $v['data']['l_short_description'],
							'l_short_description' => str_limit($v['data']['l_short_description'], 60, '...'),
							'i_total_avg_review' => number_format($v['data']['i_total_avg_review'], 2),
							'i_total_review' => $v['data']['i_total_review'],
							'v_starting_at' => $v['data']['information']['basic_package']['v_price'],
							'is_shortlist' => $is_shortlist,
							'e_sponsor_status' => $e_sponsor_status,
							'type' => "skill",
						);
					}
				}
			}

			$skillsidebar = GeneralHelper::SkillSearchBoldSidebar($data, $pricefilter);
			$jobsidebar = GeneralHelper::JobSearchBoldSidebar($data, $pricefilter);


			$is_next_page = false;
			if ($data['e_type_data'] == "skill") {
				if (count($skillsidebar) > count($existskillids)) {
					$is_next_page = true;
				}
			}

			if ($data['e_type_data'] == "job") {
				if (count($jobsidebar) > count($existjobids)) {
					$is_next_page = true;
				}
			}


			$response_exist['existskillids'] = "";
			$response_exist['existjobids'] = "";
			if (count($existskillids)) {
				$response_exist['existskillids'] = implode(",", $existskillids);
			}
			if (count($existjobids)) {
				$response_exist['existjobids'] = implode(",", $existjobids);
			}

			$_data = array(
				'v_service' => "online",
				'data' => $data,
				'list' => $finalresultmob,
				'totaljob' => $totaljob,
				'totalskill' => $totalskill,
				'categories' => $categories,
				'existskillids' => $response_exist['existskillids'],
				'existjobids' => $response_exist['existjobids'],
				'is_next_page' => $is_next_page,
			);

			$responseData = array(
				'code' => 200,
				'message' => "",
				'data' => $_data,
			);
			return response()->json($responseData, 200);
		} else if (isset($data['e_type']) && $data['e_type'] == "inperson") {

			// echo "ddd";exit;
		
			// $pricefilter = PricefilterModal::where("e_status", "active")->orderBy("i_order")->get();
			$pricefilter = array();
			$milesfilter = array(
				'5' => "5",
				'10' => "10",
				'15' => "15",
				'25' => "25",
				'50' => "50",
			);
			


			$user_ids = $skill_users = $job_users = $s_skill = $s_job = array();
			$finalresult = array();
			$existskillids = array();
			$existjobids = array();

			$totaljob = 0;
			$totalskill = 0;
		
			if (isset($data['existskillids']) && $data['existskillids'] != "") {
				$existskillids = explode(",", $data['existskillids']);
			}
			if (isset($data['existjobids']) && $data['existjobids'] != "") {
				$existjobids = explode(",", $data['existjobids']);
			}



			//Bold Adds Skill
			if (isset($data['v_type_skill']) && $data['v_type_skill'] == "on") {

				$skilldata = GeneralHelper::InpersonSkillSearchBold($data, $pricefilter, $existskillids);
				if (count($skilldata)) {
					foreach ($skilldata as $key => $value) {
						$finalresult[] = array(
							'type' => "skill",
							'data' => $value,
						);
						$user_ids[] = $value['i_user_id'];
						$existskillids[] = $value['_id'];
						$totalskill = $totalskill + 1;

						if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
							$update = array();
							if (isset($value['i_impression'])) {
								$update['i_impression'] = $value['i_impression'] + 1;
							} else {
								$update['i_impression'] = 1;
							}
							if (isset($value['i_impression_perday'])) {
								$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
							} else {
								$update['i_impression_perday'] = 1;
							}
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						} else {
							$update = array();
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}
					}
				}
			}



			if (isset($data['v_type_job']) && $data['v_type_job'] == "on") {

				$jobsdata = GeneralHelper::InpersonJobSearchBold($data, $pricefilter, $existjobids);

				if (count($jobsdata)) {
					foreach ($jobsdata as $key => $value) {

						if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
							$update['i_impression'] = 1;
							if (isset($value['i_impression'])) {
								$update['i_impression'] = $value['i_impression'] + 1;
							}
							if (isset($value['i_impression_perday'])) {
								$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
							} else {
								$update['i_impression_perday'] = 1;
							}
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							JobsModel::find($value['_id'])->update($update);
						} else {
							$update = array();
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

						$finalresult[] = array(
							'type' => "jobs",
							'data' => $value,
						);
						$user_ids[] = $value['i_user_id'];
						$existjobids[] = $value['_id'];
						$totaljob = $totaljob + 1;
					}
				}
			}


			//Latest user data 24 hours
			if (count($existskillids) < 1) {
				if (isset($data['v_type_skill']) && $data['v_type_skill'] == "on") {

					$skilldata = GeneralHelper::InpersonSkillSearchNew($data, $existskillids, $pricefilter);

					if (count($skilldata)) {
						foreach ($skilldata as $key => $value) {
							$finalresult[] = array(
								'type' => "skill",
								'data' => $value,
							);
							$user_ids[] = $value['i_user_id'];
							$existskillids[] = $value['_id'];
							$totalskill = $totalskill + 1;

							if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
								$update = array();
								if (isset($value['i_impression'])) {
									$update['i_impression'] = $value['i_impression'] + 1;
								} else {
									$update['i_impression'] = 1;
								}
								if (isset($value['i_impression_perday'])) {
									$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
								} else {
									$update['i_impression_perday'] = 1;
								}
								if (isset($value['i_impression_day'])) {
									$update['i_impression_day'] = $value['i_impression_day'] + 1;
								} else {
									$update['i_impression_day'] = 1;
								}
								SellerprofileModel::find($value['_id'])->update($update);
							} else {
								$update = array();
								if (isset($value['i_impression_day'])) {
									$update['i_impression_day'] = $value['i_impression_day'] + 1;
								} else {
									$update['i_impression_day'] = 1;
								}
								SellerprofileModel::find($value['_id'])->update($update);
							}
						}
					}
				}
			}

			if (count($existjobids) < 1) {
				if (isset($data['v_type_job']) && $data['v_type_job'] == "on") {

					$jobsdata = GeneralHelper::InpersonJobSearchNew($data, $pricefilter, $existjobids);
					if (count($jobsdata)) {
						foreach ($jobsdata as $key => $value) {

							if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
								$update['i_impression'] = 1;
								if (isset($value['i_impression'])) {
									$update['i_impression'] = $value['i_impression'] + 1;
								}
								if (isset($value['i_impression_perday'])) {
									$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
								} else {
									$update['i_impression_perday'] = 1;
								}
								if (isset($value['i_impression_day'])) {
									$update['i_impression_day'] = $value['i_impression_day'] + 1;
								} else {
									$update['i_impression_day'] = 1;
								}
								JobsModel::find($value['_id'])->update($update);
							} else {
								$update = array();
								if (isset($value['i_impression_day'])) {
									$update['i_impression_day'] = $value['i_impression_day'] + 1;
								} else {
									$update['i_impression_day'] = 1;
								}
								JobsModel::find($value['_id'])->update($update);
							}

							$finalresult[] = array(
								'type' => "jobs",
								'data' => $value,
							);
							$user_ids[] = $value['i_user_id'];
							$existjobids[] = $value['_id'];
							$totaljob = $totaljob + 1;
						}
					}
				}
			}

			// urgent Job Listing
			if (isset($data['v_type_job']) && $data['v_type_job'] == "on") {

				$jobsdata = GeneralHelper::InpersonJobSearchUrgent($data, $pricefilter, $existjobids);

				if (count($jobsdata)) {
					foreach ($jobsdata as $key => $value) {

						if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
							$update['i_impression'] = 1;
							if (isset($value['i_impression'])) {
								$update['i_impression'] = $value['i_impression'] + 1;
							}
							if (isset($value['i_impression_perday'])) {
								$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
							} else {
								$update['i_impression_perday'] = 1;
							}
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							JobsModel::find($value['_id'])->update($update);
						} else {
							$update = array();
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							JobsModel::find($value['_id'])->update($update);
						}
						$finalresult[] = array(
							'type' => "jobs",
							'data' => $value,
						);
						$user_ids[] = $value['i_user_id'];
						$existjobids[] = $value['_id'];
						$totaljob = $totaljob + 1;
					}
				}
			}

			//Normal Listing
			if (isset($data['v_type_skill']) && $data['v_type_skill'] == "on") {

				$skilldata = GeneralHelper::InpersonSkillSearchNoraml($data, $existskillids, $pricefilter);

				if (count($skilldata)) {
					foreach ($skilldata as $key => $value) {
						$finalresult[] = array(
							'type' => "skill",
							'data' => $value,
						);
						$user_ids[] = $value['i_user_id'];
						$existskillids[] = $value['_id'];
						$totalskill = $totalskill + 1;

						if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
							$update = array();
							if (isset($value['i_impression'])) {
								$update['i_impression'] = $value['i_impression'] + 1;
							} else {
								$update['i_impression'] = 1;
							}
							if (isset($value['i_impression_perday'])) {
								$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
							} else {
								$update['i_impression_perday'] = 1;
							}
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						} else {
							$update = array();
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}
					}
				}
			}

			if (isset($data['v_type_job']) && $data['v_type_job'] == "on") {

				$jobsdata = GeneralHelper::InpersonJobSearchNormal($data, $pricefilter, $existjobids);

				if (count($jobsdata)) {
					foreach ($jobsdata as $key => $value) {

						if (isset($value['e_sponsor_status']) && $value['e_sponsor_status'] == "start") {
							$update['i_impression'] = 1;
							if (isset($value['i_impression'])) {
								$update['i_impression'] = $value['i_impression'] + 1;
							}
							if (isset($value['i_impression_perday'])) {
								$update['i_impression_perday'] = $value['i_impression_perday'] + 1;
							} else {
								$update['i_impression_perday'] = 1;
							}
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							JobsModel::find($value['_id'])->update($update);
						} else {
							$update = array();
							if (isset($value['i_impression_day'])) {
								$update['i_impression_day'] = $value['i_impression_day'] + 1;
							} else {
								$update['i_impression_day'] = 1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

						$finalresult[] = array(
							'type' => "jobs",
							'data' => $value,
						);
						$user_ids[] = $value['i_user_id'];
						$existjobids[] = $value['_id'];
						$totaljob = $totaljob + 1;
					}
				}
			}

			$user_datalist = UsersModel::query()->whereIn('_id', $user_ids)->get();
			$user_data = array();
			foreach ($user_datalist as $key => $value) {
				$user_data[$value->_id] =  $value;
			}

			$shortlistedSkill = ShortlistedSkillsModel::where('i_user_id', $userid)->get();
			foreach ($shortlistedSkill as $key => $value) {
				$s_skill[] = $value->i_shortlist_id;
			}

			$shortlistedJobs = ShortlistedJobsModel::where('i_user_id', $userid)->get();
			foreach ($shortlistedJobs as $key => $value) {
				$s_job[] = $value->i_shortlist_id;
			}

			$categories = CategoriesModel::where("e_status", "active")->where('v_type', "inperson")->orderBy('v_name')->get();
			$skills = array();
			if (isset($data['i_category_id']) && $data['i_category_id'] != "") {
				$skills = SkillsModel::whereIn('i_category_id', explode(',', $data['i_category_id']))->get();
			}

			$skillsidebar = GeneralHelper::InpersonSkillSearchBoldSidebar($data, $pricefilter);
			$jobsidebar = GeneralHelper::InpersonJobSearchBoldSidebar($data, $pricefilter);

			$_S3_PATH = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com/"; //env("IMAP_HOSTNAME_TEST", "https://skillboxs3bucket.s3.us-east-2.amazonaws.com");



			$finalresultmob = array();
			if (count($finalresult)) {
				foreach ($finalresult as $k => $v) {

					if ($v['type'] == "jobs") {

						// $existjobids[] = $v['data']['_id'];
						$f_name = "";
						$l_name = "";
						$user_image = "";
						if (isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])) {
							if (isset($user_data[$v['data']['i_user_id']]['v_fname'])) {
								$f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
							}
							if (isset($user_data[$v['data']['i_user_id']]['v_image'])) {
								$user_image = $_S3_PATH . $user_data[$v['data']['i_user_id']]['v_image'];
							}
							if (isset($user_data[$v['data']['i_user_id']]['v_lname'])) {
								$l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
							}
						}
						$v_location = "";
						if (isset($v['data']['v_city']) && $v['data']['v_city'] != "") {
							$cityData = explode(",", $v['data']['v_city']);
							if (count($cityData) && isset($cityData[0])) {
								$v_location = $cityData[0];
							}
						}

						$imgins = "";
						if (isset($v['data']['v_photos'][0]) && is_string($v['data']['v_photos'][0])) {
							$imgins = $v['data']['v_photos'][0];
						}

						$imgdata = "";
						if ($imgins != "") {
							$imgdata = $_S3_PATH . '/' . $imgins;
						} else {
							$imgdata = $_S3_PATH . '/jobpost/photos/No_Image_Available.jpg';
						}


						$e_sponsor_status = 0;
						if (isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == "start") {
							$e_sponsor_status = 1;
						}
						$is_shortlist = 0;
						if (in_array($v['data']['_id'], $s_job)) {
							$is_shortlist = 1;
						}
						$is_urgent = 0;
						if (isset($v['data']['e_urgent']) && $v['data']['e_urgent'] == "yes") {
							$is_urgent = 1;
						}
						$finalresultmob[] = array(
							'type' => "job",
							'id' => $v['data']['_id'],
							'v_user_name' => $f_name . ' ' . $l_name,
							'v_user_image' => $user_image,
							'v_location' => $v_location,
							'v_image' => $imgdata,
							'v_title' => $v['data']['v_job_title'],
							'i_user_id' => $v['data']['i_user_id'],
							// 'l_short_description' => $v['data']['l_short_description'],
							'l_short_description' => str_limit($v['data']['l_short_description'], 60, '...'),
							'i_total_avg_review' => number_format($value['data']['i_total_avg_review'], 2),
							'i_total_review' => $v['data']['i_total_review'],
							'is_shortlist' => $is_shortlist,
							'e_sponsor_status' => $e_sponsor_status,
							'v_budget_amt' => $v['data']['v_budget_amt'],
							'v_budget_type' => $v['data']['v_budget_type'],
							'is_urgent' => $is_urgent,
						);
					} else {
						// $existskillids[] = $v['data']['_id'];
						$f_name = "";
						$l_name = "";
						$user_image = "";
						if (isset($v['data']['i_user_id']) && isset($user_data[$v['data']['i_user_id']])) {
							if (isset($user_data[$v['data']['i_user_id']]['v_fname'])) {
								$f_name = $user_data[$v['data']['i_user_id']]['v_fname'];
							}
							if (isset($user_data[$v['data']['i_user_id']]['v_image'])) {
								$user_image = $_S3_PATH . $user_data[$v['data']['i_user_id']]['v_image'];
							}
							if (isset($user_data[$v['data']['i_user_id']]['v_lname'])) {
								$l_name = $user_data[$v['data']['i_user_id']]['v_lname'];
							}
						}

						$v_location = "";
						if (isset($v['data']['v_city']) && $v['data']['v_city'] != "") {
							$cityData = explode(",", $v['data']['v_city']);
							if (count($cityData) && isset($cityData[0])) {
								$v_location = $cityData[0];
							}
						}

						$is_shortlist = 0;
						if (in_array($v['data']['_id'], $s_skill)) {
							$is_shortlist = 1;
						}
						$e_sponsor_status = 0;
						if (isset($v['data']['e_sponsor_status']) && $v['data']['e_sponsor_status'] == "start") {
							$e_sponsor_status = 1;
						}

						$finalresultmob[] = array(
							'type' => "skill",
							'id' => $v['data']['_id'],
							'v_user_name' => $f_name . ' ' . $l_name,
							'v_user_image' => $user_image,
							'v_location' => $v_location,
							'v_title' => $v['data']['v_profile_title'],
							'i_user_id' => $v['data']['i_user_id'],
							// 'l_short_description' => $v['data']['l_short_description'],
							'l_short_description' => str_limit($v['data']['l_short_description'], 60, '...'),
							'i_total_avg_review' => number_format($v['data']['i_total_avg_review'], 2),
							'i_total_review' => $v['data']['i_total_review'],
							'v_starting_at' => $v['data']['information']['basic_package']['v_price'],
							'is_shortlist' => $is_shortlist,
							'e_sponsor_status' => $e_sponsor_status,
							'type' => "skill",
						);
					}
				}
			}

			$is_next_page = false;
			if ($data['e_type_data'] == "skill") {
				if (count($skillsidebar) > count($existskillids)) {
					$is_next_page = true;
				}
			}

			if ($data['e_type_data'] == "job") {
				if (count($jobsidebar) > count($existjobids)) {
					$is_next_page = true;
				}
			}

			$response_exist['existskillids'] = "";
			$response_exist['existjobids'] = "";
			if (count($existskillids)) {
				$response_exist['existskillids'] = implode(",", $existskillids);
			}
			if (count($existjobids)) {
				$response_exist['existjobids'] = implode(",", $existjobids);
			}

			$_data = array(
				'v_service' => "inperson",
				'data' => $data,
				'list' => $finalresultmob,
				'totaljob' => $totaljob,
				'totalskill' => $totalskill,
				'categories' => $categories,
				'skills' => $skills,
				'existskillids' => $response_exist['existskillids'],
				'existjobids' => $response_exist['existjobids'],
				'is_next_page' => $is_next_page,
			);

			$responseData = array(
				'code' => 200,
				'message' => "",
				'data' => $_data,
			);
			return response()->json($responseData, 200);
		}

		$responseData = array(
			'code' => 200,
			'message' => Lang::get('api.moreOption.supportList'),
			'data' => $_data,
		);
		return response()->json($responseData, 200);
	}

	public function skillDetail()
	{

		$pdata = Request::all();
		$authdata = Request::get('authdata');

		$rules = [
			'v_skill_id' => 'required',
		];

		$validator = Validator::make($pdata, $rules);
		if ($validator->fails()) {
			$responseData = array(
				'code' => 400,
				'message' => $validator->errors()->first(),
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}
		$id = $pdata['v_skill_id'];

		$sellerprofile = SellerprofileModel::find($id);

		if (!count($sellerprofile)) {

			$responseData = array(
				'code' => 400,
				'message' => "Something went wrong.",
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		if (isset($sellerprofile->i_delete) && $sellerprofile->i_delete == "1") {
			$responseData = array(
				'code' => 400,
				'message' => "Something went wrong.",
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		if (isset($sellerprofile->e_sponsor_status) && $sellerprofile->e_sponsor_status == "start") {
			$update['i_clicks'] = 1;
			if (isset($sellerprofile->i_clicks)) {
				$update['i_clicks'] = $sellerprofile->i_clicks + 1;
			}
			SellerprofileModel::find($id)->update($update);
		}

		$res['v_introducy_video'] = "";
		$res['v_work_video'] = "";
		$res['v_work_photos'] = array();

		$videoData = array();
		if (isset($sellerprofile->v_introducy_video) && $sellerprofile->v_introducy_video != "") {
			$res['v_introducy_video'] = \Storage::cloud()->url($sellerprofile->v_introducy_video);
		}

		if (isset($sellerprofile->v_work_video) && count($sellerprofile->v_work_video)) {
			foreach ($sellerprofile->v_work_video as $k => $v) {
				$res['v_work_video'] = \Storage::cloud()->url($v);
			}
		}

		$imagData = array();
		if (isset($sellerprofile->v_work_photos) && count($sellerprofile->v_work_photos)) {
			foreach ($sellerprofile->v_work_photos as $k => $v) {
				$imagData[] = \Storage::cloud()->url($v);
			}
			$res['v_work_photos'] = $imagData;
		}

		$res['v_service'] = $sellerprofile->v_service;
		$res['id'] = $sellerprofile->id;
		$res['l_short_description'] = $sellerprofile->l_short_description;
		$res['l_brief_description'] = $sellerprofile->l_brief_description;
		$res['starting_at'] = isset($sellerprofile->information['basic_package']['v_price']) ? $sellerprofile->information['basic_package']['v_price'] : '';
		$res['v_hourly_rate'] = $sellerprofile->v_hourly_rate;

		$v_image = "";
		if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_image)) {
			$v_image = \Storage::cloud()->url($sellerprofile->hasUser()->v_image);
		}
		$res['v_user_image'] = $v_image;

		$useridseller = "";
		if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->id)) {
			$useridseller = $sellerprofile->hasUser()->id;
		}


		$v_fname = "";
		if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_fname)) {
			$v_fname = $sellerprofile->hasUser()->v_fname;
		}
		$res['v_user_fname'] = $v_fname;

		$res['i_user_id'] = '';
		if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->id)) {
			$res['i_user_id'] = $sellerprofile->hasUser()->id;
		}
		$v_lname = "";
		if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_lname)) {
			$v_lname = $sellerprofile->hasUser()->v_lname;
		}
		$res['v_user_lname'] = $v_lname;

		$v_name_main_skill = "";
		if (count($sellerprofile->hasSkill()) && isset($sellerprofile->hasSkill()->v_name)) {
			$v_name_main_skill = $sellerprofile->hasSkill()->v_name;
		}
		$res['v_name_main_skill'] = $v_name_main_skill;

		$v_name_other_skill = "";
		if (count($sellerprofile->hasOtherSkill()) && isset($sellerprofile->hasOtherSkill()->v_name)) {
			$v_name_other_skill = $sellerprofile->hasOtherSkill()->v_name;
		}
		$res['v_name_other_skill'] = $v_name_other_skill;


		$other_skill_ids = array();
		if (isset($sellerprofile) && count($sellerprofile)) {
			if (isset($sellerprofile->i_otherskill_id) && count($sellerprofile->i_otherskill_id)) {
				foreach ($sellerprofile->i_otherskill_id as $key => $value) {
					$other_skill_ids[] = $value;
				}
			}
		}

		$other_skills_name = SkillsModel::whereIn("_id", $other_skill_ids)->get();
		if (isset($other_skills_name) && count($other_skills_name)) {
			foreach ($other_skills_name as $key => $value) {
				$v_name[] = $value->v_name;
			}
		}
		$v_name = implode(', ', $v_name);
		$res['other_skills_name'] = $v_name;
		$res['v_english_proficiency'] = isset($sellerprofile->v_english_proficiency) ? $sellerprofile->v_english_proficiency : '';


		$v_verified = "";
		$i_verified = 0;

		if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->e_email_confirm)) {
			$v_verified = ($sellerprofile->hasUser()->e_email_confirm  == 'yes') ? 'verified' : 'not verified';

			if ($sellerprofile->hasUser()->e_email_confirm  == 'yes') {
				$i_verified = 1;
			}
		}
		$res['v_user_verified'] = $i_verified;


		$availabel = "";
		if (isset($sellerprofile->v_avalibility_from) && $sellerprofile->v_avalibility_from != "") {
			$availabel = ucfirst(substr($sellerprofile->v_avalibility_from, 0, 3)) . ' - ';
		}
		if (isset($sellerprofile->v_avalibility_to) && $sellerprofile->v_avalibility_to != "") {
			$availabel .= ucfirst(substr($sellerprofile->v_avalibility_to, 0, 3)) . ' / ';
		}

		if (isset($sellerprofile->v_avalibilitytime_from) && $sellerprofile->v_avalibilitytime_from != "") {
			$availabel .= date("h a", strtotime($sellerprofile->v_avalibilitytime_from . ':00')) . ' - ';
		}

		if (isset($sellerprofile->v_avalibilitytime_to) && $sellerprofile->v_avalibilitytime_to != "") {
			$availabel .= date("h a", strtotime($sellerprofile->v_avalibilitytime_to . ':00'));
		}

		$v_replies_time = "";
		if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_replies_time)) {
			$v_replies_time = $sellerprofile->hasUser()->v_replies_time;
		}
		$res['v_replies_time'] = $v_replies_time;

		$v_plan = "";
		if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_plan)) {
			$v_plan = $sellerprofile->hasUser()->v_plan;
		}

		$res['availabel'] = $availabel;
		$res['city'] = "";

		if (isset($sellerprofile->v_service) && $sellerprofile->v_service == "inperson" && isset($sellerprofile->v_city) && $sellerprofile->v_city != "") {
			$res['city'] = $sellerprofile->v_city;
		}
		$start = strtotime('2019-01-01 00:00:00');
		if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->d_added)) {
			$start = strtotime($sellerprofile->hasUser()->d_added);
		}
		$end    = time();
		$diff   = $end - $start;
		// DR 2020-01-27
		$years  = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		if($years > 0) {
			$days_between = $years.' Years';
		}else if($years < 0 && $months > 0) {
			$days_between = $months.' Months';
		}else {
			$days_between = $days.' Days';
		}
		// DR 2020-01-27
		// $days_between = round($diff / 86400);
		// if ($days_between > 30) {
		// 	$days_between = round($days_between / 30);
		// 	// DR 2019-11-28
		// 	$days_between = "(".$days_between . " Month)";
		// 	// DR 2019-11-28
		// } else {
		// 	if ($days_between > 1) {
		// 		$days_between = $days_between . " day(s)";
		// 	} else {
		// 		$days_between = "1 day";
		// 	}
		// }
		$res['with_skillbox'] = $days_between;
		$res['v_website'] = $sellerprofile->v_website;
		$res['v_contact_phone'] = $sellerprofile->v_contact_phone;

		$package = array();
		// $sellerprofile->information->toArray();

		if (isset($sellerprofile->information) && count($sellerprofile->information)) {
			foreach ($sellerprofile->information as $k => $v) {
				$v['v_key'] = $k;
				if ($k == 'basic_package' && $v['v_title'] != '') {
					$v['v_title'] = 'Basic Package';
				} else if ($k == 'standard_package' && $v['v_title'] != '') {
					$v['v_title'] = 'Standard Package';
				} else if ($k == 'premium_package' && $v['v_title'] != '') {
					$v['v_title'] = 'Premium Package';
				}
				$package[] = $v;
			}
		}


		$res['package'] = $package;

		$title="";
		if(isset($sellerprofile->v_profile_title)){
			$title =GeneralHelper::TitleSlug($sellerprofile->v_profile_title);
		}
		$res['v_share_url']=url('online/'.$sellerprofile->id.'/'.$title.'?id='.$sellerprofile->id);

		$userid = $authdata->id;
		$shortlistedSkill = ShortlistedSkillsModel::where('i_shortlist_id', $id)->where('i_user_id', $userid)->first();

		$res['is_shortlisted'] = 0;
		if (count($shortlistedSkill) && !empty($shortlistedSkill)) {
			$res['is_shortlisted'] = 1;
		}

		$leveldata = array();
		if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_level)) {
			if ($sellerprofile->hasUser()->v_level != "0") {
				$leveldata = GeneralHelper::LevelData($sellerprofile->hasUser()->v_level);
			}
		}
		$res['leveldata'] = $leveldata;

		$comission = GeneralHelper::getSiteSetting('SITE_COMMISSION');
		if ($comission == "") {
			$comission = 10;
		}
		$processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE');
		if ($processing == "") {
			$processing = 0.60;
		}

		// cart >>
		$res['comission'] = $comission;
		$res['processing'] = $processing;

		$v_image = "";
		if (isset($sellerprofile->v_work_photos[0]) && $sellerprofile->v_work_photos[0] != "") {
			$v_image = Storage::cloud()->url($sellerprofile->v_work_photos[0]);
		} else {

			if (count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_image) && $sellerprofile->hasUser()->v_image != "") {
				$v_image = Storage::cloud()->url($sellerprofile->hasUser()->v_image);
			} else {
				$v_image = Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
			}
		}
		$res['cartImage'] = $v_image;
		// cart <<

		$resdata['detail'] = $res;

		$responseData = array(
			'code' => 200,
			'message' => "",
			'data' => $resdata,
		);
		return response()->json($responseData, 200);
	}

	public function SkillReview()
	{

		$pdata = Request::all();
		$authdata = Request::get('authdata');

		$dataLimit = 20;
		$currentPage = 1;

		$rules = [
			'v_skill_id' => 'required',
		];

		$validator = Validator::make($pdata, $rules);
		if ($validator->fails()) {
			$responseData = array(
				'code' => 400,
				'message' => $validator->errors()->first(),
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}
		$id =  $pdata['v_skill_id'];

		$sellerprofile = SellerprofileModel::find($id);
		if (!count($sellerprofile)) {
			$responseData = array(
				'code' => 400,
				'message' => "Something went wrong.",
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		$query = SellerreviewModal::query();
		$query = $query->where('i_seller_profile_id', $sellerprofile->id);

		$reviewdata = array();
		if (isset($pdata['pagination']) && $pdata['pagination'] == 1) {
			$reviewdata = $query->paginate($dataLimit);
		} else {
			$reviewdata = $query->get();
		}
		//$reviewdata = SellerreviewModal::where('i_seller_profile_id',$sellerprofile->id)->get();

		$reviewdataList = array();

		if (isset($reviewdata) && count($reviewdata)) {
			foreach ($reviewdata as $k => $v) {

				$v_image = "";
				if (count($v->hasUser()) && isset($v->hasUser()->v_image)) {
					$v_image = $v->hasUser()->v_image;
				}
				$v_fname = "";
				if (count($v->hasUser()) && isset($v->hasUser()->v_fname)) {
					$v_fname = $v->hasUser()->v_fname;
				}

				$v_lname = "";
				if (count($v->hasUser()) && isset($v->hasUser()->v_lname)) {
					$v_lname = $v->hasUser()->v_lname;
				}

				$imgdata = '';
				if (Storage::disk('s3')->exists($v_image)) {
					$imgdata = \Storage::cloud()->url($v_image);
				} else {
					$imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
				}

				$totalavg = $v->i_communication_star + $v->i_described_star + $v->i_recommend_star;
				$totalavg = $totalavg / 3;

				$vs_image = "";
				if (count($v->hasSeller()) && isset($v->hasSeller()->v_image)) {
					$vs_image = $v->hasSeller()->v_image;
				}
				$vs_fname = "";
				if (count($v->hasSeller()) && isset($v->hasSeller()->v_fname)) {
					$vs_fname = $v->hasSeller()->v_fname;
				}

				$vs_lname = "";
				if (count($v->hasSeller()) && isset($v->hasSeller()->v_lname)) {
					$vs_lname = $v->hasSeller()->v_lname;
				}

				$imgdatas = '';
				if (Storage::disk('s3')->exists($vs_image)) {
					$imgdatas = \Storage::cloud()->url($vs_image);
				} else {
					$imgdatas = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
				}
				$seller_comment = "";
				if (isset($v->l_answers) && count($v->l_answers)) {
					$seller_comment = $v->l_answers['l_comment'];
				}

				$reviewdataList[] = array(
					'v_name' => $v_fname . ' ' . $v_lname,
					'v_image' => $imgdata,
					'totalavg' => number_format($totalavg, 1),
					'l_comment' => $v->l_comment,
					'v_seller_name' => $vs_fname . ' ' . $vs_lname,
					'v_seller_image' => $imgdatas,
					'v_seller_comment' => $seller_comment,
				);
			}
		}


		$resdata = GeneralHelper::apiPagination($reviewdata, $pdata);
		$resdata['list'] = $reviewdataList;
		// $_data=array(
		// 	'list'=> $reviewdataList,
		// );
		$responseData = array(
			'code' => 200,
			'message' => "",
			'data' => $resdata,
		);
		return response()->json($responseData, 200);
	}

	public function ContactSelller()
	{

		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$rules = [
			'i_profile_id' => 'required',
			'v_subject_title' => 'required',
			'l_message' => 'required',
		];

		$validator = Validator::make($post_data, $rules);
		if ($validator->fails()) {
			$responseData = array(
				'code' => 400,
				'message' => $validator->errors()->first(),
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		
		$sellerdata = SellerprofileModel::where('_id', $post_data['i_profile_id'])->where('e_sponsor', "yes")->where('e_sponsor_status', "start")->first();
		
		if (count($sellerdata)) {
			$updatedata['i_contact'] = $sellerdata->i_contact + 1;
			SellerprofileModel::find($sellerdata->id)->update($updatedata);
		}
		
		// print_r($sellerdata); exit;		
		$sellerdata = SellerprofileModel::where('_id', $post_data['i_profile_id'])->first();
		$data['i_parent_id'] = 0;
		$data['v_subject_title'] = $post_data['v_subject_title'];
		$data['i_to_id'] = $sellerdata->i_user_id;
		$data['i_from_id'] = $userid;
		$data['l_message'] = $post_data['l_message'];
		$data['e_view'] = "unread";
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		$mid = MessagesModel::create($data)->id;
		self::sendEmailNotificationSellerForMessage($data['i_to_id'], $post_data);
		
		$sellerUserData = UsersModel::find($sellerdata->i_user_id);
		if (count($sellerUserData)) {
			if (isset($sellerUserData->l_seller_auto_message) && $sellerUserData->l_seller_auto_message != "") {

				$ctime = date("Y-m-d H:i:s");
				$ltime = $data['d_added'];
				$totaldiff = strtotime($ctime) - strtotime($ltime);
				$totaldiff = $totaldiff;
				$insertreplay = array();
				$insertreplay['i_parent_id'] = $mid;
				$insertreplay['v_subject_title'] = "";
				$insertreplay['i_to_id'] = $userid;
				$insertreplay['i_from_id'] = $sellerdata->i_user_id;
				$insertreplay['l_message'] = $sellerUserData->l_seller_auto_message;
				$insertreplay['v_replay_time'] = $totaldiff;
				$insertreplay['e_view'] = "unread";
				$insertreplay['d_added'] = date("Y-m-d H:i:s");
				$insertreplay['d_modified'] = date("Y-m-d H:i:s");
				$mid = MessagesModel::create($insertreplay)->id;
				self::sendEmailNotificationBuyerForMessage($sellerdata->i_user_id, $userid, $sellerUserData->l_seller_auto_message);
			}
		}

		$finalResponse = array();
		$responseData = array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.messageSent'),
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);
	}

	public function sendEmailNotificationSellerForMessage($id = "", $data = "")
	{

		if ($id == "") {
			return 0;
		}

		$userdata = UsersModel::find($id);
		if (!count($userdata)) {
			return 0;
		}

		$sellername = "";
		if (isset($userdata->v_fname)) {
			$sellername = $userdata->v_fname;
		}
		if (isset($userdata->v_lname)) {
			$sellername .= ' ' . $userdata->v_lname;
		}

		$selleremail = "";
		if (isset($userdata->v_email)) {
			$selleremail = $userdata->v_email;
		}

		$buyername = "";
		if (isset(auth()->guard('web')->user()->v_fname)) {
			$buyername = auth()->guard('web')->user()->v_fname;
		}
		if (isset(auth()->guard('web')->user()->v_lname)) {
			$buyername .= ' ' . auth()->guard('web')->user()->v_fname;
		}

		$messagestr = "";
		if (isset($data['v_subject_title']) && $data['v_subject_title'] != "") {
			$messagestr  .= "<b>Subject </b>: " . $data['v_subject_title'] . "<br>";
		}
		if (isset($data['l_message']) && $data['l_message'] != "") {
			$messagestr  .= "<b>Message </b>: " . $data['l_message'];
		}

		$data = array(
			'USER_FULL_NAME' => $sellername,
			'BUYER_NAME' => $buyername,
			'MESSAGE' => $messagestr,
		);
		$mailcontent = EmailtemplateHelper::sendMessageSeller($data);
		$subject = "Message";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5bb5ba4276fbae17da5ac234");

		$mailids = array(
			$sellername => $selleremail,
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject, $mailcontent, $mailids);
	}

	public function sendEmailNotificationBuyerForMessage($id = "", $buyerid = "", $msg = "")
	{

		if ($id == "") {
			return 0;
		}
		$userdata = UsersModel::find($id);
		if (!count($userdata)) {
			return 0;
		}
		$sellername = "";
		if (isset($userdata->v_fname)) {
			$sellername = $userdata->v_fname;
		}
		if (isset($userdata->v_lname)) {
			$sellername .= ' ' . $userdata->v_lname;
		}

		$userdataBuyer = UsersModel::find($buyerid);
		if (!count($userdata)) {
			return 0;
		}
		$buyername = "";
		if (isset($userdataBuyer->v_fname)) {
			$buyername = $userdataBuyer->v_fname;
		}
		if (isset($userdataBuyer->v_lname)) {
			$buyername .= ' ' . $userdataBuyer->v_lname;
		}

		$buyeremail = "";
		if (isset($userdataBuyer->v_email)) {
			$buyeremail = $userdataBuyer->v_email;
		}
		$messagestr = "<b>Message </b>: " . $msg;

		$data = array(
			'USER_FULL_NAME' => $buyername,
			'SELLER_NAME' => $sellername,
			'MESSAGE' => $messagestr,
		);
		$mailcontent = EmailtemplateHelper::sendMessageBuyer($data);
		$subject = "Message";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5bb5bdfd76fbae2bb2317f43");
		$mailids = array(
			$buyername => $buyeremail,
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject, $mailcontent, $mailids);
	}

	public function skillShortlist()
	{


		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$rules = [
			'i_skill_id' => 'required',
		];

		$validator = Validator::make($post_data, $rules);
		if ($validator->fails()) {
			$responseData = array(
				'code' => 400,
				'message' => $validator->errors()->first(),
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		$sData = ShortlistedSkillsModel::where('i_shortlist_id', $post_data['i_skill_id'])->where('i_user_id', $userid)->first();


		$is_shortlisted = 0;
		$msg = "";

		if (count($sData)) {

			ShortlistedSkillsModel::where('i_shortlist_id', $post_data['i_skill_id'])->where('i_user_id', $userid)->delete();
			$msg = Lang::get('api.jobs.removeToShortList');
		} else {

			$data['i_user_id'] = $userid;
			$data['d_added'] = date("Y-m-d H:i:s");
			$data['i_shortlist_id'] = $post_data['i_skill_id'];
			$a = ShortlistedSkillsModel::create($data);

			$is_shortlisted = 1;
			$msg = Lang::get('api.jobs.addToShortList');
		}

		$finalResponse = array(
			'is_shortlisted' => $is_shortlisted,
		);
		$responseData = array(
			'code'    => 200,
			'message' => $msg,
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);
	}

	public function courseDetail()
	{

		$pdata = Request::all();
		$authdata = Request::get('authdata');

		$rules = [
			'v_course_id' => 'required',
		];

		$validator = Validator::make($pdata, $rules);
		if ($validator->fails()) {
			$responseData = array(
				'code' => 400,
				'message' => $validator->errors()->first(),
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		$_S3_PATH = "https://skillboxs3bucket.s3.us-east-2.amazonaws.com"; //env("IMAP_HOSTNAME_TEST", "https://skillboxs3bucket.s3.us-east-2.amazonaws.com");

		$id = $pdata['v_course_id'];
		$courses = CoursesModel::find($id);
		
		if(!count($courses)) {
			$responseData = array(
				'code' => 400,
				'message' => "Something went wrong.",
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}


		$query = BuyerCourseModel::query();
		$bcdata = $query->where('i_user_id',$authdata->id)->where('i_course_id',$id)->where('e_payment_status','success')->first();

		$resdata['detail']['is_purchase'] = false;
		if(count($bcdata)){
			$resdata['detail']['is_purchase'] = true;
		}
		
	
		$imgdata = \Storage::cloud()->url($courses->v_instructor_image);
		$videodata = \Storage::cloud()->url($courses->l_video);

		$resdata['detail']['id'] = $courses->id;
		$resdata['detail']['v_video'] = $videodata;
		$resdata['detail']['v_instructor_image'] = $imgdata;

		$imgins="";
		if(isset($courses->v_course_thumbnail) && $courses->v_course_thumbnail!=""){
			$imgins = $courses->v_course_thumbnail;
		}
		$imgdata=$_S3_PATH.'/'.$imgins;
		
		$resdata['detail']['v_share_url'] = url('course/'.$courses->id).'/'.GeneralHelper::TitleSlug($courses->v_title);
		$resdata['detail']['v_course_thumbnail'] = $imgdata;
		$resdata['detail']['v_title'] = $courses->v_title;
		$resdata['detail']['v_sub_title'] = $courses->v_sub_title;
		$resdata['detail']['v_author_name'] = $courses->v_author_name;
		$resdata['detail']['v_requirement'] = $courses->v_requirement;
		$resdata['detail']['l_description'] = $courses->l_description;
		$resdata['detail']['l_instructor_details'] = $courses->l_instructor_details;
		$resdata['detail']['f_price'] = $courses->f_price;
		
		$totallacture=0;
		if(isset($courses->section) && count($courses->section)){
			foreach ($courses->section as $key => $value) {
				$totallacture= $totallacture+count($value['video']);   
			}
		}
		$resdata['detail']['totallacture'] = $totallacture;
		$resdata['detail']['i_duration'] = $courses->i_duration;

		$resdata['detail']['v_level_title']="";
		if(count( $courses->hasLevel() ) ){
			$resdata['detail']['v_level_title'] = $courses->hasLevel()->v_title;
		} 
		
		$resdata['detail']['i_language_id'] = $courses->i_language_id;
		$resdata['detail']['i_level_id'] = $courses->i_level_id;
		$resdata['detail']['v_title'] = $courses->v_title;

		$section=array();
        if(isset($courses->section)){
            $section = $courses->section;
		}
		
		$resdata['baseUrl'] = $_S3_PATH.'/';
		$resdata['section'] = $section;

		$comission = GeneralHelper::getSiteSetting('SITE_COMMISSION');
		if ($comission == "") {
			$comission = 10;
		}
		$processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE');
		if ($processing == "") {
			$processing = 0.60;
		}
		// cart >>
		$resdata['detail']['comission'] = $comission;
		$resdata['detail']['processing'] = $processing;

	
		// $resdata['detail'] = $courses->toArray();
		$responseData = array(
			'code' => 200,
			'message' => "",
			'data' => $resdata,
		);
		return response()->json($responseData, 200);
	}

	public function CourseReview()
	{

		$pdata = Request::all();
		$authdata = Request::get('authdata');
		
		$dataLimit = 20;
		$currentPage = 1;

		$rules = [
			'v_course_id' => 'required',
		];

		$validator = Validator::make($pdata, $rules);
		if ($validator->fails()) {
			$responseData = array(
				'code' => 400,
				'message' => $validator->errors()->first(),
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}
		$id =  $pdata['v_course_id'];

		$courses = CoursesModel::find($id);
		if(!count($courses)) {
			$responseData = array(
				'code' => 400,
				'message' => "Something went wrong.",
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		$query = CoursesReviews::query();
		$query = $query->where('i_courses_id', $id);
		$reviewdata = array();
		if (isset($pdata['pagination']) && $pdata['pagination'] == 1) {
			$reviewdata = $query->paginate($dataLimit);
		} else {
			$reviewdata = $query->get();
		}
		
		$reviewdataList = array();
		if(isset($reviewdata) && count($reviewdata)) {
			foreach ($reviewdata as $k => $v) {
				
				$v_image = "";
				if (count($v->hasUser()) && isset($v->hasUser()->v_image)) {
					$v_image = $v->hasUser()->v_image;
				}
				$v_fname = "";
				if (count($v->hasUser()) && isset($v->hasUser()->v_fname)) {
					$v_fname = $v->hasUser()->v_fname;
				}

				$v_lname = "";
				if (count($v->hasUser()) && isset($v->hasUser()->v_lname)) {
					$v_lname = $v->hasUser()->v_lname;
				}
				
				$imgdata = '';
				if (Storage::disk('s3')->exists($v_image)) {
					$imgdata = \Storage::cloud()->url($v_image);
				} else {
					$imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
				}

				$totalavg = $v->f_rate_instruction + $v->f_rate_navigate + $v->f_rate_reccommend;
				$totalavg = $totalavg / 3;

				$vs_image = "";
				if (count($v->hasSeller()) && isset($v->hasSeller()->v_image)) {
					$vs_image = $v->hasSeller()->v_image;
				}
				$vs_fname = "";
				if (count($v->hasSeller()) && isset($v->hasSeller()->v_fname)) {
					$vs_fname = $v->hasSeller()->v_fname;
				}

				$vs_lname = "";
				if (count($v->hasSeller()) && isset($v->hasSeller()->v_lname)) {
					$vs_lname = $v->hasSeller()->v_lname;
				}

				$imgdatas = '';
				if (Storage::disk('s3')->exists($vs_image)) {
					$imgdatas = \Storage::cloud()->url($vs_image);
				} else {
					$imgdatas = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
				}

				$seller_comment = "";
				if (isset($v->l_answers) && count($v->l_answers)) {
					$seller_comment = $v->l_answers['l_comment'];
				}

				$reviewdataList[] = array(
					'v_name' => $v_fname . ' ' . $v_lname,
					'v_image' => $imgdata,
					'totalavg' => number_format($totalavg, 1),
					'l_comment' => $v->l_comment,
					'v_seller_name' => $vs_fname . ' ' . $vs_lname,
					'v_seller_image' => $imgdatas,
					'v_seller_comment' => $seller_comment,
				);
			}
		}


		$resdata = GeneralHelper::apiPagination($reviewdata, $pdata);
		$resdata['list'] = $reviewdataList;
		// $_data=array(
		// 	'list'=> $reviewdataList,
		// );
		$responseData = array(
			'code' => 200,
			'message' => "",
			'data' => $resdata,
		);
		return response()->json($responseData, 200);
	}

	public function coursesShortlist(){

		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$rules = [
			'i_course_id' => 'required',
		];

		$validator = Validator::make($post_data, $rules);
		if ($validator->fails()) {
			$responseData = array(
				'code' => 400,
				'message' => $validator->errors()->first(),
				'data' => array(),
			);
			return response()->json($responseData, 400);
		}

		$sData = ShortlistedCourseModel::where('i_shortlist_id', $post_data['i_course_id'])->where('i_user_id', $userid)->first();
	
		$is_shortlisted = 0;
		$msg = "";
		
		if(count($sData)){
			
			ShortlistedCourseModel::where('i_shortlist_id', $post_data['i_course_id'])->where('i_user_id', $userid)->delete();
			$msg = "Successfully removed from shortlisted course.";

		}else{

			$data['i_user_id'] = $userid;
			$data['d_added'] = date("Y-m-d H:i:s");
			$data['i_shortlist_id'] = $post_data['i_course_id'];
			$a = ShortlistedCourseModel::create($data);
			
			$is_shortlisted = 1;
			$msg = "Course successfully shortlisted";
			
		}

		$finalResponse = array(
			'is_shortlisted' => $is_shortlisted,
		);
		$responseData = array(
			'code'    => 200,
			'message' => $msg,
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);
		
	}

}
