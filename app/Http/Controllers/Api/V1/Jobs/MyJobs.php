<?php
namespace App\Http\Controllers\Api\V1\Jobs;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Buyerreview as BuyerreviewModal;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Users\Messages as MessagesModel;

class MyJobs extends Controller {

	
	public function index(){


		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$joblist = JobsModel::where('i_user_id',$userid)->where('v_service',"online")->get();
		
		$headerdata['ACTIVE_POSTINGS']=0;
		$headerdata['EXPIRED_POSTINGS']=0;
		$headerdata['SPONSORED_ADS']=0;
				
		$onlineJob=array();
		$currentdate=date("Y-m-d");
		if(count($joblist)){
			foreach ($joblist as $key => $value) {
			
				if(strtotime($value->d_expiry_date) < strtotime($currentdate)){
					if($value->e_status=="active"){
					$headerdata['EXPIRED_POSTINGS']=$headerdata['EXPIRED_POSTINGS']+1;;
					}
				}else{
					if($value->e_status=="active"){
						$headerdata['ACTIVE_POSTINGS']=$headerdata['ACTIVE_POSTINGS']+1;;
					}
				}	

				if(isset($value->e_sponsor) && $value->e_sponsor=="yes"){
					$headerdata['SPONSORED_ADS']=$headerdata['SPONSORED_ADS']+1;;
				}
				
				$e_job_end_status=0;
				if(strtotime($value->d_expiry_date)<=strtotime($currentdate)){
					$e_job_end_status=1;
				}
				$onlineJob[]=array(
					'id'=>$value->id,
					'v_job_title'=>$value->v_job_title,
					'd_expiry_date'=>$value->d_expiry_date,
					'e_job_end_status'=>$e_job_end_status,
					'd_added'=>$value->d_added,

				);
			}
		}

		$finalResponse['online']=array(
			'headerdata'=>$headerdata,
			'job_list'=>$onlineJob,
		);
				
		$joblist = JobsModel::where('i_user_id',$userid)->where('v_service',"inperson")->get();
		
		$headerdata['ACTIVE_POSTINGS']=0;
		$headerdata['EXPIRED_POSTINGS']=0;
		$headerdata['SPONSORED_ADS']=0;
		
		$inpersonJob=array();
		$currentdate=date("Y-m-d");
		if(count($joblist)){
			foreach ($joblist as $key => $value) {
			
				if(strtotime($value->d_expiry_date) < strtotime($currentdate)){
					if($value->e_status=="active"){
					$headerdata['EXPIRED_POSTINGS']=$headerdata['EXPIRED_POSTINGS']+1;;
					}
				}else{
					if($value->e_status=="active"){
						$headerdata['ACTIVE_POSTINGS']=$headerdata['ACTIVE_POSTINGS']+1;;
					}
				}	
				
				if(isset($value->e_sponsor) && $value->e_sponsor=="yes"){
					$headerdata['SPONSORED_ADS']=$headerdata['SPONSORED_ADS']+1;;
				}
				$e_job_end_status=0;
				if(strtotime($value->d_expiry_date)<=strtotime($currentdate)){
					$e_job_end_status=1;
				}
				$inpersonJob[]=array(
					'id'=>$value->id,
					'v_job_title'=>$value->v_job_title,
					'd_expiry_date'=>$value->d_expiry_date,
					'e_job_end_status'=>$e_job_end_status,
					'd_added'=>$value->d_added,

				);
			}
		}
		
		$finalResponse['inperson']=array(
			'headerdata'=>$headerdata,
			'job_list'=>$inpersonJob,
		);
		
		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.myJobsList'),
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);

		
	}

	public function detail(){

		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$rules = [
			'i_job_id' => 'required',
	  	];
		
		$validator = Validator::make($post_data, $rules);
    	if($validator->fails()) {
				$responseData=array(
						'code'=>400,
						'message'=>$validator->errors()->first(),
						'data'=>array(),
				);
				return response()->json($responseData, 400);
		}
		
		$jobs = JobsModel::find($post_data['i_job_id']);

		if(!count($jobs)){
			
			$responseData=array(
					'code'=>400,
					'message'=>"Something went wrong.Please try again after sometime.",
					'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$buyerreview = BuyerreviewModal::where('i_buyer_id',$jobs->i_user_id)->get();
		
		$totalavg=0;
		if(count($buyerreview)){
			foreach ($buyerreview as $key => $value) {
				$totalavg = $totalavg + ($value['i_work_star']+$value['i_support_star']+$value['i_opportunities_star']+$value['i_work_again_star'])/4;
			}
			$totalavg  = $totalavg/count($buyerreview);
		}

		$shortlistedJobs = ShortlistedJobsModel::where('i_shortlist_id',$jobs->id)->where('i_user_id',$userid)->first();
		$is_shortlisted = 0;
		if(count($shortlistedJobs) && !empty($shortlistedJobs)){
			$is_shortlisted = 1;
		}
		
		$is_applied = 0;
		$appliedJob = AppliedJobModel::where('i_applied_id',$jobs->id)->where('i_user_id',$userid)->first();
		if(count($appliedJob) && !empty($appliedJob)){
			$is_applied = 1;
		}
		
		$finalResponse['requirements']['id']=$jobs->id;
		$finalResponse['requirements']['v_job_title']=$jobs->v_job_title;
		$finalResponse['requirements']['v_budget_type']=$jobs->v_budget_type;
		$finalResponse['requirements']['v_budget_amt']=$jobs->v_budget_amt;
		$finalResponse['requirements']['v_city']=$jobs->v_city;
		$finalResponse['requirements']['is_shortlisted']=$is_shortlisted;
		$finalResponse['requirements']['is_applied']=$is_applied;
		$finalResponse['requirements']['job_posted_id'] = "";
    	if(count($jobs->hasCategory()) && isset($jobs->hasCategory()->v_name)){
			$finalResponse['requirements']['job_posted_id']=$jobs->hasCategory()->v_name;
		}
		
    	$finalResponse['requirements']['main_skill'] = "";
    	if(count($jobs->hasSkill()) && isset($jobs->hasSkill()->v_name)){
			$finalResponse['requirements']['main_skill'] = $jobs->hasSkill()->v_name;
    	}
		$finalResponse['requirements']['skill_level'] = $jobs->v_skill_level_looking;
		$finalResponse['requirements']['v_contact'] = $jobs->v_contact;


		$finalResponse['detail']['id']=$jobs->id;
		$finalResponse['detail']['l_job_description'] = $jobs->l_job_description;

		$job_photos=array();

		if(count($jobs->v_photos)){
			foreach($jobs->v_photos as $k=>$v){
				$job_photos[]=\Storage::cloud()->url($v);                
			}
		}
		$finalResponse['detail']['v_photos'] = $job_photos;

		$reviewData=array();
		if(isset($buyerreview) && count($buyerreview)){
				foreach($buyerreview as $k=>$v){

					$v_image="";
					$v_fromimage="";
					if(count($v->hasUser()) && isset($v->hasUser()->v_image)){
							$v_image=$v->hasUser()->v_image;
							 $v_fromimage = \Storage::cloud()->url($v_image);
					}

					$v_fname="";
					if(count($v->hasUser()) && isset($v->hasUser()->v_fname)){
							$v_fname=$v->hasUser()->v_fname;
					}

					$v_lname="";
					if(count($v->hasUser()) && isset($v->hasUser()->v_lname)){
							$v_lname=$v->hasUser()->v_lname;
					}

					$totalavg=0;
					$totalavg = ($v['i_work_star']+$v['i_support_star']+$v['i_opportunities_star']+$v['i_work_again_star'])/4;
					
					$reviewData[]=array(
							'v_image'=>$v_fromimage,
							'v_name'=>$v_fname.' '.$v_lname,
							'l_comment'=>$v->l_comment,
							'totalavg'=>number_format($totalavg,1),
					);
				}
		}
		$finalResponse['review']['id']=$jobs->id;
		$finalResponse['review']['data'] = $reviewData;
		
		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.myJobsList'),
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);


	}

	public function endJob(){

		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;
		
		$rules = [
			'i_job_id' => 'required',
	  	];
		
		$validator = Validator::make($post_data, $rules);
    	if($validator->fails()) {
			$responseData=array(
					'code'=>400,
					'message'=>$validator->errors()->first(),
					'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		$jobs = JobsModel::find($post_data['i_job_id']);

		if(count($jobs)){
			$jobs->e_status="inactive";
			$jobs->d_expiry_date=date("Y-m-d");
			

			$jobs->save();
		}
		$finalResponse=array();

		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.endjob'),
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);
	
	}
	
	public function addToShortList(){

		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;
		
		$rules = [
			'i_job_id' => 'required',
		];
		
		$validator = Validator::make($post_data, $rules);
    	if($validator->fails()) {
				$responseData=array(
						'code'=>400,
						'message'=>$validator->errors()->first(),
						'data'=>array(),
				);
				return response()->json($responseData, 400);
		}
		
		$sData = ShortlistedJobsModel::where('i_shortlist_id',$post_data['i_job_id'])->where('i_user_id',$userid)->get();
		$is_shortlisted=0;

		$msg="";
		if(count($sData)){
				//$sData->delete();
				ShortlistedJobsModel::where('i_shortlist_id',$post_data['i_job_id'])->where('i_user_id',$userid)->delete();
				$msg=Lang::get('api.jobs.removeToShortList');
		}else{
			$data['i_user_id']     = $userid;	
			$data['d_added']       = date("Y-m-d H:i:s");	
			$data['i_shortlist_id']= $post_data['i_job_id'];	
			ShortlistedJobsModel::create($data);
			$is_shortlisted=1;
			$msg=Lang::get('api.jobs.addToShortList');
		}

		$finalResponse=array(
			'is_shortlisted'=>$is_shortlisted,
		);
		$responseData=array(
			'code'    => 200,
			'message' => $msg,
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);
	
	}

	public function applyJob(){

		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;
		
		$rules = [
			'i_job_id' => 'required',
		];
		
		$validator = Validator::make($post_data, $rules);
    	if($validator->fails()) {
				$responseData=array(
						'code'=>400,
						'message'=>$validator->errors()->first(),
						'data'=>array(),
				);
				return response()->json($responseData, 400);
		}

		$jobdata = JobsModel::find($post_data['i_job_id']);
		if(!count($jobdata)){
				$responseData=array(
					'code'=>400,
					'message'=>$validator->errors()->first(),
					'data'=>array(),
				);
				return response()->json($responseData, 400);
		}
		$service = $jobdata->v_service;
		$userprofile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$service)->first();
		
		if(!count($userprofile)){
				
				$responseData=array(
					'code'=>400,
					'message'=>"profile doesn't exist. Please go to your user panel and create an ".ucfirst($service)." profile to apply for this job.",
					'data'=>array(),
				);
				return response()->json($responseData, 400);
		}


		$insert['i_user_id']     = $userid;	
		$insert['i_seler_profile_id']  = $userprofile->id;	
		$insert['d_added']       = date("Y-m-d H:i:s");	
		$insert['i_applied_id']  = $post_data['i_job_id'];
		$insert['i_buyer_id']  	 = $jobdata->i_user_id;
		$insert['v_service']  	 = $jobdata->v_service;
		AppliedJobModel::create($insert);

		if(isset($jobdata->v_service) && $jobdata->v_service=="online"){
			$updateuser['v_seller_online_service']="active";
			UsersModel::find($userid)->update($updateuser);
		}else{
			$updateuser['v_seller_inperson_service']="active";
			UsersModel::find($userid)->update($updateuser);
		}
		
		$mailData['USER_FULL_NAME']="";
		$mailData['USER_EMAIL']="";
		$mailData['JOB_NAME']="";
		$mailData['JOB_RESPONSE_LINK']=url('buyer/appliedjobs');

		if(count($jobdata)){
			if(isset($jobdata->v_job_title)){
				$mailData['JOB_NAME']=$jobdata->v_job_title;
			}
			if(count($jobdata->hasUser()) && isset($jobdata->hasUser()->v_fname)){
		        $mailData['USER_FULL_NAME']=$jobdata->hasUser()->v_fname;
		    }
		    if(count($jobdata->hasUser()) && isset($jobdata->hasUser()->v_lname)){
		        $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$jobdata->hasUser()->v_lname;
		    }
		    if(count($jobdata->hasUser()) && isset($jobdata->hasUser()->v_email)){
		        $mailData['USER_EMAIL']= $jobdata->hasUser()->v_email;
		    }
		}	
		
		$mailcontent = EmailtemplateHelper::AppliedJobpost($mailData);
		$subject = "Seller applied for job";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b560ad1516b462ef00036a4");
		$mailids=array(
			$mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
		);
		EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
	
		$finalResponse=array(
			'is_applied'=>"1",
		);
		
		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.appiedJob'),
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);

	
	}

	public function ContactSelller(){

		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;
		
		$rules = [
			'i_job_id' => 'required',
			'v_subject_title' => 'required',
			'l_message'=>'required',
		];
		
		$validator = Validator::make($post_data, $rules);
    	if($validator->fails()) {
				$responseData=array(
						'code'=>400,
						'message'=>$validator->errors()->first(),
						'data'=>array(),
				);
				return response()->json($responseData, 400);
		}
		
		$jobdata = JobsModel::where('_id',$post_data['i_job_id'])->where('e_sponsor',"yes")->where('e_sponsor_status',"start")->first();
		if(count($jobdata)){
			$updatedata['i_contact'] = 1;
			if(isset($jobdata->i_contact)){
				$updatedata['i_contact'] = $jobdata->i_contact+1;
			}
			$a = JobsModel::find($jobdata->id)->update($updatedata);
		}
		$jobdata = JobsModel::find($post_data['i_job_id']);
		
		$data['i_parent_id']=0;
		$data['v_subject_title']=$post_data['v_subject_title'];
		$data['i_to_id'] = $jobdata->i_user_id;
		$data['i_from_id']=$userid;
		$data['l_message']=$post_data['l_message'];
		$data['e_view']="unread";
		$data['d_added']=date("Y-m-d H:i:s");
		$data['d_modified']=date("Y-m-d H:i:s");
		$mid = MessagesModel::create($data)->id;
		self::sendEmailNotificationSellerForMessage($data['i_to_id'],$post_data);	
		
		$sellerdata = UsersModel::find($jobdata->i_user_id);
		if(count($sellerdata)){
			if(isset($sellerdata->l_seller_auto_message) && $sellerdata->l_seller_auto_message!=""){
				$ctime=date("Y-m-d H:i:s");
				$ltime = $data['d_added'];
				$totaldiff =strtotime($ctime)-strtotime($ltime); 
				$totaldiff = $totaldiff;
				$insertreplay=array();
				$insertreplay['i_parent_id']=$mid;
				$insertreplay['v_subject_title']="";
				$insertreplay['i_to_id']=$userid;
				$insertreplay['i_from_id']=$jobdata->i_user_id;
				$insertreplay['l_message']=$sellerdata->l_seller_auto_message;
				$insertreplay['v_replay_time']=$totaldiff;
				$insertreplay['e_view']="unread";
				$insertreplay['d_added']=date("Y-m-d H:i:s");
				$insertreplay['d_modified']=date("Y-m-d H:i:s");
				$mid = MessagesModel::create($insertreplay)->id;
				self::sendEmailNotificationBuyerForMessage($jobdata->i_user_id,$userid,$sellerdata->l_seller_auto_message);	
			}
		}
		
		$finalResponse=array();
		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.messageSent'),
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);

	}

	public function sendEmailNotificationSellerForMessage($id="",$data=""){
        
		if($id==""){
            return 0;
        }

        $userdata = UsersModel::find($id);
		if(!count($userdata)){
			return 0;	
		}

		$sellername="";
        if(isset($userdata->v_fname)){
            $sellername = $userdata->v_fname;
        }
        if(isset($userdata->v_lname)){
            $sellername .= ' '.$userdata->v_lname;
        }

        $selleremail="";
        if(isset($userdata->v_email)){
            $selleremail = $userdata->v_email;
        }

        $buyername="";
        if(isset(auth()->guard('web')->user()->v_fname)){
            $buyername = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname)){
            $buyername .= ' '.auth()->guard('web')->user()->v_fname;
        }

        $messagestr="";
        if(isset($data['v_subject_title']) && $data['v_subject_title']!=""){
        	$messagestr  .= "<b>Subject </b>: ".$data['v_subject_title']."<br>";
        }
        if(isset($data['l_message']) && $data['l_message']!=""){
        	$messagestr  .= "<b>Message </b>: ".$data['l_message'];
        }

        $data=array(
            'USER_FULL_NAME'=>$sellername,
            'BUYER_NAME'=>$buyername,
            'MESSAGE'=>$messagestr,
        );
        $mailcontent = EmailtemplateHelper::sendMessageSeller($data);
        $subject = "Message";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bb5ba4276fbae17da5ac234");
        
        $mailids=array(
            $sellername=>$selleremail,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		}
		
		public function sendEmailNotificationBuyerForMessage($id="",$buyerid="",$msg=""){
        
			if($id==""){
							return 0;
					}
					$userdata = UsersModel::find($id);
			if(!count($userdata)){
				return 0;	
			}
			$sellername="";
					if(isset($userdata->v_fname)){
							$sellername = $userdata->v_fname;
					}
					if(isset($userdata->v_lname)){
							$sellername .= ' '.$userdata->v_lname;
					}
	
					$userdataBuyer = UsersModel::find($buyerid);
			if(!count($userdata)){
				return 0;	
			}
			$buyername="";
			if(isset($userdataBuyer->v_fname)){
					$buyername = $userdataBuyer->v_fname;
			}
			if(isset($userdataBuyer->v_lname)){
					$buyername .= ' '.$userdataBuyer->v_lname;
			}
			
			$buyeremail="";
			if(isset($userdataBuyer->v_email)){
					$buyeremail = $userdataBuyer->v_email;
			}
			$messagestr = "<b>Message </b>: ".$msg;

			$data=array(
					'USER_FULL_NAME'=>$buyername,
					'SELLER_NAME'=>$sellername,
					'MESSAGE'=>$messagestr,
			);
			$mailcontent = EmailtemplateHelper::sendMessageBuyer($data);
			$subject = "Message";
			$subject = EmailtemplateHelper::EmailTemplateSubject("5bb5bdfd76fbae2bb2317f43");
			$mailids=array(
					$buyername=>$buyeremail,
			);
			$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
	}


}