<?php
namespace App\Http\Controllers\Api\V1\Jobs;

use Request, Hash, Lang,Crypt,Session,Validator,Auth, Storage;
use App\Http\Controllers\Controller;
use App\Helpers\ApiGeneral as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Buyerreview as BuyerreviewModal;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Users\Messages as MessagesModel;
use App\Models\Categories as CategoriesModel;
use App\Models\Skills as SkillsModel;


class Postjob extends Controller {

	
	public function index(){
				
		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$inperson = CategoriesModel::where("e_status","active")->where("v_type","inperson")->orderBy('v_name')->get();
		$online = CategoriesModel::where("e_status","active")->where("v_type","online")->orderBy('v_name')->get();
		
		$inpersonList=array();
		if(count($inperson)){
			foreach($inperson as $k=>$v){
				$inpersonList[]=array(
					'id'=>$v->id,
					'v_title'=>$v->v_name,
					'v_image'=> $v->v_image_mobile ? \Storage::cloud()->url($v->v_image_mobile) : ''    
				);
			}
		}

		$onlineList=array();
		if(count($online)){
			foreach($online as $k=>$v){
				$onlineList[]=array(
					'id'=>$v->id,
					'v_title'=>$v->v_name,
					'v_image'=>$v->v_image_mobile ? \Storage::cloud()->url($v->v_image_mobile) : ''  
				);
			}
		}

		$inpersonList = array_chunk($inpersonList, 3);
		$onlineList = array_chunk($onlineList, 3);
		$finalResponse['inperson']=$inpersonList;
		$finalResponse['online']=$onlineList;
	
		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.myJobsList'),
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);
	}

	public function getCategorySkill(){
				
		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$rules = [
			'i_category_id' => 'required',
	    ];
		
		$validator = Validator::make($post_data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$skilldata = SkillsModel::where("i_category_id",$post_data['i_category_id'])->where("e_status","active")->get();

		$skilldataList=array();
		if(count($skilldata)){
			foreach ($skilldata as $key => $value) {
				if(isset($value->id) && isset($value->v_name)){

					$skilldataList[]=array(
						'id'=>$value->id,
						'v_name'=>$value->v_name,
					);
				}
			}
		}
		
		$finalResponse['skilllist']=$skilldataList;
		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.myJobsList'),
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);
	}

	public function postJob(){
				
		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;
		
		$rules = [
			'v_service'=> 'required',
			'i_category_id' => 'required',
			'i_mailskill_id' => 'required',
			'v_skill_level_looking'=> 'required',
			'v_job_title'=> 'required',
			'v_budget_type'=> 'required',
			'v_budget_amt'=> 'required',
			'l_short_description'=> 'required',
			'l_job_description'=> 'required',
			'v_city'=>'required',
			'v_pincode'=>'required',
			'i_long_expect_duration'=>'required',
			'v_long_expect_duration_type'=>'required',
			'v_search_tags'=>'required',
		];
		
		$validator = Validator::make($post_data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		
		$post_data['d_expiry_date'] = date('Y-m-d',strtotime('+30 days'));
		$post_data['i_total_avg_review']=0;
	  	$post_data['i_total_review']=0;
	  	$post_data['i_applied']=0;
		$post_data['i_user_id']=$userid;
		

		if(isset($post_data['i_otherskill_id']) && $post_data['i_otherskill_id']!=""){
			$post_data['i_otherskill_id'] = explode(",",$post_data['i_otherskill_id']);
		}

		if(isset($post_data['v_search_tags']) && $post_data['v_search_tags']!=""){
			$post_data['v_search_tags'] = explode(",",$post_data['v_search_tags']);
		}

		if(isset($post_data['v_budget_amt'])){
			$post_data['v_budget_amt'] = (float)$post_data['v_budget_amt'];
		}

		
		$post_data['d_start_date']=date("Y-m-d");
		if(isset($post_data['d_start_date']) && $post_data['d_start_date']!=""){
			$post_data['v_start_year']=date("Y",strtotime($post_data['d_start_date']));
			$post_data['v_start_month']=date("m",strtotime($post_data['d_start_date']));
			$post_data['v_start_date']=date("d",strtotime($post_data['d_start_date']));
		}

		$post_data['v_avalibility_from'] = "monday";
		$post_data['v_avalibility_to'] = "friday";
		$post_data['v_avalibilitytime_from'] = "10";
		$post_data['v_avalibilitytime_to'] = "18";
	
		if(isset($post_data['v_latitude']) && $post_data['v_latitude']!="" && isset($post_data['v_longitude']) && $post_data['v_longitude']!=""){
			$lat=(float)$post_data['v_latitude'];
			$lont=(float)$post_data['v_longitude'];
			$post_data['location']['type']="Point";
			$post_data['location']['coordinates']=array($lont,$lat);
		}
		
		$post_data['e_sponsor']="no";
		$post_data['e_status']="active";
		$post_data['d_added']=date("Y-m-d H:i:s");
		$post_data['d_modified']=date("Y-m-d H:i:s");
		$post_data['d_signup_date']=date("Y-m-d",strtotime($authdata->d_added));	

		$photos=array();
		if(isset($post_data['v_photos'])){
			$photos = $post_data['v_photos'];	
			unset($post_data['v_photos']);
		}
		if(isset($post_data['token'])){
			unset($post_data['token']);
		}
		$photosArr = array();
		if($photos){
			foreach($photos as $fileName){
				$imgname = self::uploadFileBase64($fileName,'public');
				$photosArr[] = $imgname;
			}
		}

		$jobData = JobsModel::create($post_data);
		$jobid = $jobData->id;

		if($photosArr){
		 	$update['v_photos']= $photosArr;
		 	JobsModel::find($jobid)->update($update);
		}
		
		
		// if(Request::hasFile('file')) {
		// 	sleep(1);
		// 	$image = Request::file('file');
		// 	$imgname = self::uploadFileBase64($image,'public');
			// $fileName = 'job-photo-'.time().'.'.$image->getClientOriginalExtension();
			// $fileName = str_replace(' ', '_', $fileName);
			// $update['v_photos'][] = 'jobpost/photos/' . $fileName;
			// $uploadS3 = Storage::disk('s3')->put('/jobpost/photos/'.$fileName, File::get((string)$image), 'public');
			// JobsModel::find($id)->update($update);
		// }
		$mailData['USER_FULL_NAME']="";
		$mailData['USER_EMAIL']="";
		$mailData['JOB_PREVIEW_LINK']=url('online-job').'/'.$jobid.'/'.$post_data['v_job_title'];

		if(isset($authdata->v_fname)){
			$mailData['USER_FULL_NAME'] = $authdata->v_fname;			
		}
		if(isset($authdata->v_lname)){
			$mailData['USER_FULL_NAME'] = $mailData['USER_FULL_NAME'].' '.$authdata->v_lname;	
		}
		if(isset($authdata->v_email)){
			$mailData['USER_EMAIL'] = $authdata->v_email;			
		}
		
		$mailcontent = EmailtemplateHelper::JobpostLive($mailData);
		$subject = "Job post preview";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b55fe15516b462cb0001aaa");
		$mailids=array(
			$mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
		);
		EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

		$finalResponse=array();
		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.postjob'),
			'data'    => $finalResponse,
		);
		return response()->json($responseData, 200);
	}

	public function edit(){
				
		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;

		$rules = [
			'i_job_id' => 'required',
	  	];
		
		$validator = Validator::make($post_data, $rules);
    	if($validator->fails()) {
			$responseData=array(
					'code'=>400,
					'message'=>$validator->errors()->first(),
					'data'=>array(),
			);
			return response()->json($responseData, 400);
		}
		$jobs = JobsModel::find($post_data['i_job_id']);

		if(!count($jobs)){

			$responseData=array(
					'code'=>400,
					'message'=>"Something went wrong.Please try again after sometime.",
					'data'=>array(),
			);
			return response()->json($responseData, 400);

		}else{
			$jobs->l_job_description = strip_tags(html_entity_decode($jobs->l_job_description));
			
			$job_photos=array();
			
			if(count($jobs->v_photos)){
				foreach($jobs->v_photos as $k=>$v){
					$job_photos[]="data:image/png;base64,".base64_encode(file_get_contents(\Storage::cloud()->url($v)));                
				}
			}
			$jobs->v_photos = $job_photos;

		}

		$responseData=array(
			'code'    => 200,
			'message' => "",
			'data'    => $jobs,
		);
		return response()->json($responseData, 200);	
	}

	public function updateJob(){
				
		$post_data = Request::all();
		$authdata = Request::get('authdata');
		$userid = $authdata->id;
		
		$rules = [
			'i_job_id' => 'required',
			'i_category_id' => 'required',
			'i_mailskill_id' => 'required',
			'v_skill_level_looking'=> 'required',
			'v_job_title'=> 'required',
			'v_budget_type'=> 'required',
			'v_budget_amt'=> 'required',
			'l_short_description'=> 'required',
			'l_job_description'=> 'required',
			'v_city'=>'required',
			'v_pincode'=>'required',
			'i_long_expect_duration'=>'required',
			'v_long_expect_duration_type'=>'required',
			'v_search_tags'=>'required',
		];
		
		$validator = Validator::make($post_data, $rules);
        if($validator->fails()) {
            $responseData=array(
                'code'=>400,
                'message'=>$validator->errors()->first(),
                'data'=>array(),
			);
			return response()->json($responseData, 400);
		}

		$job_id="";
		if(isset($post_data['i_job_id'])){
			$job_id=$post_data['i_job_id'];
			unset($post_data['i_job_id']);	
		}
		if(isset($post_data['token'])){
			unset($post_data['token']);
		}

		if(isset($post_data['i_otherskill_id']) && $post_data['i_otherskill_id']!=""){
			$post_data['i_otherskill_id'] = explode(",",$post_data['i_otherskill_id']);
		}

		if(isset($post_data['v_search_tags']) && $post_data['v_search_tags']!=""){
			$post_data['v_search_tags'] = explode(",",$post_data['v_search_tags']);
		}

		if(isset($post_data['v_budget_amt'])){
			$post_data['v_budget_amt'] = (float)$post_data['v_budget_amt'];
		}

		
		if(isset($post_data['d_start_date']) && $post_data['d_start_date']!=""){
			$post_data['v_start_year']=date("Y",strtotime($post_data['d_start_date']));
			$post_data['v_start_month']=date("m",strtotime($post_data['d_start_date']));
			$post_data['v_start_date']=date("d",strtotime($post_data['d_start_date']));
		}

		$post_data['v_avalibility_from'] = "monday";
		$post_data['v_avalibility_to'] = "friday";
		$post_data['v_avalibilitytime_from'] = "10";
		$post_data['v_avalibilitytime_to'] = "18";
	
		if(isset($post_data['v_latitude']) && $post_data['v_latitude']!="" && isset($post_data['v_longitude']) && $post_data['v_longitude']!=""){
			$lat=(float)$post_data['v_latitude'];
			$lont=(float)$post_data['v_longitude'];
			$post_data['location']['type']="Point";
			$post_data['location']['coordinates']=array($lont,$lat);
		}
		
		$post_data['e_status']="active";
		$post_data['d_modified']=date("Y-m-d H:i:s");
		
	
		$photos=array();
		if(isset($post_data['v_photos'])){
			$photos = $post_data['v_photos'];	
			unset($post_data['v_photos']);
		}
		
		$post_data['d_modified']=date("Y-m-d H;i:s");
		JobsModel::find($job_id)->update($post_data);

		$photosArr = array();
		if($photos){
			foreach($photos as $fileName){
				$imgname = self::uploadFileBase64($fileName,'public');
				$photosArr[] = $imgname;
			}
		}

		if($photosArr){
		 	$update['v_photos']= $photosArr;
		 	JobsModel::find($job_id)->update($update);
		}


		// if(Request::hasFile('file')) {
		// 	sleep(1);
		// 	$image = Request::file('file');
		// 	$fileName = 'job-photo-'.time().'.'.$image->getClientOriginalExtension();
		// 	$fileName = str_replace(' ', '_', $fileName);
		// 	$update['v_photos'][] = 'jobpost/photos/' . $fileName;
		// 	$uploadS3 = Storage::disk('s3')->put('/jobpost/photos/'.$fileName, File::get((string)$image), 'public');
		// 	JobsModel::find($id)->update($update);
		// }

		$finalResponse=array();
		$responseData=array(
			'code'    => 200,
			'message' => Lang::get('api.jobs.postjob'),
			'data'    => array(),
		);
		return response()->json($responseData, 200);
		
	}
	
	public function uploadFileBase64($file, string $remotePath){

		$data=$file;
		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);

		$diskName = env('S3_DISK') ? env('S3_DISK'):'public';
		$extension = "png";
		$fileName = str_replace('.', '', uniqid('', true)) . '.' . $extension;
		$fileName = $remotePath.'/'.$fileName;
		$uploadS3 = Storage::disk('s3')->put($fileName, $data, 'public');     
		// if ($uploadS3 === false) {
		//     throw new \Exception('File was not uploaded.');
		// }
		return $fileName;
	}
}