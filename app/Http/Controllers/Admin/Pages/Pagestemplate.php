<?php
namespace App\Http\Controllers\Admin\Pages;

use Request, Lang, File, GeneralHelper,Storage;

use App\Models\Pages\Pages as PagesModel;
use App\Models\Pages\Pagestemplate as PagestemplateModel;


use App\Http\Controllers\Controller;

class Pagestemplate extends Controller {

	protected $section;
	
	public function __construct(){
		$this->section = "Pages templates";
	}
	
	public function index() {

		$query = PagestemplateModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/pages/pagestemplate', $_data);
	}

	public function Add() {
		
		$_data=array(
			'view'=>"add",		
		);
		return view('admin/pages/pagestemplate', $_data);
	}
	
	public function Edit($id="") {
	    $data = PagestemplateModel::where("_id",$id)->first();
		
		$_data=array(
            'view'=>"edit",
            'data'=>$data,  
        );
		return view('admin/pages/pagestemplate', $_data);
	}

    public function Action($action="",$id="") {

        $post_data = Request::all();

        if($action=="add"){

		      	$existEmail = PagestemplateModel::where('v_title',$post_data['v_title'])->get();

	          	if(count($existEmail)){
	            	return redirect( 'admin/pages-template/add/0')->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section ])); 
	          	}
				$post_data['l_description'] = html_entity_decode($post_data['l_description']);
	            $post_data['d_added']=date("Y-m-d h:i:s");
	            $post_data['d_modified']=date("Y-m-d h:i:s");
	           	unset($post_data['_token']);
                PagestemplateModel::create($post_data);  
            	return redirect('admin/pages-template')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ]));

        }else if($action=="edit"){

        		$existEmail = PagestemplateModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
		      	if(count($existEmail)){
	            	return redirect( 'admin/pages-template/edit/'.$id)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section ])); 
	          	}
				$post_data['l_description'] = html_entity_decode($post_data['l_description']);
            	$post_data['dModify']=date("Y-m-d h:i:s");
	            PagestemplateModel::find($id)->update($post_data);
	            return redirect( 'admin/pages-template/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){

            $_data = PagestemplateModel::where('_id',$id)->first();
            
            if(count($_data)){
		        PagestemplateModel::find($id)->delete();
            	return redirect( 'admin/pages-template')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/pages-template')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }
        }     
    }

}