<?php
namespace App\Http\Controllers\Admin\Pages;

use Request, Lang, File, GeneralHelper,Storage;

use App\Models\Pages\Pages as PagesModel;

use App\Http\Controllers\Controller;

class Pages extends Controller {

	protected $section;

	public function __construct(){
		$this->section = "Pages";
	}

	public function index() {

		$query = PagesModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/pages/pages', $_data);
	}

	public function Add() {

		$pagelist = PagesModel::get();
		$_data=array(
			'view'=>"add",		
			'pagelist'=>$pagelist,		
		);
		return view('admin/pages/pages', $_data);
	}
		
	public function Edit($id="") {

        $data = PagesModel::where("_id",$id)->first();
        $pagelist = PagesModel::get();

        $_data=array(
            'view'=>"edit",
            'data'=>$data,  
            'pagelist'=>$pagelist,          
        );
        return view('admin/pages/pages', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();

        if($action=="add"){

		      	$existEmail = PagesModel::where('v_title',$post_data['v_title'])->get();

	          	if(count($existEmail)){
	            	return redirect( 'admin/pages/add/0')->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section ])); 
	          	}

	          	if(Request::file()){
			  
					$image = Request::file('v_banner_img');
					$ext   = $image->getClientOriginalExtension();
					
					if(count($image) ){
						
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
							
							$fileName = 'page-'.time().'.'.$image->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_banner_img'] = 'pages/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/pages/'.$fileName, File::get((string)$image), 'public');

						}else{
							return redirect( 'admin/pages/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						
						}
					}
				}

				$post_data['l_description'] = html_entity_decode($post_data['l_description']);
	            $post_data['d_added']=date("Y-m-d h:i:s");
	            $post_data['d_modified']=date("Y-m-d h:i:s");
	           	unset($post_data['_token']);
                PagesModel::create($post_data);  
            	return redirect('admin/pages')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ]));

        }else if($action=="edit"){

        		$existEmail = PagesModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
	          	
	          	if(count($existEmail)){
	            	return redirect( 'admin/pages/edit/'.$id)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section ])); 
	          	}

	          	if(Request::file()){
				
					$image = Request::file('v_banner_img');
					$ext   = $image->getClientOriginalExtension();
					
					if( count($image) ){
						
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){

							$imageExists = PagesModel::where('_id',$id)->value('v_banner_img');
							if(Storage::disk('s3')->exists($imageExists)){
								Storage::disk('s3')->Delete($imageExists);
							}

							$fileName = 'page-'.time().'.'.$image->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_banner_img'] = 'pages/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/pages/'.$fileName, File::get((string)$image), 'public');

						}else{
							return redirect('admin/pages/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}
				
				}

			  	$post_data['l_description'] = html_entity_decode($post_data['l_description']);
            	$post_data['dModify']=date("Y-m-d h:i:s");
	            PagesModel::find($id)->update($post_data);
	            return redirect( 'admin/pages/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){

            $_data = PagesModel::where('_id',$id)->first();
            
            if(count($_data)){
               	
               	$destination = public_path('uploads/pages');
				$imageExists = $_data['v_page_icon'];
				
				if( isset($imageExists) && $imageExists != '' ) {
					if( File::exists( $destination . '/' . $imageExists ) ) {
						File::delete( $destination . '/' . $imageExists );
					}
				}
				
				$destination = public_path('uploads/pages');
				$imageExists = $_data['v_banner_img'];
				
				if( isset($imageExists) && $imageExists != '' ) {
					if( File::exists( $destination . '/' . $imageExists ) ) {
						File::delete( $destination . '/' . $imageExists );
					}
				}
			    PagesModel::find($id)->delete();
            	return redirect( 'admin/pages')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/pages')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }
        }     
    }

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = PagesModel::find($id);
			$country->update($data);
		}

		return redirect('admin/pages')->with('success', $message);
	}



}