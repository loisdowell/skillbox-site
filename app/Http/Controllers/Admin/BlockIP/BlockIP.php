<?php
namespace App\Http\Controllers\Admin\BlockIP;

use Request, Lang, File;

use App\Models\BlockedIPaddress as BlockIPModel;

use App\Http\Controllers\Controller;

class BlockIP extends Controller {

	protected $section;

	public function __construct(){
		
		$this->section = 'Block IP';
	}

	public function index() {

		$query = BlockIPModel::query();
		$data = $query->orderBy('_id','DESC')->get();
		$_data=array(
			'section'=>$this->section,
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/block-ip/block-ip', $_data);
	}

	public function Add() {

		$_data=array(
			'section'=>$this->section,
			'view'=>"add",
		);
		return view('admin/block-ip/block-ip', $_data);
	
	}
	
	public function Edit($id="") {

        $data = BlockIPModel::where("_id",$id)->first();

        $_data=array(
        	'section'=>$this->section,
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/block-ip/block-ip', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();

        if($action=="add"){

		      	$existEmail = BlockIPModel::where('v_ipaddress',$post_data['v_ipaddress'])->get();
	          	if(count($existEmail)){
	          		
	            	return redirect( 'admin/block-ip/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section.' IP Address' ])); 
	          	}
	            
	           	unset($post_data['_token']);
                BlockIPModel::create($post_data);  
            	return redirect( 'admin/block-ip')->with( 'success', Lang::get('message.common.detailAdded', [ 'section' => $this->section ])); 

        }else if($action=="edit"){

        		$existEmail = BlockIPModel::where('v_ipaddress',$post_data['v_ipaddress'])->where('_id',"!=",$id)->get();
	          	if(count($existEmail)){
	            	return redirect( 'admin/block-ip/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section.' IP Address' ])); 
	          	}
             	
            	BlockIPModel::find($id)->update($post_data);
	            return redirect( 'admin/block-ip/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){

            $_data = BlockIPModel::where('_id',$id)->first();
            if(count($_data)){
                BlockIPModel::find($id)->delete();
                return redirect( 'admin/block-ip')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/block-ip')->with( 'warning', Lang::get('message.common.somethingWrong'));
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive'
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active'
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$category = BlockIPModel::find($id);
			$category->update($data);
		}

		return redirect('admin/block-ip')->with('success', $message);
	}

}
