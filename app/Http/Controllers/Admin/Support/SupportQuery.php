<?php
namespace App\Http\Controllers\Admin\Support;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Support\SupportQuery as SupportQueryModel;

class SupportQuery extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "Support Query";
	}

	public function index() {

		$query = SupportQueryModel::query();
		$data = $query->orderBy('i_order','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/support/supportcategory', $_data);
	}

	public function Add() {

		$_data=array(
			'view'=>"add",
		);
		return view('admin/support/supportcategory', $_data);
	
	}
	
	public function Edit($id="") {
		
	  $data = SupportQueryModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/support/supportcategory', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

       		$existCat = SupportQueryModel::where('v_name',$post_data['v_name'])->get();
	        if(count($existCat)){
	        	return redirect( 'admin/supportquery/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

	        $post_data['i_order'] = (int)$post_data['i_order'];
	        $post_data['d_added']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        
	        SupportQueryModel::create($post_data); 
	        return redirect('admin/supportcategory')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
        		
    			$existName = SupportQueryModel::where('v_name',$post_data['v_name'])->where('_id',"!=",$id)->get();
    	  		
    	  		if(count($existName)){
            		return redirect( 'admin/supportquery/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
          		}
				$post_data['i_order'] = (int)$post_data['i_order'];  
				$post_data['d_modified']=date("Y-m-d h:i:s");
	            SupportQueryModel::find($id)->update($post_data);
	            return redirect( 'admin/supportquery/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = SupportQueryModel::find($id)->first();

            if(count($_data)){
               	
                SupportQueryModel::find($id)->delete();
                return redirect( 'admin/supportquery')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/supportquery')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = SupportQueryModel::find($id);
			$country->update($data);
		}

		return redirect('admin/supportquery')->with('success', $message);
	}



}