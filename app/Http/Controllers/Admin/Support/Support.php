<?php
namespace App\Http\Controllers\Admin\Support;

use Request, Lang, File;
use App\Http\Controllers\Controller;
use App\Models\Support\SupportCategory as SupportCategoryModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Admin as AdminModel;
use App\Models\Support\SupportQuery as SupportQueryModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;


class Support extends Controller {

	protected $section;
	public function __construct(){ 
		$this->section = "Support";
	}

	public function index() {

		$query 	= SupportModel::query();
		$query 	= $query->where('i_parent_id',0);
		$data 	= $query->orderBy('d_added','DESC')->get();
		
		//$supportquery = GeneralHelper::supportQueryData();

		$query = SupportQueryModel::query();
		$supportquery = $query->where('e_status','active')->orderBy('i_order')->get();

		$supportqueryList=array();
		if(count($supportquery)){
			foreach ($supportquery as $key => $value) {
				$supportqueryList[$value->id]=$value->v_name;		
			}
		}


		$_data	= array(
			'view'	=>"list",
			'data'	=>$data,
			'supportquery'=> $supportqueryList,
		);
		return view('admin/support/support', $_data);
	}

	public function Add() {
		//$supportquery = GeneralHelper::supportQueryData();
		$users = UsersModel::where("e_status","active")->get();
		$query = SupportQueryModel::query();
		$supportquery = $query->where('e_status','active')->orderBy('i_order')->get();

		$supportqueryList=array();
		if(count($supportquery)){
			foreach ($supportquery as $key => $value) {
				$supportqueryList[$value->id]=$value->v_name;		
			}
		}
		$admin_list = AdminModel::where("e_status","active")->get();
		$messagedata=array();
		$_data=array(
			'view'				=>"add",
			'supportquery' 		=> $supportqueryList,
			'users'				=> $users,
			'messagedata'	=> $messagedata,
			 'admin_list'	=>$admin_list,
		);
		return view('admin/support/support', $_data);
	}

	public function Edit($id="") {
	  	$users = UsersModel::where("e_status","active")->get();
		
		$query = SupportQueryModel::query();
		$supportquery = $query->where('e_status','active')->orderBy('i_order')->get();

		$supportqueryList=array();
		if(count($supportquery)){
			foreach ($supportquery as $key => $value) {
				$supportqueryList[$value->id]=$value->v_name;		
			}
		}
		

		$admin_list = AdminModel::where("e_status","active")->get();
		$data = SupportModel::find($id);
		$messagedata = SupportModel::where('i_parent_id',$id)->get();
	    
	    $_data = array(
            'view'			=>"edit",
            'data'			=>$data,
            'admin_list'	=>$admin_list,
            'supportquery' 	=> $supportqueryList,
			'users'			=> $users,
			'messagedata'	=> $messagedata
        );
        return view('admin/support/support', $_data);

    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
        if(isset($post_data['_token'])){
    		unset($post_data['_token']);
    	}

        if($action=="add"){
        	
        	$post_data['i_parent_id'] = 0;	
        	$post_data['e_user_type'] = "user";	
        	$post_data['i_ticket_num'] = GeneralHelper::supportNumber();
        	$post_data['d_added'] = date("Y-m-d H:i:s");
        	$post_data['d_modified'] = date("Y-m-d H:i:s");

            SupportModel::create($post_data); 
	        return redirect('admin/support')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$supportdata = SupportModel::find($id); 

        	if(isset($post_data['i_admin_id']) && $post_data['i_admin_id']!="" ){

        		if(!isset($supportdata->i_admin_id)){
        			self::AssignSupportTicket($supportdata,$post_data['i_admin_id']);	
        		}else{
        			if($supportdata->i_admin_id!=$post_data['i_admin_id']){
        				self::AssignSupportTicket($supportdata,$post_data['i_admin_id']);		
        			}
        		}
        	}

        	if(isset($post_data['e_status']) && $post_data['e_status']=="close" && $supportdata->e_status!="close"){
        		self::CloseSupportTicket($supportdata);		
        	}

        	if(isset($post_data['e_status']) && $post_data['e_status']=="open" && $supportdata->e_status!="open"){
        		self::OpenSupportTicket($supportdata);		
        	}


			$post_data['d_modified'] = date("Y-m-d H:i:s");
		    SupportModel::find($id)->update($post_data);
            return redirect( 'admin/support/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
        
        }else if($action=="sendreply"){

        	$supportdata = SupportModel::find($id); 

        	$userid = auth()->guard('admin')->user()->_id;
        	$post_data['i_parent_id'] = $id;
			$post_data['i_user_id'] = $userid;
			$post_data['e_user_type'] = "admin";
			$post_data['e_view'] = "unread";
			$post_data['i_touser_id'] = $supportdata->i_user_id;
			$post_data['d_added'] = date("Y-m-d H:i:s");
        	$post_data['d_modified'] = date("Y-m-d H:i:s");
        	SupportModel::create($post_data); 
        	
        	//$supportdata = SupportModel::find($id); 

        	$data['USER_FULL_NAME']="";
        	$data['USER_EMAIL']="";
			$data['SUPPORT_QUERY']="";
			$data['SUPPORT_QUERY_REPLAY']=$post_data['l_question'];

			if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_fname)){
				$data['USER_FULL_NAME'] = $supportdata->hasUser()->v_fname;
			}
			if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_lname)){
				$data['USER_FULL_NAME'] .= ' '.$supportdata->hasUser()->v_lname;
			}	
			if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_email)){
				$data['USER_EMAIL'] = $supportdata->hasUser()->v_email;
			}
			if(count($supportdata->hasQuery()) && isset($supportdata->hasQuery()->v_name)){
				$data['SUPPORT_QUERY'] = $supportdata->hasQuery()->v_name;
			}

			$mailcontent = EmailtemplateHelper::supportReplaytoUser($data);
			$subject = "Support";
			$subject = EmailtemplateHelper::EmailTemplateSubject("5bbdf8ba76fbae01fc5a6552");
			
			$mailids=array(
				$data['USER_FULL_NAME']=>$data['USER_EMAIL']
			);
			$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

			return redirect( 'admin/support/edit/'.$id)->with( 'success', "Successfully send reply."); 


	
	    }else if($action=="delete"){
        	
        	$_data = SupportModel::find($id)->first();

            if(count($_data)){
                SupportModel::find($id)->delete();
                return redirect( 'admin/support')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/support')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	
    public function status( $id, $status ) {
		
		if ($status == 'open') {

			$data = [
				'e_status' => 'close',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusClose', [ 'section' => $this->section ]);
		}
		else if ($status == 'close') {

			$data = [
				'e_status' => 'open',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusOpen', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = SupportModel::find($id);
			$country->update($data);
		}

		return redirect('admin/support')->with('success', $message);
	}

	public function AssignSupportTicket($supportdata,$aid) {

		if(!count($supportdata)){
			return 0;
		}
		$data['USER_NAME']="";
    	$data['SUPPORT_QUERY']="";
    	$data['SUPPORT_CATEGORY']=ucfirst($supportdata->e_type);
    	$data['SUPPORT_QUESTIONS']=$supportdata->l_question;
    	$data['SUPPORT_LINK']=url('admin/support/edit')."/".$supportdata->id;

		if(count($supportdata->hasQuery()) && isset($supportdata->hasQuery()->v_name)){
			$data['SUPPORT_QUERY'] = $supportdata->hasQuery()->v_name;
		}
		$admindata = AdminModel::find($aid);
		if(!count($admindata)){
			return 0;
		}
		$data['USER_NAME']=$admindata->v_name;
		$mailcontent = EmailtemplateHelper::AssignSupportTicket($data);
		$subject = "Assign Support ticket";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5bd6c2a876fbae537e4660d2");
		$mailids=array(
			$admindata->v_name=>$admindata->v_email
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
	}

	public function CloseSupportTicket($supportdata) {

		if(!count($supportdata)){
			return 0;
		}

		$data['USER_FULL_NAME']="";
    	$data['USER_EMAIL']="";
		$data['SUPPORT_QUERY']="";
		
		if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_fname)){
			$data['USER_FULL_NAME'] = $supportdata->hasUser()->v_fname;
		}
		if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_lname)){
			$data['USER_FULL_NAME'] .= ' '.$supportdata->hasUser()->v_lname;
		}	
		if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_email)){
			$data['USER_EMAIL'] = $supportdata->hasUser()->v_email;
		}
		if(count($supportdata->hasQuery()) && isset($supportdata->hasQuery()->v_name)){
			$data['SUPPORT_QUERY'] = $supportdata->hasQuery()->v_name;
		}

		$mailcontent = EmailtemplateHelper::CloseSupportTicket($data);
		$subject = "Close Support ticket by admin";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5bd6c70376fbae530721e632");
		
		$mailids=array(
			$data['USER_FULL_NAME']=>$data['USER_EMAIL']
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
	}

	public function OpenSupportTicket($supportdata) {

		if(!count($supportdata)){
			return 0;
		}

		$data['USER_FULL_NAME']="";
    	$data['USER_EMAIL']="";
		$data['SUPPORT_QUERY']="";
		
		if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_fname)){
			$data['USER_FULL_NAME'] = $supportdata->hasUser()->v_fname;
		}
		if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_lname)){
			$data['USER_FULL_NAME'] .= ' '.$supportdata->hasUser()->v_lname;
		}	
		if(count($supportdata->hasUser()) && isset($supportdata->hasUser()->v_email)){
			$data['USER_EMAIL'] = $supportdata->hasUser()->v_email;
		}
		if(count($supportdata->hasQuery()) && isset($supportdata->hasQuery()->v_name)){
			$data['SUPPORT_QUERY'] = $supportdata->hasQuery()->v_name;
		}

		$mailcontent = EmailtemplateHelper::OpenSupportTicket($data);
		$subject = "Reopen Support ticket by admin";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5bd6c87076fbae5195337fc3");
		
		$mailids=array(
			$data['USER_FULL_NAME']=>$data['USER_EMAIL']
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

	}



}