<?php
namespace App\Http\Controllers\Admin\Plan;

use Request, Lang, File;
use App\Http\Controllers\Controller;
use App\Models\Plan as PlanModel;
use App\Helpers\General as GeneralHelper;

class Plan extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Plan";
	}

	public function index() {
		
		// $plandata = GeneralHelper::PlansList();
		// dd($plandata);
		$query = PlanModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/plan/plan', $_data);
	}

	public function Add() {

		$_data=array(
			'view'=>"add",
		);
		return view('admin/plan/plan', $_data);
	
	}
	
	public function Edit($id="") {
		
	  $data = PlanModel::find($id);

	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/plan/plan', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();

        if($action=="add"){
        
        	$post_data['d_added'] = date("Y-m-d h:i:s");
	        unset($post_data['_token']);

	        if(isset($post_data['l_bullet']) && count($post_data['l_bullet'])){
       			foreach ($post_data['l_bullet'] as $key => $value) {
       				if($value==""){
       					unset($post_data['l_bullet'][$key]);
       				}
       			}
       		}
       		$post_data['d_modified'] = date("Y-m-d h:i:s");	
	        PlanModel::create($post_data); 
	        return redirect('admin/plan')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
       		
       		$post_data['d_modified'] = date("Y-m-d h:i:s");
       		if(isset($post_data['l_bullet']) && count($post_data['l_bullet'])){
       			foreach ($post_data['l_bullet'] as $key => $value) {
       				if($value==""){
       					unset($post_data['l_bullet'][$key]);
       				}
       			}
       		}

       	    PlanModel::find($id)->update($post_data);
	        return redirect( 'admin/plan/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = PlanModel::find($id)->first();

            if(count($_data)){
         	    PlanModel::find($id)->delete();
                return redirect( 'admin/plan')->with('success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/plan')->with('warning', Lang::get('message.common.somethingWrong')); 
            }
        }
    }

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$faqs = PlanModel::find($id);
			$faqs->update($data);
		}

		return redirect('admin/plan')->with('success', $message);
	}



}