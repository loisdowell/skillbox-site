<?php

namespace App\Http\Controllers\Admin;

use Validator, Session, Request, DB, Auth, Hash, Lang, Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Admin\LoginRequest;
use App\Helpers\General as GeneralHelper;
use App\Models\Admin as AdminModel;
use App\Models\Sitesettings as SitesettingsModel;
use App\Models\EmailTemplate as EmailTemplateModel;


class Login extends Controller {

	protected function getLogin() {
	
		if (auth()->guard('admin')->check()) {
			return redirect('admin/dashboard');
		}
		return view('admin/login/login');
	}
	
	protected function postLogin(LoginRequest $request) {
		
		$remember_me = $request->only('remember') ? true : false;
		$auth = auth()->guard('admin');
		if ($auth->attempt($request->only('v_email', 'password'), $remember_me)) {
			return redirect('admin/dashboard');
		}
		else {
			return redirect('admin/login')->with('warning', Lang::get('message.common.incorrectEmailPassword') ); 
		}
	}
	
	protected function getLogout() {

		$auth = auth()->guard('admin');
		$auth->logout();

		return redirect('admin/login')->with([
			'success' => 'Logged Out',
		]);
	}

	/**
	 * Forgot Password
	 */
	protected function getForgotPassword() {

		if (auth()->guard('admin')->check()) {
			return redirect('admin/dashboard');
		}
		return view('admin/login/forgotpassword');
	}

	/**
	 * Forgot Password ->
	 * Authenticate User
	 */
	protected function postForgotPassword( Request $request ) {
		$data = Request::All();
		
		if (auth()->guard('admin')->check()) {
			return redirect('admin/dashboard');
		}

		$validator = Validator::make($data, [
			'v_email' => 'required|min:2|max:30|email',
		]);
		if($validator->fails()) {
			$messages = $validator->errors()->first();
			return redirect('admin/forgotpassword')->with( 'warning', $messages );
		}else{
			$admin = AdminModel::where('v_email',$data['v_email'])->first();
			if( isset($admin) && count($admin) ) {

				$resetPasswordToken = str_random(60);
				$otp = GeneralHelper::generateOtp();
				AdminModel::where('id',$admin->id)->update([
															'reset_password_token'	=> $resetPasswordToken,
															'v_code' => $otp,
															'd_modified' => date("Y-m-d H:i:s"),
														]);

				$sitesetting	= SitesettingsModel::all();
				$siteData		= [];
				$sitesetting	= json_decode(json_encode($sitesetting), True);
				// echo '<pre>';
				// print_r($sitesetting);
				// exit;
				
				foreach ( $sitesetting as $key => $value ) {

					$siteData[$value['v_key']] = $value['v_value'];
				}

				$userId = encrypt($admin->id.' - '.time());

				$mailLink = url('admin/new-password').'?admin='.$userId.'&token='.$resetPasswordToken;
				$emailTemplate = EmailTemplateModel::find(4);
				

				$keys = [
					'{USER_ACC_RESET_LINK}'		=> $mailLink ?: $siteData['SITE_URL'],
					'{SITE_LOGO_IMAGE}'		=>  $siteData['SITE_LOGO'] ?$siteData['SITE_LOGO']:"",
					'{SITE_EMAIL}'		=>  $siteData['SITE_EMAIL'] ?$siteData['SITE_EMAIL']:"",
					'{USER_FULL_NAME}'				=> $admin->v_name ?: '',
					'{SITE_URL}'			=> url('/'),
					'{SITE_LOGO}'			=> url('/public/uploads/settings/').'/'.$siteData['SITE_LOGO'],
				];
			
				$siteData['SMTP_FROM_EMAIL'] = ( $emailTemplate->v_from )?$emailTemplate->v_from:$siteData['SMTP_FROM_EMAIL'];

				//$content = $emailTemplate->l_body ?: '';
    			$content = "";
    			if(isset($emailTemplate->hasTemplateHeader->l_description))
    				$content .= $emailTemplate->hasTemplateHeader->l_description;
	            
	            $content .= $emailTemplate->l_body;
	            
	            if(isset($emailTemplate->hasTemplateFooter->l_description))
    				$content .= $emailTemplate->hasTemplateFooter->l_description;

				$subject = isset($emailTemplate->v_subject) ? $emailTemplate->v_subject : 'Reset Passwort';
				if( isset($content) && $content != '' && $content != NULL ) {
				}else{
					$content = $mailLink;
				}
				foreach ($keys as $k => $v){
					$content = str_replace( $k, $v, $content );
					$subject = str_replace( $k, $v, $subject );
				}

				$adminName = '';
				if( isset($admin->v_name) ){ $adminName = $admin->v_name; }

				try {
					$mail = new PHPMailer;
					$mail->isSMTP();
					$mail->CharSet = "utf-8";
					$mail->Host = $siteData['SMTP_HOST'];
					$mail->Port = (int)$siteData['SMTP_PORT'];
					$mail->SMTPAuth = true;
					$mail->SMTPSecure = $siteData['SMTP_PROTOCOL'];
					$mail->Username = $siteData['SMTP_USERNAME'];
					$mail->Password = $siteData['SMTP_PASSWORD'];
					$mail->setFrom($siteData['SMTP_FROM_EMAIL'], $siteData['SMTP_FROM_NAME']);
					$mail->Subject = $subject;
					$mail->MsgHTML($content);
					$mail->addAddress($admin->v_email, $adminName);
					$mail->isHTML(true);
					$mail->send();
				}
				catch (phpmailerException $e) {

					return $e->errorMessage();
				}
				catch (Exception $e) {

					return $e->getMessage();
				}

				/**
				 * SEND MAIL TO CHANGE PASSWORD
				 */
				return redirect('admin/forgotpassword')->with('success', Lang::get('message.checkEmail'));
			}
			else {
				return redirect('admin/forgotpassword')->with('warning', Lang::get('message.emailNotExist'));
			}	
		}
	}

	/*
	* code for reset new password
	*/
	public function newPassword() {

		$data = Request::all();
		$link = '?admin='.$data['admin']."&token=".$data['token'];
		 

		if( isset($data['admin']) && $data['admin'] != '' ) {
			try {
				$adminData = decrypt($data['admin']);
				$adminDataArray = explode(' - ', $adminData);
			}
			catch (DecryptException $e) {
				dd($e);
			}
			if( isset($adminDataArray) && count($adminDataArray) > 1 ) {
				$admin = AdminModel::find($adminDataArray[0]);

				if( isset($admin) && !empty($admin) ) {

					if( isset($admin->reset_password_token) && isset($data['token']) && $data['token'] == $admin->reset_password_token ) {

						return view( 'admin/login/new-password' )->with([
																	'admin_id' => $admin->id,
																	'reset_password_token' => $data['token'],
																	'link' => $link,
																]);
					}
					else {
						return redirect('admin/login')->with('warning',Lang::get('message.requestExpired'));
					}
				}
				else {

					return redirect('admin/login')->with('warning',Lang::get('message.somethingWrong'));
				}
			}
			else {

				return redirect('admin/login')->with('warning',Lang::get('message.somethingWrong'));
			}
		}
		else {
			return redirect('admin/login')->with('warning',Lang::get('message.somethingWrong'));
		}
	}
	
	/*
	* Confirm new password of admin
	*/
	public function postConfirmNewPassword( Request $request ) {

		$data = Request::all();
		if( $data['password'] == $data['confirm_password'] ){
			if( isset($data['admin_id']) && $data['admin_id'] != '' ) {
				$user = AdminModel::find($data['admin_id']);
				if(
						isset($user->reset_password_token) &&
						isset($data['reset_password_token']) &&
						$user->reset_password_token == $data['reset_password_token']
					)
				{

					$update = [
							'password' => Hash::make($data['password']),
							'reset_password_token' => '',
							'd_modified' => date('Y-m-d H:i:s'),
						];

					$user->update($update);
					return redirect('admin/login')->with('success',Lang::get('message.reseAccount'));
				}
				else {
					return redirect('admin/new-password'.$data['link'])->with('warning',Lang::get('message.requestExpired'));
				}
			}
			else {

				return redirect('admin/new-password'.$data['link'])->with('warning',Lang::get('message.adminNotExist'));
			}
		}else{
			
			return redirect('admin/new-password'.$data['link'])->with('warning',Lang::get('message.passwordNotMatch'));
		}
		
	}


	
}