<?php
namespace App\Http\Controllers\Admin\Job;

use Request, Lang, File,Storage;
use App\Http\Controllers\Controller;
use App\Models\Plan as PlanModel;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Skills as SkillsModel;


class Job extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "Job";
	}

	public function index() {
		
		$query = JobsModel::query();
		$data = $query->where('i_delete','!=',"1")->orderBy('d_added','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/job/job', $_data);
	}

	public function Add() {

		$category = CategoriesModel::where("e_status","active")->get();
		$usersLists = UsersModel::where("e_status","active")->where("buyer.e_status","active")->get();

		$_data=array(
			'view'=>"add",
			'category'=>$category,
			'usersLists'=>$usersLists,
		);

		return view('admin/job/job', $_data);
	
	}
	
	public function Edit($id="")
	{

		$category = CategoriesModel::where("e_status","active")->get();
		$usersLists = UsersModel::where("e_status","active")->where("buyer.e_status","active")->get();
		$skilldata = SkillsModel::where("e_status","active")->get();
		$data=JobsModel::find($id);

		$_data=array(
			'view' => "edit",
			'data' => $data,
			'category'=> $category,
			'usersLists' => $usersLists,
			'skilldata' => $skilldata,
		);
		return view('admin/job/job',$_data);
	}

	public function Action($action="",$id=""){
			
			$post_data=Request::all();
			
			// if(isset($post_data['v_search_tags']) && $post_data['v_search_tags']!=""){
			// 	$post_data['v_search_tags'] = explode(",", $post_data['v_search_tags']);
			// }
			
			if(isset($post_data['_token'])){
		     	unset($post_data['_token']);
		    }

            if($action=="add"){
			
				if(Request::hasFile('work_video')) {
					
					$destination = public_path('uploads/userprofiles');
					$image = Request::file('work_video');
					
					foreach($image as $file){
						$fileName = 'job-video-'.time().'.'.$file->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_video'][] = 'jobpost/videos/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/jobpost/videos/'.$fileName, File::get((string)$file), 'public');
				    }
			    }	

				if(Request::hasFile('photos')) {

					$destination = public_path('uploads/userprofiles');
					$image = Request::file('photos');

					foreach($image as $file){
			           	
			           	$fileName = 'job-photo-'.time().'.'.$file->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_photos'][] = 'jobpost/photos/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/jobpost/photos/'.$fileName, File::get((string)$file), 'public');
				    }
			  	}

			  	if(isset($post_data['v_latitude']) && $post_data['v_latitude']!="" && isset($post_data['v_longitude']) && $post_data['v_longitude']!=""){
					$lat=(float)$post_data['v_latitude'];
					$lont=(float)$post_data['v_longitude'];
					$post_data['location']['type']="Point";
					$post_data['location']['coordinates']=array($lat,$lont);
				}

				$post_data['d_expiry_date'] = date('Y-m-d',strtotime('+30 days'));
			  	$post_data['i_total_avg_review']=0;
			  	$post_data['i_total_review']=0;
			  	$post_data['i_applied']=0;
			  	$post_data['e_sponsor']="no";
			  	$post_data['e_status']="active";
			  	$post_data['d_added']=date('Y-m-d H:i:s');
			  	$post_data['d_modified']=date("Y-m-d H:i:s");

			  	if(isset($post_data['d_start_date']) && $post_data['d_start_date']!=""){
					$post_data['v_start_year']=date("Y",strtotime($post_data['d_start_date']));
					$post_data['v_start_month']=date("m",strtotime($post_data['d_start_date']));
					$post_data['v_start_date']=date("d",strtotime($post_data['d_start_date']));
				}

			    if(isset($post_data['work_video'])){
			     	unset($post_data['work_video']);
			    }
			    if(isset($post_data['photos'])){
			     	unset($post_data['photos']);
			    }
		     	JobsModel::create($post_data);
		     	return redirect('admin/job')->with('success',Lang::get('message.common.detailAdded', [ 'section' => $this->section ]));
		 	}

		    else if($action=="edit"){

		    	$olddata = JobsModel::find($id);

		    	if(isset($olddata->v_video) && count($olddata->v_video)){
					$post_data['v_video'] = $olddata->v_video;	
				}

				if(Request::hasFile('work_video')) {
					$image = Request::file('work_video');
					foreach($image as $file){
			        	$fileName = 'job-video-'.time().'.'.$file->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_video'][] = 'jobpost/videos/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/jobpost/videos/'.$fileName, File::get((string)$file), 'public');
				    }
				}

			    if(isset($olddata->v_photos) && count($olddata->v_photos)){
					$post_data['v_photos'] = $olddata->v_photos;	
				}

			    if(Request::hasFile('photos')) {

					$image = Request::file('photos');
					foreach($image as $file){
			 		    $fileName = 'job-photo-'.time().'.'.$file->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_photos'][] = 'jobpost/photos/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/jobpost/photos/'.$fileName, File::get((string)$file), 'public');

				    }
			  	}
			  	
			  	$post_data['d_modified']=date('Y-m-d H:i:s');
			  	$post_data['e_status'] = 'active';

			  	if(isset($post_data['work_video'])){
			     	unset($post_data['work_video']);
			     }
			     if(isset($post_data['photos'])){
			     	unset($post_data['photos']);
			     }

			    JobsModel::find($id)->update($post_data); 
	            return redirect('admin/job')->with('success',Lang::get('message.common.detailAdded', [ 'section' => $this->section ]));
	        }
		    else if($action=="delete"){
		    	
		    	$_data = JobsModel::find($id);
		    	
		    	if(count($_data)){
		    		
		    		$update['i_delete']="1";
		    		JobsModel::find($id)->update($update); 
		    	// 	if(isset($_data->v_video) && count($_data->v_video)){
		    	// 		foreach ($_data->v_video as $k => $v) {
		    	// 			if(Storage::disk('s3')->exists($v)){
							//  	Storage::disk('s3')->Delete($v);
							// }
		    	// 		}
		    	// 	}
		    	// 	if(isset($_data->v_photos) && count($_data->v_photos)){
		    	// 		foreach ($_data->v_photos as $k => $v) {
		    	// 			if(Storage::disk('s3')->exists($v)){
							//  	Storage::disk('s3')->Delete($v);
							// }
		    	// 		}
		    	// 	}	
		    	// 	JobsModel::find($id)->delete();
					return redirect('admin/job')->with('success',Lang::get('message.common.detailAdded', [ 'section' => $this->section ]));
		    	}
		   }
		   else {
                return redirect('admin/job')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }
		    
		}

    
    public function getSkill(){ 
    	
    	$data = Request::all(); 
		$response['status']=0;
		$response['msg']="Something Went Wrong";

		if(isset($data['i_category_id'])){
			$skilldata = SkillsModel::where("i_category_id",$data['i_category_id'])->where("e_status","active")->get();
			
			$optionstr="<option value=''>-select-</option>";
			if(count($skilldata)){
				foreach ($skilldata as $key => $value) {
					if(isset($value->id) && isset($value->v_name)){
						$optionstr .="<option value='".$value->id."'>".$value->v_name."</option>";
					}
				}
			}
			$response['status']=1;
			$response['optionstr']=$optionstr;
			$response['msg']="succesfully list skill list.";
		}
		echo json_encode($response);
		exit;
    }

    public function status($id,$status)
    {
    	if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$jobvalue = JobsModel::find($id);
			$jobvalue->update($data);
		}

		return redirect('admin/job')->with('success', $message);
    }


    public function removeVideo(){

    	$data = Request::all();
		$response = array();
	
		if(!isset($data['i_job_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['i_job_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['v_video_name'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['v_video_name']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$jobdata = JobsModel::find($data['i_job_id']);

		if(!count($jobdata)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}

		$update['v_video']=array();

		if(Storage::disk('s3')->exists($data['v_video_name'])){
			Storage::disk('s3')->Delete($data['v_video_name']);
		}

		if(count($jobdata->v_video)){
			foreach ($jobdata->v_video as $key => $value) {
				if($value!=$data['v_video_name']){
					$update['v_video'][]=$value;					
				}
			}
		}

		$update['d_modified']=date("Y-m-d H:i:s");
		JobsModel::find($data['i_job_id'])->update($update);
		$response['status']=1;
		$response['msg']="succesfully remove video.";
		echo json_encode($response);
		exit;
    }

    public function removeImage(){

		$data = Request::all();
		$response = array();

		if(!isset($data['i_job_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['i_job_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['v_photo_name'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['v_photo_name']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$jobdata = JobsModel::find($data['i_job_id']);

		if(!count($jobdata)){

			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}
		$update['v_photos']=array();

		if(Storage::disk('s3')->exists($data['v_photo_name'])){
			Storage::disk('s3')->Delete($data['v_photo_name']);
		}

		if(count($jobdata->v_photos)){
			foreach ($jobdata->v_photos as $key => $value) {
				if($value!=$data['v_photo_name']){
					$update['v_photos'][]=$value;					
				}
			}
		}

		$update['d_modified']=date("Y-m-d H:i:s");
		JobsModel::find($data['i_job_id'])->update($update);
		$response['status']=1;
		$response['msg']="succesfully remove image.";
		echo json_encode($response);
		exit;

	}



}
?>