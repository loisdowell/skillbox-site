<?php
namespace App\Http\Controllers\Admin\SkillboxJob;

use Request, Lang, File, GeneralHelper,Storage;
use App\Http\Controllers\Controller;
use App\Models\SkillboxJob\SkillboxJobCategory as SkillboxJobCategoryModel;
use App\Models\SkillboxJob\SkillboxJob as SkillboxJobModel;

class SkillboxJob extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "Skillbox Job";
	}

	public function index() {

		$query 	= SkillboxJobModel::query();
		$data 	= $query->orderBy('id','DESC')->get();
		
		$_data	= array(
			'section'=>$this->section,
			'view'	=>"list",
			'data'	=>$data,
		);
		return view('admin/skillbox-job/skillbox-job', $_data);
	}

	public function Add() {

		$category = SkillboxJobCategoryModel::where('e_status', 'active')->orderBy('i_order')->get();

		$_data	= array(
			'section'	=> $this->section,
			'view'		=> "add",
			'category'	=> $category,
		);
		return view('admin/skillbox-job/skillbox-job', $_data);
	
	}
	
	public function Edit($id="") {

		$category = SkillboxJobCategoryModel::where('e_status', 'active')->orderBy('i_order')->get();
	  	
	  	$data = SkillboxJobModel::find($id);

	    $_data = array(
	    	'section' => $this->section,
            'view'	  => "edit",
            'data'	  => $data,
            'category'=> $category,
        );
        return view('admin/skillbox-job/skillbox-job', $_data);
    }

   public function Action($action="",$id="") {

        $post_data = Request::all();
        

        if(isset($post_data['_token'])){
        	unset($post_data['_token']);
        }
      
        if($action=="add"){

        	$existLang = SkillboxJobModel::where('v_title',$post_data['v_title'])->get();
	        if(count($existLang)){
	        	return redirect( 'admin/skillbox-job/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }
        	
       		$post_data['d_added']=date("Y-m-d h:i:s");
       		$post_data['d_modified']=date("Y-m-d h:i:s");
	       	
       	    SkillboxJobModel::create($post_data); 
	        return redirect('admin/skillbox-job')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$existLang = SkillboxJobModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
	        if(count($existLang)){
	        	return redirect( 'admin/skillbox-job/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

			$post_data['d_modified']=date("Y-m-d h:i:s");
            SkillboxJobModel::find($id)->update($post_data);
            return redirect( 'admin/skillbox-job/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = SkillboxJobModel::find($id)->first();

            if(count($_data)){
                SkillboxJobModel::find($id)->delete();
                return redirect( 'admin/skillbox-job')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/skillbox-job')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$course = SkillboxJobModel::find($id);
			$course->update($data);
		}

		return redirect('admin/skillbox-job')->with('success', $message);
	}



}