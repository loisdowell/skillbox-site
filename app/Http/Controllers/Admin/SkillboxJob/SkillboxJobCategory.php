<?php
namespace App\Http\Controllers\Admin\SkillboxJob;

use Request, Lang, File, GeneralHelper,Storage;
use App\Http\Controllers\Controller;
use App\Models\SkillboxJob\SkillboxJobCategory as SkillboxJobCategoryModel;

class SkillboxJobCategory extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "Job Category";
	}

	public function index() {
		
		$query 	= SkillboxJobCategoryModel::query();
		$data 	= $query->orderBy('id','DESC')->get();
		
		$_data	= array(
			'section'=>$this->section,
			'view'	=>"list",
			'data'	=>$data,
		);
		return view('admin/skillbox-job/skillbox-job-category', $_data);
	}

	public function Add() {

		$_data	= array(
			'section'			=>$this->section,
			'view'				=>"add",
		);
		return view('admin/skillbox-job/skillbox-job-category', $_data);
	
	}
	
	public function Edit($id="") {
	  	
	  	$data = SkillboxJobCategoryModel::find($id);

	    $_data = array(
	    	'section'			=>$this->section,
            'view'	 =>"edit",
            'data'	 => $data,
        );
        return view('admin/skillbox-job/skillbox-job-category', $_data);
    }

   public function Action($action="",$id="") {

        $post_data = Request::all();
        

        if(isset($post_data['_token'])){
        	unset($post_data['_token']);
        }
      
        if($action=="add"){

        	$existLang = SkillboxJobCategoryModel::where('v_title',$post_data['v_title'])->get();
	        if(count($existLang)){
	        	return redirect( 'admin/skillbox-job-category/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }
        	
       		$post_data['d_added']=date("Y-m-d h:i:s");
       		$post_data['d_modified']=date("Y-m-d h:i:s");
	       	
       	    SkillboxJobCategoryModel::create($post_data); 
	        return redirect('admin/skillbox-job-category')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$existLang = SkillboxJobCategoryModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
	        if(count($existLang)){
	        	return redirect( 'admin/skillbox-job-category/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

			$post_data['d_modified']=date("Y-m-d h:i:s");
            SkillboxJobCategoryModel::find($id)->update($post_data);
            return redirect( 'admin/skillbox-job-category/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = SkillboxJobCategoryModel::find($id)->first();

            if(count($_data)){
                SkillboxJobCategoryModel::find($id)->delete();
                return redirect( 'admin/skillbox-job-category')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/skillbox-job-category')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$course = SkillboxJobCategoryModel::find($id);
			$course->update($data);
		}

		return redirect('admin/skillbox-job-category')->with('success', $message);
	}



}