<?php
namespace App\Http\Controllers\Admin\Faqs;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Faqs as FaqsModel;

class Faqs extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Faqs";
	}

	public function index() {
		$query = FaqsModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/faqs/faqs', $_data);
	}

	public function Add() {

		$_data=array(
			'view'=>"add",
		);
		return view('admin/faqs/faqs', $_data);
	
	}
	
	public function Edit($id="") {
		
	  $data = FaqsModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/faqs/faqs', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){
       		$post_data['d_added']=date("Y-m-d h:i:s");
       		//$post_data['l_answer'] = nl2br($post_data['l_answer']);
	        unset($post_data['_token']);
	        FaqsModel::create($post_data); 
	        return redirect('admin/faqs')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
       			$post_data['d_modified']=date("Y-m-d h:i:s");
       			//$post_data['l_answer'] = nl2br($post_data['l_answer']);
       		    FaqsModel::find($id)->update($post_data);
	            return redirect( 'admin/faqs/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = FaqsModel::find($id)->first();

            if(count($_data)){
               	
			    FaqsModel::find($id)->delete();
                return redirect( 'admin/faqs')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/faqs')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$faqs = FaqsModel::find($id);
			$faqs->update($data);
		}

		return redirect('admin/faqs')->with('success', $message);
	}



}