<?php
namespace App\Http\Controllers\Admin\Feedback;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Feedback\Feedback as FeedbackModel;

class Feedback extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Feedback";
	}

	public function index() {

		$query = FeedbackModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/feedback/feedback', $_data);
	}

	public function Add() {

		$feedback = FeedbackModel::where("i_parent_id",0)->where("e_status","active")->get();
		$_data=array(
			'view'=>"add",
			'feedback' => $feedback
		);
		return view('admin/feedback/feedback', $_data);
	
	}
	
	public function View($id="") {
		
	  $data = FeedbackModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/feedback/feedback', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

       		$existEmail = FeedbackModel::where('v_name',$post_data['v_name'])->get();
	        if(count($existEmail)){
	        	return redirect( 'admin/feedback/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

          	if( Request::file() ){
			  
				$image = Request::file('v_image');
				$ext   = $image->getClientOriginalExtension();
				
				if( count($image) ){
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						$destination = public_path('uploads/feedback');
						$fileName  = 'category-image-' . rand(1000, 9999) . time() . '.' . $image->getClientOriginalExtension();
						$image->move($destination, $fileName);
						$post_data['v_image'] = $fileName;
					}else{
						return redirect( 'admin/feedback/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					}
				}
			}
			
			$post_data['d_added']=date("Y-m-d h:i:s");
	        $post_data['d_modified']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        
	        
	        FeedbackModel::create($post_data); 
	        return redirect('admin/feedback')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
        		
    			$existName = FeedbackModel::where('v_name',$post_data['v_name'])->where('_id',"!=",$id)->get();
    	  		
    	  		if(count($existName)){
            		return redirect( 'admin/feedback/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
          		}
				  
        	  	if( Request::file() ){
					$image = Request::file('v_image');
					$ext   = $image->getClientOriginalExtension();
					
					if( count($image) ){
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){

							$destination = public_path('uploads/feedback');
							$imageExists = FeedbackModel::where('id',$id)->value('v_image');

							if( isset($imageExists) && $imageExists != '' ) {
								if( File::exists( $destination . '/' . $imageExists ) ) {
									File::delete( $destination . '/' . $imageExists );
								}
							}
							
							$fileName  = 'category-image-' . rand(1000, 9999) . time() . '.' . $image->getClientOriginalExtension();
							$image->move($destination, $fileName);
							$post_data['v_image'] = $fileName;
						}else{
							return redirect( 'admin/feedback/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					}
					}
				
				}

				$post_data['dModify']=date("Y-m-d h:i:s");
	            FeedbackModel::find($id)->update($post_data);
	            return redirect( 'admin/feedback/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = FeedbackModel::find($id);

            if(count($_data)){
               	
               	$destination = public_path('uploads/feedback');
				$imageExists = $_data['v_image'];

				
				if( isset($imageExists) && $imageExists != '' ) {
					if( File::exists( $destination . '/' . $imageExists ) ) {
						 File::delete( $destination . '/' . $imageExists );
					}
				}
			    FeedbackModel::find($id)->delete();
                return redirect( 'admin/feedback')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/feedback')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = FeedbackModel::find($id);
			$country->update($data);
		}

		return redirect('admin/feedback')->with('success', $message);
	}



}