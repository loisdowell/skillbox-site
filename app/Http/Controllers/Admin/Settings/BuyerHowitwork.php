<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Country as CountryModel;
use App\Models\Howitwork as HowitworkModel;




class BuyerHowitwork extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "How its work";
	}

	public function index() {

		$query = HowitworkModel::query();
		$data = $query->where('e_type','buyer')->orderBy('i_order')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/buyerhowitwork', $_data);
	}

	public function Add() {
		
		$_data=array(
			'view'=>"add",
		);
		return view('admin/settings/buyerhowitwork', $_data);
		
	}
	
	public function Edit($id="") {
	  
	  $data = HowitworkModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/settings/buyerhowitwork', $_data);

    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      	unset($post_data['_token']);

        if($action=="add"){

        	$post_data['e_type']="buyer";
          	$post_data['d_added']=date("Y-m-d h:i:s");
            $post_data['d_modified']=date("Y-m-d h:i:s");
	        HowitworkModel::create($post_data); 
	        return redirect('admin/buyer-how-its-work')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$post_data['d_modified']=date("Y-m-d h:i:s");
            HowitworkModel::find($id)->update($post_data);
            return redirect( 'admin/buyer-how-its-work/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
   
        }else if($action=="delete"){
        	$_data = HowitworkModel::find($id)->first();
         	if(count($_data)){
			    HowitworkModel::find($id)->delete();
                return redirect( 'admin/buyer-how-its-work')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/buyer-how-its-work')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = HowitworkModel::find($id);
			$country->update($data);
		}

		return redirect('admin/buyer-how-its-work')->with('success', $message);
	}



}