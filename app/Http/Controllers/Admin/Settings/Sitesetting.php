<?php

namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, DB, File, GeneralHelper,Storage;

use App\Http\Controllers\Controller;
use App\Models\Sitesettings as SitesettingsModel;

class Sitesetting extends Controller {
    protected $section;

    public function __construct(){
        $this->section = Lang::get( 'Sitesetting' );
    }

    public function index(){
        $data = [];
        $sitesetting = SitesettingsModel::all();
        // $fb_data = Facebook::get('/crestwebtech/feed', '174980089314320|85c5f18817ee4a74f19f79cecb6d7972');
        foreach ($sitesetting as $key => $value ) {
            if ($value['v_key'] == 'UPLOAD_FILE_TYPES') {
                $data[$value['v_key']] = json_decode($value['v_value']);
            }
            else {
                $data[$value['v_key']]=$value['v_value'];
            }
        }   
        return view('admin/settings/sitesetting', compact( 'data' ) )->with(['section'=>$this->section]);
    }

    public function store(){

        $post_data = Request::all();
     //    print_r($post_data); exit;
        unset($post_data['_token']);

        ##  Logo Upload
        if( Request::file() ) {

            
            $logo = Request::file('SITE_LOGO');
            if($logo){
                $imageExists = SitesettingsModel::where('v_key','SITE_LOGO')->value('v_value');
                if(Storage::disk('s3')->exists($imageExists)){
                    Storage::disk('s3')->Delete($imageExists);
                }
                $fileName = 'site-'.time().'.'.$logo->getClientOriginalExtension();
                $fileName = str_replace(' ', '_', $fileName);
                $post_data['SITE_LOGO'] = 'common/' . $fileName;
                $uploadS3 = Storage::disk('s3')->put('/common/'.$fileName, File::get((string)$logo), 'public');
            }

            $logo = Request::file('HOMEPAGE_IMAGE_1');
            if($logo){
                $imageExists = SitesettingsModel::where('v_key','HOMEPAGE_IMAGE_1')->value('v_value');
                if(Storage::disk('s3')->exists($imageExists)){
                    Storage::disk('s3')->Delete($imageExists);
                }
                $fileName = 'site-'.time().'.'.$logo->getClientOriginalExtension();
                $fileName = str_replace(' ', '_', $fileName);
                $post_data['HOMEPAGE_IMAGE_1'] = 'common/' . $fileName;
                $uploadS3 = Storage::disk('s3')->put('/common/'.$fileName, File::get((string)$logo), 'public');
            }

            $logo = Request::file('HOMEPAGE_IMAGE_2');
            if($logo){
                $imageExists = SitesettingsModel::where('v_key','HOMEPAGE_IMAGE_2')->value('v_value');
                if(Storage::disk('s3')->exists($imageExists)){
                    Storage::disk('s3')->Delete($imageExists);
                }
                $fileName = 'site-'.time().'.'.$logo->getClientOriginalExtension();
                $fileName = str_replace(' ', '_', $fileName);
                $post_data['HOMEPAGE_IMAGE_2'] = 'common/' . $fileName;
                $uploadS3 = Storage::disk('s3')->put('/common/'.$fileName, File::get((string)$logo), 'public');
            }

            $logo = Request::file('HOMEPAGE_IMAGE_3');
            if($logo){
                $imageExists = SitesettingsModel::where('v_key','HOMEPAGE_IMAGE_3')->value('v_value');
                if(Storage::disk('s3')->exists($imageExists)){
                    Storage::disk('s3')->Delete($imageExists);
                }
                $fileName = 'site-'.time().'.'.$logo->getClientOriginalExtension();
                $fileName = str_replace(' ', '_', $fileName);
                $post_data['HOMEPAGE_IMAGE_3'] = 'common/' . $fileName;
                $uploadS3 = Storage::disk('s3')->put('/common/'.$fileName, File::get((string)$logo), 'public');
            }

            $logo = Request::file('HOMEPAGE_IMAGE_4');
            if($logo){
                $imageExists = SitesettingsModel::where('v_key','HOMEPAGE_IMAGE_4')->value('v_value');
                if(Storage::disk('s3')->exists($imageExists)){
                    Storage::disk('s3')->Delete($imageExists);
                }
                $fileName = 'site-'.time().'.'.$logo->getClientOriginalExtension();
                $fileName = str_replace(' ', '_', $fileName);
                $post_data['HOMEPAGE_IMAGE_4'] = 'common/' . $fileName;
                $uploadS3 = Storage::disk('s3')->put('/common/'.$fileName, File::get((string)$logo), 'public');
            }

            $logo = Request::file('HOMEPAGE_IMAGE_5');
            if($logo){
                $imageExists = SitesettingsModel::where('v_key','HOMEPAGE_IMAGE_5')->value('v_value');
                if(Storage::disk('s3')->exists($imageExists)){
                    Storage::disk('s3')->Delete($imageExists);
                }
                $fileName = 'site-'.time().'.'.$logo->getClientOriginalExtension();
                $fileName = str_replace(' ', '_', $fileName);
                $post_data['HOMEPAGE_IMAGE_5'] = 'common/' . $fileName;
                $uploadS3 = Storage::disk('s3')->put('/common/'.$fileName, File::get((string)$logo), 'public');
            }

            $logo = Request::file('I_WANT_BUY_IMAGE');
            if($logo){
                $imageExists = SitesettingsModel::where('v_key','I_WANT_BUY_IMAGE')->value('v_value');
                if(Storage::disk('s3')->exists($imageExists)){
                    Storage::disk('s3')->Delete($imageExists);
                }
                $fileName = 'site-'.time().'.'.$logo->getClientOriginalExtension();
                $fileName = str_replace(' ', '_', $fileName);
                $post_data['I_WANT_BUY_IMAGE'] = 'common/' . $fileName;
                $uploadS3 = Storage::disk('s3')->put('/common/'.$fileName, File::get((string)$logo), 'public');
                $imgdata = \Storage::cloud()->url($post_data['I_WANT_BUY_IMAGE']);
            }

            $logo = Request::file('I_WANT_SELL_IMAGE');
            if($logo){
                $imageExists = SitesettingsModel::where('v_key','I_WANT_SELL_IMAGE')->value('v_value');
                if(Storage::disk('s3')->exists($imageExists)){
                    Storage::disk('s3')->Delete($imageExists);
                }

                $fileName = 'site-'.time().'.'.$logo->getClientOriginalExtension();
                $fileName = str_replace(' ', '_', $fileName);
                $post_data['I_WANT_SELL_IMAGE'] = 'common/' . $fileName;
                
                $uploadS3 = Storage::disk('s3')->put('/common/'.$fileName, File::get((string)$logo), 'public');

            }



            $invoiceLogo = Request::file('INVOICE_LOGO');
            if ($invoiceLogo) {

                $imageExists = SitesettingsModel::where('v_key','INVOICE_LOGO')->value('v_value');

                if(Storage::disk('s3')->exists($imageExists)){
                    Storage::disk('s3')->Delete($imageExists);
                }

                $fileName = 'site-'.time().'.'.$invoiceLogo->getClientOriginalExtension();
                $fileName = str_replace(' ', '_', $fileName);
                $post_data['INVOICE_LOGO'] = 'common/' . $fileName;
                $uploadS3 = Storage::disk('s3')->put('/common/'.$fileName, File::get((string)$invoiceLogo), 'public');

                // $fileName = time() . '-invoice-logo-' . $invoiceLogo->getClientOriginalName();
                // $fileName = str_replace(' ', '_', $fileName);
                // $path = public_path('uploads/settings');
                // $invoiceLogo->move($path, $fileName);
                // $post_data['INVOICE_LOGO'] = $fileName;
            }
        }
        #end

        
        //dd($post_data);
        foreach ($post_data as $key => $value) {
            
            $siteval = DB::table('tbl_sitesettings')->where('v_key', '=', $key)->get();
            // echo '<pre>';
            // print_r($siteval);
            // exit;
            if(count($siteval)){
          //      $siteval = json_decode(json_encode($siteval), True);
                $siteval=$siteval[0];
                $update=array(
                    'v_value'=>$value
                );
                SitesettingsModel::find($siteval['_id'])->update($update);
            }else{
                $insert=array(
                    'v_key'=>$key,
                    'v_value'=>$value,
                );
                SitesettingsModel::create($insert);
            }
        }
        
       

        return redirect( 'admin/sitesetting')->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
    }   
}