<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, GeneralHelper;

use App\Models\EmailTemplate as EmailTemplateModel;
use App\Models\EmailTemplateHeader as EmailTemplateHeaderModel;
use App\Models\EmailTemplateFooter as EmailTemplateFooterModel;

use App\Http\Controllers\Controller;

class EmailTemplate extends Controller {

	protected $section;

	public function __construct(){

		$this->section = 'EmailTemplate';
	}

	public function index() {

		$query = EmailTemplateModel::query();
		// $data = $query->orderBy('id','DESC')->paginate($this->records_per_page);
		$data = $query->where('i_delete','!=',"1")->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/email-template', $_data);

	}

	public function Add() {
		
		$TemplateHeader = EmailTemplateHeaderModel::where('e_status','active')->get();
		$TemplateFooter = EmailTemplateFooterModel::where('e_status','active')->get();

		$_data=array(
			'view'=>"add",
			'TemplateHeader'=>$TemplateHeader,
			'TemplateFooter'=>$TemplateFooter			
		);
		
		return view('admin/settings/email-template', $_data);
	}

	public function edit($id) {
		
		$TemplateHeader = EmailTemplateHeaderModel::where('e_status','active')->get();
		$TemplateFooter = EmailTemplateFooterModel::where('e_status','active')->get();
		$data = EmailTemplateModel::where("_id",$id)->first();

		$_data=array(
            'view'=>"edit",
            'data'=>$data,
			'TemplateHeader'=>$TemplateHeader,
			'TemplateFooter'=>$TemplateFooter			
        );
        return view('admin/settings/email-template', $_data);

	}

	public function Action($action="",$id="") {

        $post_data = Request::all();

        if($action=="add"){

        		$data = Request::all();

				$data['d_added'] = date('Y-m-d H:i:s');
				$data['d_modified'] = date('Y-m-d H:i:s');

				if( isset($data) && count($data) ) {
					EmailTemplateModel::create($data);
				}
				unset($post_data['_token']);
				
				return redirect( 'admin/email-template')->with( 'success', Lang::get('message.common.detailAdded', [ 'section' => $this->section ])); 
		      	

        }else if($action=="edit"){

        		$record = EmailTemplateModel::find($id);
             	
            	$post_data['d_modified']=date("Y-m-d h:i:s");
	            EmailTemplateModel::find($id)->update($post_data);
	            return redirect( 'admin/email-template/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 

       
        }else if($action=="delete"){

            $_data = EmailTemplateModel::where('_id',$id)->first();
            if(count($_data)){
            	$update['i_delete']="1";
                EmailTemplateModel::find($id)->update($update);
                return redirect( 'admin/email-template')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/email-template')->with( 'warning', Lang::get('message.somethingWrong')); 
            }

        }
          
    }	

	
	public function status( $id, $status ) {
			
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusInactive', ['section' => $this->section]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', ['section' => $this->section]);
		}
		
		if ( isset($data) && count($data) ) {

			$record = EmailTemplateModel::find($id);
			$record->update($data);
		}

		return redirect('admin/email-template')->with('success', $message);
	}

}