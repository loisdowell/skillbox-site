<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper,Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;

class Categories extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Categories";
	}

	public function index() {
		
		$query = CategoriesModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/categories', $_data);
	}

	public function Add() {

		$categories = CategoriesModel::where("i_parent_id",0)->where("e_status","active")->get();
		$_data=array(
			'view'=>"add",
			'categories' => $categories
		);
		return view('admin/settings/categories', $_data);
	
	}
	
	public function Edit($id="") {
		
	  $categories = CategoriesModel::where("i_parent_id",0)->where("e_status","active")->get();
	  $data = CategoriesModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
            'categories' => $categories
        );
        return view('admin/settings/categories', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

       		$existEmail = CategoriesModel::where('v_name',$post_data['v_name'])->get();
	        if(count($existEmail)){
	        	return redirect( 'admin/categories/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

          	
          	if(Request::file()){
			  
				$image = Request::file('v_image');
				$imageMobile = Request::file('v_image_mobile');
				$mainImage = Request::file('v_main_image');
				
				if(count($image) ){
					
					$ext   = $image->getClientOriginalExtension();
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						
						$fileName = 'categories-'.time().'.'.$image->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_image'] = 'categories/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/categories/'.$fileName, File::get((string)$image), 'public');

					}else{
						return redirect( 'admin/categories/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					
					}
				}

				if(count($imageMobile) ){
					
					$ext   = $imageMobile->getClientOriginalExtension();
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						
						$fileName = 'categories-'.time().'.'.$imageMobile->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_image_mobile'] = 'categories/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/categories/'.$fileName, File::get((string)$imageMobile), 'public');

					}else{
						return redirect( 'admin/categories/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					
					}
				}else{
					if(isset($post_data['v_image_mobile'])){
						unset($post_data['v_image_mobile']);
					}
				}

				if(count($mainImage) ){

					$ext   = $image->getClientOriginalExtension();
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						
						$fileName = 'categories-main-'.time().'.'.$mainImage->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_main_image'] = 'categories/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/categories/'.$fileName, File::get((string)$mainImage), 'public');

					}else{
						return redirect( 'admin/categories/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					
					}
				}
			}
			
			$post_data['d_added']=date("Y-m-d h:i:s");
	        $post_data['d_modified']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        
	        
	        CategoriesModel::create($post_data); 
	        return redirect('admin/categories')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
        		
    			$existName = CategoriesModel::where('v_name',$post_data['v_name'])->where('_id',"!=",$id)->get();
    	  		
    	  		if(count($existName)){
            		return redirect( 'admin/categories/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
				}
				  
				if(Request::file('v_image_mobile') ){
					
					$v_image_mobile = Request::file('v_image_mobile');
					if( count($v_image_mobile) ){
					
						$ext   = $v_image_mobile->getClientOriginalExtension();
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
							$imageExists = CategoriesModel::where('id',$id)->value('v_image_mobile');
							if(Storage::disk('s3')->exists($imageExists)){
								Storage::disk('s3')->Delete($imageExists);
							}
							$fileName = 'categories-'.time().'.'.$v_image_mobile->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_image_mobile'] = 'categories/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/categories/'.$fileName, File::get((string)$v_image_mobile), 'public');

						}else{
							return redirect( 'admin/categories/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}
				}else{
					if(isset($post_data['v_image_mobile'])){
						unset($post_data['v_image_mobile']);
					}
				}
				  
        	  	if( Request::file() ){
					
					$image = Request::file('v_image');
				
					
					if( count($image) ){
					
						$ext   = $image->getClientOriginalExtension();
						
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){

							$imageExists = CategoriesModel::where('id',$id)->value('v_image');

							if(Storage::disk('s3')->exists($imageExists)){
								Storage::disk('s3')->Delete($imageExists);
							}

							$fileName = 'categories-'.time().'.'.$image->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_image'] = 'categories/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/categories/'.$fileName, File::get((string)$image), 'public');

						}else{
							return redirect( 'admin/categories/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}
				}
				if( Request::file() ){
					$mainImage = Request::file('v_main_image');
					
					if( count($mainImage) ){
						
						$mainImageExt   = $mainImage->getClientOriginalExtension();
						
						if($mainImageExt == 'jpg' || $mainImageExt == 'png' || $mainImageExt == 'jpeg'){

							$mainImageExists = CategoriesModel::where('id',$id)->value('v_main_image');

							if(Storage::disk('s3')->exists($mainImageExists)){
								Storage::disk('s3')->Delete($mainImageExists);
							}

							$fileName = 'categories-main-'.time().'.'.$mainImage->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_main_image'] = 'categories/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/categories/'.$fileName, File::get((string)$mainImage), 'public');

						}else{
							return redirect( 'admin/categories/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}
				
				}

				$post_data['dModify']=date("Y-m-d h:i:s");
	            CategoriesModel::find($id)->update($post_data);
	            return redirect( 'admin/categories/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = CategoriesModel::find($id);

            if(count($_data)){
               	
               	$destination = public_path('uploads/categories');
				$imageExists = $_data['v_profile'];

				if( isset($imageExists) && $imageExists != '' ) {
					if( File::exists( $destination . '/' . $imageExists ) ) {
						 File::delete( $destination . '/' . $imageExists );
					}
				}

				$_data->delete();
                return redirect( 'admin/categories')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/categories')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = CategoriesModel::find($id);
			$country->update($data);
		}

		return redirect('admin/categories')->with('success', $message);
	}



}