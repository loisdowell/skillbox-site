<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\State as StateModel;
use App\Models\City as CityModel;

class City extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "City";
	}

	public function index() {

		$query = CityModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/city', $_data);
	}

	public function Add() {

		$state = StateModel::where("e_status","active")->get();;
		$_data=array(
			'view'=>"add",
			'state' => $state
		);
		return view('admin/settings/city', $_data);
		
	}
	
	public function Edit($id="") {
		
	  $state = StateModel::get();
	  $data = CityModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
            'state' => $state
        );
        return view('admin/settings/city', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

        	$existCity = CityModel::where('v_title',$post_data['v_title'])->get();
	        if(count($existCity)){
	        	return redirect( 'admin/city/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

          	$post_data['d_added']=date("Y-m-d h:i:s");
	        $post_data['d_modified']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        CityModel::create($post_data); 
	        return redirect('admin/city')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$existCity = CityModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
    	  		
    	  		if(count($existCity)){
            		return redirect( 'admin/city/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
          		}

    		$post_data['d_modified']=date("Y-m-d h:i:s");
            CityModel::find($id)->update($post_data);
            return redirect( 'admin/city/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
   
        }else if($action=="delete"){
        	$_data = CityModel::find($id)->first();
         	if(count($_data)){
			    CityModel::find($id)->delete();
                return redirect( 'admin/city')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/city')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$city = CityModel::find($id);
			$city->update($data);
		}

		return redirect('admin/city')->with('success', $message);
	}



}