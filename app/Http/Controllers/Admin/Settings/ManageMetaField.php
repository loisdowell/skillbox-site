<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper,DB,Storage;
use App\Models\ManageMetaField as ManageMetaFieldModel;

use App\Http\Controllers\Controller;

class ManageMetaField extends Controller {

	protected $section;

	public function __construct(){
		
		$this->section = 'Meta';
	}

	public function index() {
		
		$data = ManageMetaFieldModel::get();
	   	$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		
		return view('admin/settings/header-image-meta', $_data);
	}

	public function Add() {

      	$_data=array(
			'view'=>"add",		
		);
		return view('admin/settings/header-image-meta', $_data);
	
	}
	
	public function Edit($id="") {

        $data = ManageMetaFieldModel::find($id);
        
        $_data=array(
            'view'=>"edit",
            'data'=>$data
        );
        return view('admin/settings/header-image-meta', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();

        if($action=="add"){

	      	$existEmail = ManageMetaFieldModel::where('v_title',$post_data['v_title'])->get();

          	if(count($existEmail)){
            	return redirect( 'admin/manage-meta-field/add/0')->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section ])); 
          	}

          	if(Request::file()){
			  
				$image = Request::file('v_image');
				$ext   = $image->getClientOriginalExtension();
				
				if(count($image) ){
					
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						
						$fileName = 'meta-image-'.time().'.'.$image->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_image'] = 'meta/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/meta/'.$fileName, File::get((string)$image), 'public');

					}else{
						return redirect( 'admin/manage-meta-field/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					
					}
				}
			}

           	unset($post_data['_token']);
            
            $post_data['d_added'] = date('Y-m-d H:i:s');
            $post_data['d_modified'] = date('Y-m-d H:i:s');
            $post_data['e_status'] = 'active';

            ManageMetaFieldModel::create($post_data);  
        	return redirect( 'admin/manage-meta-field')->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ])); 

        }else if($action=="edit"){

        		$existEmail = ManageMetaFieldModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
	          	if(count($existEmail)){
	            	return redirect( 'admin/manage-meta-field/edit/'.$id)->with( 'warning', Lang::get('message.keyExist', [ 'section' => 'City Title' ])); 
	          	}
             	
				if( Request::file() ){
					
					$image = Request::file('v_image');
					$ext   = $image->getClientOriginalExtension();
					
					if( count($image) ){
						
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){

							$imageExists = ManageMetaFieldModel::find($id)->value('v_image');

							if(Storage::disk('s3')->exists($imageExists)){
								Storage::disk('s3')->Delete($imageExists);
							}

							$fileName = 'meta-image-'.time().'.'.$image->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_image'] = 'meta/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/meta/'.$fileName, File::get((string)$image), 'public');

						}else{
							return redirect('admin/manage-meta-field/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}
				}
            	$post_data['d_modified']=date("Y-m-d H:i:s");
	            ManageMetaFieldModel::find($id)->update($post_data);
	            return redirect( 'admin/manage-meta-field/edit/'.$id)->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
            
            $_data = ManageMetaFieldModel::find($id);
        	
            if(count($_data)){

            	$destination = public_path('uploads/meta');
				$imageExists = $_data['v_image'];
				
				if( isset($imageExists) && $imageExists != '' ) {
					if(Storage::disk('s3')->exists($imageExists)){
						Storage::disk('s3')->Delete($imageExists);
					}
				}
			    ManageMetaFieldModel::find($id)->delete();
                return redirect( 'admin/manage-meta-field')->with( 'success', Lang::get('message.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/manage-meta-field')->with( 'warning', Lang::get('message.somethingWrong')); 
            }

        }
          
    }

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = ManageMetaFieldModel::find($id);
			$country->update($data);
		}

		return redirect('admin/manage-meta-field')->with('success', $message);
	}


}