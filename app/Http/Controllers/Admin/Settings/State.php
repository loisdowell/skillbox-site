<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Country as CountryModel;
use App\Models\State as StateModel;



class State extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "State";
	}

	public function index() {

		$query = StateModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/state', $_data);
	}

	public function Add() {

		$country = CountryModel::where("e_status","active")->get();
		$_data=array(
			'view'=>"add",
			'country' => $country
		);
		return view('admin/settings/state', $_data);
		
	}
	
	public function Edit($id="") {
		
	  $country = CountryModel::get();
	  $data = StateModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
            'country' => $country
        );
        return view('admin/settings/state', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

        	$existState = StateModel::where('v_title',$post_data['v_title'])->get();
	        if(count($existState)){
	        	return redirect( 'admin/state/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

          	$post_data['d_added']=date("Y-m-d h:i:s");
	        $post_data['d_modified']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        StateModel::create($post_data); 
	        return redirect('admin/state')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$existState = StateModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
    	  		
    	  	if(count($existState)){
        		return redirect( 'admin/state/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
          	}
    		$post_data['d_modified']=date("Y-m-d h:i:s");
            StateModel::find($id)->update($post_data);
            return redirect( 'admin/state/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
   
        }else if($action=="delete"){
        	$_data = StateModel::find($id)->first();
         	if(count($_data)){
			    StateModel::find($id)->delete();
                return redirect( 'admin/state')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/state')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$state = StateModel::find($id);
			$state->update($data);
		}

		return redirect('admin/state')->with('success', $message);
	}



}