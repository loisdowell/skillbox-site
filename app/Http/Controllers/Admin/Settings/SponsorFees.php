<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\SponsorFees as SponsorFeesModel;


class SponsorFees extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "SponsorFees";
	}

	public function index() {

		$query = SponsorFeesModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/sponsorfees', $_data);
	}

	public function Add() {
	
		$_data=array(
			'view'=>"add",
		);
		return view('admin/settings/sponsorfees', $_data);
		
	}
	
	public function Edit($id="") {
		
	  $data = SponsorFeesModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/settings/sponsorfees', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

          	$post_data['d_added']=date("Y-m-d h:i:s");
	        $post_data['d_modified']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        SponsorFeesModel::create($post_data); 
	        return redirect('admin/sponsorfees')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
        
    		$post_data['d_modified']=date("Y-m-d h:i:s");
            SponsorFeesModel::find($id)->update($post_data);
            return redirect( 'admin/sponsorfees/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
   
        }else if($action=="delete"){
        	$_data = SponsorFeesModel::find($id)->first();
         	if(count($_data)){
			    SponsorFeesModel::find($id)->delete();
                return redirect( 'admin/sponsorfees')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/sponsorfees')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$fees = SponsorFeesModel::find($id);
			$fees->update($data);
		}

		return redirect('admin/sponsorfees')->with('success', $message);
	}



}