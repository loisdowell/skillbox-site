<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper,Storage;
use App\Http\Controllers\Controller;
use App\Models\Skills as SkillsModel;
use App\Models\Levels as LevelsModel;


class Levels extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "Levels";
	}

	public function index() {

		$query = LevelsModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/levels', $_data);
	}

	public function Add() {

		$skills = SkillsModel::where("e_status","active")->get();
		$_data=array(
			'view'=>"add",
			'skills' => $skills
		);
		return view('admin/settings/levels', $_data);
	
	}
	
	public function Edit($id="") {
		
	  $skills = SkillsModel::get();
	  $data = LevelsModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
            'skills' => $skills
        );
        return view('admin/settings/levels', $_data);
    }

    public function Action($action="",$id="") {
        $post_data = Request::all();
        if($action=="add"){
			

			if(Request::file()){
			 	$image = Request::file('v_icon');
				if(count($image) ){
					$ext   = $image->getClientOriginalExtension();
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						$fileName = 'categories-'.time().'.'.$image->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_icon'] = 'level/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/level/'.$fileName, File::get((string)$image), 'public');
					}else{
						return redirect( 'admin/levels/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					}
				}
			}

			$post_data['d_added']=date("Y-m-d h:i:s");
	        $post_data['d_modified']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        LevelsModel::create($post_data); 
	        return redirect('admin/levels')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	if(Request::file()){
			 	$image = Request::file('v_icon');
				if(count($image) ){
					$ext   = $image->getClientOriginalExtension();
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						$fileName = 'categories-'.time().'.'.$image->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_icon'] = 'level/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/level/'.$fileName, File::get((string)$image), 'public');
					}else{
						return redirect( 'admin/levels/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					}
				}
			}
			$post_data['d_modified']=date("Y-m-d h:i:s");
	        LevelsModel::find($id)->update($post_data);
	        return redirect( 'admin/levels/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 

        }else if($action=="delete"){
        	
        	$_data = LevelsModel::find($id)->first();

            if(count($_data)){
               	
			    LevelsModel::find($id)->delete();
                return redirect( 'admin/levels')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/levels')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }


}