<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Skills as SkillsModel;
use App\Models\Categories as CategoriesModel;



class Skills extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Skills";
	}

	public function index() {

		$query = SkillsModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/skills', $_data);
	}

	public function Add() {

		$categories = CategoriesModel::where("e_status","active")->get();
		$_data=array(
			'view'=>"add",
			'categories' => $categories
		);
		return view('admin/settings/skills', $_data);
		
	}
	
	public function Edit($id="") {
		
	  $categories = CategoriesModel::get();
	  $data = SkillsModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
            'categories' => $categories
        );
        return view('admin/settings/skills', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){
          	$post_data['d_added']=date("Y-m-d h:i:s");
	        $post_data['d_modified']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        SkillsModel::create($post_data); 
	        return redirect('admin/skills')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
    		$post_data['d_modified']=date("Y-m-d h:i:s");
            SkillsModel::find($id)->update($post_data);
            return redirect( 'admin/skills/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
   
        }else if($action=="delete"){
        	$_data = SkillsModel::find($id)->first();
         	if(count($_data)){
			    SkillsModel::find($id)->delete();
                return redirect( 'admin/skills')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/skills')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = SkillsModel::find($id);
			$country->update($data);
		}

		return redirect('admin/skills')->with('success', $message);
	}



}