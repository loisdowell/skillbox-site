<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Country as CountryModel;
use App\Models\Howitwork as HowitworkModel;
use App\Models\Guide as GuideModel;

class Guide extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "Homepage How to Guide Managment";
	}	

	public function index() {

		$query = GuideModel::query();
		$data = $query->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/guide', $_data);
	}

	public function Add() {
		
		$_data=array(
			'view'=>"add",
		);
		return view('admin/settings/guide', $_data);
		
	}
	
	public function Edit($id="") {
	  
	  $data = GuideModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/settings/guide', $_data);

    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      	unset($post_data['_token']);

        if($action=="add"){
          	$post_data['d_added']=date("Y-m-d h:i:s");
            $post_data['d_modified']=date("Y-m-d h:i:s");
	        GuideModel::create($post_data); 
	        return redirect('admin/guide')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

	    }else if($action=="edit"){

        	$post_data['d_modified']=date("Y-m-d h:i:s");
            GuideModel::find($id)->update($post_data);
            return redirect( 'admin/guide/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
   
        }else if($action=="delete"){
        	$_data = GuideModel::find($id)->first();
         	if(count($_data)){
			    GuideModel::find($id)->delete();
                return redirect( 'admin/guide')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/guide')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }

}