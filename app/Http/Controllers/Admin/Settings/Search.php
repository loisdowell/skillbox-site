<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Search as SearchModel;



class Search extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Search";
	}

	public function index() {

		$query = SearchModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/search', $_data);
	}

	public function Add() {

		$_data=array(
			'view'=>"add",
		);
		return view('admin/settings/search', $_data);
		
	}
	
	public function Edit($id="") {
		
	  $data = SearchModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/settings/search', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

          	$post_data['d_added']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        SearchModel::create($post_data); 
	        return redirect('admin/search')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

    		$post_data['d_modified']=date("Y-m-d h:i:s");
            SearchModel::find($id)->update($post_data);
            return redirect( 'admin/search/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
   
        }else if($action=="delete"){
        	$_data = SearchModel::find($id)->first();
         	if(count($_data)){
			    SearchModel::find($id)->delete();
                return redirect( 'admin/search')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/search')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	


}