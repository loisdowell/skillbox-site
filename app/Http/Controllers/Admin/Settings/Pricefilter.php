<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Pricefilter as PricefilterModel;


class Pricefilter extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Price filter";
	}

	public function index() {

		$query = PricefilterModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
			'section'=>$this->section
		);
		return view('admin/settings/price-filter', $_data);
	}

	public function Add() {

		$_data=array(
			'view'=>"add",
			'section'=>$this->section
		);
		return view('admin/settings/price-filter', $_data);
	
	}
	
	public function Edit($id="") {
		
	  $data = PricefilterModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
            'section'=>$this->section
        );
        return view('admin/settings/price-filter', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){
			$post_data['d_added']=date("Y-m-d h:i:s");
	        $post_data['d_modified']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        PricefilterModel::create($post_data); 
	        return redirect('admin/price-filter')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
        
			$post_data['d_modified']=date("Y-m-d h:i:s");
	        PricefilterModel::find($id)->update($post_data);
	        return redirect( 'admin/price-filter/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 

        }else if($action=="delete"){
        	
        	$_data = PricefilterModel::find($id)->first();

            if(count($_data)){
               	
			    PricefilterModel::find($id)->delete();
                return redirect( 'admin/price-filter')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/price-filter')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }


}