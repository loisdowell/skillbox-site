<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper,Storage;
use App\Http\Controllers\Controller;
use App\Models\CompanyEthos as CompanyEthosModel;

class CompanyEthos extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Company Ethos";
	}

	public function index() {

		$query = CompanyEthosModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/company-ethos', $_data);
	}

	public function Add() {

		$_data=array(
			'view'=>"add",
		);
		return view('admin/settings/company-ethos', $_data);
	
	}
	
	public function Edit($id="") {
		
	  $data = CompanyEthosModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/settings/company-ethos', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

       		$existEmail = CompanyEthosModel::where('v_title',$post_data['v_title'])->get();
	        if(count($existEmail)){
	        	return redirect( 'admin/company-ethos/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

          	
          	if(Request::file()){
			  
				$image = Request::file('v_image');
				$ext   = $image->getClientOriginalExtension();
				
				if(count($image) ){
					
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						
						$fileName = 'company-ethos-'.time().'.'.$image->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_image'] = 'company-ethos/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/company-ethos/'.$fileName, File::get((string)$image), 'public');

					}else{
						return redirect( 'admin/company-ethos/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					
					}
				}
			}
			
			$post_data['d_added']=date("Y-m-d h:i:s");
	        $post_data['d_modified']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        
	        
	        CompanyEthosModel::create($post_data); 
	        return redirect('admin/company-ethos')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
        		
    			$existName = CompanyEthosModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
    	  		
    	  		if(count($existName)){
            		return redirect( 'admin/company-ethos/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
          		}
				  
        	  	if( Request::file() ){
					
					$image = Request::file('v_image');
					$ext   = $image->getClientOriginalExtension();
					
					if( count($image) ){
						
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){

							$imageExists = CompanyEthosModel::where('id',$id)->value('v_image');

							if(Storage::disk('s3')->exists($imageExists)){
								Storage::disk('s3')->Delete($imageExists);
							}

							$fileName = 'company-ethos-'.time().'.'.$image->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_image'] = 'company-ethos/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/company-ethos/'.$fileName, File::get((string)$image), 'public');

						}else{
							return redirect( 'admin/company-ethos/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					}
					}
				
				}

				$post_data['dModify']=date("Y-m-d h:i:s");
	            CompanyEthosModel::find($id)->update($post_data);
	            return redirect( 'admin/company-ethos/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = CompanyEthosModel::find($id);

            if(count($_data)){
               	
               	$destination = public_path('uploads/company-ethos');
				$imageExists = $_data['v_image'];

				
				if( isset($imageExists) && $imageExists != '' ) {
					if( File::exists( $destination . '/' . $imageExists ) ) {
						 File::delete( $destination . '/' . $imageExists );
					}
				}
			    CompanyEthosModel::find($id)->delete();
                return redirect( 'admin/company-ethos')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/company-ethos')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = CompanyEthosModel::find($id);
			$country->update($data);
		}

		return redirect('admin/company-ethos')->with('success', $message);
	}



}