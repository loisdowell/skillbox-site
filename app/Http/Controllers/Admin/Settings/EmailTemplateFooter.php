<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, GeneralHelper;

use App\Models\EmailTemplateFooter as EmailTemplateFooterModel;

use App\Http\Controllers\Controller;

class EmailTemplateFooter extends Controller {

	protected $section;

	public function __construct(){

		$this->section = 'EmailTemplateFooter';
	}


	public function index() {

		$query = EmailTemplateFooterModel::query();
		// $data = $query->orderBy('id','DESC')->paginate($this->records_per_page);
		$data = $query->orderBy('id','DESC')->get();
		
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/email-template-footer', $_data);

	}

	public function Add() {
		
		$_data=array(
			'view'=>"add",
		);
		return view('admin/settings/email-template-footer', $_data);
	}

	public function edit($id) {
		
		$data = EmailTemplateFooterModel::where("_id",$id)->first();

		$_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/settings/email-template-footer', $_data);

	}

	public function Action($action="",$id="") {

        $post_data = Request::all();
        
        if($action=="add"){

        		$data = Request::all();

				$data['d_added'] = date('Y-m-d H:i:s');
				$data['d_modified'] = date('Y-m-d H:i:s');

				if( isset($data) && count($data) ) {
					EmailTemplateFooterModel::create($data);
				}
				unset($post_data['_token']);
				
				return redirect( 'admin/email-template-footer')->with( 'success', Lang::get('message.common.detailAdded', [ 'section' => $this->section ])); 
		      	

        }else if($action=="edit"){

        		$record = EmailTemplateFooterModel::find($id);
             	
            	$post_data['d_modified']=date("Y-m-d h:i:s");
	            EmailTemplateFooterModel::find($id)->update($post_data);
	            return redirect( 'admin/email-template-footer/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 

       
        }else if($action=="delete"){

            $_data = EmailTemplateFooterModel::where('_id',$id)->first();
            if(count($_data)){
                EmailTemplateFooterModel::find($id)->delete();
                return redirect( 'admin/email-template-footer')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/email-template-footer')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }	

	
	public function status( $id, $status ) {
			
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusInactive', ['section' => $this->section]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', ['section' => $this->section]);
		}
		
		if ( isset($data) && count($data) ) {

			$record = EmailTemplateFooterModel::find($id);
			$record->update($data);
		}

		return redirect('admin/email-template-footer')->with('success', $message);
	}

}