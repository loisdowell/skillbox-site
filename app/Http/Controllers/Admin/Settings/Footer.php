<?php
namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Country as CountryModel;
use App\Models\Footer as FooterModel;
use App\Models\Pages\Pages as PagesModel;



class Footer extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "Footer Menu";
	}
	
	public function index() {

		$query = FooterModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/settings/footer', $_data);
	}
	
	public function Add() {
		
		$page = PagesModel::where("e_status","active")->get();
		$_data=array(
			'view'=>"add",
			'page'=>$page,
		);
		return view('admin/settings/footer', $_data);
	}

	public function Edit($id="") {
	  	$page = PagesModel::where("e_status","active")->get();
	  	$data = FooterModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
            'page'=>$page,
        );
        return view('admin/settings/footer', $_data);

    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

        	$existCountry = FooterModel::where('v_title',$post_data['v_title'])->get();
	        if(count($existCountry)){
	        	return redirect( 'admin/footer/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }
          	$post_data['d_added']=date("Y-m-d h:i:s");
	        $post_data['d_modified']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        FooterModel::create($post_data); 
	        return redirect('admin/footer')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$existCountry = FooterModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
    	  		
	  		if(count($existCountry)){
        		return redirect( 'admin/footer/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
      		}
    		$post_data['d_modified']=date("Y-m-d h:i:s");
            FooterModel::find($id)->update($post_data);
            return redirect( 'admin/footer/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
   
        }else if($action=="delete"){
        	$_data = FooterModel::find($id)->first();
         	if(count($_data)){
			    FooterModel::find($id)->delete();
                return redirect( 'admin/footer')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/footer')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = FooterModel::find($id);
			$country->update($data);
		}

		return redirect('admin/footer')->with('success', $message);
	}



}