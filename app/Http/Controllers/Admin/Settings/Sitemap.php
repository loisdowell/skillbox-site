<?php

namespace App\Http\Controllers\Admin\Settings;

use Request, Lang, DB, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Users\Order as OrderModel;
use App\Models\Blogs\Blogs as BlogsModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Pages\Pages as PagesModel;
use App\Models\SkillboxJob\SkillboxJobCategory as SkillboxJobCategoryModel;
use App\Models\SkillboxJob\SkillboxJob as SkillboxJobModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Skills as SkillsModel;

use App\Models\Users\Sellerprofile as SellerprofileModel;


class Sitemap extends Controller {
  
    protected $section;

    public function __construct(){
        $this->section = Lang::get( 'section.sitemap' );
    }

    public function index(){
        return view('admin/settings/sitemap')->with(['section'=>$this->section]);
    }

    public function Sitemap_Page($post_data=array()){

    	$SITE_URL= url('/');
        $pageFilePath = "sitemap_page.xml";
        $pageData = PagesModel::where('e_status','active')->where("v_key","!=","")->orderBy('id','ASC')->get();

        $cat_str = "";
       	if(count($pageData)){
       		foreach ($pageData as $key => $value) {

       			$mapUrl = $SITE_URL."/cms/".$value->v_key;
	  			$cat_str .= 
					"<url>\n" .
					"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
					"	<lastmod>".date( 'Y-m-d', strtotime($value->d_modified))."</lastmod>\n" .
					"	<changefreq>".htmlentities($post_data['changefreq_page'])."</changefreq>\n" .
					"	<priority>".htmlentities ($post_data['priority_page'])."</priority>\n".
					"</url>\n";
	       	}
       	}

       	$pf = fopen($pageFilePath, "w");
		if(!$pf){
			echo "Cannot create $pageFilePath!" . NL;
			return;
		}
		fwrite ($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
					 "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
					 "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
					 "	xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\"\n". 
					 "	xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
					 "	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" );

			
		fwrite ($pf, $cat_str);
		fwrite ($pf, "</urlset>\n");
		fclose ($pf);
    }

    public function Sitemap_Blog($post_data=array()){

    	$SITE_URL= url('/');
        $pageFilePath = "sitemap_blog.xml";
        $blogData = BlogsModel::where('e_status','active')->where("v_key","!=","")->orderBy('d_modified','DESC')->get();


        $cat_str = "";

        $mapUrl=$SITE_URL."/blog";
        $cat_str .= 
		"<url>\n" .
		"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
		"	<lastmod>".date( 'Y-m-d')."</lastmod>\n" .
		"	<changefreq>".htmlentities($post_data['changefreq_blog'])."</changefreq>\n" .
		"	<priority>".htmlentities ($post_data['priority_blog'])."</priority>\n".
		"</url>\n";

       	if(count($blogData)){
       		foreach ($blogData as $key => $value) {
       			$mapUrl = $SITE_URL."/blog/".$value->v_slug;
	  			$cat_str .= 
					"<url>\n" .
					"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
					"	<lastmod>".date( 'Y-m-d', strtotime($value->d_modified))."</lastmod>\n" .
					"	<changefreq>".htmlentities($post_data['changefreq_blog'])."</changefreq>\n" .
					"	<priority>".htmlentities ($post_data['priority_blog'])."</priority>\n".
					"</url>\n";
	       	}
       	}

       	$pf = fopen($pageFilePath, "w");
		if(!$pf){
			echo "Cannot create $pageFilePath!" . NL;
			return;
		}
		fwrite ($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
					 "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
					 "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
					 "	xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\"\n". 
					 "	xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
					 "	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" );

			
		fwrite ($pf, $cat_str);
		fwrite ($pf, "</urlset>\n");
		fclose ($pf);
    }

    public function Sitemap_Common($post_data=array()){

    	$SITE_URL= url('/');
        
        $commonPageData = array();
		$commonPageData[] =  array(
			'lastmod'		=> date('Y-m-d' ),
			'url'			=> 'login',
			'priority'		=> $post_data['priority_login'],
			'changefreq'	=> $post_data['changefreq_login'],
		);
		$commonPageData[] =  array(
			'lastmod'		=> date('Y-m-d' ),
			'url'			=> 'forgot-password',
			'priority'		=> $post_data['priority_forgotpassword'],
			'changefreq'	=> $post_data['changefreq_forgotpassword'],
		);
		$commonPageData[] =  array(
			'lastmod'		=> date('Y-m-d' ),
			'url'			=> 'skillbox-faqs',
			'priority'		=> $post_data['priority_faq'],
			'changefreq'	=> $post_data['changefreq_faq'],
		);
		$commonPageData[] =  array(
			'lastmod'		=> date('Y-m-d' ),
			'url'			=> 'account/buyer',
			'priority'		=> $post_data['priority_login'],
			'changefreq'	=> $post_data['changefreq_login'],
		);
		$commonFilePath = "sitemap_common.xml";	
		
		$cat_str = "";
       	if( count( $commonPageData ) ){
       		foreach ($commonPageData as $key => $value) {

       			$mapUrl = $SITE_URL."/".$value['url'];
	  			$cat_str .= 
					"<url>\n" .
					"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
					"	<lastmod>".date('Y-m-d', strtotime( $value['lastmod']))."</lastmod>\n" .
					"	<changefreq>".htmlentities ($value['changefreq'])."</changefreq>\n" .
					"	<priority>".htmlentities ($value['priority'] )."</priority>\n".
					"</url>\n";
			}
       	}

       	$pf = fopen ($commonFilePath, "w");
		if(!$pf){
			echo "Cannot create $commonFilePath!" . NL;
			return;
		}

		fwrite ($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
					 "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
					 "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
					 "	xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\"\n". 
					 "	xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
					 "	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" );
			
		fwrite ($pf, $cat_str);
		fwrite ($pf, "</urlset>\n");
		fclose ($pf);
    }

    public function Sitemap_Joinourteam($post_data=array()){

    	$SITE_URL= url('/');
        $pageFilePath = "sitemap_joinourteam.xml";
        $cateData = SkillboxJobModel::where('e_status','active')->orderBy('d_modified','DESC')->get();
        
        $cat_str = "";
        $mapUrl=$SITE_URL."/join-our-team";
        $cat_str .= 
		"<url>\n" .
		"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
		"	<lastmod>".date( 'Y-m-d')."</lastmod>\n" .
		"	<changefreq>".htmlentities($post_data['changefreq_skillboxjob'])."</changefreq>\n" .
		"	<priority>".htmlentities ($post_data['priority_skillboxjob'])."</priority>\n".
		"</url>\n";

	   	if(count($cateData)){
       		foreach ($cateData as $key => $value) {
       			$mapUrl = $SITE_URL."/join-our-team/".$value->_id;
	  			$cat_str .= 
					"<url>\n" .
					"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
					"	<lastmod>".date( 'Y-m-d', strtotime($value->d_modified))."</lastmod>\n" .
					"	<changefreq>".htmlentities($post_data['changefreq_skillboxjob'])."</changefreq>\n" .
					"	<priority>".htmlentities ($post_data['priority_skillboxjob'])."</priority>\n".
					"</url>\n";
	       	}
       	}

       	$pf = fopen($pageFilePath, "w");
		if(!$pf){
			echo "Cannot create $pageFilePath!" . NL;
			return;
		}
		fwrite ($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
					 "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
					 "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
					 "	xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\"\n". 
					 "	xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
					 "	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" );

			
		fwrite ($pf, $cat_str);
		fwrite ($pf, "</urlset>\n");
		fclose ($pf);
    }

    public function Sitemap_Course($post_data=array()){

    	$SITE_URL= url('/');
        $pageFilePath = "sitemap_courses.xml";
        $cateData = CoursesModel::where('e_status','published')->orderBy('d_modified','DESC')->get();
        
        $cat_str = "";
        $mapUrl=$SITE_URL."/search?keyword=&e_type=courses";
        $cat_str .= 
		"<url>\n" .
		"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
		"	<lastmod>".date( 'Y-m-d')."</lastmod>\n" .
		"	<changefreq>".htmlentities($post_data['changefreq_course'])."</changefreq>\n" .
		"	<priority>".htmlentities ($post_data['priority_course'])."</priority>\n".
		"</url>\n";

	   	if(count($cateData)){
       		foreach ($cateData as $key => $value) {
       			$mapUrl = $SITE_URL."/course/".$value->_id.'/'.$value->v_title;
	  			$cat_str .= 
					"<url>\n" .
					"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
					"	<lastmod>".date( 'Y-m-d', strtotime($value->d_modified))."</lastmod>\n" .
					"	<changefreq>".htmlentities($post_data['changefreq_course'])."</changefreq>\n" .
					"	<priority>".htmlentities ($post_data['priority_course'])."</priority>\n".
					"</url>\n";
	       	}
       	}

       	$pf = fopen($pageFilePath, "w");
		if(!$pf){
			echo "Cannot create $pageFilePath!" . NL;
			return;
		}
		fwrite ($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
					 "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
					 "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
					 "	xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\"\n". 
					 "	xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
					 "	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" );

			
		fwrite ($pf, $cat_str);
		fwrite ($pf, "</urlset>\n");
		fclose ($pf);
    }

    public function Sitemap_OnlineSkill($post_data=array()){

    	$SITE_URL= url('/');
        $pageFilePath = "sitemap_skill.xml";
        $cateData = SellerprofileModel::where('v_service','online')->where('e_status','active')->orderBy('d_modified','DESC')->get();
        $cat_str = "";

        $cat_str = "";
        $mapUrl=$SITE_URL."/search?keyword=&e_type=online";
        $cat_str .= 
		"<url>\n" .
		"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
		"	<lastmod>".date( 'Y-m-d')."</lastmod>\n" .
		"	<changefreq>".htmlentities($post_data['changefreq_online'])."</changefreq>\n" .
		"	<priority>".htmlentities ($post_data['priority_online'])."</priority>\n".
		"</url>\n";

       	if(count($cateData)){
       		foreach ($cateData as $key => $value) {
       			$mapUrl = $SITE_URL."/online/".$value->_id.'/'.$value->v_profile_title;
	  			$cat_str .= 
					"<url>\n" .
					"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
					"	<lastmod>".date( 'Y-m-d', strtotime($value->d_modified))."</lastmod>\n" .
					"	<changefreq>".htmlentities($post_data['changefreq_online'])."</changefreq>\n" .
					"	<priority>".htmlentities ($post_data['priority_online'])."</priority>\n".
					"</url>\n";
	       	}
       	}

       	$pf = fopen($pageFilePath, "w");
		if(!$pf){
			echo "Cannot create $pageFilePath!" . NL;
			return;
		}
		fwrite ($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
					 "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
					 "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
					 "	xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\"\n". 
					 "	xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
					 "	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" );

			
		fwrite ($pf, $cat_str);
		fwrite ($pf, "</urlset>\n");
		fclose ($pf);
    }

    public function Sitemap_OnlineJob($post_data=array()){

    	$SITE_URL= url('/');
        $pageFilePath = "sitemap_job.xml";
        $cateData = JobsModel::where('v_service','online')->where('e_status','active')->orderBy('d_modified','DESC')->get();
        
        $cat_str = "";
        $mapUrl=$SITE_URL."/search?keyword=&e_type=online&v_type_job=on";
        $cat_str .= 
		"<url>\n" .
		"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
		"	<lastmod>".date( 'Y-m-d')."</lastmod>\n" .
		"	<changefreq>".htmlentities($post_data['changefreq_online'])."</changefreq>\n" .
		"	<priority>".htmlentities ($post_data['priority_online'])."</priority>\n".
		"</url>\n";

       	
       	if(count($cateData)){
       		foreach ($cateData as $key => $value) {
       			$mapUrl = $SITE_URL."/online-job/".$value->_id.'/'.$value->v_job_title;
	  			$cat_str .= 
					"<url>\n" .
					"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
					"	<lastmod>".date( 'Y-m-d', strtotime($value->d_modified))."</lastmod>\n" .
					"	<changefreq>".htmlentities($post_data['changefreq_online'])."</changefreq>\n" .
					"	<priority>".htmlentities ($post_data['priority_online'])."</priority>\n".
					"</url>\n";
	       	}
       	}

       	$pf = fopen($pageFilePath, "w");
		if(!$pf){
			echo "Cannot create $pageFilePath!" . NL;
			return;
		}
		fwrite ($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
					 "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
					 "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
					 "	xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\"\n". 
					 "	xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
					 "	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" );

			
		fwrite ($pf, $cat_str);
		fwrite ($pf, "</urlset>\n");
		fclose ($pf);
    }

    public function Sitemap_InpersonSkill($post_data=array()){

    	$SITE_URL= url('/');
        $pageFilePath = "sitemap_inperson_skill.xml";
        $cateData = SellerprofileModel::where('v_service','inperson')->where('e_status','active')->orderBy('d_modified','DESC')->get();
        

        $cat_str = "";
        $mapUrl=$SITE_URL."/search?keyword=&e_type=inperson";
        $cat_str .= 
		"<url>\n" .
		"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
		"	<lastmod>".date( 'Y-m-d')."</lastmod>\n" .
		"	<changefreq>".htmlentities($post_data['changefreq_inperson'])."</changefreq>\n" .
		"	<priority>".htmlentities ($post_data['priority_inperson'])."</priority>\n".
		"</url>\n";


       	if(count($cateData)){
       		foreach ($cateData as $key => $value) {
       			$mapUrl = $SITE_URL."/online/".$value->_id.'/'.$value->v_profile_title;
	  			$cat_str .= 
					"<url>\n" .
					"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
					"	<lastmod>".date( 'Y-m-d', strtotime($value->d_modified))."</lastmod>\n" .
					"	<changefreq>".htmlentities($post_data['changefreq_inperson'])."</changefreq>\n" .
					"	<priority>".htmlentities ($post_data['priority_inperson'])."</priority>\n".
					"</url>\n";
	       	}
       	}

       	$pf = fopen($pageFilePath, "w");
		if(!$pf){
			echo "Cannot create $pageFilePath!" . NL;
			return;
		}
		fwrite ($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
					 "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
					 "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
					 "	xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\"\n". 
					 "	xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
					 "	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" );

			
		fwrite ($pf, $cat_str);
		fwrite ($pf, "</urlset>\n");
		fclose ($pf);
    }

    public function Sitemap_InpersonJob($post_data=array()){

    	$SITE_URL= url('/');
        $pageFilePath = "sitemap_inperson_job.xml";
        $cateData = JobsModel::where('v_service','inperson')->where('e_status','active')->orderBy('d_modified','DESC')->get();
        
        $cat_str = "";
        $mapUrl=$SITE_URL."/search?keyword=&e_type=inperson&&v_type_job=on";
        $cat_str .= 
		"<url>\n" .
		"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
		"	<lastmod>".date( 'Y-m-d')."</lastmod>\n" .
		"	<changefreq>".htmlentities($post_data['changefreq_inperson'])."</changefreq>\n" .
		"	<priority>".htmlentities ($post_data['priority_inperson'])."</priority>\n".
		"</url>\n";

	   	if(count($cateData)){
       		foreach ($cateData as $key => $value) {
       			$mapUrl = $SITE_URL."/online-job/".$value->_id.'/'.$value->v_job_title;
	  			$cat_str .= 
					"<url>\n" .
					"	<loc>".str_replace( array("&","<",">",'"',"'","�"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"," "),$mapUrl)."</loc>\n".
					"	<lastmod>".date( 'Y-m-d', strtotime($value->d_modified))."</lastmod>\n" .
					"	<changefreq>".htmlentities($post_data['changefreq_inperson'])."</changefreq>\n" .
					"	<priority>".htmlentities ($post_data['priority_inperson'])."</priority>\n".
					"</url>\n";
	       	}
       	}

       	$pf = fopen($pageFilePath, "w");
		if(!$pf){
			echo "Cannot create $pageFilePath!" . NL;
			return;
		}
		fwrite ($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
					 "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
					 "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
					 "	xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\"\n". 
					 "	xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
					 "	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" );

			
		fwrite ($pf, $cat_str);
		fwrite ($pf, "</urlset>\n");
		fclose ($pf);
    }

    public function Generate(){
        
        $post_data = Request::all();
        $SITE_URL= url('/');
        
        self::Sitemap_Page($post_data);
        self::Sitemap_Blog($post_data);
        self::Sitemap_Common($post_data);
        self::Sitemap_Joinourteam($post_data);
        self::Sitemap_Course($post_data);
        self::Sitemap_OnlineSkill($post_data);
        self::Sitemap_OnlineJob($post_data);
        self::Sitemap_InpersonSkill($post_data);
        self::Sitemap_InpersonJob($post_data);
     
		$indexFilePath = "sitemap_index.xml";
		$pf = fopen ($indexFilePath, "w");
		if(!$pf){
			echo "Cannot create $sitemap_file_path!" . NL;
			return;
		}
		
		$sitemap_blog="sitemap_blog.xml";
		$sitemap_common="sitemap_common.xml";
		$sitemap_joinourteam="sitemap_joinourteam.xml";
		$sitemap_courses="sitemap_courses.xml";
		$sitemap_inperson_job="sitemap_inperson_job.xml";
		$sitemap_inperson_skill="sitemap_inperson_skill.xml";
		$sitemap_job="sitemap_job.xml";
		$sitemap_skill="sitemap_skill.xml";
		$sitemap_page="sitemap_page.xml";

		fwrite( $pf, 
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
		"<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n".
		"	<sitemap>\n".
		"		<loc>".$SITE_URL.'/'.$sitemap_blog."</loc>\n" .
		"		<lastmod>".date("Y-m-d")."</lastmod>\n" .
		"	</sitemap>\n".
		"	<sitemap>\n" .
		"		<loc>".$SITE_URL.'/'.$sitemap_common."</loc>\n" .
		"		<lastmod>".date("Y-m-d")."</lastmod>\n" .
		"	</sitemap>\n".
		"	<sitemap>\n" .
		"		<loc>".$SITE_URL.'/'.$sitemap_joinourteam."</loc>\n" .
		"		<lastmod>".date("Y-m-d")."</lastmod>\n" .
		"	</sitemap>\n".
		"	<sitemap>\n" .
		"		<loc>".$SITE_URL.'/'.$sitemap_courses."</loc>\n" .
		"		<lastmod>".date("Y-m-d")."</lastmod>\n" .
		"	</sitemap>\n".
		"	<sitemap>\n" .
		"		<loc>".$SITE_URL.'/'.$sitemap_inperson_job."</loc>\n" .
		"		<lastmod>".date("Y-m-d")."</lastmod>\n" .
		"	</sitemap>\n".
		"	<sitemap>\n" .
		"		<loc>".$SITE_URL.'/'.$sitemap_inperson_skill."</loc>\n" .
		"		<lastmod>".date("Y-m-d")."</lastmod>\n" .
		"	</sitemap>\n".
		"	<sitemap>\n" .
		"		<loc>".$SITE_URL.'/'.$sitemap_job."</loc>\n" .
		"		<lastmod>".date("Y-m-d")."</lastmod>\n" .
		"	</sitemap>\n".
		"	<sitemap>\n" .
		"		<loc>".$SITE_URL.'/'.$sitemap_page."</loc>\n" .
		"		<lastmod>".date("Y-m-d")."</lastmod>\n" .
		"	</sitemap>\n".
		"	<sitemap>\n" .
		"		<loc>".$SITE_URL.'/'.$sitemap_skill."</loc>\n" .
		"		<lastmod>".date("Y-m-d")."</lastmod>\n" .
		"	</sitemap>\n".
		"</sitemapindex>"
		);

		fclose ($pf);

        return redirect( 'admin/sitemap')->with( 'success', Lang::get('message.sitemap')); 
    }

}