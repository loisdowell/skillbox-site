<?php
namespace App\Http\Controllers\Admin\Order;

use Request, Lang;
use App\Http\Controllers\Controller;
use App\Models\Users\Order as OrderModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;

class Skill extends Controller {

	protected $section;

	public function __construct(\MangoPay\MangoPayApi $mangopay){
	    $this->mangopay = $mangopay;
		$this->section = "Skill Order";
	}

	public function index() {
		
		$query = OrderModel::query();
		$data = $query->where('e_transaction_type','skill');
		$data = $query->where('i_delete','!=',"1")->orderBy('d_added','DESC')->get();
		
		$_data=array(
			'section'=>$this->section,
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/order/skill', $_data);
	}

	public function Edit($id=""){
		
		$data=OrderModel::find($id);
		$_data=array(
			'section'=>$this->section,
			'view' => "edit",
			'data' => $data,
		);
		return view('admin/order/skill',$_data);
	}

	public function Action($action="",$id=""){
			
			$post_data=Request::all();
			if(isset($post_data['_token'])){
		     	unset($post_data['_token']);
		    }

            if($action=="edit"){
		    	
		    	$orderdata = OrderModel::find($id);

		    	if(isset($post_data['e_seller_payment_status']) && $post_data['e_seller_payment_status']=="success" && $orderdata->e_seller_payment_status=="pending"){
            		self::sellerpayment($orderdata);
            	}

                if($post_data['e_payment_status']=="refund" && $orderdata->e_payment_status != "refund" ){
                    self::BuyerRefund($orderdata);
                }

              	$post_data['d_modified']=date('Y-m-d H:i:s');
			    OrderModel::find($id)->update($post_data); 
			    return redirect('admin/order/skill/edit/'. $id)->with('success',Lang::get('message.common.detailUpdated', [ 'section' => $this->section ]));

            }
		    else if($action=="delete"){
		    	
		    	$_data = OrderModel::find($id);
		    	
		    	if(count($_data)){
		    		$updateorder['i_delete']="1";
		    		OrderModel::find($id)->update($updateorder);
					return redirect('admin/order/skill')->with('success',Lang::get('message.common.detailDeleted', [ 'section' => $this->section ]));
		    	}
		   }
		   else {
                return redirect('admin/order/skill')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }
		    
	}

	public function sellerpayment($orderdata){

        if(!count($orderdata)){
			return 0;
		}

		$userdata = UsersModel::find($orderdata->i_seller_id);
		if(!count($userdata)){
			return 0;
		}

	    $creditselleramt=0;
        $comissionfees=0;
        $processingamt=0;
        $comission=10;
        $processing=0.60;
            
        if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b9f4d3e8124f123c986c"){

            $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_PREMIUM');
            if($comission==""){
                $comission=10;
            }
            $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_PREMIUM');
            if($processing==""){
                $processing=0.60;
            }
        }

        if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b757d3e8125e323c986a"){

            $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_STANDARD');
            if($comission==""){
                $comission=10;
            }
            $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_STANDARD');
            if($processing==""){
                $processing=0.60;
            }
        }

        if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b48cd3e812a4253c9869"){

            $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_BASIC');
            if($comission==""){
                $comission=10;
            }
            $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_BASIC');
            if($processing==""){
                $processing=0.60;
            }
        }

        $creditselleramt = $orderdata->v_amount;
        $comissionfees = $creditselleramt*$comission/100;
        $comissionfees = number_format($comissionfees,2);
        $processingamt = number_format($processing,2);
        $totalminus = $comissionfees+$processingamt;
        $creditselleramt = $creditselleramt-$totalminus;
        $creditselleramt = number_format($creditselleramt,2);

        $mangototalamt = $creditselleramt*100;

        $DebitedWalletID = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
        if($DebitedWalletID!=""){
            $DebitedWalletID = (int)$DebitedWalletID;  
        }else{
            $DebitedWalletID=55569248;  
        }

        $AuthorId = GeneralHelper::getSiteSetting('MANGOPAY_USERID');
        if($AuthorId!=""){
            $AuthorId = (int)$AuthorId;  
        }else{
            $AuthorId=55569242;  
        }
        $CreditedWalletId = $userdata->i_wallet_id;

        $Transfer = new \MangoPay\Transfer();
        $Transfer->AuthorId = $AuthorId;
        $Transfer->DebitedFunds = new \MangoPay\Money();
        $Transfer->DebitedFunds->Currency = "GBP";
        $Transfer->DebitedFunds->Amount = (float)$mangototalamt;
        $Transfer->Fees = new \MangoPay\Money();
        $Transfer->Fees->Currency = "GBP";
        $Transfer->Fees->Amount = 0;
        $Transfer->DebitedWalletID = $DebitedWalletID;
        $Transfer->CreditedWalletId = (int)$CreditedWalletId;

        $result = $this->mangopay->Transfers->Create($Transfer);
        
        if(count($result)){
            if(isset($result->Status) && $result->Status=="SUCCEEDED"){
                
                $updateorder['e_seller_payment_status']="success";
                $updateorder['e_seller_payment_amount']=$creditselleramt;
                $updateorder['e_seller_connection_fee']=$processingamt; 
                $updateorder['v_seller_commission']=$comissionfees;
                $updateorder['v_seller_processing_fees']=$processingamt;  
                OrderModel::find($orderdata->id)->update($updateorder); 
            }
        }
    }

    public function BuyerRefund($orderdata){

        if(!count($orderdata)){
            return 0;
        }

        $AuthorId = GeneralHelper::getSiteSetting('MANGOPAY_USERID');
        if($AuthorId!=""){
            $AuthorId = (int)$AuthorId;  
        }else{
            $AuthorId=55569242;  
        }

        $DebitedWalletID = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
        if($DebitedWalletID!=""){
            $DebitedWalletID = (int)$DebitedWalletID;  
        }else{
            $DebitedWalletID = 55569248;  
        }

        $cancellfees = GeneralHelper::getSiteSetting('ORDER_CANCEL_ADMIN_FEES');
        if($cancellfees!=""){
            $cancellfees = $cancellfees;  
        }else{
            $cancellfees=2.5;  
        }

        $CreditedWalletId = ""; 
        if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->i_wallet_id)){
            $CreditedWalletId = (int)$orderdata->hasUser()->i_wallet_id;
        }

        $creditselleramt=0;
        $creditselleramt = $orderdata->v_amount+$orderdata->v_buyer_commission;
        $adminfees =$creditselleramt*$cancellfees/100;
        $creditselleramt = $creditselleramt-$adminfees;
        $mangototalamt = $creditselleramt*100;

        if($CreditedWalletId!=""){
            $Transfer = new \MangoPay\Transfer();
            $Transfer->AuthorId = $AuthorId;
            $Transfer->DebitedFunds = new \MangoPay\Money();
            $Transfer->DebitedFunds->Currency = "GBP";
            $Transfer->DebitedFunds->Amount = (float)$mangototalamt;
            $Transfer->Fees = new \MangoPay\Money();
            $Transfer->Fees->Currency = "GBP";
            $Transfer->Fees->Amount = 0;
            $Transfer->DebitedWalletID = $DebitedWalletID;
            $Transfer->CreditedWalletId = (int)$CreditedWalletId;
            $result = $this->mangopay->Transfers->Create($Transfer);
            if(count($result)){
                if(isset($result->Status) && $result->Status=="SUCCEEDED"){
                    $update['e_payment_status']="refund";
                    $update['v_admin_fees']=$adminfees;
                    OrderModal::find($orderdata->id)->update($update);  
                }
            }
        }

    }

}
?>