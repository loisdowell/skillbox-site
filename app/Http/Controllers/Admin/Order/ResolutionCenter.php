<?php
namespace App\Http\Controllers\Admin\Order;

use Request, Lang;
use App\Http\Controllers\Controller;
use App\Models\Support\Resolution as ResolutionModal;
use App\Models\Users\Order as OrderModal;
use App\Models\Users\Users as UsersModal;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\General as GeneralHelper;


class ResolutionCenter extends Controller {

	protected $section;

	public function __construct(\MangoPay\MangoPayApi $mangopay){
	    $this->mangopay = $mangopay;
	    $this->section = "Resolution Center";
	}
	
	public function index() {
		
		$query = ResolutionModal::query();
		$data = $query->where('i_delete',"!=","1")->orderBy('d_added','DESC')->get();
		
		$_data=array(
			'section'=>$this->section,
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/order/resolution-center', $_data);
	}

	public function Edit($id=""){
		
		$data=ResolutionModal::find($id);
		$issue = GeneralHelper::ResolutionProblem();    

		$orderdata=array();
		if(count($data)){
			$orderdata = OrderModal::find($data->i_order_id);
		}
		$_data=array(
			'section'=>$this->section,
			'view' => "edit",
			'data' => $data,
			'issue' => $issue,
			'orderdata'=>$orderdata
		);
		return view('admin/order/resolution-center',$_data);

	}

	public function Action($action="",$id=""){
			
			$post_data=Request::all();

			if(isset($post_data['_token'])){
		     	unset($post_data['_token']);
		    }

		    if($action=="edit"){
		    	
		    	$data = OrderModal::find($id);

		    	if(!count($data)){
		    		return redirect('admin/order/resolution-center')->with( 'warning', Lang::get('message.common.somethingWrong')); 
		    	}

		    	if($post_data['v_order_status'] == 'cancelled' && $data->v_order_status!="cancelled"){
		    			
		    			$post_data['v_order_status']="cancelled";
		    			$post_data['d_modified']=date('Y-m-d H:i:s');
						$post_data['e_cancelled_by']="admin";
		    			OrderModal::find($id)->update($post_data); 
		    			self::BuyerRefundPayment($data);

		    			$userid = auth()->guard('web')->user()->_id;
						$insert['e_type']="order_cancelled";
						$insert['i_user_id']=$userid;
						$insert['v_order_id']=$post_data['cancelOrderId'];
						$insert['d_added'] = date("Y-m-d H:i:s");
						$insert['d_modified'] = date("Y-m-d H:i:s");
						OrderRequirements::insert($insert);

						$user = UsersModal::find($data->i_user_id);
						if(isset($user) && count($user)){
							$username = $user->v_fname .' '. $user->v_lname;
							$emailData=array(
								'name'=>$username
							);
							$mailcontent = EmailtemplateHelper::OrderCancelled($emailData);
							$subject = "Order cancelled";
							$subject = EmailtemplateHelper::EmailTemplateSubject("5b0e7dbf76fbae5b834b02e3");
							$mailids=array(
								$username=>$user->v_email,
							);
							$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
						}
				}

			return redirect('admin/order/resolution-center')->with('success',Lang::get('message.common.detailUpdated', [ 'section' => $this->section ]));

            }
		    else if($action=="delete"){
		    		
		    	$_data = ResolutionModal::find($id);
		    	
		    	if(count($_data)){
		    		$updateorder['i_delete']="1";
		    		ResolutionModal::find($id)->update($updateorder);
					return redirect('admin/order/resolution-center')->with('success',Lang::get('message.common.detailDeleted', [ 'section' => $this->section ]));
		    	}
		    }
		    else {
                return redirect('admin/order/resolution-center')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }
		    
	}

	public function BuyerRefundPayment($orderdata="") {
		
		if(!count($orderdata)){
			return 0;
		}

	   	$AuthorId = GeneralHelper::getSiteSetting('MANGOPAY_USERID');
        if($AuthorId!=""){
            $AuthorId = (int)$AuthorId;  
        }else{
            $AuthorId=55569242;  
        }

        $DebitedWalletID = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
        if($DebitedWalletID!=""){
            $DebitedWalletID = (int)$DebitedWalletID;  
        }else{
            $DebitedWalletID=55569248;  
        }

        $CreditedWalletId = "";	
        if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->i_wallet_id)){
        	$CreditedWalletId = (int)$orderdata->hasUser()->i_wallet_id;
        }

        $creditselleramt=0;
		$creditselleramt = $orderdata->v_amount+$orderdata->v_buyer_commission;
	   	$mangototalamt = $creditselleramt*100;

	    if($CreditedWalletId!=""){

	    	$Transfer = new \MangoPay\Transfer();
	        $Transfer->AuthorId = $AuthorId;
	        $Transfer->DebitedFunds = new \MangoPay\Money();
	        $Transfer->DebitedFunds->Currency = "GBP";
	        $Transfer->DebitedFunds->Amount = (float)$mangototalamt;
	        $Transfer->Fees = new \MangoPay\Money();
	        $Transfer->Fees->Currency = "GBP";
	        $Transfer->Fees->Amount = 0;
	        $Transfer->DebitedWalletID = $DebitedWalletID;
	        $Transfer->CreditedWalletId = (int)$CreditedWalletId;
	        $result = $this->mangopay->Transfers->Create($Transfer);
			
			if(count($result)){
	            if(isset($result->Status) && $result->Status=="SUCCEEDED"){
	                $update['e_payment_status']="refund";
	                OrderModal::find($orderdata->id)->update($update);	
	            }
	        }
    	}
    }



}
?>