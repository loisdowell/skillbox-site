<?php
namespace App\Http\Controllers\Admin\Order;

use Request, Lang;
use App\Http\Controllers\Controller;
use App\Models\Users\Order as OrderModel;
use App\Models\Users\Users as UsersModel;

class UserPlan extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "User's subscription";
	}

	public function index() {
		
		$query = OrderModel::query();
		$query = $query->where('e_transaction_type','userplan');
		$data = $query->where('i_delete','!=',"1")->orderBy('d_added',"DESC")->get();
		
		$finallist=array();
		if(count($data)){
			foreach ($data as $key => $value) {
				if(!isset($finallist[$value->i_user_id])){
					$finallist[$value->i_user_id]=$value;
				}		
			}
		}

		$_data=array(
			'section'=>$this->section,
			'view'=>"list",
			'data'=>$finallist,
		);
		return view('admin/order/user-plan', $_data);

	}

	public function Edit($id="")
	{
		
		$data = OrderModel::find($id);

		$query = OrderModel::query();
		$query = $query->where('e_transaction_type','userplan');
		$alldata = $query->where('i_delete','!=',"1")->where('i_user_id',$data->i_user_id)->orderBy('d_added',"DESC")->get();
	
		$_data=array(
			'section'=>$this->section,
			'view' => "edit",
			'data' => $data,
			'alldata' => $alldata,
		);
		return view('admin/order/user-plan',$_data);
	}

	public function Action($action="",$id=""){
				
			$post_data=Request::all();
		
			if(isset($post_data['_token'])){
		     	unset($post_data['_token']);
		    }

		    if($action=="edit"){
		    	
		    	$orderData = OrderModel::find($id);

            	if($post_data['e_payment_status']=="success" && $orderData->e_payment_status!="success"){
            		
            		$userdata = UsersModel::find($orderData->i_user_id);

            		if(count($userdata) && count($orderData)){
            			$updateUser['v_plan'] = $userdata->v_plan;
	            		$updateUser['v_plan']['id'] = $orderData->v_order_detail['v_plan_id'];
	            		$updateUser['v_plan']['duration'] = $orderData->v_order_detail['v_plan_duration'];
	            		$updateUser['v_plan']['d_start_date'] = date("Y-m-d");
	            		if($updateUser['v_plan']['duration']=="monthly"){
							$updateUser['v_plan']['d_end_date'] = date('Y-m-d', strtotime('+1 months'));
	            		}else{
	            			$updateUser['v_plan']['d_end_date'] = date('Y-m-d', strtotime('+1 years'));
	            		}
	            		UsersModel::find($orderData->i_user_id)->update($updateUser);
	            	}		
	          	}
	          	$update['e_payment_status'] = $post_data['e_payment_status'];
		    	$update['d_modified']=date('Y-m-d H:i:s');
			    OrderModel::find($id)->update($update); 
			   return redirect('admin/order/user-plan/edit/'. $id)->with('success',Lang::get('message.common.detailUpdated', [ 'section' => $this->section ]));
		    }
		    else if($action=="delete"){
		    	$_data = OrderModel::find($id);
		    	if(count($_data)){
		    		$updatedata['i_delete']="1";
		    		OrderModel::find($id)->update($updatedata);
		    		return redirect('admin/order/user-plan')->with('success',Lang::get('message.common.detailDeleted', [ 'section' => $this->section ]));
		    	}
		    }
		    else {
                return redirect('admin/order/user-plan')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }
		    
		}

}
?>