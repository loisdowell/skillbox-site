<?php
namespace App\Http\Controllers\Admin\Notify;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Country as CountryModel;
use App\Models\Notify as NotifyModel;
use App\Models\Categories as CategoriesModel;
use App\Models\Skills as SkillsModel;

class Notifyuser extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "Notifu user";
	}

	public function index() {

		$query = NotifyModel::query();
		$data = $query->where('i_delete','!=',"1")->orderBy('d_added','DESC')->get();

		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/notify/notifyuser', $_data);
	}

	public function Add() {

		$categories = CategoriesModel::where("e_status","active")->get();
		$skilldata = SkillsModel::where("e_status","active")->get();

		$_data=array(
			'view'=>"add",
			'categories'=>$categories,
			'skilldata'=>$skilldata,
		);
		return view('admin/notify/notifyuser', $_data);
	}
	
	public function Edit($id="") {
	  	
	  	$data = NotifyModel::find($id);
	  	$categories = CategoriesModel::where("e_status","active")->get();
		$skilldata = SkillsModel::where("e_status","active")->get();

	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
            'categories'=>$categories,
			'skilldata'=>$skilldata,
        );
        return view('admin/notify/notifyuser', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

        	$existCountry = NotifyModel::where('v_email',$post_data['v_email'])->where('i_skill_id',$post_data['i_skill_id'])->get();
	        if(count($existCountry)){
	        	return redirect( 'admin/notify-user/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

	        $post_data['i_count']="0";
			$post_data['d_added']=date("Y-m-d H:i:s");
			$post_data['d_modified']=date("Y-m-d H:i:s");
		    unset($post_data['_token']);
	        
	        NotifyModel::create($post_data); 
	        return redirect('admin/notify-user')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$existCountry = NotifyModel::where('v_email',$post_data['v_email'])->where('i_skill_id',$post_data['i_skill_id'])->where('_id',"!=",$id)->get();
    	  		
	  		if(count($existCountry)){
        		return redirect( 'admin/notify-user/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
      		}
      		$post_data['d_modified']=date("Y-m-d H:i:s");
            NotifyModel::find($id)->update($post_data);
            return redirect( 'admin/notify-user/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
   
        }else if($action=="delete"){
        	
        	$_data = NotifyModel::find($id)->first();

         	if(count($_data)){
			    $update['i_delete']="1";
			    NotifyModel::find($id)->update($update);
                return redirect( 'admin/notify-user')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/notify-user')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }
        }
          
    }
    
    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = NotifyModel::find($id);
			$country->update($data);
		}

		return redirect('admin/notify-user')->with('success', $message);
	}



}