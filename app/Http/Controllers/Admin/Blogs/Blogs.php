<?php
namespace App\Http\Controllers\Admin\Blogs;

use Request, Lang, File, GeneralHelper,Storage;
use App\Http\Controllers\Controller;
use App\Models\Blogs\Blogs as BlogsModel;

class Blogs extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "Blogs";
	}

	public function index() {
		
		$query = BlogsModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/blogs/blogs', $_data);
	}

	public function Add() {

		$_data=array(
			'view'=>"add",
		);
		return view('admin/blogs/blogs', $_data);
	}

	public function Edit($id="") {
		
	  $data = BlogsModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/blogs/blogs', $_data);
    }
    
    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

        	$exist = BlogsModel::where('v_slug',$post_data['v_slug'])->get(); 
        	if(count($exist)){
        		return redirect( 'admin/blogs/add/0')->with( 'warning', "Blog slug already exist.please use different."); 
        	}
        	if(Request::file()){
			  
				$image = Request::file('v_image');
				$ext   = $image->getClientOriginalExtension();
				
				if(count($image) ){
					
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						
						$fileName = 'blog-'.time().'.'.$image->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_image'] = 'blog/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/blog/'.$fileName, File::get((string)$image), 'public');

					}else{
						return redirect( 'admin/blogs/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					
					}
				}
			}

			if(isset($post_data['d_publish_date']) && $post_data['d_publish_date']!=""){
				$post_data['d_publish_date'] = date("Y-m-d",strtotime($post_data['d_publish_date']));
			}else{
				$post_data['d_publish_date'] = date("Y-m-d");
			}


			$post_data['i_user_id']=auth()->guard('admin')->user()->id;
			$post_data['e_user_type']='admin';
       		$post_data['d_added']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        BlogsModel::create($post_data); 
	        return redirect('admin/blogs')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$exist = BlogsModel::where('v_slug',$post_data['v_slug'])->where('_id',"!=",$id)->get(); 
        	if(count($exist)){
        		return redirect( 'admin/blogs/edit/'.$id)->with( 'warning', "Blog slug already exist.please use different."); 
        	}

    		if(Request::file()){
				
				$image = Request::file('v_image');
				$ext   = $image->getClientOriginalExtension();
				
				if( count($image) ){
					
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){

						$imageExists = BlogsModel::where('_id',$id)->value('v_image');
						if(Storage::disk('s3')->exists($imageExists)){
							Storage::disk('s3')->Delete($imageExists);
						}

						$fileName = 'blog-'.time().'.'.$image->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_image'] = 'blog/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/blog/'.$fileName, File::get((string)$image), 'public');

					}else{
						return redirect('admin/blogs/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					}
				}
			
			}
				if(isset($post_data['d_publish_date']) && $post_data['d_publish_date']!=""){
					$post_data['d_publish_date'] = date("Y-m-d",strtotime($post_data['d_publish_date']));
				}

				$post_data['i_user_id']=auth()->guard('admin')->user()->id;
				$post_data['e_user_type']='admin';
       			$post_data['d_modified']=date("Y-m-d h:i:s");
	            BlogsModel::find($id)->update($post_data);
	            return redirect( 'admin/blogs/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = BlogsModel::find($id)->first();

            if(count($_data)){
               	
			    BlogsModel::find($id)->delete();
                return redirect( 'admin/blogs')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/blogs')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
    }
    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$faqs = BlogsModel::find($id);
			$faqs->update($data);
		}

		return redirect('admin/blogs')->with('success', $message);
	}



}