<?php
namespace App\Http\Controllers\Admin\NewsLetter;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\NewsLetter as NewsLetterModel;

class NewsLetter extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "NewsLetter";
	}

	public function index() {
		$query = NewsLetterModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/newsletter/newsletter', $_data);
	}

	public function Add() {

		$categories = NewsLetterModel::get();
		$_data=array(
			'view'=>"add",
			'categories' => $categories
		);
		return view('admin/newsletter/newsletter', $_data);
	
	}
	
	public function Edit($id="") {
		
	  $data = NewsLetterModel::find($id);
	    $_data=array(
            'view'=>"edit",
            'data'=>$data,
        );
        return view('admin/newsletter/newsletter', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){
       		$post_data['d_added']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        NewsLetterModel::create($post_data); 
	        return redirect('admin/newsletterusers')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
       		$post_data['d_modified']=date("Y-m-d h:i:s");
	            NewsLetterModel::find($id)->update($post_data);
	            return redirect( 'admin/newsletterusers/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = NewsLetterModel::find($id)->first();

            if(count($_data)){
               	
			    NewsLetterModel::find($id)->delete();
                return redirect( 'admin/newsletterusers')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/newsletterusers')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$newsletter = NewsLetterModel::find($id);
			$newsletter->update($data);
		}

		return redirect('admin/newsletterusers')->with('success', $message);
	}



}