<?php
namespace App\Http\Controllers\Admin\Profile;

use Request, Hash, Lang, File, Storage;
use App\Http\Controllers\Controller;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Admin as AdminModel;


class Profile extends Controller {

	 protected $section;

	public function __construct(){
		
		$this->section = Lang::get('section.dashboard');
	}

	public function index() {
		$admin = AdminModel::orderBy('v_name')->get();	
		$_data = array(
				'view'=>"list",
				'admin'	 => $admin
		);
		return view('admin/profile/profile' , $_data);
	}

	public function Add() {
		
		$_data = array(
				'view'=>"add"
		);
		return view('admin/profile/profile' , $_data);
	}

	public function Edit($id) {
		
		$admin = AdminModel::where("_id",$id)->first();	
		
		$_data = array(
				'view'=>"edit",
				'admin'	 => $admin
		);
		return view('admin/profile/profile' , $_data);
	}

	public function addProfile($id) {

		$post_data = Request::all();
		
		if(!auth()->guard('admin')->check()) {
			return redirect('admin/login');
		}

		$adminid = auth()->guard('admin')->user()->_id;

		$existEmail = AdminModel::where('v_email',$post_data['v_email'])->get();
   	
   		if(count($existEmail) > 0){
    		return redirect( 'admin/admin/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
  		}

       	if( Request::file() ){

			$image = Request::file('v_profile');
			
			if(count($image) ){
					
					$ext   = $image->getClientOriginalExtension();
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						
						$fileName = 'profile-image-'.time().'.'.$image->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_profile'] = 'admin/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/admin/'.$fileName, File::get((string)$image), 'public');

					}else{
						return redirect( 'admin/admin/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					
					}
				}
		}

		if(isset($post_data['password']) && $post_data['password']!=""){
			$post_data['password'] = Hash::make($post_data['password']);
		}
		
		$post_data['d_added']=date("Y-m-d h:i:s");
		$post_data['d_modified']=date("Y-m-d h:i:s");
        AdminModel::create($post_data);

		return redirect('admin/admin')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 
	}

	public function updateProfile($id) {

		$post_data = Request::all();
		if(!auth()->guard('admin')->check()) {
			return redirect('admin/login');
		}
		$existName = AdminModel::where('v_email',$post_data['v_email'])->get();
   	
   		if(count($existName) > 1){
    		return redirect( 'admin/admin/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
  		}

		if( Request::file() ){
					
			$image = Request::file('v_profile');
		
			
			if( count($image) ){
			
				$ext   = $image->getClientOriginalExtension();
				
				if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){

					$imageExists = AdminModel::where('id',$id)->value('v_profile');

					if(Storage::disk('s3')->exists($imageExists)){
						Storage::disk('s3')->Delete($imageExists);
					}

					$fileName = 'profile-image-'.time().'.'.$image->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$post_data['v_profile'] = 'admin/' . $fileName;
					$uploadS3 = Storage::disk('s3')->put('/admin/'.$fileName, File::get((string)$image), 'public');

				}else{
					return redirect( 'admin/admin/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
				}
			}
		}

		if(isset($post_data['password']) && $post_data['password']!=""){
			$post_data['password'] = Hash::make($post_data['password']);
		}else{
			unset($post_data['password']);
		}
		$post_data['d_modified']=date("Y-m-d h:i:s");
        AdminModel::find($id)->update($post_data);

		return redirect('admin/admin/edit/'.$id)->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 
	}

	public function deleteProfile($id) {
		$_data = AdminModel::find($id);

        if(count($_data)){
           	
           	$destination = public_path('Assets/uploads');
			$imageExists = $_data['v_profile'];

			if( isset($imageExists) && $imageExists != '' ) {
				if( File::exists( $destination . '/' . $imageExists ) ) {
					 File::delete( $destination . '/' . $imageExists );
				}
			}

			$_data->delete();
            return redirect( 'admin/admin')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
        }else{
            return redirect( 'admin/admin')->with( 'warning', Lang::get('message.common.somethingWrong')); 
        }
	}

	 public function status( $id, $status ) {
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$admin = AdminModel::find($id);
			$admin->update($data);
		}

		return redirect('admin/admin')->with('success', $message);
	}
	



}