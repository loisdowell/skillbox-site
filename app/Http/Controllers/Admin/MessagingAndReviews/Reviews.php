<?php
namespace App\Http\Controllers\Admin\MessagingAndReviews;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\MessagingReviews\Reviews as ReviewsModel;
use App\Models\MessagingReviews\ReviewComments as ReviewsCommentsModel;
use App\Models\Users\Users as UsersModel;

class Reviews extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Reviews";
	}

	public function index() {

		$query 				= ReviewsModel::query();
		$data 				= $query->orderBy('id','DESC')->get();
		$_data				= array(
								'view'	=>"list",
								'data'	=>$data,
							);
		return view('admin/messagingandreviews/reviews', $_data);
	}

	public function View($id="") {
	  	$comment 			= ReviewsCommentsModel::where("e_status","active")->get();
		$data 				= ReviewsModel::find($id);
	    $_data				= array(
					            'view'				=>"view",
					            'data'				=> $data,
								'comment'			=> $comment,
					        );
	    return view('admin/messagingandreviews/reviews', $_data);
    }
    public function Action($action="",$id="") {

        if($action=="delete"){
        	
        	$_data = ReviewsModel::find($id)->first();

            if(count($_data)){
               
			    ReviewsModel::find($id)->delete();
			    ReviewsCommentsModel::where("i_review_id",$id)->delete();
                return redirect( 'admin/reviews')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/reviews')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }

          
    }

    public function deletecomment() {
	  
	  $post_data = Request::all();
	  $id = $post_data['comment_id'];
	  
	  if(count($id)){
	  	ReviewsCommentsModel::find($id)->delete();
	  }
	     
	}
	

}