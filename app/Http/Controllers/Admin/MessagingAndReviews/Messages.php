<?php
namespace App\Http\Controllers\Admin\MessagingAndReviews;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\MessagingReviews\Messages as MessagesModel;
use App\Models\Users\Users as UsersModel;

class Messages extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Messaging";
	}

	public function index() {

		$data 				= MessagesModel::where("i_parent_id",0)->get();
		$_data				= array(
								'view'	=>"list",
								'data'	=>$data,
							);
		return view('admin/messagingandreviews/messages', $_data);
	}

	public function View($id="") {
	  	$users 				= UsersModel::where("e_status","active")->get();
		$data 				= MessagesModel::where("i_parent_id",$id)->get();
		$maindata 			= MessagesModel::find($id);
	    $_data				= array(
					            'view'				=>"view",
					            'data'				=> $data,
								'users'				=> $users,
								'maindata'			=>$maindata
					        );
	    return view('admin/messagingandreviews/messages', $_data);
    }
    public function Action($action="",$id="") {

        if($action=="delete"){
        	
        	$_data = MessagesModel::find($id)->first();

            if(count($_data)){
               
			    MessagesModel::find($id)->delete();
                return redirect( 'admin/messages')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/messages')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

}