<?php
namespace App\Http\Controllers\Admin\Dashboard;

use Request, Hash, Lang;
use App\Http\Controllers\Controller;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Users\Order as OrderModel;
use App\Models\Blogs\Blogs as BlogsModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;


class Reportdata extends Controller {

	 protected $section;

	public function __construct(){
		$this->section = Lang::get('section.dashboard');
	}
	
	public function index() {

		$data = Request::all();

		$startdate="";

		if(isset($data['d_start_date']) && $data['d_start_date']!=""){
			$startdate = date("Y-m-d",strtotime($data['d_start_date']));		
			$response['d_start_date']=$data['d_start_date'];
		}

		$enddate="";
		if(isset($data['d_end_date']) && $data['d_end_date']!=""){
			$enddate = date("Y-m-d",strtotime($data['d_end_date']));		
			$response['d_end_date']=$data['d_end_date'];
		}

		$userdata=array();
		$query = OrderModel::query();
		
		$query = $query->where(function($query ) use($userdata){
			$query->where('e_transaction_type','skill')->orWhere('e_transaction_type', 'course');
    	});	
		$query = $query->where('e_seller_payment_status',"success");
    	if($startdate!="" && $enddate!=""){
			$query = $query->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$orderdata = $query->get();
		
		$totalsales=0;
		$totalsalesnumber=count($orderdata);

		$totalsponseredsales=0;
		$totalsponseredsalesnumber=0;

		$totalcommisionearn=0;
		$totalcommisionearnnumber=count($orderdata);

		if(count($orderdata)){
			foreach ($orderdata as $key => $value) {
				
				if(isset($value->v_amount)){
					$totalsales =$totalsales+$value->v_amount;
				}
				if(isset($value->v_sponser_sales) && $value->v_sponser_sales=="yes"){
					$totalsponseredsales =$totalsponseredsales+$value->v_amount;
					$totalsponseredsalesnumber =$totalsponseredsalesnumber+1;
				}

				if(isset($value->v_buyer_commission) && $value->v_buyer_commission!=""){
					$totalcommisionearn=$totalcommisionearn+$value->v_buyer_commission;
				}

				if(isset($value->v_buyer_processing_fees) && $value->v_buyer_processing_fees!=""){
					$totalcommisionearn=$totalcommisionearn+$value->v_buyer_processing_fees;
				}

				if(isset($value->v_seller_commission) && $value->v_seller_commission!=""){
					$totalcommisionearn=$totalcommisionearn+$value->v_seller_commission;
				}

				if(isset($value->v_seller_processing_fees) && $value->v_seller_processing_fees!=""){
					$totalcommisionearn=$totalcommisionearn+$value->v_seller_processing_fees;
				}

			}
		}

		$data[]=array(
			'title'=>"Total Sales",
			'number'=>$totalsalesnumber,
			'amount'=>$totalsales
		);

		$data[]=array(
			'title'=>"Total Sponsored ads sale",
			'number'=>$totalsponseredsalesnumber,
			'amount'=>$totalsponseredsales,
		);

		$query = SellerprofileModel::query();
		if($startdate!="" && $enddate!=""){
			$query = $query->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$sellerdata = $query->where('e_sponsor_status',"start")->get();

		$totalAdsimp=0;	
		$totalAdsclick=0;	


		if(count($sellerdata)){
			foreach ($sellerdata as $key => $value) {
				if(isset($value->i_impression))	
				$totalAdsimp = $totalAdsimp+$value->i_impression;

				if(isset($value->i_clicks))	
				$totalAdsclick = $totalAdsimp+$value->i_clicks;
			}
		}

		$jobquery = JobsModel::query();
		if($startdate!="" && $enddate!=""){
			$jobquery = $jobquery->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$currentDate=date("Y-m-d");
		$jobdatalivecount = $jobquery->where('e_sponsor_status',"start")->get();

		if(count($jobdatalivecount)){
			foreach ($jobdatalivecount as $key => $value) {
				if(isset($value->i_impression))	
				$totalAdsimp = $totalAdsimp+$value->i_impression;

				if(isset($value->i_clicks))	
				$totalAdsclick = $totalAdsimp+$value->i_clicks;

			}
		}

		$data[]=array(
			'title'=>"Total Ads impressions",
			'number'=>$totalAdsimp,
			//'number'=>"0",
			'amount'=>"-",
		);
		
		$data[]=array(
			'title'=>"Total Ads clicks ",
			'number'=>$totalAdsclick,
			'number'=>"0",
			'amount'=>"-",
		);

		$query = OrderModel::query();
		$query = $query->where('e_transaction_type',"userplan");
    	$query = $query->where('e_seller_payment_status',"success");
    	if($startdate!="" && $enddate!=""){
			$query = $query->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$orderdata = $query->get();

		$totalplansales=0;
		$totalplansalesnumber=count($orderdata);

		if(count($orderdata)){
			foreach ($orderdata as $key => $value) {
				if(isset($value->v_amount)){
					$totalplansales =$totalplansales+$value->v_amount;
				}
			}
		}

		$data[]=array(
			'title'=>"Total Plan sales",
			'number'=>$totalplansalesnumber,
			'amount'=>$totalplansales
		);

		$currentstart=date("Y-m-d")."00:00:00";
		$currentend=date("Y-m-d")."23:59:59";

		$todayusercount = UsersModel::where("d_added",">=",$currentstart)->where("d_added","<",$currentend)->count();	

		$data[]=array(
			'title'=>"Total Sign ups Today",
			'number'=>$todayusercount,
			'amount'=>"-"
		);

		$todayusercount = UsersModel::where('e_status','active')->count();	

		$data[]=array(
			'title'=>"Total Members since inception of SkillBox ",
			'number'=>$todayusercount,
			'amount'=>"-"
		);

		$query = OrderModel::query();
		$query = $query->where('e_seller_payment_status',"refund");
    	if($startdate!="" && $enddate!=""){
			$query = $query->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$orderdata = $query->get();

		$totalrefund=0;
		$totalrefundnumber=count($orderdata);

		if(count($orderdata)){
			foreach ($orderdata as $key => $value) {
				if(isset($value->v_amount)){
					$totalrefund =$totalrefund+$value->v_amount;
				}
			}
		}
		$data[]=array(
			'title'=>"Total refunds",
			'number'=>$totalrefundnumber,
			'amount'=>$totalrefund
		);
		$data[]=array(
			'title'=>"Total commission earned ",
			'number'=>$totalcommisionearnnumber,
			'amount'=>$totalcommisionearn
		);

		$jobquery = JobsModel::query();
		if($startdate!="" && $enddate!=""){
			$jobquery = $jobquery->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$currentDate=date("Y-m-d");
		$jobquery = $jobquery->where('d_expiry_date','>',$currentDate);	
		$jobdatalivecount = $jobquery->where('e_sponsor_status',"start")->count();
		
		$data[]=array(
			'title'=>"Total no. of Sponsored jobs live",
			'number'=>$jobdatalivecount,
			'amount'=>"-"
		);

		$query = SellerprofileModel::query();
		if($startdate!="" && $enddate!=""){
			$query = $query->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$sellerlivecount = $query->where('e_sponsor_status',"start")->count();

		$data[]=array(
			'title'=>"Total no. of Sponsored profile live",
			'number'=>$sellerlivecount,
			'amount'=>"-"
		);

		$totalloginnow = UsersModel::where("e_login","yes")->count();	

		$data[]=array(
			'title'=>"Total active logins right now",
			'number'=>$totalloginnow,
			'amount'=>"-"
		);

		$_data=array(
			'data'=>$data,
		);
		
		return view('admin/dashboard/report' , $_data);


	}
	

}