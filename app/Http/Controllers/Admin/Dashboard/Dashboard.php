<?php
namespace App\Http\Controllers\Admin\Dashboard;

use Request, Hash, Lang;
use App\Http\Controllers\Controller;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Users\Order as OrderModel;
use App\Models\Blogs\Blogs as BlogsModel;
use App\Models\Support\Support as SupportModel;

class Dashboard extends Controller {

	 protected $section;

	public function __construct(){
		$this->section = Lang::get('section.dashboard');
	}


	public function index() {
		
		$data = Request::all();
		$startdate="";

		if(isset($data['d_start_date']) && $data['d_start_date']!=""){
			$startdate = date("Y-m-d",strtotime($data['d_start_date']));		
			$response['d_start_date']=$data['d_start_date'];
		}

		$enddate="";
		if(isset($data['d_end_date']) && $data['d_end_date']!=""){
			$enddate = date("Y-m-d",strtotime($data['d_end_date']));		
			$response['d_end_date']=$data['d_end_date'];
		}
	
		$response['totaluser']=0;
		$response['totalbasicuser']=0;
		$response['totalstandarduser']=0;
		$response['totalpremiumuser']=0;

		$response['totalsales']=0;
		$response['totalMonthlySubscribersSales ']=0;
		$response['totalAnnualSubscribersSales']=0;

		$response['totalCoursesLive']=0;
		$response['totalCoursesSold']=0;
		$response['totalCoursesSales']=0;
		$response['totalCommissionbyBuyers']=0;
		$response['totalCommissionbySellers']=0;
		$response['totalCommissionCourseSales']=0;

		$response['totalActiveJobs']=0;
		$response['totalTicketOpen']=0;
		$response['totalTicketClosed']=0;
		$response['totalJobsApplications']=0;
		$response['totalJobsApplied']=0;
		$response['totalProccessing']=0;


		$userquery = UsersModel::query()->where('i_delete','!=',"1");
		if($startdate!="" && $enddate!=""){
			$userquery = $userquery->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}

		$userdata = $userquery->get();

		$response['totaluser'] = count($userdata);
		if(count($userdata)){
			foreach ($userdata as $key => $value) {
				if(isset($value->v_plan['id']) && $value->v_plan['id']=="5a65b48cd3e812a4253c9869"){
					$response['totalbasicuser'] = $response['totalbasicuser']+1;			
				}else if(isset($value->v_plan['id']) && $value->v_plan['id']=="5a65b757d3e8125e323c986a"){
					$response['totalstandarduser'] = $response['totalstandarduser']+1;
				}else if(isset($value->v_plan['id']) && $value->v_plan['id']=="5a65b9f4d3e8124f123c986c"){
					$response['totalpremiumuser'] = $response['totalpremiumuser']+1;
				}	
			}	
		}

		$coursequery = CoursesModel::query()->where('i_delete','!=',"1");
		if($startdate!="" && $enddate!=""){
			$coursequery = $coursequery->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$coursedata = $coursequery->get();

		if(count($coursedata)){
			foreach ($coursedata as $key => $value) {
				if(isset($value->e_status) && $value->e_status=="published"){
					$response['totalCoursesLive']=$response['totalCoursesLive']+1;
				}
			}
		}


		$courseorderquery = OrderModel::query()->where('i_delete','!=',"1")->where('e_transaction_type','course')->where('e_payment_status','success');
		if($startdate!="" && $enddate!=""){
			$courseorderquery = $courseorderquery->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$courseorderdata = $courseorderquery->get();
		
		$response['totalCoursesSold'] = count($courseorderdata);

		if(count($courseorderdata)){
			foreach ($courseorderdata as $key => $value) {
				if(isset($value->v_amount) && $value->v_amount!=""){
					$response['totalCoursesSales']=$response['totalCoursesSales']+$value->v_amount;
				}

				if(isset($value->v_buyer_commission) && $value->v_buyer_commission!=""){
					$response['totalCommissionCourseSales']=$response['totalCommissionCourseSales']+$value->v_buyer_commission;
				}
				if(isset($value->v_seller_commission) && $value->v_seller_commission!=""){
					$response['totalCommissionCourseSales']=$response['totalCommissionCourseSales']+$value->v_seller_commission;
				}
				if(isset($value->v_amount) && $value->v_amount!=""){
					$response['totalsales']=$response['totalsales']+$value->v_amount;
				}

				if(isset($value->v_buyer_processing_fees) && $value->v_buyer_processing_fees!=""){
					$response['totalProccessing']=$response['totalProccessing']+$value->v_buyer_processing_fees;
				}
				if(isset($value->v_seller_processing_fees) && $value->v_seller_processing_fees!=""){
					$response['totalProccessing']=$response['totalProccessing']+$value->v_seller_processing_fees;
				}

				//$response['totalProccessing']

			}
		}

		$skillorderquery = OrderModel::query()->where('e_transaction_type','skill')->where('e_payment_status','success');
		if($startdate!="" && $enddate!=""){
			$skillorderquery = $skillorderquery->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$skillorderdata = $skillorderquery->get();

		if(count($skillorderdata)){
			foreach ($skillorderdata as $key => $value) {

				if(isset($value->v_buyer_commission) && $value->v_buyer_commission!=""){
					$response['totalCommissionbyBuyers']=$response['totalCommissionbyBuyers']+$value->v_buyer_commission;
				}

				if(isset($value->v_seller_commission) && $value->v_seller_commission!=""){
					$response['totalCommissionbySellers']=$response['totalCommissionbySellers']+$value->v_seller_commission;
				}

				if(isset($value->v_amount) && $value->v_amount!=""){
					$response['totalsales']=$response['totalsales']+$value->v_amount;
				}

				if(isset($value->v_buyer_processing_fees) && $value->v_buyer_processing_fees!=""){
					$response['totalProccessing']=$response['totalProccessing']+$value->v_buyer_processing_fees;
				}
				if(isset($value->v_seller_processing_fees) && $value->v_seller_processing_fees!=""){
					$response['totalProccessing']=$response['totalProccessing']+$value->v_seller_processing_fees;
				}
				

			}
		}


		$jobquery = JobsModel::query()->where('i_delete','!=',"1");
		if($startdate!="" && $enddate!=""){
			$jobquery = $jobquery->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$jobdata = $jobquery->get();
		$response['totalJobsApplications']=count($jobdata);

		
		if(count($jobdata)){
			foreach ($jobdata as $key => $value) {
				$currentdata=date("Y-m-d");

				if(isset($value->d_expiry_date) && $currentdata<=$value->d_expiry_date){
					$response['totalActiveJobs']=$response['totalActiveJobs']+$value->v_buyer_commission;
				}
				if(isset($value->i_applied) && $value->i_applied!=""){
					$response['totalJobsApplied']=$response['totalJobsApplied']+$value->v_buyer_commission;
				}
			}
		}

		$supportquery = SupportModel::query();
		if($startdate!="" && $enddate!=""){
			$supportquery = $supportquery->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$supportdata = $supportquery->get();

		if(count($supportdata)){
			foreach ($supportdata as $key => $value) {

				if(isset($value->e_status) && $value->e_status=="open"){
					$response['totalTicketOpen']=$response['totalTicketOpen']+1;
				}
				if(isset($value->e_status) && $value->e_status=="close"){
					$response['totalTicketClosed']=$response['totalTicketClosed']+1;
				}
				
			}
			
		}

		$plansales = OrderModel::query()->where('i_delete','!=',"1")->where('e_transaction_type','userplan')->where('e_payment_status','success');
		if($startdate!="" && $enddate!=""){
			$plansales = $plansales->where("d_added",">=",$startdate)->where("d_added","<",$enddate);
		}
		$plansales = $plansales->get();
		
		$response['totalmonthlysales'] = 0;
		$response['totalanualsales'] = 0;

		if(count($plansales)){
			foreach ($plansales as $key => $value) {
					
				if(isset($value->v_order_detail['v_plan_duration']) && $value->v_order_detail['v_plan_duration']=="monthly"){
					$response['totalmonthlysales']=$response['totalmonthlysales']+$value->v_amount;		
				}else{
					$response['totalanualsales']=$response['totalanualsales']+$value->v_amount;		
				}					
				$response['totalsales']=$response['totalsales']+$value->v_amount;

			}
		}




		//$latestorders = OrderModel::orderBy("d_added","DESC")->limit(6)->get();
		$latestuser = UsersModel::where("e_status","active")->where('i_delete','!=',"1")->orderBy("d_added","DESC")->limit(6)->get();

		$latestorders=array();
		$response['latestuser']=$latestuser;
		$response['latestorders']=$latestorders;

		return view('admin/dashboard/dashboard' , $response);

	}
	

}