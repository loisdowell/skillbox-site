<?php
namespace App\Http\Controllers\Admin\Users;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Users\Order as OrderModel;
use App\Models\Users\Users as UsersModel;

class SellerOrders extends Controller {


	public function index($id) {
		$seller_user = UsersModel::find($id);
		$seller_orders = OrderModel::query();
		$seller_orders = $seller_orders->where(function($query ){
		        $query->where('e_transaction_type', 'course')
		              ->orWhere('e_transaction_type', 'skill');
		    	});
		$seller_orders = $seller_orders->where('i_seller_id',$id);
		$seller_orders = $seller_orders->get();
		$data=array(
			'seller_user'=>$seller_user,
			'seller_orders'=>$seller_orders,
		);
		return view('admin/users/seller-orders', $data);
		
	}
}