<?php
namespace App\Http\Controllers\Admin\Users;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Users\Sellerreview as SellerreviewModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Users as UsersModel;

class SellerReviews extends Controller {


	public function index($id) {
		
		$seller_user = UsersModel::find($id);
		$seller_review = SellerreviewModel::where('i_seller_id',$id)->get();
		$data=array(
			'seller_user'=>$seller_user,
			'seller_review'=>$seller_review,
		);
		return view('admin/users/seller-reviews', $data);
		
	}
	
	public function SkillReview($id) {
		
		$seller_user = UsersModel::find($id);
		$seller_review = SellerreviewModel::where('i_seller_id',$id)->get();
		$data=array(
			'seller_user'=>$seller_user,
			'seller_review'=>$seller_review,
		);
		return view('admin/users/seller-reviews', $data);
		
	}

	public function CourseReview($id) {
		
		$seller_user = UsersModel::find($id);
		$seller_review = CoursesReviews::where('i_seller_id',$id)->get();
		$data=array(
			'seller_user'=>$seller_user,
			'seller_review'=>$seller_review,
		);
		return view('admin/users/seller-course-reviews', $data);
		
	}
}