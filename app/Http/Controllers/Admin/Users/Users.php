<?php
namespace App\Http\Controllers\Admin\Users;

use Request, Hash, Lang, File, GeneralHelper, Storage,Crypt;
use App\Http\Controllers\Controller;
use App\Models\Users\Users as UsersModel;
use App\Models\Categories as CategoriesModel;
use App\Models\Skills as SkillsModel;
use App\Models\Country as CountryModel;
use App\Models\State as StateModel;
use App\Models\City as CityModel;
use App\Models\Users\UserCompany as UserCompanyModel;
use App\Models\Users\UserAddresses as UserAddressesModel;
use App\Models\Users\SellerInfo as SellerInfoModel;
use App\Models\Users\UserPaymentInfo as UserPaymentInfoModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\SellerEducation as SellerEducationModel;
use App\Models\Users\SellerEmployment as SellerEmploymentModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\Buyerreview as BuyerreviewModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Sellerreview as SellerreviewModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Users\Order as OrderModel;

class Users extends Controller {

	protected $section;
	private $mangopay;
	public function __construct(\MangoPay\MangoPayApi $mangopay){ 
		$this->section = "Users";
		$this->mangopay = $mangopay;
	}
	
	public function index() {
		$datas = Request::all();
		$query = UsersModel::query();
		$data = $query->where('i_delete','!=',"1")->orderBy('id','DESC')->get();
		if(isset($datas['dd']) && $datas['dd'] == '1'){
			dd($data);
		}
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/users/users', $_data);
	}

	public function Add() {

		$categories = CategoriesModel::where("e_status","active")->get();
		$skills = SkillsModel::where("e_status","active")->get();
		$country = CountryModel::where("e_status","active")->get();
		
		$_data = array(
			'view'			=>"add",
			'categories' 	=> $categories,
			'skills' 		=> $skills,
			'country' 		=> $country,
		);
		return view('admin/users/users', $_data);
	}
	
	public function Edit($id="") {
		
		$categories 		= CategoriesModel::where("e_status","active")->get();
		$skills 			= SkillsModel::where("e_status","active")->get();
		$country 			= CountryModel::where("e_status","active")->get();
		$userdata 		= UsersModel::find($id);
		$uid 				= $userdata['_id'];
	    $useraddressdata  = UserAddressesModel::where("i_user_id",$uid)->first();
	    $paymentdata  	= UserPaymentInfoModel::where("i_user_id",$uid)->first();
	    $sellerInfoData  	= SellerInfoModel::where("i_user_id",$uid)->first();
	    $sellerEduData  	= SellerEducationModel::where("i_user_id",$uid)->first();
	    $sellerEmpData  	= SellerEmploymentModel::where("i_user_id",$uid)->first();
	    $companyData  	= UserCompanyModel::where("i_user_id",$uid)->first();
      
      	$_data  = array(
            'view'				=> "edit",
            'data'				=> $userdata,
            'useraddressdata' 	=> $useraddressdata,
            'paymentdata'		=> $paymentdata,
            'categories'		=> $categories,
            'skills' 			=> $skills,
			'country' 			=> $country,
			'sellerInfoData' 	=> $sellerInfoData,
			'sellerEduData' 	=> $sellerEduData,
			'sellerEmpData' 	=> $sellerEmpData,
			'companyData' 		=> $companyData
        );
        return view('admin/users/users', $_data);
    }

	public function updateUserProfile($action="",$id=""){
			
			$post_data = Request::all();
		
			if(!isset($post_data['i_newsletter'])){
				$post_data['i_newsletter']="off";
			}	

			if(isset($post_data['_token'])){
				unset($post_data['_token']);
			}

			if($action=="add"){

				$existEmail = UsersModel::where('v_email',$post_data['v_email'])->get();
		        if(count($existEmail)){
		        	return redirect( 'admin/users/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
		        }	
		        if(Request::file()){
					$image= Request::file('v_image');
					$ext = $image->getClientOriginalExtension();
					
					if(count($image)){
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
							
							$fileName = 'profile-'.time().'.'.$image->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_image'] = 'users/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$image), 'public');
						}else{
							return redirect( 'admin/users/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}
				}

				$post_data['v_buyer_online_service'] = "active";
				$post_data['v_buyer_inperson_service'] = "inactive";
				$post_data['v_seller_online_service'] = "inactive";
				$post_data['v_seller_inperson_service'] = "inactive";
				$post_data['e_status']="active";
				$post_data['e_email_confirm']="no";
				$post_data['i_total_avg_review']=0;
				$post_data['i_total_review']=0;
				$post_data['i_course_total_review']=0;
				$post_data['i_course_total_avg_review']=0;

				$res = self::createMangopayUser($post_data);
				$post_data['i_mangopay_id']= $res['i_mangopay_id'];
				$post_data['i_wallet_id']= $res['i_wallet_id'];
				
				$post_data['password'] = Hash::make($post_data['password']);
				$post_data['d_added']=date("Y-m-d h:i:s");
		        $post_data['d_modified']=date("Y-m-d h:i:s");

		        UsersModel::create($post_data);
		        return redirect('admin/users')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

			}else if($action=="edit"){

				$existEmail = UsersModel::where('v_email',$post_data['v_email'])->where('_id',"!=",$id)->get();
				if(count($existEmail)){
	        		return redirect( 'admin/users/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ $this->section ]));    
	      		}
	      		
	      		if( Request::file() ){
					
					$image = Request::file('v_image');
					$ext   = $image->getClientOriginalExtension();

					if(count($image) ){
						
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){

							$existdata = UsersModel::find($id);	

							if(isset($existdata->v_image) && $existdata->v_image!=""){
								if(Storage::disk('s3')->exists($existdata->v_image)){
									Storage::disk('s3')->Delete($existdata->v_image);
								}
							}

							$fileName = 'profile-'.time().'.'.$image->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_image'] = 'users/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$image), 'public');

						}else{
							return redirect( 'admin/users/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}
				}

				if(isset($post_data['password']) && $post_data['password']!=""){
					$post_data['password'] = Hash::make($post_data['password']);
				}

				$post_data['d_modified']=date("Y-m-d h:i:s");
				UsersModel::find($id)->update($post_data);
				return redirect( 'admin/users/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
			}
	}

	public function EditPassword($id=""){
    	
	  	$userdata=UsersModel::find($id);
	  	$_data = array(
            'view'=> "edit",
            'data'=> $userdata,
        );
	    return view('admin/users/users_password', $_data);
  	}

 	public function updateUserPassword($action="",$id=""){
		
		$post_data = Request::all();
		
		if(isset($post_data['_token'])){
			unset($post_data['_token']);
		}
		if(isset($post_data['cnpassword'])){
			unset($post_data['cnpassword']);
		}

		$userdata = UsersModel::find($id);
		
		if(count($userdata)){

			$username = "";
			if(isset($userdata->v_fname)){
			   	$username = $userdata->v_fname;
			}
			if(isset($userdata->v_lname)){
			   	$username .= " ".$userdata->v_lname;
			}

			$useremail="";
			if(isset($userdata->v_email)){
			   	$useremail .= " ".$userdata->v_email;
			}

			$data=array(
				'name'=>$username,
				'password'=>$post_data['password'],
			);

			$mailcontent = EmailtemplateHelper::ResetPasswordAdmin($data);
			$subject = "Reset password";
			$subject = EmailtemplateHelper::EmailTemplateSubject("5b17abfa76fbae34dd205c03");
			
			$mailids=array(
				$username=>$useremail
			);
			$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

			if(isset($post_data['password'])){
				$post_data['password'] = Hash::make($post_data['password']);
			}

			$post_data['d_modified']=date("Y-m-d h:i:s");
			
			$userdata->update($post_data);
			
			return redirect( 'admin/users/edit/password/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
		}
		else{
			return redirect('admin/users/edit/password/'.$id)->with(['warning' => 'Something went wrong']);	
		}	
    }

    public function EditBilling($id=""){
    	
	  	$userdata=UsersModel::find($id);
	  	$_data = array(
            'view'=> "edit",
            'data'=> $userdata,
        );
	    return view('admin/users/users_billing', $_data);
    }
   
    public function updateUserBilling($action="",$id=""){
		
		$post_data = Request::all();
		
		if(isset($post_data['billing_detail']['v_type']) && $post_data['billing_detail']['v_type']=="bank"){
			$post_data['billing_detail']['paypal_email']="";
		}else{
			$post_data['billing_detail']['v_bankname']="";
			$post_data['billing_detail']['v_accountno']="";
			$post_data['billing_detail']['v_ifsc']="";
			$post_data['billing_detail']['v_account_holder_name']="";
		}

		if(isset($post_data['_token'])){
			unset($post_data['_token']);
		}
		$post_data['d_modified']=date("Y-m-d h:i:s");
		UsersModel::find($id)->update($post_data);
		return redirect( 'admin/users/edit/billing/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
    }

    public function EditSellerInperson($id=""){
    	
    	$categories = CategoriesModel::where("e_status","active")->get();
	  	$country = CountryModel::where("e_status","active")->get();
	  	$userdata = UsersModel::find($id);
	  	$sellerdata = SellerprofileModel::where("i_user_id",$id)->where('v_service','inperson')->first();

	  	$skillsdata=array();
	  	if(count($sellerdata) && isset($sellerdata->i_category_id)){
	  		$skillsdata  = SkillsModel::where("e_status","active")->where('i_category_id',$sellerdata->i_category_id)->get();
	  	}


		$_data = array(
	        'view'		=> "edit",
	        'data'		=> $sellerdata,
	        'userdata'	=> $userdata,
	        'categories'=> $categories,
	        'skilldata' => $skillsdata,
			'country' 	=> $country,
		);
		return view('admin/users/users_seller', $_data);
	}

    public function EditSellerOnline($id=""){
    	
    	$categories = CategoriesModel::where("e_status","active")->get();
	  	$country = CountryModel::where("e_status","active")->get();
	  	$userdata = UsersModel::find($id);
	  	$sellerdata = SellerprofileModel::where("i_user_id",$id)->where('v_service','online')->first();

	  	$skillsdata=array();
	  	if(count($sellerdata) && isset($sellerdata->i_category_id)){
	  		$skillsdata  = SkillsModel::where("e_status","active")->where('i_category_id',$sellerdata->i_category_id)->get();
	  	}

		$_data = array(
	        'view'		=> "edit",
	        'data'		=> $sellerdata,
	        'userdata'	=> $userdata,
	        'categories'=> $categories,
	        'skilldata' => $skillsdata,
			'country' 	=> $country,
		);
		return view('admin/users/users_seller', $_data);
    }

    public function updateUserSellerInperson($userid=""){
		
		$data = Request::all();
	
		if(isset($data['_token'])){
			unset($data['_token']);
		}
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service','inperson')->first();

		if(isset($existProfile->v_work_photos) && count($existProfile->v_work_photos)){
			$data['v_work_photos'] = $existProfile->v_work_photos;
		}

		if(Request::hasFile('work_photo')) {
			$destination = public_path('uploads/userprofiles');
			$image = Request::file('work_photo');
			foreach($image as $file){
	          	$fileName = 'work_photo-'.time().'.'.$file->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_work_photos'][] = 'users/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$file), 'public');
		    }	
	        unset($data['information']['work_photo']);
		}

		if(isset($existProfile->v_work_video) && count($existProfile->v_work_video)){
			$data['v_work_video'] = $existProfile->v_work_video;
		}
		if(Request::hasFile('work_video')) {
		
			$image = Request::file('work_video');
			foreach($image as $file){
	          	$fileName = 'work_video-'.time().'.'.$file->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_work_video'][] = 'users/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$file), 'public');
		    }

		    // $image = Request::file('work_video');
			// if(isset($existProfile->v_work_video) && $existProfile->v_work_video!=""){
			// 	if(Storage::disk('s3')->exists($existProfile->v_work_video)){
			// 		Storage::disk('s3')->Delete($existProfile->v_work_video);
			// 	}
			// }
			// $fileName = 'work_video-'.time().'.'.$image->getClientOriginalExtension();
			// $fileName = str_replace(' ', '_', $fileName);
			// $data['v_work_video'] = 'users/video/' . $fileName;
			// $uploadS3 = Storage::disk('s3')->put('/users/video/'.$fileName, File::get((string)$image), 'public');

			if(isset($data['information']['work_video'])){
				unset($data['information']['work_video']);	
			}
		}

		if(Request::hasFile('introducy_video')) {
			$image = Request::file('introducy_video');
			if(isset($existProfile->v_introducy_video) && $existProfile->v_introducy_video!=""){
				if(Storage::disk('s3')->exists($existProfile->v_introducy_video)){
					Storage::disk('s3')->Delete($existProfile->v_introducy_video);
				}
			}
			$fileName = 'introducy_video-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$data['v_introducy_video'] = 'users/video/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/video/'.$fileName, File::get((string)$image), 'public');
			
			if(isset($data['information']['introducy_video'])){
				unset($data['information']['introducy_video']);	
			}
		}

		if(isset($data['work_photo'])){
			unset($data['work_photo']);
		}
		if(isset($data['introducy_video'])){
			unset($data['introducy_video']);
		}
		if(isset($data['work_video'])){
			unset($data['work_video']);
		}

		if(count($existProfile)){
			
			$data['d_modified']=date("Y-m-d H:i:s");
			SellerprofileModel::where('i_user_id',$userid)->update($data);	

		}else{
			$data['i_total_review'] = 0;
			$data['i_total_avg_review'] = 0;
			$data['e_status'] = "active";
			$data['v_service'] = "inperson";
			$data['i_user_id'] = $userid;
			$data['d_added']=date("Y-m-d H:i:s");
			$data['d_modified']=date("Y-m-d H:i:s");
			SellerprofileModel::create($data);	
		}
		return redirect( 'admin/users/edit/sellerinperson/'.$userid)->with('success',Lang::get('message.common.detailUpdated',['section' => $this->section ])); 
    }

    public function updateUserSellerOnline($userid=""){
		
		$data = Request::all();
	
		if(isset($data['_token'])){
			unset($data['_token']);
		}
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service','online')->first();

		if(isset($existProfile->v_work_photos) && count($existProfile->v_work_photos)){
			$data['v_work_photos'] = $existProfile->v_work_photos;
		}

		if(Request::hasFile('work_photo')) {
			$destination = public_path('uploads/userprofiles');
			$image = Request::file('work_photo');
			foreach($image as $file){
	          	$fileName = 'work_photo-'.time().'.'.$file->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_work_photos'][] = 'users/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$file), 'public');
		    }	
	        unset($data['information']['work_photo']);
		}

		if(Request::hasFile('work_video')) {
				
			$image = Request::file('work_video');
			if(isset($existProfile->v_work_video) && $existProfile->v_work_video!=""){
				if(Storage::disk('s3')->exists($existProfile->v_work_video)){
					Storage::disk('s3')->Delete($existProfile->v_work_video);
				}
			}
			$fileName = 'work_video-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$data['v_work_video'] = 'users/video/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/video/'.$fileName, File::get((string)$image), 'public');

			if(isset($data['information']['work_video'])){
				unset($data['information']['work_video']);	
			}
		}

		if(Request::hasFile('introducy_video')) {
			$image = Request::file('introducy_video');
			if(isset($existProfile->v_introducy_video) && $existProfile->v_introducy_video!=""){
				if(Storage::disk('s3')->exists($existProfile->v_introducy_video)){
					Storage::disk('s3')->Delete($existProfile->v_introducy_video);
				}
			}
			$fileName = 'introducy_video-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$data['v_introducy_video'] = 'users/video/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/video/'.$fileName, File::get((string)$image), 'public');
			
			if(isset($data['information']['introducy_video'])){
				unset($data['information']['introducy_video']);	
			}
		}

		if(isset($data['work_photo'])){
			unset($data['work_photo']);
		}
		if(isset($data['introducy_video'])){
			unset($data['introducy_video']);
		}
		if(isset($data['work_video'])){
			unset($data['work_video']);
		}

		if(count($existProfile)){
			
			$data['d_modified']=date("Y-m-d H:i:s");
			SellerprofileModel::where('i_user_id',$userid)->update($data);	

		}else{
			$data['i_total_review'] = 0;
			$data['i_total_avg_review'] = 0;
			$data['e_status'] = "active";
			$data['v_service'] = "online";
			$data['i_user_id'] = $userid;
			$data['d_added']=date("Y-m-d H:i:s");
			$data['d_modified']=date("Y-m-d H:i:s");
			SellerprofileModel::create($data);	
		}
		return redirect( 'admin/users/edit/sellerinperson/'.$userid)->with('success',Lang::get('message.common.detailUpdated',['section' => $this->section ])); 
    }
    public function updateUserSeller($userid=""){
		
		$data = Request::all();

		if(isset($data['_token'])){
			unset($data['_token']);
		}
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->first();

		if(isset($data['information']) && count($data['information'])){
			
			if(Request::hasFile('introducy_video')) {
				$image = Request::file('introducy_video');
				if(isset($existProfile->introducy_video) && $existProfile->introducy_video!=""){
					if(Storage::disk('s3')->exists($existProfile->introducy_video)){
						Storage::disk('s3')->Delete($existProfile->introducy_video);
					}
				}
				$fileName = 'introducy_video-'.time().'.'.$image->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_introducy_video'] = 'users/video/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/users/video/'.$fileName, File::get((string)$image), 'public');
				unset($data['information']['introducy_video']);
			}

			if(Request::hasFile('work_video')) {
				
				$image = Request::file('work_video');

				if(isset($existProfile->work_video) && $existProfile->work_video!=""){
					if(Storage::disk('s3')->exists($existProfile->work_video)){
						Storage::disk('s3')->Delete($existProfile->work_video);
					}
				}
				$fileName = 'work_video-'.time().'.'.$image->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_work_video'] = 'users/video/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/users/video/'.$fileName, File::get((string)$image), 'public');
				unset($data['information']['work_video']);
			}

			if(isset($existProfile->v_work_photos) && count($existProfile->v_work_photos)){
				$data['v_work_photos'] = $existProfile->v_work_photos;
			}

			if(Request::hasFile('work_photo')) {
				$destination = public_path('uploads/userprofiles');
				$image = Request::file('work_photo');
				foreach($image as $file){
		          	$fileName = 'work_photo-'.time().'.'.$file->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$data['v_work_photos'][] = 'users/' . $fileName;
					$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$file), 'public');
			    }	
		        unset($data['information']['work_photo']);
			}

		}

		if(isset($data['work_photo'])){
			unset($data['work_photo']);
		}
		if(isset($data['introducy_video'])){
			unset($data['introducy_video']);
		}
		if(isset($data['work_video'])){
			unset($data['work_video']);
		}

		if(isset($data['billing_detail']) && count($data['billing_detail'])){
			$billing_detail['billing_detail'] = $data['billing_detail'];
			unset($data['billing_detail']);	
			UsersModel::where('_id',$userid)->update($billing_detail);	
		}

		if(isset($data['information']['v_tags'])){
			$data['v_tags'] = $data['information']['v_tags'];
			unset($data['information']['v_tags']);	
		}

		if(count($existProfile)){
		
			$data['d_modified']=date("Y-m-d H:i:s");
			SellerprofileModel::where('i_user_id',$userid)->update($data);	

		}else{
			$data['i_user_id'] = $userid;
			$data['d_added']=date("Y-m-d H:i:s");
			$data['d_modified']=date("Y-m-d H:i:s");
			SellerprofileModel::create($data);	
		}

	
		return redirect( 'admin/users/edit/seller/'.$userid)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
    }
    
    public function Action($action="",$id="") {

        $post_data = Request::all();
       

        if($action=="delete"){
        	
        	$userdata = UsersModel::find($id);
        	if(count($userdata)){
        		
        		$updateuser['i_delete']="1";
	        	$updateuser['v_email']="";
	        	UsersModel::find($id)->update($updateuser);

	        	$update['i_delete']="1";
	        	SellerprofileModel::where('i_user_id',$id)->update($update);
	        	JobsModel::where('i_user_id',$id)->update($update);
	        	CoursesModel::where('i_user_id',$id)->update($update);
        	}

        	return redirect( 'admin/users')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
       
        }else if($action=="resetpassword"){
        	
        	$userdata = UsersModel::find($id);

        	$username = "";
			if(isset($userdata->v_fname)){
			   	$username = $userdata->v_fname;
			}
			if(isset($userdata->v_lname)){
			   	$username .= " ".$userdata->v_lname;
			}

			$encryptedid = Crypt::encrypt($userdata->id);
			$passwordreseturl= url('reset-password').'/'.$encryptedid;

			$useremail="";
			if(isset($userdata->v_email)){
			   	$useremail .= " ".$userdata->v_email;
			}

			$data=array(
				'name'=>$username,
				'passwordreseturl'=>$passwordreseturl,
			);

			$mailcontent = EmailtemplateHelper::ForgotPassword($data);
			$subject = "Reset Password";
			$subject = EmailtemplateHelper::EmailTemplateSubject("5a60a0f1d3e812bc4b3c986a");
			$mailids=array(
				$username=>$useremail
			);

			$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
			return redirect( 'admin/users')->with( 'success', "Email sent successfully for reset password"); 


        }

    }
	
    public function status( $id, $status ) {
		
		if ($status == 'active') {
			
			//User All item Inactive
			$SellerProfileInperson = SellerprofileModel::where('i_user_id',$id)->where('v_service','inperson')->first();
			if(count($SellerProfileInperson)){
				$SellerProfileInperson->e_status="inactive";
				$SellerProfileInperson->save();
			}

			$SellerProfileOnline = SellerprofileModel::where('i_user_id',$id)->where('v_service','online')->first();
			if(count($SellerProfileOnline)){
				$SellerProfileOnline->e_status="inactive";
				$SellerProfileOnline->save();
			}
			
			$jobdata = JobsModel::where('i_user_id',$id)->get();
			if(count($jobdata)){
				foreach ($jobdata as $key => $value) {
					$value->e_status="inactive";
					$value->save();
				}
			}

			$coursesdata = CoursesModel::where('i_user_id',$id)->get();
			if(count($coursesdata)){
				foreach ($coursesdata as $key => $value) {
					$value->e_status="unpublished";
					$value->save();
				}
			}
			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$SellerProfileInperson = SellerprofileModel::where('i_user_id',$id)->where('v_service','inperson')->first();
			if(count($SellerProfileInperson)){
				$SellerProfileInperson->e_status="active";
				$SellerProfileInperson->save();
			}

			$SellerProfileOnline = SellerprofileModel::where('i_user_id',$id)->where('v_service','online')->first();
			if(count($SellerProfileOnline)){
				$SellerProfileOnline->e_status="active";
				$SellerProfileOnline->save();
			}
			$jobdata = JobsModel::where('i_user_id',$id)->get();
			if(count($jobdata)){
				foreach ($jobdata as $key => $value) {
					$value->e_status="active";
					$value->save();
				}
			}

			$coursesdata = CoursesModel::where('i_user_id',$id)->get();
			if(count($coursesdata)){
				foreach ($coursesdata as $key => $value) {
					$value->e_status="published";
					$value->save();
				}
			}

			$data = [
				'e_status' => 'active',
		   		'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		
		}
		
		if (isset($data) && count($data)) {
			$user = UsersModel::find($id);
			$user->update($data);
		}
		return redirect('admin/users')->with('success', $message);
	}

	public function subCategories() {
	  
	  $post_data = Request::all();
	  $response = array();
	  $optingStr = '<option value="">- select -</option>';
	  $cat_id = $post_data['category_id'];
	  $sub_cat_id = $post_data['sub_category_id'];
	  
	  $categories = CategoriesModel::where("i_parent_id",$cat_id)->where("e_status","active")->get();
	  if(count($categories)){
	  	foreach ($categories as $key => $val) {
	  		$a="";
	  		if($val['_id']==$sub_cat_id){
	  			$a="selected";
	  		}
	  		$optingStr.= '<option value="'.$val['_id'].'" '.$a.'>'.$val['v_name'].'</option>' ;
	  	}
	  }
	  $response['optingStr']=$optingStr;
	  $response['status'] = 1;
	  
	  echo json_encode($response); 
	  exit;   
	}

	public function skills() {
	  $post_data = Request::all();
	  $response = array();
	  $optingStr = '<option value="">- select -</option>';
	  $cat_id = $post_data['category_id'];
	  $selected_skill_id = $post_data['selected_skill_id'];
	  $categories = SkillsModel::where("i_category_id",$cat_id)->where("e_status","active")->get();
	  if(count($categories)){
	  	foreach ($categories as $key => $val) {
	  		$a="";
	  		if($val['v_name']==$selected_skill_id){
	  			$a="selected";
	  		}
	  		$optingStr.= '<option value="'.$val['v_name'].'" '.$a.'>'.$val['v_name'].'</option>' ;
	  	}
	  }
	  $response['optingStr']=$optingStr;
	  $response['status'] = 1;
	  
	  echo json_encode($response); 
	  exit;   
	}

	public function state() {
	  $post_data = Request::all();
	  $response = array();
	  $optingStr = '<option value="">- select -</option>';
	  $country_id = $post_data['country_id'];
	  $state_id = $post_data['selected_state_id'];
	  $state = StateModel::where("i_country_id",$country_id)->where("e_status","active")->get();
	  if(count($state)){
	  	foreach ($state as $key => $val) {
	  		$a="";
	  		if($val['_id']==$state_id){
	  			$a="selected";
	  		}
	  		$optingStr.= '<option value="'.$val['_id'].'" '.$a.'>'.$val['v_title'].'</option>' ;
	  	}
	  }
	  $response['optingStr']=$optingStr;
	  $response['status'] = 1;
	  
	  echo json_encode($response); 
	  exit;   
	}

	public function city() {
	  $post_data = Request::all();
	  $response = array();
	  $optingStr = '<option value="">- select -</option>';
	  $state_id = $post_data['state_id'];
	  $city_id = $post_data['selected_city_id'];
	  $city = CityModel::where("i_state_id",$state_id)->where("e_status","active")->get();
	  if(count($city)){
	  	foreach ($city as $key => $val) {
	  		$a="";
	  		if($val['_id']==$city_id){
	  			$a="selected";
	  		}
	  		$optingStr.= '<option value="'.$val['_id'].'" '.$a.'>'.$val['v_title'].'</option>' ;
	  	}
	  }
	  $response['optingStr']=$optingStr;
	  $response['status'] = 1;
	  
	  echo json_encode($response); 
	  exit;   
	}

	public function removeImage(){

		$data = Request::all();
		$response = array();

		$userid = auth()->guard('web')->user()->_id;
		$jobid = "";

		if(!isset($data['i_seller_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['i_seller_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['v_photo_name'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['v_photo_name']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$jobdata = SellerprofileModel::find($data['i_seller_id']);
		
		if(!count($jobdata)){

			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}

		$update['v_work_photos']=array();
		if(Storage::disk('s3')->exists($data['v_photo_name'])){
			Storage::disk('s3')->Delete($data['v_photo_name']);
		}
		if(count($jobdata->v_work_photos)){
			foreach ($jobdata->v_work_photos as $key => $value) {
				if($value!=$data['v_photo_name']){
					$update['v_work_photos'][]=$value;					
				}
			}
		}
		$update['d_modified']=date("Y-m-d H:i:s");
		SellerprofileModel::where('_id',$data['i_seller_id'])->update($update);
		$response['status']=1;
		$response['msg']="succesfully remove image.";
		echo json_encode($response);
		exit;
	}

	public function removeVideo(){

		$data = Request::all();
		$response = array();

		$userid = auth()->guard('web')->user()->_id;
		$jobid = "";

		if(!isset($data['i_seller_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['i_seller_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['v_video_name'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['v_video_name']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$jobdata = SellerprofileModel::find($data['i_seller_id']);
		
		if(!count($jobdata)){

			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}

		$update['v_introducy_video'] = '';
		$update['d_modified']=date("Y-m-d H:i:s");
		if(Storage::disk('s3')->exists($data['v_video_name'])){
			Storage::disk('s3')->Delete($data['v_video_name']);
		}

		SellerprofileModel::where('_id',$data['i_seller_id'])->update($update);
		$response['status']=1;
		$response['msg']="succesfully remove video.";
		echo json_encode($response);
		exit;
	}
	
	public function removeWorkVideo(){

		$data = Request::all();
		$response = array();

		$userid = auth()->guard('web')->user()->_id;
		$jobid = "";

		if(!isset($data['i_seller_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['i_seller_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['v_video_name'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['v_video_name']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$jobdata = SellerprofileModel::find($data['i_seller_id']);
		
		if(!count($jobdata)){

			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}

		// $update['v_work_video'] = '';
		$update['d_modified']=date("Y-m-d H:i:s");

		$update['v_work_video']=array();
		if(Storage::disk('s3')->exists($data['v_video_name'])){
			Storage::disk('s3')->Delete($data['v_video_name']);
		}
		if(count($jobdata->v_work_video)){
			foreach ($jobdata->v_work_video as $key => $value) {
				if($value!=$data['v_video_name']){
					$update['v_work_video'][]=$value;					
				}
			}
		}
	
		// if(Storage::disk('s3')->exists($data['v_video_name'])){
		// 	Storage::disk('s3')->Delete($data['v_video_name']);
		// }

		SellerprofileModel::where('_id',$data['i_seller_id'])->update($update);
		$response['status']=1;
		$response['msg']="succesfully remove video.";
		echo json_encode($response);
		exit;
	}

	public function createMangopayUser($data){
		
		$response['i_mangopay_id']=0;
		$response['i_wallet_id']=0;

		$UserNatural = new \MangoPay\UserNatural();
		$UserNatural->FirstName = $data['v_fname'];//"First Name";
		$UserNatural->LastName = $data['v_lname'];
		$UserNatural->Birthday = 1463496101;
		$UserNatural->Nationality = "GB";
		$UserNatural->Email = $data['v_email'];
		$UserNatural->CountryOfResidence = "FR";
		$result = $this->mangopay->Users->Create($UserNatural);

		if(count($result)){
			$mangopayid = $result->Id;
			$Wallet = new \MangoPay\Wallet();
			$Wallet->Tag = "custom meta";
			$Wallet->Owners = array ($result->Id);
			$Wallet->Description = "My skillbox project";
			$Wallet->Currency = "GBP";
			$resultWallet = $this->mangopay->Wallets->Create($Wallet);
			
			$response['i_mangopay_id']=$result->Id;
			$response['i_wallet_id']=$resultWallet->Id;

		}
		return $response;
	}



}