<?php
namespace App\Http\Controllers\Admin\Users;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Users\Order as OrderModel;
use App\Models\Users\Users as UsersModel;

class BuyerOrders extends Controller {


	public function index($id) {
		
		$buyer_user = UsersModel::find($id);

		$buyer_orders = OrderModel::query();
		$buyer_orders = $buyer_orders->where(function($query ){
		        $query->where('e_transaction_type', 'course')
		              ->orWhere('e_transaction_type', 'skill');
		    	});
		$buyer_orders = $buyer_orders->where('i_user_id',$id);
		$buyer_orders = $buyer_orders->get();

		$data=array(
			'buyer_user'=>$buyer_user,
			'buyer_orders'=>$buyer_orders,
		);
		return view('admin/users/buyer-orders', $data);
		
	}
}