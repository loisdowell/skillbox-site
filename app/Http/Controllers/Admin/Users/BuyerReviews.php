<?php
namespace App\Http\Controllers\Admin\Users;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Users\Buyerreview as BuyerreviewModel;
use App\Models\Users\Users as UsersModel;

class BuyerReviews extends Controller {


	public function index($id) {
		$buyer_user = UsersModel::find($id);
		
		if(isset($buyer_user) && count($buyer_user)){
			
			$buyer_review = BuyerreviewModel::where('i_buyer_id',$id)->get();
			
			$data=array(
				'buyer_user'=>$buyer_user,
				'buyer_review'=>$buyer_review,
			);
			return view('admin/users/buyer-reviews', $data);
		}else{
			return redirect('admin/users'); 
		}
		
	}
}