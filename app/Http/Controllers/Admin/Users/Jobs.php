<?php
namespace App\Http\Controllers\Admin\Users;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Users\Users as UsersModel;
use App\Models\City as CityModel;
use App\Models\Users\Jobs as JobsModel;

class Jobs extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "Jobs";
	}

	public function index() {

		$query 				= JobsModel::query();
		$data 				= $query->orderBy('id','DESC')->get();
		$_data				= array(
								'view'	=>"list",
								'data'	=>$data,
							);
		return view('admin/users/jobs', $_data);
	}

	public function Add() {

		$users 				= UsersModel::where("e_status","active")->get();
		$city 				= CityModel::where("e_status","active")->get();
		$_data				= array(
								'view'				=>"add",
								'users'				=> $users,
								'city'				=> $city
							);
		return view('admin/users/jobs', $_data);
	
	}
	
	public function Edit($id="") {
		
	  	$users 				= UsersModel::where("e_status","active")->get();
		$city 				= CityModel::where("e_status","active")->get();
		$data 				= JobsModel::find($id);
	    $_data				= array(
					            'view'				=>"edit",
					            'data'				=> $data,
								'users'				=> $users,
								'city'				=> $city
					        );
        return view('admin/users/jobs', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

			$post_data['d_added']		= date("Y-m-d h:i:s");
			if($post_data['v_timeframe'] == ''){
				$post_data['v_timeframe'] = 'on-going';
			}
			//$post_data['i_preferred_city_ids']	= implode(',', $post_data['i_preferred_city_ids']);
	        unset($post_data['_token']);
	        
	        JobsModel::create($post_data); 
	        return redirect('admin/jobs')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

			$post_data['d_modified']	= date("Y-m-d h:i:s");
			$post_data['i_preferred_city_ids']	= implode(',', $post_data['i_preferred_city_ids']);
            JobsModel::find($id)->update($post_data);
            return redirect( 'admin/jobs/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = JobsModel::find($id)->first();

            if(count($_data)){
               
			    JobsModel::find($id)->delete();
                return redirect( 'admin/jobs')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/jobs')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'open') {

			$data = [
				'e_status' => 'close',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusClose', [ 'section' => $this->section ]);
		}
		else if ($status == 'close') {

			$data = [
				'e_status' => 'open',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusOpen', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = SupportModel::find($id);
			$country->update($data);
		}

		return redirect('admin/support')->with('success', $message);
	}



}