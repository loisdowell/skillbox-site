<?php
namespace App\Http\Controllers\Admin\Users;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\SellerWork as SellerWorkModel;

class SellerWork extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "SellerWork";
	}

	public function index() {

		$query 				= SellerWorkModel::query();
		$data 				= $query->orderBy('id','DESC')->get();
		$_data				= array(
								'view'	=>"list",
								'data'	=>$data,
							);
		return view('admin/users/sellerwork', $_data);
	}

	public function Add() {

		$users 				= UsersModel::where("e_status","active")->get();
		$_data				=array(
								'view'		=>"add",
								'users'		=> $users
							);
		return view('admin/users/sellerwork', $_data);
	
	}
	
	public function Edit($id="") {
		
	  	$users 				= UsersModel::where("e_status","active")->get();
		$data 				= SellerWorkModel::find($id);
	    $_data				= array(
					            'view'		=>"edit",
					            'data'		=>$data,
								'users'		=> $users
					        );
        return view('admin/users/sellerwork', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

        	if( Request::file() ){
			  
				$media = Request::file('v_media');
				$ext   = $media->getClientOriginalExtension();
				if( count($media) ){
					if($post_data['e_type'] == 'image'){
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
							$destination 	= public_path('uploads/users');
							$fileName  		= 'user-image-' . rand(1000, 9999) . time() . '.' . $media->getClientOriginalExtension();
							$media->move($destination, $fileName);
							$post_data['v_media'] = $fileName;
						}else{
							return redirect( 'admin/sellerwork/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}else{
						if($ext == 'mov' || $ext == 'avi' || $ext == 'mp4'){
							$destination 	= public_path('uploads/users');
							$fileName  		= 'user-image-' . rand(1000, 9999) . time() . '.' . $media->getClientOriginalExtension();
							$media->move($destination, $fileName);
							$post_data['v_media'] = $fileName;
						}else{
							return redirect( 'admin/sellerwork/add/0')->with( 'warning', Lang::get('message.common.onlyVideo', [ 'section' => $this->section ])); 
						}
					}
				}
			}
			$post_data['d_added']		= date("Y-m-d h:i:s");
			unset($post_data['_token']);
	        
	        SellerWorkModel::create($post_data); 
	        return redirect('admin/sellerwork')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	if( Request::file() ){
					$media = Request::file('v_media');
					$ext   = $media->getClientOriginalExtension();
					if( count($media) ){

						if($post_data['e_type'] == 'image'){
							if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
								$destination = public_path('uploads/users');
								$imageExists = SellerWorkModel::where('id',$id)->value('v_media');

								if( isset($imageExists) && $imageExists != '' ) {
									if( File::exists( $destination . '/' . $imageExists ) ) {
										File::delete( $destination . '/' . $imageExists );
									}
								}
								
								$fileName  = 'user-image-' . rand(1000, 9999) . time() . '.' . $media->getClientOriginalExtension();
								$media->move($destination, $fileName);
								$post_data['v_media'] = $fileName;
							}else{
								return redirect( 'admin/sellerwork/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ]));
							}
						}else{
							if($ext == 'mov' || $ext == 'avi' || $ext == 'mp4'){
								$destination = public_path('uploads/users');
								$imageExists = SellerWorkModel::where('id',$id)->value('v_media');

								if( isset($imageExists) && $imageExists != '' ) {
									if( File::exists( $destination . '/' . $imageExists ) ) {
										File::delete( $destination . '/' . $imageExists );
									}
								}
								
								$fileName  = 'user-image-' . rand(1000, 9999) . time() . '.' . $media->getClientOriginalExtension();
								$media->move($destination, $fileName);
								$post_data['v_media'] = $fileName;
							}else{
								return redirect( 'admin/sellerwork/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyVideo', [ 'section' => $this->section ]));
							}


					}
				
				}
			}
			$post_data['d_modified']	= date("Y-m-d h:i:s");
			SellerWorkModel::find($id)->update($post_data);
            return redirect( 'admin/sellerwork/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 

       
        }else if($action=="delete"){
        	
        	$_data = SellerWorkModel::find($id)->first();

            if(count($_data)){
               
			    SellerWorkModel::find($id)->delete();
                return redirect( 'admin/sellerwork')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/sellerwork')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = SellerWorkModel::find($id);
			$country->update($data);
		}

		return redirect('admin/sellerwork')->with('success', $message);
	}



}