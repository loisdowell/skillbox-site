<?php
namespace App\Http\Controllers\Admin\Users;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\BuyerWork as BuyerWorkModel;


class BuyerWork extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "BuyerWork";
	}

	public function index() {

		$query 				= BuyerWorkModel::query();
		$data 				= $query->orderBy('id','DESC')->get();
		$_data				= array(
								'view'	=>"list",
								'data'	=>$data,
							);
		return view('admin/users/buyerwork', $_data);
	}

	public function Add() {

		$jobs 				= JobsModel::where("e_status","active")->get();
		$_data				= array(
								'view'	=> "add",
								'jobs'	=> $jobs
							);
		return view('admin/users/buyerwork', $_data);
	
	}
	
	public function Edit($id="") {
		
	  	$jobs 				= JobsModel::where("e_status","active")->get();
		$data 				= BuyerWorkModel::find($id);
	    $_data				= array(
					            'view'	=> "edit",
					            'data'	=> $data,
								'jobs'	=> $jobs
					        );
        return view('admin/users/buyerwork', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

        	if( Request::file() ){
			  
				$media = Request::file('v_media');
				$ext   = $media->getClientOriginalExtension();
				if( count($media) ){
					if($post_data['e_type'] == 'image'){
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
							$destination 	= public_path('uploads/users');
							$fileName  		= 'user-image-' . rand(1000, 9999) . time() . '.' . $media->getClientOriginalExtension();
							$media->move($destination, $fileName);
							$post_data['v_media'] = $fileName;
						}else{
							return redirect( 'admin/buyerwork/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}else{
						if($ext == 'mov' || $ext == 'avi' || $ext == 'mp4'){
							$destination 	= public_path('uploads/users');
							$fileName  		= 'user-image-' . rand(1000, 9999) . time() . '.' . $media->getClientOriginalExtension();
							$media->move($destination, $fileName);
							$post_data['v_media'] = $fileName;
						}else{
							return redirect( 'admin/buyerwork/add/0')->with( 'warning', Lang::get('message.common.onlyVideo', [ 'section' => $this->section ])); 
						}
					}
				}
			}

			$post_data['d_added']		= date("Y-m-d h:i:s");
		    unset($post_data['_token']);
	        
	        BuyerWorkModel::create($post_data); 
	        return redirect('admin/buyerwork')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	if( Request::file() ){
					$media = Request::file('v_media');
					$ext   = $media->getClientOriginalExtension();
					if( count($media) ){

						if($post_data['e_type'] == 'image'){
							if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
								$destination = public_path('uploads/users');
								$imageExists = BuyerWorkModel::where('id',$id)->value('v_media');

								if( isset($imageExists) && $imageExists != '' ) {
									if( File::exists( $destination . '/' . $imageExists ) ) {
										File::delete( $destination . '/' . $imageExists );
									}
								}
								
								$fileName  = 'user-image-' . rand(1000, 9999) . time() . '.' . $media->getClientOriginalExtension();
								$media->move($destination, $fileName);
								$post_data['v_media'] = $fileName;
							}else{
								return redirect( 'admin/buyerwork/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ]));
							}
						}else{
							if($ext == 'mov' || $ext == 'avi' || $ext == 'mp4'){
								$destination = public_path('uploads/users');
								$imageExists = BuyerWorkModel::where('id',$id)->value('v_media');

								if( isset($imageExists) && $imageExists != '' ) {
									if( File::exists( $destination . '/' . $imageExists ) ) {
										File::delete( $destination . '/' . $imageExists );
									}
								}
								
								$fileName  = 'user-image-' . rand(1000, 9999) . time() . '.' . $media->getClientOriginalExtension();
								$media->move($destination, $fileName);
								$post_data['v_media'] = $fileName;
							}else{
								return redirect( 'admin/buyerwork/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyVideo', [ 'section' => $this->section ]));
							}


					}
				
				}
			}

			$post_data['d_modified']	= date("Y-m-d h:i:s");
		    BuyerWorkModel::find($id)->update($post_data);
            return redirect( 'admin/buyerwork/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = BuyerWorkModel::find($id)->first();

            if(count($_data)){
               
			    BuyerWorkModel::find($id)->delete();
                return redirect( 'admin/buyerwork')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/buyerwork')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$buyer = BuyerWorkModel::find($id);
			$buyer->update($data);
		}

		return redirect('admin/buyerwork')->with('success', $message);
	}



}