<?php
namespace App\Http\Controllers\Admin\Courses;

use Request, Lang, File, GeneralHelper,Storage;
use App\Http\Controllers\Controller;
use App\Models\Support\SupportCategory as SupportCategoryModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Levels as LevelsModel;


class Courses extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "Courses";
	}

	public function index() {

		$query 	= CoursesModel::query();
		$data 	= $query->where('i_delete','!=',"1")->orderBy('id','DESC')->get();
		$_data	= array(
			'view'	=>"list",
			'data'	=>$data,
		);
		return view('admin/courses/courses', $_data);
	}

	public function Add() {

		$usersLists = UsersModel::where("e_status","active")->where("seller.v_service","online")->where("seller.e_status","active")->get();

		$coursesLevel = CoursesLevelModel::where("e_status","active")->get();
		$coursesLanguage = CoursesLanguageModel::where("e_status","active")->get();
		$coursesCategory = CoursesCategoryModel::where("e_status","active")->get();

		$_data	= array(
			'view'				=>"add",
			'usersLists'		=> $usersLists,
			'coursesLevel' 		=> $coursesLevel,
			'coursesLanguage'	=> $coursesLanguage,
			'coursesCategory'	=> $coursesCategory,
		);
		return view('admin/courses/courses', $_data);
	}
	
	public function Edit($id="") {
		
		$usersLists = UsersModel::where("e_status","active")->where("seller.v_service","online")->where("seller.e_status","active")->get();
		$coursesLevel = CoursesLevelModel::where("e_status","active")->get();
		$coursesLanguage = CoursesLanguageModel::where("e_status","active")->get();
		$coursesCategory = CoursesCategoryModel::where("e_status","active")->get();
		$data = CoursesModel::find($id);

		// if(isset($data->v_search_tags) && $data->v_search_tags!=""){
		// 	$data->v_search_tags = implode(",", $data->v_search_tags);
		// }

		$_data	= array(
			'view'				=>"edit",
			'usersLists'		=> $usersLists,
			'data'				=>$data,
			'coursesLevel' 		=> $coursesLevel,
			'coursesLanguage'	=> $coursesLanguage,
			'coursesCategory'	=> $coursesCategory,
		);

	    return view('admin/courses/courses', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        
        if(isset($post_data['_token'])){
        	unset($post_data['_token']);
        }

        if($action=="add"){

			if(count(Request::file('v_instructor_image'))){
				$image = Request::file('v_instructor_image');
				if( count($image) ){
					
					$fileName = 'instructor_image-'.time().'.'.$image->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$post_data['v_instructor_image'] = 'courses/' . $fileName;
					$uploadS3 = Storage::disk('s3')->put('/courses/'.$fileName, File::get((string)$image), 'public');
				}
			}

			if(count(Request::file('l_video'))){
				$video = Request::file('l_video');
				if( count($video) ){
					
					$fileName = 'introduction_video-'.time().'.'.$video->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$post_data['l_video'] = 'courses/video/' . $fileName;
					$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$video), 'public');
				}
			}



			$data['chapter']=array();
			if(isset($post_data['section']) && count($post_data['section']) ){
				foreach($data['section'] as $key => $value) {
					$post_data['section'][$key]['_id']=new \MongoDB\BSON\ObjectId();
					if(isset($value['video']) && count($value['video'])){
						foreach ($value['video'] as $y => $z) {
							$post_data['section'][$key]['video'][$y]['_id']=new \MongoDB\BSON\ObjectId();
							if(count($z['v_video'])){
								$fileName = 'section_video-'.time().'.'.$z['v_video']->getClientOriginalExtension();
								$fileName = str_replace(' ', '_', $fileName);
								$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$z['v_video']), 'public');
								$post_data['section'][$key]['video'][$y]['v_video'] = 'courses/video/' . $fileName;
							}

							if(isset($z['v_doc']) && count($z['v_doc'])){
								$fileName = 'section_video_doc-'.time().'.'.$z['v_doc']->getClientOriginalExtension();
								$fileName = str_replace(' ', '_', $fileName);
								$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$z['v_doc']), 'public');
								$post_data['section'][$key]['video'][$y]['v_doc'] = 'courses/video/' . $fileName;
							}
						}
					}
					
				}
			}
			if(isset($post_data['section']) && count($post_data['section']) ){
				foreach($post_data['section'] as $key => $value) {

					$tempdata=array();
					if(isset($value['video']) && count($value['video'])){
						foreach ($value['video'] as $y => $z) {
							$tempdata[]=$z;
						}
					}
					$value['video']=$tempdata;	
					$data['chapter'][]=$value;
				}
			}	
			$data['section']=$data['chapter'];

			$post_data['v_requirement']=nl2br($post_data['v_requirement']);
			$post_data['l_description']=nl2br($post_data['l_description']);
			$post_data['l_instructor_details']=nl2br($post_data['l_instructor_details']);
			
			$post_data['e_sponsor'] = "no";
			$post_data['review']['total']=0;	
			$post_data['review']['avgtotal']=0;	
			$post_data['i_total_review']=0;	
			$post_data['i_total_avg_review']=0;	
			$post_data['e_status']="published";	
			$post_data['e_qa_enabled']="on";	
			$post_data['e_reviews_enabled']="on";	
			$post_data['e_announcements_enabled']="on";	
			$post_data['d_added']=date("Y-m-d H:i:s");	
			$post_data['d_modified']=date("Y-m-d H:i:s");
		    CoursesModel::create($post_data); 
	        return redirect('admin/courses')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 


        }else if($action=="edit"){

        	$existsdata = CoursesModel::find($id);

        	if(count(Request::file('v_instructor_image'))){
				$image = Request::file('v_instructor_image');
				if( count($image) ){
					if(isset($existsdata->v_instructor_image) && $existsdata->v_instructor_image!=""){
						if(Storage::disk('s3')->exists($existsdata->v_instructor_image)){
							Storage::disk('s3')->Delete($existsdata->v_instructor_image);
						}
					}
					$fileName = 'instructor_image-'.time().'.'.$image->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$post_data['v_instructor_image'] = 'courses/' . $fileName;
					$uploadS3 = Storage::disk('s3')->put('/courses/'.$fileName, File::get((string)$image), 'public');
				}
			}

			if(count(Request::file('v_course_thumbnail'))){
				$image = Request::file('v_course_thumbnail');
				if( count($image) ){
					if(isset($existsdata->v_course_thumbnail) && $existsdata->v_course_thumbnail!=""){
						if(Storage::disk('s3')->exists($existsdata->v_course_thumbnail)){
							Storage::disk('s3')->Delete($existsdata->v_course_thumbnail);
						}
					}
					$fileName = 'instructor_image-'.time().'.'.$image->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$post_data['v_course_thumbnail'] = 'courses/' . $fileName;
					$uploadS3 = Storage::disk('s3')->put('/courses/'.$fileName, File::get((string)$image), 'public');
				}
			}

			if(count(Request::file('l_video'))){
				
				$video = Request::file('l_video');
				if( count($video) ){
					
					if(isset($existsdata->l_video) && $existsdata->l_video!=""){
						if(Storage::disk('s3')->exists($existsdata->l_video)){
							Storage::disk('s3')->Delete($existsdata->l_video);
						}
					}

					$fileName = 'introduction_video-'.time().'.'.$video->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$post_data['l_video'] = 'courses/video/' . $fileName;
					$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$video), 'public');
				}
			}

			//$Exists = CoursesModel::find($id);
			$sectiondata=array();
			//dd($existsdata->section);

			foreach ($existsdata->section as $key => $value) {
				$sectiondata[(string)$value['_id']] = $value;
			}

		
			$data['chapter']=array();

			if(isset($post_data['section']) && count($post_data['section']) ){
				foreach($post_data['section'] as $key => $value) {

					if(!isset($value['_id'])){
						$post_data['section'][$key]['_id'] = (string)new \MongoDB\BSON\ObjectId();
					}
					if(isset($value['video']) && count($value['video'])){
						foreach ($value['video'] as $y => $z) {
							if(isset($z['_id']) && $z['_id']!=""){
								$post_data['section'][$key]['video'][$y]['_id'] = $z['_id'];	
							}else{
								$post_data['section'][$key]['video'][$y]['_id'] = (string)new \MongoDB\BSON\ObjectId();	
							}
							if(isset($z['v_video']) && count($z['v_video'])){
								$fileName = 'section_video-'.time().'.'.$z['v_video']->getClientOriginalExtension();
								$fileName = str_replace(' ', '_', $fileName);
								$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$z['v_video']), 'public');
								$post_data['section'][$key]['video'][$y]['v_video'] = 'courses/video/' . $fileName;
							}else{
								$post_data['section'][$key]['video'][$y]['v_video'] = $sectiondata[$value['_id']]['video'][$y]['v_video'];
							}
							if(isset($z['v_doc']) && count($z['v_doc'])){
								$fileName = 'section_video_doc-'.time().'.'.$z['v_doc']->getClientOriginalExtension();
								$fileName = str_replace(' ', '_', $fileName);
								$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$z['v_doc']), 'public');
								$post_data['section'][$key]['video'][$y]['v_doc'] = 'courses/video/' . $fileName;
							}else{
								if(isset($sectiondata['video'][$y]['v_doc'])){
									$post_data['section'][$key]['video'][$y]['v_doc']=$sectiondata[$value['_id']]['video'][$y]['v_doc'];
								}else{
									$post_data['section'][$key]['video'][$y]['v_doc']="";
								}
							}
						}
					}
					
				}
			}

			if(isset($post_data['section']) && count($post_data['section']) ){
				foreach($post_data['section'] as $key => $value) {

					$tempdata=array();
					if(isset($value['video']) && count($value['video'])){
						foreach ($value['video'] as $y => $z) {
							$tempdata[]=$z;
						}
					}
					$value['video']=$tempdata;	
					$data['chapter'][]=$value;
				}
			}	
			$post_data['section']=$data['chapter'];	

			$post_data['v_requirement']=nl2br($post_data['v_requirement']);
			$post_data['l_description']=nl2br($post_data['l_description']);
			$post_data['l_instructor_details']=nl2br($post_data['l_instructor_details']);
			$post_data['d_modified']	= date("Y-m-d h:i:s");
		 
		    CoursesModel::find($id)->update($post_data);
            return redirect( 'admin/courses/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = CoursesModel::find($id);
            if(count($_data)){
            	$update['i_delete']="1";
            	CoursesModel::find($id)->update($update);
                return redirect( 'admin/courses')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/courses')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }
        }
    }

    public function status( $id, $status ) {
		
		if ($status == 'active') {
			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$course = CoursesModel::find($id);
			$course->update($data);
		}

		return redirect('admin/courses')->with('success', $message);
	}



}