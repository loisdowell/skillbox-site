<?php
namespace App\Http\Controllers\Admin\Courses;

use Request, Lang, File, GeneralHelper,Storage;
use App\Http\Controllers\Controller;
use App\Models\Support\SupportCategory as SupportCategoryModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Helpers\General;

class CoursesCategory extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "CoursesCategory";
	}


	public function index() {

		$query = CoursesCategoryModel::query();
		$data = $query->where('i_delete','!=',"1")->orderBy('d_added','DESC')->get();

		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/courses/coursescategory', $_data);
	}

	public function Add() {
		
		$_data= array(
			'view'   =>"add",
		);
		return view('admin/courses/coursescategory', $_data);
	
	}
	
	public function Edit($id="") {
	  	
	  	$data = CoursesCategoryModel::find($id);

	    $_data = array(
            'view'	 =>"edit",
            'data'	 => $data,
        );
        return view('admin/courses/coursescategory', $_data);
    }


    public function Action($action="",$id="") {

        $post_data = Request::all();
        

        if(isset($post_data['_token'])){
        	unset($post_data['_token']);
        }
      
        if($action=="add"){

        	$existLang = CoursesCategoryModel::where('v_title',$post_data['v_title'])->get();
	        if(count($existLang)){
	        	return redirect( 'admin/courses-category/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

	       	if(Request::file()){
			  
				$image = Request::file('v_image');
				$mainImage = Request::file('v_main_image');
				
				if(count($image) ){
					
					$ext   = $image->getClientOriginalExtension();
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						
						$fileName = 'categories-'.time().'.'.$image->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_image'] = 'categories/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/categories/'.$fileName, File::get((string)$image), 'public');

					}else{
						return redirect( 'admin/courses-category/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					
					}
				}

				if(count($mainImage) ){

					$ext   = $mainImage->getClientOriginalExtension();
					if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
						
						$fileName = 'categories-main-'.time().'.'.$mainImage->getClientOriginalExtension();
						$fileName = str_replace(' ', '_', $fileName);
						$post_data['v_main_image'] = 'categories/' . $fileName;
						$uploadS3 = Storage::disk('s3')->put('/categories/'.$fileName, File::get((string)$mainImage), 'public');

					}else{
						return redirect( 'admin/courses-category/add/0')->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
					
					}
				}
			}

			$post_data['d_added']=date("Y-m-d h:i:s");
       		$post_data['d_modified']=date("Y-m-d h:i:s");
	       	
       	    CoursesCategoryModel::create($post_data); 
	        return redirect('admin/courses-category')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$existLang = CoursesCategoryModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
	        if(count($existLang)){
	        	return redirect( 'admin/courses-category/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

	        if( Request::file() ){
					
					$image = Request::file('v_image');
					if( count($image) ){
						$ext   = $image->getClientOriginalExtension();
						if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
							$imageExists = CoursesCategoryModel::where('id',$id)->value('v_image');
							if(Storage::disk('s3')->exists($imageExists)){
								Storage::disk('s3')->Delete($imageExists);
							}
							$fileName = 'categories-'.time().'.'.$image->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_image'] = 'categories/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/categories/'.$fileName, File::get((string)$image), 'public');

						}else{
							return redirect( 'admin/courses-category/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}
				}
				if( Request::file() ){
					
					$mainImage = Request::file('v_main_image');
					
					if( count($mainImage) ){
						$mainImageExt   = $mainImage->getClientOriginalExtension();
						if($mainImageExt == 'jpg' || $mainImageExt == 'png' || $mainImageExt == 'jpeg'){
							$mainImageExists = CoursesCategoryModel::where('id',$id)->value('v_main_image');
							if(Storage::disk('s3')->exists($mainImageExists)){
								Storage::disk('s3')->Delete($mainImageExists);
							}
							$fileName = 'categories-main-'.time().'.'.$mainImage->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$post_data['v_main_image'] = 'categories/' . $fileName;
							$uploadS3 = Storage::disk('s3')->put('/categories/'.$fileName, File::get((string)$mainImage), 'public');

						}else{
							return redirect( 'admin/courses-category/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyImage', [ 'section' => $this->section ])); 
						}
					}
				
				}

			$post_data['d_modified']=date("Y-m-d h:i:s");
            CoursesCategoryModel::find($id)->update($post_data);
            return redirect( 'admin/courses-category/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = CoursesCategoryModel::find($id)->first();

            if(count($_data)){

            	$update['i_delete']="1";	
                CoursesCategoryModel::find($id)->update($update);
                return redirect( 'admin/courses-category')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/courses-category')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = CoursesCategoryModel::find($id);
			$country->update($data);
		}

		return redirect('admin/courses-category')->with('success', $message);
	}



}