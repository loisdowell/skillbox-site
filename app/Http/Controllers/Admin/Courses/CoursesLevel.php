<?php
namespace App\Http\Controllers\Admin\Courses;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Support\SupportCategory as SupportCategoryModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Helpers\General;

class CoursesLevel extends Controller {

	protected $section;

	public function __construct(){ 
		$this->section = "CoursesLevel";
	}


	public function index() {

		$query = CoursesLevelModel::query();
		$data = $query->orderBy('d_added','DESC')->get();

		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/courses/courseslevel', $_data);
	}

	public function Add() {
		
		$_data= array(
			'view'   =>"add",
		);
		return view('admin/courses/courseslevel', $_data);
	
	}
	
	public function Edit($id="") {
	  	
	  	$data = CoursesLevelModel::find($id);

	    $_data = array(
            'view'	 =>"edit",
            'data'	 => $data,
        );
        return view('admin/courses/courseslevel', $_data);
    }


    public function Action($action="",$id="") {

        $post_data = Request::all();
        

        if(isset($post_data['_token'])){
        	unset($post_data['_token']);
        }
      
        if($action=="add"){

        	$existLang = CoursesLevelModel::where('v_title',$post_data['v_title'])->get();
	        if(count($existLang)){
	        	return redirect( 'admin/courses-level/add/0')->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }
        	
       		$post_data['d_added']=date("Y-m-d h:i:s");
       		$post_data['d_modified']=date("Y-m-d h:i:s");
	       	
       	    CoursesLevelModel::create($post_data); 
	        return redirect('admin/courses-level')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        	$existLang = CoursesLevelModel::where('v_title',$post_data['v_title'])->where('_id',"!=",$id)->get();
	        if(count($existLang)){
	        	return redirect( 'admin/courses-level/edit/'.$id)->with( 'warning', Lang::get('message.common.keyExist', [ 'section' => $this->section ])); 
	        }

			$post_data['d_modified']=date("Y-m-d h:i:s");
            CoursesLevelModel::find($id)->update($post_data);
            return redirect( 'admin/courses-level/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = CoursesLevelModel::find($id)->first();

            if(count($_data)){
                CoursesLevelModel::find($id)->delete();
                return redirect( 'admin/courses-level')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/courses-level')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = CoursesLevelModel::find($id);
			$country->update($data);
		}

		return redirect('admin/courses-level')->with('success', $message);
	}



}