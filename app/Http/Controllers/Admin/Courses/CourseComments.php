<?php
namespace App\Http\Controllers\Admin\Courses;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Support\SupportCategory as SupportCategoryModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CourseFaqs as CourseFaqsModel;
use App\Models\Courses\CourseComments as CourseCommentsModel;



class CourseComments extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "CourseComments";
	}

	public function index() {

		$query = CourseCommentsModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/courses/coursecomments', $_data);
	}

	public function Add() {

		$course 			= CoursesModel::where("e_status","active")->get();
		$_data				= array(
								'view'	=>"add",
								'course'=> $course
							);
		return view('admin/courses/coursecomments', $_data);
	
	}
	
	public function Edit($id="") {
	  	$course 			= CoursesModel::where("e_status","active")->get();
	  	$data 				= CourseCommentsModel::find($id);
	    $_data				= array(
					            'view'	=>"edit",
					            'data'	=>$data,
					            'course'=> $course
					        );
        return view('admin/courses/coursecomments', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

			$post_data['d_added']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        
	        CourseCommentsModel::create($post_data); 
	        return redirect('admin/coursecomments')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
        		
    			$post_data['d_modified']=date("Y-m-d h:i:s");
	            CourseCommentsModel::find($id)->update($post_data);
	            return redirect( 'admin/coursecomments/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = CourseCommentsModel::find($id)->first();

            if(count($_data)){
               	
                CourseCommentsModel::find($id)->delete();
                return redirect( 'admin/coursecomments')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/coursecomments')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$cfaqs = CourseCommentsModel::find($id);
			$cfaqs->update($data);
		}

		return redirect('admin/coursecomments')->with('success', $message);
	}

	public function GetTitle() {
	  $post_data = Request::all();
	  $response = array();
	  $optingStr = '<option value="">- select -</option>';
	  $cat_id = $post_data['v_type'];
	  $selected_skill_id = $post_data['selected_type'];
	  $categories = CourseFaqsModel::where("v_type",$cat_id)->where("e_status","active")->get();
	  if(count($categories)){
	  	foreach ($categories as $key => $val) {
	  		$a="";
	  		if($val['_id']==$selected_skill_id){
	  			$a="selected";
	  		}
	  		$optingStr.= '<option value="'.$val['_id'].'" '.$a.'>'.$val['v_title'].'</option>' ;
	  	}
	  }
	  $response['optingStr']=$optingStr;
	  $response['status'] = 1;
	  
	  echo json_encode($response); 
	  exit;   
	}

}