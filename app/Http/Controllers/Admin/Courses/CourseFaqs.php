<?php
namespace App\Http\Controllers\Admin\Courses;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Support\SupportCategory as SupportCategoryModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CourseFaqs as CourseFaqsModel;



class CourseFaqs extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "CourseFaqs";
	}

	public function index() {

		$query = CourseFaqsModel::query();
		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/courses/coursefaqs', $_data);
	}

	public function Add() {

		$course 			= CoursesModel::where("e_status","active")->get();
		$_data				= array(
								'view'	=>"add",
								'course'=> $course
							);
		return view('admin/courses/coursefaqs', $_data);
	
	}
	
	public function Edit($id="") {
	  	$course 			= CoursesModel::where("e_status","active")->get();
	  	$data 				= CourseFaqsModel::find($id);
	    $_data				= array(
					            'view'	=>"edit",
					            'data'	=>$data,
					            'course'=> $course
					        );
        return view('admin/courses/coursefaqs', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

			$post_data['d_added']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        
	        CourseFaqsModel::create($post_data); 
	        return redirect('admin/coursefaqs')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){
        		
    			$post_data['d_modified']=date("Y-m-d h:i:s");
	            CourseFaqsModel::find($id)->update($post_data);
	            return redirect( 'admin/coursefaqs/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = CourseFaqsModel::find($id)->first();

            if(count($_data)){
               	
                CourseFaqsModel::find($id)->delete();
                return redirect( 'admin/coursefaqs')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/coursefaqs')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$cfaqs = CourseFaqsModel::find($id);
			$cfaqs->update($data);
		}

		return redirect('admin/coursefaqs')->with('success', $message);
	}



}