<?php
namespace App\Http\Controllers\Admin\Courses;

use Request, Lang, File, GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Support\SupportCategory as SupportCategoryModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CourseChapters as CourseChaptersModel;
use App\Helpers\General;



class CourseChapters extends Controller {

	protected $section;

	public function __construct(){ 
		
		$this->section = "CourseChapters";
	}

	public function index() {

		$query = CourseChaptersModel::query();

		$data = $query->orderBy('id','DESC')->get();
		$_data=array(
			'view'=>"list",
			'data'=>$data,
		);
		return view('admin/courses/coursechapters', $_data);
	}

	public function Add() {
		$course 			= CoursesModel::where("e_status","active")->get();
		$chapters 			= CourseChaptersModel::where("i_parent_id",0)->where("e_status","active")->get();
		$_data				= array(
								'view'   =>"add",
								'course' => $course,
								'chapters'=>$chapters
							);
		return view('admin/courses/coursechapters', $_data);
	
	}
	
	public function Edit($id="") {
	  	$course 			= CoursesModel::where("e_status","active")->get();	
	  	$data 				= CourseChaptersModel::find($id);
	  	$chapters 			= CourseChaptersModel::where("i_parent_id",0)->where("e_status","active")->get();
	  	$resonseicon = '';
	  	$img_url = $data->l_documents;
       
       	if(isset($img_url) && $img_url != ''){
       	$img_data = General::fmw_get_ducument($img_url);
       	if( strpos($img_data['link'],'png') || strpos($img_data['link'],'jpg') ) {
            $resonseicon = $img_data['link'];
       	}else{
            $resonseicon = $img_data['icon'];
       	}
       }
       
        $_data				= array(
					            'view'	 =>"edit",
					            'data'	 => $data,
					            'course' => $course,
					            'chapters'=>$chapters,
					            'resonseicon'=>$resonseicon
					        );
        return view('admin/courses/coursechapters', $_data);
    }

    public function Action($action="",$id="") {

        $post_data = Request::all();
      
        if($action=="add"){

        	if( Request::file() ){
			  
				$docx = Request::file('l_documents');
				
				if( count($docx) ){
					$ext  = $docx->getClientOriginalExtension();
					if($ext=="pdf" || $ext=="docx" || $ext=="doc" || $ext=="xlsx"){

						$destination 	= public_path('uploads/courses');
						$fileName  		= 'course-document-' . rand(1000, 9999) . time() . '.' . $docx->getClientOriginalExtension();
						$docx->move($destination, $fileName);
						$post_data['l_documents'] = $fileName;
					}else{
							return redirect( 'admin/coursechapters/add/0')->with( 'warning', Lang::get('message.common.onlyDocs', [ 'section' => $this->section ]));

					}	
				}

				$video= Request::file('l_video');
				
				if( count($video) ){

					$ext  = $video->getClientOriginalExtension();
					if($ext=="mov" || $ext=="mp4" || $ext=="avi"){
						$destination 	= public_path('uploads/courses');
						$fileName  		= 'course-video-' . rand(1000, 9999) . time() . '.' . $video->getClientOriginalExtension();
						$video->move($destination, $fileName);
						$post_data['l_video'] = $fileName;
					}else{
							return redirect( 'admin/coursechapters/add/0')->with( 'warning', Lang::get('message.common.onlyVideo', [ 'section' => $this->section ]));

					}	
				}
			}
			if(isset($post_data['i_parent_id']) && $post_data['i_parent_id'] != ''){
				$i_parent_id = $post_data['i_parent_id'];
			}else{
				$i_parent_id = 0;
			}
			$post_data['i_parent_id']=$i_parent_id;
       		$post_data['d_added']=date("Y-m-d h:i:s");
	        unset($post_data['_token']);
	        
	        CourseChaptersModel::create($post_data); 
	        return redirect('admin/coursechapters')->with( 'success', Lang::get('message.common.detailAdded',[ 'section' => $this->section ])); 

        }else if($action=="edit"){

        		if( Request::file() ){
					$docx = Request::file('l_documents');

					if( count($docx) ){
						$ext  = $docx->getClientOriginalExtension();

						if($ext=="pdf" || $ext=="docx" || $ext=="doc" || $ext=="xlsx"){

							$destination = public_path('uploads/courses');
							$imageExists = CoursesModel::where('_id',$id)->value('v_instructor_image');

							if( isset($imageExists) && $imageExists != '' ) {
								if( File::exists( $destination . '/' . $imageExists ) ) {
									File::delete( $destination . '/' . $imageExists );
								}
							}
							
							$fileName  = 'course-document-' . rand(1000, 9999) . time() . '.' . $docx->getClientOriginalExtension();
							$docx->move($destination, $fileName);
							$post_data['l_documents'] = $fileName;
						}else{
							return redirect( 'admin/coursechapters/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyDocs', [ 'section' => $this->section ]));

						}
					}

					$video = Request::file('l_video');
					
					if( count($video) ){
						$ext  = $video->getClientOriginalExtension();
						if($ext=="mov" || $ext=="mp4" || $ext=="avi"){
							$destination = public_path('uploads/courses');
							$imageExists = CoursesModel::where('_id',$id)->value('l_video');

							if( isset($imageExists) && $imageExists != '' ) {
								if( File::exists( $destination . '/' . $imageExists ) ) {
									File::delete( $destination . '/' . $imageExists );
								}
							}
							
							$fileName  = 'course-video-' . rand(1000, 9999) . time() . '.' . $video->getClientOriginalExtension();
							$video->move($destination, $fileName);
							$post_data['l_video'] = $fileName;
						}else{
							return redirect( 'admin/coursechapters/edit/'.$id)->with( 'warning', Lang::get('message.common.onlyDocs', [ 'section' => $this->section ]));

						}
					}
				
				}
        		if(isset($post_data['i_parent_id']) && $post_data['i_parent_id'] != ''){
					$i_parent_id = $post_data['i_parent_id'];
				}else{
					$i_parent_id = 0;
				}
				$post_data['i_parent_id']=$i_parent_id;
    			$post_data['d_modified']=date("Y-m-d h:i:s");
	            CourseChaptersModel::find($id)->update($post_data);
	            return redirect( 'admin/coursechapters/edit/'.$id)->with( 'success', Lang::get('message.common.detailUpdated', [ 'section' => $this->section ])); 
       
        }else if($action=="delete"){
        	
        	$_data = CourseChaptersModel::find($id)->first();

            if(count($_data)){
               	
                CourseChaptersModel::find($id)->delete();
                return redirect( 'admin/coursechapters')->with( 'success', Lang::get('message.common.detailDeleted', [ 'section' => $this->section ])); 
            }else{
                return redirect( 'admin/coursechapters')->with( 'warning', Lang::get('message.common.somethingWrong')); 
            }

        }
          
    }
	

    public function status( $id, $status ) {
		
		if ($status == 'active') {

			$data = [
				'e_status' => 'inactive',
				'd_modified' => date('Y-m-d H:i:s')
			];
			$message = Lang::get('message.common.statusInactive', [ 'section' => $this->section ]);
		}
		else if ($status == 'inactive') {

			$data = [
				'e_status' => 'active',
				'd_modified' => date('Y-m-d H:i:s')
			];

			$message = Lang::get('message.common.statusActive', [ 'section' => $this->section ]);
		}
		
		if (isset($data) && count($data)) {

			$country = CourseChaptersModel::find($id);
			$country->update($data);
		}

		return redirect('admin/coursechapters')->with('success', $message);
	}



}