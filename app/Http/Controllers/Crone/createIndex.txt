db.tbl_courses.createIndex( { i_delete: 1 } )
db.tbl_courses.createIndex( { e_status: 1 } )

db.tbl_courses.createIndex( { d_added: 1 } )
db.tbl_courses.createIndex( { f_price: 1 } )
db.tbl_courses.createIndex( { i_total_avg_review: 1 } )

db.tbl_courses.createIndex( { i_impression_perday: 1 } )


