<?php
namespace App\Http\Controllers\Crone;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Messages as MessagesModel;
use App\Models\Users\Order as OrderModal;
use App\Helpers\Emailtemplate as EmailtemplateHelper;




class Userplanexpire extends Controller {

	protected $section;
	private $mangopay;

	public function __construct(\MangoPay\MangoPayApi $mangopay){
	    $this->mangopay = $mangopay;
	}

	public function index() {

		$currentdate = date("Y-m-d");
		$userdata = UsersModel::where('v_plan.id',"!=",'5a65b48cd3e812a4253c9869')->where('v_plan.d_end_date','<=',$currentdate)->get();
		
		if(count($userdata)){
			foreach ($userdata as $key => $value) {

				$update=array();
				$update['v_plan']['id']="5a65b48cd3e812a4253c9869";
				$update['v_plan']['duration']=$value->v_plan['duration'];
				$update['v_plan']['d_start_date']=$value->v_plan['d_start_date'];
				$update['v_plan']['d_end_date']=$value->v_plan['d_end_date'];
				UsersModel::find($value->id)->update($update);

			}
		}	
		return 1;
	}

	public function updateUser() {

		$currentdate = date("Y-m-d");
		$userdata = UsersModel::where('e_status','active')->get();
		if(count($userdata)){
			foreach ($userdata as $key => $value) {

				$meesage = MessagesModel::where('i_parent_id','!=',0)->where('i_from_id',$value->id)->get();
				$update=array();
				$totaltime = 0;
				if(count($meesage)){
					foreach ($meesage as $k => $v) {
						if(isset($v->v_replay_time)){
							$totaltime = $totaltime+$v->v_replay_time;
						}
					}
					$totaltimeavg1 = $totaltime/count($meesage);
					$totaltimeavg = number_format($totaltimeavg1/3600,0); 
					if($totaltimeavg>=1){
						$update['v_replies_time']=$totaltimeavg." hours";
					}else{
						$update['v_replies_time'] = "10 Minute";
					}	
				}else{
					$update['v_replies_time']="24 hours";	
				}

				$orderdata = OrderModal::where('e_transaction_type','skill')->where('v_order_status','delivered')->where('i_seller_id',$value->id)->get();

				$totalamt=0;
				if(count($orderdata)){
					foreach ($orderdata as $k => $v) {
							$totalamt=$totalamt+$v->v_amount;		
					}
				}
				$totalamt = (float)$totalamt;

				$ordercnt = count($orderdata);
				
				$update=array();
				if(($ordercnt>=10 && $ordercnt<50) || ($totalamt>=400 && $totalamt<1000) ){
					$update['v_level']="1";	
				}

				if(($ordercnt>=50 && $ordercnt<150) || ($totalamt>=1000 && $totalamt<3000) ){
					$update['v_level']="2";	
				}

				if(($ordercnt>=150) || ($totalamt>=3000)){
					$update['v_level']="3";	
				}

				if(count($update)){
					UsersModel::find($value->id)->update($update);	
				}
				if(isset($update['v_level']) && $value->v_level!=$update['v_level']  && $update['v_level']!="0"){
					$mailData['USER_NAME']="";
			        $mailData['LEVEL']=$update['v_level'];
			        $mailData['USER_NAME'] = $value->v_fname.' '.$value->v_lname;	
			        $mailcontent = EmailtemplateHelper::LevelEarned($mailData);
			        $subject = " You've just earned a Level ".$update['v_level']." Seller Status";
			        $mailids=array(
			            $mailData['USER_NAME']=>$value->v_email
			        );
			        EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
			    }

			}
		}	
		return 1;
	}

	public function mangopayKycApproved(){
		
		$userdata = UsersModel::where('v_kyc_success_mail',"!=",'1')->where('i_mangopay_id',"!=",'')->where('i_delete','!=','1')->get();
		if(count($userdata)){
			foreach ($userdata as $key => $value) {

				$mangopayid = $value->i_mangopay_id;
				$result = $this->mangopay->Users->Get($mangopayid);
				if(count($result) && isset($result->KYCLevel) && $result->KYCLevel=="REGULAR"){

				    $mailData['USER_FULL_NAME'] = $value->v_fname.' '.$value->v_lname;	
			       
			        $mailcontent = EmailtemplateHelper::KycComplete($mailData);
			        $subject = EmailtemplateHelper::EmailTemplateSubject("5c0bc2f176fbae10c00de3f3");
			        $mailids=array(
			            $mailData['USER_FULL_NAME']=>$value->v_email
			        );
			        EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
			        $value->v_kyc_success_mail="1";
			        $value->save();
			    }	
			}
		}
		return 1;

	}

	public function mangopayKycRefused(){
		
		$userdata = UsersModel::where('v_kyc_success_mail',"!=",'1')->where('i_mangopay_id',"!=",'')->where('i_delete','!=','1')->get();
		

		if(count($userdata)){
			foreach ($userdata as $key => $value) {

				$mangopayid = $value->i_mangopay_id;

				if(isset($value->i_identity_kyc_id) && count($value->i_identity_kyc_id) ){
					
					if(!isset($value->i_identity_kyc_refused_mail) || $value->i_identity_kyc_refused_mail!="1"){
						$i_identity_kyc_id = $value->i_identity_kyc_id[0];
						try{
							
							$kycidenty = $this->mangopay->Users->GetKycDocument($mangopayid, $i_identity_kyc_id);
							
							if(count($kycidenty) && isset($kycidenty->Status) && $kycidenty->Status=="REFUSED"){
							    $mailData['USER_FULL_NAME'] = $value->v_fname.' '.$value->v_lname;	
							    $mailData['REFUSED_REASON'] = $kycidenty->RefusedReasonMessage;	
							   
						        $mailcontent = EmailtemplateHelper::KycRefused($mailData);
						        $subject = EmailtemplateHelper::EmailTemplateSubject("5c0bc34876fbae10ee57c122");
						        $mailids=array(
						            $mailData['USER_FULL_NAME']=>$value->v_email
						        );
						        EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
						        $value->i_identity_kyc_refused_mail="1";
				        		$value->save();
				            }
				        }catch (\Exception $e) {
						}
					}    	

				}

				if(isset($value->i_registration_kyc_id) && count($value->i_registration_kyc_id)){
					
					if(!isset($value->i_registration_refused_mail) || $value->i_registration_refused_mail!="1"){
						$i_registration_kyc_id = $value->i_registration_kyc_id[0];
						try{
							$kycidenty = $this->mangopay->Users->GetKycDocument($mangopayid, $i_registration_kyc_id);
							
							if(count($kycidenty) && isset($kycidenty->Status) && $kycidenty->Status=="REFUSED"){
							    
							    $mailData['USER_FULL_NAME'] = $value->v_fname.' '.$value->v_lname;	
							    $mailData['REFUSED_REASON'] = $kycidenty->RefusedReasonMessage;	
							   
						        $mailcontent = EmailtemplateHelper::KycRefused($mailData);
						        $subject = EmailtemplateHelper::EmailTemplateSubject("5c0bc34876fbae10ee57c122");
						        $mailids=array(
						            $mailData['USER_FULL_NAME']=>$value->v_email
						        );
						        EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
						        $value->i_registration_refused_mail="1";
				        		$value->save();

						    }
						}catch (\Exception $e) {
						}
					}	    	

				}

				if(isset($value->i_articles_kyc_id) && count($value->i_articles_kyc_id)){
					
					if(!isset($value->i_articles_kyc_refused_mail) || $value->i_articles_kyc_refused_mail!="1"){
						$i_articles_kyc_id = $value->i_articles_kyc_id[0];
						try{
							$kycidenty = $this->mangopay->Users->GetKycDocument($mangopayid, $i_articles_kyc_id);
							
							if(count($kycidenty) && isset($kycidenty->Status) && $kycidenty->Status=="REFUSED"){
							    
							    $mailData['USER_FULL_NAME'] = $value->v_fname.' '.$value->v_lname;	
							    $mailData['REFUSED_REASON'] = $kycidenty->RefusedReasonMessage;	
							   
						        $mailcontent = EmailtemplateHelper::KycRefused($mailData);
						        $subject = EmailtemplateHelper::EmailTemplateSubject("5c0bc34876fbae10ee57c122");
						        $mailids=array(
						            $mailData['USER_FULL_NAME']=>$value->v_email
						        );
						        EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
						        $value->i_articles_kyc_refused_mail="1";
				        		$value->save();

						    }
						}catch (\Exception $e) {
						}
					}	    	

				}


				if(isset($value->i_shareholder_kyc_id) && count($value->i_shareholder_kyc_id)){
					
					if(!isset($value->i_shareholder_kyc_refused_mail) || $value->i_shareholder_kyc_refused_mail!="1"){
						$i_shareholder_kyc_id = $value->i_shareholder_kyc_id[0];
						try{
							$kycidenty = $this->mangopay->Users->GetKycDocument($mangopayid, $i_shareholder_kyc_id);
							
							if(count($kycidenty) && isset($kycidenty->Status) && $kycidenty->Status=="REFUSED"){
							    
							    $mailData['USER_FULL_NAME'] = $value->v_fname.' '.$value->v_lname;	
							    $mailData['REFUSED_REASON'] = $kycidenty->RefusedReasonMessage;	
							   
						        $mailcontent = EmailtemplateHelper::KycRefused($mailData);
						        $subject = EmailtemplateHelper::EmailTemplateSubject("5c0bc34876fbae10ee57c122");
						        $mailids=array(
						            $mailData['USER_FULL_NAME']=>$value->v_email
						        );
						        EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
						        $value->i_shareholder_kyc_refused_mail="1";
				        		$value->save();

						    }
						}catch (\Exception $e) {
						}
					}	    	

				}


				
			}
		}

		return 1;


	}
	
	
}