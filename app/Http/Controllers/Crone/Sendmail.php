<?php
namespace App\Http\Controllers\Crone;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Messages as MessagesModel;
use App\Models\Users\Order as OrderModal;
use App\Models\Emailsend as Emailsend;
use App\Helpers\Emailtemplate as EmailtemplateHelper;


class Sendmail extends Controller {

	public function index() {
		
		$fromdata = EmailtemplateHelper::mailFromData();
		$maildata = Emailsend::where('i_send_mail','!=','1')->get();
		
		if(count($maildata)){
			foreach ($maildata as $key => $value) {
				$attachment=array();
				if(isset($value->attachments)){
					$attachment = $value->attachments;
				}
				$res = EmailtemplateHelper::MailSendGeneralFinal($fromdata,$value->subject,$value->message,$value->mailids,$attachment);
				
				if($res){
					$value->i_send_mail="1";
					$value->save();	
				}
				
				//Emailsend::find($value->id)->delete();
			}
		}
		return 1;
	}
	
	public function deleteSendMail() {
		Emailsend::where('i_send_mail','1')->delete();
		return 1;
	}

}