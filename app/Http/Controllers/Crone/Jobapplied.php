<?php
namespace App\Http\Controllers\Crone;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as Jobs;
use App\Models\Users\Buyerreview as Buyerreview;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Users\Sellerprofile as Sellerprofile;



class Jobapplied extends Controller {

	public function index() {

		$jdata = Jobs::get();
		if(count($jdata)){
			foreach($jdata as $key => $value) {
				$reviewData=array();	
				$AppliedJob = AppliedJobModel::where('i_applied_id',$value->id)->count();
				$update['i_applied'] = $AppliedJob;
				Jobs::find($value->id)->update($update);
			}
		}	
		return 1;

	}

	public function signupdate(){

		$userdata = UsersModel::where('i_delete','!=',"1")->get();
		if(count($userdata)){
			foreach ($userdata as $key => $value) {
				$update=array();
				$update['d_signup_date']=date("Y-m-d",strtotime($value->d_added));	
				Jobs::where('i_user_id',$value->id)->update($update);
				CoursesModel::where('i_user_id',$value->id)->update($update);
				Sellerprofile::where('i_user_id',$value->id)->update($update);
			
			}
		}
		return 1;

	}
	
	
}