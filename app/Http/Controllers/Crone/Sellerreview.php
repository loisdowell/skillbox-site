<?php
namespace App\Http\Controllers\Crone;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Sellerreview as SellerreviewModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Sellerprofile as Sellerprofile;

class Sellerreview extends Controller {

	public function index() {

		$sellerprofile = Sellerprofile::get();
		$updateuser=array();

		if(count($sellerprofile)){
			foreach ($sellerprofile as $key => $value) {
				
				$reviewData = SellerreviewModel::where('i_seller_profile_id',$value->id)->get();
				
				$update['i_total_review'] = (int)count($reviewData);
				$update['i_total_avg_review']=0;
				$totalreview=0;

				if(count($reviewData)){
					foreach ($reviewData as $k => $v) {
						$totalreview = $totalreview+$v['i_communication_star']+$v['i_described_star']+$v['i_recommend_star'];
					}
					$totalmultiplay = count($reviewData)*3;
					$totalreview = $totalreview/$totalmultiplay;
				}

				$update['i_total_avg_review']=(float)$totalreview;
				Sellerprofile::find($value->id)->update($update);
				
				if(isset($updateuser[$value->i_user_id])){
					$updateuser[$value->i_user_id]['i_total_review'] += $update['i_total_review'];	
					$updateuser[$value->i_user_id]['i_total_avg_review'] += $update['i_total_avg_review'];
				}else{
					$updateuser[$value->i_user_id]['i_total_review'] = $update['i_total_review'];	
					$updateuser[$value->i_user_id]['i_total_avg_review'] = $update['i_total_avg_review'];
				}
			}
		}
	
		if(count($updateuser)){
			foreach ($updateuser as $key => $value) {
				if($value['i_total_review']>0){
					$updateuser['i_total_review'] = $value['i_total_review'];	
					$updateuser['i_total_avg_review'] = (float)$value['i_total_avg_review']/2;	
					UsersModel::find($key)->update($updateuser);
				}	
			}
		}
		return 1;

	}

	// public function SellerProfileRevew() {

	// 	$cdata = UsersModel::get();	

	// 	if(count($cdata)){
	// 		foreach ($cdata as $key => $value) {
				
	// 			$reviewData=array();	
	// 			$reviewData = SellerreviewModel::where('i_seller_id',$value->id)->get();

	// 			$totalreview="";
	// 			$multiplayavg = count($reviewData)*3;	
	// 			$update['i_total_review']=count($reviewData);
	// 			$update['i_total_avg_review']=0;
	// 			if(count($reviewData)){
	// 				foreach ($reviewData as $k => $v) {
	// 					$totalreview = $totalreview+$v['i_communication_star']+$v['i_described_star']+$v['i_recommend_star'];
	// 				}
	// 			}

	// 			if(count($reviewData)>0){
	// 				$update['i_total_avg_review'] = $totalreview/$multiplayavg;
	// 				$update['i_total_avg_review'] = $update['i_total_avg_review']/$update['i_total_review'];
	// 			}

				
	// 			Sellerprofile::where('i_user_id',$value->id)->update($update);
				
	// 			$cdata = CoursesModel::where('i_user_id',$value->id)->get();
	// 			$cids=array();
	// 			if(count($cdata)){
	// 				foreach ($cdata as $k => $v) {
	// 					$cids[]=$v->id;	
	// 				}
	// 			}
				
	// 			$creview = CoursesReviews::whereIn('i_courses_id', $cids)->get();
				
	// 			$multiplayavg = count($creview)*3;	
	// 			$totalreview=0;
	// 			if(count($creview)){
	// 				foreach ($creview as $k => $v) {
	// 					$totalreview = $totalreview+$v['f_rate_instruction']+$v['f_rate_navigate']+$v['f_rate_reccommend'];
	// 				}
	// 				$totalreview = $totalreview/$multiplayavg;
	// 				$totalreview = $totalreview/count($creview);
	// 			}
	// 			$update['i_course_total_avg_review']=$totalreview;
	// 			$update['i_course_total_review']=count($creview);
	// 			$update['i_job_total_avg_review']=0;
	// 			$update['i_job_total_review']=0;
	// 			UsersModel::find($value->id)->update($update);


	// 		}
	// 	}

	// 	return 1;
	// }

	
}