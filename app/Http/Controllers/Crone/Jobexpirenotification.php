<?php
namespace App\Http\Controllers\Crone;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as Jobs;
use App\Models\Users\Buyerreview as Buyerreview;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use MongoDB\BSON\UTCDateTime;


class Jobexpirenotification extends Controller {

	public function index() {

		$expdate = date('Y-m-d',strtotime('-2 days'));
		$jdata = Jobs::where('d_expiry_date',$expdate)->where('e_expire_notification',"!=","1")->get();
	
		if(count($jdata)){
			foreach($jdata as $key => $value) {
				
				$mailData['USER_FULL_NAME']="";
		        $mailData['USER_EMAIL']="";
		        $mailData['JOB_TITLE']=$value->v_job_title;
		        $mailData['JOB_EXPIRE_DATE']=date("M d,Y",strtotime($value->d_expiry_date));

		        if(count($value->hasUser()) && isset($value->hasUser()->v_fname)){
		            $mailData['USER_FULL_NAME']=$value->hasUser()->v_fname;
		        }
		        if(count($value->hasUser()) && isset($value->hasUser()->v_lname)){
		            $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$value->hasUser()->v_lname;
		        }
		         if(count($value->hasUser()) && isset($value->hasUser()->v_email)){
		            $mailData['USER_EMAIL']= $value->hasUser()->v_email;
		        }

		        $value->e_expire_notification="1";
		        $value->save();

		        $mailcontent = EmailtemplateHelper::JobExpireNotification($mailData);
		        $subject = EmailtemplateHelper::EmailTemplateSubject("5b585c111b544d5d44495722");
		        $mailids=array(
		            $mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
		        );
		        EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

		    }
		}	

		$cdata=date("Y-m-d");
		$jdata = Jobs::where('d_expiry_date',$cdata)->where('e_expired_notification',"!=","1")->get();
		
		if(count($jdata)){
			foreach($jdata as $key => $value) {
				$mailData['USER_FULL_NAME']="";
		        $mailData['USER_EMAIL']="";
		        $mailData['JOB_TITLE']=$value->v_job_title;
		        $mailData['JOB_EXPIRE_DATE']=date("M d,Y",strtotime($value->d_expiry_date));

		        if(count($value->hasUser()) && isset($value->hasUser()->v_fname)){
		            $mailData['USER_FULL_NAME']=$value->hasUser()->v_fname;
		        }
		        if(count($value->hasUser()) && isset($value->hasUser()->v_lname)){
		            $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$value->hasUser()->v_lname;
		        }
		         if(count($value->hasUser()) && isset($value->hasUser()->v_email)){
		            $mailData['USER_EMAIL']= $value->hasUser()->v_email;
		        }
		        $value->e_expired_notification="1";
		        $value->save();
		        $cdata=date("Y-m-d");
		        $mailcontent = EmailtemplateHelper::JobExpiredNotification($mailData);
		        $subject = EmailtemplateHelper::EmailTemplateSubject("5bd85241dabab22d45465a02");
		        $mailids=array(
		            $mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
		        );
		        EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		        
		    }
		}
		return 1;

	}
	
	
}