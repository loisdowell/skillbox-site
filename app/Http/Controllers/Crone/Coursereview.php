<?php
namespace App\Http\Controllers\Crone;

use Request, Hash, Lang,Validator,Auth,Session,Log;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Jobs as Jobs;
use App\Models\Users\Sellerprofile as Sellerprofile;




class Coursereview extends Controller {


	public function index() {

		$cdata = CoursesModel::get();
		$updateuser=array();

		if(count($cdata)){
			foreach ($cdata as $key => $value) {
				
				$reviewData=array();
				$reviewData = CoursesReviews::where('i_courses_id',$value->id)->get();

				$update['review']['total']=(int)count($reviewData);
				$update['review']['avgtotal']=0;
				$update['i_total_review']=(int)count($reviewData);
				$update['i_total_avg_review']=0;

				$totalreview=0;
				if(count($reviewData)){

					foreach ($reviewData as $k => $v) {
						$totalreview = $totalreview+$v['f_rate_instruction']+$v['f_rate_navigate']+$v['f_rate_reccommend'];
					}
					$multiplyavg = count($reviewData)*3;
					$totalreview = $totalreview/$multiplyavg;

					if(isset($updateuser[$value->i_user_id]['total_course'])){
						$updateuser[$value->i_user_id]['total_course'] += 1;
					}else{
						$updateuser[$value->i_user_id]['total_course'] = 1;
					}	


				}
				$update['review']['avgtotal']=(float)$totalreview;
				$update['i_total_avg_review']=(float)$totalreview;
				CoursesModel::find($value->id)->update($update);

				if(isset($updateuser[$value->i_user_id]['i_course_total_review'])){
					$updateuser[$value->i_user_id]['i_course_total_review'] += $update['i_total_review'];	
					$updateuser[$value->i_user_id]['i_course_total_avg_review'] += $update['i_total_avg_review'];
				}else{
					$updateuser[$value->i_user_id]['i_course_total_review'] = $update['i_total_review'];	
					$updateuser[$value->i_user_id]['i_course_total_avg_review'] = $update['i_total_avg_review'];
				}	
			}
		}
		
		if(count($updateuser)){
			foreach ($updateuser as $key => $value) {
				if($value['i_course_total_review']>0){
					$updateuser['i_course_total_review'] = $value['i_course_total_review'];	
					$updateuser['i_course_total_avg_review'] =(float)$value['i_course_total_avg_review']/$value['total_course'];
					UsersModel::find($key)->update($updateuser);
				}	
			}
		}
		return 1;

	}
	

	
	
}