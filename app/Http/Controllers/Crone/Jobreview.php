<?php
namespace App\Http\Controllers\Crone;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as Jobs;
use App\Models\Users\Buyerreview as Buyerreview;
use App\Models\Users\AppliedJob as AppliedJobModel;


class Jobreview extends Controller {


	public function index() {

		$userdata = UsersModel::where('i_delete','!=',"1")->get();
		

		if(count($userdata)){
			foreach ($userdata as $key => $value) {
				
				$reviewData=array();
				$reviewData = Buyerreview::where('i_buyer_id',$value->id)->get();
				$updateuser = array();
				$updateuser['i_job_total_review'] = count($reviewData);	
				$updateuser['i_job_total_avg_review'] = 0;	

				$update=array();
				$update['i_total_avg_review'] = 0;
				$update['i_total_review']=count($reviewData);

				$totalreview=0;
				if(count($reviewData)){
					
					foreach ($reviewData as $k => $v) {
						$totalreview = $totalreview+($v['i_work_star']+$v['i_support_star']+$v['i_opportunities_star']+$v['i_work_again_star'])/4;
					}

					$totalreview = $totalreview/count($reviewData);
					$updateuser['i_job_total_review'] = count($reviewData);	
					$updateuser['i_job_total_avg_review'] = $totalreview;	

					$update['i_total_avg_review'] = $totalreview;
					$update['i_total_review']=(int)count($reviewData);

				}

				UsersModel::where('_id',$value->id)->update($updateuser);
				Jobs::where('i_user_id',$value->id)->update($update);

			}
		}

		$jdata = Jobs::get();
		if(count($jdata)){
			foreach($jdata as $key => $value) {
				$update=array();
				$AppliedJob = AppliedJobModel::where('i_applied_id',$value->id)->count();
				$update['i_applied'] = $AppliedJob;
				Jobs::where('_id',$value->id)->update($update);
			}
		}
		return 1;


	}

	// public function index() {

	// 	$jdata = Jobs::get();
		
	// 	$updateuser=array();

	// 	if(count($jdata)){
	// 		foreach($jdata as $key => $value) {
				
	// 			$reviewData=array();	
	// 			$reviewData = Buyerreview::where('i_job_id',$value->id)->get();
				
	// 			$update['i_total_review']=(int)count($reviewData);
	// 			$update['i_total_avg_review']=0;

	// 			$totalreview="";
	// 			if(count($reviewData)){
	// 				foreach ($reviewData as $k => $v) {
	// 					$totalreview = $totalreview+($v['i_work_star']+$v['i_support_star']+$v['i_opportunities_star']+$v['i_work_again_star'])/4;
	// 				}
	// 				$multiplayavg = count($reviewData);//*4;	
	// 				$totalreview = $totalreview/$multiplayavg;	

	// 				if(isset($updateuser[$value->i_user_id]['total_job'])){
	// 					$updateuser[$value->i_user_id]['total_job'] += 1;
	// 				}else{
	// 					$updateuser[$value->i_user_id]['total_job'] = 1;
	// 				}	
	// 			}

	// 			$AppliedJob = AppliedJobModel::where('i_applied_id',$value->id)->count();
	// 			$update['i_applied'] = $AppliedJob;
	// 			$update['i_total_avg_review'] = $totalreview;
	// 			//Jobs::find($value->id)->update($update);

	// 			if(isset($updateuser[$value->i_user_id]['i_job_total_review'])){
	// 				$updateuser[$value->i_user_id]['i_job_total_review'] += $update['i_total_review'];	
	// 				$updateuser[$value->i_user_id]['i_job_total_avg_review'] += $update['i_total_avg_review'];
	// 			}else{
	// 				$updateuser[$value->i_user_id]['i_job_total_review'] = $update['i_total_review'];	
	// 				$updateuser[$value->i_user_id]['i_job_total_avg_review'] = $update['i_total_avg_review'];
	// 			}




	// 		}
	// 	}
		
	// 	if(count($updateuser)){
	// 		foreach ($updateuser as $key => $value) {
	// 			if($value['i_job_total_review']>0){
	// 				$updateuser['i_job_total_review'] = $value['i_job_total_review'];	
	// 				$updateuser['i_job_total_avg_review'] = $value['i_job_total_avg_review']/$value['total_job'];	
	// 				UsersModel::find($key)->update($updateuser);

	// 				$updatejob['i_total_review'] = $value['i_job_total_review'];		
	// 				$updatejob['i_total_avg_review'] = $value['i_job_total_avg_review']/$value['total_job'];
	// 				Jobs::where('i_user_id',$key)->update($updatejob);

	// 			}	
	// 		}
	// 	}


	// 	return 1;
	// }
	
	
}