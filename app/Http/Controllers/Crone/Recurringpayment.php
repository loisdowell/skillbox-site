<?php
namespace App\Http\Controllers\Crone;

use Request, Hash, Lang,Validator,Auth,Session,PDF;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Messages as MessagesModel;
use App\Models\Users\Order as OrderModal;
use App\Helpers\Emailtemplate as EmailtemplateHelper;

class Recurringpayment extends Controller {


	protected $section;
    private $mangopay;

    public function __construct(\MangoPay\MangoPayApi $mangopay){
        $this->section = Lang::get('section.Payment');
        $this->mangopay = $mangopay;
    }

	public function index() {

		$currentdate = date("Y-m-d");
		$userdata = UsersModel::where('v_plan.id',"!=",'5a65b48cd3e812a4253c9869')->where('v_plan.d_end_date','<=',$currentdate)->get();
		//dd($userdata);

		if(count($userdata)){

			$plandata = GeneralHelper::PlansList();
        	$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

        	$currency = "GBP";
	        $creditedwalletid = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
	        if($creditedwalletid!=""){
	            $creditedwalletid = (int)$creditedwalletid;  
	        }else{
	            $creditedwalletid=55569248;  
	        }

	       	foreach ($userdata as $key => $value) {

	       		$data['v_total_amount']=0;
		        $data['v_plan_name']="";
		        $data['v_plan_duration']="";

		       	if(isset($value['v_plan']['id']) && $value['v_plan']['id']!=""){   
		       		if(count($plandata)){
		                foreach ($plandata as $k => $v) {
		                    if($v['id'] == $value['v_plan']['id']){
		                        $data['v_plan_name'] = $v['v_name'];
		                        $data['v_plan_duration'] = $value['v_plan']['duration'];

		                        if(isset($value['v_plan']['duration']) && $value['v_plan']['duration']=="monthly"){
		                            $data['v_total_amount'] = $v['f_monthly_dis_price'];
		                        }else{
		                            $data['v_total_amount'] = $v['f_yealry_dis_price'];
		                        }
		                    }
		                }
		            }
		        }

		        $authorId = "";
		        if(isset($value->i_mangopay_id)){
		            $authorId = $value->i_mangopay_id;
		        }
		        $authorCardId = "";
		        if(isset($value->i_card_id)){
		            $authorCardId = $value->i_card_id;
		        }
		        $userid = $value->id;

		        $mangoamt = $data['v_total_amount']*100;
		        $PayIn = new \MangoPay\PayIn();
		        $PayIn->CreditedWalletId = (int)$creditedwalletid;
		        $PayIn->AuthorId = (int)$authorId;
		        $PayIn->PaymentType = "CARD";
		        $PayIn->DebitedFunds = new \MangoPay\Money();
		        $PayIn->DebitedFunds->Currency = "GBP";
		        $PayIn->DebitedFunds->Amount = (float)$mangoamt;
		        $PayIn->Fees = new \MangoPay\Money();
		        $PayIn->Fees->Currency = "GBP";
		        $PayIn->Fees->Amount = 0;
		        $PayIn->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
		        $PayIn->PaymentDetails->CardType = "CB_VISA_MASTERCARD";
		        $PayIn->PaymentDetails->CardId = (int)$authorCardId;
		        $PayIn->ExecutionType = "DIRECT";
		        $PayIn->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();
		        $PayIn->ExecutionDetails->SecureModeReturnURL = url('payment/card4');
		        $PayIn->ExecutionDetails->Culture = "EN";
		        $result = $this->mangopay->PayIns->Create($PayIn);
		        
		        if(count($result) && $result->Status=="SUCCEEDED"){

		            $insert['i_user_id']=$userid;
		            $insert['i_order_no']=GeneralHelper::OrderRefNumbaer();
		            $insert['e_transaction_type']="userplan";
		            $insert['e_payment_type']="mangopay";
		            $insert['v_transcation_id']=$result->Id;
		            $insert['e_payment_status']="success";
		            $insert['v_amount'] = $data['v_total_amount'];
		            $insert['v_order_detail']=array();
		            $insert['v_order_detail']['v_plan_id'] = $value['v_plan']['id'];
		            $insert['v_order_detail']['v_plan_name']=$data['v_plan_name'];    
		            $insert['v_order_detail']['v_plan_duration']= $data['v_plan_duration'];
		            $insert['v_order_detail']['v_action'] = "userplan";
            
		            $update['v_plan']['id'] = $value['v_plan']['id'];
		            $update['v_plan']['duration']=$data['v_plan_duration'];
		            $expdate="";

		            if($data['v_plan_duration']=="monthly"){
		                $expdate=date('Y-m-d',strtotime('+30 days'));
		            }else{
		                $expdate=date('Y-m-d',strtotime('+365 days'));
		            }
		            $update['v_plan']['d_start_date']=date("Y-m-d H:i:s");
		            $update['v_plan']['d_end_date']=$expdate;
		            $update['d_modified'] = date("Y-m-d H:i:s");
		           	UsersModel::find($userid)->update($update);

		            $insert['d_date']=date("Y-m-d");
		            $insert['d_added']=date("Y-m-d H:i:s");
		            $insert['d_modified']=date("Y-m-d H:i:s");
		            $orderid = OrderModal::create($insert)->id;

		            $orderdata = OrderModal::find($orderid);
		            if(count($orderdata)){
		                self::GenerateInvoice($orderdata);
		            }
                }
            }
		}	
		return 1;
	}

	public function GenerateInvoice($orderdata=array()){

        if(!count($orderdata)){
            return 0;
        }

        $userdata = auth()->guard('web')->user();
        $data['v_name']=$userdata->v_fname.' '.$userdata->v_lname;
        $data['v_address']="";
        if(isset($userdata->l_address)){
            $data['v_address'] = $userdata->l_address;
        }

        if(isset($userdata->v_city)){
            $data['v_address'] = $data['v_address'].''.$userdata->v_city;
        }
        
        $data['d_date'] = $orderdata->d_date;
        $data['orderno'] = $orderdata->i_order_no;
        $data['title'] = ucfirst($orderdata->v_order_detail['v_plan_duration']).' subscription for '.$orderdata->v_order_detail['v_plan_name'];
        $data['qty'] = 1;
        $data['price'] = $orderdata->v_amount;
        $data['subtotal'] = $orderdata->v_amount;
        $data['total'] = $orderdata->v_amount;
        $companyadd = GeneralHelper::getSiteSetting('SITE_ADDRESS');
        $sitename = GeneralHelper::getSiteSetting('SITE_NAME');

        $_data=array(
            'data'=>$data,
            'companyadd'=>$companyadd,
            'userdata'=>$userdata,
            'sitename'=>$sitename
        );
        $filepath = public_path('uploads/invoice/invoice.pdf');
        $pdf = PDF::loadView('frontend/account/subscription_invoice',$_data)->save($filepath);
        GeneralHelper::SendSkillboxRecuringSubscriptionEmail($userdata,$filepath);
        return 1;
    }



	
	
}