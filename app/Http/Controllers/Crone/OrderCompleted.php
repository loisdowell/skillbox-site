<?php
namespace App\Http\Controllers\Crone;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;


class OrderCompleted extends Controller {

	protected $section;
	private $mangopay;
    
    public function __construct(\MangoPay\MangoPayApi $mangopay){
		$this->section = Lang::get('section.Payment');
        $this->mangopay = $mangopay;
	}

	public function index() {

		$currentdate = date('Y-m-d',strtotime('-10 days'));
		$orderdata = OrderModal::where('e_transaction_type','skill')->where('v_order_status','delivered')->where('d_order_delivered_date','<=',$currentdate)->get();

		if(count($orderdata)){
			foreach ($orderdata as $key => $value) {

				$update=array();
			
			    $DebitedWalletID = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
		        if($DebitedWalletID!=""){
		            $DebitedWalletID = (int)$DebitedWalletID;  
		        }else{
		            $DebitedWalletID=55569248;  
		        }

		        $AuthorId = GeneralHelper::getSiteSetting('MANGOPAY_USERID');
	            if($AuthorId!=""){
	                $AuthorId = (int)$AuthorId;  
	            }else{
	                $AuthorId=55569242;  
	            }


	            $CreditedWalletId ="";	
	            if(count($value->hasUserSeller()) && isset($value->hasUserSeller()->i_wallet_id)){
	            	$CreditedWalletId = (int)$value->hasUserSeller()->i_wallet_id;
	            }
	           
	            $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER');
	            if($comission==""){
	                $comission=10;
	            }
	            $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER');
	            if($processing==""){
	                $processing=0.60;
	            }

	            $creditselleramt=0;
            	$comissionfees=0;
            	$processingamt=0;

            	$plandata=array();
                if(count($value->hasUserSeller()) && isset($value->hasUserSeller()->v_plan)){
                    $plandata = $value->hasUserSeller()->v_plan;
                }


                if(isset($plandata['id']) && $plandata['id']=="5a65b9f4d3e8124f123c986c"){

	                $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_PREMIUM');
	                if($comission==""){
	                    $comission=10;
	                }
	                $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_PREMIUM');
	                if($processing==""){
	                    $processing=0.60;
	                }


	            }

	            if(isset($plandata['id']) && $plandata['id']=="5a65b757d3e8125e323c986a"){

	                $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_STANDARD');
	                if($comission==""){
	                    $comission=10;
	                }
	                $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_STANDARD');
	                if($processing==""){
	                    $processing=0.60;
	                }

	            }

	            if(isset($plandata['id']) && $plandata['id']=="5a65b48cd3e812a4253c9869"){

	                $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_BASIC');
	                if($comission==""){
	                    $comission=10;
	                }
	                $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_BASIC');
	                if($processing==""){
	                    $processing=0.60;
	                }

	            }
	           
               	$creditselleramt = $value->v_amount;
                $comissionfees = $creditselleramt*$comission/100;
                $comissionfees = number_format($comissionfees,2);
                $processingamt = number_format($processing,2);
                $totalminus = $comissionfees+$processingamt;
                $creditselleramt = $creditselleramt-$totalminus;
                $creditselleramt = number_format($creditselleramt,2);
                $mangototalamt = $creditselleramt*100;

	            if($CreditedWalletId!=""){

	            	$Transfer = new \MangoPay\Transfer();
		            $Transfer->AuthorId = $AuthorId;
		            $Transfer->DebitedFunds = new \MangoPay\Money();
		            $Transfer->DebitedFunds->Currency = "GBP";
		            $Transfer->DebitedFunds->Amount = (float)$mangototalamt;
		            $Transfer->Fees = new \MangoPay\Money();
		            $Transfer->Fees->Currency = "GBP";
		            $Transfer->Fees->Amount = 0;
		            $Transfer->DebitedWalletID = $DebitedWalletID;
		            $Transfer->CreditedWalletId = (int)$CreditedWalletId;

		            $result = $this->mangopay->Transfers->Create($Transfer);
           		    if(count($result)){
		                if(isset($result->Status) && $result->Status=="SUCCEEDED"){
		                    $update['v_order_status']="completed";
							$update['d_order_completed_date']=date("Y-m-d H:i:s");
						    $update['e_seller_payment_status']="success";
		                    $update['e_seller_payment_amount']=$creditselleramt;
		                    $update['v_seller_commission']=$comissionfees;
                    		$update['v_seller_processing_fees']=$processingamt;   
                    	    $update['e_seller_connection_fee']=0;//$connectionfee; 
		                 	OrderModal::find($value->id)->update($update);	
		                }
		            }
		        }	
	        }
	            
		}	
		return 1;
	}

	public function directPayment() {

		$orderdata = OrderModal::where('e_transaction_type','skill')->where('v_order_status','delivered')->where('v_buyer_deliver_status',"accept")->get();

		if(count($orderdata)){
			foreach ($orderdata as $key => $value) {

				$update=array();
			    $DebitedWalletID = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
		        if($DebitedWalletID!=""){
		            $DebitedWalletID = (int)$DebitedWalletID;  
		        }else{
		            $DebitedWalletID=55569248;  
		        }

		        $AuthorId = GeneralHelper::getSiteSetting('MANGOPAY_USERID');
	            if($AuthorId!=""){
	                $AuthorId = (int)$AuthorId;  
	            }else{
	                $AuthorId=55569242;  
	            }
	           
	            $CreditedWalletId ="";	
	            if(count($value->hasUserSeller()) && isset($value->hasUserSeller()->i_wallet_id)){
	            	$CreditedWalletId = (int)$value->hasUserSeller()->i_wallet_id;
	            }


	            $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER');
	            if($comission==""){
	                $comission=10;
	            }
	            $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER');
	            if($processing==""){
	                $processing=0.60;
	            }

	            $creditselleramt=0;
            	$comissionfees=0;
            	$processingamt=0;

            	$plandata=array();
                if(count($value->hasUserSeller()) && isset($value->hasUserSeller()->v_plan)){
                    $plandata = $value->hasUserSeller()->v_plan;
                }


                if(isset($plandata['id']) && $plandata!="5a65b9f4d3e8124f123c986c"){

	                $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_PREMIUM');
	                if($comission==""){
	                    $comission=10;
	                }
	                $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_PREMIUM');
	                if($processing==""){
	                    $processing=0.60;
	                }
	            }

	            if(isset($plandata['id']) && $plandata['id']!="5a65b757d3e8125e323c986a"){

	                $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_STANDARD');
	                if($comission==""){
	                    $comission=10;
	                }
	                $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_STANDARD');
	                if($processing==""){
	                    $processing=0.60;
	                }
	            
	            }

	            if(isset($plandata['id']) && $plandata['id']!="5a65b48cd3e812a4253c9869"){

	                $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_BASIC');
	                if($comission==""){
	                    $comission=10;
	                }
	                $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_BASIC');
	                if($processing==""){
	                    $processing=0.60;
	                }

	            }

			   	$creditselleramt = $value->v_amount;
                $comissionfees = $creditselleramt*$comission/100;
                $comissionfees = number_format($comissionfees,2);
                $processingamt = number_format($processing,2);
                $totalminus = $comissionfees+$processingamt;
                $creditselleramt = $creditselleramt-$totalminus;
                $creditselleramt = number_format($creditselleramt,2);
                $mangototalamt = $creditselleramt*100;

	            if($CreditedWalletId!=""){

	            	$Transfer = new \MangoPay\Transfer();
		            $Transfer->AuthorId = $AuthorId;
		            $Transfer->DebitedFunds = new \MangoPay\Money();
		            $Transfer->DebitedFunds->Currency = "GBP";
		            $Transfer->DebitedFunds->Amount = (float)$mangototalamt;
		            $Transfer->Fees = new \MangoPay\Money();
		            $Transfer->Fees->Currency = "GBP";
		            $Transfer->Fees->Amount = 0;
		            $Transfer->DebitedWalletID = $DebitedWalletID;
		            $Transfer->CreditedWalletId = (int)$CreditedWalletId;

		            $result = $this->mangopay->Transfers->Create($Transfer);
           		    if(count($result)){
		                if(isset($result->Status) && $result->Status=="SUCCEEDED"){
		                    $update['v_order_status']="completed";
							$update['d_order_completed_date']=date("Y-m-d H:i:s");
						    $update['e_seller_payment_status']="success";
		                    $update['e_seller_payment_amount']=$creditselleramt;
		                    $update['v_seller_commission']=$comissionfees;
                    		$update['v_seller_processing_fees']=$processingamt;   
                    	    $update['e_seller_connection_fee']=0;//$connectionfee; 
		                 	OrderModal::find($value->id)->update($update);	
		                }
		            }
		        }	
	        }
	            
		}	
		return 1;
	}

	
	
	
}