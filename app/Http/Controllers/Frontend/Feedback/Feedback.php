<?php
namespace App\Http\Controllers\Frontend\Feedback;

use Request, Hash, Lang,Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Blogs\Blogs as BlogsModel;
use App\Models\Feedback\Feedback as FeedbackModel;
use App\Models\Pages\Pages as PagesModel;
use App\Models\Notify as NotifyModel;


class Feedback extends Controller {

	protected $section;
	public function __construct(){
		$this->section = Lang::get('section.blog');
	}

	public function index(){

		$cmsdata = PagesModel::where("v_key","feedback")->where("e_status","active")->first();
		if(!count($cmsdata)){
			return redirect('/');
		}

		$metaDetails['v_meta_title'] = $cmsdata->v_meta_title;
		$metaDetails['v_meta_keywords'] = $cmsdata->v_meta_keywords;
		$metaDetails['l_meta_description'] = $cmsdata->l_meta_description;
		$metaDetails['v_image'] = "";
		
		if(Storage::disk('s3')->exists($cmsdata->v_banner_img)){
            $metaDetails['v_image'] = Storage::cloud()->url($cmsdata->v_banner_img);      
        }

		$data=array(
			'cmsdata'=>$cmsdata,
			'metaDetails'=>$metaDetails,
		);
		
		return view('frontend/Feedback/feedback',$data);
	}
	
	public function postFeedback(){
		
		$data = Request::all();
		
		if(isset($data['_token'])){
			unset($data['_token']);	
		}

		$data['d_added']=date("Y-m-d H;i:s");
		FeedbackModel::insert($data);
		return redirect('cms/feedback')->with(['success' => 'Your feedback has been submitted successfully.']);

	}

	public function postNotify(){
		
		$data = Request::all();

		if(isset($data['_token'])){
			unset($data['_token']);	
		}

		$existdata = NotifyModel::where('v_email',$data['v_email'])->get();
		if(!count($existdata)){
			$data['i_count']="0";
			$data['d_added']=date("Y-m-d H:i:s");
			$data['d_modified']=date("Y-m-d H:i:s");
			NotifyModel::insert($data);
		}

		$response['status']=1;
		$response['msg'] = "";
		echo json_encode($response);
		exit;

	}

	public function UnscubsribedNotify($id){

		$data = NotifyModel::find($id);
		if(!count($data)){
			return redirect('unsubscribed/thanks');	
		}
		$update['i_delete']="1";
		NotifyModel::where('v_email',$data->v_email)->update($update);
		return redirect('unsubscribed/thanks');
	}

	public function UnscubsribedThanks(){
		return view('frontend/Feedback/thanks');
	}

	


}