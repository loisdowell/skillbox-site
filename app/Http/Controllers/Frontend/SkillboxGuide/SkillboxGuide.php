<?php
namespace App\Http\Controllers\Frontend\SkillboxGuide;

use Request;
use App\Http\Controllers\Controller;
use App\Models\Faqs as FaqsModel;
use App\Helpers\General as GeneralHelper;


class SkillboxGuide extends Controller {
	
	public function index() {
		
		$search  = 	Request::all();

		$skillboxGuide = FaqsModel::query();

		if( isset($search['search']) && $search['search'] != "" ) {
			$search = $search['search'];
			$skillboxGuide = $skillboxGuide->where(function($q) use ($search) {
				$q->where('v_question','like','%'.$search.'%')
					->orWhere('l_answer','like','%'.$search.'%');
			});
		}
		else {
			$search = '';
		}
		$skillboxGuide = $skillboxGuide->orderBy('d_added', 'DESC');
		$skillboxGuide = $skillboxGuide->get();
		
		$data=array();
		if(isset($skillboxGuide) && count($skillboxGuide)){
			foreach ($skillboxGuide as $key => $value) {
				$data[$value->e_type][$key] = $value;
			}
		}
		
		$metaDetails = GeneralHelper::MetaFiledData('skillbox-faq','Skillbox faq');
		$_data	= array(
			'data' => $data,
			'search' => $search,
			'metaDetails'=> $metaDetails
		);
		return view('frontend/skillbox-guide/skillbox-guide',$_data);
	}

	public function SearchFaq() {
		
		$data =	Request::all();
 		$skillboxGuide = FaqsModel::query();
		
		if(isset($data['search']) && $data['search'] != ""){
			$search = $data['search'];
			$skillboxGuide = $skillboxGuide->where(function($q) use ($search) {
				$q->where('v_question','like','%'.$search.'%')
					->orWhere('l_answer','like','%'.$search.'%');
			});
		}
		$skillboxGuide = $skillboxGuide->orderBy('d_added', 'DESC');
		$skillboxGuide = $skillboxGuide->get();
		
		$data=array();
		if(isset($skillboxGuide) && count($skillboxGuide)){
			foreach ($skillboxGuide as $key => $value) {
				$data[$value->e_type][$key] = $value;
			}
		}
		$_data	= array(
			'data' => $data,
		);
		$faqstr = view('frontend/skillbox-guide/skillbox-guide-search',$_data);
		$responsestr = $faqstr->render();
		$response['status']=1;
		$response['responsestr']=$responsestr;
		echo json_encode($response);
		exit;
	}

}