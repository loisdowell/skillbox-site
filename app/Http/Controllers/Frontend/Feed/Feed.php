<?php
namespace App\Http\Controllers\Frontend\Feed;

use Request, Hash, Lang,Validator,Auth,Storage,Cart;

use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;
use App\Models\Courses\BuyerCourse as BuyerCourse; 
use App\Models\Blogs\Blogs as BlogsModel;

//use MongoDB\BSON\ObjectId;


class Feed extends Controller {
		
	protected $section;

	public function __construct(){
		$this->section = Lang::get('section.feed');
	}

    public function index() {


        $blog = BlogsModel::where("e_status","active")->orderBy("d_added","DESC")->get();
        
        $data=array(
            'posts'=>$blog,
        );
        $faqstr = view('frontend/Feed/feed',$data);
        $responsestr = '<?xml version="1.0" encoding="UTF-8"?>';
        $responsestr .= $faqstr->render();

        


        return response($responsestr, 200)->header('Content-Type', 'application/rss+xml; charset=ISO-8859-1');

       
    }


   	


}