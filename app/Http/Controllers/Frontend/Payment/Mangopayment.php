<?php
namespace App\Http\Controllers\Frontend\Payment;

use Request, Hash, Lang,Validator,Auth,Storage,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;

class Mangopayment extends Controller {
		
	protected $section;

	public function __construct(){
		$this->section = Lang::get('section.Payment');
	}

	public function index($id="") {

        $response = Request::all();
        print_r($id);
        dd($response);

		$ordersummary = Session::get('ordersummary');
        $plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

		if(!count($ordersummary)){
			return redirect('dashboard');
		}

		$data['v_total_amount']=0;
		$data['v_plan_name']="";
		$data['v_plan_duration']="";

		if(isset($ordersummary['v_plan_id']) && $ordersummary['v_plan_id']!=""){	
			if(count($plandata)){
				foreach ($plandata as $key => $value) {
		 			if($value['id'] == $ordersummary['v_plan_id']){
						$data['v_plan_name'] = $value['v_name'];
						$data['v_plan_duration'] = $ordersummary['v_plan_duration'];
						if(isset($ordersummary['v_plan_duration']) && $ordersummary['v_plan_duration']=="monthly"){
							$data['v_total_amount'] = $data['v_total_amount']+$value['f_monthly_dis_price'];
						}else{
							$data['v_total_amount'] = $data['v_total_amount']+$value['f_yealry_dis_price'];
						}
					}

				}
			}
		}
	
		$_data=array(
			'data'=>$data
		);
       return view('frontend/Payment/payment',$_data);

	}


	public function paymentWithPaypal(){

		$ordersummary = Session::get('ordersummary');
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

		if(!count($ordersummary)){
			return redirect('dashboard');
		}
		    
		$data['v_total_amount']=0;
		$data['v_plan_name']="";
		$data['v_plan_duration']="";

    	if(isset($ordersummary['v_plan_id']) && $ordersummary['v_plan_id']!=""){	
			if(count($plandata)){
				foreach ($plandata as $key => $value) {
						
					if($value['id'] == $ordersummary['v_plan_id']){
						$data['v_plan_name'] = $value['v_name'];
						$data['v_plan_duration'] = $ordersummary['v_plan_duration'];
						if(isset($ordersummary['v_plan_duration']) && $ordersummary['v_plan_duration']=="monthly"){
							$data['v_total_amount'] = $value['f_monthly_dis_price'];
						}else{
							$data['v_total_amount'] = $value['f_yealry_dis_price'];
						}
					}
                }
			}
		}

        $currency = "EUR";
        $paypalid = GeneralHelper::getSiteSetting("PAYPAL_EMIAL");
        $paypalmode = GeneralHelper::getSiteSetting("PAYPAL_MODE");

        $paypalurl="https://www.sandbox.paypal.com/cgi-bin/webscr";
        if($paypalmode=="live"){
            $paypalurl="https://www.paypal.com/cgi-bin/webscr";
        }else{
            $paypalurl="https://www.sandbox.paypal.com/cgi-bin/webscr";
        }
        $userid = auth()->guard('web')->user()->id;
	    $custom = 'no';
	    
        $_data=array(
            'paypalURL' =>$paypalurl,
            'paypalID'  => $paypalid,
            'item_name' => 'Skillbox Subscription of '.$data['v_plan_name'].' Membership charge',
            'item_number' => $userid.'#'.$ordersummary['v_plan_id'].'#'.$data['v_plan_name'].'#'.$data['v_plan_duration'],
            'custom' => $custom,
            'amount'    => $data['v_total_amount'],
            'currency_code' => $currency,
            'cancel_return' => url('payment/paypal-cancel'),
            'return_url' => url('payment/paypal-return'),
            'cmd' => '_xclick',
        );
        return view('frontend/Payment/paypal-payment',$_data);
    }



    public function getPaypalReturn() {

        $response = Request::all();
        if(!count($response)){
          return redirect('/')->with( 'warning', 'Something went wrong.');
	    }

        $oldpaymentdata = OrderModal::where("v_transcation_id",$response['txn_id'])->first();

        $orderdata=array();
        if($response['item_number']!=""){
            $orderdata = explode("#", $response['item_number']);
        }

        if(!count($oldpaymentdata)){

        	$orderdata=array();
        	if($response['item_number']!=""){
        		$orderdata = explode("#", $response['item_number']);
        	}
            
        	$insert = array();
	       	if(isset($orderdata[0])){
				$insert['i_user_id']=$orderdata[0];
        	}
            
            $insert['i_order_no']=GeneralHelper::OrderRefNumbaer();
            $insert['e_transaction_type']="userplan";
        	$insert['e_payment_type']="paypal";
        	$insert['v_transcation_id']=$response['txn_id'];

        	if($response['payment_status']=="Pending"){
        		$insert['e_payment_status']="pending";
        	}else if($response['payment_status']=="Completed" || $response['payment_status']=="Processed"){
        		
        		$insert['e_payment_status']="success";

        		if(isset($orderdata[0]) && isset($orderdata[1]) && isset($orderdata[3])){
        			
        			$update['v_plan']['id']=$orderdata[1];
        			$update['v_plan']['duration']=$orderdata[3];
        			$expdate="";
        			
        			if($orderdata[3]=="monthly"){
        				$expdate=date('Y-m-d',strtotime('+30 days'));
        			}else{
        				$expdate=date('Y-m-d',strtotime('+365 days'));
        			}

        			$update['v_plan']['d_start_date']=date("Y-m-d H:i:s");
        			$update['v_plan']['d_end_date']=$expdate;
        			$update['d_modified'] = date("Y-m-d H:i:s");
        			UsersModel::find($orderdata[0])->update($update);
        		}

        	}else{
        		$insert['e_payment_status']=$response['payment_status'];
        	}

            $ordersummary = Session::get('ordersummary');
           
        	$insert['v_amount']=$response['payment_gross'];
        	$insert['v_order_detail']=array();
        	if(isset($orderdata[1])){
				$insert['v_order_detail']['v_plan_id']=$orderdata[1];
        	}
        	if(isset($orderdata[2])){
				$insert['v_order_detail']['v_plan_name']=$orderdata[2];
        	}
        	if(isset($orderdata[3])){
				$insert['v_order_detail']['v_plan_duration']=$orderdata[3];
        	}

            if(isset($ordersummary['v_action'])){
                $insert['v_order_detail']['v_action']=$ordersummary['v_action'];
            }

            if(isset($ordersummary['v_activate_profile'])){
                $insert['v_order_detail']['v_activate_profile']=$ordersummary['v_activate_profile'];
            }

        	$insert['d_date']=date("Y-m-d");
        	$insert['d_added']=date("Y-m-d H:i:s");
        	$insert['d_modified']=date("Y-m-d H:i:s");
        	OrderModal::create($insert);
        }else{

        	$updateorder=array();

        	if($response['payment_status']=="Pending"){
        		$updateorder['e_payment_status']="pending";
        	}else if($response['payment_status']=="Completed" || $response['payment_status']=="Processed"){
        		
        		$updateorder['e_payment_status']="success";
                $userdata = UsersModel::find($orderdata[0]);

        		if(isset($orderdata[0]) && isset($orderdata[1]) && isset($orderdata[3])){
        			
        			$update['v_plan']['id']=$orderdata[1];
        			$update['v_plan']['duration']=$orderdata[3];
        			$expdate="";

        			if($orderdata[3]=="monthly"){
        				$expdate=date('Y-m-d',strtotime('+30 days'));
        			}else{
        				$expdate=date('Y-m-d',strtotime('+365 days'));
        			}
                    $update['v_plan']['d_start_date']=date("Y-m-d H:i:s");
        			$update['v_plan']['d_end_date']=$expdate;
        			$update['d_modified'] = date("Y-m-d H:i:s");

                    if(isset($oldpaymentdata->v_order_detail) && isset($oldpaymentdata->v_order_detail['v_activate_profile'])){
                        $profile = $oldpaymentdata->v_order_detail['v_activate_profile'];
                        $update[$profile] = $userdata->$profile;
                        $update[$profile]['e_status']="active";
                    }
                    UsersModel::find($orderdata[0])->update($update);
        		}
        	}else{
        		$updateorder['e_payment_status']=$response['payment_status'];
        	}
        	$updateorder['d_modified']=date("Y-m-d H:i:s");
        	OrderModal::where('v_transcation_id',$response['txn_id'])->update($updateorder);
        }


        $ordersummary = Session::get('ordersummary');
        if(isset($ordersummary['redirect'])){
        	Session::forget('ordersummary');
        	return redirect($ordersummary['redirect']);	
        }
        return redirect('dashboard');
    }

    
    public function getPaypalCancel() {
    	dd("Payment Cancel");	
    }

}