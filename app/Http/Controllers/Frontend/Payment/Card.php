<?php
namespace App\Http\Controllers\Frontend\Payment;

use Request, Hash, Lang,Validator,Auth,Storage,Session,PDF;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;

class Card extends Controller {
	
    protected $section;
    private $mangopay;

    public function __construct(\MangoPay\MangoPayApi $mangopay){
        $this->section = Lang::get('section.Payment');
        $this->mangopay = $mangopay;
    }

    public function index() {
            
        $userid = auth()->guard('web')->user()->_id;

        $ordersummary = Session::get('ordersummary');
        $plandata = GeneralHelper::PlansList();
        $discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");
        
        if(!count($ordersummary)){
            return redirect('dashboard');
        }
        if(isset(auth()->guard('web')->user()->i_card_id) && auth()->guard('web')->user()->i_card_id!=""){
            try{
                $card = $this->mangopay->Cards->Get(auth()->guard('web')->user()->i_card_id);
                if(count($card)){
                    //return redirect('payment/mangopay/card');
                    return redirect('payment/mangopay-option');

                }
            }
            catch (\Exception $e) {
                $update['i_card_id']="";
                UsersModel::find($userid)->update($update);
                return redirect('payment/card')->with(['warning' => 'Something went wrong.please try again after sometime']); 
            }    
        }
        $data['v_total_amount']=0;
        $data['v_plan_name']="";
        $data['v_plan_duration']="";

        if(isset($ordersummary['v_plan_id']) && $ordersummary['v_plan_id']!=""){    
            if(count($plandata)){
                foreach ($plandata as $key => $value) {
                    if($value['id'] == $ordersummary['v_plan_id']){
                        $data['v_plan_name'] = $value['v_name'];
                        $data['v_plan_duration'] = $ordersummary['v_plan_duration'];
                        if(isset($ordersummary['v_plan_duration']) && $ordersummary['v_plan_duration']=="monthly"){
                            $data['v_total_amount'] = $value['f_monthly_dis_price'];
                        }else{
                            $data['v_total_amount'] = $value['f_yealry_dis_price'];
                        }
                    }
                }
            }
        }
        
        $MangopayUserId = auth()->guard('web')->user()->i_mangopay_id;
        $cardRegister = new \MangoPay\CardRegistration();
        $cardRegister->UserId = (int)$MangopayUserId;
        $cardRegister->Currency = "GBP";
        $cardRegister->CardType = "CB_VISA_MASTERCARD";
        $result = $this->mangopay->CardRegistrations->Create($cardRegister);
        
        if(count($result) && isset($result->Id) && $result->Id!=""){
            $update['i_card_id']=$result->Id;
            UsersModel::find($userid)->update($update);
        }

        $app=0;
        if(isset($ordersummary['app'])){
            $app=1;
        }

        $_data = array(
            'result'=>$result,
            'data'=>$data,
            'app'=>$app,
            'returnurl'=>url('payment/mangopaycreatecard'),
        );
        return view('frontend/Payment/card',$_data);    
    }

    public function CardOption() {

        $ordersummary = Session::get('ordersummary');
        $data['app']=0;
        if(isset($ordersummary['app'])){
            $data['app']=1;
        }
        return view('frontend/Payment/cardoption',$data);    
    }

    public function postCardOption() {
        $data = Request::all();
        if(isset($data['v_card_option']) && $data['v_card_option']=="new"){
            $userid = auth()->guard('web')->user()->_id;
            $update['i_card_id']="";
            UsersModel::find($userid)->update($update);
            return redirect('payment/card');
        }else{
            return redirect('payment/mangopay/card');
        }
    }


    public function CreateCardMangopay() {

        $MangopayCardId = auth()->guard('web')->user()->i_card_id;
        $cardRegisterPut = $this->mangopay->CardRegistrations->Get($MangopayCardId);
        $cardRegisterPut->RegistrationData = isset($_GET['data']) ? 'data=' . $_GET['data'] : 'errorCode=' . $_GET['errorCode'];
        $result = $this->mangopay->CardRegistrations->Update($cardRegisterPut);
        
        $userid = auth()->guard('web')->user()->_id;
        if(count($result) && isset($result->Status) && $result->Status=="VALIDATED"){
            $update['i_card_id']=$result->CardId;
            UsersModel::find($userid)->update($update);
            return redirect('payment/mangopay/card');
        }else{
            $update['i_card_id']="";
            UsersModel::find($userid)->update($update);
            return redirect('payment/card')->with(['warning' => 'Something went wrong.please try again after sometime']); 
        }
    }

    public function DirectPaymentUsingCardId() {

        $ordersummary = Session::get('ordersummary');
        $plandata = GeneralHelper::PlansList();
        $discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

        if(!count($ordersummary)){
            return redirect('dashboard');
        }

        $userid = auth()->guard('web')->user()->id;
        $userdata = auth()->guard('web')->user();

        $data['v_total_amount']=0;
        $data['v_plan_name']="";
        $data['v_plan_duration']="";

        if(isset($ordersummary['v_plan_id']) && $ordersummary['v_plan_id']!=""){    
            if(count($plandata)){
                foreach ($plandata as $key => $value) {
                    if($value['id'] == $ordersummary['v_plan_id']){
                        $data['v_plan_name'] = $value['v_name'];
                        $data['v_plan_duration'] = $ordersummary['v_plan_duration'];
                        if(isset($ordersummary['v_plan_duration']) && $ordersummary['v_plan_duration']=="monthly"){
                            $data['v_total_amount'] = $value['f_monthly_dis_price'];
                        }else{
                            $data['v_total_amount'] = $value['f_yealry_dis_price'];
                        }
                    }
                }
            }
        }

        $thankspage = array(
            'rurl'=>url('dashboard'),
            'issuccess'=>0,
        );

        if($ordersummary['v_plan_id']=="5a65b757d3e8125e323c986a" && !isset($userdata->e_standard_free_used)){
            
            $standardplanFree = GeneralHelper::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
            if(isset($standardplanFree) && $standardplanFree!="" && $standardplanFree!=null){
                
                $update['v_plan']['id'] = $ordersummary['v_plan_id'];
                $update['v_plan']['duration'] = $ordersummary['v_plan_duration'];
                $expdate = date('Y-m-d',strtotime('+'.$standardplanFree.' months'));
                $update['v_plan']['d_start_date']=date("Y-m-d H:i:s");
                $update['v_plan']['d_end_date']=$expdate;
                $update['v_plan']['v_standard_free'] = $standardplanFree;
                $update['d_modified'] = date("Y-m-d H:i:s");
                $update['e_standard_free_used'] = "yes";

                if(isset($ordersummary['v_activate_profile']) && $ordersummary['v_activate_profile']=="seller"){
                    $update['seller']=$userdata->seller;
                    $update['seller']['e_status'] = "active";
                    if(isset($userdata->seller['v_service']) && $userdata->seller['v_service']=="online"){
                        $update['v_seller_online_service'] = "active";
                    }else{
                        $update['v_seller_inperson_service'] = "active";
                    }
                }
                if(isset($ordersummary['v_activate_profile']) && $ordersummary['v_activate_profile']=="buyer"){
                    $update['buyer']=$userdata->buyer;
                    $update['buyer']['e_status'] = "active";
                    if(isset($userdata->buyer['v_service']) && $userdata->buyer['v_service']=="online"){
                        $update['v_buyer_online_service'] = "active";
                    }else{
                        $update['v_buyer_inperson_service'] = "active";
                    }
                }
                UsersModel::find($userid)->update($update);
                self::FreeStandardEmailSend();

                if(isset($ordersummary['i_job_id']) && $ordersummary['i_job_id']!=""){
                    
                    $updatejobpost=array();
                    if(isset($ordersummary['i_job_type']) && $ordersummary['i_job_type']=="urgent"){
                        $updatejobpost['e_sponsor']="yes";
                    }else if(isset($ordersummary['i_job_type']) && $ordersummary['i_job_type']=="notify"){
                        $updatejobpost['e_sponsor']="yes";
                    }else{
                        $updatejobpost['e_sponsor']="yes";
                        $updatejobpost['e_sponsor_status']="start";
                    }
                    if(count($updatejobpost)){
                        JobsModel::find($ordersummary['i_job_id'])->update($updatejobpost);    
                    }
                    // $updatejobpost['e_sponsor']="yes";
                    // $updatejobpost['e_sponsor_status']="start";
                    // JobsModel::find($ordersummary['i_job_id'])->update($updatejobpost);
                }   

                $thankspage['issuccess']=1;    
                $thankspage['rurl']=$ordersummary['redirect'];
                Session::forget('ordersummary');

                Session::put('thankspage',$thankspage);
                return redirect('thanks');
            }

        }

        $currency = "GBP";
        $creditedwalletid = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
        if($creditedwalletid!=""){
            $creditedwalletid = (int)$creditedwalletid;  
        }else{
            $creditedwalletid=55569248;  
        }
        
        $authorId = "";
        if(isset(auth()->guard('web')->user()->i_mangopay_id)){
            $authorId = auth()->guard('web')->user()->i_mangopay_id;
        }
        $authorCardId = "";
        if(isset(auth()->guard('web')->user()->i_card_id)){
            $authorCardId = auth()->guard('web')->user()->i_card_id;
        }
        $userservice = auth()->guard('web')->user()->seller['v_service'];
        $userid = auth()->guard('web')->user()->id;
        
        $mangoamt = $data['v_total_amount']*100;
        $PayIn = new \MangoPay\PayIn();
        $PayIn->CreditedWalletId = (int)$creditedwalletid;
        $PayIn->AuthorId = (int)$authorId;
        $PayIn->PaymentType = "CARD";
        $PayIn->Tag="Skillbox Subscription charge";

        $PayIn->DebitedFunds = new \MangoPay\Money();
        $PayIn->DebitedFunds->Currency = "GBP";
        $PayIn->DebitedFunds->Amount = (float)$mangoamt;
        $PayIn->Fees = new \MangoPay\Money();
        $PayIn->Fees->Currency = "GBP";
        $PayIn->Fees->Amount = 0;
        $PayIn->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
        $PayIn->PaymentDetails->CardType = "CB_VISA_MASTERCARD";
        $PayIn->PaymentDetails->CardId = (int)$authorCardId;
        $PayIn->ExecutionType = "DIRECT";
        $PayIn->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();
        $PayIn->ExecutionDetails->SecureModeReturnURL = url('payment/card4');
        $PayIn->ExecutionDetails->Culture = "EN";
        $result = $this->mangopay->PayIns->Create($PayIn);

        $thankspage = array(
            'rurl'=>url('dashboard'),
            'issuccess'=>0,
        );
            
        if(count($result) && $result->Status=="SUCCEEDED"){

            $insert['i_user_id']=$userid;
            $insert['i_order_no']=GeneralHelper::OrderRefNumbaer();
            $insert['e_transaction_type']="userplan";
            $insert['e_payment_type']="mangopay";
            $insert['v_transcation_id']=$result->Id;
            $insert['e_payment_status']="success";
            $insert['v_amount'] = $data['v_total_amount'];
            $insert['v_order_detail']=array();
            $insert['v_order_detail']['v_plan_id'] = $ordersummary['v_plan_id'];
            $insert['v_order_detail']['v_plan_name']=$data['v_plan_name'];    
            $insert['v_order_detail']['v_plan_duration']=$data['v_plan_duration'];
            $insert['v_order_detail']['v_action'] = "userplan";
            
            if(isset($ordersummary['v_update'])){
                $sdata = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$userservice)->first();    
                if(count($sdata)){
                    $update['e_sponsor']="yes";
                    $update['e_sponsor_status']="start";
                    SellerprofileModel::where('i_user_id',$userid)->update($update);
                }
            }

            if(isset($ordersummary['i_course_id'])){
                $updatecourse['e_sponsor']="yes";
                $updatecourse['e_sponsor_status']="start";
                CoursesModel::find($ordersummary['i_course_id'])->update($updatecourse);
            }

            $update['v_plan']['id']=$ordersummary['v_plan_id'];
            $update['v_plan']['duration']=$ordersummary['v_plan_duration'];
            $expdate="";

            if($ordersummary['v_plan_duration']=="monthly"){
                $expdate=date('Y-m-d',strtotime('+30 days'));
            }else{
                $expdate=date('Y-m-d',strtotime('+365 days'));
            }
            $update['v_plan']['d_start_date']=date("Y-m-d H:i:s");
            $update['v_plan']['d_end_date']=$expdate;
            $update['d_modified'] = date("Y-m-d H:i:s");

            if(isset($ordersummary['v_activate_profile']) && $ordersummary['v_activate_profile']=="seller"){
                $update['seller']=$userdata->seller;
                $update['seller']['e_status'] = "active";
                if(isset($userdata->seller['v_service']) && $userdata->seller['v_service']=="online"){
                    $update['v_seller_online_service'] = "active";
                }else{
                    $update['v_seller_inperson_service'] = "active";
                }
            }
            UsersModel::find($userid)->update($update);

            if(isset($ordersummary['i_job_id']) && $ordersummary['i_job_id']!=""){
                $updatejobpost=array();
                if(isset($ordersummary['i_job_type']) && $ordersummary['i_job_type']=="urgent"){
                    $updatejobpost['e_sponsor']="yes";
                }else if(isset($ordersummary['i_job_type']) && $ordersummary['i_job_type']=="notify"){
                    $updatejobpost['e_sponsor']="yes";
                }else{
                    $updatejobpost['e_sponsor']="yes";
                    $updatejobpost['e_sponsor_status']="start";
                }
                if(count($updatejobpost)){
                    JobsModel::find($ordersummary['i_job_id'])->update($updatejobpost);    
                }
            }

            $insert['d_date']=date("Y-m-d");
            $insert['d_added']=date("Y-m-d H:i:s");
            $insert['d_modified']=date("Y-m-d H:i:s");
            $orderid = OrderModal::create($insert)->id;

            $orderdata = OrderModal::find($orderid);
            if(count($orderdata)){
                self::GenerateInvoice($orderdata);
            }
            
            $thankspage['issuccess']=1;

        }

        $ordersummary = Session::get('ordersummary');
        if(isset($ordersummary['redirect'])){
            $thankspage['rurl']=$ordersummary['redirect'];
            Session::forget('ordersummary');
        }
        $thankspage['page']="plan";
        Session::put('thankspage',$thankspage);
        return redirect('thanks');
    }

    public function Card4() {
        $response = Request::all();
        $update['i_card_id']=$response;
        UsersModel::create($update);
    }


    public function GenerateInvoice($orderdata=array()){

        if(!count($orderdata)){
            return 0;
        }

        $userdata = auth()->guard('web')->user();
        $data['v_name']=$userdata->v_fname.' '.$userdata->v_lname;
        $data['v_address']="";
        if(isset($userdata->l_address)){
            $data['v_address'] = $userdata->l_address;
        }

        if(isset($userdata->v_city)){
            $data['v_address'] = $data['v_address'].''.$userdata->v_city;
        }
        
        $data['d_date'] = $orderdata->d_date;
        $data['orderno'] = $orderdata->i_order_no;
        $data['title'] = ucfirst($orderdata->v_order_detail['v_plan_duration']).' subscription for '.$orderdata->v_order_detail['v_plan_name'];
        $data['qty'] = 1;
        $data['price'] = $orderdata->v_amount;
        $data['subtotal'] = $orderdata->v_amount;
        $data['total'] = $orderdata->v_amount;
        $companyadd = GeneralHelper::getSiteSetting('SITE_ADDRESS');
        $sitename = GeneralHelper::getSiteSetting('SITE_NAME');

        $_data=array(
            'data'=>$data,
            'companyadd'=>$companyadd,
            'userdata'=>$userdata,
            'sitename'=>$sitename

        );

        $filepath = public_path('uploads/invoice/invoice-'.time().'.pdf');
        $pdf = PDF::loadView('frontend/account/subscription_invoice',$_data)->save($filepath);
        GeneralHelper::SendSkillboxSubscriptionEmail($userdata,$filepath);

        return 1;
    }

    public function FreeStandardEmailSend(){

        $userdata = auth()->guard('web')->user();
        $data['USER_FULL_NAME']=$userdata->v_fname.' '.$userdata->v_lname;
        $data['USER_EMAIL'] = $userdata->v_email;
       
        $standardplanFree = GeneralHelper::getSiteSetting("STANDARD_MEMBERSHIP_FREE");
        $data['MONTH'] = $standardplanFree;   

        $mailcontent = EmailtemplateHelper::TrialStandardPlanEmailTemplate($data);
        $subject = "Free Standard Plan Subscription";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bc0774476fbae2d0a4354f5");
        
        $mailids=array(
            $data['USER_FULL_NAME']=>$data['USER_EMAIL'],
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

    

}