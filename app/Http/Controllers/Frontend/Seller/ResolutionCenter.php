<?php
namespace App\Http\Controllers\Frontend\Seller;

use Request, Hash, Lang,Validator,Auth,PDF,Storage,File;
use App\Helpers\General as GeneralHelper;
use App\Http\Controllers\Controller;
use App\Models\Support\Resolution as ResolutionModal;
use App\Models\Users\Order as OrderModal;
use App\Models\Users\Users as UsersModal;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\OrderRequirements;


class ResolutionCenter extends Controller {


	public function __construct(\MangoPay\MangoPayApi $mangopay){
	    $this->mangopay = $mangopay;
	}
	
	public function index() {
		
		$userid = auth()->guard('web')->user()->_id;
		$data = ResolutionModal::where("i_seller_id",$userid)->orderBy("d_added","DESC")->get();
		// dd($data);
		
		$_data=array(
			'data'=>$data
		);
		return view('frontend/seller/resolution-center',$_data);
	}

	public function cancelOrder() {

		$post_data = Request::all();
		$response = array();
		if(!isset($post_data['cancelOrderId'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}
		$data = OrderModal::where("_id",$post_data['cancelOrderId'])->first();
		
		if(isset($data) && count($data)){
				
			$post_data['v_order_status']='cancelled';
			$post_data['d_modified']=date("Y-m-d h:i:s");
			$post_data['e_cancelled_by']="seller";
		    $data->update($post_data);

		    $userid = auth()->guard('web')->user()->_id;
			$insert['e_type']="order_cancelled";
			$insert['i_user_id']=$userid;
			$insert['v_order_id']=$post_data['cancelOrderId'];
			$insert['d_added'] = date("Y-m-d H:i:s");
			$insert['d_modified'] = date("Y-m-d H:i:s");
			OrderRequirements::insert($insert);
		    self::sendEmailNotificationBuyer($data->id);
		    $amt = self::BuyerRefundPayment($post_data['cancelOrderId']);

		    $msg="Order cancelled successfully. The amount £".number_format($amt,2)."  is refunded into buyers account.";

		    $response['status']=1;
			$response['msg']=GeneralHelper::successMessage($msg);
			echo json_encode($response);
			exit;
		}
		else{

			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}

	}

	public function BuyerRefundPayment($oderid="") {
		
		$orderdata = OrderModal::find($oderid);
		if(!count($orderdata)){
			return 0;
		}

	    
       	$AuthorId = GeneralHelper::getSiteSetting('MANGOPAY_USERID');
        if($AuthorId!=""){
            $AuthorId = (int)$AuthorId;  
        }else{
            $AuthorId=55569242;  
        }

        $DebitedWalletID = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
        if($DebitedWalletID!=""){
            $DebitedWalletID = (int)$DebitedWalletID;  
        }else{
            $DebitedWalletID=55569248;  
        }

        $cancellfees = GeneralHelper::getSiteSetting('ORDER_CANCEL_ADMIN_FEES');
        if($cancellfees!=""){
            $cancellfees = $cancellfees;  
        }else{
            $cancellfees=2.5;  
        }

        $CreditedWalletId = "";	
        if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->i_wallet_id)){
        	$CreditedWalletId = (int)$orderdata->hasUser()->i_wallet_id;
        }

        $creditselleramt=0;
		$creditselleramt = $orderdata->v_amount+$orderdata->v_buyer_commission;
		$adminfees =$creditselleramt*$cancellfees/100;
		$creditselleramt = $creditselleramt-$adminfees;
	   	$mangototalamt = $creditselleramt*100;

	    if($CreditedWalletId!=""){
	    	$Transfer = new \MangoPay\Transfer();
	        $Transfer->AuthorId = $AuthorId;
	        $Transfer->DebitedFunds = new \MangoPay\Money();
	        $Transfer->DebitedFunds->Currency = "GBP";
	        $Transfer->DebitedFunds->Amount = (float)$mangototalamt;
	        $Transfer->Fees = new \MangoPay\Money();
	        $Transfer->Fees->Currency = "GBP";
	        $Transfer->Fees->Amount = 0;
	        $Transfer->DebitedWalletID = $DebitedWalletID;
	        $Transfer->CreditedWalletId = (int)$CreditedWalletId;
	        $result = $this->mangopay->Transfers->Create($Transfer);
			
			if(count($result)){
	            if(isset($result->Status) && $result->Status=="SUCCEEDED"){
	                $update['e_payment_status']="refund";
	                $update['v_admin_fees']=number_format($adminfees,2);
	                OrderModal::find($orderdata->id)->update($update);	
	            }
	        }
    	}

    	return $creditselleramt;
    }

    public function replyOrderResolution() {

		$post_data = Request::all();
		$response = array();
		if(!isset($post_data['rid'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		$data = ResolutionModal::find($post_data['rid']);

		if(!count($data)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		$update['l_reply']=$post_data['l_reply'];
		$update['d_reply_date']=date("Y-m-d H:i:s");

		ResolutionModal::find($post_data['rid'])->update($update);

		##Send Email to buyer for reply
		self::OrderCancelRequestReplyToBuyer($data->i_order_id,$update);

		$replystr="";
		$replystr.='<div class="row" style="margin-top:15px">';
		$replystr.='<div class="col-xs-3">';
		$replystr.='<div class="Original-data">';
		$replystr.='<h3>Original message by Me</h3>';
		$replystr.='<p>'.date("F d, Y. h:i:s A",strtotime($update['d_reply_date'])).'</p>';
		$replystr.='</div>';
		$replystr.='</div>';
		$replystr.='<div class="col-xs-9">';
		$replystr.='<div class="Original-second">';
		$replystr.='<p>'.$update['l_reply'].'</p>';
		$replystr.='</div>';
		$replystr.='</div>';
		$replystr.='</div>';

		$response['status']=1;	
		$response['replystr']=$replystr;
		$response['msg']=GeneralHelper::successMessage("Successfully send reply to buyer.");
		echo json_encode($response);
		exit;
	
	}	

	public function sendEmailNotificationBuyer($orderid=""){
		
		//$orderid="5b4f7c29516b462714005316";
		$order = OrderModal::find($orderid);

		$mailData['USER_FULL_NAME']="";
        $mailData['USER_EMAIL']="";
        $mailData['ORDER_NO']=$order->i_order_no;
        $mailData['ORDER_TITLE']=$order->v_order_detail[0]['name'];
    
        if(count($order->hasUser()) && isset($order->hasUser()->v_fname)){
            $mailData['USER_FULL_NAME']=$order->hasUser()->v_fname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_lname)){
            $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$order->hasUser()->v_lname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_email)){
            $mailData['USER_EMAIL']= $order->hasUser()->v_email;
        }

        $mailcontent = EmailtemplateHelper::OrderCanceledToBuyer($mailData);
        $subject = "Order Cancelled";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5b5827c71b544d15094fbab2");
        
        $mailids=array(
            $mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

    public function OrderCancelRequestReplyToBuyer($orderid="",$data=array()){
		
		$order = OrderModal::find($orderid);

		$mailData['USER_FULL_NAME']="";
        $mailData['USER_EMAIL']="";
        $mailData['ORDER_NO']=$order->i_order_no;
        $mailData['ORDER_TITLE']=$order->v_order_detail[0]['name'];
        $mailData['MESSAGE']=$data['l_reply'];
    
        if(count($order->hasUser()) && isset($order->hasUser()->v_fname)){
            $mailData['USER_FULL_NAME']=$order->hasUser()->v_fname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_lname)){
            $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$order->hasUser()->v_lname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_email)){
            $mailData['USER_EMAIL']= $order->hasUser()->v_email;
        }

        $mailcontent = EmailtemplateHelper::OrderCancelRequestReplyToBuyer($mailData);
        $subject = "Order Cancel Request Reply";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5b5851ac1b544d58f07925b3");
        $mailids=array(
            $mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }



}