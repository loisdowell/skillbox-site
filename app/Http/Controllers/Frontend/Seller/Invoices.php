<?php
namespace App\Http\Controllers\Frontend\Seller;
use Request, Hash, Lang,Validator,Auth,Session,PDF,App;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Order as OrderModel;

class Invoices extends Controller {

	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.Invoices');
	}

	public function courseInvoice() {	

		if(!auth()->guard('web')->check()) {
			return redirect('/');		
		}
		
		$userdata = auth()->guard('web')->user();
		
		$query = OrderModel::query();
		$query = $query->where(function($query ) use($userdata){
        $query->where('e_transaction_type','skill')
              ->orWhere('e_transaction_type', 'course');
    	});	
		$query = $query->where('e_seller_payment_status',"success");
		$query = $query->where('i_seller_id',$userdata->id);
		$invoice = $query->orderBy('d_added','DESC')->get();


		$data=array(
			'invoice'=>$invoice,
			'type'=>"Course",

		);
		return view('frontend/seller/invoice', $data);
	}

	public function downloadCourseInvoice($id=""){ 
		
		$orderdata = OrderModel::find($id);	
		$userdata = UsersModel::find($orderdata->i_user_id);
		$companyadd = GeneralHelper::getSiteSetting('SITE_ADDRESS');
		$sitename = GeneralHelper::getSiteSetting('SITE_NAME');
       
		$_data=array(
			'data'=>$orderdata,
			'userdata'=>$userdata,
			'companyadd'=>$companyadd,
			'sitename'=>$sitename
		);

		$pdf = PDF::loadView('frontend/seller/invoice_pdf',$_data);
    	return $pdf->download('invoice.pdf');	

    }

   	public function skillInvoice() {	

   		if(!auth()->guard('web')->check()) {
			return redirect('/');		
		}

		$userdata = auth()->guard('web')->user();
		
		$query = OrderModel::query();
		$query = $query->where('e_transaction_type',"skill");
		$query = $query->where('e_seller_payment_status',"success");
		$query = $query->where('i_user_id',$userdata->id);
		$invoice = $query->orderBy('d_added','DESC')->get();

		$data=array(
			'invoice'=>$invoice,
			'type'=>"Skill",
		);
		return view('frontend/seller/invoice', $data);
	}

	public function downloadSkillInvoice($id=""){ 
		
		$orderdata = OrderModel::find($id);	
		$userdata = UsersModel::find($orderdata->i_user_id);
		$companyadd = GeneralHelper::getSiteSetting('SITE_ADDRESS');
		$sitename = GeneralHelper::getSiteSetting('SITE_NAME');
       
		$_data=array(
			'data'=>$orderdata,
			'userdata'=>$userdata,
			'companyadd'=>$companyadd,
			'sitename'=>$sitename
		);

		$pdf = PDF::loadView('frontend/seller/invoice_pdf',$_data);
  		return $pdf->download('invoice.pdf');	

  	}


	

}