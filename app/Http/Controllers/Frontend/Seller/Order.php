<?php
namespace App\Http\Controllers\Frontend\Seller;

use Request, Hash, Lang,Validator,Auth,PDF,Storage,File;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\BuyerCourse as BuyerCourseModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesAnnouncements as CoursesAnnouncements;
use App\Models\Courses\CoursesQA as CoursesQA;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;
use App\Models\OrderRequirements;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Sellerreview as SellerreviewModal;
use App\Models\Support\Resolution as ResolutionModal;
use App\Helpers\Emailtemplate as EmailtemplateHelper;


class Order extends Controller {

	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.Courses');
	}

	public function index($status="") {
		
		$userid = auth()->guard('web')->user()->_id;

		$query = OrderModal::query();
		$query = $query->where('i_seller_id',$userid);
		$query = $query->where('e_transaction_type','skill');
		
		if($status != "cancelled"){
			//$query = $query->where('e_payment_status','success');
			if($status == "all"){
				$query = $query->where(function($query ) use($userid){
		        $query->where('e_payment_status','success')
		              ->orWhere('e_payment_status','refund');
		    	});	
		   	}else{
				$query = $query->where('e_payment_status','success');	
			}
		}

		if($status == "active"){
			$query = $query->where('v_order_status','active');			
		}
		else if($status == "delivered"){
			$query = $query->where('v_order_status','delivered');	
			$query = $query->where('v_buyer_deliver_status','!=','accept');	
		}
		else if($status == "completed"){

			$query = $query->where(function($query ) use($userid){
		        $query->where('v_order_status','completed')
		              ->orWhere('v_buyer_deliver_status','accept');
		    });
		}
		else if($status == "cancelled"){
			$query = $query->where('v_order_status','cancelled');	
		}
		
		$data = $query->orderBy("d_added","DESC")->paginate(20);
       	
	
		$query = OrderModal::query();
		$query = $query->where('i_seller_id',$userid);
		//$query = $query->where('e_payment_status','success');
		$order = $query->where('e_transaction_type','skill')->get();

		$total['active']=0;
		$total['cancelled']=0;
		$total['completed']=0;
		$total['delivered']=0;
		$total['order']=count($data);
		$total['orderamt']=0;

		if(count($order)){
			foreach ($order as $key => $value) {
				
				if($value->v_order_status=="cancelled"){
						$total['cancelled'] = $total['cancelled']+1;
				}
				if($value->e_payment_status=="success"){
					if($value->v_order_status=="active"){
						$total['active'] = $total['active']+1;
					}
					else if($value->v_order_status=="completed"){
						$total['completed'] = $total['completed']+1;
					}
					else if($value->v_order_status=="delivered"){
						if(isset($value->v_buyer_deliver_status) && $value->v_buyer_deliver_status=="accept"){
							$total['completed'] = $total['completed']+1;
						}else{
							$total['delivered'] = $total['delivered']+1;	
						}
					}
					$total['orderamt']  =$total['orderamt']+$value->v_amount;
				}	
			}	
		}

		$totalResolution = ResolutionModal::where("i_seller_id",$userid)->get();

		$totalResolutioncnt=0;
		if(count($totalResolution)){
			foreach ($totalResolution as $key => $val) {
				//dump($val->hasOrder()->v_order_status);
				if(isset($val->hasOrder()->v_order_status) && $val->hasOrder()->v_order_status != 'cancelled'){
					$totalResolutioncnt = $totalResolutioncnt+1;
				}				
			}
		}

		//dd($totalResolutioncnt);

		$_data=array(
			'data'=>$data,			
			'status'=>$status,
			'total'=>$total,			
			'totalResolution'=>$totalResolutioncnt,			
		);
		return view('frontend/seller/order',$_data);
	}

	public function detail($id=""){
		
		$data = OrderModal::find($id);

		if(!count($data)){
			return redirect("buyer/orders");
		}

		$sellerprofile=array();
		
		if(isset($data->i_seller_profile_id) && count($data->i_seller_profile_id)){
			$sellerprofile = SellerprofileModel::find($data->i_seller_profile_id);
		}
		$orderRequirements = OrderRequirements::where("v_order_id",$id)->orderBy("d_added","ASC")->get();
		$userdata = auth()->guard('web')->user();
		$sellerreview = SellerreviewModal::where('i_order_id',$id)->first();

		
		$comission=10;
		$processing=0.60;
		if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b9f4d3e8124f123c986c"){

            $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_PREMIUM');
            if($comission==""){
                $comission=10;
            }
            $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_PREMIUM');
            if($processing==""){
                $processing=0.60;
            }
        }
        
        if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b757d3e8125e323c986a"){
         
            $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_STANDARD');
            if($comission==""){
                $comission=10;
            }
            $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_STANDARD');
            if($processing==""){
                $processing=0.60;
            }
        }
        
        if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b48cd3e812a4253c9869"){
            $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_BASIC');
            if($comission==""){
                $comission=10;
            }
            $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_BASIC');
            if($processing==""){
                $processing=0.60;
            }
        }
    	
    	$_data=array(
			'data'=>$data,	
			'orderRequirements'=>$orderRequirements,
			'sellerprofile'=>$sellerprofile,
			'userdata'=>$userdata,
			'sellerreview'=>$sellerreview,
			'comission'=>$comission,
			'processing'=>$processing,
			
		);
		return view('frontend/seller/order_detail',$_data);

	}

	public function downloadInvoice($id=""){
		
		$pdf = PDF::loadView('frontend/buyer/courses_invoice');
        return $pdf->download('invoice.pdf');
	}

	public function postReviewCommenr(){

		$data= Request::all();
		$response = array();

		if(!isset($data['i_review_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		$userid = auth()->guard('web')->user()->_id;

		$update['l_answers']['l_comment'] = $data['l_comment'];
		$update['l_answers']['i_user_id'] = $userid;
		$update['l_answers']['d_added'] = date("Y-m-d H:i:s");

		SellerreviewModal::find($data['i_review_id'])->update($update);

		$reviewdata = SellerreviewModal::find($data['i_review_id']);
		$orderdata = OrderModal::find($reviewdata->i_order_id);

		self::orderReviewCommentNotification($orderdata,$data['l_comment']);
		


		$userdata = auth()->guard('web')->user();

		$btnstr="";
		$imgdata = GeneralHelper::userroundimage($userdata);
	    $btnstr.='<div class="accordion-content-progress1">';
		$btnstr.='<div class="ans-qus-final">';
		$btnstr.='<div class="Announcements-find Announcements-find-q">';
		$btnstr.='<div class="Announcements-man">'.$imgdata.'</div>';
		$btnstr.='<div class="Announcements-name Announcements-name-q">';
		$btnstr.='<p>Me</p>';
		$btnstr.='<p>'.$data['l_comment'].'</p>';
		$btnstr.='</div>';
		$btnstr.='</div>';
		$btnstr.='</div>';
		$btnstr.='</div>';

		$response['status']=1;
		$response['btnstr']=$btnstr;
		$response['msg']=GeneralHelper::successMessage("Comment posted successfully.");
		echo json_encode($response);
		exit;
	}

	public function orderReviewCommentNotification($data=array(),$l_comment){

		if(!count($data)){
			return 0;
		}
		$order = $data;
		
		$mailData['SELLER_NAME']="";
        $mailData['USER_EMAIL']="";
        $mailData['BUYER_NAME']="";
        $mailData['SKILL_REVIEW_COMMENT']="<b>Comment</b>:".$l_comment;
        $mailData['ORDER_NO']=$order->i_order_no;
        $mailData['ORDER_TITLE']=$order->v_order_detail[0]['name'];
        $mailData['ORDER_BUYER_LINK']=url('buyer/orders/detail').'/'.$order->id;

        $mailData['SELLER_NAME'] = auth()->guard('web')->user()->v_fname;
        $mailData['SELLER_NAME'] = $mailData['SELLER_NAME'].' '.auth()->guard('web')->user()->v_lname;

        if(count($order->hasUser()) && isset($order->hasUser()->v_fname)){
            $mailData['BUYER_NAME'] = $order->hasUser()->v_fname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_lname)){
            $mailData['BUYER_NAME'] = $mailData['BUYER_NAME'].' '.$order->hasUser()->v_lname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_email)){
            $mailData['USER_EMAIL']= $order->hasUser()->v_email;
        }

        $mailcontent = EmailtemplateHelper::SkillReviewCommentToBuyer($mailData);
        $subject = "Seller left comment on order review";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bbc81df76fbae1d7640125a");
        
        $mailids=array(
            $mailData['BUYER_NAME']=>$mailData['USER_EMAIL']
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

   	public function extendRequest(){

		$data= Request::all();
		$response = array();

		if(!isset($data['v_order_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->_id;

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$data['e_type']="extendtime";
		$data['i_user_id']=$userid;
		$data['v_extend_resquest_status']="pending";
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		OrderRequirements::insert($data);
		$messageextend='Seller requested to extend the order delivery by '.$data['v_duration_time'].' '.$data['v_duration_in'];
		self::EmailNotificationExtendRequestToBuyer($data,$messageextend);

		$btnstr="";
		$btnstr.='<div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">';
		$btnstr.='<p>Extend Order Request</p>';
		$btnstr.='<p class="litral-text">You have requested to extend the order delivery by '.$data['v_duration_time'].' '.$data['v_duration_in'].'.</p>';
		$btnstr.='<p class="litral-text">'.date("H:i M d Y",strtotime($data['d_added'])).'</p>';
		$btnstr.='</div>';


		$response['status']=1;
		$response['btnstr']=$btnstr;
		$response['msg']=GeneralHelper::successMessage("Your extend delivery time request sent successfully.");
		echo json_encode($response);
		exit;

	}	

	public function EmailNotificationExtendRequestToBuyer($data=array(),$messageextend=""){

		$order = OrderModal::find($data['v_order_id']);
		if(!count($order)){
			return 0;
		}

		$mailData['BUYER_NAME']="";
        $mailData['SELLER_NAME']="";
        $mailData['ORDER_NO']=$order->i_order_no;
        $mailData['ORDER_TITLE']=$order->v_order_detail[0]['name'];
        $mailData['ORDER_EXTEND_REQUEST']=$messageextend;
        $mailData['ORDER_BUYER_LINK'] = url('buyer/orders/detail').'/'.$order->id;
        $mailData['USER_EMAIL']="";

        $mailData['SELLER_NAME'] = auth()->guard('web')->user()->v_fname;
        $mailData['SELLER_NAME'] = $mailData['SELLER_NAME'].' '.auth()->guard('web')->user()->v_lname;

        if(count($order->hasUser()) && isset($order->hasUser()->v_fname)){
            $mailData['BUYER_NAME'] = $order->hasUser()->v_fname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_lname)){
            $mailData['BUYER_NAME'] = $mailData['BUYER_NAME'].' '.$order->hasUser()->v_lname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_email)){
            $mailData['USER_EMAIL']= $order->hasUser()->v_email;
        }

        $mailcontent = EmailtemplateHelper::OrderExtendDeliveryToBuyer($mailData);
        $subject = "Seller Request to Extend delivery date";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bbc987e76fbae2f04230d95");

        $mailids=array(
            $mailData['BUYER_NAME']=>$mailData['USER_EMAIL']
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

	
	public function SendDeliverHeader(){

		$data= Request::all();
	
		if(isset($data['_token'])){
			unset($data['_token']);
		}	

		$userid = auth()->guard('web')->user()->_id;
		$insert['e_type']="order_deliver";
		$insert['i_user_id']=$userid;
		$insert['v_order_id']=$data['v_order_id'];
		$insert['d_added'] = date("Y-m-d H:i:s");
		$insert['d_modified'] = date("Y-m-d H:i:s");
		OrderRequirements::insert($insert);
		$orderdata = OrderModal::find($data['v_order_id']);
		
		if(count($orderdata)){
			

			$update=array(
				'v_order_status'=>"delivered",
				'v_order_start'=>"no",
				'v_order_deliver_date'=>date("Y-m-d H:i:s"),
				'd_modified'=>date("Y-m-d H:i:s"),
			);
			OrderModal::find($data['v_order_id'])->update($update);	

			// $update=array(
			// 	'v_order_status'=>"delivered",
			// 	'd_modified'=>date("Y-m-d H:i:s"),
			// );
			// OrderModal::find($data['v_order_id'])->update($update);	
			
			## Email to buyer for deliver order
			$orderTitle="";
			if(isset($orderdata->v_order_detail[0]['name'])){
				$orderTitle = $orderdata->v_order_detail[0]['name'];
			}	
			$mailData['USER_FULL_NAME']="";
			$mailData['USER_EMAIL']="";
			$mailData['ORDER_NO']=$orderdata->i_order_no;
			$mailData['ORDER_TITLE']=$orderTitle;
			$mailData['SELLER_NAME']="";

			if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_fname)){
		        $mailData['USER_FULL_NAME']= $orderdata->hasUser()->v_fname;
		    }
			if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_lname)){
		        $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$orderdata->hasUser()->v_lname;
		    }
		    if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_email)){
		        $mailData['USER_EMAIL']= $orderdata->hasUser()->v_email;
		    }
		    if(isset(auth()->guard('web')->user()->v_fname)){
				$mailData['SELLER_NAME'] = 	auth()->guard('web')->user()->v_fname;
			}

			if(isset(auth()->guard('web')->user()->v_lname)){
				$mailData['SELLER_NAME'] = $mailData['SELLER_NAME'].' '.auth()->guard('web')->user()->v_lname;
			}

			$mailcontent = EmailtemplateHelper::DeliverOrder($mailData);
			$subject = "Order Delivered";
			$subject = EmailtemplateHelper::EmailTemplateSubject("5b561895516b462ef00036ad");
			$mailids=array(
				$mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
			);
			$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		}
		return redirect('seller/orders/detail/'.$data['v_order_id'])->with(['success' => 'Your order delivery has been successfully submitted.']);			

	}

	
	public function postRequirement(){

		$data= Request::all();
		$msg = "Your message has successfully posted.";
		if(isset($data['_token'])){
			unset($data['_token']);
		}

		if(!isset($data['v_order_id'])){
			return redirect('seller/orders/active')->with(['warning' => 'Something went wrong.Please try again after sometime.']);		
		}

		if(Request::hasFile('v_document_order')) {
			
			$image = Request::file('v_document_order');
			foreach($image as $file){
	        	
	        	$fileName = 'order-'.time().'.'.$file->getClientOriginalExtension();
	        	$filenameorignal = $file->getClientOriginalName();
	        	$fileName = str_replace(' ', '_', $fileName);
				$filenameorignal = str_replace('.', '=', $filenameorignal);
				$data['v_document'][$filenameorignal] = 'order/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/order/'.$fileName, File::get((string)$file), 'public');
		    }
		    unset($data['v_document_order']);
	    }

	    $userid = auth()->guard('web')->user()->_id;
		$data['e_type']="message";
		$data['i_user_id']=$userid;
		$data['v_extend_resquest_status']="";
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		OrderRequirements::insert($data);
		
		if(isset($data['v_order_deliver']) && $data['v_order_deliver']==1){
			
			$userid = auth()->guard('web')->user()->_id;
			$insert['e_type']="order_deliver";
			$insert['i_user_id']=$userid;
			$insert['v_order_id']=$data['v_order_id'];
			$insert['d_added'] = date("Y-m-d H:i:s");
			$insert['d_modified'] = date("Y-m-d H:i:s");
			OrderRequirements::insert($insert);
			$orderdata = OrderModal::find($data['v_order_id']);
			
			if(count($orderdata)){
				
				$update=array(
					'v_order_status'=>"delivered",
					'v_order_start'=>"no",
					'v_order_deliver_date'=>date("Y-m-d H:i:s"),
					'd_modified'=>date("Y-m-d H:i:s"),
				);
				OrderModal::find($data['v_order_id'])->update($update);	
				
				## Email to buyer for deliver order
				$orderTitle="";
				if(isset($orderdata->v_order_detail[0]['name'])){
					$orderTitle = $orderdata->v_order_detail[0]['name'];
				}	
				$mailData['USER_FULL_NAME']="";
				$mailData['USER_EMAIL']="";
				$mailData['ORDER_NO']=$orderdata->i_order_no;
				$mailData['ORDER_TITLE']=$orderTitle;
				$mailData['SELLER_NAME']="";

				if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_fname)){
			        $mailData['USER_FULL_NAME']= $orderdata->hasUser()->v_fname;
			    }
				if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_lname)){
			        $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$orderdata->hasUser()->v_lname;
			    }
			    if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_email)){
			        $mailData['USER_EMAIL']= $orderdata->hasUser()->v_email;
			    }
			    if(isset(auth()->guard('web')->user()->v_fname)){
					$mailData['SELLER_NAME'] = 	auth()->guard('web')->user()->v_fname;
				}

				if(isset(auth()->guard('web')->user()->v_lname)){
					$mailData['SELLER_NAME'] = $mailData['SELLER_NAME'].' '.auth()->guard('web')->user()->v_lname;
				}

				$mailcontent = EmailtemplateHelper::DeliverOrder($mailData);
				$subject = "Order Delivered";
				$subject = EmailtemplateHelper::EmailTemplateSubject("5b561895516b462ef00036ad");
				
				$mailids=array(
					$mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
				);
				$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

			}
			$msg = "Your order delivery has been successfully submited.";
		}else{
			self::EmailNotificationBuyer($data);	
		}
		return redirect('seller/orders/detail/'.$data['v_order_id'])->with(['success' => $msg]);		

	}

	public function EmailNotificationBuyer($data=array()){

		$order = OrderModal::find($data['v_order_id']);

		$mailData['USER_FULL_NAME']="";
        $mailData['USER_EMAIL']="";
        $mailData['ORDER_NO']=$order->i_order_no;
        $mailData['ORDER_TITLE']=$order->v_order_detail[0]['name'];
        $mailData['ORDER_MESSAGE']=$data['l_message'];

        if(count($order->hasUser()) && isset($order->hasUser()->v_fname)){
            $mailData['USER_FULL_NAME']=$order->hasUserSeller()->v_fname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_lname)){
            $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$order->hasUser()->v_lname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_email)){
            $mailData['USER_EMAIL']= $order->hasUser()->v_email;
        }

        $mailcontent = EmailtemplateHelper::OrderRequirementsBuyer($mailData);
        $subject = "Seller Posted message on order";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5b581eff1b544d14d61d5fa2");
        $mailids=array(
            $mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
        

    }


	

	




}