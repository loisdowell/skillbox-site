<?php
namespace App\Http\Controllers\Frontend\Seller;

use Request, Hash, Lang,Validator,Auth,Storage,File,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Models\Users\Jobs as JobsModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Buyerreview as BuyerreviewModel;
use App\Models\Users\Sellerreview as SellerreviewModel;
use App\Models\Faqs as FaqsModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Users\Order as OrderModal;
use App\Models\Howitwork as HowitworkModel;




class Seller extends Controller {
	
	protected $section;
	public function __construct(){
		$this->section = Lang::get('section.dashboard');
	}
	
	public function index(){
			
		$userservice = auth()->guard('web')->user()->seller['v_service'];
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");
		
		$category=array();
		if($userservice=="online"){
			$category = CategoriesModel::where("v_type","online")->where("e_status","active")->orderBy('v_name')->get();			
		}else{
			$category = CategoriesModel::where("v_type","inperson")->where("e_status","active")->orderBy('v_name')->get();			
		}

		$sellerProfile =array();
		$userdata =array();
		$skilldata = SkillsModel::where("e_status","active")->get();
		$planstep=1;

		$userid = auth()->guard('web')->user()->id;
		$sellerProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$userservice)->first();

		$other_skill = $other_skill_data = array();

		if(isset($sellerProfile) && count($sellerProfile)){
			if(isset($sellerProfile->i_otherskill_id) && count($sellerProfile->i_otherskill_id)){
				foreach ($sellerProfile->i_otherskill_id as $key => $value) {
					$other_skill[] = $value;
				}	
			}
		}
	
		$skills = SkillsModel::whereIn("_id",$other_skill)->get();
		if(isset($skills) && count($skills)){
			foreach ($skills as $key => $value) {
				$other_skill_data[$value->_id] = $value->v_name;
			}	
		}

	
		$userdata = UsersModel::find($userid);
	
		$data=array(
			'category'=>$category,
			'v_service'=>$userservice,
			'plandata'=>$plandata,
			'discount'=>$discount,
			'sellerProfile'=>$sellerProfile,
			'userdata'=>$userdata,
			'skilldata'=>$skilldata,
			'planstep'=>$planstep,
			'other_skill_data'=>$other_skill_data
		);
		return view('frontend/seller/signup_step',$data);
	}

	public function dashboard() {

		$userid = auth()->guard('web')->user()->_id;
		$userdata = UsersModel::find($userid);

		if(!isset(auth()->guard('web')->user()->i_country_id)){
			return redirect('profile/edit');
		}

		$usercnt = UsersModel::where("e_status","active")->get();
		$countryname = count($userdata->hasCountry())  ? $userdata->hasCountry()->v_title : '';
		$userplandata = GeneralHelper::userPlanDetail($userdata);
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

		$buyerreview = SellerreviewModel::where('i_seller_id',$userid)->get();
		$fAQs = FaqsModel::where('e_type', 'seller')->orderBy('d_added', 'DESC')->limit(5)->get();

		$howitswork = HowitworkModel::where("e_type","seller")->where('e_status',"active")->orderBy('i_order')->get();

		$leveldata=array();
		if(isset(auth()->guard('web')->user()->v_level) && auth()->guard('web')->user()->v_level!="0"){
            $leveldata = GeneralHelper::LevelData(auth()->guard('web')->user()->v_level);
        }

        $profiledata = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$userdata->seller['v_service'])->first();
       	
       	$data=array(
			'userdata'=>$userdata,
			'countryname'=>$countryname,
			'usercnt'=>count($usercnt),
			'userplandata'=>$userplandata,
			'plandata'=>$plandata,
			'discount'=>$discount,
			'buyerreview'=>$buyerreview,
			'leveldata'=>$leveldata,
			'fAQs'=>$fAQs,
			'howitswork'=>$howitswork,
			'profiledata'=>$profiledata,

		);
		return view('frontend/seller/dashboard',$data);
	}

	public function shortlistedJobs() {

		$s_jobs = $a_jobs = array();
		$userid = auth()->guard('web')->user()->_id;
	  	$shortlistedJobs = ShortlistedJobsModel::where('i_user_id',$userid)->get();
	  	$appliedJob = AppliedJobModel::where('i_user_id',$userid)->get();

		foreach ($appliedJob as $key => $value) {
			$a_jobs[] = $value->i_applied_id;
		}

	  	foreach ($shortlistedJobs as $key => $value) {
			$s_jobs[] = $value->i_shortlist_id;
		}
		$jobs_data = JobsModel::query()->whereIn('_id', $s_jobs)->get();
		$data=array(
			'jobs_data'=>$jobs_data,
			'a_jobs' => $a_jobs,
		);
		return view('frontend/seller/shortlisted_jobs',$data);
	}

	public function appliedJobs() {

		$a_jobs = array();
	
		$userid = auth()->guard('web')->user()->_id;
	  	
	  	$reviewdata = BuyerreviewModel::where('i_user_id',$userid)->get();

	  	$reviewlist=array();
	  	if(count($reviewdata)){
	  		foreach ($reviewdata as $key => $value) {
	  			$reviewlist[$value->i_job_id]=0;
	  		}
	  	}

	  	$shortlistedJobs = AppliedJobModel::where('i_user_id', $userid)->get();

	  	foreach ($shortlistedJobs as $key => $value) {
			$a_jobs[] = $value->i_applied_id;
		}

		$jobs_data = JobsModel::query()->whereIn('_id', $a_jobs)->get();

		//dd($jobs_data);
		
		$data=array(
			'jobs_data'=>$jobs_data,
			'reviewlist'=>$reviewlist
		);
		return view('frontend/seller/applied_jobs',$data);
	}

	public function deleteShortlistedJobs(){
		
		$data     = Request::all();

		if(isset($data['shortlistIds']) && $data['shortlistIds'] == ''){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Please select Job.");
			echo json_encode($response);
			exit;
		}
		
		$shortlistIds   = explode(',', $data['shortlistIds']);

		$userid = auth()->guard('web')->user()->id;
	
		ShortlistedJobsModel::whereIn('i_shortlist_id',$shortlistIds)->where('i_user_id',$userid)->delete();
	
		$response['status']=1;
		$response['msg']=GeneralHelper::successMessage("Successfully removed shortlisted jobs !");
		echo json_encode($response);
		exit;
	}

	public function deleteAppliedJobs(){
		
		$data     = Request::all();

		if(isset($data['Ids']) && $data['Ids'] == ''){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Please select Job.");
			echo json_encode($response);
			exit;
		}
		
		$Ids   = explode(',', $data['Ids']);

		$userid = auth()->guard('web')->user()->id;
	
		AppliedJobModel::whereIn('i_applied_id',$Ids)->where('i_user_id',$userid)->delete();
	
		$response['status']=1;
		$response['msg']=GeneralHelper::successMessage("Successfully removed applied jobs !");
		echo json_encode($response);
		exit;
	}

	public function activateProfile(){
		
		$data = Request::all();
		$response=array();

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		
		$userid = auth()->guard('web')->user()->_id;
		$userdata = UsersModel::find($userid);
		
		if(!count($userdata)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$isbasicplan = GeneralHelper::userbasicPlanCheck($userdata);

		if($isbasicplan){
			$response['status']=2;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if($userdata->seller['v_service']=="online"){
			$update['v_seller_online_service']="active";
		}
		if($userdata->seller['v_service']=="inperson"){
			$update['v_seller_inperson_service']="active";
		}
	
		$update['seller'] = $userdata->seller;
		$update['seller']['e_status'] = "active";
		$update['d_modified']=date("Y-m-d H:i:s");
		UsersModel::find($userid)->update($update);

		$msg="You have successfully activated your profile.";

		if($userdata->seller['v_service']=="online"){
			$msg="You have successfully activated your Online profile.";
		}else{
			$msg="You have successfully activated your In Person profile.";			
		}
		$sellerdata = SellerprofileModel::where("i_user_id",$userdata->id)->where("v_service",$userdata->seller['v_service'])->get();

		$response['redirectprofile']=0;
		if(!count($sellerdata)){
			$response['redirectprofile']=1;	
		}
		$response['status']=1;
		$response['data']=$data;
		$response['msg']=GeneralHelper::successMessage($msg);
		echo json_encode($response);
		exit;
	}

	public function profileUpgradeUserPlan(){
	
		$data = Request::all();

		if(isset($data['_token'])){
			unset($data['_token']);
		}	

		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'seller/dashboard',
			'v_activate_profile'=>'seller',
			'v_action'=>'Activate profile',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],			
		);
		$userdata = auth()->guard('web')->user();

		if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869"){
			
			$userid = auth()->guard('web')->user()->id;
			$data['v_plan']['d_start_date']=date("Y-m-d");
			$data['v_plan']['d_end_date']=date('Y-m-d',strtotime('+4000 days'));	

			if($userdata->seller['v_service']=="online"){
				$data['v_seller_online_service']="active";
			}
			if($userdata->seller['v_service']=="inperson"){
				$data['v_seller_inperson_service']="active";
			}

			$data['seller']=$userdata->seller;
			$data['seller']['e_status'] = "active";
			$planupdate = GeneralHelper::upgradeUserPlan($userid,$data);
			
			$msg="You have successfully activated your profile.";

			if($userdata->seller['v_service']=="online"){
				$msg="You have successfully activated your Online profile.";
			}else{
				$msg="You have successfully activated your In Person profile.";			
			}

			$sellerdata = SellerprofileModel::where("i_user_id",$userdata->id)->where("v_service",$userdata->seller['v_service'])->get();
			if(!count($sellerdata)){
				return redirect('seller/edit-my-profile')->with(['success' => $msg]);				
			}
			return redirect('seller/dashboard')->with(['success' => $msg]);

		}

		$upgrade = GeneralHelper::upgradeUserPlanDetail($userdata,$data['v_plan']['id']);
		if($upgrade){
			Session::put('ordersummary',$orderdata);
			//return redirect('payment');
			return redirect('payment/card');
		}else{
			$update['seller']=$userdata->seller;
			$update['seller']['e_status'] = "active";
			UsersModel::find($userdata->id)->update($update);

			$msg="You have successfully activated your profile.";

			if($userdata->seller['v_service']=="online"){
				$msg="You have successfully activated your Online profile.";
			}else{
				$msg="You have successfully activated your In Person profile.";			
			}

			$sellerdata = SellerprofileModel::where("i_user_id",$userdata->id)->where("v_service",$userdata->seller['v_service'])->get();
			if(!count($sellerdata)){
				return redirect('seller/edit-my-profile')->with(['success' => $msg]);				
			}
			return redirect('seller/dashboard')->with(['success' => $msg]);
		}
	}

	public function upgradeUserPlan(){
	
		$data = Request::all();
		
		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'seller/dashboard',
			'v_action'=>'upgrade user plan',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],			
		);
		if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869"){
			$userid = auth()->guard('web')->user()->id;
			$data['d_start_date']=date("Y-m-d");
			$data['d_end_date']=date('Y-m-d',strtotime('+4000 days'));
			$planupdate = GeneralHelper::upgradeUserPlan($userid,$data);
			return redirect('seller/dashboard')->with(['success' => "You're already subscribed to this plan."]);
		}

		$userdata = auth()->guard('web')->user();
		$upgrade = GeneralHelper::upgradeUserPlanDetail($userdata,$data['v_plan']['id']);
		
		if($upgrade){
			Session::put('ordersummary',$orderdata);
			return redirect('payment/card');
		}else{
			return redirect('seller/dashboard')->with(['success' => "You're already subscribed to this plan."]);
		}

	}

	public function updateService(){
		
		$data = Request::all();
		$response=array();

		if(!isset($data['v_service']) && $data['v_service']==""){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->_id;
		$userdata = UsersModel::find($userid);

		if(!count($userdata)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		// if($data['v_service']=="online"){
		// 	$update['v_seller_online_service']="active";
		// }else{
		// 	$update['v_seller_inperson_service']="active";
		// }

		$statusdata="inactive";
		if($data['v_service']=="online" && $userdata->v_seller_online_service=="active"){
			$statusdata="active";
		}
		if($data['v_service']=="inperson" && $userdata->v_seller_inperson_service=="active"){
			$statusdata="active";
		}

		$update['seller'] = $userdata->seller;
		$update['seller']['v_service'] = $data['v_service'];
		$update['seller']['e_status'] = $statusdata;
		$update['d_modified'] = date("Y-m-d H:i:s");
		UsersModel::find($userid)->update($update);

		$response['status']=1;
		$response['active']=$statusdata;
		$response['data']=$data;
		$response['msg']=GeneralHelper::successMessage("Successfully update user service.");
		echo json_encode($response);
		exit;
	}


	public function updateProfileStatus(){
		
		$data = Request::all();
		$response=array();

		if(!isset($data['e_profile_status']) && $data['e_profile_status']==""){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->_id;
		$userdata = UsersModel::find($userid);

		if(!count($userdata)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		$update['e_profile_status']=$data['e_profile_status'];
		UsersModel::find($userid)->update($update);

		$response['msg']="";
		$response['active']=2;
		if($data['e_profile_status']=="on"){
			$profiledata = SellerprofileModel::where('i_user_id',$userid)->get();
			if(count($profiledata)){
				$updateprofile['e_status']="inactive";
				SellerprofileModel::where('i_user_id',$userid)->update($updateprofile);
			}
			$response['msg']=GeneralHelper::successMessage("Your holiday mode is set to on and your account is on pause status.");
		}else{
			$profiledata = SellerprofileModel::where('i_user_id',$userid)->get();
			if(count($profiledata)){
				$updateprofile['e_status']="active";
				SellerprofileModel::where('i_user_id',$userid)->update($updateprofile);
			}
			$response['active']=1;
			$response['msg']=GeneralHelper::successMessage("Your holiday mode is set to Off.");
		}
		
		$response['status']=1;
		echo json_encode($response);
		exit;
	
	}
	
	public function getSkill(){

		$data = Request::all();

		$response['status']=0;
		$response['msg']="Something Went Wrong";

		if(isset($data['i_category_id'])){
			$skilldata = SkillsModel::where("i_category_id",$data['i_category_id'])->where("e_status","active")->orderBy('v_name')->get();

			$optionstr="<option value=''>-select-</option>";
			if(count($skilldata)){
				foreach ($skilldata as $key => $value) {
					if(isset($value->id) && isset($value->v_name)){
						$optionstr .="<option value='".$value->id."'>".$value->v_name."</option>";
					}
				}
			}
			$response['status']=1;
			$response['optionstr']=$optionstr;
			$response['msg']="succesfully list skill list.";
		}
		echo json_encode($response);
		exit;
	}

	public function editProfile(){
	}


	public function sellerProfileIntoducyVideo($id=""){

		$data = Request::all();

		$userid = auth()->guard('web')->user()->_id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();

		if(!count($existProfile)){
			return 0;
		}

		if(Request::hasFile('file')) {
			$image = Request::file('file');
			if(isset($existProfile->introducy_video) && $existProfile->introducy_video!=""){
				if(Storage::disk('s3')->exists($existProfile->introducy_video)){
					Storage::disk('s3')->Delete($existProfile->introducy_video);
				}
			}
			$fileName = 'introducy_video-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$data['v_introducy_video'] = 'users/video/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/video/'.$fileName, File::get((string)$image), 'public');
			SellerprofileModel::find($existProfile->id)->update($data);
		}

		$videodata = \Storage::cloud()->url($data['v_introducy_video']);     
		$appendstr="";
		$appendstr.='<div class="dz-default dz-message"><span>Drop files here to upload</span></div>';
		$appendstr.='<div class="dropzone-previews" style="width: auto;display: inline-block;">';
		$appendstr.='<div class="dz-preview dz-processing dz-success dz-image-preview edit-img"  id="remove-row-'.$id.'">';
		$appendstr.='<div class="dz-details"> ';
		$appendstr.='<video width="100px" height="100px" controls>';
		$appendstr.='<source src="'.$videodata.'" type="video/mp4">';
		$appendstr.='</video> ';
		$appendstr.='</div>';
		$appendstr.='<a href="javascript:" onclick="removeIntroducy_video()" class="dz-remove">Remove video</a></div>';
		$appendstr.='</div>';
		return $appendstr;

	}

	public function RemoveSellerProfileIntoducyVideo(){

		$data = Request::all();
		
		$userid = auth()->guard('web')->user()->_id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();

		if(!count($existProfile)){
			return 0;
		}
		if(isset($existProfile->introducy_video) && $existProfile->introducy_video!=""){
			if(Storage::disk('s3')->exists($existProfile->introducy_video)){
				Storage::disk('s3')->Delete($existProfile->introducy_video);
			}
		}
		$update['v_introducy_video'] = '';
		SellerprofileModel::find($existProfile->id)->update($update);
		return 1;

	}

	public function sellerProfileWorkVideo($id=""){

		$data = Request::all();

		$userid = auth()->guard('web')->user()->_id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();
	
		if(!count($existProfile)){
			return 0;
		}
		$update['v_work_video']=array();
		if(isset($existProfile->v_work_video)){
			$update['v_work_video']=$existProfile->v_work_video;
		}

		if(Request::hasFile('file')) {
			$image = Request::file('file');
			$fileName = 'work_video-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$update['v_work_video'][] = 'users/video/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/video/'.$fileName, File::get((string)$image), 'public');
			SellerprofileModel::find($existProfile->id)->update($update);
		}

		$appendstr="";
		$appendstr.='<div class="dz-default dz-message"><span>Drop files here to upload</span></div>';
		foreach($update['v_work_video'] as $k=>$v){
			$videodata = \Storage::cloud()->url($v);     
			$appendstr.='<div class="dropzone-previews" style="width: auto;display: inline-block;" id="remvideo'.$k.'">';
			$appendstr.='<div class="dz-preview dz-processing dz-success dz-image-preview edit-videowork">';
			$appendstr.='<div class="dz-details"> ';
			$appendstr.='<video width="100px" height="100px" controls>';
			$appendstr.='<source src="'.$videodata.'" type="video/mp4">';
			$appendstr.='</video> ';
			$appendstr.='</div>';
			$appendstr.='<a href="javascript:" onclick="removeWork_video';
			$appendstr.="('".$v."','".$k."')";
			$appendstr.='"class="dz-remove">Remove video</a></div>';
			$appendstr.='</div>';
		}
		return $appendstr;

	}

	public function RemoveSellerProfileWorkVideo(){

		$data = Request::all();
		
		$userid = auth()->guard('web')->user()->_id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();
		if(!count($existProfile)){
			return 0;
		}


		if(isset($existProfile->v_work_video) && count($existProfile->v_work_video)){
			
			if(Storage::disk('s3')->exists($data['name'])){
				Storage::disk('s3')->Delete($data['name']);
			}
			$updatedata['v_work_video']=array();
			foreach ($existProfile->v_work_video as $key => $value) {
				
				if($data['name']!=$value){
					$updatedata['v_work_video'][]=$value;
				}

			}
			SellerprofileModel::find($existProfile->id)->update($updatedata);
		}
		return 1;

	}

	public function sellerProfileWorkPhotos($id=""){

		$data = Request::all();	
	
		$userid = auth()->guard('web')->user()->_id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();

		if(!count($existProfile)){
			return 0;
		}
		if(isset($existProfile->v_work_photos)){
			$update['v_work_photos']=$existProfile->v_work_photos;
		}

		if(Request::hasFile('file')) {
			
			$a=uniqid();
			$destination = public_path('uploads/userprofiles');
			$image = Request::file('file');

			$fileName = 'work_photo-'.$a.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$update['v_work_photos'][] = 'users/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$image), 'public');

			SellerprofileModel::find($existProfile->id)->update($update);
			$fileName='users/' . $fileName;
		}

		$appendstr="";
		$appendstr.='<div class="dz-default dz-message"><span>Drop files here to upload</span></div>';
		foreach($update['v_work_photos'] as $k=>$v){
			$imgdaata = \Storage::cloud()->url($v);
			$appendstr.='<div class="dropzone-previews" style="width: auto;display: inline-block;" id="rem'.$k.'">';
			$appendstr.='<div class="dz-preview dz-processing dz-success dz-image-preview">';
			$appendstr.='<div class="dz-details"> ';
			$appendstr.='<img src="'.$imgdaata.'" style="max-width:100px;">';
			$appendstr.='</div>';
			$appendstr.='<a href="javascript:" onclick="removeWork_workphotos';
			$appendstr.="('".$v."','".$k."')";
			$appendstr.='"class="dz-remove">Remove image</a></div>';
			$appendstr.='</div>';

		}
	    return $appendstr;

	}

	public function RemoveSellerProfileWorkPhotos(){

		$data = Request::all();
		
		$userid = auth()->guard('web')->user()->_id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();
		
		if(!count($existProfile)){
			return 0;
		}
		
		if(isset($existProfile->v_work_photos) && count($existProfile->v_work_photos)){
			
			if(Storage::disk('s3')->exists($data['name'])){
				Storage::disk('s3')->Delete($data['name']);
			}
			$updatedata['v_work_photos']=array();
			foreach ($existProfile->v_work_photos as $key => $value) {
				
				if($data['name']!=$value){
					$updatedata['v_work_photos'][]=$value;
				}

			}
			SellerprofileModel::find($existProfile->id)->update($updatedata);
		}
		return 1;
	}

	public function sellerProfileDoc($id=""){

		$data = Request::all();	
	
		$userid = auth()->guard('web')->user()->_id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();

		if(!count($existProfile)){
			return 0;
		}
		
		if(isset($existProfile->v_doc)){
			$update['v_doc']=$existProfile->v_doc;
		}

		if(Request::hasFile('file')) {
			
			$a=uniqid();
			$destination = public_path('uploads/userprofiles');
			$image = Request::file('file');	
		
			//$fileName = 'doc-'.$a.time().'.'.$image->getClientOriginalExtension();
			$fileName = $image->getClientOriginalName();
			//$fileName = 'doc-'.$a.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$update['v_doc'][] = 'users/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$image), 'public');
			SellerprofileModel::find($existProfile->id)->update($update);
			$fileName='users/' . $fileName;
		}

		$appendstr="";
		$appendstr.='<div class="dz-default dz-message"><span>Drop files here to upload</span></div>';
		foreach($update['v_doc'] as $k=>$v){
			
			$ext = pathinfo($v, PATHINFO_EXTENSION);
			if($ext=="pdf"){
				$imgdaata = url('public/Assets/frontend/images').'/pdf.png';         
			}else if($ext=="doc"){
				$imgdaata = url('public/Assets/frontend/images').'/doc.png';         
			}else if($ext=="docx"){
				$imgdaata = url('public/Assets/frontend/images').'/docx.jpeg';         
			}else if($ext=="csv"){
				$imgdaata = url('public/Assets/frontend/images').'/csv.png';         
			}else if($ext=="xls"){
				$imgdaata = url('public/Assets/frontend/images').'/xls.jpeg';         
			}else{
				$imgdaata = url('public/Assets/frontend/images').'/doc.png';
			}
																
			$appendstr.='<div class="dropzone-previews" style="width: auto;display: inline-block;" id="remd'.$k.'">';
			$appendstr.='<div class="dz-preview dz-processing dz-success dz-image-preview">';
			$appendstr.='<div class="dz-details"> ';
			$appendstr.='<img src="'.$imgdaata.'" style="max-width:100px;">';
			$appendstr.='</div>';
			$appendstr.='<a href="javascript:" onclick="removeWork_doc';
			$appendstr.="('".$v."','".$k."')";
			$appendstr.='"class="dz-remove">Remove image</a></div>';
			$appendstr.='</div>';
		}

	    return $appendstr;

	}

	public function RemovesellerProfileDoc(){

		$data = Request::all();
		
		$userid = auth()->guard('web')->user()->_id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();
		
		if(!count($existProfile)){
			return 0;
		}
		
		if(isset($existProfile->v_doc) && count($existProfile->v_doc)){
			
			if(Storage::disk('s3')->exists($data['name'])){
				Storage::disk('s3')->Delete($data['name']);
			}
			$updatedata['v_doc']=array();
			foreach ($existProfile->v_doc as $key => $value) {
				
				if($data['name']!=$value){
					$updatedata['v_doc'][]=$value;
				}

			}
			SellerprofileModel::find($existProfile->id)->update($updatedata);
		}
		return 1;
	}

	public function updateProfile(){

		$data = Request::all();
		
		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$userid = auth()->guard('web')->user()->_id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();
		$data['v_service'] = $v_service;


		if(isset($data['information']['v_avalibility_from'])){
			$data['v_avalibility_from'] = $data['information']['v_avalibility_from'];
			unset($data['information']['v_avalibility_from']);
		}

		if(isset($data['information']['v_avalibility_to'])){
			$data['v_avalibility_to'] = $data['information']['v_avalibility_to'];
			unset($data['information']['v_avalibility_to']);
		}

		if(isset($data['information']['v_avalibilitytime_from'])){
			$data['v_avalibilitytime_from'] = $data['information']['v_avalibilitytime_from'];
			unset($data['information']['v_avalibilitytime_from']);
		}

		if(isset($data['information']['v_avalibilitytime_to'])){
			$data['v_avalibilitytime_to'] = $data['information']['v_avalibilitytime_to'];
			unset($data['information']['v_avalibilitytime_to']);
		}

		if(isset($data['information']) && count($data['information'])){

			
			if(Request::hasFile('introducy_video')) {
				
				$image = Request::file('introducy_video');
				
				if(isset($existProfile->introducy_video) && $existProfile->introducy_video!=""){
					if(Storage::disk('s3')->exists($existProfile->introducy_video)){
						Storage::disk('s3')->Delete($existProfile->introducy_video);
					}
				}
				
				$fileName = 'introducy_video-'.time().'.'.$image->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_introducy_video'] = 'users/video/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/users/video/'.$fileName, File::get((string)$image), 'public');
				unset($data['information']['introducy_video']);
			}

			if(Request::hasFile('work_video')) {
				
				$image = Request::file('work_video');

				if(isset($existProfile->work_video) && $existProfile->work_video!=""){
					if(Storage::disk('s3')->exists($existProfile->work_video)){
						Storage::disk('s3')->Delete($existProfile->work_video);
					}
				}

				$fileName = 'work_video-'.time().'.'.$image->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_work_video'] = 'users/video/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/users/video/'.$fileName, File::get((string)$image), 'public');
				unset($data['information']['work_video']);
			}

			if(isset($existProfile->v_work_photos) && count($existProfile->v_work_photos)){
				$data['v_work_photos'] = $existProfile->v_work_photos;
			}

			if(Request::hasFile('work_photo')) {
			
				$destination = public_path('uploads/userprofiles');
				$image = Request::file('work_photo');
				$k=0;
				foreach($image as $file){
		          	$fileName = 'work_photo-'.$k.'-'.time().'.'.$file->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$data['v_work_photos'][] = 'users/' . $fileName;
					$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$file), 'public');
					$k=$k+1;
			    }	
		        unset($data['information']['work_photo']);
			}
			
			$data['e_status']="active";
		}

		if(isset($data['work_photo'])){
			unset($data['work_photo']);
		}
		if(isset($data['introducy_video'])){
			unset($data['introducy_video']);
		}
		if(isset($data['work_video'])){
			unset($data['work_video']);
		}

		if(isset($data['billing_detail']) && count($data['billing_detail'])){
			$billing_detail['billing_detail'] = $data['billing_detail'];
			unset($data['billing_detail']);	
			UsersModel::find($userid)->update($billing_detail);	
		}

		if(isset($data['information']['v_tags'])){
			$data['v_tags'] = $data['information']['v_tags'];
			unset($data['information']['v_tags']);	
		}

		if(isset($data['information']['basic_package']['v_price']) && $data['information']['basic_package']['v_price']!=""){
			$data['i_price'] = (float)$data['information']['basic_package']['v_price'];
		}

		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){
			$lat=(float)$data['v_latitude'];
			$lont=(float)$data['v_longitude'];
			$data['location']['type']="Point";
			$data['location']['coordinates']=array($lont,$lat);
		}

		if(count($existProfile)){
			$data['d_modified']=date("Y-m-d H:i:s");
			SellerprofileModel::find($existProfile->id)->update($data);	

		}else{
			
			if(isset($data['e_notification_subscribe']) && $data['e_notification_subscribe']=="on"){
				$data['e_notification_subscribe']="on";
			}else{
				$data['e_notification_subscribe']="off";
			}
			$data['i_user_id'] = $userid;
			$data['i_impression']=0;
			$data['i_contact']=0;
			$data['i_clicks']=0;
			$data['i_total_avg_review']=0;
			$data['i_total_review']=0;
			$data['d_added']=date("Y-m-d H:i:s");
			$data['d_modified']=date("Y-m-d H:i:s");
			$data['d_signup_date']=date("Y-m-d",strtotime(auth()->guard('web')->user()->d_added));	
			$id = SellerprofileModel::create($data)->id;	
			if(isset($data['e_notification_subscribe']) && $data['e_notification_subscribe']=="on"){
				GeneralHelper::JobNotifyEmailNotificationMultiple($id);
			}
			GeneralHelper::SkillNotifyEmailNotification($id);
		}

		$response['status']=1;
		$response['msg']="succesfully update information.";
		echo json_encode($response);
		exit;
	}

	public function updateUserPlan(){

		$data = Request::all();
	
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}
		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'seller/dashboard',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],			
			'v_signup'=>"yes",
		);

		if($data['v_plan']['id']!="5a65b48cd3e812a4253c9869"){

			$userdata = auth()->guard('web')->user();
			$upgrade = GeneralHelper::upgradeUserPlanDetail($userdata,$data['v_plan']['id']);
			if($upgrade){
				Session::put('ordersummary',$orderdata);
				return redirect('payment/card');
			}else{
				return redirect('seller/dashboard')->with(['success' => "You're already subscribed to this plan."]);
			}
		}

		$userid = auth()->guard('web')->user()->id;
		$userdata = auth()->guard('web')->user();

		if(!isset($userdata->e_basic_mail_send)){
			GeneralHelper::SendSkillboxSubscriptionEmail($userdata);
			$updateUser['e_basic_mail_send']="yes";
		}

		$updateUser['v_plan']['id']=$data['v_plan']['id'];
		$updateUser['v_plan']['duration']=$data['v_plan']['duration'];
		$updateUser['d_modified']=date("Y-m-d H:i:s");
		$updateUser['e_basic_mail_send']="yes";
		UsersModel::find($userid)->update($updateUser);

		//$planupdate = GeneralHelper::upgradeUserPlan($userid,$data);
		return redirect('seller/dashboard')->with(['success' => 'Profile successfully activated.']);

	}
	
	public function myProfile(){

		if(!auth()->guard('web')->check()) {
			return redirect('/')->with(['warning' => 'Something went wrong']);
		}
		$v_service = auth()->guard('web')->user()->seller['v_service'];

		$userid = auth()->guard('web')->user()->id;
		$sellerProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();
		
		$userdata = UsersModel::find($userid);
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");
		$userplandata = GeneralHelper::userPlanDetail($userdata);

		$leveldata=array();
		if(isset(auth()->guard('web')->user()->v_level) && auth()->guard('web')->user()->v_level!="0"){
            $leveldata = GeneralHelper::LevelData(auth()->guard('web')->user()->v_level);
        }
		
        $sponsoredSales=0;
		if(count($sellerProfile)){
			$courseorder = OrderModal::where('i_seller_profile_id',$sellerProfile->id)->where('e_transaction_type','skill')->where('v_sponser_sales','yes')->where('e_payment_status','success')->get();
			if(count($courseorder)){
				foreach ($courseorder as $key => $value) {
					$sponsoredSales = $sponsoredSales+$value->v_amount;			
				}
			}
		}
		
		$rdirect=1;	
		if(isset($sellerProfile->i_category_id) && $sellerProfile->i_category_id!=""){
			$rdirect=0;	
		}
		
		if($rdirect){
			return redirect('seller/edit-my-profile');
		}
	 
		$data=array(
			'sellerProfile'=>$sellerProfile,
			'v_service'=>$v_service,
			'userdata'=>$userdata,
			'plandata'=>$plandata,
			'discount'=>$discount,
			'userplandata'=>$userplandata,
			'leveldata'=>$leveldata,
			'sponsoredSales'=>$sponsoredSales,
		);
		return view('frontend/seller/my_profile',$data);
	}

	public function editMyProfile(){

		if(!auth()->guard('web')->check()) {
			return redirect('/')->with(['warning' => 'Something went wrong']);
		}
		$v_service = auth()->guard('web')->user()->seller['v_service'];

		$userid = auth()->guard('web')->user()->id;
		$sellerProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();

		$other_skill = $other_skill_data = array();

		if(isset($sellerProfile) && count($sellerProfile)){
			if(isset($sellerProfile->i_otherskill_id) && count($sellerProfile->i_otherskill_id)){
				foreach ($sellerProfile->i_otherskill_id as $key => $value) {
					$other_skill[] = $value;
				}	
			}
		}
	
		$skills = SkillsModel::whereIn("_id",$other_skill)->get();
		if(isset($skills) && count($skills)){
			foreach ($skills as $key => $value) {
				$other_skill_data[$value->_id] = $value->v_name;
			}	
		}
		
		$userdata = UsersModel::find($userid);
		$category = CategoriesModel::where("v_type",$v_service)->where("e_status","active")->orderBy('v_name')->get();
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");
		$skilldata = SkillsModel::where("e_status","active")->get();
		
		$planstep=0;
		if(isset($userdata->v_plan) && count($userdata->v_plan)){
			if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b48cd3e812a4253c9869" ){
				$planstep=1;
			}
		}

		$data=array(
			'category'=>$category,
			'sellerProfile'=>$sellerProfile,
			'v_service'=>$v_service,
			'userdata'=>$userdata,
			'plandata'=>$plandata,
			'discount'=>$discount,
			'skilldata'=>$skilldata,
			'planstep'=>$planstep,
			'other_skill_data'=>$other_skill_data
		);
		return view('frontend/seller/signup_step',$data);
	}

	public function sponsorProfile(){

		$data = Request::all();
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}

		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'seller/my-profile',
			'v_action'=>'sponser profile',
			'v_update'=>'sponser_profile',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],			
		);

		Session::put('ordersummary',$orderdata);
		return redirect('payment/card');

		//return redirect('payment');

		// $data['v_plan']['date'] = date("Y-m-d H;i:s");
		// $data['d_modified']=date("Y-m-d H;i:s");	
		// $data['e_sponsor_profile'] = "yes";
		// $data['e_sponsor_profile_status'] = "start";
		// UsersModel::find($user_id)->update($data);
		//return redirect('seller/my-profile');
	}
	
	public function updateSponsorProfileStatus(){

		$data = Request::all();

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		if(!isset($data['e_sponsor_profile_status'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$user_id = auth()->guard('web')->user()->id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];

		$sdata = SellerprofileModel::where('i_user_id',$user_id)->where('v_service',$v_service)->first();    
	    
	    if(count($sdata)){
	    	if($data['e_sponsor_profile_status'] == "start"){
	    		$update['e_sponsor'] = "yes";	
	    	}
	        $update['e_sponsor_status'] = $data['e_sponsor_profile_status'];
            SellerprofileModel::find($sdata->id)->update($update);
        }

        // $data['d_modified']=date("Y-m-d H;i:s");
		// UsersModel::find($user_id)->update($data);	

        $btnstr="";
        $msg="";
		if($data['e_sponsor_profile_status']=="start"){


			$btnstr.='<button type="button" class="btn btn-sponsor-add-first" id="myBtn" onclick = "updateSponsoredStatus';
			$btnstr.="('pause')";
			$btnstr.='")>Pause Sponsored Ad</button>';
			$btnstr.='<button class="btn btn-sponsor-add-green">Sponsored</button>';
			$msg="Your profile is successfully updated to sponsored ad.";

		}else{

			$btnstr.='<button type="button" class="btn btn-sponsor-add-first" id="myBtn" onclick = "updateSponsoredStatus';
			$btnstr.="('start')";
			$btnstr.='")>Start Sponsored Ad</button>';
			$msg="You've successfully paused the  sponsored ad.";

		}
		$response['status']=1;
		$response['btnstr']=$btnstr;
		$response['msg']=GeneralHelper::successMessage($msg);
		echo json_encode($response);
		exit;
	}

	public function removeImage(){

		$data = Request::all();
		$response = array();

		$userid = auth()->guard('web')->user()->_id;
		$jobid = "";

		if(!isset($data['i_seller_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['i_seller_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['v_photo_name'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['v_photo_name']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$jobdata = SellerprofileModel::find($data['i_seller_id']);
		
		if(!count($jobdata)){

			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}

		$update['v_work_photos']=array();

		if(Storage::disk('s3')->exists($data['v_photo_name'])){
			Storage::disk('s3')->Delete($data['v_photo_name']);
		}

		
		if(count($jobdata->v_work_photos)){
			foreach ($jobdata->v_work_photos as $key => $value) {
				if($value!=$data['v_photo_name']){
					$update['v_work_photos'][]=$value;					
				}
			}
		}
		
		$update['d_modified']=date("Y-m-d H:i:s");
		SellerprofileModel::where('_id',$data['i_seller_id'])->update($update);
		$response['status']=1;
		$response['msg']="succesfully remove image.";
		echo json_encode($response);
		exit;
	}

	public function removeVideo(){

		$data = Request::all();
		$response = array();

		$userid = auth()->guard('web')->user()->_id;
		$jobid = "";

		if(!isset($data['i_seller_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['i_seller_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['v_video_name'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['v_video_name']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$jobdata = SellerprofileModel::find($data['i_seller_id']);
		
		if(!count($jobdata)){

			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}

		$update['v_introducy_video'] = '';
		$update['d_modified']=date("Y-m-d H:i:s");

		if(Storage::disk('s3')->exists($data['v_video_name'])){
			Storage::disk('s3')->Delete($data['v_video_name']);
		}

		
		SellerprofileModel::where('_id',$data['i_seller_id'])->update($update);
		$response['status']=1;
		$response['msg']="succesfully remove video.";
		echo json_encode($response);
		exit;
	}

	public function removeWorkVideo(){

		$data = Request::all();
		$response = array();

		$userid = auth()->guard('web')->user()->_id;
		$jobid = "";

		if(!isset($data['i_seller_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['i_seller_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['v_video_name'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['v_video_name']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$jobdata = SellerprofileModel::find($data['i_seller_id']);
		
		if(!count($jobdata)){

			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}

		$update['v_work_video'] = '';
		$update['d_modified']=date("Y-m-d H:i:s");

		if(Storage::disk('s3')->exists($data['v_video_name'])){
			Storage::disk('s3')->Delete($data['v_video_name']);
		}

		SellerprofileModel::where('_id',$data['i_seller_id'])->update($update);
		$response['status']=1;
		$response['msg']="succesfully remove video.";
		echo json_encode($response);
		exit;
	}

	public function appliedJobsReview(){

		$data= Request::all();
	
		$response = array();

		if(!isset($data['i_job_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}
		$job = JobsModel::find($data['i_job_id']);

		if(!count($job)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		$userid = auth()->guard('web')->user()->_id;

		$totalavg =  $data['i_work_star']+$data['i_support_star']+$data['i_opportunities_star']+$data['i_work_again_star']/4;

		$data['i_avg_star'] = number_format($totalavg,1);	
		$data['i_user_id'] = $userid;
		$data['i_buyer_id'] = $job->i_user_id;
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		BuyerreviewModel::create($data);	

		$response['status']=1;
		$response['msg']=GeneralHelper::successMessage("Your review has been posted successfully.");
		echo json_encode($response);
		exit;

	}

}