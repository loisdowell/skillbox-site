<?php
namespace App\Http\Controllers\Frontend\Sponsorads;

use Request, Hash, Lang,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Skills as SkillsModel;



class Sponsorads extends Controller {

	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.dashboard');
	}	
	
	public function index(){
		
		if(!auth()->guard('web')->check()) {
			return redirect('/');
		}
		
		$user_id = auth()->guard('web')->user()->id;
		$userservice = auth()->guard('web')->user()->buyer['v_service'];
		$joblist = JobsModel::where('i_user_id',$user_id)->where('e_sponsor',"yes")->where('v_service',$userservice)->orderBy('d_modified','DESC')->get();

		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

		$userdata = auth()->guard('web')->user();
		$userplandata = GeneralHelper::userPlanDetail($userdata);		


		$data=array(
			'joblist'=>$joblist,
			'plandata'=>$plandata,
			'discount'=>$discount,
			'userplandata'=>$userplandata
		);
		return view('frontend/Sponsorads/sponsorads_list',$data);

	}

	
	public function addSponsorAds(){

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after some time.";
			echo json_encode($response);
			exit;
		}

		$userdata = auth()->guard('web')->user();

		$userplan = array();
		if(isset($userdata->v_plan) && count($userdata->v_plan)){
			$userplan = $userdata->v_plan;
		}
	

		if($userplan['id']=="5a65b48cd3e812a4253c9869"){
			$response['status']=2;
			$response['msg'] = "You need to upgrade your plan.";
			echo json_encode($response);
			exit;
		}
		
		$userservice = auth()->guard('web')->user()->buyer['v_service'];

		$user_id = auth()->guard('web')->user()->id;
		$joblist = JobsModel::where('i_user_id',$user_id)->where('v_service',$userservice)->where('e_sponsor',"no")->get();

		$response=array();
		$newads="";
		if(count($joblist)){

			foreach ($joblist as $key => $val) {

				$newads .='<tr>';
		        $newads .='<td>'.$val->v_job_title.'</td>';
		        $newads .='<td>'.date("d/m/Y",strtotime($val->d_added)).'</td>';
		        $newads .='<td>'.date("d/m/Y",strtotime($val->d_expiry_date)).'</td>';
		        $newads .='<td><button type="button" class="btn btn-jobpost" onclick="SponsorNowModal';
		        $newads .="('".$val->id."')";
		        $newads .='")>Select</button></td>';
		      	$newads .='</tr>';
		   	}

		}else{
				$newads .='<tr>';
		        $newads .='<td colspan="4">No job post found.</td>';
		    	$newads .='</tr>';
		}

		$response['status']=1;
		$response['htmlstr']=$newads;
		echo json_encode($response);
		exit;

	}


	public function sponsorAdsStatus(){
		
		$data = Request::all();

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['job_id'])) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$update=array(
			'e_sponsor_status'=>$data['status'],
			'd_modified'=>date("Y-m-d H:i:s"),
		);
		$a = JobsModel::find($data['job_id'])->update($update);

		// print_r($a);
		// exit;
		
		$response['msg'] = "";
		$btnstr="";
		if($data['status']=="start"){
			$btnstr.='<button type="button" class="btn btn-pause" onclick = "SponsoreStatusChange';
			$btnstr.="('".$data['job_id']."','pause')";
			$btnstr.='")>Pause</button>';
			$response['msg'] = GeneralHelper::successMessage("Your sponsored ad successfully started.");
		}else{
			$btnstr.='<button type="button" class="btn btn-start" onclick = "SponsoreStatusChange';
			$btnstr.="('".$data['job_id']."','start')";
			$btnstr.='")>Start</button>';
			$response['msg'] = GeneralHelper::successMessage("Your sponsored ad successfully paused.");
		}	
		
		Session::put('success', 'This is a message!'); 
		$response['status']=1;
		$response['btnstr'] = $btnstr;
		$response['a'] = $a;
		echo json_encode($response);
		exit;

	}


}