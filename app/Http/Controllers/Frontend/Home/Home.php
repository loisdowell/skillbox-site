<?php
namespace App\Http\Controllers\Frontend\Home;

use Request, Hash, Lang,Validator,Auth,Storage,Cart,PDF,Session,DB;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Sellerreview as SellerreviewModal;
use App\Models\CompanyEthos as CompanyEthosModal;
use App\Models\Users\Buyerreview as BuyerreviewModal;
use App\Models\Pricefilter as PricefilterModal;
use App\Models\ManageMetaField as ManageMetaFieldModel;
use App\Models\Users\Order as OrderModal;
use App\Models\Search as SearchModal;
use App\Models\Emailsend as Emailsend;
use App\Models\Sitesettings as SiteSettingsModel;
use App\Models\Guide as GuideModel;

//use MongoDB\BSON\ObjectId;
class Home extends Controller {

	protected $section;
	private $mangopay;

	public function __construct(\MangoPay\MangoPayApi $mangopay){
		$this->section = Lang::get('section.Home');
		$this->mangopay = $mangopay;
	}

	public function index() {

		$metaDetails = GeneralHelper::MetaFiledData('home-page','Home');
		$companyEthos = CompanyEthosModal::where('e_status', 'active')->orderBy('i_order')->get();

		$inperson = CategoriesModel::where("e_status","active")->where("v_type","inperson")->orderBy('v_name')->limit(6)->get();
		$online = CategoriesModel::where("e_status","active")->where("v_type","online")->orderBy('v_name')->limit(6)->get();
		$userdata = UsersModel::where('v_plan.id',"5a65b9f4d3e8124f123c986c")->where("e_status","active")->orderBy('i_total_avg_review',"DESC")->get();

		$uids=array();
		if(count($userdata)){
			foreach ($userdata as $key => $value) {
				$uids[]=$value->id;
			}
		}

		$featuredProfile=array();
		if(count($uids)){
			$featuredProfile = SellerprofileModel::whereIn('i_user_id',$uids)->orderBy('i_total_avg_review','DESC')->limit(10)->get();
		}

		$cid=array();
		foreach ($inperson as $key => $value) {
			$cid[] = $value->id;
		}
		foreach ($online as $key => $value) {
			$cid[] = $value->id;
		}

		$homepage = GeneralHelper::getHomepageImage();
		//dd($homepage);

		$sitedata = SiteSettingsModel::get();
		$signupdata['sell_image']=url('public/Assets/frontend/images/hend.png');
		$signupdata['buy_image']=url('public/Assets/frontend/images/hend.png');
		$signupdata['sell_text']="Create a FREE profile now to find your skills";
		$signupdata['buy_text'] = "Buy your perfect Skills and Courses";

		if(count($sitedata)){
			foreach ($sitedata as $key => $value) {

				if($value->v_key == "I_WANT_SELL_IMAGE" && $value->v_value!=""){
					$signupdata['sell_image'] = \Storage::cloud()->url($value->v_value);
				}

				if($value->v_key == "I_WANT_BUY_IMAGE" && $value->v_value!=""){
					$signupdata['buy_image'] = \Storage::cloud()->url($value->v_value);
				}

				if($value->v_key == "I_WANT_SELL_TEXT" && $value->v_value!=""){
					$signupdata['sell_text'] = $value->v_value;
				}

				if($value->v_key == "I_WANT_BUY_TEXT" && $value->v_value!=""){
					$signupdata['buy_text'] = $value->v_value;
				}

			}
		}
		$guidedata = GuideModel::get();
		$guidedatalist=array();
		if(count($guidedata)){
			foreach ($guidedata as $key => $value) {
				$guidedatalist[$value->v_name]=array(
					'title'=>$value->v_title,
					'desc'=>$value->l_description,
				);
			}
		}

		$moreCategoryButton = CategoriesModel::whereNotIn('_id', $cid)->where("e_status","active")->count();
		$_data=array(
			'inperson' => $inperson,
			'online'   => $online,
			'featuredProfile'   => $featuredProfile,
			'companyEthos'=> $companyEthos,
			'metaDetails'=> $metaDetails,
			'moreCategoryButton'=> $moreCategoryButton,
			'homepage'=> $homepage,
			'signupdata'=>$signupdata,
			'guidedatalist'=>$guidedatalist

		);
		return view('frontend/home/home' , $_data);

	}

	public function inpersonSuggest() {

		$data = Request::all();

		$query = SellerprofileModel::query()->where('v_service', 'inperson');
		if(isset($data['keyword']) && $data['keyword']!=""){

			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });

		}
		$query = $query->where('e_status',"active");
		$query = $query->where('i_delete','!=',"1");
		$skilldata = $query->get();

		$currentDate=date("Y-m-d");
		$query = JobsModel::query()->where('v_service', 'inperson');
		if(isset($data['keyword']) && $data['keyword']!=""){

			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('d_expiry_date','>',$currentDate);
		$jobsdata = $query->get();

		$response['status']=1;
		$response['searchstr']="";

		if(count($skilldata)){
			foreach ($skilldata as $key => $value) {
				if(isset($value->v_profile_title)){
					$response['searchstr'].='<option value="'.$value->v_profile_title.'">';
				}

			}
		}

		if(count($jobsdata)){
			foreach ($jobsdata as $key => $value) {
				$response['searchstr'].='<option value="'.$value->v_job_title.'">';
			}
		}
		echo json_encode($response);
		exit;
	}

	public function onlineSuggest() {

		$data = Request::all();
		$currentDate=date("Y-m-d");

		$query = SellerprofileModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){

			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });

		}
		$query = $query->where('e_status',"active");
		$query = $query->where('i_delete','!=',"1");
		$skilldata = $query->get();

		$query = JobsModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('d_expiry_date','>',$currentDate);
		$jobsdata = $query->get();

		$response['status']=1;
		$response['searchstr']="";

		if(count($skilldata)){
			foreach ($skilldata as $key => $value) {
				if(isset($value->v_profile_title)){
					$response['searchstr'].='<option value="'.$value->v_profile_title.'">';
				}

			}
		}

		if(count($jobsdata)){
			foreach ($jobsdata as $key => $value) {
				$response['searchstr'].='<option value="'.$value->v_job_title.'">';
			}
		}

		echo json_encode($response);
		exit;
	}

	public function courseSuggest() {

		$data = Request::all();

		$query = CoursesModel::query();
		if(isset($data['keyword']) && $data['keyword']!=""){

			$query = $query->where(function($query ) use($data){
	        $query->where('v_title','like','%'.$data['keyword'].'%')
	              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
	    	});
		}
		$query = $query->where('i_delete','!=',"1");
		$courses = $query->where('e_status',"published")->get();

		$response['status']=1;
		$response['searchstr']="";

		if(count($courses)){
			foreach ($courses as $key => $value) {
				if(isset($value->v_title)){
					$response['searchstr'].='<option value="'.$value->v_title.'">';
				}

			}
		}
		echo json_encode($response);
		exit;
	}

	public function moreCategory(){

		$data = Request::all();

		if(isset($data['ids']) && $data['ids']!=""){
			$data['ids'] = explode(',', $data['ids']);
		}

		$inperson = CategoriesModel::whereNotIn('_id', $data['ids'])->where("e_status","active")->where("v_type","inperson")->orderBy('v_name')->limit(6)->get();

		$online = CategoriesModel::whereNotIn('_id', $data['ids'])->where("e_status","active")->where("v_type","online")->orderBy('v_name')->limit(6)->get();

		$inpersonstr="";
		$onlinestr = "";

		if(count($inperson)){
			foreach ($inperson as $key => $val) {

				$urlcat = url('courses-search')."?e_type=inperson&keyword=&i_category_id=".$val->id."&v_type_skill=on";
				$data['ids'][] = $val->id;
				$imgdata="";
				if(Storage::disk('s3')->exists($val->v_image)){
	                $imgdata = \Storage::cloud()->url($val->v_image);
	            }else{
	                $imgdata = \Storage::cloud()->url('common/no-image.png');
	            }
	            $inpersonstr.='<div class="col-sm-6">';
				$inpersonstr.='<div class="home-skill">';
				$inpersonstr.='<a href="'.$urlcat.'">';
				$inpersonstr.='<div class="skills-img">';
				$inpersonstr.='<img src="'.$imgdata.'" class="img-responsive" alt="" />';
				$inpersonstr.='<p>'.$val->v_name.'</p>';
				$inpersonstr.='</div>';
				$inpersonstr.='</a>';
				$inpersonstr.='</div>';
				$inpersonstr.='</div>';
			}
		}


		if(count($online)){
			foreach ($online as $key => $val) {

				$urlcat = url('courses-search')."?e_type=online&keyword=&i_category_id=".$val->id."&v_type_skill=on";
				$data['ids'][]=	$val->id;
				$imgdata="";
				if(Storage::disk('s3')->exists($val->v_image)){
	                $imgdata = \Storage::cloud()->url($val->v_image);
	            }else{
	                $imgdata = \Storage::cloud()->url('common/no-image.png');
	            }
	            $onlinestr.='<div class="col-sm-6">';
				$onlinestr.='<div class="home-skill">';
				$onlinestr.='<a href="'.$urlcat.'">';
				$onlinestr.='<div class="skills-img">';
				$onlinestr.='<img src="'.$imgdata.'" class="img-responsive" alt="" />';
				$onlinestr.='<p>'.$val->v_name.'</p>';
				$onlinestr.='</div>';
				$onlinestr.='</a>';
				$onlinestr.='</div>';
				$onlinestr.='</div>';
			}
		}

		$btnstatus = CategoriesModel::whereNotIn('_id', $data['ids'])->where("e_status","active")->count();
		$data['btnstatus']=0;
		if($btnstatus>0){
			$data['btnstatus']=1;
		}
		$data['onlinestr']=$onlinestr;
		$data['inpersonstr']=$inpersonstr;

		echo json_encode($data);
		exit;



		//$inperson = CategoriesModel::where("e_status","active")->where("v_type","inperson")->limit(6)->get();
	}

	public function buyNowCourses($courseid=""){

		if($courseid==""){
			return redirect("/");
		}

		$coursesdata = CoursesModel::where("_id",$courseid)->where("e_status","published")->first();

		if(!count($coursesdata)){
			return redirect("course/".$courseid);
		}
		//$userid = auth()->guard('web')->user()->_id;
		Cart::destroy();
		$cartparams = array(
			'id'   => $courseid,
			'name' => $coursesdata->v_title,
			'qty'  => 1,
			'price'=> $coursesdata->f_price,
			'options' 	=> array(
				'i_seller_id' 		=> $coursesdata->i_user_id,
				//'i_buyer_id' 		=> $userid,
				'v_instructor_image'=> $coursesdata->v_course_thumbnail,
			)
		);
		Cart::add($cartparams)->associate('App\Models\Courses\Courses');
		if(!auth()->guard('web')->check()) {
			$rcart = array(
				'rurl'=>url('cart/course'),
				'v_service'=>"online",
			);
			Session::put('rcart',$rcart);
			// $rurl=url('cart/course');
			// Session::put('redirecturl',$rurl);
			return redirect("login");
		}
		return redirect("cart/course");
	}
	public function buyNowCoursesApp(){

		// if($courseid==""){
		// 	return redirect("/");
		// }

		// CORS --
		// header('Access-Control-Allow-Origin: *');
		// header('Content-Type: application/json');

		$data = Request::all();

		// print_r($data);
		// exit;

		if(isset($data['is_check']) && $data['is_check'] == "true" ){

			if(!isset($data['token'])){

				$responseData = array(
					'code' => 404,
					'message' =>"Invalid Token Please logout from device and login again",
					'data' => array(),
				);
				return response()->json($responseData, 404);
			}

			$uData  =  UsersModel::where("v_auth_token",$data['token'])->first();
			if(!count($uData)){

				$responseData = array(
					'code' => 404,
					'message' =>"Invalid Token Please logout from device and login again",
					'data' => array(),
				);
				return response()->json($responseData, 404);
			}

			if(!isset($data['i_course_id'])){
				$responseData = array(
					'code' => 400,
					'message' =>"Something went wrong.Please try again latter.",
					'data' => array(),
				);
				return response()->json($responseData, 400);
			}

			$courseid=$data['i_course_id'];
			$coursesdata = CoursesModel::where("_id",$courseid)->where("e_status","published")->first();
			if(!count($coursesdata)){
				$responseData = array(
					'code' => 400,
					'message' =>"Something went wrong.Please try again latter.",
					'data' => array(),
				);
				return response()->json($responseData, 400);

			}

			$responseData = array(
				'code' => 200,
				'message' =>"Success.",
				'data' => array(),
			);
			return response()->json($responseData, 200);

		}

		if($data['i_course_id']==""){
			return redirect("/");
		}

		$qty=1;
		if(isset($data['qty'])){
			$qty=$data['qty'];
		}
		$courseid=$data['i_course_id'];

		$uData  =  UsersModel::where("v_auth_token",$data['token'])->first();
		if(!count($uData)){
			return redirect("/");
		}

		Auth::guard('web')->login($uData, true);
		$userdata = auth()->guard('web')->user();

		$coursesdata = CoursesModel::where("_id",$courseid)->where("e_status","published")->first();

		if(!count($coursesdata)){
			return redirect("course/".$courseid);
		}
		//$userid = auth()->guard('web')->user()->_id;
		Cart::destroy();
		$cartparams = array(
			'id'   => $courseid,
			'name' => $coursesdata->v_title,
			'qty'  => $qty,
			'price'=> $coursesdata->f_price,
			'options' 	=> array(
				'i_seller_id' 		=> $coursesdata->i_user_id,
				//'i_buyer_id' 		=> $userid,
				'v_instructor_image'=> $coursesdata->v_course_thumbnail,
			)
		);
		Cart::add($cartparams)->associate('App\Models\Courses\Courses');

		if(!auth()->guard('web')->check()) {
			$rcart = array(
				'rurl'=>url('cart/course'),
				'v_service'=>"online",
			);
			Session::put('rcart',$rcart);
			// $rurl=url('cart/course');
			// Session::put('redirecturl',$rurl);
			return redirect("login");
		}
		return redirect("cart/course/mangopay");
		// return redirect("cart/course");

	}

	public function addNowCourses($courseid=""){

		if($courseid==""){
			return redirect("/");
		}

		$coursesdata = CoursesModel::where("_id",$courseid)->where("e_status","published")->first();

		if(!count($coursesdata)){
			return redirect("course/".$courseid);
		}
		Cart::destroy();
		$cartparams = array(
			'id'   => $courseid,
			'name' => $coursesdata->v_title,
			'qty'  => 1,
			'price'=> $coursesdata->f_price,
			'options' 	=> array(
				'i_seller_id' 		=> $coursesdata->i_user_id,
				'v_instructor_image'=> $coursesdata->v_course_thumbnail,
			)
		);
		Cart::add($cartparams)->associate('App\Models\Courses\Courses');
		if(!auth()->guard('web')->check()) {
			$rcart = array(
				'rurl'=>url('cart/course'),
				'v_service'=>"online",
			);
			Session::put('rcart',$rcart);
			return redirect("login");
		}
		return redirect("course/".$coursesdata->id.'/'.$coursesdata->v_title)->with(['success' => "Course successfully added to cart."]);

	}

	public function postBuyNowSkill(){

		$data = Request::all();
		// print_r($data);
		// exit;

		if($data['i_seller_profile_id']==""){
			return redirect("/");
		}

		if($data['v_seller_plan']==""){
			return redirect("/");
		}
		$package = $data['v_seller_plan'];

		$sellerprofile = SellerprofileModel::find($data['i_seller_profile_id']);

		if(!count($sellerprofile)){
			return redirect("/");
		}

		//$userid = auth()->guard('web')->user()->_id;
		if(!isset($sellerprofile->information[$package]['v_price'])){
			return redirect("/");
		}

		$v_image="";
		if(isset($sellerprofile->v_work_photos[0]) && $sellerprofile->v_work_photos[0]!=""){
			$v_image=Storage::cloud()->url($sellerprofile->v_work_photos[0]);
		}else{

			if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_image) && $sellerprofile->hasUser()->v_image!=""){
                $v_image=Storage::cloud()->url($sellerprofile->hasUser()->v_image);
            }else{
            	$v_image = Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
            }
		}

		$addon=array();
		if(isset($data['planaddon']) && $data['planaddon']!=""){

			if(isset($sellerprofile->information[$package]['add_on']['title'][$data['planaddon']])){
				$addon['title'] = $sellerprofile->information[$package]['add_on']['title'][$data['planaddon']];
			}
			if(isset($sellerprofile->information[$package]['add_on']['v_price'][$data['planaddon']])){
				$addon['price'] = trim($sellerprofile->information[$package]['add_on']['v_price'][$data['planaddon']]);
			}
		}

		Cart::destroy();

		$cartparams = array(
			'id'   => $sellerprofile->id,
			'name' => $sellerprofile->v_profile_title,
			'qty'  => 1,
			'price'=> $sellerprofile->information[$package]['v_price'],
			'options' => array(
				'i_seller_id' 		 => $sellerprofile->i_user_id,
				'i_seller_profile_id'=> $sellerprofile->id,
				'v_image' 			 => $v_image,
				'type' 				 => "skill",
				'typedata' 			 => "skill",
				'package' 			 => $package,
			)
		);
		Cart::add($cartparams);
		if(count($addon)){

				$cartparams = array(
					'id'   => 1,
					'name' => $addon['title'],
					'qty'  => 1,
					'price'=> $addon['price'],
					'options' => array(
						'i_seller_id' 		 => $sellerprofile->i_user_id,
						'i_seller_profile_id'=> $sellerprofile->id,
						'v_addon' 		 	 => $addon,
						'v_image' 			 => $v_image,
						'type' 				 => "skill",
						'typedata' 			 => "addon",
						'package' 			 => $package,
					)
				);
				Cart::add($cartparams);

		}

		if(!auth()->guard('web')->check()) {
			$rcart=array(
				'rurl'=>url('cart/skill'),
				'v_service'=>$sellerprofile->v_service,
			);
			Session::put('rcart',$rcart);
			return redirect("login");
		}

		return redirect("cart/skill");
	}

	public function postBuyNowSkillApp(){

		$data = Request::all();

		if(isset($data['is_check']) && $data['is_check'] == "true"){

			if(!isset($data['token'])){

				$responseData = array(
					'code' => 404,
					'message' =>"Invalid Token Please logout from device and login again",
					'data' => array(),
				);
				return response()->json($responseData, 404);
			}

			$uData  =  UsersModel::where("v_auth_token",$data['token'])->first();
			if(!count($uData)){

				$responseData = array(
					'code' => 404,
					'message' =>"Invalid Token Please logout from device and login again",
					'data' => array(),
				);
				return response()->json($responseData, 404);
			}

			if(!isset($data['v_seller_plan'])){
				$responseData = array(
					'code' => 400,
					'message' =>"Something went wrong.Please try again latter.",
					'data' => array(),
				);
				return response()->json($responseData, 400);
			}

			if(!isset($data['i_seller_profile_id'])){
				$responseData = array(
					'code' => 400,
					'message' =>"Something went wrong.Please try again latter.",
					'data' => array(),
				);
				return response()->json($responseData, 400);
			}

			$sellerprofile = SellerprofileModel::find($data['i_seller_profile_id']);
			if(!count($sellerprofile)){
				$responseData = array(
					'code' => 400,
					'message' =>"Something went wrong.Please try again latter.",
					'data' => array(),
				);
				return response()->json($responseData, 400);
			}

			$responseData = array(
				'code' => 200,
				'message' =>"Success.",
				'data' => array(),
			);
			return response()->json($responseData, 200);

		}

		$uData  =  UsersModel::where("v_auth_token",$data['token'])->first();
		Auth::guard('web')->login($uData, true);
		$userdata = auth()->guard('web')->user();


		if($data['i_seller_profile_id']==""){
			return redirect("/");
		}

		$qty=1;
		if(isset($data['qty'])){
			$qty=$data['qty'];
		}

		if($data['v_seller_plan']==""){
			return redirect("/");
		}
		$package = $data['v_seller_plan'];

		$sellerprofile = SellerprofileModel::find($data['i_seller_profile_id']);

		if(!count($sellerprofile)){
			return redirect("/");
		}

		if(!isset($sellerprofile->information[$package]['v_price'])){
			return redirect("/");
		}

		$v_image="";
		if(isset($sellerprofile->v_work_photos[0]) && $sellerprofile->v_work_photos[0]!=""){
			$v_image=Storage::cloud()->url($sellerprofile->v_work_photos[0]);
		}else{
			if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_image) && $sellerprofile->hasUser()->v_image!=""){
                $v_image=Storage::cloud()->url($sellerprofile->hasUser()->v_image);
            }else{
            	$v_image = Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
            }
		}

		$addon=array();
		if(isset($data['planaddon']) && $data['planaddon']!=""){

			if(isset($sellerprofile->information[$package]['add_on']['title'][$data['planaddon']])){
				$addon['title'] = $sellerprofile->information[$package]['add_on']['title'][$data['planaddon']];
			}
			if(isset($sellerprofile->information[$package]['add_on']['v_price'][$data['planaddon']])){
				$addon['price'] = trim($sellerprofile->information[$package]['add_on']['v_price'][$data['planaddon']]);
			}
		}
		Cart::destroy();


		$cartparams = array(
			'id'   => $sellerprofile->id,
			'name' => $sellerprofile->v_profile_title,
			'qty'  => $qty,
			'price'=> $sellerprofile->information[$package]['v_price'],
			'options' => array(
				'i_seller_id' 		 => $sellerprofile->i_user_id,
				'i_seller_profile_id'=> $sellerprofile->id,
				'v_image' 			 => $v_image,
				'type' 				 => "skill",
				'typedata' 			 => "skill",
				'package' 			 => $package,
			)
		);
		Cart::add($cartparams);
		if(count($addon)){

				$cartparams = array(
					'id'   => 1,
					'name' => $addon['title'],
					'qty'  => 1,
					'price'=> $addon['price'],
					'options' => array(
						'i_seller_id' 		 => $sellerprofile->i_user_id,
						'i_seller_profile_id'=> $sellerprofile->id,
						'v_addon' 		 	 => $addon,
						'v_image' 			 => $v_image,
						'type' 				 => "skill",
						'typedata' 			 => "addon",
						'package' 			 => $package,
					)
				);
				Cart::add($cartparams);

		}



		if(!auth()->guard('web')->check()) {
			$rcart=array(
				'rurl'=>url('cart/skill'),
				'v_service'=>$sellerprofile->v_service,
			);
			Session::put('rcart',$rcart);
			return redirect("login");
		}
		// dd($userdata);
		return redirect("cart/skill/mangopay");

	}

	public function buyNowSkill($id="",$package=""){

		if($id==""){
			return redirect("/");
		}

		if($package==""){
			return redirect("/");
		}

		if(!auth()->guard('web')->check()) {
			return redirect("login");
		}

		$sellerprofile = SellerprofileModel::find($id);

		if(!count($sellerprofile)){
			return redirect("/");
		}
		$userid = auth()->guard('web')->user()->_id;

		if(!isset($sellerprofile->information[$package]['v_price'])){
			return redirect("/");
		}

		$v_image="";
		if(isset($sellerprofile->v_work_photos[0]) && $sellerprofile->v_work_photos[0]!=""){
			$v_image=Storage::cloud()->url($sellerprofile->v_work_photos[0]);
		}else{

			if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_image) && $sellerprofile->hasUser()->v_image!=""){
                $v_image=Storage::cloud()->url($sellerprofile->hasUser()->v_image);
            }else{
            	$v_image = Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
            }
		}

		Cart::destroy();
		$cartparams = array(
			'id'   => $sellerprofile->id,
			'name' => $sellerprofile->v_profile_title,
			'qty'  => 1,
			'price'=> $sellerprofile->information[$package]['v_price'],
			'options' => array(
				'i_seller_id' 		 => $sellerprofile->i_user_id,
				'i_seller_profile_id'=> $sellerprofile->id,
				'i_buyer_id' 		 => $userid,
				'v_image' 			 => $v_image,
				'type' 				 => "skill",
				'package' 			 => $package,
			)
		);

		Cart::add($cartparams);
		return redirect("cart/skill");
	}

	public function LoadMoreCourseSearch(){

		$data = Request::all();
		$keyword  = isset($data['keyword']) ? $data['keyword'] : '';

		$cids=array();
		if(isset($data['cidsdata']) && $data['cidsdata']!=""){
			$cids = explode(",", $data['cidsdata']);
		}

		$pricefilter = PricefilterModal::where("e_status","active")->orderBy("i_order")->get();
		$s_course = array();
		$userid = 0;

		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;
		}

		if(auth()->guard('web')->check()) {
			$shortlistedCourse = ShortlistedCourseModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedCourse as $key => $value) {
				$s_course[] = $value->i_shortlist_id;
			}
		}

		$findResult=array();
		$ispricefilter = 0;

		$courses = GeneralHelper::CourseSearchBoldLoad($data,$pricefilter,$cids);
		if(count($courses)){
			foreach ($courses as $key => $value) {
				if(isset($value->e_sponsor_status) && $value->e_sponsor_status=="start"){
					$update=array();
					if(isset($value['i_impression'])){
						$update['i_impression']=$value['i_impression']+1;
					}else{
						$update['i_impression']=1;
					}
					if(isset($value['i_impression_perday'])){
						$update['i_impression_perday']=$value['i_impression_perday']+1;
					}else{
						$update['i_impression_perday']=1;
					}
					if(isset($value['i_impression_day'])){
						$update['i_impression_day']=$value['i_impression_day']+1;
					}else{
						$update['i_impression_day']=1;
					}
					CoursesModel::find($value['_id'])->update($update);
					if(isset($value->f_price) && $value->f_price>50){
		                $ispricefilter = 1;
		            }
		        }
				$cids[]	=$value->id;
			}
			$findResult = $courses;
		}

		$courses = GeneralHelper::CourseSearchNormal($data,$pricefilter,$cids);
		if(count($courses)){
			foreach ($courses as $key => $value) {
				if(isset($value->e_sponsor_status) && $value->e_sponsor_status=="start"){

					$update=array();
					if(isset($value['i_impression'])){
						$update['i_impression']=$value['i_impression']+1;
					}else{
						$update['i_impression']=1;
					}
					if(isset($value['i_impression_perday'])){
						$update['i_impression_perday']=$value['i_impression_perday']+1;
					}else{
						$update['i_impression_perday']=1;
					}
					CoursesModel::find($value['_id'])->update($update);
					if(isset($value->f_price) && $value->f_price>50){
		                $ispricefilter = 1;
		            }
		        }else{
		        	$update=array();
					if(isset($value['i_impression_day'])){
						$update['i_impression_day']=$value['i_impression_day']+1;
					}else{
						$update['i_impression_day']=1;
					}
					CoursesModel::find($value['_id'])->update($update);
			    }
				$cids[]	=$value->id;
			}
			if(count($findResult)){
				$findResult = $findResult->merge($courses);
			}else{
				$findResult = $courses;
			}
		}

		$_data=array(
			'type'   => "grid",
			'courses'   => $findResult,
			's_course'  => $s_course,
		);

		$messagestr = view('frontend/home/morecourse', $_data);
		$responsestr = $messagestr->render();
		$response['gridviewtext']=$responsestr;

		$_data=array(
			'type'   => "list",
			'courses'   => $findResult,
			's_course'  => $s_course,
		);

		$messagestr = view('frontend/home/morecourse', $_data);
		$responsestr = $messagestr->render();
		$response['status']=1;
		$response['listviewtext']=$responsestr;
		$response['ispricefilter']=$ispricefilter;

		$response['cids']="";
		if(count($cids)){
			$response['cids']=implode(",", $cids);
		}
		echo json_encode($response);
		exit;

	}

	public function LoadMoreSkillandJobSearch(){

		$data = Request::all();
		$keyword  = isset($data['keyword']) ? $data['keyword'] : '';

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;
		}

		if(isset($data['e_type']) && $data['e_type']=="online"){

			$pricefilter = PricefilterModal::where("e_status","active")->orderBy("i_order")->get();

			$user_ids = $skill_users = $job_users = $s_skill = $s_job = array();
			$finalresult=array();
			$existskillids=array();
			$existjobids=array();

			if(isset($data['skillidsdata']) && $data['skillidsdata']!=""){
				$existskillids = explode(",", $data['skillidsdata']);
			}
			if(isset($data['jobidsdata']) && $data['jobidsdata']!=""){
				$existjobids = explode(",", $data['jobidsdata']);
			}

			$totaljob = 0;
			$totalskill = 0;

			// if(count($data)<=7){
			// 	$data['v_type_skill'] = "on";
			// 	$data['v_type_job'] = "on";
			// }

			$ispricefilter=0;

			//Bold Adds Skill
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){

				$skilldata = GeneralHelper::SkillSearchBoldLoad($data,$pricefilter,$existskillids);

				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			//i_impression_perday
			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}
						if(isset($value['i_price']) && $value['i_price']>50){
			                $ispricefilter = 1;
			            }

			  		}
			  	}
			}

		  	if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::JobSearchBoldLoad($data,$pricefilter,$existjobids);

		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							JobsModel::find($value['_id'])->update($update);
			  			}
			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;

			  			if(isset($value['v_budget_amt']) && $value['v_budget_amt']>50){
			                $ispricefilter = 1;
			            }

			  		}
			  	}
			}

			//Normal Listing
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){

				$skilldata = GeneralHelper::SkillSearchNoraml($data,$existskillids,$pricefilter);

				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

						if(isset($value['i_price']) && $value['i_price']>50){
			                $ispricefilter = 1;
			            }

			  		}
			  	}
			}

			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::JobSearchNormal($data,$pricefilter,$existjobids);

		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
				  			JobsModel::find($value['_id'])->update($update);
			  			}
			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;

			  			if(isset($value['v_budget_amt']) && $value['v_budget_amt']>50){
			                $ispricefilter = 1;
			            }

			  		}
			  	}
			}

		 	$shortlistedSkill = ShortlistedSkillsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedSkill as $key => $value) {
				$s_skill[] = $value->i_shortlist_id;
			}

			$shortlistedJobs = ShortlistedJobsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedJobs as $key => $value) {
				$s_job[] = $value->i_shortlist_id;
			}

			$skills=array();
			if(isset($data['i_category_id']) && $data['i_category_id']!=""){
				$skills = SkillsModel::where('i_category_id',$data['i_category_id'])->get();
			}

			$_data=array(
				'finalresult'=> $finalresult,
				'type'   => "grid",
				's_skill' => $s_skill,
				's_job' => $s_job,
				'v_service'=>"online",
			);

			$messagestr = view('frontend/home/skilljobsearchmore', $_data);
			$responsestr = $messagestr->render();
			$response['gridviewtext']=$responsestr;

			$_data=array(
				'finalresult'=> $finalresult,
				's_skill' => $s_skill,
				's_job' => $s_job,
				'v_service'=>"online",
				'type'   => "list",
			);

			$messagestr = view('frontend/home/skilljobsearchmore', $_data);
			$responsestr = $messagestr->render();

			$response['status']=1;
			$response['listviewtext']=$responsestr;
			$response['ispricefilter']=$ispricefilter;

			$response['existskillids']="";
			$response['existjobids']="";

			if(count($existskillids)){
				$response['existskillids']=implode(",", $existskillids);
			}
			if(count($existjobids)){
				$response['existjobids']=implode(",", $existjobids);
			}
			echo json_encode($response);
			exit;

	    }
		else if(isset($data['e_type']) && $data['e_type']=="inperson"){

	    	$pricefilter = PricefilterModal::where("e_status","active")->orderBy("i_order")->get();
			$milesfilter = array(
				'5'=>"5",
				'10'=>"10",
				'15'=>"15",
				'25'=>"25",
				'50'=>"50",
			);

			$user_ids = $skill_users = $job_users = $s_skill = $s_job = array();
			$finalresult=array();
			$existskillids=array();
			$existjobids=array();

			if(isset($data['skillidsdata']) && $data['skillidsdata']!=""){
				$existskillids = explode(",", $data['skillidsdata']);
			}
			if(isset($data['jobidsdata']) && $data['jobidsdata']!=""){
				$existjobids = explode(",", $data['jobidsdata']);
			}

			$totaljob = 0;
			$totalskill = 0;
			// if(count($data)<=7){
			// 	$data['v_type_skill'] = "on";
			// 	$data['v_type_job'] = "on";
			// }
			$ispricefilter=0;

			$totaljob = 0;
			$totalskill = 0;

			//Normal Listing
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){

				$skilldata = GeneralHelper::InpersonSkillSearchNoraml($data,$existskillids,$pricefilter);

				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

			  		}
			  	}
			}

			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::InpersonJobSearchNormal($data,$pricefilter,$existjobids);

		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
				  			JobsModel::find($value['_id'])->update($update);
			  			}
			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

			//Bold Adds Skill
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
				$skilldata = GeneralHelper::InpersonSkillSearchBold($data,$pricefilter,$existskillids);
				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

			  		}
			  	}
			}

		  	if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::InpersonJobSearchBold($data,$pricefilter,$existjobids);
		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
				  			JobsModel::find($value['_id'])->update($update);
			  			}
			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}


		  	$shortlistedSkill = ShortlistedSkillsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedSkill as $key => $value) {
				$s_skill[] = $value->i_shortlist_id;
			}

			$shortlistedJobs = ShortlistedJobsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedJobs as $key => $value) {
				$s_job[] = $value->i_shortlist_id;
			}


			$skills=array();
			if(isset($data['i_category_id']) && $data['i_category_id']!=""){
				$skills = SkillsModel::where('i_category_id',$data['i_category_id'])->get();
			}


			$_data=array(
				'finalresult'=> $finalresult,
				'type'   => "grid",
				's_skill' => $s_skill,
				's_job' => $s_job,
				'v_service'=>"inperson",
			);

			$messagestr = view('frontend/home/skilljobsearchmore', $_data);
			$responsestr = $messagestr->render();
			$response['gridviewtext']=$responsestr;

			$_data=array(
				'finalresult'=> $finalresult,
				's_skill' => $s_skill,
				's_job' => $s_job,
				'v_service'=>"inperson",
				'type'   => "list",
			);

			$messagestr = view('frontend/home/skilljobsearchmore', $_data);
			$responsestr = $messagestr->render();

			$response['status']=1;
			$response['listviewtext']=$responsestr;
			$response['ispricefilter']=$ispricefilter;

			$response['existskillids']="";
			$response['existjobids']="";

			if(count($existskillids)){
				$response['existskillids']=implode(",", $existskillids);
			}
			if(count($existjobids)){
				$response['existjobids']=implode(",", $existjobids);
			}
			echo json_encode($response);
			exit;

	  	}

	}

	public function Search(){

		$data = Request::all();

		$keyword  = isset($data['keyword']) ? $data['keyword'] : '';
		$userid = 0;

		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;
		}

		Session::put('e_type', $data['e_type']);

		if(isset($data['e_type']) && $data['e_type']!="" && isset($data['keyword']) && $data['keyword']!=""){
			$ip = $_SERVER['REMOTE_ADDR'];
			$insert=array(
				'v_type'=>$data['e_type'],
				'v_text'=>$data['keyword'],
				'v_ipaddress'=>$ip,
				'd_added'=>date("Y-m-d H:i:s"),

			);
			SearchModal::create($insert);
		}

		if(isset($data['e_type']) && $data['e_type']=="courses"){

			$s_course = array();
			$coursesLanguage = CoursesLanguageModel::where("e_status","active")->get();
			$coursesCategory = CoursesCategoryModel::where("e_status","active")->where("i_delete","!=",'1')->orderBy('v_title')->get();
			$coursesLevel = CoursesLevelModel::where("e_status","active")->get();
			$pricefilter = PricefilterModal::where("e_status","active")->orderBy("i_order")->get();

			if(auth()->guard('web')->check()) {
				$shortlistedCourse = ShortlistedCourseModel::where('i_user_id',$userid)->get();
				foreach ($shortlistedCourse as $key => $value) {
					$s_course[] = $value->i_shortlist_id;
				}
			}
			$findResult=array();

			$coursessidebar=array();
			$coursessidebar1=array();

			$coursessidebar = GeneralHelper::CourseSearchNormalSidebar($data,$pricefilter);
			$coursessidebar1 = GeneralHelper::CourseSearchNormalSidebar1($data,$pricefilter);

			$cids=array();
			$courses = GeneralHelper::CourseSearchBold($data,$pricefilter,$cids);

			if(count($courses)){
				foreach ($courses as $key => $value) {
					if(isset($value->e_sponsor_status) && $value->e_sponsor_status=="start"){

						$update=array();
						if(isset($value['i_impression'])){
							$update['i_impression']=$value['i_impression']+1;
						}else{
							$update['i_impression']=1;
						}

						if(isset($value['i_impression_perday'])){
							$update['i_impression_perday']=$value['i_impression_perday']+1;
						}else{
							$update['i_impression_perday']=1;
						}

						if(isset($value['i_impression_day'])){
							$update['i_impression_day']=$value['i_impression_day']+1;
						}else{
							$update['i_impression_day']=1;
						}
						CoursesModel::find($value['_id'])->update($update);
					}
					$cids[]	=$value->id;
				}
				$findResult = $courses;
			}

			$courses = GeneralHelper::CourseSearchNew($data,$pricefilter,$cids);
			if(count($courses)){
				foreach ($courses as $key => $value) {
					if(isset($value->e_sponsor_status) && $value->e_sponsor_status=="start"){

						$update=array();
						if(isset($value['i_impression'])){
							$update['i_impression']=$value['i_impression']+1;
						}else{
							$update['i_impression']=1;
						}
						if(isset($value['i_impression_perday'])){
							$update['i_impression_perday']=$value['i_impression_perday']+1;
						}else{
							$update['i_impression_perday']=1;
						}

						if(isset($value['i_impression_day'])){
							$update['i_impression_day']=$value['i_impression_day']+1;
						}else{
							$update['i_impression_day']=1;
						}
						CoursesModel::find($value['_id'])->update($update);
					}else{
						$update=array();
						if(isset($value['i_impression_day'])){
							$update['i_impression_day']=$value['i_impression_day']+1;
						}else{
							$update['i_impression_day']=1;
						}
						CoursesModel::find($value['_id'])->update($update);
					}

					$cids[]	=$value->id;
				}

				if(count($findResult)){
					$findResult = $findResult->merge($courses);
				}else{
					$findResult = $courses;
				}
			}

			$courses = GeneralHelper::CourseSearchNormal($data,$pricefilter,$cids);
			if(count($courses)){
				foreach ($courses as $key => $value) {
					if(isset($value->e_sponsor_status) && $value->e_sponsor_status=="start"){

						$update=array();
						if(isset($value['i_impression'])){
							$update['i_impression']=$value['i_impression']+1;
						}else{
							$update['i_impression']=1;
						}
						if(isset($value['i_impression_perday'])){
							$update['i_impression_perday']=$value['i_impression_perday']+1;
						}else{
							$update['i_impression_perday']=1;
						}
						if(isset($value['i_impression_day'])){
							$update['i_impression_day']=$value['i_impression_day']+1;
						}else{
							$update['i_impression_day']=1;
						}
						CoursesModel::find($value['_id'])->update($update);
					}else{
						$update=array();
						if(isset($value['i_impression_day'])){
							$update['i_impression_day']=$value['i_impression_day']+1;
						}else{
							$update['i_impression_day']=1;
						}
						CoursesModel::find($value['_id'])->update($update);
					}
					$cids[]	=$value->id;

				}

				if(count($findResult)){
					$findResult = $findResult->merge($courses);
				}else{
					$findResult = $courses;
				}
			}

			$metaDetails = GeneralHelper::MetaFiledData('course-search','Course search');
		   	$_data = array(
					'data'   	=> $data,
					'courses'   => $findResult,
					'coursesLanguage' => $coursesLanguage,
					'coursesCategory' => $coursesCategory,
					'keyword'   => $keyword,
					's_course' => $s_course,
					'coursesLevel'=>$coursesLevel,
					'pricefilter'=>$pricefilter,
					'metaDetails'=>$metaDetails,
					'metaDetails'=> $metaDetails,
					'coursessidebar'=> $coursessidebar,
					'coursessidebar1'=> $coursessidebar1,
			);
		    return view('frontend/home/courses_search',$_data);

		}
		else if(isset($data['e_type']) && $data['e_type']=="online"){

			if(!isset($data['v_type_skill']) && !isset($data['v_type_job'])){
				$data['v_type_skill'] = "on";
			}

			$pricefilter = PricefilterModal::where("e_status","active")->orderBy("i_order")->get();
			$user_ids = $skill_users = $job_users = $s_skill = $s_job = array();
			$finalresult=array();
			$existskillids=array();
			$existjobids=array();

			$totaljob = 0;
			$totalskill = 0;

			//Bold Adds Skill
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){

				$skilldata = GeneralHelper::SkillSearchBold($data,$pricefilter);
				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}

							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);

						}

			  		}
			  	}
			}

		  	if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::JobSearchBold($data,$pricefilter);

		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}

							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
			  			}
			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

			//Latest user data 24 hours
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){

				$skilldata = GeneralHelper::SkillSearchNew($data,$existskillids,$pricefilter);

				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

			  		}
			  	}
			}

			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::JobSearchNew($data,$pricefilter,$existjobids);

		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}

							JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

			// urgent Job Listing
			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::JobSearchUrgent($data,$pricefilter,$existjobids);

		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}

							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}

				  			JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

			//Normal Listing
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){

				$skilldata = GeneralHelper::SkillSearchNoraml($data,$existskillids,$pricefilter);

				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

			  		}
			  	}
			}

			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::JobSearchNormal($data,$pricefilter,$existjobids);

		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

		  	$user_datalist = UsersModel::query()->whereIn('_id', $user_ids)->get();
		  	$user_data=array();
		  	foreach ($user_datalist as $key => $value) {
			  	$user_data[$value->_id] =  $value;
			}

		  	$shortlistedSkill = ShortlistedSkillsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedSkill as $key => $value) {
				$s_skill[] = $value->i_shortlist_id;
			}

			$shortlistedJobs = ShortlistedJobsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedJobs as $key => $value) {
				$s_job[] = $value->i_shortlist_id;
			}

			$categories = CategoriesModel::where("e_status","active")->where('v_type',"online")->orderBy('v_name')->get();
			$skills=array();

			if(isset($data['i_category_id']) && $data['i_category_id']!=""){
				$skills = SkillsModel::where('i_category_id',$data['i_category_id'])->orderBy('v_name')->get();
			}
			$metaDetails = GeneralHelper::MetaFiledData('online-skill-and-job','online job and skill search');

			$skillsidebar = GeneralHelper::SkillSearchBoldSidebar($data,$pricefilter);

			// echo "<pre>";
			// print_r($skillsidebar);
			// exit;


			$jobsidebar = GeneralHelper::JobSearchBoldSidebar($data,$pricefilter);

			$skillsidebar1=array();
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
				$skillsidebar1 = $skillsidebar;
			}

			$jobsidebar1=array();
			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
				$jobsidebar1 = $jobsidebar;
			}

			$_data=array(
				'data'   	=> $data,
				'finalresult'=> $finalresult,
				'user_data'  => $user_data,
				'keyword'   => $keyword,
				's_skill' => $s_skill,
				's_job' => $s_job,
				'totaljob' => $totaljob,
				'totalskill' => $totalskill,
				'categories' => $categories,
				'skills' => $skills,
				'pricefilter'=>$pricefilter,
				'metaDetails'=>$metaDetails,
				'skillsidebar'=>$skillsidebar,
				'jobsidebar'=>$jobsidebar,
				'skillsidebar1'=>$skillsidebar1,
				'jobsidebar1'=>$jobsidebar1,
				'v_service'=>"online",

			);
			return view('frontend/home/online_search',$_data);
	    }
		else if(isset($data['e_type']) && $data['e_type']=="inperson"){

			if(!isset($data['v_type_skill']) && !isset($data['v_type_job'])){
				$data['v_type_skill'] = "on";
			}

			//if(count($data)<=5){
			// 	$data['v_type_skill'] = "on";
			// 	$data['v_type_job'] = "on";
			// }

			$pricefilter = PricefilterModal::where("e_status","active")->orderBy("i_order")->get();
			$milesfilter = array(
				'5'=>"5",
				'10'=>"10",
				'15'=>"15",
				'25'=>"25",
				'50'=>"50",
			);

			$user_ids = $skill_users = $job_users = $s_skill = $s_job = array();
			$finalresult=array();
			$existskillids=array();
			$existjobids=array();

			$totaljob = 0;
			$totalskill = 0;

			//Bold Adds Skill
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
				$skilldata = GeneralHelper::InpersonSkillSearchBold($data,$pricefilter);
				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

			  		}
			  	}
			}

		  	if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::InpersonJobSearchBold($data,$pricefilter);

		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
				  			JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

			//Latest user data 24 hours
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){

				$skilldata = GeneralHelper::InpersonSkillSearchNew($data,$existskillids,$pricefilter);

				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

			  		}
			  	}
			}

			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::InpersonJobSearchNew($data,$pricefilter,$existjobids);
		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
				  			JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

			// urgent Job Listing
			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::InpersonJobSearchUrgent($data,$pricefilter,$existjobids);

		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}
			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

			//Normal Listing
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){

				$skilldata = GeneralHelper::InpersonSkillSearchNoraml($data,$existskillids,$pricefilter);

				if(count($skilldata)){
			  		foreach ($skilldata as $key => $value) {
			  			$finalresult[]=array(
			  				'type'=>"skill",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existskillids[] = $value['_id'];
			  			$totalskill = $totalskill+1;

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
							$update=array();
							if(isset($value['i_impression'])){
								$update['i_impression']=$value['i_impression']+1;
							}else{
								$update['i_impression']=1;
							}
							if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							SellerprofileModel::find($value['_id'])->update($update);
						}

			  		}
			  	}
			}

			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){

		  		$jobsdata = GeneralHelper::InpersonJobSearchNormal($data,$pricefilter,$existjobids);

		  	  	if(count($jobsdata)){
			  		foreach($jobsdata as $key => $value) {

			  			if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
			  				$update['i_impression']=1;
				  			if(isset($value['i_impression'])){
				  				$update['i_impression'] = $value['i_impression']+1;
				  			}
				  			if(isset($value['i_impression_perday'])){
								$update['i_impression_perday']=$value['i_impression_perday']+1;
							}else{
								$update['i_impression_perday']=1;
							}
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
			  			}else{
							$update=array();
							if(isset($value['i_impression_day'])){
								$update['i_impression_day']=$value['i_impression_day']+1;
							}else{
								$update['i_impression_day']=1;
							}
							JobsModel::find($value['_id'])->update($update);
						}

			  			$finalresult[]=array(
			  				'type'=>"jobs",
			  				'data'=>$value,
			  			);
			  			$user_ids[] = $value['i_user_id'];
			  			$existjobids[] = $value['_id'];
			  			$totaljob =$totaljob+1;
			  		}
			  	}
			}

		  	$user_datalist = UsersModel::query()->whereIn('_id', $user_ids)->get();
		  	$user_data=array();
		  	foreach ($user_datalist as $key => $value) {
			  	$user_data[$value->_id] =  $value;
			}

		  	$shortlistedSkill = ShortlistedSkillsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedSkill as $key => $value) {
				$s_skill[] = $value->i_shortlist_id;
			}

			$shortlistedJobs = ShortlistedJobsModel::where('i_user_id',$userid)->get();
			foreach ($shortlistedJobs as $key => $value) {
				$s_job[] = $value->i_shortlist_id;
			}

			$categories = CategoriesModel::where("e_status","active")->where('v_type',"inperson")->orderBy('v_name')->get();

			$skills=array();

			if(isset($data['i_category_id']) && $data['i_category_id']!=""){
				$skills = SkillsModel::where('i_category_id',$data['i_category_id'])->get();
			}

			$metaDetails = GeneralHelper::MetaFiledData('inperson-skill-and-job-result','inperson job and skill search');

			$skillsidebar = GeneralHelper::InpersonSkillSearchBoldSidebar($data,$pricefilter);
			$jobsidebar = GeneralHelper::InpersonJobSearchBoldSidebar($data,$pricefilter);

			$skillsidebar1=array();
			if(isset($data['v_type_skill']) && $data['v_type_skill']=="on"){
				$skillsidebar1 = $skillsidebar;
			}

			$jobsidebar1=array();
			if(isset($data['v_type_job']) && $data['v_type_job']=="on"){
				$jobsidebar1 = $jobsidebar;
			}


			$_data=array(
				'data'   	=> $data,
				'finalresult'=> $finalresult,
				'user_data'  => $user_data,
				'keyword'   => $keyword,
				's_skill' => $s_skill,
				's_job' => $s_job,
				'totaljob' => $totaljob,
				'totalskill' => $totalskill,
				'categories' => $categories,
				'skills' => $skills,
				'pricefilter'=>$pricefilter,
				'v_service'=>"inperson",
				'milesfilter'=>$milesfilter,
				'metaDetails'=>$metaDetails,
				'skillsidebar'=>$skillsidebar,
				'jobsidebar'=>$jobsidebar,
				'skillsidebar1'=>$skillsidebar1,
				'jobsidebar1'=>$jobsidebar1,
			);
			return view('frontend/home/online_search',$_data);

	  	}
    }

    public function shortlistResult(){

		$data  = Request::all();
		$response=array();

		if(!auth()->guard('web')->check()) {
			$response['status']=3;
			$response['msg']=GeneralHelper::errorMessage("Please Login to shortlist.");
			echo json_encode($response);
			exit;
		}

		if(!isset($data['_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		if(!isset($data['type'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		$userid = auth()->guard('web')->user()->id;

		if($data['type']=="jobs"){

			$existdata = ShortlistedJobsModel::where('i_user_id',$userid)->where('i_shortlist_id',$data['_id'])->first();

			if(!count($existdata)){

				$insert=array(
					'i_user_id' => $userid,
					'i_shortlist_id' => $data['_id'],
					'd_added' => date("Y-m-d H:i:s"),
				);
				ShortlistedJobsModel::create($insert);

				$jobdata = JobsModel::find($data['_id']);
				if(count($jobdata)){
					if(isset($jobdata->v_service) && $jobdata->v_service=="online"){
						$updateuser['v_seller_online_service']="active";
						UsersModel::find($userid)->update($updateuser);
					}else{
						$updateuser['v_seller_inperson_service']="active";
						UsersModel::find($userid)->update($updateuser);
					}
				}
				$response['status']=1;
				$response['msg']=GeneralHelper::successMessage("Job successfully shortlisted.");
				echo json_encode($response);
				exit;
			}else{
				ShortlistedJobsModel::find($existdata->_id)->delete();
				$response['status']=2;
				$response['msg']=GeneralHelper::successMessage("Successfully removed from shortlisted jobs.");
				echo json_encode($response);
				exit;
			}
		}else{

			$existdata = ShortlistedSkillsModel::where('i_user_id',$userid)->where('i_shortlist_id',$data['_id'])->first();
			$sellerdata = SellerprofileModel::find($data['_id']);

			$userid = auth()->guard('web')->user()->id;

			if(count($sellerdata)){
				if(isset($sellerdata->v_service) && $sellerdata->v_service=="online"){
					$updateuser['v_buyer_online_service']="active";
					UsersModel::find($userid)->update($updateuser);
				}else{
					$updateuser['v_buyer_inperson_service']="active";
					UsersModel::find($userid)->update($updateuser);
				}
			}

			if(!count($existdata)){

				$insert=array(
					'i_user_id' => $userid,
					'i_shortlist_id' => $data['_id'],
					'd_added' => date("Y-m-d H:i:s"),
				);
				ShortlistedSkillsModel::create($insert);

				$response['status']=1;
				$response['msg']=GeneralHelper::successMessage("Skill successfully shortlisted.");
				echo json_encode($response);
				exit;

			}else{

				ShortlistedSkillsModel::find($existdata->id)->delete();
				$response['status']=2;
				$response['msg']=GeneralHelper::successMessage("Successfully removed from shortlisted skill.");
				echo json_encode($response);
				exit;

			}
		}
	}

	public function coursesDetail($id="",$name=""){

		$courses = CoursesModel::find($id);
		if(!count($courses)){
			return redirect('/');
		}
		if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){
			if(isset($courses->e_sponsor_status) && $courses->e_sponsor_status=="start"){
				$total = $courses->i_clicks+1;
				$courses->i_clicks = (int)$total;
				$courses->save();
			}
			return redirect('course/'.$_REQUEST['id'].'/'.$name);
		}
		$reviews = CoursesReviews::where("i_courses_id",$id)->get();
		$ratedata=0;

		$total['f_rate_instruction']=0;
		$total['f_rate_navigate']=0;
		$total['f_rate_reccommend']=0;

		if(count($reviews)){
			foreach ($reviews as $key => $value) {
				$ratedata = $ratedata+($value->f_rate_instruction+$value->f_rate_navigate+$value->f_rate_reccommend)/3;
				$total['f_rate_instruction'] = $total['f_rate_instruction']+$value->f_rate_instruction;
				$total['f_rate_navigate'] = $total['f_rate_navigate']+$value->f_rate_navigate;
				$total['f_rate_reccommend'] = $total['f_rate_reccommend']+$value->f_rate_reccommend;
			}
		}
		$total['reviews'] = count($reviews);
		$total['ratedata'] = $ratedata;

		$user = UsersModel::where("e_status","active")->get();
		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}

		$metaDetails['v_meta_title'] = "Skillbox Course";
		$metaDetails['v_meta_keywords'] = "Skillbox Course";
		$metaDetails['l_meta_description'] = "Skillbox Course";

		$_data = array(
			'courses'   => $courses,
			'total'   => $total,
			'reviews'   => $reviews,
			'userList'   => $userList,
			'metaDetails'   => $metaDetails,
		);
		return view('frontend/home/courses_detail',$_data);
	}

	public function applyJob(){

		$data  = Request::all();
		$response=array();

		if(!isset($data['_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		$jobdata = JobsModel::find($data['_id']);
		if(!count($jobdata)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		if(!auth()->guard('web')->check()) {
			$rurl=url('online-job').'/'.$data['_id'];
			Session::put('redirecturl',$rurl);
			$response['status']=3;
			$response['msg'] = GeneralHelper::errorMessage("Please Login to shortlist.");
			echo json_encode($response);
			exit;
		}

		$service = $jobdata->v_service;
		$userid = auth()->guard('web')->user()->id;
		$userprofile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$service)->first();

		if(!count($userprofile)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Your ".ucfirst($service)." profile doesn't exist. Please go to your user panel and create an ".ucfirst($service)." profile to apply for this job.");
			echo json_encode($response);
			exit;
		}

		$insert['i_user_id']     = $userid;
		$insert['i_seler_profile_id']  = $userprofile->id;
		$insert['d_added']       = date("Y-m-d H:i:s");
		$insert['i_applied_id']  = $data['_id'];
		$insert['i_buyer_id']  	 = $jobdata->i_user_id;
		$insert['v_service']  	 = $jobdata->v_service;
		AppliedJobModel::create($insert);

		if(isset($jobdata->v_service) && $jobdata->v_service=="online"){
			$updateuser['v_seller_online_service']="active";
			UsersModel::find($userid)->update($updateuser);
		}else{
			$updateuser['v_seller_inperson_service']="active";
			UsersModel::find($userid)->update($updateuser);
		}

		$mailData['USER_FULL_NAME']="";
		$mailData['USER_EMAIL']="";
		$mailData['JOB_NAME']="";
		$mailData['JOB_RESPONSE_LINK']=url('buyer/appliedjobs');

		if(count($jobdata)){
			if(isset($jobdata->v_job_title)){
				$mailData['JOB_NAME']=$jobdata->v_job_title;
			}
			if(count($jobdata->hasUser()) && isset($jobdata->hasUser()->v_fname)){
		        $mailData['USER_FULL_NAME']=$jobdata->hasUser()->v_fname;
		    }
		    if(count($jobdata->hasUser()) && isset($jobdata->hasUser()->v_lname)){
		        $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$jobdata->hasUser()->v_lname;
		    }
		    if(count($jobdata->hasUser()) && isset($jobdata->hasUser()->v_email)){
		        $mailData['USER_EMAIL']= $jobdata->hasUser()->v_email;
		    }
		    //$mailData['JOB_RESPONSE_LINK'] = $mailData['JOB_RESPONSE_LINK'].$jobdata->id;

		}

		$mailcontent = EmailtemplateHelper::AppliedJobpost($mailData);
		$subject = "Seller applied for job";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b560ad1516b462ef00036a4");
		$mailids=array(
			$mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

		$response['status']=1;
		$response['msg']=GeneralHelper::successMessage("Successfully applied for job.");
		echo json_encode($response);
		exit;
	}

	public function jobdetailReview(){

		$data  = Request::all();
		$response=array();

		if(!isset($data['id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.please try again after sometime.");
			echo json_encode($response);
			exit;
		}
		$jobs = JobsModel::find($data['id']);

		$buyerreview = BuyerreviewModal::where('i_buyer_id',$jobs->i_user_id)->paginate(20);

		$btnstatus=0;
		if($buyerreview->lastPage()>$buyerreview->currentPage()){
			$btnstatus=1;
		}

		$_data=array(
			'buyerreview'=>$buyerreview,
		);

		$reviewstr = view('frontend/home/jobreview', $_data);
		$responsestr = $reviewstr->render();

		$response['status']=1;
		$response['btnstatus']=$btnstatus;
		$response['responsestr']=$responsestr;
		$response['msg']="Please Login.";
		echo json_encode($response);
		exit;
	}

	public function onlineJobDetail($id="",$name=""){

		if($id==""){
			return redirect("/");
		}
		$jobs = JobsModel::find($id);

		if(!count($jobs)){
			return redirect("/");
		}

		if(isset($jobs->i_delete) && $jobs->i_delete=="1"){
			return redirect("/");
		}

		$totaljob = JobsModel::where('i_user_id',$jobs->i_user_id)->get();
		if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){
			if(isset($jobs->e_sponsor_status) && $jobs->e_sponsor_status == "start"){
				$update['i_clicks']=1;
				if(isset($jobs->i_clicks)){
					$update['i_clicks'] = $jobs->i_clicks+1;
				}
				JobsModel::find($id)->update($update);
			}
			return redirect('online-job/'.$_REQUEST['id'].'/'.$name);
		}

		$buyerreview = BuyerreviewModal::where('i_buyer_id',$jobs->i_user_id)->get();

		$totalavg=0;
		if(count($buyerreview)){
			foreach ($buyerreview as $key => $value) {
				$totalavg = $totalavg + ($value['i_work_star']+$value['i_support_star']+$value['i_opportunities_star']+$value['i_work_again_star'])/4;
			}
			$totalavg  = $totalavg/count($buyerreview);
		}


		$other_skill_ids = array();
		if(isset($jobs) && count($jobs)){
			if(isset($jobs->i_otherskill_id) && count($jobs->i_otherskill_id)){
				foreach ($jobs->i_otherskill_id as $key => $value) {
					$other_skill_ids[] = $value;
				}
			}
		}

		$other_skills_name = SkillsModel::whereIn("_id",$other_skill_ids)->get();
		if(isset($other_skills_name) && count($other_skills_name)){
			foreach($other_skills_name as $key => $value){
				$v_name[] = $value->v_name;
			}
		}
    	$v_name = implode(', ', $v_name);

		$is_shortlisted = $is_applied = 0;

		$userid = "";

		if(auth()->guard('web')->check()) {

			$userid = auth()->guard('web')->user()->id;
			$shortlistedJobs = ShortlistedJobsModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->first();

			if(count($shortlistedJobs) && !empty($shortlistedJobs)){
				$is_shortlisted = 1;
			}

			$appliedJob = AppliedJobModel::where('i_applied_id',$id)->where('i_user_id',$userid)->first();
			if(count($appliedJob) && !empty($appliedJob)){
				$is_applied = 1;
			}
		}

		$metaDetails['v_meta_title'] = "Skillbox skill and job";
		$metaDetails['v_meta_keywords'] = "Skillbox skill and job";
		$metaDetails['l_meta_description'] = "Skillbox skill and job";

		$_data=array(
			'jobs'   => $jobs,
			'is_shortlisted'  => $is_shortlisted,
			'is_applied'  => $is_applied,
			'other_skills_data'=>$v_name,
			'buyerreview'=>$buyerreview,
			'totalavg'=>$totalavg,
			'metaDetails'=>$metaDetails,
			'totaljob'=>count($totaljob)
		);
		return view('frontend/home/online_job_detail',$_data);
	}

    public function jobShortlist(){

		$data     = Request::all();
		$hidval   = $data['hidval'];
		$id       = $data['_id'];

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Please Login.";
			echo json_encode($response);
			exit;
		}

		if($hidval == 0){

		}else{
			ShortlistedJobsModel::where('i_shortlist_id',$id)->delete();
		}

		$response['status']=1;
		echo json_encode($response);
	}

	public function onlineDetail($id="",$name=""){

		if($id==""){
			return redirect("/");
		}


		$sellerprofile = SellerprofileModel::find($id);

		if(!count($sellerprofile)){
			return redirect("/");
		}

		if(isset($sellerprofile->i_delete) && $sellerprofile->i_delete=="1"){
			return redirect("/");
		}

		if(isset($_REQUEST['id']) && $_REQUEST['id']!=""){

			if(isset($sellerprofile->e_sponsor_status) && $sellerprofile->e_sponsor_status == "start"){
				$update['i_clicks']=1;
				if(isset($sellerprofile->i_clicks)){
					$update['i_clicks'] = $sellerprofile->i_clicks+1;
				}
				SellerprofileModel::find($id)->update($update);
			}
			return redirect('online/'.$_REQUEST['id'].'/'.$name);
		}

		$reviewdata = SellerreviewModal::where('i_seller_profile_id',$sellerprofile->id)->get();
		$review['avgtotal']=0;
		$review['total']=count($reviewdata);
		$review['i_communication_star']=0;
		$review['i_described_star']=0;
		$review['i_recommend_star']=0;

		if(count($reviewdata)){
			foreach ($reviewdata as $key => $value) {

				$review['avgtotal'] = 	$review['avgtotal']+(($value['i_communication_star']+$value['i_described_star']+$value['i_recommend_star'])/3);
				$review['i_communication_star'] = $review['i_communication_star']+$value['i_communication_star'];
				$review['i_described_star'] = 	$review['i_described_star']+$value['i_described_star'];
				$review['i_recommend_star'] = 	$review['i_recommend_star']+$value['i_recommend_star'];
			}

		}
		//dd($review);

		$other_skill_ids = array();
		if(isset($sellerprofile) && count($sellerprofile)){
			if(isset($sellerprofile->i_otherskill_id) && count($sellerprofile->i_otherskill_id)){
				foreach ($sellerprofile->i_otherskill_id as $key => $value) {
					$other_skill_ids[] = $value;
				}
			}
		}

		$other_skills_name = SkillsModel::whereIn("_id",$other_skill_ids)->get();
		if(isset($other_skills_name) && count($other_skills_name)){
			foreach($other_skills_name as $key => $value){
				$v_name[] = $value->v_name;
			}
		}
 	    $v_name = implode(', ', $v_name);

   		$userid = "";
		$is_shortlisted = 0;
		$is_login=0;
		if(auth()->guard('web')->check()) {

			$userid = auth()->guard('web')->user()->id;
			$shortlistedSkill = ShortlistedSkillsModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->first();

			if(count($shortlistedSkill) && !empty($shortlistedSkill)){
				$is_shortlisted = 1;
			}
			$is_login=1;
		}


		$query = SellerprofileModel::query();
		$query = $query->where('i_user_id','!=',$sellerprofile->i_user_id);
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$query = $query->where('i_category_id',$sellerprofile->i_category_id);
		$skilldata = $query->limit(4)->get()->toArray();
		$user_ids=array();

		if(count($skilldata)){
			foreach ($skilldata as $key => $value) {
				$user_ids[] = $value['i_user_id'];
			}
		}

		$user_data = UsersModel::query()->whereIn('_id', $user_ids)->get();
		$userlist=array();
	    foreach ($user_data as $key => $value) {
	  	   $userlist[$value->_id] =  $value;
	    }

	    $userdata['email']="";
	    $userdata['name']="";

	    if(auth()->guard('web')->check()) {
	    	$userdata['email'] = auth()->guard('web')->user()->v_email;
	       	if(isset(auth()->guard('web')->user()->v_fname)){
		    	$userdata['name'] = auth()->guard('web')->user()->v_fname;
		    }
	    	if(isset(auth()->guard('web')->user()->v_lname)){
	    		$userdata['name'] = $userdata['name'] .' '.auth()->guard('web')->user()->v_lname;
	    	}
	    }

	    $metaDetails['v_meta_title'] = "Skillbox skill and job";
		$metaDetails['v_meta_keywords'] = "Skillbox skill and job";
		$metaDetails['l_meta_description'] = "Skillbox skill and job";

		$leveldata=array();
		if(count($sellerprofile->hasUser()) && isset($sellerprofile->hasUser()->v_level)){
            if($sellerprofile->hasUser()->v_level!="0"){
            	$leveldata = GeneralHelper::LevelData($sellerprofile->hasUser()->v_level);
            }
        }

        $s_skill=array();
        $shortlistedSkill = ShortlistedSkillsModel::where('i_user_id',$userid)->get();
		foreach ($shortlistedSkill as $key => $value) {
			$s_skill[] = $value->i_shortlist_id;
		}

		// print_r($sellerprofile); exit;

       	$_data=array(
			'sellerprofile'   => $sellerprofile,
			'is_shortlisted'  => $is_shortlisted,
			'other_skills_data'=>$v_name,
			'skilldata'=>$skilldata,
			'userlist'=>$userlist,
			'review'=>$review,
			'metaDetails'=>$metaDetails,
			'userdata'=>$userdata,
			'leveldata'=>$leveldata,
			's_skill'=>$s_skill
		);
       	return view('frontend/home/online_detail',$_data);

	}

	public function SkillReview(){

		$data = Request::all();

		if(!isset($data['id'])){
			$response['status']=0;
			$response['msg']="Please Login.";
			echo json_encode($response);
			exit;
		}

		$sellerprofile = SellerprofileModel::find($data['id']);
		if(!count($sellerprofile)){
			return redirect("/");
		}
		$reviewdata = SellerreviewModal::where('i_seller_profile_id',$sellerprofile->id)->paginate(20);

		$btnstatus=0;
		if($reviewdata->lastPage()>$reviewdata->currentPage()){
			$btnstatus=1;
		}

		$_data=array(
			'reviewdata'=>$reviewdata,
		);

		$reviewstr = view('frontend/home/skillreview', $_data );
		$responsestr = $reviewstr->render();
		$response['status']=1;
		$response['page']=$reviewdata->currentPage()+1;
		$response['btnstatus']=$btnstatus;
		$response['responsestr']=$responsestr;
		$response['msg']="Please Login.";
		echo json_encode($response);
		exit;
	}

	public function coursesShortlist(){

		$data     = Request::all();
		$hidval   = $data['hidval'];
		$id       = $data['_id'];

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Please Login.";
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->id;

		$updateuser['v_buyer_online_service']="active";
		UsersModel::find($userid)->update($updateuser);

		$response['msg']="";
		if($hidval == 0){
			$data['i_user_id']  = $userid;
			$data['d_added']    = date("Y-m-d H:i:s");
			$data['i_shortlist_id'] = $id;
			$ShortlistedCoursesId = ShortlistedCourseModel::create($data)->id;
			$response['msg']=GeneralHelper::successMessage("Course successfully shortlisted.");
		}else{
			ShortlistedCourseModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->delete();
			$response['msg']=GeneralHelper::successMessage("Successfully removed from shortlisted course.");
		}

		$response['status']=1;
		echo json_encode($response);
		exit;
	}

	public function skillShortlist(){

		$data     = Request::all();
		$hidval   = $data['hidval'];
		$id       = $data['_id'];
		$type     = $data['type'];

		if(!auth()->guard('web')->check()) {

			$response['status']=0;
			$response['msg']="Please Login.";
			echo json_encode($response);
			exit;
		}
		else{

			$userid = auth()->guard('web')->user()->id;
		}

		if($hidval == 0){

				if($type == 'skill'){

					$data['i_user_id']     = $userid;
					$data['d_added']       = date("Y-m-d H:i:s");
					$data['i_shortlist_id']= $id;
					ShortlistedSkillsModel::create($data)->id;
				}else if($type == 'job'){

					$data['i_user_id']     = $userid;
					$data['d_added']       = date("Y-m-d H:i:s");
					$data['i_shortlist_id']= $id;

					ShortlistedJobsModel::create($data)->id;
				}

		}
		else{
				if($type == 'skill'){
				 ShortlistedSkillsModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->delete();
				}else if($type == 'job'){
					 ShortlistedJobsModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->delete();
				}
		}

		$response['status']=1;
		echo json_encode($response);
		exit;
	}






}