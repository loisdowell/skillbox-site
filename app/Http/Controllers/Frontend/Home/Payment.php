<?php
namespace App\Http\Controllers\Frontend\Home;

use Request, Hash, Lang,Validator,Auth,Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
//use MongoDB\BSON\ObjectId;


class Payment extends Controller {
		
	protected $section;

	public function __construct(){
		$this->section = Lang::get('section.dashboard');
	}
		
	public function index() {
		return view('frontend/home/payment');
	}

	public function Search(){

		$data = Request::all();
		$keyword  = isset($data['keyword']) ? $data['keyword'] : ''; 
		$userid = 0;
		
		if(auth()->guard('web')->check()) {
			  $userid 	= auth()->guard('web')->user()->id;   
		}
		
		if(isset($data['e_type']) && $data['e_type']=="courses"){
			
			$s_course = array();
			$coursesLevel = CoursesLevelModel::where("e_status","active")->get();
			$coursesLanguage = CoursesLanguageModel::where("e_status","active")->get();
			$coursesCategory = CoursesCategoryModel::where("e_status","active")->get();
			$shortlistedCourse = ShortlistedCourseModel::where('i_user_id',$userid)->get();
		
			foreach ($shortlistedCourse as $key => $value) {
				$s_course[] = $value->i_shortlist_id;
			}
			
			$query = CoursesModel::query();
			
			if(isset($data['keyword']) && $data['keyword']!=""){
				$query = $query->where('tbl_courses.v_title',$data['keyword']);
			}
		    $query = $query->where('e_status',"published");
		    $courses = $query->get();

		    $cids=array();
			if(count($courses)){
				foreach ($courses as $key => $value) {
					$cids[]	=$value->id;
				}
			}

			$reviews = CoursesReviews::whereIn("i_courses_id",$cids)->get();
			$reviewsdata=array();
			if(count($reviews)){
				foreach ($reviews as $key => $value) {

					if(isset($reviewsdata[$value->i_courses_id])){
						$ratingdata = ceil(($value->f_rate_instruction+$value->f_rate_navigate+$value->f_rate_reccommend)/3); 					
						$reviewsdata[$value->i_courses_id]['rate']=$reviewsdata[$value->i_courses_id]['rate']+$ratingdata;
						$reviewsdata[$value->i_courses_id]['cnt'] = $reviewsdata[$value->i_courses_id]['cnt']+1;
					}else{
						$reviewsdata[$value->i_courses_id]['rate'] = ceil(($value->f_rate_instruction+$value->f_rate_navigate+$value->f_rate_reccommend)/3); 			
						$reviewsdata[$value->i_courses_id]['cnt'] = 1;

					}		
				}
			}

		   	$_data=array(
					'data'   	=> $data,
					'courses'   => $courses,
					'coursesLevel'   => $coursesLevel,
					'coursesLanguage'   => $coursesLanguage,
					'coursesCategory'   => $coursesCategory,
					'keyword'   => $keyword,
					's_course' => $s_course,
					'reviewsdata' => $reviewsdata,

			);
			return view('frontend/home/courses_search',$_data);	



		}else if(isset($data['e_type']) && $data['e_type']=="online"){
			
			$user_ids = $skill_users = $job_users = $s_skill = $s_job = array();
			$query = SellerprofileModel::query();
			
			if(isset($data['keyword']) && $data['keyword']!=""){
				$query = $query->where('tbl_seller_profile.v_profile_title',$data['keyword']);
			}
		    $skill = $query->get()->toArray();

		    $shortlistedSkill = ShortlistedSkillsModel::where('i_user_id',$userid)->get();
			
			foreach ($shortlistedSkill as $key => $value) {
				$s_skill[] = $value->i_shortlist_id;
			}
		 

		  foreach ($skill as $key => $value) {
		  		$skill[$key]['type'] = 'skill'; 
		  		$user_ids[] = $value['i_user_id'];		
		  }

		  $skilluser_data = UsersModel::query()->whereIn('_id', $user_ids)->get();

		  foreach ($skilluser_data as $key => $value) {
		  	$skill_users[$value->_id] =  $value;
		  }
			
		  $query1 = JobsModel::query()->where('v_service', 'online');

		 //  if(isset($data['keyword']) && $data['keyword']!=""){
			// 	$query1 = $query1->where('tbl_jobs.v_job_title',$data['keyword']);
			// }

			$jobs = $query1->get()->toArray();

			$shortlistedJobs = ShortlistedJobsModel::where('i_user_id',$userid)->get();
			
			foreach ($shortlistedJobs as $key => $value) {
				$s_job[] = $value->i_shortlist_id;
			}
		  
		  foreach ($jobs as $key => $value) {
		  		$jobs[$key]['type'] = 'job'; 
		  }

		  $jobuser_data = UsersModel::query()->whereIn('_id', $user_ids)->get();

		  foreach ($jobuser_data as $key => $value) {
		  	$job_users[$value->_id] =  $value;
		  } 
		   
		  $skills = array_merge($skill,$jobs);
			
		  $_data=array(
				'skill'   	=> $skills,
				'skill_users' => $skill_users,
				'job_users' => $job_users,
				'keyword'   => $keyword,
				's_skill' => $s_skill,
				's_job' => $s_job,
			);
	    
	    return view('frontend/home/online_search',$_data);	
	    
	  }else if(isset($data['e_type']) && $data['e_type']=="online-job"){
	  	
	  	$query = JobsModel::query()->where('v_service', 'online');
			
			// if(isset($data['keyword']) && $data['keyword']!=""){
			// 	$query = $query->where('tbl_seller_profile.v_profile_title',$data['keyword']);
			// }
		  
		  $jobs = $query->get();
		  
		  $_data=array(
				'jobs'   	  => $jobs,
				'keyword'   => $data['keyword'],
			);
	    
	    return view('frontend/home/online_job_search',$_data);

		}else if(isset($data['e_type']) && $data['e_type']=="inperson"){
			
			$user_ids = $skill_users = $job_users = $s_skill = $s_job = array();
			$query = SellerprofileModel::query();
			
			if(isset($data['keyword']) && $data['keyword']!=""){
				$query = $query->where('tbl_seller_profile.v_profile_title',$data['keyword']);
			}
		    $skill = $query->get()->toArray();

		    $shortlistedSkill = ShortlistedSkillsModel::where('i_user_id',$userid)->get();
			
			foreach ($shortlistedSkill as $key => $value) {
				$s_skill[] = $value->i_shortlist_id;
			}
		 

		  foreach ($skill as $key => $value) {
		  		$skill[$key]['type'] = 'skill'; 
		  		$user_ids[] = $value['i_user_id'];		
		  }

		  $skilluser_data = UsersModel::query()->whereIn('_id', $user_ids)->get();

		  foreach ($skilluser_data as $key => $value) {
		  	$skill_users[$value->_id] =  $value;
		  }
			
		  $query1 = JobsModel::query()->where('v_service', 'inperson');

		 //  if(isset($data['keyword']) && $data['keyword']!=""){
			// 	$query1 = $query1->where('tbl_jobs.v_job_title',$data['keyword']);
			// }

			$jobs = $query1->get()->toArray();

			$shortlistedJobs = ShortlistedJobsModel::where('i_user_id',$userid)->get();
			
			foreach ($shortlistedJobs as $key => $value) {
				$s_job[] = $value->i_shortlist_id;
			}
		  
		  foreach ($jobs as $key => $value) {
		  		$jobs[$key]['type'] = 'job'; 
		  }

		  $jobuser_data = UsersModel::query()->whereIn('_id', $user_ids)->get();

		  foreach ($jobuser_data as $key => $value) {
		  	$job_users[$value->_id] =  $value;
		  } 
		   
		  $skills = array_merge($skill,$jobs);
			
		  $_data=array(
				'skill'   	=> $skills,
				'skill_users' => $skill_users,
				'job_users' => $job_users,
				'keyword'   => $keyword,
				's_skill' => $s_skill,
				's_job' => $s_job,
			);
	    
	    return view('frontend/home/inperson_search',$_data);	
	    
	  } 

}

	public function coursesSearchAdcance(){
		
		$data = Request::all();
	
		$coursesLevel = CoursesLevelModel::where("e_status","active")->get();
		$coursesLanguage = CoursesLanguageModel::where("e_status","active")->get();
		$coursesCategory = CoursesCategoryModel::where("e_status","active")->get();
		
		$query = CoursesModel::query();

		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where('v_title',$data['keyword']);
		}
		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_level_id']) && $data['i_level_id']!=""){
			$query = $query->where('i_level_id',$data['i_level_id']);
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			
			if($data['relevance']=="latest"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="lowtohigh"){
				$query = $query->orderBy('f_price',"ASC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('f_price',"DESC");
			}

		}
	    $query = $query->where('e_status',"published");
        $courses = $query->get();

        $_data=array(
			'data'   	=> $data,
			'courses'   => $courses,
			'coursesLevel'   => $coursesLevel,
			'coursesLanguage'   => $coursesLanguage,
			'coursesCategory'   => $coursesCategory,
			'keyword'   => $data['keyword'],
		);
		
		return view('frontend/home/courses_search',$_data);		
	}

	

	public function coursesDetail($id=""){
	
		$courses = CoursesModel::find($id);
		if(!count($courses)){
			return redirect('/');	
		}

		$reviews = CoursesReviews::where("i_courses_id",$id)->get();
		$ratedata=0;

		$total['f_rate_instruction']=0;
		$total['f_rate_navigate']=0;
		$total['f_rate_reccommend']=0;
	
		if(count($reviews)){
			foreach ($reviews as $key => $value) {
				$ratedata = $ratedata+ceil(($value->f_rate_instruction+$value->f_rate_navigate+$value->f_rate_reccommend)/3); 
				$total['f_rate_instruction'] = $total['f_rate_instruction']+$value->f_rate_instruction;
				$total['f_rate_navigate'] = $total['f_rate_navigate']+$value->f_rate_navigate;
				$total['f_rate_reccommend'] = $total['f_rate_reccommend']+$value->f_rate_reccommend;
			}
		}
		
		$total['reviews'] = count($reviews);
		$total['ratedata'] = $ratedata;

		$user = UsersModel::where("e_status","active")->get();
		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}

		$_data=array(
			'courses'   => $courses,
			'total'   => $total,
			'reviews'   => $reviews,
			'userList'   => $userList,

		);
		return view('frontend/home/courses_detail',$_data);		

	}

	public function onlineDetail($id=""){

		$sellerprofile = SellerprofileModel::find($id);

		$other_skills = array();
		foreach ($sellerprofile as $key => $value) {
				$other_skills[] = $value->i_otherskill_id;
		}

		dd($other_skills);
		$userid = "";
		if(auth()->guard('web')->check()) {
			  $userid 	= auth()->guard('web')->user()->id;   
		}
		
		$is_shortlisted = 0;
		$shortlistedSkill = ShortlistedSkillsModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->first();

		if(count($shortlistedSkill) && !empty($shortlistedSkill)){
			$is_shortlisted = 1;
		}
		
 		if(!count($sellerprofile)){
		 	return redirect('/');	
		}

		$_data=array(
			'sellerprofile'   => $sellerprofile,
			'is_shortlisted'  => $is_shortlisted,
		);
		 return view('frontend/home/online_detail',$_data);		

	}
	public function onlineJobDetail($id=""){
		
		$jobs = JobsModel::find($id);
		$userid = "";
		if(auth()->guard('web')->check()) {
			  $userid 	= auth()->guard('web')->user()->id;   
		}
		
		$is_shortlisted = $is_applied = 0;
		$shortlistedJobs = ShortlistedJobsModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->first();

		if(count($shortlistedJobs) && !empty($shortlistedJobs)){
			$is_shortlisted = 1;
		}

		$appliedJob = AppliedJobModel::where('i_applied_id',$id)->where('i_user_id',$userid)->first();
		
		if(count($appliedJob) && !empty($appliedJob)){
			$is_applied = 1;
		}
		 
 		if(!count($jobs)){
		 	return redirect('/');	
		}

		$_data=array(
			'jobs'   => $jobs,
			'is_shortlisted'  => $is_shortlisted,
			'is_applied'  => $is_applied,
		);
		 return view('frontend/home/online_job_detail',$_data);		

	}

	public function coursesShortlist(){

		$data     = Request::all();
		$hidval   = $data['hidval'];
		$id       = $data['_id'];
		
		if(!auth()->guard('web')->check()) { 
			$response['status']=0;
			$response['msg']="Please Login.";
			echo json_encode($response);
			exit;
		}else{
			$userid 	= auth()->guard('web')->user()->id;
		}
		if($hidval == 0){
				
				$data['i_user_id']     = $userid;	
				$data['d_added']       = date("Y-m-d H:i:s");	
				$data['i_shortlist_id']= $id;	

				$ShortlistedCoursesId = ShortlistedCourseModel::create($data)->id;
		}else{
				 ShortlistedCourseModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->delete();
		}

		$response['status']=1;
		echo json_encode($response);
		exit;
	}

	public function skillShortlist(){
		
		$data     = Request::all();
		$hidval   = $data['hidval'];
		$id       = $data['_id'];
		$type     = $data['type'];

		if(!auth()->guard('web')->check()) {
			
			$response['status']=0;
			$response['msg']="Please Login.";
			echo json_encode($response);
			exit;
		}
		else{
			
			$userid = auth()->guard('web')->user()->id;
		}

		if($hidval == 0){
				
				if($type == 'skill'){

					$data['i_user_id']     = $userid;	
					$data['d_added']       = date("Y-m-d H:i:s");	
					$data['i_shortlist_id']= $id;	

					ShortlistedSkillsModel::create($data)->id;
				}else if($type == 'job'){
					
					$data['i_user_id']     = $userid;	
					$data['d_added']       = date("Y-m-d H:i:s");	
					$data['i_shortlist_id']= $id;	

					ShortlistedJobsModel::create($data)->id;
				}
				
		}
		else{
				if($type == 'skill'){
				 ShortlistedSkillsModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->delete();
				}else if($type == 'job'){
					 ShortlistedJobsModel::where('i_shortlist_id',$id)->where('i_user_id',$userid)->delete();
				}
		}

		$response['status']=1;
		echo json_encode($response);
		exit;
	}


	public function jobShortlist(){
		
		$data     = Request::all();
		$hidval   = $data['hidval'];
		$id       = $data['_id'];

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Please Login.";
			echo json_encode($response);
			exit;
		}
		
		if($hidval == 0){
				
		}else{
				 ShortlistedJobsModel::where('i_shortlist_id',$id)->delete();
		}

		$response['status']=1;
		echo json_encode($response);
	}

	public function applyJob(){

		$data     = Request::all();
		$id       = $data['_id'];
		
		// print_r($id);
		// exit;
		if(!auth()->guard('web')->check()) { 
			$response['status']=0;
			$response['msg']="Please Login.";
			echo json_encode($response);
			exit;
		}else{
			$userid 	= auth()->guard('web')->user()->id;
		}
				
		$data['i_user_id']     = $userid;	
		$data['d_added']       = date("Y-m-d H:i:s");	
		$data['i_applied_id']= $id;	

		AppliedJobModel::create($data)->id;
		

		$response['status']=1;
		echo json_encode($response);
		exit;
	}

}