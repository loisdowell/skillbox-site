<?php
namespace App\Http\Controllers\Frontend\Blog;

use Request, Hash, Lang;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Blogs\Blogs as BlogsModel;


class Blog extends Controller {

	protected $section;
	public function __construct(){
		$this->section = Lang::get('section.blog');
	}
	
	public function index(){

		$metaDetails = GeneralHelper::MetaFiledData('blog-education','Blog & Education');
		$blog = BlogsModel::where("e_status","active")->orderBy("d_added","DESC")->paginate(20);
		
		$data=array(
			'blog'=>$blog,
			'metaDetails'=> $metaDetails
		);
		return view('frontend/Blog/blog_list',$data);

	}

	public function detail($id=""){
		
		$blogdata = BlogsModel::where('v_slug',$id)->first();
		$blog = BlogsModel::where("e_status","active")->where("v_slug","!=",$id)->orderBy("d_added","DESC")->get();
		
		$metaDetails = GeneralHelper::MetaFiledData('blog-deatail','Blog & Education detail');

		if(isset($blogdata->v_meta_title) && $blogdata->v_meta_title!=""){
			$metaDetails['v_meta_title']=$blogdata->v_meta_title;	
		}
		if(isset($blogdata->v_meta_keywords) && $blogdata->v_meta_keywords!=""){
			$metaDetails['v_meta_keywords']=$blogdata->v_meta_keywords;	
		}
		if(isset($blogdata->l_meta_description) && $blogdata->l_meta_description!=""){
			$metaDetails['l_meta_description']=$blogdata->l_meta_description;	
		}

		$data=array(
			'blog'=>$blog,
			'blogdata'=>$blogdata,
			'metaDetails'=> $metaDetails
		);
		return view('frontend/Blog/blog_detail',$data);	

	}

}