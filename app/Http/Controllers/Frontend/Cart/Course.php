<?php
namespace App\Http\Controllers\Frontend\Cart;

use Request, Hash, Lang,Validator,Auth,Storage,Cart;

use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;
use App\Models\Courses\BuyerCourse as BuyerCourse; 

//use MongoDB\BSON\ObjectId;


class Course extends Controller {
		
	protected $section;

	public function __construct(){
		$this->section = Lang::get('section.course cart');
	}

    public function cartCommon() {
       
        if(sizeof(Cart::content())<1){
            return view('frontend/Cart/common');    
        }

        $skill=0;
        foreach (Cart::content() as $value) {
            if(isset($value->options['i_seller_profile_id']) && $value->options['i_seller_profile_id']!=""){
                $skill=1;
            }
        }
        if($skill){
            return redirect('cart/skill');
        }else{
            return redirect('cart/course');    
        }
    }

	public function index() {
		
        if(!auth()->guard('web')->check()) {
            return redirect('login');
        }
        
        $data=Cart::content();    
        $itemdata=array();
        $totalamt = 0;
        $course_id ="";
        $price=0;
        foreach(Cart::content() as $item){
            $itemdata[] =array(
                'id'=>$item->id,
                'name'=>$item->name,
                'price'=>$item->price,
            );
            $price = $item->price;
            $totalamt = $totalamt + ($item->price*$item->qty);
            $course_id = $item->id;
        }
        $userid = auth()->guard('web')->user()->id;
        $exist = BuyerCourse::where('i_user_id',$userid)->where('i_course_id',$course_id)->get();
        
        if(count($exist)){
            Cart::destroy();
            return redirect('buyer/courses');
        }
        
        if($price==0.0 && count($data)){
            
            $coursedata = CoursesModel::find($course_id);
            if(!count($coursedata)){
	            return redirect('cart/course');
	        } 
            
            if(isset($coursedata->e_sponsor_status) && $coursedata->e_sponsor_status=="start"){
                $insert['v_sponser_sales']="yes";
            }
            
            $insert['i_user_id']=$userid;
	        $insert['i_seller_id']=$coursedata->i_user_id;
	        $insert['i_course_id']=$coursedata->id;
	        $insert['i_order_no']=GeneralHelper::OrderRefNumbaer();
	        $insert['e_transaction_type']="course";
	        $insert['e_payment_type']="mangopay";
	        $insert['v_transcation_id']=0;
	        $insert['e_payment_status']="success";
	        $insert['v_amount']=$totalamt;
	        $insert['v_order_detail'] = $itemdata;
	        $insert['v_processing_fees']=0;
	        $insert['v_order_status']="active";
	        $insert['e_seller_payment_status']="success";
	        $insert['d_date']=date("Y-m-d");
	        $insert['d_added']=date("Y-m-d H:i:s");
	        $insert['d_modified']=date("Y-m-d H:i:s");
	        $oid = OrderModal::create($insert)->id;
	        $orderdata = OrderModal::find($oid);

	        $totallacture=0;
            if(isset($coursedata->section) && count($coursedata->section)){
                foreach ($coursedata->section as $key => $value) {
                	$totallacture= $totallacture+count($value['video']);   
                }
            }

            $insertbuyer[]=array(
                'i_course_id'=>$coursedata->id,
                'i_user_id'=>$orderdata->i_user_id,
                'v_order_id'=>$orderdata->id,
                'e_payment_type'=>"mangopay",
                'v_transcation_id'=>$orderdata->v_transcation_id,
                'e_payment_status'=>"success",
                'e_status'=>"not_started",
                'v_total_lectures'=>$totallacture,
                'v_complete_lectures'=>"0",
                'v_complete_ids'=>array(),
                'd_added'=>date("Y-m-d H:i:s"),
                'd_modified'=>date("Y-m-d H:i:s"),
            );
            BuyerCourse::insert($insertbuyer);
            self::sendEmailNotificationSeller($coursedata->id,$orderdata->id);
            self::sendEmailNotificationBuyer($coursedata->id,$orderdata->id);
           
            if(isset($coursedata->messages) && count($coursedata->messages)){
                if(isset($coursedata->messages['welcome_message']) && $coursedata->messages['welcome_message']!=""){
                    self::sendWelcomeCourseNotification($coursedata);    
                }
                
            }
            Cart::destroy();
        	return redirect('buyer/courses');
        }

        $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION');
        if($comission==""){
            $comission=10;
        }
        $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE');
        if($processing==""){
            $processing=0.60;
        }

        $_data = array(
            'comission'=>$comission,
            'processing'=>$processing,
        );
       	return view('frontend/Cart/course',$_data);
   	}

   	public function removeRawItem($id=""){
		Cart::remove($id);
		return redirect('cart/course');
	}

    public function sendEmailNotificationSeller($id="",$orderid=""){
        
        if($id==""){
            return 0;
        }
        if($orderid==""){
            return 0;
        }

        $data = CoursesModel::find($id);
        $orderdata = OrderModal::find($orderid);

        if(!count($data)){
            return 0;
        }

        if(!count($orderdata)){
            return 0;
        }

        $buyername="";
        if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_fname)){
            $buyername = $orderdata->hasUser()->v_fname;
        }
        if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_lname)){
            $buyername .= ' '.$orderdata->hasUser()->v_lname;
        }

        $sellername="";
        if(count($data->hasUser()) && isset($data->hasUser()->v_fname)){
            $sellername = $data->hasUser()->v_fname;
        }
        if(count($data->hasUser()) && isset($data->hasUser()->v_lname)){
            $sellername .= ' '.$data->hasUser()->v_lname;
        }

        $selleremail="";
        if(count($data->hasUser()) && isset($data->hasUser()->v_email)){
            $selleremail = $data->hasUser()->v_email;
        }

        $orderstr="";
        $orderstr.="<p>Order Successfully Placed.</p>";
        $orderstr.="<p>We are Pleased to confirm your order no ".$orderdata->i_order_no."</p>";
        if($buyername!=""){
            $orderstr.="<br><p>Buyer Name : ".$buyername."</p>";
        }
        $orderstr.="<table style='width:100%;border-spacing: 0px;' border=1>";
        $orderstr.="<tr>
                    <th><b>Title</b></th>
                    <th><b>Price</b></th>
                    <th><b>Total</b></th>
                    </tr>";
       $orderstr.="<tr>
                    <td>".$data->v_title."</td>
                    <td>Free</td>
                    <td>Free</td>
                   </tr>"; 
        $orderstr.="<tr>
                    <td></td>
                    <td>Total</td>
                    <td>Free</td>
                   </tr>";                      
        $orderstr.="</table>";

        $data=array(
            'name'=>$sellername,
            'COURSE_ORDER_DETAIL'=>$orderstr,
        );

        $mailcontent = EmailtemplateHelper::OrderconformationSeller($data);
        $subject = "Order Placed";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5b0e89fc76fbae6964350442");
        $mailids=array(
            $sellername=>$selleremail,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

    public function sendEmailNotificationBuyer($id="",$orderid=""){
        
        if($id==""){
            return 0;
        }
        if($orderid==""){
            return 0;
        }


        $v_email = auth()->guard('web')->user()->v_email;
        $buyername="";
        if(isset(auth()->guard('web')->user()->v_fname) && auth()->guard('web')->user()->v_fname!=""){
            $buyername = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname) && auth()->guard('web')->user()->v_lname!=""){
            $buyername .= ' '.auth()->guard('web')->user()->v_lname;
        }

        $data = CoursesModel::find($id);
        $orderdata = OrderModal::find($orderid);

        if(!count($data)){
            return 0;
        }

        if(!count($orderdata)){
            return 0;
        }

        $orderstr="";
        $orderstr.="<p>Order Successfully Placed.</p>";
        $orderstr.="<p>We are Pleased to confirm your order no ".$orderdata->i_order_no."</p>";
       
        $orderstr.="<table style='width:100%;border-spacing: 0px;' border=1>";
        $orderstr.="<tr>
                    <th><b>Title</b></th>
                    <th><b>Price</b></th>
                    <th><b>Total</b></th>
                    </tr>";
       $orderstr.="<tr>
                    <td>".$data->v_title."</td>
                    <td>Free</td>
                    <td>Free</td>
                   </tr>"; 
        $orderstr.="<tr>
                    <td></td>
                    <td>Total</td>
                    <td>Free</td>
                   </tr>";                      
        $orderstr.="</table>";

        $data=array(
            'name'=>$buyername,
            'COURSE_ORDER_DETAIL'=>$orderstr,
        );
        $mailcontent = EmailtemplateHelper::OrderconformationCourseBuyer($data);
        $subject = "Order Placed";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bd691d876fbae16740f8642");
        
        $mailids=array(
            $buyername=>$v_email,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

    public function sendWelcomeCourseNotification($coursedata=array()){
        
        if(!count($coursedata)){
            return 0;
        }
        
        $v_email = auth()->guard('web')->user()->v_email;
        $username="";
        if(isset(auth()->guard('web')->user()->v_fname) && auth()->guard('web')->user()->v_fname!=""){
            $username = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname) && auth()->guard('web')->user()->v_lname!=""){
            $username .= ' '.auth()->guard('web')->user()->v_lname;
        }

        $message = $coursedata->messages['welcome_message'].'<br/>';
        // if(isset($coursedata->messages['welcome_congratulations']) && $coursedata->messages['welcome_congratulations']!=""){
        //     $message .= $coursedata->messages['welcome_congratulations'].'<br><br>';    
        // }

        $data=array(
           'COURSE_MESSAGE'=>$message,
           'USER_FULL_NAME'=>$username,
        );
        
        $mailcontent = EmailtemplateHelper::CourseWelcomeMessage($data);
        $subject = "Course Order";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5ba8771f76fbae10255546f3");
        
        $mailids=array(
            $username=>$v_email,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

    }


}