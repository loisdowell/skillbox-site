<?php
namespace App\Http\Controllers\Frontend\Cart;

use Request, Hash, Lang,Validator,Auth,Storage,Session,Cart;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;
use App\Models\Courses\BuyerCourse as BuyerCourse;
use App\Models\OrderRequirements;


class SkillPayment extends Controller {
		
	protected $section;

	public function __construct(\MangoPay\MangoPayApi $mangopay){
		$this->section = Lang::get('section.Skill Payment');
        $this->mangopay = $mangopay;
	}

    public function paymentWithMangoPay(){

        if(sizeof(Cart::content())<=0){
            return redirect('cart/course');
        }
       
        $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION');
        if($comission==""){
            $comission=10;
        }
        $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE');
        if($processing==""){
            $processing=0.60;
        }
        $itemdata=array();
        
        $totalamt = 0;
        $i_seller_profile_id="";
        $package = "";
        foreach(Cart::content() as $item){
            $itemdata[] =array(
                'id'=>$item->id,
                'name'=>$item->name,
                'price'=>$item->price,
                'qty'=>$item->qty,
                'i_seller_id'=>$item->options->i_seller_id,
                'package'=>$item->options->package,
            );
            $totalamt = $totalamt+($item->price*$item->qty);
            $i_seller_profile_id = $item->options->i_seller_profile_id;
            $package= $item->options->package;

        }

        $comissionfees = $totalamt*$comission/100;
        $comissionfees = number_format($comissionfees,2);
        $processingamt = number_format($processing,2);
        $mangototalamt = ($totalamt+$comissionfees+$processingamt)*100;

        $currency = "GBP";
        //$creditedwalletid=52542003;    
        $authorId = "";

        $creditedwalletid = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
        if($creditedwalletid !=""){
            $creditedwalletid = (int)$creditedwalletid;  
        }else{
            $creditedwalletid=55569248;  
        }

        $sellerprofile = SellerprofileModel::find($i_seller_profile_id);

        if(isset(auth()->guard('web')->user()->i_mangopay_id)){
            $authorId = auth()->guard('web')->user()->i_mangopay_id;
        }
        $userid = auth()->guard('web')->user()->id;

        
        $PayIn = new \MangoPay\PayIn();
        $PayIn->CreditedWalletId = $creditedwalletid;
        $PayIn->AuthorId = (int)$authorId;
        $PayIn->PaymentType = "CARD";
        $PayIn->Tag = "Skillbox Skill charge";
        $PayIn->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
        $PayIn->PaymentDetails->CardType = "CB_VISA_MASTERCARD";
        $PayIn->DebitedFunds = new \MangoPay\Money();
        $PayIn->DebitedFunds->Currency = $currency;
        $PayIn->DebitedFunds->Amount = (float)$mangototalamt;
       
        $PayIn->Fees = new \MangoPay\Money();
        $PayIn->Fees->Currency = $currency;
        $PayIn->Fees->Amount = 0;

        $PayIn->ExecutionType = "WEB";
        $PayIn->ExecutionDetails = new \MangoPay\PayInExecutionDetailsWeb();
        $PayIn->ExecutionDetails->ReturnURL = url('cart/skill/mangopay-return');
        $PayIn->ExecutionDetails->Culture = "EN";
        $result = $this->mangopay->PayIns->Create($PayIn);
        
        if(!count($result)){
            return redirect('/')->with('warning', 'Something went wrong.');
        }   

        if(isset($sellerprofile->e_sponsor_status) && $sellerprofile->e_sponsor_status=="start"){
            $insert['v_sponser_sales']="yes";
        }

        $insert['i_user_id']=$userid;
        $insert['i_seller_id']=$sellerprofile->i_user_id;
        $insert['i_seller_profile_id']=$sellerprofile->id;
        $insert['v_profile_title']=$sellerprofile->v_profile_title;
        $insert['i_order_no']=GeneralHelper::OrderRefNumbaer();
        $insert['e_transaction_type']="skill";
        $insert['e_payment_type']="mangopay";
        $insert['v_transcation_id']=$result->Id;
        $insert['e_payment_status']="pending";

        $insert['v_buyer_commission']=$comissionfees;
        $insert['v_buyer_processing_fees']=$processingamt;   
        $insert['v_amount']=$totalamt;
        $insert['v_order_start']="no";
        $insert['v_order_requirements']="no";
        $insert['v_order_detail'] = $itemdata;
        $insert['v_processing_fees']= 0;//$processingamt;
        $insert['v_order_status']="inactive";
        $insert['e_seller_payment_status']="pending";
        $insert['v_duration_time'] = "";
        $insert['v_duration_in'] = "";
        
        if(count($sellerprofile)){
            $insert['v_duration_time']=$sellerprofile->information[$package]['v_delivery_time'];
            $insert['v_duration_in']=$sellerprofile->information[$package]['v_delivery_type'];    
        }
        
        $insert['d_date']=date("Y-m-d");
        $insert['d_added']=date("Y-m-d H:i:s");
        $insert['d_modified']=date("Y-m-d H:i:s");

        OrderModal::create($insert);
        header('Location: '.$result->ExecutionDetails->RedirectURL);
        exit;
    }
        
    public function getMangoPaymentReturn(){

        $response = Request::all();
       
        if(!isset($response['transactionId'])){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }
        if($response['transactionId']==""){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }

        $orderdata = OrderModal::where('v_transcation_id',$response['transactionId'])->first();
        if(!count($orderdata)){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }

        $result = $this->mangopay->PayIns->Get($response['transactionId']);

        if(!count($result)){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }

         $thankspage = array(
            'rurl'=>url('buyer/orders/active'),
            'issuccess'=>0,
        );

        if($result->Status=="SUCCEEDED"){

            $sellerprofile = SellerprofileModel::find($orderdata->i_seller_profile_id);

            $updateorder['v_order_requirements']="no";

            $sellerReq="";

            if(count($sellerprofile) && isset($sellerprofile->v_order_requirements) && $sellerprofile->v_order_requirements!=""){
                $insertreq[]=array(
                    'e_type'=>"requirements",
                    'i_user_id'=>$sellerprofile->i_user_id,
                    'v_order_id'=>$orderdata->id,
                    'l_message'=>$sellerprofile->v_order_requirements,
                    'v_document'=>"",
                    'v_main_req'=>"",
                    'd_added'=>date("Y-m-d H:i:s"),
                    'd_modified'=>date("Y-m-d H:i:s"),
                );
                OrderRequirements::insert($insertreq);
                $updateorder['v_order_requirements']="yes";
                $sellerReq = $sellerprofile->v_order_requirements;
            }else{
                $updateorder['v_order_start']="yes";
                $updateorder['d_order_start_date']=date("Y-m-d H:i:s");
            } 
           
            self::sendEmailNotificationSeller($orderdata);
            self::sendEmailNotificationBuyer($orderdata,$sellerReq);

            if(isset($sellerprofile->v_order_requirements) && $sellerprofile->v_order_requirements!=""){
                $updateorder['v_order_start']="no";
            }else{
                $updateorder['v_order_start']="yes";
            }

            $updateorder['v_order_status']="active";
            $updateorder['e_payment_status']="success";
            $updateorder['d_modified']=date("Y-m-d H:i:s");
            OrderModal::find($orderdata->id)->update($updateorder);

            $thankspage['issuccess']=1;

        }
        $thankspage['page']="order";
        Cart::destroy();
        Session::put('thankspage',$thankspage);
        return redirect('thanks');
        // Cart::destroy();
        // return redirect('buyer/orders/active');
    }


    public function sendEmailNotificationSeller($order=""){

        //$order = OrderModal::find($orderid);

        if(!count($order)){
            return 0;
        }

        $mailData['USER_FULL_NAME']="";
        $mailData['USER_EMAIL']="";
        $mailData['ORDER_NUMBER']=$order->i_order_no;
        $mailData['SELLER_ORDER_LINK']=url('seller/orders/active');

        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_fname)){
            $mailData['USER_FULL_NAME']=$order->hasUserSeller()->v_fname;
        }
        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_lname)){
            $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$order->hasUserSeller()->v_lname;
        }
        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_email)){
            $mailData['USER_EMAIL']= $order->hasUserSeller()->v_email;
        }

        $orderstr="";
        $orderstr.="<table style='width:100%;border-spacing: 0px;' border=1>";
        $orderstr.="<tr>
                    <th><b>TITLE</b></th>
                    <th style='text-align: center;'><b>QUANTITY</b></th>
                    <th style='text-align: center;'><b>PRICE</b></th>
                    </tr>";

        foreach($order->v_order_detail as $k=>$v){
            $price = $v['price']*$v['qty'];
            $orderstr.="<tr>
                    <td>".$v['name']."</td>
                    <td style='text-align: center;'>".$v['qty']."</td>
                    <td style='text-align: center;'>£".$price."</td>
                   </tr>"; 
        }    
        $total=  $order->v_amount;
        $orderstr.="<tr>
                    <td><b>Total</b></td>
                    <td></td>
                    <td style='text-align: center;'>£".number_format($total,2)."</td>
                   </tr>";      
        $orderstr.="</table>";
        $mailData['ORDER_MESSAGE']=$orderstr;
        
        $mailcontent = EmailtemplateHelper::OrderPlacedSellerNotification($mailData);
        $subject = EmailtemplateHelper::EmailTemplateSubject("5b575807516b460db00053ed");
        if($subject==""){
            $subject = "Buyer placed order";
        }
        $mailids=array(
            $mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
        return 1;
    }


    public function sendEmailNotificationBuyer($order="",$sellerreq=""){

        //$order = OrderModal::find($orderid);
        if(!count($order)){
            return 0;
        }

        $buyeremail="";
        if(count($order->hasUser()) && isset($order->hasUser()->v_email)){
            $buyeremail = $order->hasUser()->v_email;
        }

        $mailData['USER_FULL_NAME']="";
        $mailData['ORDER_NUMBER']=$order->i_order_no;
        $mailData['BUYER_ORDER_LINK']=url('buyer/orders/active');

        if(count($order->hasUser()) && isset($order->hasUser()->v_fname)){
            $mailData['USER_FULL_NAME'] = $order->hasUser()->v_fname;
        }
        if(count($order->hasUser()) && isset($order->hasUser()->v_lname)){
            $mailData['USER_FULL_NAME'] = $mailData['USER_FULL_NAME'].' '.$order->hasUser()->v_lname;
        }

        $orderstr="";
        $orderstr.="<table style='width:100%;border-spacing: 0px;' border=1>";
        $orderstr.="<tr>
                    <th><b>TITLE</b></th>
                    <th style='text-align: center;'><b>QUANTITY</b></th>
                    <th style='text-align: center;'><b>PRICE</b></th>
                    </tr>";

        foreach($order->v_order_detail as $k=>$v){
            $price = $v['price']*$v['qty'];
            $orderstr.="<tr>
                    <td>".$v['name']."</td>
                    <td style='text-align: center;'>".$v['qty']."</td>
                    <td style='text-align: center;'>£".$price."</td>
                   </tr>"; 
        }    
        $orderstr.="<tr>
                    <td><b>Skillbox commission</b></td>
                    <td></td>
                    <td style='text-align: center;'>£".$order->v_buyer_commission."</td>
                   </tr>";
        $orderstr.="<tr>
                    <td><b>Processing fees</b></td>
                    <td></td>
                    <td style='text-align: center;'>£".$order->v_buyer_processing_fees."</td>
                   </tr>"; 
        $total=  $order->v_buyer_processing_fees+$order->v_buyer_commission+$order->v_amount;

        $orderstr.="<tr>
                    <td><b>Total</b></td>
                    <td></td>
                    <td style='text-align: center;'>£".number_format($total,2)."</td>
                   </tr>";      
        $orderstr.="</table>";
        
        if($sellerreq!=""){
            $orderstr.="<br><b>Please find below Seller requirements</b><br>";
            $orderstr.=$sellerreq;
        }
        $mailData['ORDER_MESSAGE']=$orderstr;

        $mailcontent = EmailtemplateHelper::SkillOrderPlacedBuyerNotification($mailData);
        //$subject = "Order Placed";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bb5f25176fbae617d680c42");

        $mailids=array(
            $mailData['USER_FULL_NAME']=>$buyeremail,
        );
       
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
        return 1;
    }



   
}