<?php
namespace App\Http\Controllers\Frontend\Cart;

use Request, Hash, Lang,Validator,Auth,Storage,Session,Cart;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;
use App\Models\Courses\BuyerCourse as BuyerCourse;    


class Payment extends Controller {
		
	protected $section;
    
    public function __construct(\MangoPay\MangoPayApi $mangopay){
		$this->section = Lang::get('section.Payment');
        $this->mangopay = $mangopay;
	}

    public function paymentWithMangoPay(){


        if(sizeof(Cart::content())<=0){
            return redirect('cart/course');
        }

        $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION');
        if($comission==""){
            $comission=10;
        }
        $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE');
        if($processing==""){
            $processing=0.60;
        }


        $itemdata=array();
        $totalamt = 0;
        $course_id ="";
        foreach(Cart::content() as $item){
            
            $itemdata[] =array(
                'id'=>$item->id,
                'name'=>$item->name,
                'price'=>$item->price,
            );
            $totalamt = $totalamt + ($item->price*$item->qty);
            $course_id = $item->id;

        }

        $coursedata = CoursesModel::find($course_id);

        if(!count($coursedata)){
            return redirect('cart/course');
        }   

        $comissionfees = $totalamt*$comission/100;
        $comissionfees = number_format($comissionfees,2);
        $processingamt = number_format($processing,2);
        $mangototalamt = ($totalamt+$comissionfees+$processingamt)*100;

        $currency = "GBP";
        
        //$creditedwalletid=52542003;  
        //$creditedwalletid=55569248;  
        
        $creditedwalletid = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
        if($creditedwalletid!=""){
            $creditedwalletid = (int)$creditedwalletid;  
        }else{
            $creditedwalletid=55569248;  
        }

        // $AuthorId = GeneralHelper::getSiteSetting('MANGOPAY_USERID');
        // if($AuthorId==""){
        //     $AuthorId = (int)$AuthorId;  
        // }else{
        //     $AuthorId=55569242;  
        // }


        // $AuthorId=55569242;
        // $DebitedWalletID=55569248;
        
        $authorId = "";
        if(isset(auth()->guard('web')->user()->i_mangopay_id)){
            $authorId = auth()->guard('web')->user()->i_mangopay_id;
        }
        $userid = auth()->guard('web')->user()->id;

        $PayIn = new \MangoPay\PayIn();
        $PayIn->CreditedWalletId = $creditedwalletid;
        $PayIn->AuthorId = (int)$authorId;
        $PayIn->PaymentType = "CARD";
        $PayIn->Tag = "Skillbox Course charge";
        $PayIn->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
        $PayIn->PaymentDetails->CardType = "CB_VISA_MASTERCARD";
        $PayIn->DebitedFunds = new \MangoPay\Money();
        $PayIn->DebitedFunds->Currency = $currency;
        $PayIn->DebitedFunds->Amount = (float)$mangototalamt;
            
        $PayIn->Fees = new \MangoPay\Money();
        $PayIn->Fees->Currency = "GBP";
        $PayIn->Fees->Amount = 0;

        $PayIn->ExecutionType = "WEB";
        $PayIn->ExecutionDetails = new \MangoPay\PayInExecutionDetailsWeb();
        $PayIn->ExecutionDetails->ReturnURL = url('cart/course/mangopay-return');
        $PayIn->ExecutionDetails->Culture = "EN";
        $result = $this->mangopay->PayIns->Create($PayIn);
        
        if(!count($result)){
            return redirect('/')->with('warning', 'Something went wrong.');
        }   

        if(isset($coursedata->e_sponsor_status) && $coursedata->e_sponsor_status=="start"){
            $insert['v_sponser_sales']="yes";
        }

        $insert['i_user_id']=$userid;
        $insert['i_seller_id']=$coursedata->i_user_id;
        $insert['i_course_id']=$coursedata->id;
        $insert['i_order_no']=GeneralHelper::OrderRefNumbaer();
        $insert['e_transaction_type']="course";
        $insert['e_payment_type']="mangopay";
        $insert['v_transcation_id']=$result->Id;
        $insert['e_payment_status']="pending";
        $insert['v_amount']=$totalamt;
        $insert['v_order_detail'] = $itemdata;
        
        $insert['v_buyer_commission']=$comissionfees;
        $insert['v_buyer_processing_fees']=$processingamt;   

        $insert['v_processing_fees'] = 0; //$processingamt;
        $insert['v_order_status']="inactive";
        $insert['e_seller_payment_status']="pending";
        $insert['d_date']=date("Y-m-d");
        $insert['d_added']=date("Y-m-d H:i:s");
        $insert['d_modified']=date("Y-m-d H:i:s");
        OrderModal::create($insert);
       
        if(isset(auth()->guard('web')->user()->v_buyer_online_service)){
            if(auth()->guard('web')->user()->v_buyer_online_service=="inactive"){
                $update['v_buyer_online_service']="active";
                UsersModel::find($userid)->update($update);
            }
        }else{
            $update['v_buyer_online_service']="active";
            UsersModel::find($userid)->update($update);
        }

        header('Location: '.$result->ExecutionDetails->RedirectURL);
        exit;
    }
        
    public function getMangoPaymentReturn(){

        $response = Request::all();

        if(!isset($response['transactionId'])){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }
        if($response['transactionId']==""){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }

        $orderdata = OrderModal::where('v_transcation_id',$response['transactionId'])->first();
        if(!count($orderdata)){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }

        $result = $this->mangopay->PayIns->Get($response['transactionId']);

        if(!count($result)){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }

        $thankspage = array(
            'rurl'=>url('buyer/courses'),
            'issuccess'=>0,
        );
        
        if($result->Status=="SUCCEEDED"){

            $cdata = CoursesModel::find($orderdata->i_course_id);
            $totallacture=0;


            if(isset($cdata->section) && count($cdata->section)){
                foreach ($cdata->section as $key => $value) {
                    $totallacture= $totallacture+count($value['video']);   
                }
            }
            
            $userdata = UsersModel::find($orderdata->i_seller_id);

            // $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER');
            // if($comission==""){
            //     $comission=10;
            // }
            // $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER');
            // if($processing==""){
            //     $processing=0.60;
            // }



            $creditselleramt=0;
            $comissionfees=0;
            $processingamt=0;

            $comission=10;
            $processing=0.60;
            
            if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b9f4d3e8124f123c986c"){

                $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_PREMIUM');
                if($comission==""){
                    $comission=10;
                }
                $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_PREMIUM');
                if($processing==""){
                    $processing=0.60;
                }


            }

            if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b757d3e8125e323c986a"){

                $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_STANDARD');
                if($comission==""){
                    $comission=10;
                }
                $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_STANDARD');
                if($processing==""){
                    $processing=0.60;
                }

            }

            if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b48cd3e812a4253c9869"){

                $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION_SELLER_BASIC');
                if($comission==""){
                    $comission=10;
                }
                $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE_SELLER_BASIC');
                if($processing==""){
                    $processing=0.60;
                }

            }


                
            //     $creditselleramt = $orderdata->v_amount;
            //     $comissionfees = $creditselleramt*5/100;
            //     $comissionfees = number_format($comissionfees,2);
            //     $processingamt = number_format($processing,2);
            //     $totalminus = $comissionfees+$processingamt;
            //     $creditselleramt = $creditselleramt-$totalminus;
            //     $creditselleramt = number_format($creditselleramt,2);
            // }else{
            //     $creditselleramt = $orderdata->v_amount;
            //     $comissionfees = $creditselleramt*$comission/100;
            //     $comissionfees = number_format($comissionfees,2);
            //     $processingamt = number_format($processing,2);
            //     $totalminus = $comissionfees+$processingamt;
            //     $creditselleramt = $creditselleramt-$totalminus;
            //     $creditselleramt = number_format($creditselleramt,2);
            // }

            $creditselleramt = $orderdata->v_amount;
            $comissionfees = $creditselleramt*$comission/100;
            $comissionfees = number_format($comissionfees,2);
            $processingamt = number_format($processing,2);
            $totalminus = $comissionfees+$processingamt;
            $creditselleramt = $creditselleramt-$totalminus;
            $creditselleramt = number_format($creditselleramt,2);


            $mangototalamt = $creditselleramt*100;

            //$AuthorId=52541934;
            //$DebitedWalletID=52542003;
            //$AuthorId=55569242;
            //$DebitedWalletID=55569248;


            $DebitedWalletID = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
            if($DebitedWalletID!=""){
                $DebitedWalletID = (int)$DebitedWalletID;  
            }else{
                $DebitedWalletID=55569248;  
            }

            $AuthorId = GeneralHelper::getSiteSetting('MANGOPAY_USERID');
            if($AuthorId!=""){
                $AuthorId = (int)$AuthorId;  
            }else{
                $AuthorId=55569242;  
            }

            $CreditedWalletId = $userdata->i_wallet_id;

            $Transfer = new \MangoPay\Transfer();
            $Transfer->AuthorId = $AuthorId;
            $Transfer->Tag = "Payment seller for Course";
            $Transfer->DebitedFunds = new \MangoPay\Money();
            $Transfer->DebitedFunds->Currency = "GBP";
            $Transfer->DebitedFunds->Amount = (float)$mangototalamt;
            $Transfer->Fees = new \MangoPay\Money();
            $Transfer->Fees->Currency = "GBP";
            $Transfer->Fees->Amount = 0;
            $Transfer->DebitedWalletID = $DebitedWalletID;
            $Transfer->CreditedWalletId = (int)$CreditedWalletId;
            $result = $this->mangopay->Transfers->Create($Transfer);
            
            if(count($result)){
                if(isset($result->Status) && $result->Status=="SUCCEEDED"){
                    $updateorder['e_seller_payment_status']="success";
                    $updateorder['e_seller_payment_amount']=$creditselleramt;
                    $updateorder['e_seller_connection_fee']=$processingamt; 
                    
                    $updateorder['v_seller_commission']=$comissionfees;
                    $updateorder['v_seller_processing_fees']=$processingamt;   

                }
            }
            
            $insertbuyer[]=array(
                'i_course_id'=>$cdata->id,
                'i_user_id'=>$orderdata->i_user_id,
                'v_order_id'=>$orderdata->id,
                'e_payment_type'=>"mangopay",
                'v_transcation_id'=>$orderdata->v_transcation_id,
                'e_payment_status'=>"success",
                'e_status'=>"not_started",
                'v_total_lectures'=>$totallacture,
                'v_complete_lectures'=>"0",
                'v_complete_ids'=>array(),
                'd_added'=>date("Y-m-d H:i:s"),
                'd_modified'=>date("Y-m-d H:i:s"),
            );
            BuyerCourse::insert($insertbuyer);
            self::sendEmailNotificationSeller($cdata->id,$orderdata->id);
            self::sendEmailNotificationBuyer($cdata->id,$orderdata->id);

            if(isset($cdata->messages) && count($cdata->messages)){
                if(isset($cdata->messages['welcome_message']) && $cdata->messages['welcome_message']!=""){
                    self::sendWelcomeCourseNotification($cdata);    
                }
                
            }
            $updateorder['v_order_status']="active";
            $updateorder['e_payment_status']="success";
            $updateorder['d_modified']=date("Y-m-d H:i:s");
            OrderModal::find($orderdata->id)->update($updateorder);
            $thankspage['issuccess']=1;
        }
        $thankspage['page']="order";
        Cart::destroy();
        Session::put('thankspage',$thankspage);
        return redirect('thanks');
        //return redirect('buyer/courses');
    }

    public function sendWelcomeCourseNotification($coursedata=array()){
        
        if(!count($coursedata)){
            return 0;
        }
        
        $v_email = auth()->guard('web')->user()->v_email;
        $username="";
        if(isset(auth()->guard('web')->user()->v_fname) && auth()->guard('web')->user()->v_fname!=""){
            $username = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname) && auth()->guard('web')->user()->v_lname!=""){
            $username .= ' '.auth()->guard('web')->user()->v_lname;
        }
        $message = $coursedata->messages['welcome_message'].'<br/>';
        // if(isset($coursedata->messages['welcome_congratulations']) && $coursedata->messages['welcome_congratulations']!=""){
        //     $message .= $coursedata->messages['welcome_congratulations'].'<br><br>';    
        // }
        $data=array(
           'COURSE_MESSAGE'=>$message,
           'USER_FULL_NAME'=>$username,
        );
        $mailcontent = EmailtemplateHelper::CourseWelcomeMessage($data);
        $subject = "Course Order";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5ba8771f76fbae10255546f3");
        $mailids=array(
            $username=>$v_email,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

    }

    
    /*
    public function coursePaypal() {

        if(sizeof(Cart::content())<=0){
            return redirect('cart/course');
        }

        $itemdata=array();
        $totalamt = 0;
        foreach(Cart::content() as $item){
            $itemdata[] =array(
                'id'=>$item->id,
                'name'=>$item->name,
                'price'=>$item->price,
            );
            $totalamt = $totalamt + ($item->price*$item->qty);
        }

        $processingamt = number_format($totalamt*5/100);

        $itemdata[]=array(
            'id'=>"0",
            'name'=>"PROCESSING FEE",
            'price'=>$processingamt,
            'qty'=>1,
        );
        dd($itemdata);

        
        $currency = "EUR";
        $paypalid = GeneralHelper::getSiteSetting("PAYPAL_EMIAL");
        $paypalmode = GeneralHelper::getSiteSetting("PAYPAL_MODE");

        $paypalurl="https://www.sandbox.paypal.com/cgi-bin/webscr";
        if($paypalmode=="live"){
            $paypalurl="https://www.paypal.com/cgi-bin/webscr";
        }else{
            $paypalurl="https://www.sandbox.paypal.com/cgi-bin/webscr";
        }
        $userid = auth()->guard('web')->user()->id;
        $custom = 'no';
       
        $_data=array(
            'paypalURL' =>$paypalurl,
            'paypalID'  => $paypalid,
            'itemdata'  => $itemdata,
            'custom' => $custom,
            'currency_code' => $currency,
            'cancel_return' => url('cart/course/paypal-cancel'),
            'return_url' => url('cart/course/paypal-return'),
            'cmd' => '_xclick',
        );
        return view('frontend/Cart/course-paypal-payment',$_data);
    }
    public function getPaypalReturn(){
        
        $response = Request::all();

        if(!count($response)){
          return redirect('/')->with( 'warning', 'Something went wrong.');
        }

        $oldpaymentdata = OrderModal::where("v_transcation_id",$response['txn_id'])->first();

        if(!count($oldpaymentdata)){

            $userid = auth()->guard('web')->user()->_id;
            $insert = array();

            $insert['i_order_no']=GeneralHelper::OrderRefNumbaer();
            $insert['e_transaction_type']="course";
            $insert['e_payment_type']="paypal";
            $insert['v_transcation_id']=$response['txn_id'];
            
            if($response['payment_status']=="Pending"){
                $insert['e_payment_status']="pending";
            }else if($response['payment_status']=="Completed" || $response['payment_status']=="Processed"){
                $insert['e_payment_status']="success";
            }else{
                $insert['e_payment_status']=$response['payment_status'];
            }

            $insert['v_amount']=$response['payment_gross'];
            $insert['v_order_detail']=array();

            $insertbuyer=array();
            $userid = auth()->guard('web')->user()->_id;
            
            $totalamt = 0;
            $cnt=1;
            
            $course_id=0;
            foreach(Cart::content() as $item){
                if($cnt==1){
                    $course_id = $item->id;
                }
                $insert['v_order_detail'][] =array(
                    'id'=>$item->id,
                    'name'=>$item->name,
                    'price'=>$item->price,
                );
                $totalamt = $totalamt + ($item->price*$item->qty);
                $cnt = $cnt+1;
            }

            $cdata = CoursesModel::find($course_id);


            $processingamt = number_format($totalamt*5/100);
            $insert['v_processing_fees']=$processingamt;
            $insert['v_order_status']="active";
            $insert['i_user_id']=$userid;
            $insert['i_seller_id']=$cdata->i_user_id;
            $insert['e_seller_payment_status']="pending";

            $insert['d_date']=date("Y-m-d");
            $insert['d_added']=date("Y-m-d H:i:s");
            $insert['d_modified']=date("Y-m-d H:i:s");
            

            $orderid = OrderModal::create($insert)->id;
            $totallacture=0;

            if(isset($cdata->section) && count($cdata->section)){
                foreach ($cdata->section as $key => $value) {
                    if(isset($value['video']['v_title'])){
                        $totallacture= $totallacture+count($value['video']['v_title']);     
                    }
                    if(isset($value['doc']['v_title'])){
                        $totallacture= $totallacture+count($value['doc']['v_title']);         
                    }
                }
            }

            foreach(Cart::content() as $item){
                $insertbuyer[]=array(
                    'i_course_id'=>$item->id,
                    'i_user_id'=>$userid,
                    'v_order_id'=>$orderid,
                    'e_payment_type'=>"paypal",
                    'v_transcation_id'=>$response['txn_id'],
                    'e_payment_status'=>$insert['e_payment_status'],
                    'e_status'=>"not_started",
                    'v_total_lectures'=>$totallacture,
                    'v_complete_lectures'=>"0",
                    'v_complete_ids'=>array(),
                    'd_added'=>date("Y-m-d H:i:s"),
                    'd_modified'=>date("Y-m-d H:i:s"),
                );
            }

            if(count($insertbuyer)){
                BuyerCourse::insert($insertbuyer);
            }
            if($response['payment_status']=="Completed" || $response['payment_status']=="Processed"){
                self::sendEmailNotificationSeller($item->id,$orderid);
            } 
            

        }else{

            $updateorder=array();

            if($response['payment_status']=="Pending"){
                $updateorder['e_payment_status']="pending";
            }else if($response['payment_status']=="Completed" || $response['payment_status']=="Processed"){
                $updateorder['e_payment_status']="success";
            }else{
                $updateorder['e_payment_status']=$response['payment_status'];
            }
            $updateorder['d_modified']=date("Y-m-d H:i:s");
            OrderModal::where('v_transcation_id',$response['txn_id'])->update($updateorder);    

            if($response['payment_status']=="Completed" || $response['payment_status']=="Processed"){
                
                $updatebuyer['e_payment_status']="success";
                $updatebuyer['d_modified']=date("Y-m-d H:i:s");
                BuyerCourse::where('v_transcation_id',$response['txn_id'])->update($updatebuyer);
            }
            if($response['payment_status']=="Completed" || $response['payment_status']=="Processed"){
                self::sendEmailNotificationSeller($item->id,$orderid);
            }
        }
        Cart::destroy();
        return redirect('buyer/courses');
    }
    
    public function getPaypalCancel(){
        dd("Payment cancel by buyer");
    }
    */

    public function sendEmailNotificationSeller($id="",$orderid=""){
        
        if($id==""){
            return 0;
        }
        if($orderid==""){
            return 0;
        }

        $data = CoursesModel::find($id);
        $orderdata = OrderModal::find($orderid);

        if(!count($data)){
            return 0;
        }

        if(!count($orderdata)){
            return 0;
        }

        $buyername="";
        if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_fname)){
            $buyername = $orderdata->hasUser()->v_fname;
        }
        if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_lname)){
            $buyername .= ' '.$orderdata->hasUser()->v_lname;
        }

        $sellername="";
        if(count($data->hasUser()) && isset($data->hasUser()->v_fname)){
            $sellername = $data->hasUser()->v_fname;
        }
        if(count($data->hasUser()) && isset($data->hasUser()->v_lname)){
            $sellername .= ' '.$data->hasUser()->v_lname;
        }

        $selleremail="";
        if(count($data->hasUser()) && isset($data->hasUser()->v_email)){
            $selleremail = $data->hasUser()->v_email;
        }

        $orderstr="";
        $orderstr.="<p>Order Successfully Placed.</p>";
        $orderstr.="<p>We are Pleased to confirm your order no ".$orderdata->i_order_no."</p>";
        if($buyername!=""){
            $orderstr.="<br><p>Buyer Name : ".$buyername."</p>";
        }

        $orderstr.="<table style='width:100%;border-spacing: 0px;' border=1 >";
        $orderstr.="<tr>
                    <th><b>Title</b></th>
                    <th style='text-align: center;'><b>Price</b></th>
                    <th style='text-align: center;'><b>Total</b></th>
                    </tr>";
       $orderstr.="<tr>
                    <td>".$data->v_title."</td>
                    <td style='text-align: center;'>".$data->f_price."</td>
                    <td style='text-align: center;'>".$data->f_price."</td>
                   </tr>"; 
        $orderstr.="<tr>
                    <td><b>Total</b></td>
                    <td></td>
                    <td style='text-align: center;'>".$data->f_price."</td>
                   </tr>";                      
        $orderstr.="</table>";

        // $orderstr="";
        // $orderstr.="<table style='width:100%'>";
        // $orderstr.="<tr><th colspan='2'><b>Order detail</b></th></tr>";
        // $orderstr.="<tr><td>Course Name</td>";
        // $orderstr.="<td>".$data->v_title."</td></tr>";
        // $orderstr.="<tr><td>Course Price</td>";
        // $orderstr.="<td>".$data->f_price."</td></tr>";
        // $orderstr.="<tr><td>Order Number</td>";
        // $orderstr.="<td>".$orderdata->i_order_no."</td></tr>";
        // if($buyername!=""){
        //     $orderstr.="<tr><td>Buyer Name</td>";
        //     $orderstr.="<td>".$buyername."</td></tr>";
        // }
        // $orderstr.="<table>";


        $data=array(
            'name'=>$sellername,
            'COURSE_ORDER_DETAIL'=>$orderstr,
        );
        
        $mailcontent = EmailtemplateHelper::OrderconformationSeller($data);
        $subject = "Order Placed";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5b0e89fc76fbae6964350442");
        
        $mailids=array(
            $sellername=>$selleremail,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

    public function sendEmailNotificationBuyer($id="",$orderid=""){
        
        if($id==""){
            return 0;
        }
        if($orderid==""){
            return 0;
        }

        $v_email = auth()->guard('web')->user()->v_email;
        $buyername="";
        if(isset(auth()->guard('web')->user()->v_fname) && auth()->guard('web')->user()->v_fname!=""){
            $buyername = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname) && auth()->guard('web')->user()->v_lname!=""){
            $buyername .= ' '.auth()->guard('web')->user()->v_lname;
        }

        $data = CoursesModel::find($id);
        $orderdata = OrderModal::find($orderid);

        if(!count($data)){
            return 0;
        }

        if(!count($orderdata)){
            return 0;
        }

        $orderstr="";
        $orderstr.="<p>Order Successfully Placed.</p>";
        $orderstr.="<p>We are Pleased to confirm your order no ".$orderdata->i_order_no."</p>";
       
        $orderstr.="<table style='width:100%;border-spacing: 0px;' border=1 >";
        $orderstr.="<tr>
                    <th><b>Title</b></th>
                    <th style='text-align: center;'><b>Price</b></th>
                    <th style='text-align: center;'><b>Total</b></th>
                    </tr>";
       $orderstr.="<tr>
                    <td>".$data->v_title."</td>
                    <td style='text-align: center;'>".$data->f_price."</td>
                    <td style='text-align: center;'>".$data->f_price."</td>
                   </tr>"; 
        $orderstr.="<tr>
                    <td><b>Skillbox commission</b></td>
                    <td></td>
                    <td style='text-align: center;'>".$orderdata->v_buyer_commission."</td>
                   </tr>";
        $orderstr.="<tr>
                    <td><b>Processing fees</b></td>
                    <td></td>
                    <td style='text-align: center;'>".$orderdata->v_buyer_processing_fees."</td>
                   </tr>";
        $total =   $orderdata->v_buyer_commission+$orderdata->v_buyer_processing_fees+$data->f_price;
        $total = number_format($total,2);

        $orderstr.="<tr>
                    <td><b>Total</b></td>
                    <td></td>
                    <td style='text-align: center;'>".$total."</td>
                   </tr>";                      
        $orderstr.="</table>";
        $data=array(
            'name'=>$buyername,
            'COURSE_ORDER_DETAIL'=>$orderstr,
        );
        $mailcontent = EmailtemplateHelper::OrderconformationCourseBuyer($data);
        $subject = "Order Placed";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bd691d876fbae16740f8642");
        $mailids=array(
            $buyername=>$v_email,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }
    


}