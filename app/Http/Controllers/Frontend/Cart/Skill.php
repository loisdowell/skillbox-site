<?php
namespace App\Http\Controllers\Frontend\Cart;

use Request, Hash, Lang,Validator,Auth,Storage,Cart;

use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
//use MongoDB\BSON\ObjectId;


class Skill extends Controller {
		
	protected $section;
	public function __construct(){
		$this->section = Lang::get('section.skill cart');
	}
	public function index() {
	 	
	 	if(!auth()->guard('web')->check()) {
            return redirect('login');
        }

	 	$i_seller_profile_id ="";
	 	foreach (Cart::content() as $value) {
	 		$i_seller_profile_id = $value->options->i_seller_profile_id;	
	 	}
	 	$sellerprofile = SellerprofileModel::find($i_seller_profile_id);
	 	
	 	$userid = auth()->guard('web')->user()->id;
	 	if(isset($sellerprofile->v_service) && $sellerprofile->v_service=="online"){

	 		if(isset(auth()->guard('web')->user()->v_buyer_online_service)){
	            if(auth()->guard('web')->user()->v_buyer_online_service=="inactive"){
	                $update['v_buyer_online_service']="active";
	                UsersModel::find($userid)->update($update);
	            }
	        }else{
	            $update['v_buyer_online_service']="active";
	            UsersModel::find($userid)->update($update);
	        }	

	 	}else{

	 		if(isset(auth()->guard('web')->user()->v_buyer_inperson_service)){
	            if(auth()->guard('web')->user()->v_buyer_inperson_service=="inactive"){
	                $update['v_buyer_inperson_service']="active";
	                UsersModel::find($userid)->update($update);
	            }
	        }else{
	            $update['v_buyer_inperson_service']="active";
	            UsersModel::find($userid)->update($update);
	        }
	    }

	    $comission = GeneralHelper::getSiteSetting('SITE_COMMISSION');
	    if($comission==""){
	    	$comission=10;
	    }
	    $processing = GeneralHelper::getSiteSetting('SITE_PROCESSING_FEE');
	    if($processing==""){
	    	$processing=0.60;
	    }

	    $_data=array(
	    	'comission'=>$comission,
	    	'processing'=>$processing,
	    );
	    return view('frontend/Cart/skill',$_data);
	}

	public function removeRawItem($id=""){
		
		$cardremove=0;	
		foreach(Cart::content() as $value) {
	 		if($value->rowId==$id){
	 			if($value->options->typedata=="skill"){
	 				$cardremove=1;			
	 			}
	 		}	
	 	}

	 	if($cardremove){
	 		Cart::destroy();
	 	}else{
	 		Cart::remove($id);	
	 	}
	 	return redirect('cart/skill');
	}
	
	public function updateCart(){

		$data = Request::all();

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		foreach ($data as $key => $value) {
			Cart::update($key, $value);
		}
		return redirect('cart/skill');
	}


}