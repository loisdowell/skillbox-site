<?php
namespace App\Http\Controllers\Frontend\Login;

use Request, Hash,Lang,Socialite,Auth,Session,Redirect,Storage,File;
//use Request, Redirect, Auth, Socialite, Config, Session, Hash;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\RegistersUsers;

class SocialAccounts extends Controller {

	protected $section;
	private $social_settings;
	
	public function __construct(\MangoPay\MangoPayApi $mangopay){
		$this->social_settings = array();
		$this->section = Lang::get('section.dashboard');
		$this->mangopay = $mangopay;
	}

	public function index() {
		return Socialite::driver('facebook')->redirect();
	}

	public function facebookCallback() {
		

		try {
			$user = Socialite::driver('facebook')->stateless()->user();
			$authdata = self::findOrCreateFacebookUser($user);
			Auth::guard('web')->login($authdata, true);
			return redirect('dashboard');	
		}
		catch (\Exception $e) {
			return redirect('login')->with(['error'=>$e]);
		}
	}

	public function findOrCreateFacebookUser($facebookUser) {

		$authUser = UsersModel::where('v_facebook_id', $facebookUser->id)->first();
		
		if(count($authUser)){
			
			$update=array();
			if(isset($facebookUser->name) && $facebookUser->name!=""){
				$split = explode(" ",isset($facebookUser->name) ? $facebookUser->name : '');	
				if(isset($split[0])){
					$update['v_fname']=$split[0];
				}
				if(isset($split[0])){
					$update['v_lname']=$split[1];
				}
			}
			$update['d_modified']=date("Y-m-d H:i:s");
			$authUser->update($update);
			return $authUser;
		
		}else{
			
			$update=array();
			if(isset($facebookUser->name) && $facebookUser->name!=""){
				$split = explode(" ",isset($facebookUser->name) ? $facebookUser->name : '');	
				if(isset($split[0])){
					$update['v_fname']=$split[0];
				}
				if(isset($split[0])){
					$update['v_lname']=$split[1];
				}

				$avatar = new \LasseRafn\InitialAvatarGenerator\InitialAvatar();
				$image_avatar = $avatar->name($facebookUser->name)
					->length(2)
					->fontSize(0.5)
					->size(250) 
					->background('#ddd')
					->color('#222')
					->generate()
					->stream('png', 100);

				$fileName = 'profile-'.time().'.png';
				$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, $image_avatar->__toString(), 'public');     
				$update['v_image']="users/".$fileName;
			}
			
			if(isset($facebookUser->email) && $facebookUser->email!=""){
				$authUser = UsersModel::where('v_email', $facebookUser->email)->first();
				if(count($authUser)){
					return $authUser;
				}
				$update['v_email'] = $facebookUser->email;

				$res = self::createMangopayUser($update);
				$update['i_mangopay_id']= $res['i_mangopay_id'];
				$update['i_wallet_id']= $res['i_wallet_id'];
			}
			
			$update['buyer']['v_service']="online";
			$update['buyer']['e_status']="active";
			$update['seller']['v_service']="online";
			$update['seller']['e_status']="inactive";
			$update['v_plan']['id']="5a65b48cd3e812a4253c9869";
			$update['v_plan']['duration']="monthly";
			$update['v_plan']['d_date']=date("Y-m-d H:i:s");
			$update['v_plan']['d_start_date']=date("Y-m-d H:i:s");
			$update['v_plan']['d_end_date']=date('Y-m-d',strtotime('+4000 days'));
			$update['v_buyer_online_service'] = "active";
			$update['v_buyer_inperson_service'] = "inactive";
			$update['v_seller_online_service'] = "inactive";
			$update['v_seller_inperson_service'] = "inactive";
			$update['v_level']="0";
			$update['v_replies_time']="24 hours";
			$update['i_total_avg_review']=0;
			$update['i_total_review']=0;
			$update['i_course_total_review']=0;
			$update['i_course_total_avg_review']=0;
			$update['i_job_total_review']=0;
			$update['i_job_total_avg_review']=0;
			$update['v_company_type']="INDIVIDUAL";
			$update['e_login']="yes";
			$update['v_facebook_id'] = $facebookUser->id;
			$update['i_newsletter'] = "on";
			$update['e_status'] = "active";
			$update['e_email_confirm'] = "yes";	
			$update['d_added'] = date('Y-m-d H:i:s');
			$update['d_modified'] = date('Y-m-d H:i:s');
			return UsersModel::create($update);

		}
		
	}


	public function createMangopayUser($data){
		
		$response['i_mangopay_id']=0;
		$response['i_wallet_id']=0;

		if(!isset($data['v_fname'])){
			$data['v_fname']="First name";	
		}
		if(!isset($data['v_lname'])){
			$data['v_fname']="Last name";	
		}

		$UserNatural = new \MangoPay\UserNatural();
		$UserNatural->FirstName = $data['v_fname'];//"First Name";
		$UserNatural->LastName = $data['v_lname'];
		$UserNatural->Birthday = 1463496101;
		$UserNatural->Nationality = "GB";
		$UserNatural->Email = $data['v_email'];
		$UserNatural->CountryOfResidence = "GB";
		$result = $this->mangopay->Users->Create($UserNatural);

		if(count($result)){
			
			$mangopayid = $result->Id;
			$Wallet = new \MangoPay\Wallet();
			$Wallet->Tag = "custom meta";
			$Wallet->Owners = array ($result->Id);
			$Wallet->Description = "My skillbox project";
			$Wallet->Currency = "GBP";
			$resultWallet = $this->mangopay->Wallets->Create($Wallet);
			$response['i_mangopay_id']=$result->Id;
			$response['i_wallet_id']=$resultWallet->Id;
		
		}
		return $response;
	}



}