<?php
namespace App\Http\Controllers\Frontend\Login;

use Request, Hash, Lang,Crypt,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Users as UsersModel;


use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequestException;
use Facebook\GraphObject;
use Facebook\GraphUser;

class Login extends Controller {

	protected $section;

	public function __construct(){
		$this->section = Lang::get('section.dashboard');
	}

	public function index() {
		// $rcart = Session::get('rcart');
		// dd($rcart);
		if(auth()->guard('web')->check()) {
			return redirect('dashboard');
		}
		return view('frontend/login/login');
	}

	public function postlogin(){
		
		$post_data = Request::all();
		$auth = auth()->guard('web');

		if($auth->attempt( Request::only('v_email', 'password' ))) {
			$rcart=array();
		 	if(Session::has('rcart')){
		 		$rcart = Session::get('rcart');	
		 		Session::forget('rcart');
		 	}
			if(isset($rcart['rurl']) && $rcart['rurl']!=""){
				return redirect($rcart['rurl']);					
			}	
			
			if(isset(auth()->guard('web')->user()->i_delete) && auth()->guard('web')->user()->i_delete=="1"){
				$auth = auth()->guard('web');
            	$auth->logout();
				return redirect('login')->with(['warning' => 'Invalid credentials.']);
			}
			$userid = auth()->guard('web')->user()->id;
			$update['e_login']="yes";
			UsersModel::find($userid)->update($update);
			return redirect('dashboard');
		}else{
			return redirect('login')->with(['warning' => 'Invalid credentials.']);	
		}	
	}

	public function forgotPassword(){
		return view('frontend/login/forgotPassword');
	}

	public function postForgotPassword(){

		$post_data = Request::all();
		$userdata = UsersModel::where('v_email',$post_data['v_email'])->first();

		if(count($userdata)){
			
			//$newPassword = GeneralHelper::generate_password();
			
			$username = "";
			if(isset($userdata->v_fname)){
			   	$username = $userdata->v_fname;
			}
			if(isset($userdata->v_lname)){
			   	$username .= " ".$userdata->v_lname;
			}

			$encryptedid = Crypt::encrypt($userdata->id);
			$passwordreseturl= url('reset-password').'/'.$encryptedid;

			$useremail="";
			if(isset($userdata->v_email)){
			   	$useremail .= " ".$userdata->v_email;
			}

			$data=array(
				'name'=>$username,
				'passwordreseturl'=>$passwordreseturl,
			);

			$mailcontent = EmailtemplateHelper::ForgotPassword($data);
			$subject = EmailtemplateHelper::EmailTemplateSubject("5a60a0f1d3e812bc4b3c986a");
			//$subject = "Forgot password";
			$mailids=array(
				$username=>$useremail
			);
			$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
			return redirect('login')->with(['success' => 'Email sent successfully for forgot password.']);

			// $data=array(
			// 	'name'=>$username,
			// 	'password'=>$newPassword,
			// );
			// $mailcontent = EmailtemplateHelper::ForgetPassword($data);
			// $subject = "Forgot password";
			// $mailids=array(
			// 	$username=>$useremail
			// );
			// $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
			
			// $update=array(
			// 	'password'=>Hash::make($newPassword),
			// 	'd_modified'=>date("Y-m-d H:i:s"),
			// );		
			// UsersModel::find($userdata->id)->update($update);

			// return redirect('login')->with(['success' => 'Please check your email for New password.']);

		}else{
			return redirect('forgot-password')->with(['warning' => 'Invalid email id.']);	
		}

	}

	public function resetPassword($id=""){
		

		if($id==""){
			return redirect('login')->with(['warning' => 'Something went wrong.']);
		}
		$userid = Crypt::decrypt($id);
		$userdata = UsersModel::find($userid);

		if(!count($userdata)){
			return redirect('login')->with(['warning' => 'Something went wrong.']);	
		}

		$_data=array(
			'token'=>$userid,
		);

		return view('frontend/login/reset_password',$_data);
	}

	public function postUpdatePassword(){

		$post_data = Request::all();
	
		$userid="";
		
		if(isset($post_data['v_user_token']) && $post_data['v_user_token']!=""){
			
			$userid = $post_data['v_user_token'];
			unset($post_data['v_user_token']);

		}else{
			return redirect('login')->with(['warning' => 'Something went wrong.']);	
		}

		$userdata = UsersModel::find($userid);

		if(!count($userdata)){
			return redirect('login')->with(['warning' => 'Something went wrong.']);	
		}

		if(isset($post_data['_token'])){
			unset($post_data['_token']);
		}

		if(!isset($post_data['password'])){
			return redirect('login')->with(['warning' => 'Something went wrong.']);	
		}

		$update=array(
			'password'=>Hash::make($post_data['password']),
			'd_modified'=>date("Y-m-d H:i:s"),
		);		
		UsersModel::find($userid)->update($update);

		$username = "";
		if(isset($userdata->v_fname)){
		   	$username = $userdata->v_fname;
		}
		if(isset($userdata->v_lname)){
		   	$username .= " ".$userdata->v_lname;
		}
		$useremail="";
		if(isset($userdata->v_email)){
		   	$useremail .= " ".$userdata->v_email;
		}
		$loginurl= url('login');
		$data=array(
			'name'=>$username,
			'loginurl'=>$loginurl,
		);

		$mailcontent = EmailtemplateHelper::Passwordconfirmation($data);
		$subject = "Password confirmation";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b17826c76fbae1c5e740b02");
		
		$mailids=array(
			$username=>$useremail
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		return redirect('login')->with(['success' => 'successfully changed password.']);

	}



	public function getLogout() {
		
		if(auth()->guard('web')->check()) {
			$auth = auth()->guard('web');
			$userid = auth()->guard('web')->user()->id;
			$update['e_login']="no";
			UsersModel::find($userid)->update($update);

		    $auth->logout();
		}
		return redirect('/');
	}
	
	public function signupFacebook(){

		FacebookSession::setDefaultApplication('196021424488808','587914c5828b38ada1b32fbcf1011b80');
       
        $redirect_url = url('facebook-callback');
        $helper = new FacebookRedirectLoginHelper($redirect_url);
        $fbloginurl = $helper->getLoginUrl(array('scope' => 'public_profile,email'));
        $state = md5(rand());
        $request->session()->set('g_state', $state);
        dd($fbloginurl);
        return redirect()->to($fbloginurl);
    
    }

	public function getFacebookcallback(){

	}


}