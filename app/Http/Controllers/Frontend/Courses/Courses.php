<?php
namespace App\Http\Controllers\Frontend\Courses;

use Request, Hash, Lang,Validator,Auth,Storage,File;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesAnnouncements as CoursesAnnouncements;
use App\Models\Courses\CoursesQA as CoursesQA;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Courses\BuyerCourse as BuyerCourseModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;


class Courses extends Controller {
	
	protected $section;

	public function __construct(){
		$this->section = Lang::get('section.courses');
	}
		
	public function index() {
		
		$userid = auth()->guard('web')->user()->_id;
		$query 	= CoursesModel::query();
		$data 	= $query->where('i_user_id',$userid)->orderBy('d_added','DESC')->paginate(20);	
		$_data	= array(
			'data'=>$data,
		);
		return view('frontend/Courses/courses_lists',$_data);
	}
	
	public function add() {

		$userid = auth()->guard('web')->user()->_id;
		$isBasicPlan = GeneralHelper::checkBasicPlan($userid);

		$coursesLevel = CoursesLevelModel::where("e_status","active")->get();
		$coursesLanguage = CoursesLanguageModel::where("e_status","active")->get();
		$coursesCategory = CoursesCategoryModel::where("e_status","active")->where('i_delete','!=',"1")->get();
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");


		$userdata = auth()->guard('web')->user();
		$userplan = array();
		if(isset($userdata->v_plan) && count($userdata->v_plan)){
			$userplan = $userdata->v_plan;
		}

		$v_author_name = auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname;

		$_data	= array(
			'coursesLevel' 		=> $coursesLevel,
			'coursesLanguage'	=> $coursesLanguage,
			'coursesCategory'	=> $coursesCategory,
			'plandata'=>$plandata,
			'discount'=>$discount,
			'isBasicPlan'=>$isBasicPlan,
			'v_author_name'=>$v_author_name,
			'userplan'=>$userplan,
		);
		
		return view('frontend/Courses/add_courses',$_data);
	}

	public function addData(){

		$data= Request::all();

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(isset($data['_token'])){
			unset($data['_token']);	
		}

		$userid = auth()->guard('web')->user()->_id;
		
		$data['v_requirement'] =nl2br($data['v_requirement']);
		//$data['l_description'] =nl2br($data['l_description']);
		
		$data['review']['total']=0;	
		$data['review']['avgtotal']=0;	
		$data['i_total_review']=0;	
		$data['i_total_avg_review']=0;	
		$data['e_status']="unpublished";	
		$data['i_user_id']=$userid;	
		$data['e_qa_enabled']="on";	
		$data['e_reviews_enabled']="on";	
		$data['e_announcements_enabled']="on";	
		
		$data['i_impression']=0;	
		$data['i_impression_perday']=0;	
		$data['i_contact']=0;	
		$data['i_clicks']=0;	
		$data['d_signup_date']=date("Y-m-d",strtotime(auth()->guard('web')->user()->d_added));	
		$data['d_added']=date("Y-m-d H:i:s");	
		$data['d_modified']=date("Y-m-d H:i:s");	
		$CoursesId = CoursesModel::create($data)->id; 
		
		$response['status']=1;
		$response['i_course_id']=$CoursesId;
		$response['msg'] = "Successfully update courses details.";
		echo json_encode($response);
		exit;
	}

	public function addDataUpdate(){
		
		$data= Request::all();
		
		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(isset($data['_token'])){
			unset($data['_token']);	
		}

		if(!isset($data['i_course_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$i_course_id="";
		if($data['i_course_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(isset($data['v_course_free'])){
			$data['f_price'] = 0;
		}	
		
		$data['l_instructor_details'] =nl2br($data['l_instructor_details']);
		if(isset($data['f_price']) && $data['f_price']!=""){
			$data['f_price'] = (float)$data['f_price'];
		}
		$data['e_sponsor'] = "no";
		//$data['e_sponsor_status'] ="pause";
		$i_course_id=$data['i_course_id'];
		unset($data['i_course_id']);

		if(count(Request::file('v_instructor_image'))){
			$image = Request::file('v_instructor_image');
			if(count($image)){
			
				$fileName = 'instructor_image-'.time().'.'.$image->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_instructor_image'] = 'courses/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/courses/'.$fileName, File::get((string)$image), 'public');
			}
		}

		if(count(Request::file('v_course_thumbnail'))){
			$image = Request::file('v_course_thumbnail');
			if(count($image)){
				$fileName = 'course_image-'.time().'.'.$image->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_course_thumbnail'] = 'courses/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/courses/'.$fileName, File::get((string)$image), 'public');
			}
		}

		if(count(Request::file('l_video'))){
			$video = Request::file('l_video');
			if( count($video) ){
				$fileName = 'introduction_video-'.time().'.'.$video->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['l_video'] = 'courses/video/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$video), 'public');
			}
		}

		$data['chapter']=array();
		if(isset($data['section']) && count($data['section']) ){
			foreach($data['section'] as $key => $value) {
				$data['section'][$key]['_id']=new \MongoDB\BSON\ObjectId();
				if(isset($value['video']) && count($value['video'])){
					foreach ($value['video'] as $y => $z) {
						$data['section'][$key]['video'][$y]['_id']=new \MongoDB\BSON\ObjectId();
						if(count($z['v_video'])){
							$fileName = 'section_video-'.time().'.'.$z['v_video']->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$z['v_video']), 'public');
							$data['section'][$key]['video'][$y]['v_video'] = 'courses/video/' . $fileName;
						}

						if(isset($z['v_doc']) && count($z['v_doc'])){
							$fileName = 'section_video_doc-'.time().'.'.$z['v_doc']->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$z['v_doc']), 'public');
							$data['section'][$key]['video'][$y]['v_doc'] = 'courses/video/' . $fileName;
						}
					}
				}
				
			}
		}
		if(isset($data['section']) && count($data['section']) ){
			foreach($data['section'] as $key => $value) {

				$tempdata=array();
				if(isset($value['video']) && count($value['video'])){
					foreach ($value['video'] as $y => $z) {
						$tempdata[]=$z;
					}
				}
				$value['video']=$tempdata;	
				$data['chapter'][]=$value;
			}
		}	
		$data['section']=$data['chapter'];	

		
		unset($data['chapter']);
		$data['d_modified']=date("Y-m-d H:i:s");	
		CoursesModel::find($i_course_id)->update($data);
		$response['status']=1;
		$response['msg'] = "Successfully update courses details.";
		echo json_encode($response);
		exit;
	}

	public function updateUserPlan(){
		
		$data = Request::all();
		$user_id = auth()->guard('web')->user()->id;

		if(isset($data['_token'])){
			unset($data['_token']);
		}
		$data['d_modified']=date("Y-m-d H;i:s");	
		
		UsersModel::find($user_id)->update($data);
		return redirect('courses')->with(['success' => 'Course successfully added']);	
	}

	public function coursesView($id="") {
		
		$data 	= CoursesModel::find($id);
		$user = UsersModel::where("e_status","active")->get();

		$coursesQA	= CoursesQA::where("i_courses_id",$id)->orderBy("d_added","DESC")->limit(6)->get();
		$coursesAnnouncements	= CoursesAnnouncements::where("i_courses_id",$id)->orderBy("d_added","DESC")->limit(6)->get();

		$total['qa']=CoursesQA::where("i_courses_id",$id)->count();
		$total['announcements']	= CoursesAnnouncements::where("i_courses_id",$id)->count();

		
		$reviews = CoursesReviews::where("i_courses_id",$id)->get();
		$ratedata=0;

		if(count($reviews)){
			foreach ($reviews as $key => $value) {
				$ratedata = $ratedata+($value->f_rate_instruction+$value->f_rate_navigate+$value->f_rate_reccommend)/3; 				
			}
		}
		
		$total['reviews'] = count($reviews);
		$total['ratedata'] = $ratedata;
	
		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}

		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';

		$_data	= array(
			'data'=>$data,
			'coursesQA'=>$coursesQA,
			'userList'=>$userList,
			'coursesAnnouncements'=>$coursesAnnouncements,
			'curentuser'=>$curentuser,
			'total'=>$total
		);
		return view('frontend/Courses/courses_view',$_data);
	}


	public function coursesSection($id="") {
		
		$data 	= CoursesModel::find($id);

		$coursesLevel = CoursesLevelModel::where("e_status","active")->get();
		$coursesLanguage = CoursesLanguageModel::where("e_status","active")->get();
		$coursesCategory = CoursesCategoryModel::where("e_status","active")->get();

		$total['qa']=CoursesQA::where("i_courses_id",$id)->count();
		$total['announcements']	= CoursesAnnouncements::where("i_courses_id",$id)->count();
		$total['reviews'] = CoursesReviews::where("i_courses_id",$id)->count();

		$_data	= array(
			'data'=>$data,
			'coursesLevel' 		=> $coursesLevel,
			'coursesLanguage'	=> $coursesLanguage,
			'coursesCategory'	=> $coursesCategory,
			'total'=>$total
		);
		return view('frontend/Courses/courses_section',$_data);
	}

	public function coursesSectionDelete($cid="",$csid="") {
		
		$data 	= CoursesModel::find($cid);
		$update['section']=array();	
		if(isset($data['section']) && count($data['section']) ){
			foreach($data['section'] as $key => $value) {
				 if($value['_id']!=$csid){
					 $update['section'][]=$value;
				 }
			}
		}
		CoursesModel::find($cid)->update($update);
		return redirect('courses/section/'.$cid)->with( 'success', "Course section has been deleted successfully."); 
	}

	public function coursesQA($id="") {
		

		$data= Request::all();


		$data 	= CoursesModel::find($id);
		
		$query = CoursesQA::query();
		if(isset($data['search']) && $data['search']!=""){
			
			$query = $query->where(function($query ) use($data){
		        $query->where('v_title','like','%'.$data['search'].'%')
		              ->orWhere('l_questions', 'like', '%' . $data['search'].'%');
		    });
		    $searchkeyword = $data['search'];
		}

		$query = $query->where('i_courses_id',$id);
		$query = $query->orderBy('d_added',"DESC");
		$coursesQA = $query->get();

		//$coursesQA	= CoursesQA::where("i_courses_id",$id)->orderBy("d_added","DESC")->get();
	
		$user = UsersModel::where("e_status","active")->get();	
		$total['qa']=CoursesQA::where("i_courses_id",$id)->count();
		$total['announcements']	= CoursesAnnouncements::where("i_courses_id",$id)->count();
		$total['reviews'] = CoursesReviews::where("i_courses_id",$id)->count();

		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}

		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$_data	= array(
			'data'=>$data,
			'coursesQA'=>$coursesQA,
			'userList'=>$userList,
			'curentuser'=>$curentuser,
			'total'=>$total

		);

		return view('frontend/Courses/courses_qa',$_data);
		
	}


	public function Qasearch($id="") {
		
		$rdata= Request::all();
		$id = $rdata['icid'];

		$data 	= CoursesModel::find($id);
		$query = CoursesQA::query();
		if(isset($rdata['search']) && $rdata['search']!=""){
			$query = $query->where(function($query ) use($rdata){
		        $query->where('v_title','like','%'.$rdata['search'].'%')
		              ->orWhere('l_questions', 'like', '%' . $rdata['search'].'%');
		    });
		    $searchkeyword = $rdata['search'];
		}

		$query = $query->where('i_courses_id',$id);
		$query = $query->orderBy('d_added',"DESC");
		$coursesQA = $query->get();

		$user = UsersModel::where("e_status","active")->get();	
		$total['qa']=CoursesQA::where("i_courses_id",$id)->count();
		$total['announcements']	= CoursesAnnouncements::where("i_courses_id",$id)->count();
		$total['reviews'] = CoursesReviews::where("i_courses_id",$id)->count();

		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}

		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$_data	= array(
			'data'=>$data,
			'coursesQA'=>$coursesQA,
			'userList'=>$userList,
			'curentuser'=>$curentuser,
			'total'=>$total

		);

		$faqstr = view('frontend/Courses/courses_qa_search',$_data);
		$responsestr = $faqstr->render();
		$response['status']=1;
		$response['responsestr']=$responsestr;
		echo json_encode($response);
		exit;


		
	}

	public function updateQA() {
		
		$data= Request::all();
		$response = array();

		if(!isset($data['i_course_qa_id']) && $data['i_course_qa_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}
	
		$coursesQA	= CoursesQA::find($data['i_course_qa_id']);
		
		if(!count($coursesQA)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$course = CoursesModel::find($coursesQA->i_courses_id);
		
		$update = array(
			'i_user_id'=>auth()->guard('web')->user()->id,
			'v_text'=>$data['v_text'],
			'd_added'=>date("Y-m-d H:i:s"),
		);

		$ansdata['l_answers'] = $coursesQA->l_answers;
		$ansdata['l_answers'][] = $update;
	
		CoursesQA::find($data['i_course_qa_id'])->update($ansdata);

		self::postQAEmailSend($course,$data,$coursesQA);

		
		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$commentstr="";
		$commentstr.='<div class="Announcements-second-man qaseller-second-man">';
		$commentstr.='<div class="qa-seller-find">';
		$commentstr.='<div class="Announcements-man">'.GeneralHelper::userroundimage($curentuser).'</div>';
		$commentstr.='<div class="Announcements-name">';
		$commentstr.='<p>'.$curentuser['v_name'].'</p>';
		$commentstr.='<p><span>'.\Carbon\Carbon::createFromTimeStamp(strtotime($update['d_added']))->diffForHumans().'</span></p>';
		$commentstr.='<div class="Announcements-message">'.$update['v_text'].'</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';

		$response['status']=1;
		$response['commentstr']=$commentstr;
		$response['msg']="Successfully add comment.";
		echo json_encode($response);
		exit;

	}


	public function postQAEmailSend($cdata,$postdata,$coursesQA){

		if(!count($cdata)){
			return 0;
		}
		if(!count($postdata)){
			return 0;
		}
		if(!count($coursesQA)){
			return 0;
		}

		$buyername="";
		if(count($coursesQA->hasUser()) && isset($coursesQA->hasUser()->v_fname)){
			$buyername = $coursesQA->hasUser()->v_fname;
		}
		if(count($coursesQA->hasUser()) && isset($coursesQA->hasUser()->v_lname)){
			$buyername .= ' '.$coursesQA->hasUser()->v_lname;
		}

		$emailadress="";
		if(count($coursesQA->hasUser()) && isset($coursesQA->hasUser()->v_email)){
			$emailadress = $coursesQA->hasUser()->v_email;
		}
	
		$sellername="";
		if(isset(auth()->guard('web')->user()->v_fname)){
			$sellername = auth()->guard('web')->user()->v_fname;
		}
		if(isset(auth()->guard('web')->user()->v_lname)){
			$sellername .= ' '.auth()->guard('web')->user()->v_lname;
		}

		$coursename="";
		if(isset($cdata->v_title)){
			$coursename = $cdata->v_title;
		}

		$data['sellername']=$sellername;
		$data['buyername']=$buyername;
		$data['coursename']=$coursename;
		$data['v_title']="";
		$data['l_questions']="";
		$data['l_answers']="";

		if(isset($postdata['v_text'])){
			$data['l_answers'] = $postdata['v_text'];
		}

		if(isset($coursesQA['v_title'])){
			$data['v_title'] = $coursesQA['v_title'];
		}
		if(isset($coursesQA['l_questions'])){
			$data['l_questions'] = $coursesQA['l_questions'];
		}

		$mailcontent = EmailtemplateHelper::CourseQAtoBuyer($data);
		$subject = "Course QA";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b1a20a876fbae0b95669873");
		$mailids=array(
			$buyername=>$emailadress
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		return 1;		
	}



	public function coursesReviews($id=""){

		$data 	= CoursesModel::find($id);
		$coursesReviews	= CoursesReviews::where("i_courses_id",$id)->orderBy("d_added","DESC")->get();
		$user = UsersModel::where("e_status","active")->get();

		$total['qa']=CoursesQA::where("i_courses_id",$id)->count();
		$total['announcements']	= CoursesAnnouncements::where("i_courses_id",$id)->count();
		$total['reviews'] = CoursesReviews::where("i_courses_id",$id)->count();

		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}

		$totalstar = 0;
		$totalavg = 0;
		if(count($coursesReviews)){
			foreach ($coursesReviews as $key => $value) {
				$totalstar = $totalstar+$value->i_star;
			}
			$totalavg = $totalstar/count($coursesReviews);
		}

		$ratedata=0;

		if(count($coursesReviews)){
			foreach ($coursesReviews as $key => $value) {
				$ratedata = $ratedata+($value->f_rate_instruction+$value->f_rate_navigate+$value->f_rate_reccommend)/3; 				
			}
			$ratedata = $ratedata/count($coursesReviews);
		}
		

		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$_data	= array(
			'data'=>$data,
			'coursesReviews'=>$coursesReviews,
			'userList'=>$userList,
			'curentuser'=>$curentuser,
			'totalavg'=>$ratedata,
			'total'=>$total
		);
		return view('frontend/Courses/courses_reviews',$_data);
	}

	public function updateReviews() {
		
		$data= Request::all();
		$response = array();

		if(!isset($data['i_course_reviews_id']) && $data['i_course_reviews_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}
	
		$coursesReviews	= CoursesReviews::find($data['i_course_reviews_id']);
		
		if(!count($coursesReviews)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$course = CoursesModel::find($coursesReviews->i_courses_id);
	
		$update['l_answers']['i_user_id'] = auth()->guard('web')->user()->id;
		$update['l_answers']['l_comment'] = $data['l_comment'];
		$update['l_answers']['d_added'] = date("Y-m-d H:i:s");

		CoursesReviews::find($data['i_course_reviews_id'])->update($update);

		self::postReviewEmailSend($course,$data,$coursesReviews);
		
		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$commentstr="";
		$commentstr.='<div class="conversation-text extra-margin">';
		$commentstr.='<div class="conversation-find conversation-find-course">';
		$commentstr.='<div class="conversation-man">'.GeneralHelper::userroundimage($curentuser).'</div>';
		$commentstr.='<div class="conversation-name">';
		$commentstr.='<p>'.$curentuser['v_name'].'<span class="rating-buyer rating-Course-one">';
		$commentstr.='</span></p>';
		$commentstr.='<p><span>'.\Carbon\Carbon::createFromTimeStamp(strtotime($update['l_answers']['d_added']))->diffForHumans().'</span></p>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='<div class="conversation-text conversation-text-final">';
		$commentstr.='<div class="conversation-text-all conversation-text-course">';
		$commentstr.='<p class="response-seller">'.$update['l_answers']['l_comment'].'</p>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';

		$response['status']=1;
		$response['commentstr']=$commentstr;
		$response['msg']="Successfully add comment.";
		echo json_encode($response);
		exit;
	}

	public function postReviewEmailSend($cdata,$postdata,$coursesReviews){

		if(!count($cdata)){
			return 0;
		}
		if(!count($postdata)){
			return 0;
		}
		if(!count($coursesReviews)){
			return 0;
		}

		$buyername="";
		if(count($coursesReviews->hasUser()) && isset($coursesReviews->hasUser()->v_fname)){
			$buyername = $coursesReviews->hasUser()->v_fname;
		}
		if(count($coursesReviews->hasUser()) && isset($coursesReviews->hasUser()->v_lname)){
			$buyername .= ' '.$coursesReviews->hasUser()->v_lname;
		}

		$emailadress="";
		if(count($coursesReviews->hasUser()) && isset($coursesReviews->hasUser()->v_email)){
			$emailadress = ' '.$coursesReviews->hasUser()->v_email;
		}


		$sellername="";
		if(isset(auth()->guard('web')->user()->v_fname)){
			$sellername = auth()->guard('web')->user()->v_fname;
		}
		if(isset(auth()->guard('web')->user()->v_lname)){
			$sellername .= ' '.auth()->guard('web')->user()->v_lname;
		}

		$coursename="";
		if(isset($cdata->v_title)){
			$coursename = $cdata->v_title;
		}

		$data['sellername']=$sellername;
		$data['buyername']=$buyername;
		$data['coursename']=$coursename;
		$data['review_string']=$postdata['l_comment'];
	
		$mailcontent = EmailtemplateHelper::CourseReviewReplies($data);
		$subject = "Course Review";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b1a216e76fbae0b95669874");
		
		$mailids=array(
			$buyername=>$emailadress
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		return 1;		

	}

	public function coursesAnnouncements($id=""){

		$data 	= CoursesModel::find($id);
		$coursesAnnouncements	= CoursesAnnouncements::where("i_courses_id",$id)->orderBy("d_added","DESC")->get();
		$user = UsersModel::where("e_status","active")->get();

		$total['qa']=CoursesQA::where("i_courses_id",$id)->count();
		$total['announcements']	= CoursesAnnouncements::where("i_courses_id",$id)->count();
		$total['reviews'] = CoursesReviews::where("i_courses_id",$id)->count();
		
		//dd($coursesAnnouncements);

		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}

		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$_data	= array(
			'data'=>$data,
			'coursesAnnouncements'=>$coursesAnnouncements,
			'userList'=>$userList,
			'curentuser'=>$curentuser,
			'total'=>$total
		);
		return view('frontend/Courses/courses_announcements',$_data);

	}

	public function coursesAddAnnouncements(){

		$data= Request::all();

		$response = array();

		if(isset($data['_token'])){
			unset($data['_token']);
		}	

		if(!isset($data['i_courses_id']) && $data['i_courses_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$data['i_user_id'] = auth()->guard('web')->user()->_id;
		$data['e_status'] = "active";
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		$id = CoursesAnnouncements::create($data)->id;
		
		self::sendEmailAnnouncementsBuyer($id,$data['i_courses_id']);
		$response['status']=1;
		$response['i_courses_id']=$data['i_courses_id'];
		$response['msg']="Successfully add announcements.";
		echo json_encode($response);
		exit;
	}

	public function sendEmailAnnouncementsBuyer($aid="",$cid=""){
		
		if($cid==""){
			return 0;
		}
		if($aid==""){
			return 0;
		}

		$buyerdata = BuyerCourseModel::where('i_course_id',$cid)->get();
		$adata = CoursesAnnouncements::find($aid);
		$cdata = CoursesModel::find($cid);

		if(!count($buyerdata)){
			return 0;
		}
		if(!count($adata)){
			return 0;
		}

		$orderstr="";
        $orderstr.="<table style='width:100%'>";
        $orderstr.="<tr><th colspan='2'><b>Course Announcements</b></th></tr>";
        $orderstr.="<tr><td>Course Name</td>";
        $orderstr.="<td>".$cdata->v_title."</td></tr>";
        $orderstr.="<tr><td>Course Announcements</td>";
        $orderstr.="<td>".$adata->l_announcements."</td></tr>";
        $orderstr.="<table>";

		foreach ($buyerdata as $key => $value) {
			
			$buyername="";
	        if(count($value->hasUser()) && isset($value->hasUser()->v_fname)){
	            $buyername = $value->hasUser()->v_fname;
	        }
	        if(count($value->hasUser()) && isset($value->hasUser()->v_lname)){
	            $buyername .= ' '.$value->hasUser()->v_lname;
	        }

	        $buyeremail="";
	        if(count($value->hasUser()) && isset($value->hasUser()->v_email)){
	            $buyeremail = $value->hasUser()->v_email;
	        }

	        $data=array(
	            'name'=>$buyername,
	            'COURSE_ANNOUNCEMENT'=>$orderstr,
	            'COURSE_BUYER_URL'=>url('buyer/courses/announcements').'/'.$value->id,
	        );
          
            $mailcontent = EmailtemplateHelper::CourseAnnouncementBuyer($data);
	        $subject = "Course Announcement";
	        $subject = EmailtemplateHelper::EmailTemplateSubject("5b0e8ace76fbae69ad6587c2");
	        
	        $mailids=array(
	            $buyername=>$buyeremail,
	        );
	        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);


			
		}
	}


	public function coursesGetAnnouncements(){

		$data= Request::all();
		$response = array();

		if(!isset($data['id']) && $data['id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$announcementsData =  CoursesAnnouncements::find($data['id']);

		if(!count($announcementsData)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}

		$response['status']=1;
		$response['data']=$announcementsData;
		$response['msg']="Successfully edit announcements data.";
		echo json_encode($response);
		exit;	
	}

	public function coursesUpdateAnnouncements(){


		$data= Request::all();
		$response = array();

		$id="";
		if(!isset($data['i_courses_announcements_id']) && $data['i_courses_announcements_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}else{
			$id=$data['i_courses_announcements_id'];
		}
		$coursesAnnouncements	= CoursesAnnouncements::find($data['i_courses_announcements_id']);
		
		if(!count($coursesAnnouncements)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$data['d_modified'] = date("Y-m-d H:i:s");
		CoursesAnnouncements::find($id)->update($data);
		
		$response['status']=1;
		$response['data']=$data;
		$response['msg']=GeneralHelper::successMessage("Successfully update announcements.");
		echo json_encode($response);
		exit;
	}

	public function coursesCommentsAnnouncements() {
		
		$data= Request::all();
		$response = array();

		if(!isset($data['i_announcements_id']) && $data['i_announcements_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}
	
		$coursesAnnouncements= CoursesAnnouncements::find($data['i_announcements_id']);
		
		if(!count($coursesAnnouncements)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$update = array(
			'i_user_id'=>auth()->guard('web')->user()->id,
			'v_text'=>$data['v_text'],
			'd_added'=>date("Y-m-d H:i:s"),
		);

		$ansdata['l_comments'] = $coursesAnnouncements->l_comments;
		$ansdata['l_comments'][] = $update;
	
		CoursesAnnouncements::find($data['i_announcements_id'])->update($ansdata);
		self::sendEmailAnnouncementsCommentBuyer($data['i_announcements_id'],$coursesAnnouncements->i_courses_id,$data['v_text']);
		
		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$commentstr="";
		$commentstr.='<div class="Announcements-second-man">';
		$commentstr.='<div class="Announcements-find">';
		$commentstr.='<div class="Announcements-man">'.GeneralHelper::userroundimage($curentuser).'</div>';
		$commentstr.='<div class="Announcements-name">';
		$commentstr.='<p>'.$curentuser['v_name'].'</p>';
		$commentstr.='<p><span>'.\Carbon\Carbon::createFromTimeStamp(strtotime($update['d_added']))->diffForHumans().'</span></p>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='<div class="Announcements-text">';
		$commentstr.='<div class="row">';
		$commentstr.='<div class="col-sm-12">';
		$commentstr.='<div class="Announcements-text-seller">';
		$commentstr.='<p>'.$update['v_text'].'</p>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';

		$response['status']=1;
		$response['commentstr']=$commentstr;
		$response['msg']="Successfully post your comment.";
		echo json_encode($response);
		exit;
	}

	public function sendEmailAnnouncementsCommentBuyer($aid="",$cid="",$comment=""){
		
		if($cid==""){
			return 0;
		}
		if($aid==""){
			return 0;
		}

		$buyerdata = BuyerCourseModel::where('i_course_id',$cid)->get();
		$adata = CoursesAnnouncements::find($aid);
		$cdata = CoursesModel::find($cid);

		if(!count($buyerdata)){
			return 0;
		}
		if(!count($adata)){
			return 0;
		}

		$sellername="";
		if(isset(auth()->guard('web')->user()->v_fname)){
			$sellername = auth()->guard('web')->user()->v_fname;
		}
		if(isset(auth()->guard('web')->user()->v_lname)){
			$sellername .= ' '.auth()->guard('web')->user()->v_lname;
		}
		$coursename="";
		if(isset($cdata->v_title)){
			$coursename = $cdata->v_title;
		}

		$data['USER_FULL_NAME']="";
		$data['BUYER_NAME']=$sellername;
		$data['COURSE_TITLE']=$coursename;
		$data['COURSE_ANNOUNCEMENT_COMMENT']=$comment;
		$data['COURSE_SELLER_URL']=url('buyer/courses/announcements').'/'.$cdata->id;

		foreach ($buyerdata as $key => $value) {
			
			$buyername="";
	        if(count($value->hasUser()) && isset($value->hasUser()->v_fname)){
	            $buyername = $value->hasUser()->v_fname;
	        }
	        if(count($value->hasUser()) && isset($value->hasUser()->v_lname)){
	            $buyername .= ' '.$value->hasUser()->v_lname;
	        }
	        $data['USER_FULL_NAME']=$buyername;

	        $buyeremail="";
	        if(count($value->hasUser()) && isset($value->hasUser()->v_email)){
	            $buyeremail = $value->hasUser()->v_email;
	        }

	        $mailcontent = EmailtemplateHelper::CourseAnnouncementCommentNotifyToSeller($data);
			$subject = "Course announcement comment";
			$subject = EmailtemplateHelper::EmailTemplateSubject("5bbb38c476fbae654c11d283");
			$mailids=array(
				$buyername=>$buyeremail
			);
			$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

	    }
	}




	public function updateCoursesStatus(){

		$data= Request::all();

		$response = array();

		if(!isset($data['id']) && $data['id']==""){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
	
		$courses= CoursesModel::find($data['id']);
		
		if(!count($courses)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$update = array(
			'e_status'=>$data['e_status'],
			'd_modified'=>date("Y-m-d H:i:s"),
		);

		$response['msg']="";
		if($data['e_status']=="published"){
			if(!isset($courses->section)){
				$response['status']=0;
				$response['msg']=GeneralHelper::errorMessage("Your course section not completed. please complete your course section.");
				echo json_encode($response);
				exit;
			}else{
			    $response['msg']=GeneralHelper::successMessage("Your course successfully published.");
		    }
		}else{
			$response['msg']=GeneralHelper::successMessage("Your course successfully unpublished.");
		}

		CoursesModel::find($data['id'])->update($update);

		$response['status']=1;
		$response['data']=$data;
		//$response['msg']=GeneralHelper::successMessage("Successfully update course status.");
		echo json_encode($response);
		exit;

	}

	public function coursesSettings($id=""){

		$data 	= CoursesModel::find($id);
		$_data	= array(
			'data'=>$data,
		);
		return view('frontend/Courses/courses_settings',$_data);
	}

	public function updateSettings(){

		$data= Request::all();

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$i_courses_id ="";
		if(!isset($data['i_courses_id'])){
			return redirect('courses/')->with(['warning' => 'Something went wrong.']);		
		}else{
			$i_courses_id = $data['i_courses_id'];
		}

		$data['d_modified']=date("Y-m-d H;i:s");	
		CoursesModel::find($i_courses_id)->update($data);
		return redirect('courses/settings/'.$i_courses_id)->with(['success' => 'Successfully saved course settings.']);		

	}

	public function coursesMessages($id=""){
		
		$data = CoursesModel::find($id);
		$_data	= array(
			'data'=>$data,
		);
		return view('frontend/Courses/courses_messages',$_data);

	}

	public function updateMessages(){

		$data= Request::all();

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$i_courses_id ="";
		if(!isset($data['i_courses_id'])){
			return redirect('courses/')->with(['warning' => 'Something went wrong.']);		
		}else{
			$i_courses_id = $data['i_courses_id'];
		}

		$data['d_modified']=date("Y-m-d H;i:s");	
		CoursesModel::find($i_courses_id)->update($data);

		return redirect('courses/messages/'.$i_courses_id)->with(['success' => 'Successfully saved course messages.']);		

	}

	public function addNewSection(){

		$data= Request::all();
	
		if(!isset($data['i_course_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$i_course_id="";
		if($data['i_course_id']==""){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		$i_course_id = $data['i_course_id'];

		$data['chapter']=array();
		if(isset($data['section']) && count($data['section']) ){
			foreach($data['section'] as $key => $value) {

				$data['section'][$key]['_id'] = new \MongoDB\BSON\ObjectId();	
				if(isset($value['video']) && count($value['video'])){
					foreach ($value['video'] as $y => $z) {
						$data['section'][$key]['video'][$y]['_id']=new \MongoDB\BSON\ObjectId();	
						
						if(isset($z['v_video']) && count($z['v_video'])){
							$fileName = 'section_video-'.time().'.'.$z['v_video']->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$z['v_video']), 'public');
							$data['section'][$key]['video'][$y]['v_video'] = 'courses/video/' . $fileName;
						}else{
							$data['section'][$key]['video'][$y]['v_video'] = "";
						}

						if(isset($z['v_doc']) && count($z['v_doc'])){
							$fileName = 'section_video_doc-'.time().'.'.$z['v_doc']->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$z['v_doc']), 'public');
							$data['section'][$key]['video'][$y]['v_doc'] = 'courses/video/' . $fileName;
						}else{
							$data['section'][$key]['video'][$y]['v_doc']="";
						}
					}
				}
			}
		}

		if(isset($data['section']) && count($data['section']) ){
			foreach($data['section'] as $key => $value) {
				$tempdata=array();
				if(isset($value['video']) && count($value['video'])){
					foreach ($value['video'] as $y => $z) {
						$tempdata[]=$z;
					}
				}
				$value['video']=$tempdata;	
				$data['chapter'][]=$value;
			}
		}	
		$data['section']=$data['chapter'];	


		// if(isset($data['section']) && count($data['section']) ){
		// 	foreach($data['section'] as $key => $value) {
				
		// 		$data['section'][$key]['_id'] = new \MongoDB\BSON\ObjectId();

		// 		if(isset($value['video']['v_video']) && count($value['video']['v_video'])){
		// 			foreach($value['video']['v_video'] as $k => $v) {
		// 				if(count($v)){

		// 					$fileName = 'section_video-'.time().'.'.$v->getClientOriginalExtension();
		// 					$fileName = str_replace(' ', '_', $fileName);
		// 					$data['section'][$key]['video']['v_video'][$k] = 'courses/video/' . $fileName;
		// 					$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$v), 'public');

		// 				}
		// 			}
		// 		}

		// 		if(isset($value['doc']['v_doc']) && count($value['doc']['v_doc'])){
		// 			foreach($value['doc']['v_doc'] as $k => $v) {
		// 				if(count($v)){
							
		// 					$fileName = 'section_doc-'.time().'.'.$v->getClientOriginalExtension();
		// 					$fileName = str_replace(' ', '_', $fileName);
		// 					$data['section'][$key]['doc']['v_doc'][$k] = 'courses/video/' . $fileName;
		// 					$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$v), 'public');

		// 				}
			
		// 			}
		// 		}

		// 	}
		// }

		$course = CoursesModel::find($i_course_id);
		$section = $course->section;

		if(count($data['section'])){
			$section[] = $data['section'][0];	
		}
		
		$update['section']=$section;
		CoursesModel::find($i_course_id)->update($update);
		$response['status']=1;
		$response['msg'] = GeneralHelper::successMessage("Successfully update course details.");
		echo json_encode($response);
		exit;

	}

	public function getSection(){

		$data= Request::all();

	
		if(!isset($data['i_course_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!isset($data['i_section_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$course = CoursesModel::find($data['i_course_id']);

		if(!count($course)){
			
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!count($course->section)){
			
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$sectiondata=array();
		$sectioncnt=1;
		$sectionnumber=1;
		foreach ($course->section as $key => $value) {
			
			if($value['_id']==$data['i_section_id'])	{
				$sectiondata = $value;
				$sectionnumber = $sectioncnt;

			}
			$sectioncnt = $sectioncnt+1;

		}

		if(!count($sectiondata)){
			
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$_data=array(
			'sectiondata'=>$sectiondata,
			'sectionnumber'=>$sectionnumber
		);	

		$sectionstr = view('frontend/Courses/section', $_data); 
		$sectionstr = $sectionstr->render();


		// $sectionstr="";		
		// $sectionstr.='<div class="form-field">';
		// $sectionstr.='<div class="row-form-field">';
		// $sectionstr.='<div class="row">';
		// $sectionstr.='<div class="col-sm-6 col-xs-12">';
		// $sectionstr.='<label class="field-name">Section Title</label>';
		// $sectionstr.='<input type="text" class="form-control input-field" name="section[0][v_title]" required value="'.$sectiondata['v_title'].'">';
		// $sectionstr.='</div>';
		// $sectionstr.='</div>';
		// $sectionstr.='</div>';
		// $sectionstr.='</div>';

		// $destination = url('public/uploads/courses');
		// if(isset($sectiondata['video']['v_title']) && count($sectiondata['video']['v_title'])){
		// 	foreach ($sectiondata['video']['v_title'] as $key => $value) {
				
		// 		$sectionstr.='<div class="row-form-field sectionvideoeditremove">';
		// 		$sectionstr.='<div class="row">';
		// 		$sectionstr.='<div class="col-sm-6 col-xs-12">';
		// 		$sectionstr.='<label class="field-name">Section Video Title</label>';
		// 		$sectionstr.='<input type="text" class="form-control input-field" name="section[0][video][v_title][]" required value="'.$value.'">';
		// 		$sectionstr.='<video width="300" height="130" controls>';
		// 		$sectionstr.='<source src="'.$destination.'/'.$sectiondata['video']['v_video'][$key].'" type="video/mp4">';
		// 		$sectionstr.='/video>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='<div class="col-sm-6 col-xs-12">';
		// 		$sectionstr.='<label class="field-name">Section Video</label>';
		// 		$sectionstr.='<input type="text" class="form-control input-field" id="textvideouploadedit'.$key.'">';
		// 		$sectionstr.='<div class="checkbox-service">';
		// 		$sectionstr.='<label class="checkbox check-boxright">';
				
				
		// 		if(isset($sectiondata['video']['i_preview'][$key]) && $sectiondata['video']['i_preview'][$key] == "on"){
		// 			$sectionstr.='<input type="checkbox" name="section[0][video][i_preview]['.$key.']" checked>';			
		// 		}else{
		// 			$sectionstr.='<input type="hidden" name="section[0][video][i_preview]['.$key.']" value="off">';
		// 			$sectionstr.='<input type="checkbox" name="section[0][video][i_preview]['.$key.']">';	
		// 		}

		// 		$sectionstr.='<span class="subfield-name"> Preview this course</span>';
		// 		$sectionstr.='</label>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='<div class="clearfix"></div>';
		// 		$sectionstr.='<div class="col-xs-6">';
		// 		$sectionstr.='<div class="text-center">';
		// 		$sectionstr.='<input type="file" class="browsebutton" name="section[0][video][v_video][]"  id="videouploadedit'.$key.'" onchange="workvideosectionedit('.$key.');"/>';
		// 		$sectionstr.='<label id="fileupload" class="btn-editdetail" for="videouploadedit'.$key.'">Browser</label>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='</div>';
		// 		if($key<1){
		// 			$sectionstr.='<input type="hidden" id="videouploadcntedit" value="'.count($sectiondata['video']['v_title']).'">';	
		// 		}
		// 		$sectionstr.='<div class="col-xs-6">';
		// 		$sectionstr.='<div class="text-center">';
		// 		if($key<1){
		// 			$sectionstr.='<button type="button" class="btn form-add-course" onclick="editmorevideo()" >Add More</button>';
		// 		}else{
		// 			$sectionstr.='<button type="button" class="btn btn-add-remove videoeditrem" style="margin-top: 14px;" >Remove</button>';
		// 		}
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='</div>';

		// 	}
		// 	$sectionstr.='<div id="sectionvideoedit"></div>';	
		// }
		// if(isset($sectiondata['doc']['v_title']) && count($sectiondata['doc']['v_title'])){
		// 	foreach ($sectiondata['doc']['v_title'] as $key => $value) {

		// 		$sectionstr.='<div class="row-form-field doceditremove">';
		// 		$sectionstr.='<div class="row">';
		// 		$sectionstr.='<div class="col-sm-6 col-xs-12">';
		// 		$sectionstr.='<label class="field-name">Section Doc Title</label>';
		// 		$sectionstr.='<input type="text" class="form-control input-field" name="section[0][doc][v_title][]" required value="'.$value.'">';
		// 		$sectionstr.='<a href="'.$destination.'/'.$sectiondata['doc']['v_doc'][$key].'" style="margin-left: 10px;margin-top:10px">'.$value.'</a>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='<div class="col-sm-6 col-xs-12">';
		// 		$sectionstr.='<label class="field-name">Section Doc</label>';
		// 		$sectionstr.='<input type="text" class="form-control input-field" id="textdocuploadedit'.$key.'">';
		// 		$sectionstr.='<div class="col-xs-6">';
		// 		$sectionstr.='<div class="text-center">';
		// 		$sectionstr.='<input type="file" class="browsebutton" name="section[0][doc][v_doc][]" id="docuploadedit'.$key.'" onchange="uploadDocEdit('.$key.');" />';
		// 		$sectionstr.='<label id="fileupload" class="btn-editdetail" for="docuploadedit'.$key.'">Browser';
		// 		$sectionstr.='</label>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='</div>';
		// 		if($key<1){
		// 			$sectionstr.='<input type="hidden" id="docuploadcntedit" value="'.count($sectiondata['doc']['v_title']).'">';	
		// 		}
		// 		$sectionstr.='<div class="col-xs-6">  ';
		// 		$sectionstr.='<div class="text-center">';
		// 		if($key<1){
		// 			$sectionstr.='<button type="button" class="btn form-add-course" onclick="addmoredocedit()">Add More</button>';
		// 		}else{
		// 			$sectionstr.='<button type="button" class="btn btn-add-remove doceditrem" style="margin-top: 14px;">Remove</button>';
		// 		}	
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='</div>';
		// 		$sectionstr.='</div>';
		// 	}
		// 	$sectionstr.='<div id="sectiondocedit"></div>';	

		// }	



		$response['status']=1;
		$response['sectionstr']=$sectionstr;
		$response['msg'] = GeneralHelper::successMessage("Successfully update course details.");
		echo json_encode($response);
		exit;
  	}

  	public function updateSection(){

  		$data = Request::all();
  	
  		if(!isset($data['i_course_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!isset($data['i_section_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$course = CoursesModel::find($data['i_course_id']);

		if(!count($course)){
			
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!count($course->section)){
			
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$sectiondata=array();
		foreach ($course->section as $key => $value) {
			if($value['_id']==$data['i_section_id'])	{
				$sectiondata = $value;
			}
		}
	

		$data['chapter']=array();
		if(isset($data['section']) && count($data['section']) ){
			foreach($data['section'] as $key => $value) {
				$data['section'][$key]['_id'] = $data['i_section_id'];

				if(isset($value['video']) && count($value['video'])){
					
					foreach ($value['video'] as $y => $z) {
						
						if(isset($z['_id']) && $z['_id']!=""){
							$data['section'][$key]['video'][$y]['_id'] = $z['_id'];	
						}else{
							$data['section'][$key]['video'][$y]['_id']=new \MongoDB\BSON\ObjectId();	
						}
						
						if(isset($z['v_video']) && count($z['v_video'])){
							$fileName = 'section_video-'.time().'.'.$z['v_video']->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$z['v_video']), 'public');
							$data['section'][$key]['video'][$y]['v_video'] = 'courses/video/' . $fileName;
						}else{
							$data['section'][$key]['video'][$y]['v_video'] = $sectiondata['video'][$y]['v_video'];
						}

						if(isset($z['v_doc']) && count($z['v_doc'])){
							$fileName = 'section_video_doc-'.time().'.'.$z['v_doc']->getClientOriginalExtension();
							$fileName = str_replace(' ', '_', $fileName);
							$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$z['v_doc']), 'public');
							$data['section'][$key]['video'][$y]['v_doc'] = 'courses/video/' . $fileName;
						}else{
							if(isset($sectiondata['video'][$y]['v_doc'])){
								$data['section'][$key]['video'][$y]['v_doc']=$sectiondata['video'][$y]['v_doc'];
							}else{
								$data['section'][$key]['video'][$y]['v_doc']="";
							}
						}
					}
				}
				
			}
		}

		if(isset($data['section']) && count($data['section']) ){
			foreach($data['section'] as $key => $value) {
				$tempdata=array();
				if(isset($value['video']) && count($value['video'])){
					foreach ($value['video'] as $y => $z) {
						$tempdata[]=$z;
					}
				}
				$value['video']=$tempdata;	
				$data['chapter'][]=$value;
			}
		}	
		$data['section']=$data['chapter'];	

		// if(isset($data['section']) && count($data['section']) ){
			
		// 	foreach($data['section'] as $key => $value) {
				
		// 		$data['section'][$key]['_id'] = new \MongoDB\BSON\ObjectId();
		// 		if(isset($value['video']['v_video']) && count($value['video']['v_video'])){
		// 			foreach($value['video']['v_video'] as $k => $v) {
		// 				if(count($v)){
							
		// 					$fileName = 'section_video-'.time().'.'.$v->getClientOriginalExtension();
		// 					$fileName = str_replace(' ', '_', $fileName);
		// 					$data['section'][$key]['video']['v_video'][$k] = 'courses/video/' . $fileName;
		// 					$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$v), 'public');
		// 				}else{
		// 					$data['section'][$key]['video']['v_video'][$k] = $sectiondata['video']['v_video'][$k]; 		
		// 				}



		// 			}
		// 		}

		// 		if(isset($value['doc']['v_doc']) && count($value['doc']['v_doc'])){
		// 			foreach($value['doc']['v_doc'] as $k => $v) {
						
		// 				if(count($v)){
							
		// 					$fileName = 'section_doc-'.time().'.'.$v->getClientOriginalExtension();
		// 					$fileName = str_replace(' ', '_', $fileName);
		// 					$data['section'][$key]['doc']['v_doc'][$k] = 'courses/video/' . $fileName;
		// 					$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$v), 'public');

		// 				}else{
		// 					$data['section'][$key]['doc']['v_doc'][$k] = $sectiondata['doc']['v_doc'][$k]; 		
		// 				}

			
		// 			}
		// 		}

		// 	}
		// }

		$sectionupdate['section']=array();
		foreach ($course->section as $key => $value) {
			if($value['_id'] == $data['i_section_id'])	{
				$sectionupdate['section'][] = $data['section'][0];
			}else{
				$sectionupdate['section'][] = $value;
			}
		}	

		CoursesModel::find($data['i_course_id'])->update($sectionupdate);
		$response['status']=1;
		$response['msg'] = GeneralHelper::successMessage("Successfully update course details.");
		echo json_encode($response);
		exit;
  	}


  	public function getCourse(){

		$data= Request::all();

		if(!isset($data['i_course_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		$course = CoursesModel::find($data['i_course_id']);

		if(!count($course)){
			
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$response['v_instructor_image']="";	
		if(isset($course->v_instructor_image) && $course->v_instructor_image!=""){
			$response['v_instructor_image']=\Storage::cloud()->url($course->v_instructor_image);
		}

		$response['v_course_thumbnail']="";	
		if(isset($course->v_course_thumbnail) && $course->v_course_thumbnail!=""){
			$response['v_course_thumbnail']=\Storage::cloud()->url($course->v_course_thumbnail);
		}
		$destination = url('public/uploads/courses');
		
		$response['status']=1;
		$response['basepath']=$destination;
		$response['data']=$course;
		$response['msg'] = GeneralHelper::successMessage("Successfully update course details.");
		echo json_encode($response);
		exit;
  	}

  	public function updateCourse(){
		
		$data= Request::all();

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(isset($data['_token'])){
			unset($data['_token']);	
		}

		if(!isset($data['i_course_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$i_course_id="";
		if($data['i_course_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}
		$data['v_requirement'] =nl2br($data['v_requirement']);
		$data['l_description'] =nl2br($data['l_description']);
		$data['l_instructor_details'] =nl2br($data['l_instructor_details']);

		$i_course_id=$data['i_course_id'];
		unset($data['i_course_id']);

		if(isset($data['v_course_free'])){
			$data['f_price'] = 0;
		}

		$existdata = CoursesModel::find($i_course_id);

		if(!count($existdata)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}

		if(count(Request::file('v_instructor_image'))){
			$image = Request::file('v_instructor_image');
			if( count($image) ){
				if(isset($existdata->v_instructor_image) && $existdata->v_instructor_image!=""){
					if(Storage::disk('s3')->exists($existdata->v_instructor_image)){
						Storage::disk('s3')->Delete($existdata->v_instructor_image);
					}
				}
				$fileName = 'instructor_image-'.time().'.'.$image->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_instructor_image'] = 'courses/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/courses/'.$fileName, File::get((string)$image), 'public');

			}
		}

		if(count(Request::file('v_course_thumbnail'))){
			$image = Request::file('v_course_thumbnail');
			if(count($image)){
				$fileName = 'course_image-'.time().'.'.$image->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_course_thumbnail'] = 'courses/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/courses/'.$fileName, File::get((string)$image), 'public');
			}
		}

		if(count(Request::file('l_video'))){
			$video = Request::file('l_video');
			if( count($video) ){

				if(isset($existdata->l_video) && $existdata->l_video!=""){
					if(Storage::disk('s3')->exists($existdata->l_video)){
						Storage::disk('s3')->Delete($existdata->l_video);
					}
				}
				$fileName = 'introduction_video-'.time().'.'.$video->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['l_video'] = 'courses/video/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/courses/video/'.$fileName, File::get((string)$video), 'public');
			}
		}

		$data['f_price'] = (float)$data['f_price'];
		$data['d_modified']=date("Y-m-d H;i:s");	
		CoursesModel::find($i_course_id)->update($data);
		
		$response['status']=1;
		$response['msg'] = GeneralHelper::successMessage("Course details update successfully.");
		echo json_encode($response);
		exit;
	}


	

	

}