<?php
namespace App\Http\Controllers\Frontend\Courses;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesAnnouncements as CoursesAnnouncements;
use App\Models\Courses\CoursesQA as CoursesQA;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;


class SponsorCourses extends Controller {
	
	protected $section;

	public function __construct(){
		$this->section = Lang::get('section.SponsorCourses');
	}
		
	public function index() {
		
		$userid = auth()->guard('web')->user()->_id;
		$data = CoursesModel::where('i_user_id',$userid)->where('e_sponsor',"yes")->orderBy('d_modified','DESC')->get();
		
		$cids=array();
		if(count($data)){
			foreach($data as $key => $value) {
				$cids[]=$value->id;				
			}
		}
		
		$sponsoredSales=0;
		
		if(count($cids)){
			$courseorder = OrderModal::whereIn('i_course_id',$cids)->where('e_transaction_type','course')->where('v_sponser_sales','yes')->where('e_payment_status','success')->get();
			if(count($courseorder)){
				foreach ($courseorder as $key => $value) {
					$sponsoredSales = $sponsoredSales+$value->v_amount;			
				}
			}
		}
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

		$_data	= array(
			'data'=>$data,
			'plandata'=>$plandata,
			'discount'=>$discount,
			'sponsoredSales'=>$sponsoredSales,
		);
		
		return view('frontend/Courses/sponsor_courses_list',$_data);

	}

	public function Viewdata() {
		
		$data = Request::all();	

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after some time.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['i_course_id'])) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$userid = auth()->guard('web')->user()->_id;
		$cdata = CoursesModel::where('_id',$data['i_course_id'])->where('e_sponsor',"yes")->orderBy('d_modified','DESC')->first();
		if(!count($cdata)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$sponsoredSales=0;
		$courseorder = OrderModal::where('i_course_id',$data['i_course_id'])->where('e_transaction_type','course')->where('v_sponser_sales','yes')->where('e_payment_status','success')->get();
		if(count($courseorder)){
			foreach ($courseorder as $key => $value) {
				$sponsoredSales = $sponsoredSales+$value->v_amount;			
			}
		}

		$response['status']=1;
		$response['msg']="";
		$response['sales']='£'.$sponsoredSales;
		$response['click']=$cdata->i_clicks;
		$response['impression']=$cdata->i_impression;
		echo json_encode($response);
		exit;




		
		
		
		

	}


	public function addSponsor(){

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after some time.";
			echo json_encode($response);
			exit;
		}

		$user_id = auth()->guard('web')->user()->id;
		$data = CoursesModel::where('i_user_id',$user_id)->where('e_sponsor',"no")->get();

		$response=array();

		$newcourse="";
		if(count($data)){
			foreach ($data as $key => $val) {

				$newcourse .='<tr>';
		        $newcourse .='<td>'.$val->v_title.'</td>';
		        $newcourse .='<td>'.date("d/m/Y",strtotime($val->d_added)).'</td>';
		        $newcourse .='<td><button type="button" class="btn btn-jobpost" onclick="SponsorNowModal';
		        $newcourse .="('".$val->id."')";
		        $newcourse .='")>Select</button></td>';
		      	$newcourse .='</tr>';
		   	}

		}else{
				$newcourse .='<tr>';
		        $newcourse .='<td colspan="3"> Currently, there are no Courses available..</td>';
		    	$newcourse .='</tr>';
		}

		$response['status']=1;
		$response['htmlstr']=$newcourse;
		echo json_encode($response);
		exit;

	}


	public function addSponsorCourses(){
				
		$data = Request::all();

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!isset($data['i_course_id'])) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$user_id = auth()->guard('web')->user()->id;
		$userplan = GeneralHelper::userCurrentPlan($user_id);

		if(!count($userplan)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(isset($userplan['isBasic']) && $userplan['isBasic']==1){

			$response['status']=2;
			$msg="Your current plan is a basic plan. To run sponsored ads please upgrade your plan to standard or premium. <a href='javascript:;' onclick='upgradeUserPlan";
			$msg.='("'.$data['i_course_id'].'")';
			$msg.="'>Upgrade Now </a>.";
			$response['msg']=GeneralHelper::errorMessage($msg);
			echo json_encode($response);
			exit;	
		}

		$update=array(
			'e_sponsor'=>"yes",
			'e_sponsor_status'=>"start",
			'd_modified'=>date("Y-m-d H:i:s"),
		);
		CoursesModel::find($data['i_course_id'])->update($update);

		$response['status']=1;
		$response['msg']= GeneralHelper::successMessage("Successfully sponsored course.");
		echo json_encode($response);
		exit;

	}

	public function upgradeUserPlan(){

		$data = Request::all();
	
		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'sponsor-courses',
			'v_action'=>'upgrade user plan',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],
			'i_course_id'=>$data['i_payment_cid'],			
		);

		$userdata = auth()->guard('web')->user();
		$upgrade = GeneralHelper::upgradeUserPlanDetail($userdata,$data['v_plan']['id']);

		if($upgrade){
			Session::put('ordersummary',$orderdata);
			return redirect('payment');
		}else{
			return redirect('sponsor-courses')->with(['success' => 'Successfully upgrade your plan.']);
		}

		// $userid = auth()->guard('web')->user()->id;
		// $userdata = UsersModel::find($userid);
		// $update['seller'] = $userdata->seller;
		// $update['seller']['e_status'] = "active";
		// $update['d_modified']=date("Y-m-d H:i:s");
		// $update['v_plan']=$data['v_plan'];
		// UsersModel::find($userid)->update($update);
		// return redirect('sponsor-courses')->with(['success' => 'Successfully upgrade your plan.']);

	}

	public function statusSponsorCourses(){
		
		$data = Request::all();

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!isset($data['i_course_id'])) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		if(!isset($data['status'])) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$update=array(
			'e_sponsor_status'=>$data['status'],
			'd_modified'=>date("Y-m-d H:i:s"),
		);
		$a = CoursesModel::find($data['i_course_id'])->update($update);

		$response['msg']="";
		$btnstr="";
		if($data['status']=="start"){

			$btnstr.='<button type="button" class="btn btn-pause" onclick = "SponsoreStatusChange';
			$btnstr.="('".$data['i_course_id']."','pause')";
			$btnstr.='")>Pause</button>';

			$response['msg'] = GeneralHelper::successMessage("Your sponsored ad successfully started.");

		}else{

			$btnstr.='<button type="button" class="btn btn-start" onclick = "SponsoreStatusChange';
			$btnstr.="('".$data['i_course_id']."','start')";
			$btnstr.='")>Start</button>';
			$response['msg'] = GeneralHelper::successMessage("Your sponsored ad successfully paused.");

		}	

		$response['status']=1;
		//$response['msg'] = GeneralHelper::successMessage("Thank you, your message is now sponsored.");
		$response['btnstr'] = $btnstr;
		$response['a'] = $a;
		echo json_encode($response);
		exit;

	}

}