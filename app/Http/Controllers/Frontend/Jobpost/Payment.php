<?php
namespace App\Http\Controllers\Frontend\Jobpost;

use Request, Hash, Lang,Validator,Auth,Storage,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\ShortlistedCourse as ShortlistedCourseModel;
use App\Models\Users\ShortlistedJobs as ShortlistedJobsModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Skills as SkillsModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;

class Payment extends Controller {
		
	protected $section;

    public function __construct(\MangoPay\MangoPayApi $mangopay){
        $this->section = Lang::get('section.Payment');
        $this->mangopay = $mangopay;
    }

	
	public function index() {

		$ordersummary = Session::get('ordersummary');
        $plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

        if(!count($ordersummary)){
			return redirect('job-postings')->with(['warning' => 'Something went wrong']);
		} 

        $data['v_plan_charge']="";
        $data['v_total_amount']=0;
        $data['v_plan_name']="";
        $data['v_plan_duration']="";

        if(isset($ordersummary['v_plan_charge']) && $ordersummary['v_plan_charge']!="yes"){
            $data['v_plan_charge']="no";
            
            if(isset($ordersummary['v_addon']) && count($ordersummary['v_addon'])){
                $data['v_addon']=$ordersummary['v_addon'];
                
                foreach($ordersummary['v_addon'] as $key => $value) {
                   $data['v_total_amount']= $data['v_total_amount']+$value;            
                }
            }            
        }else{

            $data['v_plan_charge']="yes";

            if(isset($ordersummary['v_plan_id']) && $ordersummary['v_plan_id']!=""){    
                if(count($plandata)){
                    foreach ($plandata as $key => $value) {
                            
                        if($value['id'] == $ordersummary['v_plan_id']){
                            $data['v_plan_name'] = $value['v_name'];
                            $data['v_plan_duration'] = $ordersummary['v_plan_duration'];
                            if(isset($ordersummary['v_plan_duration']) && $ordersummary['v_plan_duration']=="monthly"){
                                $data['v_total_amount'] = $data['v_total_amount']+$value['f_monthly_dis_price'];
                            }else{
                                $data['v_total_amount'] = $data['v_total_amount']+$value['f_yealry_dis_price'];
                            }
                        }

                    }
                }
            }

            if(isset($ordersummary['v_addon']) && count($ordersummary['v_addon'])){
                $data['v_addon']=$ordersummary['v_addon'];
                
                foreach($ordersummary['v_addon'] as $key => $value) {
                   $data['v_total_amount']= $data['v_total_amount']+$value;            
                }
            }

        }
        $_data = array(
			'data'=>$data
		);
		return view('frontend/jobpost/payment',$_data);

    }

    public function paymentWithMangoPay(){

        $ordersummary = Session::get('ordersummary');   
        
        if(!count($ordersummary)){
            return redirect('job-postings')->with(['warning' => 'Something went wrong']);
        }

        $plandata = GeneralHelper::PlansList();
        $discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

        $data['v_plan_charge']="";
        $data['v_total_amount']=0;
        $data['v_plan_name']="";
        $data['v_plan_duration']="";

        if(isset($ordersummary['v_plan_charge']) && $ordersummary['v_plan_charge']!="yes"){
            $data['v_plan_charge']="no";
            
            if(isset($ordersummary['v_addon']) && count($ordersummary['v_addon'])){
                $data['v_addon']=$ordersummary['v_addon'];
                
                foreach($ordersummary['v_addon'] as $key => $value) {
                   $data['v_total_amount'] = $data['v_total_amount']+$value;            
                }
            }            
        }else{

            $data['v_plan_charge']="yes";

            if(isset($ordersummary['v_plan_id']) && $ordersummary['v_plan_id']!=""){    
                if(count($plandata)){
                    foreach ($plandata as $key => $value) {
                            
                        if($value['id'] == $ordersummary['v_plan_id']){
                            $data['v_plan_name'] = $value['v_name'];
                            $data['v_plan_duration'] = $ordersummary['v_plan_duration'];
                            if(isset($ordersummary['v_plan_duration']) && $ordersummary['v_plan_duration']=="monthly"){
                                $data['v_total_amount'] = $data['v_total_amount']+$value['f_monthly_dis_price'];
                            }else{
                                $data['v_total_amount'] = $data['v_total_amount']+$value['f_yealry_dis_price'];
                            }
                        }

                    }
                }
            }

            if(isset($ordersummary['v_addon']) && count($ordersummary['v_addon'])){
                $data['v_addon']=$ordersummary['v_addon'];
                foreach($ordersummary['v_addon'] as $key => $value) {
                   $data['v_total_amount']= $data['v_total_amount']+$value;            
                }
            }else{
                $data['v_addon']=array();
            }

        }

        $currency = "GBP";
        //$creditedwalletid=52542003;

         $creditedwalletid = GeneralHelper::getSiteSetting('MANGOPAY_USER_WALLET_ID');
        if($creditedwalletid!=""){
            $creditedwalletid = (int)$creditedwalletid;  
        }else{
            $creditedwalletid=55569248;  
        }

        $authorId = "";
        if(isset(auth()->guard('web')->user()->i_mangopay_id)){
            $authorId = auth()->guard('web')->user()->i_mangopay_id;
        }
        $userid = auth()->guard('web')->user()->id;

        $mangoamt = $data['v_total_amount']*100;

        $PayIn = new \MangoPay\PayIn();
        $PayIn->CreditedWalletId = (int)$creditedwalletid;
        $PayIn->AuthorId = (int)$authorId;
        $PayIn->PaymentType = "CARD";
        $PayIn->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
        $PayIn->PaymentDetails->CardType = "CB_VISA_MASTERCARD";
        $PayIn->DebitedFunds = new \MangoPay\Money();
        $PayIn->DebitedFunds->Currency = $currency;
        $PayIn->DebitedFunds->Amount = (float)$mangoamt;
        
        $PayIn->Fees = new \MangoPay\Money();
        $PayIn->Fees->Currency = "GBP";
        $PayIn->Fees->Amount = 0;

        $PayIn->ExecutionType = "WEB";
        $PayIn->ExecutionDetails = new \MangoPay\PayInExecutionDetailsWeb();
        $PayIn->ExecutionDetails->ReturnURL = url('job-post/payment/mangopay-return');
        $PayIn->ExecutionDetails->Culture = "EN";
        $result = $this->mangopay->PayIns->Create($PayIn);

        if(!count($result)){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }   

        $insert['i_user_id']=$userid;
        $insert['i_order_no']=GeneralHelper::OrderRefNumbaer();
        
        if($data['v_plan_charge']=="yes"){
            $insert['e_transaction_type']="userplan";
        }else{
            $insert['e_transaction_type']="jobpostaddon";
        }
        
        $insert['e_payment_type']="mangopay";
        $insert['v_transcation_id']=$result->Id;
        $insert['e_payment_status']="pending";
        $insert['v_amount']=$data['v_total_amount'];
        $insert['v_order_detail']=array();

        if($data['v_plan_charge']=="yes"){
            
            $insert['v_order_detail']['v_plan_id'] = $ordersummary['v_plan_id'];
            $insert['v_order_detail']['v_plan_name']=$data['v_plan_name'];    
            $insert['v_order_detail']['v_plan_duration']=$data['v_plan_duration'];

            if(count($data['v_addon'])){
                foreach ($data['v_addon'] as $key => $value) {
                    $insert['v_order_detail']['v_addon_name'] = $key;        
                    $insert['v_order_detail']['v_addon_amt'] = $value;        
                }
            }
            $insert['v_order_detail']['v_action'] = $ordersummary['redirect'];
            $insert['v_order_detail']['i_job_id'] = $ordersummary['i_job_id'];

        }else{
                
            if(count($data['v_addon'])){
                foreach ($data['v_addon'] as $key => $value) {
                    $insert['v_order_detail'][$key] = $value;        
                }
            }
            $insert['v_order_detail']['v_action'] = $ordersummary['redirect'];
            $insert['v_order_detail']['i_job_id'] = $ordersummary['i_job_id'];
        }

        $insert['d_date'] = date("Y-m-d");
        $insert['d_added'] = date("Y-m-d H:i:s");
        $insert['d_modified'] = date("Y-m-d H:i:s");

        OrderModal::create($insert);
        header('Location: '.$result->ExecutionDetails->RedirectURL);
        exit;
    }

    public function getMangoPaymentReturn(){

        $ordersummary = Session::get('ordersummary');
        $response = Request::all();
        
        if(!isset($response['transactionId'])){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }
        if($response['transactionId']==""){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }

        $orderdata = OrderModal::where('v_transcation_id',$response['transactionId'])->first();
        if(!count($orderdata)){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }

        $result = $this->mangopay->PayIns->Get($response['transactionId']);

        if(!count($result)){
            return redirect('/')->with( 'warning', 'Something went wrong.');
        }

        $thankspage = array(
            'rurl'=>url('dashboard'),
            'issuccess'=>0,
        );

        if($result->Status=="SUCCEEDED"){

            if(isset($orderdata->v_order_detail['v_plan_id'])){

                $update['v_plan']['id']=$orderdata->v_order_detail['v_plan_id'];
                $update['v_plan']['duration']=$orderdata->v_order_detail['v_plan_duration'];
                $expdate="";
                if($orderdata->v_order_detail['v_plan_duration']=="monthly"){
                    $expdate=date('Y-m-d',strtotime('+30 days'));
                }else{
                    $expdate=date('Y-m-d',strtotime('+365 days'));
                }
                $update['v_plan']['d_start_date']=date("Y-m-d H:i:s");
                $update['v_plan']['d_end_date']=$expdate;
                $update['d_modified'] = date("Y-m-d H:i:s");
                UsersModel::find($orderdata->i_user_id)->update($update);     

            }

            $updatejob=0;
            $updatejobpost=array();
            if(isset($orderdata->v_order_detail['jobposturgent'])){
                $updatejobpost['e_urgent']="yes";
                $updatejob=1;
            }
            if(isset($orderdata->v_order_detail['jobpostnotify'])){
                $updatejobpost['e_notify']="yes";
                $updatejob=1;
                GeneralHelper::JobNotifyEmailNotificationSingle($orderdata->v_order_detail['i_job_id']);
            }

            if($updatejob==1 && isset($orderdata->v_order_detail['i_job_id'])){
                JobsModel::find($orderdata->v_order_detail['i_job_id'])->update($updatejobpost);
            }

            $updateorder['e_payment_status']="success";
            $updateorder['d_modified']=date("Y-m-d H:i:s");
            OrderModal::find($orderdata->id)->update($updateorder);
            $thankspage['issuccess']=1;
        }

        $ordersummary = Session::get('ordersummary');
        Session::forget('ordersummary');
        if(isset($ordersummary['redirect'])){
            Session::forget('ordersummary');
            $thankspage['rurl']=$ordersummary['redirect'];
            //return redirect($ordersummary['redirect']); 
        }
        Session::put('thankspage',$thankspage);
        return redirect('thanks');

        //return redirect('dashboard');
    }

   

    
    /*
	public function paymentWithPaypal(){

		$ordersummary = Session::get('ordersummary');

        if(!count($ordersummary)){
            return redirect('job-postings')->with(['warning' => 'Something went wrong']);
        }

		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");


        $data['v_plan_charge']="";
        $data['v_total_amount']=0;
        $data['v_plan_name']="";
        $data['v_plan_duration']="";

        if(isset($ordersummary['v_plan_charge']) && $ordersummary['v_plan_charge']!="yes"){
            $data['v_plan_charge']="no";
            
            if(isset($ordersummary['v_addon']) && count($ordersummary['v_addon'])){
                $data['v_addon']=$ordersummary['v_addon'];
                
                foreach($ordersummary['v_addon'] as $key => $value) {
                   $data['v_total_amount']= $data['v_total_amount']+$value;            
                }
            }            
        }else{

            $data['v_plan_charge']="yes";

            if(isset($ordersummary['v_plan_id']) && $ordersummary['v_plan_id']!=""){    
                if(count($plandata)){
                    foreach ($plandata as $key => $value) {
                            
                        if($value['id'] == $ordersummary['v_plan_id']){
                            $data['v_plan_name'] = $value['v_name'];
                            $data['v_plan_duration'] = $ordersummary['v_plan_duration'];
                            if(isset($ordersummary['v_plan_duration']) && $ordersummary['v_plan_duration']=="monthly"){
                                $data['v_total_amount'] = $data['v_total_amount']+$value['f_monthly_dis_price'];
                            }else{
                                $data['v_total_amount'] = $data['v_total_amount']+$value['f_yealry_dis_price'];
                            }
                        }

                    }
                }
            }

            if(isset($ordersummary['v_addon']) && count($ordersummary['v_addon'])){
                $data['v_addon']=$ordersummary['v_addon'];
                
                foreach($ordersummary['v_addon'] as $key => $value) {
                   $data['v_total_amount']= $data['v_total_amount']+$value;            
                }
            }

        }

        $currency = "EUR";
        

        $paypalid = GeneralHelper::getSiteSetting("PAYPAL_EMIAL");
        $paypalmode = GeneralHelper::getSiteSetting("PAYPAL_MODE");

        $paypalurl="https://www.sandbox.paypal.com/cgi-bin/webscr";
        if($paypalmode=="live"){
            $paypalurl="https://www.paypal.com/cgi-bin/webscr";
        }else{
            $paypalurl="https://www.sandbox.paypal.com/cgi-bin/webscr";
        }

        $userid = auth()->guard('web')->user()->id;
        
        $_data=array(
            'paypalURL' =>$paypalurl,
            'paypalID'  => $paypalid,
            'custom' => "no",
            'amount'    => $data['v_total_amount'],
            'currency_code' => $currency,
            'cancel_return' => url('job-post/payment/paypal-cancel'),
            'return_url' => url('job-post/payment/paypal-return'),
            'cmd' => '_xclick',
        );

        if(isset($ordersummary['v_plan_charge']) && $ordersummary['v_plan_charge']!="yes"){
            $_data['item_name'] = "Post job addon charge";
            $_data['item_number'] = $userid.'#'.$ordersummary['i_job_id'];

        }else{
            $_data['item_name'] = 'Skillbox Subscription of '.$data['v_plan_name'].' Membership charge';
            $_data['item_number'] = $userid.'#'.$ordersummary['i_job_id'].'#'.$ordersummary['v_plan_id'].'#'.$data['v_plan_name'].'#'.$data['v_plan_duration'];

        }
        return view('frontend/jobpost/paypal-payment',$_data);
    
    }

    public function getPaypalReturn() {

        $response = Request::all();
        if(!count($response)){
          return redirect('/')->with( 'warning', 'Something went wrong.');
	    }
        $oldpaymentdata = OrderModal::where("v_transcation_id",$response['txn_id'])->first();

        $orderdata=array();
        if($response['item_number']!=""){
            $orderdata = explode("#", $response['item_number']);
        }
	    
        if(!count($oldpaymentdata)){

            $ordersummary = Session::get('ordersummary');

            $orderdata=array();
        	if($response['item_number']!=""){
        		$orderdata = explode("#", $response['item_number']);
        	}
            
            $insert = array();
	       	if(isset($orderdata[0])){
				$insert['i_user_id']=$orderdata[0];
        	}

        	$insert['e_transaction_type']="userplan";
        	$insert['e_payment_type']="paypal";
        	$insert['v_transcation_id']=$response['txn_id'];

        	if($response['payment_status']=="Pending"){
        		$insert['e_payment_status']="pending";
        	}else if($response['payment_status']=="Completed" || $response['payment_status']=="Processed"){
        	  	
                $insert['e_payment_status']="success";

                if(isset($orderdata[0]) && isset($orderdata[1])){
        			
                    if(isset($ordersummary['v_plan_charge']) && $ordersummary['v_plan_charge']!="yes"){
                          
                          $jobdata = JobsModel::find($orderdata[1]);
                          $updatejob['e_urgent']="yes";
                          $updatejob['e_notify']="yes";
                          $updatejob['d_modified']=date("Y-m-d H:i:s");

                          if(count($jobdata)){
                                $jobdata::find($orderdata[1])->update($updatejob);
                          }

                    }else{

                        $update['v_plan']['id']=$orderdata[2];
                        $update['v_plan']['duration']=$orderdata[4];
                        $expdate="";
                        
                        if($orderdata[4]=="monthly"){
                            $expdate=date('Y-m-d',strtotime('+30 days'));
                        }else{
                            $expdate=date('Y-m-d',strtotime('+365 days'));
                        }

                        $update['v_plan']['d_start_date']=date("Y-m-d H:i:s");
                        $update['v_plan']['d_end_date']=$expdate;
                        $update['d_modified'] = date("Y-m-d H:i:s");
                        UsersModel::find($orderdata[0])->update($update);

                        if(isset($ordersummary['v_addon']) && count($ordersummary['v_addon'])){

                            $jobdata = JobsModel::find($orderdata[1]);
                            $updatejob['e_urgent']="yes";
                            $updatejob['e_notify']="yes";
                            $updatejob['d_modified']=date("Y-m-d H:i:s");

                            if(count($jobdata)){
                                $jobdata::find($orderdata[1])->update($updatejob);
                            }

                        }

                    }
               	}

        	}else{
        		$insert['e_payment_status']=$response['payment_status'];
        	}

        	$insert['v_amount']=$response['payment_gross'];
        	
            $insert['v_order_detail']=array();
            $insert['v_order_detail']['v_plan_id']="";
            $insert['v_order_detail']['v_plan_name']="";
            $insert['v_order_detail']['v_plan_duration']="";
            $insert['v_order_detail']['v_addon'] = "";
            $insert['v_order_detail']['i_job_id'] = $ordersummary['i_job_id'];
            
            if(isset($ordersummary['v_addon']) && count($ordersummary['v_addon'])){
                $insert['v_order_detail']['v_addon'] = $ordersummary['v_addon'];
            }            

            if(isset($ordersummary['v_plan_charge']) && $ordersummary['v_plan_charge']!="yes"){
                $insert['v_order_detail']['v_action'] = "add on charge";
                $insert['v_order_detail']['v_plan_charge'] = "no";
            }else{
                $insert['v_order_detail']['v_action'] = "upgrade user plan with job post";
                $insert['v_order_detail']['v_plan_charge'] = "yes";
                
                if(isset($orderdata[2])){
                    $insert['v_order_detail']['v_plan_id']=$orderdata[2];
                }
                if(isset($orderdata[3])){
                    $insert['v_order_detail']['v_plan_name']=$orderdata[3];
                }
                if(isset($orderdata[4])){
                    $insert['v_order_detail']['v_plan_duration']=$orderdata[4];
                }

            }

            $insert['d_date']=date("Y-m-d");
        	$insert['d_added']=date("Y-m-d H:i:s");
        	$insert['d_modified']=date("Y-m-d H:i:s");
        	OrderModal::create($insert);

        }else{

        	$updateorder=array();

        	if($response['payment_status']=="Pending"){
        		$updateorder['e_payment_status']="pending";
        	}else if($response['payment_status']=="Completed" || $response['payment_status']=="Processed"){
        		
        		$updateorder['e_payment_status']="success";
                $userdata = UsersModel::find($orderdata[0]);

                $orderdetail = $oldpaymentdata->v_order_detail;

                if($orderdetail['v_plan_charge']!="yes"){

                    $jobdata = JobsModel::find($orderdata[1]);
                    $updatejob['e_urgent']="yes";
                    $updatejob['e_notify']="yes";
                    $updatejob['d_modified']=date("Y-m-d H:i:s");
                    if(count($jobdata)){
                        $jobdata::find($orderdata[1])->update($updatejob);
                    }

                }else{

                    $update['v_plan']['id']=$orderdata[2];
                    $update['v_plan']['duration']=$orderdata[4];
                    $expdate="";
                    
                    if($orderdata[4]=="monthly"){
                        $expdate=date('Y-m-d',strtotime('+30 days'));
                    }else{
                        $expdate=date('Y-m-d',strtotime('+365 days'));
                    }
                    $update['v_plan']['d_start_date']=date("Y-m-d H:i:s");
                    $update['v_plan']['d_end_date']=$expdate;
                    $update['d_modified'] = date("Y-m-d H:i:s");
                    UsersModel::find($orderdata[0])->update($update);

                    if(isset($orderdetail['v_addon']) && count($orderdetail['v_addon'])){

                        $jobdata = JobsModel::find($orderdata[1]);
                        $updatejob['e_urgent']="yes";
                        $updatejob['e_notify']="yes";
                        $updatejob['d_modified']=date("Y-m-d H:i:s");
                        if(count($jobdata)){
                            $jobdata::find($orderdata[1])->update($updatejob);
                        }
                    }
                }

        	}else{
        		$updateorder['e_payment_status']=$response['payment_status'];
        	}
        	$updateorder['d_modified']=date("Y-m-d H:i:s");
        	OrderModal::where('v_transcation_id',$response['txn_id'])->update($insert);
        }


        $ordersummary = Session::get('ordersummary');
        if(isset($ordersummary['redirect'])){
        	Session::forget('ordersummary');
        	return redirect($ordersummary['redirect']);	
        }
        return redirect('job-postings');
    }

    public function getPaypalCancel() {
    	dd("Payment Cancel");	
    }
    */


}