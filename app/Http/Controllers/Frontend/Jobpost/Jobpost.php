<?php
namespace App\Http\Controllers\Frontend\Jobpost;

use Request, Hash, Lang,Storage,File,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Skills as SkillsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;


class Jobpost extends Controller {

	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.dashboard');
	}

	public function index(){

		if(!auth()->guard('web')->check()) {
			return redirect('account/job-post');
		}

		$userdata = auth()->guard('web')->user();
		$jobrestriction = GeneralHelper::jobpostingrestriction($userdata);
		$isPremiumPlan = GeneralHelper::checkUserjobpostplan($userdata);

		$userplan = array();
		if(isset($userdata->v_plan) && count($userdata->v_plan)){
			$userplan = $userdata->v_plan;
		}

		$userservice = auth()->guard('web')->user()->buyer['v_service'];
		$category = CategoriesModel::where("v_type",$userservice)->where("e_status","active")->orderBy('v_name')->get();
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");
		$userid = auth()->guard('web')->user()->_id;
		
		if($jobrestriction == 0){

			$userplandata = GeneralHelper::userPlanDetail($userdata);		
			$userid = auth()->guard('web')->user()->_id;
			
			$data=array(
				'plandata'=>$plandata,
				'discount'=>$discount,
				'isPremiumPlan'=>$isPremiumPlan,
				'userplan'=>$userplan,
				'userplandata'=>$userplandata
			);
			return view('frontend/jobpost/upgrade_plan',$data);
		}

		$data=array(
			'category'=>$category,
			'v_service'=>$userservice,
			'plandata'=>$plandata,
			'discount'=>$discount,
			'userplan'=>$userplan,
			'isPremiumPlan'=>$isPremiumPlan,
		);
		return view('frontend/jobpost/post_job',$data);
	}

	public function updateStep1(){

		$data = Request::all();
		$response = array();

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(isset($data['_token'])){
			unset($data['_token']);	
		}

		if(isset($data['tstep1jobid']) && $data['tstep1jobid']!=""){

			$job_id = $data['tstep1jobid'];
			unset($data['tstep1jobid']);
			
			$data['d_modified']=date("Y-m-d H:i:s");	
			JobsModel::find($job_id)->update($data);
			$response['status']=1;
			$response['jobid']=$job_id;
			$response['msg']="succesfully update information.";
			echo json_encode($response);
			exit;
		}
	
		$userid = auth()->guard('web')->user()->_id;
		$data['d_expiry_date'] = date('Y-m-d',strtotime('+30 days'));
		$data['i_total_avg_review']=0;
	  	$data['i_total_review']=0;
	  	$data['i_applied']=0;

	  	$data['e_status']="inactive";
		$data['d_added']=date("Y-m-d H:i:s");
		$data['d_modified']=date("Y-m-d H:i:s");
		$data['i_user_id']=$userid;
		$data['d_signup_date']=date("Y-m-d",strtotime(auth()->guard('web')->user()->d_added));	
		$jobid = JobsModel::create($data)->id;

		$response['status']=1;
		$response['jobid']=$jobid;
		$response['msg']="succesfully update information.";
		echo json_encode($response);
		exit;
	}

	public function updateStep2(){

		$data = Request::all();
		$response = array();

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$userid = auth()->guard('web')->user()->_id;
		$jobid = "";

		if(isset($data['v_job_id']) && $data['v_job_id']!=""){
			$jobid = $data['v_job_id'];
			unset($data['v_job_id']);
		}else{
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(isset($data['_token'])){
			unset($data['_token']);	
		}

		if(isset($data['v_budget_amt'])){
			$data['v_budget_amt'] = (float)$data['v_budget_amt'];
		}

		if(isset($data['d_start_date']) && $data['d_start_date']!=""){
			$data['v_start_year']=date("Y",strtotime($data['d_start_date']));
			$data['v_start_month']=date("m",strtotime($data['d_start_date']));
			$data['v_start_date']=date("d",strtotime($data['d_start_date']));
		}

		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){
			$lat=(float)$data['v_latitude'];
			$lont=(float)$data['v_longitude'];
			$data['location']['type']="Point";
			$data['location']['coordinates']=array($lont,$lat);
		}

		$data['d_expiry_date'] = date('Y-m-d',strtotime('+30 days'));
	  	$data['i_total_avg_review']=0;
	  	$data['i_total_review']=0;
	  	$data['i_applied']=0;
	  	$data['e_sponsor']="no";
	  	$data['e_status']="active";
	  	$data['d_modified']=date("Y-m-d H:i:s");

	  	JobsModel::find($jobid)->update($data);

		$mailData['USER_FULL_NAME']="";
		$mailData['USER_EMAIL']="";
		$mailData['JOB_PREVIEW_LINK']=url('online-job').'/'.$jobid.'/'.$data['v_job_title'];

		if(isset(auth()->guard('web')->user()->v_fname)){
			$mailData['USER_FULL_NAME'] = auth()->guard('web')->user()->v_fname;			
		}
		if(isset(auth()->guard('web')->user()->v_lname)){
			$mailData['USER_FULL_NAME'] = $mailData['USER_FULL_NAME'].' '.auth()->guard('web')->user()->v_lname;	
		}
		if(isset(auth()->guard('web')->user()->v_email)){
			$mailData['USER_EMAIL'] = auth()->guard('web')->user()->v_email;			
		}
		
		$mailcontent = EmailtemplateHelper::JobpostLive($mailData);
		$subject = "Job post preview";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b55fe15516b462cb0001aaa");
		
		$mailids=array(
			$mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

		$response['status']=1;
		$response['id']=$jobid;
		$response['msg']="succesfully update information.";
		echo json_encode($response);
		exit;
	}

	public function listJobpost(){
		
		if(!auth()->guard('web')->check()) {
			return redirect('/');
		}
		
		$user_id = auth()->guard('web')->user()->id;
		$userservice = auth()->guard('web')->user()->buyer['v_service'];

		$joblist = JobsModel::where('i_user_id',$user_id)->where('v_service',$userservice)->get();
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

		$userdata = auth()->guard('web')->user();
		$userplandata = GeneralHelper::userPlanDetail($userdata);		

		$headerdata['ACTIVE_POSTINGS']=0;
		$headerdata['EXPIRED_POSTINGS']=0;
		$headerdata['SPONSORED_ADS']=0;

		$currentdate=date("Y-m-d");

		if(count($joblist)){
			foreach ($joblist as $key => $value) {
				
				if(strtotime($value->d_expiry_date) < strtotime($currentdate)){
					if($value->e_status=="active"){
					$headerdata['EXPIRED_POSTINGS']=$headerdata['EXPIRED_POSTINGS']+1;;
					}
				}else{
					if($value->e_status=="active"){
						$headerdata['ACTIVE_POSTINGS']=$headerdata['ACTIVE_POSTINGS']+1;;
					}
				}	

				if(isset($value->e_sponsor) && $value->e_sponsor=="yes"){
					$headerdata['SPONSORED_ADS']=$headerdata['SPONSORED_ADS']+1;;
				}

			}
		}

		$data=array(
			'joblist'=>$joblist,
			'headerdata'=>$headerdata,
			'plandata'=>$plandata,
			'discount'=>$discount,
			'userplandata'=>$userplandata,
			'userservice'=>$userservice
		);
		return view('frontend/jobpost/jobpost_list',$data);
	}

	public function uploadVideo($id=""){

		$data = Request::all();
		$existJob = JobsModel::find($id);
		if(!count($existJob)){
			return 0;
		}
		if(isset($existJob->v_video)){
			$update['v_video']=$existJob->v_video;
		}

		if(Request::hasFile('file')) {
			$image = Request::file('file');
			$fileName = 'job_video-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$update['v_video'][] = 'jobpost/videos/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/jobpost/videos/'.$fileName, File::get((string)$image), 'public');
			JobsModel::find($id)->update($update);
		}

		$appendstr="";
		$appendstr.='<div class="dz-default dz-message"><span>Drop files here to upload</span></div>';
		foreach($update['v_video'] as $k=>$v){
			
			$videodata = \Storage::cloud()->url($v);     
			$appendstr.='<div class="dropzone-previews" style="width: auto;display: inline-block;" id="rem'.$k.'">';
			$appendstr.='<div class="dz-preview dz-processing dz-success dz-image-preview">';
			$appendstr.='<div class="dz-details"> ';
			$appendstr.='<video width="100px" height="100px" controls>';
			$appendstr.='<source src="'.$videodata.'" type="video/mp4">';
			$appendstr.='</video> ';
			$appendstr.='</div>';
			$appendstr.='<a href="javascript:" onclick="remove_Video';
			$appendstr.="('".$v."','".$k."','".$id."')";
			$appendstr.='"class="dz-remove">Remove video</a></div>';
			$appendstr.='</div>';

		}	
		return $appendstr;
	}
	
	public function removeAjaxVideo(){

		$data = Request::all();
		$existJob = JobsModel::find($data['jid']);
		if(!count($existJob)){
			return 0;
		}

		if(isset($existJob->v_video) && count($existJob->v_video)){
			if(Storage::disk('s3')->exists($data['name'])){
				Storage::disk('s3')->Delete($data['name']);
			}
			$updatedata['v_video']=array();
			foreach ($existJob->v_video as $key => $value) {
				if($data['name']!=$value){
					$updatedata['v_video'][]=$value;
				}
			}
			JobsModel::find($existJob->id)->update($updatedata);
		}
		return 1;
	}

	public function uploadPhotos($id=""){

		$data = Request::all();
		$existJob = JobsModel::find($id);
		if(!count($existJob)){
			return 0;
		}
		if(isset($existJob->v_photos)){
			$update['v_photos'] = $existJob->v_photos;
		}
		if(Request::hasFile('file')) {
			sleep(1);
			$image = Request::file('file');
			$fileName = 'job-photo-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$update['v_photos'][] = 'jobpost/photos/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/jobpost/photos/'.$fileName, File::get((string)$image), 'public');
			JobsModel::find($id)->update($update);
		}

		$appendstr="";
		$appendstr.='<div class="dz-default dz-message"><span>Drop files here to upload</span></div>';
		foreach($update['v_photos'] as $k=>$v){
			$imgdaata = \Storage::cloud()->url($v);	
			$appendstr.='<div class="dropzone-previews" style="width: auto;display: inline-block;" id="remph'.$k.'">';
			$appendstr.='<div class="dz-preview dz-processing dz-success dz-image-preview">';
			$appendstr.='<div class="dz-details"> ';
			$appendstr.='<img src="'.$imgdaata.'" style="max-width:100px;">';
			$appendstr.='</div>';
			$appendstr.='<a href="javascript:" onclick="remove_Photos';
			$appendstr.="('".$v."','".$k."','".$id."')";
			$appendstr.='"class="dz-remove">Remove Image</a></div>';
			$appendstr.='</div>';
		}
    	return $appendstr;

    }
	
	public function removeAjaxPhotos(){

		$data = Request::all();
		$existJob = JobsModel::find($data['jid']);
		if(!count($existJob)){
			return 0;
		}

		if(isset($existJob->v_photos) && count($existJob->v_photos)){
			if(Storage::disk('s3')->exists($data['name'])){
				Storage::disk('s3')->Delete($data['name']);
			}
			$updatedata['v_photos']=array();
			foreach ($existJob->v_photos as $key => $value) {
				if($data['name']!=$value){
					$updatedata['v_photos'][]=$value;
				}
			}
			JobsModel::find($existJob->id)->update($updatedata);
		}
		return 1;
	}

	public function RemoveSellerProfileWorkVideo(){

		$data = Request::all();
		
		$userid = auth()->guard('web')->user()->_id;
		$v_service = auth()->guard('web')->user()->seller['v_service'];
		$existProfile = SellerprofileModel::where('i_user_id',$userid)->where('v_service',$v_service)->first();
		if(!count($existProfile)){
			return 0;
		}
		if(isset($existProfile->v_work_video) && $existProfile->v_work_video!=""){
			if(Storage::disk('s3')->exists($existProfile->v_work_video)){
				Storage::disk('s3')->Delete($existProfile->v_work_video);
			}
		}
		$update['v_work_video'] = '';
		SellerprofileModel::find($existProfile->id)->update($update);
		return 1;
	}


	public function removeImage(){

		$data = Request::all();
		$response = array();

		$userid = auth()->guard('web')->user()->_id;
		$jobid = "";

		if(!isset($data['i_job_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['i_job_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['v_photo_name'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['v_photo_name']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$jobdata = JobsModel::find($data['i_job_id']);

		if(!count($jobdata)){

			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}


		$update['v_photos']=array();

		if(Storage::disk('s3')->exists($data['v_photo_name'])){
			Storage::disk('s3')->Delete($data['v_photo_name']);
		}

		if(count($jobdata->v_photos)){
			foreach ($jobdata->v_photos as $key => $value) {
				if($value!=$data['v_photo_name']){
					$update['v_photos'][]=$value;					
				}
			}
		}

		$update['d_modified']=date("Y-m-d H:i:s");
		JobsModel::find($data['i_job_id'])->update($update);
		$response['status']=1;
		$response['msg']="succesfully remove image.";
		echo json_encode($response);
		exit;
	}

	public function removeVideo(){

		$data = Request::all();
		$response = array();

		$userid = auth()->guard('web')->user()->_id;
		$jobid = "";

		if(!isset($data['i_job_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['i_job_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['v_video_name'])){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if($data['v_video_name']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$jobdata = JobsModel::find($data['i_job_id']);

		if(!count($jobdata)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;	
		}

		$update['v_video']=array();

		if(Storage::disk('s3')->exists($data['v_video_name'])){
			Storage::disk('s3')->Delete($data['v_video_name']);
		}

		if(count($jobdata->v_video)){
			foreach ($jobdata->v_video as $key => $value) {
				if($value!=$data['v_video_name']){
					$update['v_video'][]=$value;					
				}
			}
		}

		$update['d_modified']=date("Y-m-d H:i:s");
		JobsModel::find($data['i_job_id'])->update($update);
		$response['status']=1;
		$response['msg']="succesfully remove image.";
		echo json_encode($response);
		exit;
	}

	public function editJobpost($id=""){
		
		$other_skill = $other_skill_data = array();
		$jobdata = JobsModel::find($id);

		if(isset($jobdata) && count($jobdata)){
			if(isset($jobdata->i_otherskill_id) && count($jobdata->i_otherskill_id)){
				foreach ($jobdata->i_otherskill_id as $key => $value) {
					$other_skill[] = $value;
				}	
			}
		}
		
		$skilldata = SkillsModel::whereIn("_id",$other_skill)->get();

		if(isset($skilldata) && count($skilldata)){
			foreach ($skilldata as $key => $value) {
				$other_skill_data[$value->_id] = $value->v_name;
			}	
		}

		if(!count($jobdata)){
			return redirect('job-postings')->with(['warning' => 'Something went wrong.please try again after sometime.']);
		}

		$userdata = auth()->guard('web')->user();
		$isPremiumPlan = GeneralHelper::checkUserjobpostplan($userdata);
		
		$userplan = array();
		if(isset($userdata->v_plan) && count($userdata->v_plan)){
			$userplan = $userdata->v_plan;
		}

		$userservice = auth()->guard('web')->user()->buyer['v_service'];
		$category = CategoriesModel::where("v_type",$userservice)->where("e_status","active")->orderBy('v_name')->get();

		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");
		$skilldata = SkillsModel::where("e_status","active")->get();
		$userid = auth()->guard('web')->user()->_id;
		$v_service = $jobdata->v_service;

		//dd($jobdata);
		$data=array(
			'category'=>$category,
			'v_service'=>$v_service,
			'plandata'=>$plandata,
			'discount'=>$discount,
			'skilldata'=>$skilldata,
			'jobdata'=>$jobdata,
			'userplan'=>$userplan,
			'isPremiumPlan'=>$isPremiumPlan,
			'other_skill_data'=>$other_skill_data
		);
		return view('frontend/jobpost/edit_jobpost',$data);
	}

	public function endJobPosting($id=""){
		
		$jobdata = JobsModel::find($id);
		if(count($jobdata)){
			$jobdata->e_status="inactive";
			$jobdata->save();
		}else{
			return redirect('job-postings')->with(['warning' => 'Something went wrong.Please try again after sometime.']);	
		}
		return redirect('job-postings')->with(['success' => 'job post successfully ended.']);
	}

	public function updateJobpost1(){

		$data = Request::all();

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(isset($data['_token'])){
			unset($data['_token']);	
		}

		$job_id="";
		if(isset($data['job_id'])){
			$job_id=$data['job_id'];
			unset($data['job_id']);	
		}

		$data['d_modified']=date("Y-m-d H:i:s");	
		JobsModel::find($job_id)->update($data);
		
		$response['status']=1;
		$response['jobid']=$job_id;
		$response['msg']="succesfully update information.";
		echo json_encode($response);
		exit;
	}

	public function updateJobpost2(){
		
		$data = Request::all();
		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$userid = auth()->guard('web')->user()->_id;
		$jobid = "";

		if(isset($data['v_job_id']) && $data['v_job_id']!=""){
			$jobid = $data['v_job_id'];
			unset($data['v_job_id']);
		}else{
			$response['status']=0;
			$response['msg']="Something went wrong1.";
			echo json_encode($response);
			exit;
		}

		$jobdata = JobsModel::find($jobid);

		if(!count($jobdata)){
			$response['status']=0;
			$response['msg']="Something went wrong1.";
			echo json_encode($response);
			exit;
		}

		if(!isset($jobdata->e_sponsor)){
			$data['e_sponsor']="no";		
		}
		if(isset($jobdata->e_status) && $jobdata->e_status=="inactive"){
			$data['e_status']="active";		
		}

		if(isset($data['_token'])){
			unset($data['_token']);	
		}

		if(isset($data['v_budget_amt'])){
			$data['v_budget_amt'] = (float)$data['v_budget_amt'];
		}

		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){
			
			$lat=(float)$data['v_latitude'];
			$lont=(float)$data['v_longitude'];
			$data['location']['type']="Point";
			$data['location']['coordinates']=array($lont,$lat);
		}

		if(isset($data['d_start_date']) && $data['d_start_date']!=""){
			$data['v_start_year']=date("Y",strtotime($data['d_start_date']));
			$data['v_start_month']=date("m",strtotime($data['d_start_date']));
			$data['v_start_date']=date("d",strtotime($data['d_start_date']));
		}

		// $startdate = $data['v_start_year']."-".$data['v_start_month']."-".$data['v_start_date'];
		// $data['d_start_date'] = date("Y-m-d",strtotime($startdate));
		
		// if(isset($jobdata->v_video)){
		// 	$data['v_video'] = $jobdata->v_video;	
		// }	
			
		// if(Request::hasFile('work_video')) {
			
		// 	$destination = public_path('uploads/userprofiles');
		// 	$image = Request::file('work_video');

		// 	foreach($image as $file){
	            
		// 		$fileName = 'job-video-'.time().'.'.$file->getClientOriginalExtension();
		// 		$fileName = str_replace(' ', '_', $fileName);
		// 		$data['v_video'][] = 'jobpost/videos/' . $fileName;
		// 		$uploadS3 = Storage::disk('s3')->put('/jobpost/videos/'.$fileName, File::get((string)$file), 'public');

	 //        }
	 //    }		

	 	//    if(isset($jobdata->v_photos)){
		// 	$data['v_photos'] = $jobdata->v_photos;	
		// }

	  //   if(Request::hasFile('work_photo')) {

			// $destination = public_path('uploads/userprofiles');
			// $image = Request::file('work_photo');

			// foreach($image as $file){

			// 	$fileName = 'job-photo-'.time().'.'.$file->getClientOriginalExtension();
			// 	$fileName = str_replace(' ', '_', $fileName);
			// 	$data['v_photos'][] = 'jobpost/photos/' . $fileName;
			// 	$uploadS3 = Storage::disk('s3')->put('/jobpost/photos/'.$fileName, File::get((string)$file), 'public');

	  //       }
	  // 	}

	  	$data['d_modified']=date("Y-m-d H;i:s");
		JobsModel::find($jobid)->update($data);
		$response['status']=1;
		$response['msg']="succesfully update information.";
		echo json_encode($response);
		exit;
	}

	public function updatePlan(){
		
		$data = Request::all();

		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}

		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'job-postings',
			'v_action'=>'upgrade user plan',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],			
		);

		if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869"){
			
			$userid = auth()->guard('web')->user()->id;
			$userdata = auth()->guard('web')->user();

			if(!isset($userdata->e_basic_mail_send)){
				GeneralHelper::SendSkillboxSubscriptionEmail($userdata);
			}
			$updateUser['v_plan']['id']=$data['v_plan']['id'];
			$updateUser['v_plan']['duration']=$data['v_plan']['duration'];
			$updateUser['d_modified']=date("Y-m-d H:i:s");
			$update['e_basic_mail_send']="yes";
			UsersModel::find($userid)->update($updateUser);
			// $userid = auth()->guard('web')->user()->id;
			// $data['d_start_date']=date("Y-m-d");
			// $data['d_end_date']=date('Y-m-d',strtotime('+4000 days'));
			// $planupdate = GeneralHelper::upgradeUserPlan($userid,$data);
			return redirect('job-postings')->with(['success' => 'Successfully posted your job.']);
		}

		$userdata = auth()->guard('web')->user();
		$upgrade = GeneralHelper::upgradeUserPlanDetail($userdata,$data['v_plan']['id']);
		
		if($upgrade){
			Session::put('ordersummary',$orderdata);
			return redirect('payment/card');
		}else{
			return redirect('job-postings')->with(['success' => 'Successfully posted your job.']);
		}

		// $totalamt=0;
		// $addon=array();
		// if(isset($data['v_plan']) && count($data['v_plan'])){
		// 	foreach($data['v_plan'] as $key => $value) {
		// 		if($key!="id" && $key!="duration"){
		// 			$totalamt = $totalamt+$value;	
		// 			$addon[$key]=$value;
		// 			unset($data['v_plan'][$key]);		
		// 		}		
		// 	}
		// }
		// $orderdata['v_type'] = "posting jobpost";
		// //$orderdata['i_job_id'] = $data['i_job_id'];
		// $orderdata['redirect']="job-postings";
		// if(count($addon)){
		// 	$orderdata['v_addon']=$addon;
		// 	$orderdata['v_addon_amt']=$totalamt;
		// }
		// if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869" && $totalamt==0){
		// 	return redirect('job-postings');
		// }
		// if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869" && $totalamt!=0){
		// 	$orderdata['v_plan_charge']="no";
		// 	Session::put('ordersummary',$orderdata);
		// 	return redirect('job-post/payment');
		// }
		// $userdata = auth()->guard('web')->user();
		// $userplan = array();
		// if(isset($userdata->v_plan) && count($userdata->v_plan)){
		// 	$userplan = $userdata->v_plan;
		// }
		// $currentdate = date("Y-m-d");
		// if($userplan['id'] == $data['v_plan']['id'] && strtotime($currentdate)<strtotime($userplan['d_end_date']) && $totalamt == 0){
		// 	return redirect('job-postings');
		// }
		// else if($userplan['id']==$data['v_plan']['id'] && strtotime($currentdate)<strtotime($userplan['d_end_date']) && $totalamt != 0){
		// 	$orderdata['v_plan_charge']="no";
		// 	Session::put('ordersummary',$orderdata);
		// 	return redirect('job-post/payment');
		// }
		// else{
		// 	$orderdata['v_plan_charge']="yes";
		// 	$orderdata['v_plan_id']=$data['v_plan']['id'];
		// 	$orderdata['v_plan_duration']=$data['v_plan']['duration'];
			
		// 	Session::put('ordersummary',$orderdata);
		// 	return redirect('job-post/payment');
		// }
		// return redirect('job-postings');	
	}

	public function updatePlanUrgentNotify(){
		
		$data = Request::all();
	
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}

		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'job-postings',
			'v_action'=>'upgrade user plan',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],			
		);

		if(isset($data['i_job_type']) && $data['i_job_type']!=""){
			$orderdata['i_job_id']=$data['i_job_id'];
			$orderdata['i_job_type']=$data['i_job_type'];
		}

		//dd($orderdata);
		if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869"){
			// $userid = auth()->guard('web')->user()->id;
			// $data['d_start_date']=date("Y-m-d");
			// $data['d_end_date']=date('Y-m-d',strtotime('+4000 days'));
			// $planupdate = GeneralHelper::upgradeUserPlan($userid,$data);
			return redirect('job-postings')->with(['success' => 'Successfully posted your job.']);
		}

		$userdata = auth()->guard('web')->user();
		$upgrade = GeneralHelper::upgradeUserPlanDetail($userdata,$data['v_plan']['id']);
		
		if($upgrade){
			Session::put('ordersummary',$orderdata);
			return redirect('payment/card');
		}else{
			return redirect('job-postings')->with(['success' => 'Successfully posted your job.']);
		}

		
	}

	public function checkSponsorNow(){
			
		$data = Request::all();
		$response = array();

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$userdata = auth()->guard('web')->user();

		$userplan = array();
		if(isset($userdata->v_plan) && count($userdata->v_plan)){
			$userplan = $userdata->v_plan;
		}
	

		if($userplan['id']=="5a65b48cd3e812a4253c9869"){
			$response['status']=2;
			$response['msg'] = "You need to upgrade your plan.";
			echo json_encode($response);
			exit;
		}
		$response['status']=1;
		$response['msg'] = "Thank you, your message is now sponsored.";
		echo json_encode($response);
		exit;
	}


	public function sponsorNow(){
		
		$data = Request::all();
		$response = array();

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['job_id'])) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$update=array(
			'e_sponsor'=>"yes",
			'e_sponsor_status'=>"start",
			'd_modified'=>date("Y-m-d H:i:s"),
		);
		JobsModel::find($data['job_id'])->update($update);

		$response['status']=1;
		$response['msg'] = "Your job posting status is now changed to sponsored.";
		echo json_encode($response);
		exit;
	}

	public function SponsorNowUpgradePlan(){
		
		$data = Request::all();
		
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}

		$totalamt=0;
		$addon=array();

		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'sponsor-ads',
			'v_action'=>'upgrade user plan',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],			
			'i_job_id'=>$data['i_job_id'],			
		);

		Session::put('ordersummary',$orderdata);
		return redirect('payment/card');

		// $orderdata['v_type'] = "posting jobpost upgrade user plan";
		// $orderdata['i_job_id'] = $data['i_job_id'];
		// $orderdata['redirect']="sponsor-ads";
		// $orderdata['v_plan_charge']="yes";
		// $orderdata['v_plan_id']=$data['v_plan']['id'];
		// $orderdata['v_plan_duration']=$data['v_plan']['duration'];
		// $orderdata['v_addon']=array();	

		// Session::put('ordersummary',$orderdata);
	 	// return redirect('job-post/payment');
		
	}

	public function markUrgent(){
		
		$data = Request::all();

		$response = array();

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['job_id'])) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$userdata = auth()->guard('web')->user();

		if(isset($userdata->v_plan) && $userdata->v_plan['id'] == "5a65b9f4d3e8124f123c986c"){

			$update=array(
				'e_urgent'=>"yes",
				'd_modified'=>date("Y-m-d H:i:s"),
			);
			JobsModel::find($data['job_id'])->update($update);
			
			$response['status']=1;
			$response['msg'] = GeneralHelper::successMessage("Your job posting is now set as urgent.");
			echo json_encode($response);
			exit;

		}else{

			$response['status']=2;
			$response['msg'] = GeneralHelper::successMessage("You need to pay for it.");
			echo json_encode($response);
			exit;

		}
	}

	public function markUrgentPayment(){
		
		$data = Request::all();
		
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}

		$orderdata['v_type'] = "urgent";
		$orderdata['i_job_id'] = $data['i_job_id'];
		$orderdata['redirect'] = "job-postings";
		$addon['jobposturgent']=5;
		$totalamt=5;
		
		if(count($addon)){
			$orderdata['v_addon']=$addon;
			$orderdata['v_addon_amt']=$totalamt;
		}

		$orderdata['v_plan_charge']="no";
		Session::put('ordersummary',$orderdata);
		return redirect('job-post/payment');
	}

	public function notify(){
		
		$data = Request::all();

		$response = array();

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['job_id'])) {
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}

		$userdata = auth()->guard('web')->user();

		if(isset($userdata->v_plan) && $userdata->v_plan['id'] == "5a65b9f4d3e8124f123c986c"){

			$update=array(
				'e_notify'=>"yes",
				'd_modified'=>date("Y-m-d H:i:s"),
			);
			JobsModel::find($data['job_id'])->update($update);
			GeneralHelper::JobNotifyEmailNotificationSingle($data['job_id']);

			$response['status']=1;
			$response['msg'] = GeneralHelper::successMessage("Your notification sent successfully.");
			echo json_encode($response);
			exit;

		}else{

			$response['status']=2;
			$response['msg'] = GeneralHelper::successMessage("You need to pay for it.");
			echo json_encode($response);
			exit;

		}
	}

	// public function notifyEmailNotification($jid=""){

	// 	$jobdata = JobsModel::find($jid);
	// 	if(!count($jobdata)){
	// 		return 0;
	// 	}
	// 	$sellerdata = SellerprofileModel::where("e_status","active")->where('i_category_id',$jobdata->i_category_id)->get();
	// 	if(!count($sellerdata)){
	// 		return 0;
	// 	}

	// 	$imgdata="";
	// 	if(isset($jobdata->v_photos) && count($jobdata->v_photos)){
	//             if(Storage::disk('s3')->exists($jobdata->v_photos[0])){
	//                 $imgdata = \Storage::cloud()->url($jobdata->v_photos[0]);
	//             }else{
	//                 $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
	//             }
 //        }else{
 //            $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
 //        }

 //        $urldata = url('online-job').'/'.$jobdata->id;
 //       	$orderstr="";
 //        $orderstr.="<table style='width:100%' border=2>";
 //        $orderstr.="<tbody><tr>
 //                    <td width='10%'><img src='".$imgdata."' width='150px' height='150px'></td>
 //                    <td width='80%'>
 //                    	<p>&nbsp;<b>Title</b>  ".$jobdata->v_job_title."</p>
 //                    	<p>&nbsp;<b>Description</b> ".$jobdata->l_job_description."</p>
 //                    </td>
 //                    <td width='10%'><a href='".$urldata."'>Click here</a> to view</td></tr></tbody>";
 //        $orderstr.="</table>";
        
 //        $data['BUYER_NAME'] = auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname;
 //        $data['JOB_DETAIL']=$orderstr;

 //        if(count($sellerdata)){
	// 		foreach ($sellerdata as $key => $value) {
	// 			$data['USER_FULL_NAME'] = "";
	// 			$selleremail="";
	// 			if(count($value->hasUser()) && isset($value->hasUser()->v_fname)){
	// 	            $data['USER_FULL_NAME'] = $value->hasUser()->v_fname;
	// 	        }
	// 	        if(count($value->hasUser()) && isset($value->hasUser()->v_lname)){
	// 	            $data['USER_FULL_NAME'] = $data['USER_FULL_NAME'].' '.$value->hasUser()->v_lname;
	// 	        }
	// 	        if(count($value->hasUser()) && isset($value->hasUser()->v_email)){
	// 	            $selleremail = $value->hasUser()->v_email;
	// 	        }

	// 	        $mailcontent = EmailtemplateHelper::NewJobNotificationToSeller($data);
	// 	        $subject = "New job";
	// 	        $mailids=array(
	// 	            $data['USER_FULL_NAME']=>$selleremail,
	// 	        );
	// 	        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
	// 	    }
	// 	}
	// }

	public function sellerNotifyPayment(){
		
		$data = Request::all();
		
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}

		$orderdata['v_type'] = "posting jobpost mark urgent";
		$orderdata['i_job_id'] = $data['i_job_id'];
		$orderdata['redirect']="job-postings";
		$addon['jobpostnotify']=3;
		$totalamt=3;
		if(count($addon)){
			$orderdata['v_addon']=$addon;
			$orderdata['v_addon_amt']=$totalamt;
		}
		$orderdata['v_plan_charge']="no";
		Session::put('ordersummary',$orderdata);
		return redirect('job-post/payment');

	}

	public function online() {
			
		if(!auth()->guard('web')->check()) {
			return redirect('account/job-post')->with(['warning' => 'Something went wrong']);
		}
		
		$v_service = auth()->guard('web')->user()->v_service;
		$category = CategoriesModel::where("v_type","online")->where("e_status","active")->get();
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

		$data=array(
			'category'=>$category,
			'v_service'=>$v_service,
			'plandata'=>$plandata,
			'discount'=>$discount,
		);

		return view('frontend/jobpost/online_signup',$data);
	}

	public function inperson() {
		
		if(!auth()->guard('web')->check()) {
			return redirect('account/job-post')->with(['warning' => 'Something went wrong']);
		}
		$v_service = auth()->guard('web')->user()->v_service;

		$category = CategoriesModel::where("v_type","inperson")->where("e_status","active")->get();
		$data=array(
			'category'=>$category,
			'v_service'=>$v_service,
		);
		return view('frontend/jobpost/online_signup',$data);
	}

	public function appliedJobs($id="") {

		if(!auth()->guard('web')->check()) {
			return redirect('login')->with(['warning' => 'Please login and click after that link']);
		}
		
		$userid = auth()->guard('web')->user()->_id;
		$userservice = auth()->guard('web')->user()->buyer['v_service'];
	  	$appliedJobs = AppliedJobModel::where('i_applied_id',$id)->get();

	  	$data=array(
			'appliedJobs'=>$appliedJobs,
		);

		return view('frontend/buyer/applied_for_jobs',$data);
	
	}

	public function appliedJobsDashboardList() {

		

		if(!auth()->guard('web')->check()) {
			return redirect('login')->with(['warning' => 'Please login and click after that link']);
		}

		$userid = auth()->guard('web')->user()->_id;
		$userservice = auth()->guard('web')->user()->buyer['v_service'];
		$appliedJobs = AppliedJobModel::where('i_buyer_id',$userid)->get();

	
	 
	  	$data=array(
			'appliedJobs'=>$appliedJobs,
		);
		return view('frontend/buyer/applied_for_jobs',$data);
		
	}
	
	public function deleteAppliedJobs(){
		
		$data  = Request::all();

		if(isset($data['Ids']) && $data['Ids'] == ''){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Please select Job.");
			echo json_encode($response);
			exit;
		}
		
		$Ids   = explode(',', $data['Ids']);

		$userid = auth()->guard('web')->user()->id;
	
		AppliedJobModel::whereIn('i_applied_id',$Ids)->where('i_user_id',$userid)->delete();
	
		$response['status']=1;
		$response['msg']=GeneralHelper::successMessage("Successfully removed applied jobs !");
		echo json_encode($response);
		exit;
	}

	public function upgradeUserPlan(){
	
		$data = Request::all();
	
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$userid = auth()->guard('web')->user()->id;
		$planupdate = GeneralHelper::upgradeUserPlan($userid,$data);

		if($planupdate){
			return redirect('job-postings')->with(['success' => 'Successfully upgrade your plan.']);
		}
		return redirect('job-postings');
	}


	public function updateUserPlan(){
		
		$data = Request::all();
		
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}

		$totalamt=0;
		$orderdata['v_type'] = "posting jobpost upgrade user plan";
		$orderdata['i_job_id'] = "";
		$orderdata['redirect']="buyer/post-job";

		if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869" && $totalamt==0){
			return redirect('buyer/post-job');
		}

		if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869" && $totalamt!=0){
			$orderdata['v_plan_charge']="no";
			Session::put('ordersummary',$orderdata);
			return redirect('job-post/payment');
		}

		$userdata = auth()->guard('web')->user();
		$userplan = array();
		if(isset($userdata->v_plan) && count($userdata->v_plan)){
			$userplan = $userdata->v_plan;
		}
		$currentdate = date("Y-m-d");
		
		if($userplan['id'] == $data['v_plan']['id'] && strtotime($currentdate)<strtotime($userplan['d_end_date']) && $totalamt == 0){
			return redirect('job-postings');
		}
		else if($userplan['id']==$data['v_plan']['id'] && strtotime($currentdate)<strtotime($userplan['d_end_date']) && $totalamt != 0){
			$orderdata['v_plan_charge']="no";
			Session::put('ordersummary',$orderdata);
			return redirect('job-post/payment');
		}
		else{
			$orderdata['v_plan_charge']="yes";
			$orderdata['v_plan_id']=$data['v_plan']['id'];
			$orderdata['v_plan_duration']=$data['v_plan']['duration'];
			Session::put('ordersummary',$orderdata);
			return redirect('job-post/payment');

		}
		return redirect('buyer/post-job');	
	}
	/*
	public function updateUserPlanAddnewJoppost(){
		
		$data = Request::all();
	
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}

		if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869"){
			return redirect('buyer/post-job');
		}

		$totalamt=0;

		$orderdata['v_type'] = "upgrade_plan";
		$orderdata['redirect']="buyer/post-job";
		$orderdata['i_job_id'] = "";
		$orderdata['v_plan_id'] = $data['v_plan']['id'];
		$orderdata['v_plan_duration'] = $data['v_plan']['duration'];
		
		Session::put('ordersummary',$orderdata);
		return redirect('job-post/payment');
	
	}
	*/

}