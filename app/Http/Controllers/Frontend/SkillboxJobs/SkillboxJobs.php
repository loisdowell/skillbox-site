<?php
namespace App\Http\Controllers\Frontend\SkillboxJobs;

use Request, Lang, File,Storage;
use App\Http\Controllers\Controller;
use App\Models\SkillboxJob\SkillboxJobCategory as SkillboxJobCategoryModel;
use App\Models\SkillboxJob\SkillboxJob as SkillboxJobModel;
use App\Models\SkillboxJob\SkillboxJobApply as SkillboxJobApplyModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\General as GeneralHelper;



class SkillboxJobs extends Controller {

	public function index() {
		
		$query = SkillboxJobCategoryModel::query();
		$data = $query->orderBy('i_order')->get();

		$categorylist=array();
		foreach ($data as $key => $value) {
			$categorylist[$value->id] = $value->v_title;
		}
		$skillboxJob = SkillboxJobModel::where('e_status', 'active')->get();
		$finalresult=array();
		if(count($skillboxJob)){
			foreach ($skillboxJob as $key => $value) {
				$finalresult[$value->i_category_id][]=$value;
			}
		}
		$metaDetails = GeneralHelper::MetaFiledData('skillbox-careers','Skillbox careers');	

		$_data=array(
			'view'=>"list",
			'categorylist'=>$categorylist,
			'finalresult'=>$finalresult,
			'metaDetails' =>$metaDetails,
		);
		return view('frontend/skillbox-jobs/skillbox-jobs', $_data);
	}

	public function detailsJob($id){

		$data = SkillboxJobModel::find($id);
		$metaDetails = GeneralHelper::MetaFiledData('skillbox_job_detail','Skillbox careers detail');	
		
		$_data=array(
			'data'=>$data,
			'metaDetails'=>$metaDetails,
		);
		return view('frontend/skillbox-jobs/skillbox-jobs-details', $_data);

	}

	public function applyJob($id){
		
		$data = SkillboxJobModel::find($id);
		$metaDetails = GeneralHelper::MetaFiledData('skillbox_job_apply','Skillbox careers detail');	
	
		
		$_data=array(
			'data'=>$data,
			'metaDetails'=>$metaDetails,
		);
		return view('frontend/skillbox-jobs/apply-job', $_data);
	}

	public function postApplyJob($id){
		
		$post_data = Request::all();
		
		if(Request::file()){
			$filePath="";  
			$image = Request::file('v_attach_cv');
			$ext   = $image->getClientOriginalExtension();
			if(count($image) ){
				
				if($ext == 'doc' || $ext == 'docx' || $ext == 'pdf'){
					
					$fileName = 'attach-cv-'.time().'.'.$image->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
			        $path = public_path('uploads/cv');
	                $image->move($path, $fileName);
					$post_data['v_attach_cv'] = 'common/' . $fileName;
					/*$uploadS3 = Storage::disk('s3')->put('/common/'.$fileName, File::get((string)$image), 'public');*/

	                $filePath = $path.'/'.$fileName;

				}else{
					return redirect( '/join-our-team/apply-job/'.$id)->with(['warning' => 'Please attach only PDF OR DOC']);	
				
				}
			}
		}

		SkillboxJobApplyModel::create($post_data);

		$jobData = SkillboxJobModel::find($id);
		if(isset($jobData) && count($jobData)){
			$jobTitle = $jobData->v_title;

		}else{
			$jobTitle = '';
		}
		$username = $post_data['v_fname'].' '.$post_data['v_lname'];
		$useremail = $post_data['v_email'];
		$usercontact = $post_data['v_phone_number'];
		$usermessage = $post_data['v_cover_letter'];
		$data=array(
			'jobTitle'=>$jobTitle,
			'name'=>$username,
			'email'=>$useremail,
			'contact'=>$usercontact,
			'message'=>$usermessage,
		);
		$mailcontent = EmailtemplateHelper::ApplyJob($data);
		$subject = "Apply Job";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b093cfb76fbae5f4402b223");
		
		$mailids=array(
			$username=>EmailtemplateHelper::getSiteSetting( 'SITE_EMAIL' )
		);

		$attachments = [];
		$attachments[] = $filePath;
		
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids,$attachments);

		return redirect( 'jobs-apply-form-notification'); 
	}

	public function applyJobNotification(){
		
		return view('frontend/skillbox-jobs/skillbox-jobs-notification');

	}
}
?>