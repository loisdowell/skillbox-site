<?php
namespace App\Http\Controllers\Frontend\Support;

use Request, Hash, Lang,Storage,File;
use App\Http\Controllers\Controller;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Blogs\Blogs as BlogsModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Support\SupportQuery as SupportQueryModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;




//use App\Models\Categories as CategoriesModel;


class Support extends Controller {

	protected $section;
	public function __construct(){
		$this->section = Lang::get('section.support');
	}
	
	public function index(){
		
		if(!auth()->guard('web')->check()) {
			return redirect('account/profile')->with(['warning' => 'Something went wrong.']);
		}
		$userid = auth()->guard('web')->user()->id;

		$supportdata = SupportModel::where('i_user_id',$userid)->where('i_parent_id',0)->orderBy('d_added',"DESC")->get();
		//$supportquery = GeneralHelper::supportQueryData();
		
		$query = SupportQueryModel::query();
		$supportquery = $query->where('e_status','active')->orderBy('i_order')->get();

		$supportqueryList=array();
		if(count($supportquery)){
			foreach ($supportquery as $key => $value) {
				$supportqueryList[$value->id]=$value->v_name;		
			}
		}

		$updatedata['e_view']="read";
		SupportModel::where('i_touser_id',$userid)->update($updatedata);
		$data=array(
			'supportdata'=>$supportdata,
			'supportquery'=>$supportqueryList,
		);	
		return view('frontend/Support/support',$data);

	}

	public function add(){

		//$supportquery = GeneralHelper::supportQueryData();
		
		$query = SupportQueryModel::query();
		$supportquery = $query->where('e_status','active')->orderBy('i_order')->get();

		$supportqueryList=array();
		if(count($supportquery)){
			foreach ($supportquery as $key => $value) {
				$supportqueryList[$value->id]=$value->v_name;		
			}
		}

		$data=array(
			'supportquery'=>$supportqueryList,
		);	

		return view('frontend/Support/support_add',$data);

	}

	public function addData(){
		
		$data = Request::all();

		$userid = auth()->guard('web')->user()->id;

		if(isset($data['_token'])){
			unset($data['_token']);
		}
		$data['i_parent_id']=0;
		$data['e_user_type']="user";
		$data['i_user_id']=$userid;
		$data['e_status']="open";
		$data['e_view_status']="unread";
		$data['i_ticket_num']=GeneralHelper::supportNumber();
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		$data['v_document']=array();
		if(Request::hasFile('v_document')) {
			$image = Request::file('v_document');
			foreach($image as $file){
	        	$fileName = 'doc-'.time().'.'.$file->getClientOriginalExtension();
				$fileName = str_replace(' ', '_', $fileName);
				$data['v_document'][] = 'support/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/support/'.$fileName, File::get((string)$file), 'public');
		    }
	    }
	   
	   	SupportModel::create($data);
	   	self::SendMailSupportTicket($data);
		return redirect('support')->with(['success' => 'Successfully generated support ticket.']);
		
	}

	public function view($id=""){
		
		$supportdata = SupportModel::find($id);
		//$supportquery = GeneralHelper::supportQueryData();

		$query = SupportQueryModel::query();
		$supportquery = $query->where('e_status','active')->orderBy('i_order')->get();

		$supportqueryList=array();
		if(count($supportquery)){
			foreach ($supportquery as $key => $value) {
				$supportqueryList[$value->id]=$value->v_name;		
			}
		}
		$commentdata = SupportModel::where('i_parent_id',$id)->orderBy('d_added',"ASC")->get();
		

		$data=array(
			'supportdata'=>$supportdata,
			'supportquery'=>$supportqueryList,
			'commentdata'=>$commentdata,
		);
		return view('frontend/Support/support_view',$data);	

	}

	public function postComment(){

		$data = Request::all();
	
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after some time.";
			echo json_encode($response);
			exit;
		}

		if(!isset($data['i_support_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after some time.";
			echo json_encode($response);
			exit;	
		}
		$userid = auth()->guard('web')->user()->id;
		$i_support_id =$data['i_support_id'];

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$data['i_user_id']=$userid;
		$data['i_parent_id']=$i_support_id;
		$data['e_view_status']="unread";
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		SupportModel::create($data);

		if(isset($data['e_status']) && $data['e_status']=="on"){
			$update['e_status']="close";
			SupportModel::find($i_support_id)->update($update);
		}

		$userdata=array();
		$userdata['v_image']="";
		if(isset(auth()->guard('web')->user()->v_image)){
			$userdata['v_image']=auth()->guard('web')->user()->v_image;
		}

		$userdata['v_fname']="";
		if(isset(auth()->guard('web')->user()->v_fname)){
			$userdata['v_fname']=auth()->guard('web')->user()->v_fname;
		}
		$userdata['v_lname']="";
		if(isset(auth()->guard('web')->user()->v_lname)){
			$userdata['v_lname']=auth()->guard('web')->user()->v_lname;
		}
		$imgdata = \App\Helpers\General::userroundimage($userdata);
    
        $commentstr ='
        		<div class="Announcements-second-man">
        		<div class="Announcements-find">
                	<div class="Announcements-man">
                    	'.$imgdata.'
                	</div>
                	<div class="conversation-name">
                    	<p>'.$userdata['v_fname'].' '.$userdata['v_lname'].'</p>
                    	<p><span>'.\Carbon\Carbon::createFromTimeStamp(strtotime($data['d_added']))->diffForHumans().'</span></p>
                	</div>
            	</div>
                <div class="conversation-text">
	                <div class="ticket-conversation-msg">
	                    <div class="row">
	                        <div class="col-sm-11">
	                            <p>
	                            	'.$data['l_question'].'
	                            </p>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            </div>';



		$response['status']=1;
		$response['commentstr']=$commentstr;
		$response['msg']="Your comment posted successfully.";
		echo json_encode($response);
		exit;	

	}

	public function SendMailSupportTicket($post){
		
		$query = SupportQueryModel::query();
		$supportquery = $query->where('e_status','active')->orderBy('i_order')->get();
		
		$query="";
		if(count($supportquery)){
			foreach ($supportquery as $key => $value) {
				if($value->id==$post['e_query']){
					$query=$value->id;
				}
			}
		}
		$data=array(
			'SUPPORT_QUERY'=>$query,
			'SUPPORT_CATEGORY'=>$post['e_type'],
			'SUPPORT_QUESTIONS'=>$post['l_question'],
		);

		$mailcontent = EmailtemplateHelper::SupportTicketAdmin($data);
		$subject = EmailtemplateHelper::EmailTemplateSubject("5bd80f8076fbae499a6a4f12");
		$mailids=array(
			"skillbox"=>EmailtemplateHelper::getSiteSetting( 'SITE_EMAIL' )
		);
		
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);


	}

}