<?php
namespace App\Http\Controllers\Frontend\CMS;

use Request, Hash, Lang,Storage;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Blogs\Blogs as BlogsModel;
use App\Models\Pages\Pages as PagesModel;


class Cms extends Controller {

	protected $section;
	public function __construct(){
		$this->section = Lang::get('section.blog');
	}
	
	public function index($slug=""){
			
		if($slug==""){
			return redirect('/');
		}
		$cmsdata = PagesModel::where("v_key",$slug)->where("e_status","active")->first();
		if(!count($cmsdata)){
			return redirect('/');
		}

		$metaDetails['v_meta_title'] = $cmsdata->v_meta_title;
		$metaDetails['v_meta_keywords'] = $cmsdata->v_meta_keywords;
		$metaDetails['l_meta_description'] = $cmsdata->l_meta_description;
		$metaDetails['v_image'] = "";
		
		if(Storage::disk('s3')->exists($cmsdata->v_banner_img)){
            $metaDetails['v_image'] = Storage::cloud()->url($cmsdata->v_banner_img);      
        }                

       	$data=array(
			'cmsdata'=>$cmsdata,
			'metaDetails'=>$metaDetails,
		);
		return view('frontend/CMS/cms',$data);	

	}
	
	// public function support(){

	// 	$slug="help-support";
	// 	$cmsdata = PagesModel::where("v_key",$slug)->where("e_status","active")->first();
	// 	if(!count($cmsdata)){
	// 		return redirect('/');
	// 	}
		
	// 	$metaDetails['v_meta_title'] = $cmsdata->v_meta_title;
	// 	$metaDetails['v_meta_keywords'] = $cmsdata->v_meta_keywords;
	// 	$metaDetails['l_meta_description'] = $cmsdata->l_meta_description;
	// 	$metaDetails['v_image'] = "";
		
	// 	if(Storage::disk('s3')->exists($cmsdata->v_banner_img)){
    //         $metaDetails['v_image'] = Storage::cloud()->url($cmsdata->v_banner_img);      
    //     }                

    //    	$data=array(
	// 		'cmsdata'=>$cmsdata,
	// 		'metaDetails'=>$metaDetails,
	// 	);
	// 	return view('frontend/CMS/cms',$data);	
	
	// }

}