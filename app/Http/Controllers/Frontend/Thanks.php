<?php
namespace App\Http\Controllers\Frontend;

use Request, Hash, Lang,Crypt,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Order as OrderModal;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Courses\Courses as CoursesModel;



class Thanks extends Controller {

	protected $section;

	public function __construct(){
		$this->section = Lang::get('section.thanku');
	}

	public function index() {
	 

		$thanksdata = array();
	 	if(Session::has('thankspage')){
	 		$thanksdata = Session::get('thankspage');	
	 		Session::forget('thankspage');
	 	}

	 	$trans=array();
		$items=array();

	 	if(isset($thanksdata['page']) && $thanksdata['page']=="order"){
	 		$query = OrderModal::query();
			$query = $query->where(function($query ){
	        $query->where('e_transaction_type','skill')
	              ->orWhere('e_transaction_type', 'course');
	    	});	
	    	$orderdata = $query->orderBy("d_added","DESC")->first();

	    	if(count($orderdata)){
				
				$trans = array(
					'id'=>$orderdata->i_order_no, 
					'affiliation'=>'Skillbox',
	               	'revenue'=>$orderdata->v_amount,
	            );

				if(isset($orderdata->e_transaction_type) && $orderdata->e_transaction_type=="skill"){
					
					$sellerdata = SellerprofileModel::find($orderdata->i_seller_profile_id);
					if(count($sellerdata)){
						$items = array(
						  	array('id'=>$orderdata->i_order_no,'sku'=>$sellerdata->id, 'name'=>$sellerdata->v_profile_title, 'category'=>'Skillbox Profile', 'price'=>$orderdata->v_amount, 'quantity'=>'1'),
						);
					}
				}

				if(isset($orderdata->e_transaction_type) && $orderdata->e_transaction_type=="course"){
					$cdata = CoursesModel::find($orderdata->i_course_id);
					if(count($cdata)){
						$items = array(
						  	array('id'=>$orderdata->i_order_no,'sku'=>$cdata->id, 'name'=>$cdata->v_title, 'category'=>'Skillbox Course', 'price'=>$orderdata->v_amount, 'quantity'=>'1'),
						);
					}

				}
			}

	 	}

	 	$thanksdata['trans']=json_encode($trans);
	 	$thanksdata['items']=json_encode($items);	

	 	return view('frontend/thanks',$thanksdata);
	}


}