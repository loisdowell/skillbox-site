<?php
namespace App\Http\Controllers\Frontend\Buyer;

use Request, Hash, Lang,Validator,Auth,PDF;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\BuyerCourse as BuyerCourseModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesAnnouncements as CoursesAnnouncements;
use App\Models\Courses\CoursesQA as CoursesQA;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;
use App\Helpers\Emailtemplate as EmailtemplateHelper;



class Courses extends Controller {	
	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.Courses');
	}
	
	public function index() {
		
		$data = Request::all();
		$userid = auth()->guard('web')->user()->_id;
		$query = BuyerCourseModel::query();
		
		if(isset($data['e_status']) && $data['e_status']!=""){
			$query = $query->where('e_status',$data['e_status']);
		}
		//$query = $query->where('i_user_id',$userid);
		$query = $query->where('i_user_id',$userid)->where('e_payment_status','success');
		$cdata = $query->paginate(20);
		
		$_data=array(
			'data'=>$cdata,		
			'filter'=>$data,
		);
		return view('frontend/buyer/courses',$_data);
	}



	public function overView($id=""){
		
		$data = BuyerCourseModel::find($id);

		if(!count($data)){
			return redirect("buyer/courses");
		}

		$cdata	= CoursesModel::find($data->i_course_id);		

		$coursesQA	= CoursesQA::where("i_courses_id",$data->i_course_id)->orderBy("d_added","DESC")->limit(6)->get();
		$coursesAnnouncements	= CoursesAnnouncements::where("i_courses_id",$data->i_course_id)->orderBy("d_added","DESC")->limit(6)->get();
		$user = UsersModel::where("e_status","active")->get();

		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}

		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';

		//dd($id);

		$reviews = CoursesReviews::where("i_courses_id",$data->i_course_id)->get();
		$ratedata=0;

		if(count($reviews)){
			foreach($reviews as $key => $value) {
				$ratedata = $ratedata+($value->f_rate_instruction+$value->f_rate_navigate+$value->f_rate_reccommend)/3; 				
			}
		}
		$total['reviews'] = count($reviews);
		$total['ratedata'] = $ratedata;

		$_data=array(
			'data'=>$data,	
			'coursesQA'=>$coursesQA,
			'userList'=>$userList,
			'coursesAnnouncements'=>$coursesAnnouncements,
			'curentuser'=>$curentuser,		
			'total'=>$total,		
			'cdata'=>$cdata,		
		);
		return view('frontend/buyer/courses_overview',$_data);

	}

	public function downloadInvoice($id=""){

		$data = OrderModal::find($id);
		//return $data;
		$userdata = auth()->guard('web')->user();

		$_data=array(
			'data'=>$data,
			'userdata'=>$userdata
		);
		//return view('frontend/buyer/order_invoice',$_data);
		$pdf = PDF::loadView('frontend/buyer/courses_invoice',$_data);
        return $pdf->download('invoice.pdf');
    }	

	public function section($id=""){

		$buyercoursedata = BuyerCourseModel::find($id);

		if(!count($buyercoursedata)){
			return redirect("buyer/courses");
		}

		$data 	= CoursesModel::find($buyercoursedata->i_course_id);

		$coursesLevel = CoursesLevelModel::where("e_status","active")->get();
		$coursesLanguage = CoursesLanguageModel::where("e_status","active")->get();
		$coursesCategory = CoursesCategoryModel::where("e_status","active")->get();

		$_data	= array(
			'data'=>$data,
			'coursesLevel' 		=> $coursesLevel,
			'coursesLanguage'	=> $coursesLanguage,
			'coursesCategory'	=> $coursesCategory,
			'buyercoursedata' 	=> $buyercoursedata,
		);
		return view('frontend/buyer/courses_section',$_data);

	}
	
	public function qa($id=""){

		$rdata= Request::all();
	
		$buyercoursedata = BuyerCourseModel::find($id);

		if(!count($buyercoursedata)){
			return redirect("buyer/courses");
		}
		$data 	= CoursesModel::find($buyercoursedata->i_course_id);	
		$searchkeyword="";

		//$coursesQA	= CoursesQA::where("i_courses_id",$buyercoursedata->i_course_id)->orderBy("d_added","DESC")->get();

		$query = CoursesQA::query();
		if(isset($rdata['search']) && $rdata['search']!=""){
			
			$query = $query->where(function($query ) use($rdata){
		        $query->where('v_title','like','%'.$rdata['search'].'%')
		              ->orWhere('l_questions', 'like', '%' . $rdata['search'].'%');
		    });
		    $searchkeyword = $rdata['search'];
		}

		$query = $query->where('i_courses_id',$buyercoursedata->i_course_id);
		$query = $query->orderBy('d_added',"DESC");
		$coursesQA = $query->get();
	
		$user = UsersModel::where("e_status","active")->get();
		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}
		//dd($data['search']);

		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$_data	= array(
			'data'=>$data,
			'coursesQA'=>$coursesQA,
			'userList'=>$userList,
			'curentuser'=>$curentuser,
			'buyercoursedata' => $buyercoursedata,
			'searchkeyword' => $searchkeyword,

		);
		
		return view('frontend/buyer/courses_qa',$_data);
	
	}

	public function Qasearch(){

		$rdata= Request::all();
		$id = $rdata['icid'];

		$buyercoursedata = BuyerCourseModel::find($id);
		if(!count($buyercoursedata)){
			return redirect("buyer/courses");
		}
		$data 	= CoursesModel::find($buyercoursedata->i_course_id);	
		$searchkeyword="";

		$query = CoursesQA::query();
		
		if(isset($rdata['search']) && $rdata['search']!=""){
			$query = $query->where(function($query ) use($rdata){
		        $query->where('v_title','like','%'.$rdata['search'].'%')
		              ->orWhere('l_questions', 'like', '%' . $rdata['search'].'%');
		    });
		    $searchkeyword = $rdata['search'];
		}
		$query = $query->where('i_courses_id',$buyercoursedata->i_course_id);
		$query = $query->orderBy('d_added',"DESC");
		$coursesQA = $query->get();


		$user = UsersModel::where("e_status","active")->get();
		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}
		//dd($data['search']);

		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$_data	= array(
			'data'=>$data,
			'coursesQA'=>$coursesQA,
			'userList'=>$userList,
			'curentuser'=>$curentuser,
			'buyercoursedata' => $buyercoursedata,
			'searchkeyword' => $searchkeyword,

		);
		
		$faqstr = view('frontend/buyer/courses_qa_search',$_data);
		$responsestr = $faqstr->render();
		$response['status']=1;
		$response['responsestr']=$responsestr;
		echo json_encode($response);
		exit;

	}

	public function viewUpdateSection(){

	}

	public function viewSection(){

		$data = Request::all();
		
		$coursedata = BuyerCourseModel::find($data['bcid']);
		if(!count( $coursedata ) ){
			$response['msg'] = GeneralHelper::errorMessage("Something went wrong.");
		}
		$course = CoursesModel::find($coursedata->i_course_id);
		if(!count( $course ) ){
			$response['msg'] = GeneralHelper::errorMessage("Something went wrong.");
		}


		$totallacture=0;
        if(isset($course->section) && count($course->section)){
            foreach ($course->section as $key => $value) {
            	$totallacture= $totallacture+count($value['video']);   
            }
        }
        $coursedata->v_total_lectures = $totallacture;
       	if(!in_array( $data['sid'], $coursedata->v_complete_ids ) ){
			$coursedata->v_complete_ids = array_merge( $coursedata->v_complete_ids, [ $data['sid'] ] );
			$coursedata->v_complete_lectures = $coursedata->v_complete_lectures + 1;
		}

		$coursedata->e_status = "inprogress";
		if(count( $coursedata->v_complete_ids ) >= $coursedata->v_total_lectures){
			$coursedata->e_status = "completed";
			self::sendCompleteCourseNotification($course);	
		} 
			
		
		$coursedata->d_modified = date("Y-m-d H:i:s");
		$coursedata->save();
		$response['status'] = 1;
		$response['msg'] = GeneralHelper::errorMessage("Successfully view section.");		
		echo json_encode( $response ); exit;	

	}

	public function sendCompleteCourseNotification($coursedata=array()){
        
        if(!count($coursedata)){
            return 0;
        }
       
        $v_email = auth()->guard('web')->user()->v_email;
        $username="";
        if(isset(auth()->guard('web')->user()->v_fname) && auth()->guard('web')->user()->v_fname!=""){
            $username = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname) && auth()->guard('web')->user()->v_lname!=""){
            $username .= ' '.auth()->guard('web')->user()->v_lname;
        }

        $message = "";
        if(isset($coursedata->messages['welcome_congratulations']) && $coursedata->messages['welcome_congratulations']!=""){
            $message .= $coursedata->messages['welcome_congratulations'].'<br><br>';    
        }else {
        	return 1;
        }
        $data=array(
           'COURSE_MESSAGE'=>$message,
           'USER_FULL_NAME'=>$username,
        );
        
        $mailcontent = EmailtemplateHelper::CourseCongratulationsMessage($data);
        $subject = "Course completed";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bc8259076fbae2fb4210ab2");
        
        $mailids=array(
            $username=>$v_email,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

    }


	public function postQA(){

		$data= Request::all();
		$response=array();

		if(isset($data['_token'])){
			unset($data['_token']);
		}
		$buyer_courses_id="";

		if(isset($data['buyer_courses_id'])){
			$buyer_courses_id = $data['buyer_courses_id'];
			unset($data['buyer_courses_id']);
		}

		if(!isset($data['i_courses_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		$course = CoursesModel::find($data['i_courses_id']);

		if(!count($course)){
			
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$data['i_user_id'] = auth()->guard('web')->user()->id;
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		CoursesQA::insert($data);
		self::postQAEmailSend($course,$data);

		$response['status']=1;
		$response['buyer_courses_id']=$buyer_courses_id;
		$response['msg']="Successfully add your questions.";
		echo json_encode($response);
		exit;

	}

	public function postQAEmailSend($cdata,$qdata){

		if(!count($cdata)){
			return 0;
		}
		if(!count($qdata)){
			return 0;
		}

		$sellername="";
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_fname)){
			$sellername = $cdata->hasUser()->v_fname;
		}
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_lname)){
			$sellername .= ' '.$cdata->hasUser()->v_lname;
		}

		$emailadress="";
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_email)){
			$emailadress = ' '.$cdata->hasUser()->v_email;
		}

		$buyername="";
		if(isset(auth()->guard('web')->user()->v_fname)){
			$buyername = auth()->guard('web')->user()->v_fname;
		}
		if(isset(auth()->guard('web')->user()->v_lname)){
			$buyername .= ' '.auth()->guard('web')->user()->v_lname;
		}

		$coursename="";
		if(isset($cdata->v_title)){
			$coursename = $cdata->v_title;
		}

		$data['sellername']=$sellername;
		$data['buyername']=$buyername;
		$data['coursename']=$coursename;
		$data['v_title']="";
		$data['l_questions']="";

		if(isset($qdata['v_title'])){
			$data['v_title'] = $qdata['v_title'];
		}

		if(isset($qdata['l_questions'])){
			$data['l_questions'] = $qdata['l_questions'];
		}

		$mailcontent = EmailtemplateHelper::CourseQAtoSeller($data);
		$subject = "Course QA";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b1a1d7e76fbae10da3ec042");
		$mailids=array(
			$sellername=>$emailadress
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		return 1;		

	}

	public function postQAComment() {
		
		$data= Request::all();
		$response = array();

		if(!isset($data['i_course_qa_id']) && $data['i_course_qa_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}
		$coursesQA	= CoursesQA::find($data['i_course_qa_id']);
		if(!count($coursesQA)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}
	
		$update = array(
			'i_user_id'=>auth()->guard('web')->user()->id,
			'v_text'=>$data['v_text'],
			'd_added'=>date("Y-m-d H:i:s"),
		);

		$ansdata['l_answers'] = $coursesQA->l_answers;
		$ansdata['l_answers'][] = $update;
	
		CoursesQA::find($data['i_course_qa_id'])->update($ansdata);

		$course = CoursesModel::find($coursesQA->i_courses_id);

		self::postQAEmailSendComment($course,$data,$coursesQA);
		self::postQAEmailMainSendComment($course,$data,$coursesQA);

		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$commentstr="";
		$commentstr.='<div class="Announcements-second-man qaseller-second-man">';
		$commentstr.='<div class="qa-seller-find">';
		$commentstr.='<div class="Announcements-man">'.GeneralHelper::userroundimage($curentuser).'</div>';
		$commentstr.='<div class="Announcements-name">';
		$commentstr.='<p>'.$curentuser['v_name'].'</p>';
		$commentstr.='<p><span>'.\Carbon\Carbon::createFromTimeStamp(strtotime($update['d_added']))->diffForHumans().'</span></p>';
		$commentstr.='<div class="Announcements-message">'.$update['v_text'].'</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';

		$response['status']=1;
		$response['commentstr']=$commentstr;
		$response['msg']="Successfully add comment.";
		echo json_encode($response);
		exit;

	}


	public function postQAEmailSendComment($cdata,$postdata,$coursesQA){

		if(!count($cdata)){
			return 0;
		}
		if(!count($postdata)){
			return 0;
		}
		if(!count($coursesQA)){
			return 0;
		}

		$buyername="";
		if(count($coursesQA->hasUser()) && isset($coursesQA->hasUser()->v_fname)){
			$buyername = $coursesQA->hasUser()->v_fname;
		}
		if(count($coursesQA->hasUser()) && isset($coursesQA->hasUser()->v_lname)){
			$buyername .= ' '.$coursesQA->hasUser()->v_lname;
		}

		$emailadress="";
		if(count($coursesQA->hasUser()) && isset($coursesQA->hasUser()->v_email)){
			$emailadress = ' '.$coursesQA->hasUser()->v_email;
		}

		$sellername="";
		if(isset(auth()->guard('web')->user()->v_fname)){
			$sellername = auth()->guard('web')->user()->v_fname;
		}
		if(isset(auth()->guard('web')->user()->v_lname)){
			$sellername .= ' '.auth()->guard('web')->user()->v_lname;
		}

		$coursename="";
		if(isset($cdata->v_title)){
			$coursename = $cdata->v_title;
		}

		$data['sellername']=$sellername;
		$data['buyername']=$buyername;
		$data['coursename']=$coursename;
		$data['v_title']="";
		$data['l_questions']="";
		$data['l_answers']="";

		if(isset($postdata['v_text'])){
			$data['l_answers'] = $postdata['v_text'];
		}

		if(isset($coursesQA['v_title'])){
			$data['v_title'] = $coursesQA['v_title'];
		}
		if(isset($coursesQA['l_questions'])){
			$data['l_questions'] = $coursesQA['l_questions'];
		}

		$mailcontent = EmailtemplateHelper::CourseQAtoBuyer($data);
		$subject = "Course QA";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b1a20a876fbae0b95669873");
		
		$mailids=array(
			$buyername=>$emailadress
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		return 1;		
	}

	public function postQAEmailMainSendComment($cdata,$postdata,$coursesQA){

		if(!count($cdata)){
			return 0;
		}
		if(!count($postdata)){
			return 0;
		}
		if(!count($coursesQA)){
			return 0;
		}

		$buyername="";
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_fname)){
			$buyername = $cdata->hasUser()->v_fname;
		}
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_lname)){
			$buyername .= ' '.$cdata->hasUser()->v_lname;
		}

		$emailadress="";
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_email)){
			$emailadress = ' '.$cdata->hasUser()->v_email;
		}

		$sellername="";
		if(isset(auth()->guard('web')->user()->v_fname)){
			$sellername = auth()->guard('web')->user()->v_fname;
		}
		if(isset(auth()->guard('web')->user()->v_lname)){
			$sellername .= ' '.auth()->guard('web')->user()->v_lname;
		}

		$coursename="";
		if(isset($cdata->v_title)){
			$coursename = $cdata->v_title;
		}

		$data['sellername']=$sellername;
		$data['buyername']=$buyername;
		$data['coursename']=$coursename;
		$data['v_title']="";
		$data['l_questions']="";
		$data['l_answers']="";

		if(isset($postdata['v_text'])){
			$data['l_answers'] = $postdata['v_text'];
		}

		if(isset($coursesQA['v_title'])){
			$data['v_title'] = $coursesQA['v_title'];
		}
		if(isset($coursesQA['l_questions'])){
			$data['l_questions'] = $coursesQA['l_questions'];
		}

		$mailcontent = EmailtemplateHelper::CourseQAtoBuyer($data);
		$subject = "Course QA";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b1a20a876fbae0b95669873");
		$mailids=array(
			$buyername=>$emailadress
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		return 1;		
	}


	public function reviews($id=""){

		$buyercoursedata = BuyerCourseModel::find($id);
		if(!count($buyercoursedata)){
			return redirect("buyer/courses");
		}

		$data 	= CoursesModel::find($buyercoursedata->i_course_id);	
		$coursesReviews	= CoursesReviews::where("i_courses_id",$buyercoursedata->i_course_id)->orderBy("d_added","DESC")->get();
		$user = UsersModel::where("e_status","active")->get();


		$currentuserreview=array();

		if(count($coursesReviews)){
			foreach ($coursesReviews as $key => $value) {
				if(auth()->guard('web')->user()->id==$value->i_user_id){
					$currentuserreview=$value;
				}
			}
		}

		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}

		$totalstar = 0;
		$totalavg = 0;
		if(count($coursesReviews)){
			foreach ($coursesReviews as $key => $value) {
				$totalstar = $totalstar+$value->i_star;
			}
			$totalavg = $totalstar/count($coursesReviews);
		}

		$ratedata=0;
		if(count($coursesReviews)){
			foreach($coursesReviews as $key => $value) {
				$ratedata = $ratedata+($value->f_rate_instruction+$value->f_rate_navigate+$value->f_rate_reccommend)/3; 				
			}
			$ratedata = $ratedata/count($coursesReviews);
		}

		// dd($ratedata);
		// $total['reviews'] = count($reviews);
		// $total['ratedata'] = $ratedata;
	
		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
		
		$_data	= array(
			'data'=>$data,
			'coursesReviews'=>$coursesReviews,
			'userList'=>$userList,
			'curentuser'=>$curentuser,
			'totalavg'=>$ratedata,
			'buyercoursedata' => $buyercoursedata,
			'currentuserreview'=>$currentuserreview
		);
		return view('frontend/buyer/courses_reviews',$_data);
	}

	
	public function postReviews(){

		$data= Request::all();
		$response=array();

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$buyer_courses_id="";

		if(isset($data['buyer_courses_id'])){
			$buyer_courses_id = $data['buyer_courses_id'];
			unset($data['buyer_courses_id']);
		}

		if(!isset($data['i_courses_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		$course = CoursesModel::find($data['i_courses_id']);

		if(!count($course)){
			
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$data['i_star'] = ceil(($data['f_rate_instruction']+$data['f_rate_navigate']+$data['f_rate_reccommend'])/3);
		$data['i_user_id'] = auth()->guard('web')->user()->id;
		$data['i_seller_id'] = $course->i_user_id;
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		CoursesReviews::insert($data);
		self::postReviewEmailSend($course,$data);

		$response['status']=1;
		$response['buyer_courses_id']=$buyer_courses_id;
		$response['msg']="Successfully add your questions.";
		echo json_encode($response);
		exit;
	}	


	public function postReviewEmailSend($cdata,$rdata){

		if(!count($cdata)){
			return 0;
		}
		if(!count($rdata)){
			return 0;
		}

		$sellername="";
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_fname)){
			$sellername = $cdata->hasUser()->v_fname;
		}
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_lname)){
			$sellername .= ' '.$cdata->hasUser()->v_lname;
		}

		$emailadress="";
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_email)){
			$emailadress = ' '.$cdata->hasUser()->v_email;
		}

		$buyername="";
		if(isset(auth()->guard('web')->user()->v_fname)){
			$buyername = auth()->guard('web')->user()->v_fname;
		}
		if(isset(auth()->guard('web')->user()->v_lname)){
			$buyername .= ' '.auth()->guard('web')->user()->v_lname;
		}

		$coursename="";
		if(isset($cdata->v_title)){
			$coursename = $cdata->v_title;
		}

		$data['sellername']=$sellername;
		$data['buyername']=$buyername;
		$data['coursename']=$coursename;
		$data['review_string']=$rdata['l_comment'];
		
		$mailcontent = EmailtemplateHelper::CourseReviewSeller($data);
		$subject = "Course Review";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b1a211376fbae19586f56b2");
		$mailids=array(
			$sellername=>$emailadress
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		return 1;		

	}

	public function postReviewscomments(){

		$data= Request::all();
		$response = array();

		if(!isset($data['i_course_reviews_id']) && $data['i_course_reviews_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}
	
		$coursesReviews	= CoursesReviews::find($data['i_course_reviews_id']);
		
		if(!count($coursesReviews)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}
	
		$update['l_answers']['i_user_id'] = auth()->guard('web')->user()->id;
		$update['l_answers']['l_comment'] = $data['l_comment'];
		$update['l_answers']['d_added'] = date("Y-m-d H:i:s");

		CoursesReviews::find($data['i_course_reviews_id'])->update($update);
		
		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$commentstr="";
		$commentstr.='<div class="conversation-text extra-margin">';
		$commentstr.='<div class="conversation-find conversation-find-course">';
		$commentstr.='<div class="conversation-man">'.GeneralHelper::userroundimage($curentuser).'</div>';
		$commentstr.='<div class="conversation-name">';
		$commentstr.='<p>'.$curentuser['v_name'].'<span class="rating-buyer rating-Course-one">';
		$commentstr.='</span></p>';
		$commentstr.='<p><span>'.\Carbon\Carbon::createFromTimeStamp(strtotime($update['l_answers']['d_added']))->diffForHumans().'</span></p>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='<div class="conversation-text conversation-text-final">';
		$commentstr.='<div class="conversation-text-all conversation-text-course">';
		$commentstr.='<p class="response-seller">'.$update['l_answers']['l_comment'].'</p>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';

		$response['status']=1;
		$response['commentstr']=$commentstr;
		$response['msg']="Successfully add comment.";
		echo json_encode($response);
		exit;
	}


	public function postReviewsupdates(){

		$data= Request::all();
		$response = array();

		if(!isset($data['buyer_courses_id']) && $data['buyer_courses_id']==""){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!isset($data['i_courses_id']) && $data['i_courses_id']==""){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!isset($data['i_review_id']) && $data['i_review_id']=="0"){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$ReviewsData = CoursesReviews::find($data['i_review_id']);

		if(!count($ReviewsData)){

			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;	
		}

		$update=array(
			'f_rate_instruction'=>$data['f_rate_instruction'],
			'f_rate_navigate'=>$data['f_rate_navigate'],
			'f_rate_reccommend'=>$data['f_rate_reccommend'],
			'l_comment'=>$data['l_comment'],
			'd_modified'=>date("Y-m-d H:i:s"),
		);

		CoursesReviews::find($data['i_review_id'])->update($update);

		$response['status']=1;
		$response['buyer_courses_id']=$data['buyer_courses_id'];
		$response['msg']="Successfully add your questions.";
		echo json_encode($response);
		exit;

	}

	public function announcements($id=""){

		$buyercoursedata = BuyerCourseModel::find($id);

		if(!count($buyercoursedata)){
			return redirect("buyer/courses");
		}

		$data 	= CoursesModel::find($buyercoursedata->i_course_id);
		$coursesAnnouncements	= CoursesAnnouncements::where("i_courses_id",$buyercoursedata->i_course_id)->orderBy("d_added","DESC")->get();
		$user = UsersModel::where("e_status","active")->get();
		
		$userList=array();
		if(count($user)){
			foreach ($user as $key => $value) {
				$userList[$value->id]=array(
					'v_name'=>$value->v_fname.' '.$value->v_lname,
					'v_fname'=>$value->v_fname,
					'v_lname'=>$value->v_lname,
					'v_image'=>$value->v_image,
				);
			}
		}

		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$_data	= array(
			'data'=>$data,
			'coursesAnnouncements'=>$coursesAnnouncements,
			'userList'=>$userList,
			'curentuser'=>$curentuser,
			'buyercoursedata' => $buyercoursedata,
		);
		return view('frontend/buyer/courses_announcements',$_data);

	}



	public function postAnnouncementsComments() {
		
		$data= Request::all();
		$response = array();

		if(!isset($data['i_announcements_id']) && $data['i_announcements_id']==""){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}
	
		$coursesAnnouncements= CoursesAnnouncements::find($data['i_announcements_id']);
		
		if(!count($coursesAnnouncements)){
			$response['status']=0;
			$response['msg']="Something went wrong.";
			echo json_encode($response);
			exit;
		}
		$courseData = CoursesModel::find($coursesAnnouncements->i_courses_id);

		$update = array(
			'i_user_id'=>auth()->guard('web')->user()->id,
			'v_text'=>$data['v_text'],
			'd_added'=>date("Y-m-d H:i:s"),
		);

		$ansdata['l_comments'] = $coursesAnnouncements->l_comments;
		$ansdata['l_comments'][] = $update;
	
		CoursesAnnouncements::find($data['i_announcements_id'])->update($ansdata);

		self::CourseAnnouncementCommentNotifyToSeller($courseData,$data['v_text']);
		
		$curentuser['v_name']  = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_fname'] = isset(auth()->guard('web')->user()->v_fname) ? auth()->guard('web')->user()->v_fname : '';
		$curentuser['v_lname'] = isset(auth()->guard('web')->user()->v_lname) ? auth()->guard('web')->user()->v_lname : '';
		$curentuser['v_image'] = isset(auth()->guard('web')->user()->v_image) ? auth()->guard('web')->user()->v_image : '';
	
		$commentstr="";
		$commentstr.='<div class="Announcements-second-man">';
		$commentstr.='<div class="Announcements-find">';
		$commentstr.='<div class="Announcements-man">'.GeneralHelper::userroundimage($curentuser).'</div>';
		$commentstr.='<div class="Announcements-name">';
		$commentstr.='<p>'.$curentuser['v_name'].'</p>';
		$commentstr.='<p><span>'.\Carbon\Carbon::createFromTimeStamp(strtotime($update['d_added']))->diffForHumans().'</span></p>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='<div class="Announcements-text">';
		$commentstr.='<div class="row">';
		$commentstr.='<div class="col-sm-12">';
		$commentstr.='<div class="Announcements-text-seller">';
		$commentstr.='<p>'.$update['v_text'].'</p>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';
		$commentstr.='</div>';

		$response['status']=1;
		$response['commentstr']=$commentstr;
		$response['msg']="Successfully add comment.";
		echo json_encode($response);
		exit;

	}

	public function CourseAnnouncementCommentNotifyToSeller($cdata,$comment=""){
		

		if(!count($cdata)){
			return 0;
		}
		if($comment==""){
			return 0;
		}

		$sellername="";
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_fname)){
			$sellername = $cdata->hasUser()->v_fname;
		}
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_lname)){
			$sellername .= ' '.$cdata->hasUser()->v_lname;
		}

		$emailadress="";
		if(count($cdata->hasUser()) && isset($cdata->hasUser()->v_email)){
			$emailadress = ' '.$cdata->hasUser()->v_email;
		}

		$buyername="";
		if(isset(auth()->guard('web')->user()->v_fname)){
			$buyername = auth()->guard('web')->user()->v_fname;
		}
		if(isset(auth()->guard('web')->user()->v_lname)){
			$buyername .= ' '.auth()->guard('web')->user()->v_lname;
		}

		$coursename="";
		if(isset($cdata->v_title)){
			$coursename = $cdata->v_title;
		}

		$data['USER_FULL_NAME']=$sellername;
		$data['BUYER_NAME']=$buyername;
		$data['COURSE_TITLE']=$coursename;
		$data['COURSE_ANNOUNCEMENT_COMMENT']=$comment;
		$data['COURSE_SELLER_URL']=url('courses/announcements').'/'.$cdata->id;
		
		$mailcontent = EmailtemplateHelper::CourseAnnouncementCommentNotifyToSeller($data);
		$subject = "Course announcement comment";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5bbb38c476fbae654c11d283");
		$mailids=array(
			$sellername=>$emailadress
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		
		return 1;		

	}




}