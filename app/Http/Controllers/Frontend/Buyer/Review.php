<?php
namespace App\Http\Controllers\Frontend\Buyer;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\Buyerreview as BuyerreviewModel;
use App\Helpers\General as GeneralHelper;

class Review extends Controller {


	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.Review center');
	}

	public function index() {

		$userid = auth()->guard('web')->user()->_id;
		
		$buyerreview = BuyerreviewModel::where('i_buyer_id',$userid)->orderBy("d_added","DESC")->paginate(20);
		$data=array(
			'buyerreview'=>$buyerreview,
		);
		return view('frontend/buyer/review_center', $data);
	}

	public function userReview(){

		$userid = auth()->guard('web')->user()->_id;
		$response=array();
		
		$buyerreview = BuyerreviewModel::where('i_buyer_id',$userid)->orderBy("d_added","DESC")->paginate(20);
		
		$btnstatus=0;
		if($buyerreview->lastPage()>$buyerreview->currentPage()){
			$btnstatus=1;
		}

		$_data=array(
			'buyerreview'=>$buyerreview,
		);

		$reviewstr = view('frontend/buyer/userreview', $_data); 
		$responsestr = $reviewstr->render();

		$response['status']=1;
		$response['btnstatus']=$btnstatus;
		$response['responsestr']=$responsestr;
		$response['msg']="Get review succ.";
		echo json_encode($response);
		exit;
	}

	public function dashboardReview(){

		$userid = auth()->guard('web')->user()->_id;
		$response=array();
		
		$buyerreview = BuyerreviewModel::where('i_buyer_id',$userid)->orderBy("d_added","DESC")->paginate(6);
		
		$reviewcnt = BuyerreviewModel::where('i_buyer_id',$userid)->count();

		$total=6;
		if(isset($post_data['page']) && $post_data['page']!=""){
			$total = $total*$post_data['page'];
		}
		$reviewcnt = $reviewcnt-$total;

		if($reviewcnt>6){
			$reviewcnt=6;	
		}


		$btnstatus=0;
		if($buyerreview->lastPage()>$buyerreview->currentPage()){
			$btnstatus=1;
		}

		if($buyerreview->currentPage()>1){
			$btnstatus=0;
		}

		$_data=array(
			'buyerreview'=>$buyerreview,
		);

		$reviewstr = view('frontend/buyer/dashboard-buyer-review', $_data); 
		$responsestr = $reviewstr->render();

		$response['status']=1;
		$response['btnstatus']=$btnstatus;
		$response['responsestr']=$responsestr;
		$response['reviewcntdata']=$reviewcnt;
		$response['msg']="Get review succ.";
		echo json_encode($response);
		exit;
	}


}