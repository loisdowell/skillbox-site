<?php
namespace App\Http\Controllers\Frontend\Buyer;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Buyerreview as BuyerreviewModal;
use App\Models\Faqs as FaqsModel;
use App\Models\Users\Messages as MessagesModel;
use App\Models\Howitwork as HowitworkModel;




class Buyer extends Controller {

	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.dashboard');
	}

	public function index() {
		return view('frontend/buyer/buyer_signup');
	}

	public function dashboard() {

		$userid = auth()->guard('web')->user()->_id;
		
		$userdata = UsersModel::find($userid);
		$usercnt = UsersModel::where("e_status","active")->get();
		$countryname = count($userdata->hasCountry())  ? $userdata->hasCountry()->v_title : '';
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");
		$userplandata = GeneralHelper::userPlanDetail($userdata);
		$buyerreview = BuyerreviewModal::where('i_buyer_id',$userid)->get();
		$fAQs = FaqsModel::where('e_type', 'buyer')->orderBy('d_added', 'DESC')->limit(5)->get();
		$buyermessage = MessagesModel::where('i_from_id',$userid)->where('i_parent_id',0)->orderBy("d_modified","DESC")->count();
		$howitswork = HowitworkModel::where("e_type","buyer")->where('e_status',"active")->orderBy('i_order')->get();
		
		$data=array(
			'userdata'=>$userdata,
			'countryname'=>$countryname,
			'plandata'=>$plandata,
			'usercnt'=>count($usercnt),
			'discount'=>$discount,
			'userplandata'=>$userplandata,
			'buyerreview'=>$buyerreview,
			'fAQs'=>$fAQs,
			'buyermessage'=>$buyermessage,
			'howitswork'=>$howitswork,
		);
		return view('frontend/buyer/dashboard',$data);
	}

	public function shortlistedSkills() {

		$s_skill = array();
		
		$userid = auth()->guard('web')->user()->_id;
	  	$shortlistedSkill = ShortlistedSkillsModel::where('i_user_id',$userid)->get();
	  	foreach ($shortlistedSkill as $key => $value) {
			$s_skill[] = $value->i_shortlist_id;
		}

		$skills_data=array();
		if(count($s_skill)){
			$skills_data = SellerprofileModel::whereIn('_id', $s_skill)->get();	
		}
		
		$data=array(
			'skills_data'=>$skills_data,
		);
		return view('frontend/buyer/shortlisted_skills',$data);
	}

	public function deleteShortlistedSkill(){
		
		$data     = Request::all();

		if(isset($data['shortlistIds']) && $data['shortlistIds'] == ''){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Please select skill.");
			echo json_encode($response);
			exit;
		}
		
		$shortlistIds   = explode(',', $data['shortlistIds']);

		$userid = auth()->guard('web')->user()->id;
	
		ShortlistedSkillsModel::whereIn('i_shortlist_id',$shortlistIds)->where('i_user_id',$userid)->delete();
	
		$response['status']=1;
		$response['msg']=GeneralHelper::successMessage("Successfully removed shortlisted skills !");
		echo json_encode($response);
		exit;
	}

	public function activateProfile(){
		
		$data = Request::all();
		$response=array();

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$userid = auth()->guard('web')->user()->_id;
		$userdata = UsersModel::find($userid);
			
		if(!count($userdata)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		$isbasicplan = GeneralHelper::userbasicPlanCheck($userdata);

		if($isbasicplan){
			$response['status']=2;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		
		if($userdata->buyer['v_service']=="online"){
			$update['v_buyer_online_service']="active";
		}
		if($userdata->buyer['v_service']=="inperson"){
			$update['v_buyer_inperson_service']="active";
		}

		$update['buyer'] = $userdata->buyer;
		$update['buyer']['e_status'] = "active";
		$update['d_modified']=date("Y-m-d H:i:s");
		UsersModel::find($userdata->id)->update($update);

		$msg="You have successfully activated your profile.";
		if($userdata->seller['v_service']=="online"){
			$msg="You have successfully activated your Online profile.";
		}else{
			$msg="You have successfully activated your In Person profile.";			
		}

		$response['status']=1;
		$response['data']=$data;
		$response['msg']=GeneralHelper::successMessage($msg);
		echo json_encode($response);
		exit;

	}

	public function profileUpgradeUserPlan(){
	
		$data = Request::all();

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'buyer/dashboard',
			'v_activate_profile'=>'buyer',
			'v_action'=>'Activate profile',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],			
		);
		$userdata = auth()->guard('web')->user();

		if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869"){

			$userid = auth()->guard('web')->user()->id;
			$data['v_plan']['d_start_date']=date("Y-m-d");
			$data['v_plan']['d_end_date']=date('Y-m-d',strtotime('+4000 days'));	
			$data['buyer']=$userdata->buyer;

			if($userdata->buyer['v_service']=="online"){
				$data['v_buyer_online_service']="active";
			}
			if($userdata->buyer['v_service']=="inperson"){
				$data['v_buyer_inperson_service']="active";
			}
			$data['buyer']['e_status'] = "active";
			$planupdate = GeneralHelper::upgradeUserPlan($userid,$data);
			return redirect('buyer/dashboard')->with(['success' => "You're already subscribed to this plan."]);
		}

		$upgrade = GeneralHelper::upgradeUserPlanDetail($userdata,$data['v_plan']['id']);
		if($upgrade){
			Session::put('ordersummary',$orderdata);
			//return redirect('payment');
			return redirect('payment/card');
		}else{
			
			$update['buyer']=$userdata->buyer;
			$update['buyer']['e_status'] = "active";
			UsersModel::find($userdata->id)->update($update);
			return redirect('buyer/dashboard')->with(['success' => "You're already subscribed to this plan."]);
		}
	}

	public function updateService(){
		
		$data = Request::all();
		$response=array();

		if(!isset($data['v_service']) && $data['v_service']==""){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}

		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->_id;
		$userdata = UsersModel::find($userid);

		if(!count($userdata)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");
			echo json_encode($response);
			exit;
		}


		// if($data['v_service']=="online"){
		// 	$update['v_buyer_online_service']="active";
		// }else{
		// 	$update['v_buyer_inperson_service']="active";
		// }

		$statusdata="inactive";
		if($data['v_service']=="online" && $userdata->v_buyer_online_service=="active"){
			$statusdata="active";
		}
		if($data['v_service']=="inperson" && $userdata->v_buyer_inperson_service=="active"){
			$statusdata="active";
		}


		$update['buyer'] = $userdata->buyer;
		$update['buyer']['v_service'] = $data['v_service'];
		$update['buyer']['e_status'] = $statusdata;
		$update['d_modified']=date("Y-m-d H:i:s");
		UsersModel::find($userid)->update($update);

		$response['status']=1;
		$response['active']=$statusdata;
		$response['data']=$data;
		$response['msg']=GeneralHelper::successMessage("Successfully update user service.");
		echo json_encode($response);
		exit;
	}
	
	public function upgradeUserPlan(){
	
		$data = Request::all();

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'buyer/dashboard',
			'v_action'=>'upgrade user plan',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],			
		);

		if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869"){

			$userid = auth()->guard('web')->user()->id;
			$data['d_start_date']=date("Y-m-d");
			$data['d_end_date']=date('Y-m-d',strtotime('+4000 days'));
			$planupdate = GeneralHelper::upgradeUserPlan($userid,$data);
			return redirect('buyer/dashboard')->with(['success' => 'Successfully upgrade your plan.']);
		}
		$userdata = auth()->guard('web')->user();
		$upgrade = GeneralHelper::upgradeUserPlanDetail($userdata,$data['v_plan']['id']);

		if($upgrade){
			Session::put('ordersummary',$orderdata);
			return redirect('payment/card');
		}else{
			return redirect('buyer/dashboard')->with(['success' => "You're already subscribed to this plan."]);
		}
	}

	public function search(){
		
		$metaDetails = GeneralHelper::MetaFiledData('buyer-single-search','Buyer single search');
		$homepage = GeneralHelper::getHomepageImage();
		$_data=array(
			'metaDetails'=> $metaDetails,
			'homepage'=> $homepage
		);
		return view('frontend/buyer/single-search-page',$_data);

	}
	
	public function postJob(){
		if(!auth()->guard('web')->check()) {
			return redirect('account/job-post')->with(['warning' => 'Something went wrong']);
		}
	}


}