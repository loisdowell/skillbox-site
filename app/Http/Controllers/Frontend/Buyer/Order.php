<?php
namespace App\Http\Controllers\Frontend\Buyer;

use Request, Hash, Lang,Validator,Auth,PDF,Storage,File;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\BuyerCourse as BuyerCourseModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesAnnouncements as CoursesAnnouncements;
use App\Models\Courses\CoursesQA as CoursesQA;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;
use App\Models\OrderRequirements;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Sellerreview as SellerreviewModal;
use App\Helpers\Emailtemplate as EmailtemplateHelper;


class Order extends Controller {

	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.Courses');
	}

	public function index($status="") {
		
		$userid = auth()->guard('web')->user()->_id;
		$query = OrderModal::query();
		$query = $query->where('i_user_id',$userid);
		
		if($status != "cancelled"){
			if($status == "all"){
				$query = $query->where(function($query ) use($userid){
		        $query->where('e_payment_status','success')
		              ->orWhere('e_payment_status','refund');
		    	});	
		   	}else{
				$query = $query->where('e_payment_status','success');	
			}
			
		}

		$query = $query->where('e_transaction_type','skill');
		
		if($status == "active"){
			$query = $query->where('v_order_status','active');			
		}
		else if($status == "delivered"){
			$query = $query->where('v_order_status','delivered');	
			$query = $query->where('v_buyer_deliver_status','!=','accept');	
		}
		else if($status == "completed"){
			$query = $query->where(function($query ) use($userid){
		        $query->where('v_order_status','completed')
		              ->orWhere('v_buyer_deliver_status','accept');
		    });	
		}
		else if($status == "cancelled"){
			$query = $query->where('v_order_status','cancelled');	
		}
		else if($status == "awaiting"){
			$query = $query->where('v_order_review','no');
		}

		$data = $query->orderBy("d_added","DESC")->paginate(20);

		$query = OrderModal::query();
		$query = $query->where('i_user_id',$userid);
		//$query = $query->where('e_payment_status','success');
		$order = $query->where('e_transaction_type','skill')->get();

		$total['active']=0;
		$total['delivered']=0;
		$total['awaiting']=0;
		$total['cancelled']=0;
		$total['completed']=0;
		$total['order']=count($order);
		$total['orderamt']=0;

		if(count($order)){
			foreach ($order as $key => $value) {
				
				if($value->v_order_status=="cancelled"){
					$total['cancelled'] = $total['cancelled']+1;
				}

				if($value->e_payment_status=="success"){
					if($value->v_order_status=="active"){
						$total['active'] = $total['active']+1;
					}
					else if($value->v_order_status=="completed"){
						$total['completed'] = $total['completed']+1;
					}
					else if($value->v_order_status=="delivered"){
						if(isset($value->v_buyer_deliver_status) && $value->v_buyer_deliver_status=="accept"){
							$total['completed'] = $total['completed']+1;
						}else{
							$total['delivered'] = $total['delivered']+1;	
						}
					}
					if(isset($value->v_order_review) && $value->v_order_review=="no"){
						$total['awaiting'] = $total['awaiting']+1;
					}

					$total['orderamt']  =$total['orderamt']+$value->v_amount;
				}
			}	
		}
		$_data=array(
			'data'=>$data,			
			'status'=>$status,
			'total'=>$total,			
		);
		return view('frontend/buyer/order',$_data);
	}

	public function detail($id=""){
		
		$data = OrderModal::find($id);
		if(!count($data)){
			return redirect("buyer/orders");
		}

		$sellerprofile=array();
		
		if(isset($data->i_seller_profile_id) && count($data->i_seller_profile_id)){
			$sellerprofile = SellerprofileModel::find($data->i_seller_profile_id);
		}
		
		$orderRequirements = OrderRequirements::where("v_order_id",$id)->orderBy("d_added","ASC")->get();
		$userdata = auth()->guard('web')->user();

		$sellerreview = SellerreviewModal::where('i_order_id',$id)->first();

		$_data=array(
			'data'=>$data,	
			'orderRequirements'=>$orderRequirements,
			'sellerprofile'=>$sellerprofile,
			'userdata'=>$userdata,
			'sellerreview'=>$sellerreview,
		);
		return view('frontend/buyer/order_detail',$_data);
	}

	public function downloadInvoice($id=""){

		$data = OrderModal::find($id);
		$userdata = auth()->guard('web')->user();

		$_data=array(
			'data'=>$data,
			'userdata'=>$userdata
		);

		$pdf = PDF::loadView('frontend/buyer/order_invoice',$_data);
        return $pdf->download('invoice.pdf');
    }

	public function extendRequest(){

		$data= Request::all();
		$response = array();

		if(!isset($data['id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		$reqdata = OrderRequirements::find($data['id']);

		if(!count($reqdata)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		$update=array(
			'v_extend_resquest_status'=>$data['action'],
			'd_modified'=>date("Y-m-d H:i:s")
		);

		OrderRequirements::find($data['id'])->update($update);

		if($data['action']=="accept"){

			if(isset($reqdata->v_order_id) && $reqdata->v_order_id!=""){
				$orderdata = OrderModal::find($reqdata->v_order_id);
				if(count($orderdata) && $orderdata->v_duration_time){
					$update=array(
						'v_duration_time'=>$orderdata->v_duration_time+$reqdata->v_duration_time,
					);
					OrderModal::find($reqdata->v_order_id)->update($update);
				}
			}	
		}

		$btnstr="";
		$btnstr.='<div class="accepted-msg" style="border-bottom: 1px solid #b8b8b8 !important;">';
		$btnstr.='<p>Extend Order Request</p>';
		$btnstr.='<p class="litral-text">You have '.$data['action'].'ed to extend the order delivery by '.$reqdata['v_duration_time'].' '.$reqdata['v_duration_in'].'.</p>';
		$btnstr.='<p class="litral-text">'.date("H:i M d Y").'</p>';
		$btnstr.='</div>';

		if($data['action']=="accept"){
			$response['msg']=GeneralHelper::successMessage("You've accepted the request.");
		}else{
			$response['msg']=GeneralHelper::successMessage("You've rejected the request.");
		}
		$response['status']=1;
		$response['btnstr']=$btnstr;
		echo json_encode($response);
		exit;
	}


	public function acceptOrder(){
		
		$data= Request::all();
		
		if(isset($data['_token'])){
			unset($data['_token']);
		}

		if(!isset($data['v_order_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}
		$msgstr = "";
		
		if(isset($data['l_message']) && $data['l_message']!=""){
			
			$userid = auth()->guard('web')->user()->_id;
			$data['e_type']="message";
			$data['i_user_id']=$userid;
			$data['d_added'] = date("Y-m-d H:i:s");
			$data['d_modified'] = date("Y-m-d H:i:s");
			OrderRequirements::insert($data);
			$userdata = auth()->guard('web')->user();
		    $imgdata = \App\Helpers\General::userroundimage($userdata);
		    
		    $username="";
		    if(isset($userdata['v_fname']) && $userdata['v_lname']){
	            $username = $userdata['v_fname'].' '.$userdata['v_lname'];
	        }
	        $msgstr.='<div class="accordion-content-progress1">';
			$msgstr.='<div class="ans-qus-final">';
			$msgstr.='<div class="Announcements-find-progress">';
			$msgstr.='<div class="Announcements-man">'.$imgdata.'</div>';
			$msgstr.='<div class="Announcements-p-tag">';
			$msgstr.='<p>'.$username.'</p>';
			$msgstr.='<span class="span-text">'.$data['l_message'].'</span>';
			$msgstr.='<p class="time-p-tag">'.date("H:i M d, Y",strtotime($data['d_modified'])).'</p>';
			$msgstr.='</div>';
			$msgstr.='</div>';
			$msgstr.='</div>';
			$msgstr.='</div>';
		}
		
		$orderdata = OrderModal::find($data['v_order_id']);

		if(count($orderdata)){
			$update=array(
				'v_order_status'=>"delivered",
				'v_buyer_deliver_status'=>"accept",
				'd_order_delivered_date'=>date('Y-m-d'),
				'v_order_start'=>"no",
				'd_order_completed_date'=>date('Y-m-d H:i:s'),
				'v_order_review'=>"no",
			);
			OrderModal::find($data['v_order_id'])->update($update);
		}

		$response['status']=1;
		$response['msgstr']=$msgstr;
		$response['msg']=GeneralHelper::successMessage("Successfully accepted order delivery.");
		echo json_encode($response);
		exit;
	}

	public function orderReview(){

		$data= Request::all();

		if(isset($data['_token'])){
			unset($data['_token']);
		}

		if(!isset($data['i_order_id'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		$userid = auth()->guard('web')->user()->_id;

		$orderdata = OrderModal::find($data['i_order_id']);

		if(!count($orderdata)){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.Please try again after sometime.");
			echo json_encode($response);
			exit;
		}

		$data['i_user_id'] = $userid;
		$data['i_seller_id'] = $orderdata->i_seller_id;
		$data['i_seller_profile_id'] = $orderdata->i_seller_profile_id;
		$data['i_avg_star'] =  number_format(($data['i_communication_star']+$data['i_described_star']+$data['i_recommend_star'])/3,1);
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		SellerreviewModal::insert($data);
		self::orderReviewNotification($orderdata,$data['l_comment']);
		
		if(count($orderdata)){
			$update=array(
				'i_order_review'=>"1",
				'v_order_review'=>"yes",
			);
			OrderModal::find($data['i_order_id'])->update($update);
		}

		$response['status']=1;
		$response['msg']=GeneralHelper::successMessage("Successfully give review.");
		echo json_encode($response);
		exit;
	}

	public function orderReviewNotification($data=array(),$l_comment){

		if(!count($data)){
			return 0;
		}
		$order = $data;
	
		$mailData['SELLER_NAME']="";
        $mailData['USER_EMAIL']="";
        $mailData['BUYER_NAME']="";
        $mailData['SKILL_REVIEW']=$l_comment;
        $mailData['ORDER_NO']=$order->i_order_no;
        $mailData['ORDER_TITLE']=$order->v_order_detail[0]['name'];
        $mailData['ORDER_SELLER_LINK']=url('seller/orders/detail').'/'.$order->id;

        $mailData['BUYER_NAME'] = auth()->guard('web')->user()->v_fname;
        $mailData['BUYER_NAME'] = $mailData['BUYER_NAME'].' '.auth()->guard('web')->user()->v_lname;

        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_fname)){
            $mailData['SELLER_NAME']=$order->hasUserSeller()->v_fname;
        }
        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_lname)){
            $mailData['SELLER_NAME']= $mailData['SELLER_NAME'].' '.$order->hasUserSeller()->v_lname;
        }
        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_email)){
            $mailData['USER_EMAIL']= $order->hasUserSeller()->v_email;
        }

        $mailcontent = EmailtemplateHelper::SkillReviewSeller($mailData);
        $subject = "Buyer left review on order";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bbc7c0576fbae2bcb301e2d");
        $mailids=array(
            $mailData['SELLER_NAME']=>$mailData['USER_EMAIL']
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

	public function postRequirement(){

		$data= Request::all();
		$msg="Message posted successfully.";
		$subject="Buyer posted Message on your order.";

	
		if(isset($data['_token'])){
			unset($data['_token']);
		}

		if(!isset($data['v_order_id'])){
			return redirect('buyer/orders/active')->with(['warning' => 'Something went wrong.Please try again after sometime.']);		
		}

		if(Request::hasFile('v_document_order')) {
			
			$image = Request::file('v_document_order');
			foreach($image as $file){
	        	
	        	$fileName = 'order-'.time().'.'.$file->getClientOriginalExtension();
	        	$filenameorignal = $file->getClientOriginalName();
				$fileName = str_replace(' ', '_', $fileName);
				$filenameorignal = str_replace('.', '=', $filenameorignal);
				$data['v_document'][$filenameorignal] = 'order/' . $fileName;
				$uploadS3 = Storage::disk('s3')->put('/order/'.$fileName, File::get((string)$file), 'public');
		    }
		    unset($data['v_document_order']);
	    }		
	    
	    $order = OrderModal::find($data['v_order_id']);
	   	if(count($order)){
	   		if(isset($order->v_order_start) && $order->v_order_start=="yes"){
			}else{
				if(!isset($order->d_order_start_date)){
					$updateorder['v_order_start']="yes";
					$updateorder['v_order_requirements']="no";
					$updateorder['v_order_requirements_submited']="yes";
					$updateorder['d_order_start_date']=date("Y-m-d H:i:s");
					OrderModal::find($data['v_order_id'])->update($updateorder);

				}
			}
		}

	    $userid = auth()->guard('web')->user()->_id;
		$data['e_type']="message";
		$data['i_user_id']=$userid;
		$data['v_extend_resquest_status']="";
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		OrderRequirements::insert($data);

		if(isset($data['v_order_reqpost']) && $data['v_order_reqpost']==1){
			$userid = auth()->guard('web')->user()->_id;
			$insert['e_type']="req_posted";
			$insert['i_user_id']=$userid;
			$insert['v_order_id']=$data['v_order_id'];
			$insert['d_added'] = date("Y-m-d H:i:s");
			$insert['d_modified'] = date("Y-m-d H:i:s");
			OrderRequirements::insert($insert);
			$msg="Requirements successfully posted.";	
			$subject="Buyer submitted requirement on your order.";
		}

		if(isset($data['v_satisfied']) && $data['v_satisfied']==1){

			$userid = auth()->guard('web')->user()->_id;
			$insert['e_type']="not_satisfied";
			$insert['i_user_id']=$userid;
			$insert['v_order_id']=$data['v_order_id'];
			$insert['d_added'] = date("Y-m-d H:i:s");
			$insert['d_modified'] = date("Y-m-d H:i:s");
			OrderRequirements::insert($insert);

			$orderdata = OrderModal::find($data['v_order_id']);
			
			if(count($orderdata)){
				
				$currentdate=date("Y-m-d H:i:s");
				$totalsec = strtotime($currentdate)-strtotime($orderdata->v_order_deliver_date);
				$orderstartdate = $orderdata->d_order_start_date; 
				$d_order_start_date = date('Y-m-d H:i:s',strtotime('+'.$totalsec.' seconds',strtotime($orderstartdate)));

				$update=array(
					'v_order_status'=>"active",
					'v_order_start'=>"yes",
					'd_order_start_date'=>$d_order_start_date,
					'v_order_notstatisfied_date'=>$currentdate,
					'd_modified'=>date("Y-m-d H:i:s"),
				);
				
				OrderModal::find($data['v_order_id'])->update($update);	

				## Email to Seller for not satisfied
				$orderTitle="";
				if(isset($orderdata->v_order_detail[0]['name'])){
					$orderTitle = $orderdata->v_order_detail[0]['name'];
				}	

				$mailData['USER_FULL_NAME']="";
				$mailData['USER_EMAIL']="";
				$mailData['ORDER_NO']=$orderdata->i_order_no;
				$mailData['ORDER_TITLE']=$orderTitle;
				$mailData['BUYER_NAME']="";
				$mailData['SELLER_ORDER_LINK']=url('seller/orders/detail').'/'.$orderdata->id;

				if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_fname)){
		            $mailData['USER_FULL_NAME']=$order->hasUserSeller()->v_fname;
		        }
		        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_lname)){
		            $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$order->hasUserSeller()->v_lname;
		        }
		        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_email)){
		            $mailData['USER_EMAIL']= $order->hasUserSeller()->v_email;
		        }

		        if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_fname)){
			        $mailData['BUYER_NAME']= $orderdata->hasUser()->v_fname;
			    }
				if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_lname)){
			        $mailData['BUYER_NAME']= $mailData['BUYER_NAME'].' '.$orderdata->hasUser()->v_lname;
			    }
				$mailcontent = EmailtemplateHelper::DeliverOrderBuyerNotSatisfiedToSeller($mailData);
				$subject = "Not satisfied with Order";
				$subject = EmailtemplateHelper::EmailTemplateSubject("5bb6091a76fbae571722f446");
				
				$mailids=array(
					$mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
				);
				$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

			}
		}else{
			self::postRequirementEmailNotification($order,$data,$subject);
		}

		return redirect('buyer/orders/detail/'.$data['v_order_id'])->with(['success' => $msg]);		
	
	}

	public function postRequirementEmailNotification($order=array(),$data=array(),$mailsubject=""){

		
		$mailData['USER_FULL_NAME']="";
        $mailData['USER_EMAIL']="";
        $mailData['ORDER_NO']=$order->i_order_no;
        $mailData['ORDER_TITLE']=$order->v_order_detail[0]['name'];
        $mailData['ORDER_MESSAGE']=$data['l_message'];

        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_fname)){
            $mailData['USER_FULL_NAME']=$order->hasUserSeller()->v_fname;
        }
        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_lname)){
            $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$order->hasUserSeller()->v_lname;
        }
        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_email)){
            $mailData['USER_EMAIL']= $order->hasUserSeller()->v_email;
        }

        $mailcontent = EmailtemplateHelper::OrderRequirementsSeller($mailData);
        $subject = $mailsubject;//"Buyer posted Requirement";
        $mailids=array(
            $mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);



	}	
	

	




}