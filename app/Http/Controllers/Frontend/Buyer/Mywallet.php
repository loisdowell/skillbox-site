<?php
namespace App\Http\Controllers\Frontend\Buyer;

use Request, Hash, Lang,Validator,Auth,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\ShortlistedSkills as ShortlistedSkillsModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\Sellerreview as SellerreviewModel;
use App\Models\Users\Withdraw as WithdrawModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;


class Mywallet extends Controller {

	protected $section;
	private $mangopay;
	
	public function __construct(\MangoPay\MangoPayApi $mangopay){
		$this->section = Lang::get('section.Review center');
		$this->mangopay = $mangopay;
	}

	public function index() {

		$userid = auth()->guard('web')->user()->_id;

		$i_wallet_id = "";
		if(isset(auth()->guard('web')->user()->i_wallet_id)){
			$i_wallet_id = auth()->guard('web')->user()->i_wallet_id;
		}	
		$result = $this->mangopay->Wallets->Get($i_wallet_id);	

		$kycdoc = $this->mangopay->Users->get(auth()->guard('web')->user()->i_mangopay_id);	

		$amt = 0;
		if(count($result)){

			$amt = $result->Balance->Amount;
			$amt = $amt/100;


		}
		$data=array(
			'wallet'=>$amt,
			'kycdoc'=>$kycdoc,
		);
		return view('frontend/buyer/my_wallet', $data);
		
	}

	public function withdraw(){
		
		$data = Request::all();

		if(isset($data['vAmt']) && $data['vAmt']==""){
			return redirect('buyer/my-wallet')->with(['warning' => 'Amount is required.']);
		}

		$i_wallet_id = "";
		if(isset(auth()->guard('web')->user()->i_wallet_id)){
			$i_wallet_id = auth()->guard('web')->user()->i_wallet_id;
		}	
		$result = $this->mangopay->Wallets->Get($i_wallet_id);		

		$amt = 0;
		if(count($result)){
			$amt = $result->Balance->Amount;
			$amt = $amt/100;
		}

		if($data['vAmt']>$amt){
			return redirect('buyer/my-wallet')->with(['warning' => 'You cannot withdraw value more than your current wallet value.']);
		}

		if(!isset(auth()->guard('web')->user()->i_bank_id)){
			return redirect('profile/billing?wallet=1')->with(['warning' => "Please add your bank details to withdraw your desired amount. The details you provide here will be sent to MangoPay secure system, we don't keep your bank details at SKILLBOX for security reasons."]);	
		}
		$i_bank_id = auth()->guard('web')->user()->i_bank_id;
		if($i_bank_id==""){
			return redirect('profile/billing?wallet=1')->with(['warning' => "Please add your bank details to withdraw your desired amount. The details you provide here will be sent to MangoPay secure system, we don't keep your bank details at SKILLBOX for security reasons."]);
		}

		$i_mangopay_id = auth()->guard('web')->user()->i_mangopay_id;
		$i_wallet_id = auth()->guard('web')->user()->i_wallet_id;
		$mangopayamt = $data['vAmt']*100;

		
		$PayOut = new \MangoPay\PayOut();
		$PayOut->AuthorId = (int)$i_mangopay_id;
		$PayOut->DebitedWalletId = (int)$i_wallet_id;
		$PayOut->DebitedFunds = new \MangoPay\Money();
		$PayOut->DebitedFunds->Currency = "GBP";
		$PayOut->DebitedFunds->Amount = (float)$mangopayamt;
		$PayOut->Fees = new \MangoPay\Money();
		$PayOut->Fees->Currency = "GBP";
		$PayOut->Fees->Amount = 0;
		$PayOut->PaymentType = \MangoPay\PayOutPaymentType::BankWire;
		$PayOut->MeanOfPaymentDetails = new \MangoPay\PayOutPaymentDetailsBankWire();
		$PayOut->MeanOfPaymentDetails->BankAccountId = (int)$i_bank_id;
		$result = $this->mangopay->PayOuts->Create($PayOut);		
		
		$msg = "You've successfully withdraw the amount of £".number_format($data['vAmt'],2).". Please allow up to 5 working days for money to show in your bank account.";

		if(count($result)){

			$insert['i_user_id']=auth()->guard('web')->user()->_id;
			$insert['i_amount'] = $data['vAmt'];
			$insert['i_withdraw_id'] = $result->Id;
			$insert['i_wallet_id'] = $result->DebitedWalletId;
			$insert['i_author_id'] = $result->AuthorId;
			$insert['e_status'] = $result->Status;
			$insert['d_added'] = date("Y-m-d H:i:s");
			$insert['d_modified'] = date("Y-m-d H:i:s");
			WithdrawModel::create($insert);
			self::sendEmailNotification($msg);
		}
		sleep(5);
		return redirect('buyer/my-wallet')->with(['success' => $msg]);

	}

	public function sendEmailNotification($msg=""){
        
        if($msg==""){
            return 0;
        }
       
        $v_email = auth()->guard('web')->user()->v_email;
        $buyername="";
        if(isset(auth()->guard('web')->user()->v_fname) && auth()->guard('web')->user()->v_fname!=""){
            $buyername = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname) && auth()->guard('web')->user()->v_lname!=""){
            $buyername .= ' '.auth()->guard('web')->user()->v_lname;
        }

        $data=array(
            'name'=>$buyername,
            'MESSAGE'=>$msg,
        );
        $mailcontent = EmailtemplateHelper::WithdrawMoney($data);
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bd7ffd876fbae1e590bdf42");
        $mailids=array(
            $buyername=>$v_email,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

	

}