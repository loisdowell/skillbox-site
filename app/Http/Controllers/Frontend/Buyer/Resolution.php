<?php
namespace App\Http\Controllers\Frontend\Buyer;

use Request, Hash, Lang,Validator,Auth,PDF,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Courses\BuyerCourse as BuyerCourseModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Courses\CoursesCategory as CoursesCategoryModel;
use App\Models\Courses\CoursesLanguage as CoursesLanguageModel;
use App\Models\Courses\CoursesLevel as CoursesLevelModel;
use App\Models\Courses\CoursesAnnouncements as CoursesAnnouncements;
use App\Models\Courses\CoursesQA as CoursesQA;
use App\Models\Courses\CoursesReviews as CoursesReviews;
use App\Models\Users\Order as OrderModal;
use App\Models\OrderRequirements;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Sellerreview as SellerreviewModal;
use App\Models\Support\Resolution as ResolutionModal;
use App\Helpers\Emailtemplate as EmailtemplateHelper;


class Resolution extends Controller {

	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.Resolution');
	}


	public function index($id="") {
		
		$userid = auth()->guard('web')->user()->_id;
		$orderdata = OrderModal::find($id);

		if(!count($orderdata)){
			return redirect('buyer/orders/active');
		}
		
		$sellerprofile=array();
		
		if(isset($orderdata->i_seller_profile_id) && count($orderdata->i_seller_profile_id)){
			$sellerprofile = SellerprofileModel::find($orderdata->i_seller_profile_id);
		}

		$package="";

		if(isset($orderdata->v_order_detail) && count($orderdata->v_order_detail)){
			$package = $orderdata->v_order_detail[0]['package'];

		}

		$_data=array(
			'data'=>$orderdata,			
			'sellerprofile'=>$sellerprofile,			
			'package'=>$package,			
		);
		return view('frontend/buyer/resolution',$_data);
	}

	public function Step2(){
		
		$data= Request::all();	
		
		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$stepdata = $data;
		
		$orderdata = OrderModal::find($data['i_order_id']);
		if(!count($orderdata)){
			return redirect('buyer/orders/active');
		}
		
		$sellerprofile=array();
		
		if(isset($orderdata->i_seller_profile_id) && count($orderdata->i_seller_profile_id)){
			$sellerprofile = SellerprofileModel::find($orderdata->i_seller_profile_id);
		}
		$package="";
		if(isset($orderdata->v_order_detail) && count($orderdata->v_order_detail)){
			$package = $orderdata->v_order_detail[0]['package'];
		}

		$_data=array(
			'data'=>$orderdata,			
			'sellerprofile'=>$sellerprofile,			
			'package'=>$package,			
			'stepdata'=>$stepdata,			
		);
		return view('frontend/buyer/resolution_step_2',$_data);
	}

	public function Step3(){
		
		$data= Request::all();	
		
		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$stepdata = $data;
		
		$orderdata = OrderModal::find($data['i_order_id']);
		if(!count($orderdata)){
			return redirect('buyer/orders/active');
		}
		
		$sellerprofile=array();
		
		if(isset($orderdata->i_seller_profile_id) && count($orderdata->i_seller_profile_id)){
			$sellerprofile = SellerprofileModel::find($orderdata->i_seller_profile_id);
		}
		$package="";
		if(isset($orderdata->v_order_detail) && count($orderdata->v_order_detail)){
			$package = $orderdata->v_order_detail[0]['package'];
		}

		$_data=array(
			'data'=>$orderdata,			
			'sellerprofile'=>$sellerprofile,			
			'package'=>$package,			
			'stepdata'=>$stepdata,			
		);
		return view('frontend/buyer/resolution_step_3',$_data);

	}

	public function postRequest(){

		$data= Request::all();	
		
		if(isset($data['_token'])){
			unset($data['_token']);
		}

		$userid = auth()->guard('web')->user()->_id;

		$orderdata = OrderModal::find($data['i_order_id']);
		
		$data['i_user_id'] = $userid;
		$data['i_seller_id'] = $orderdata->i_seller_id;
		$data['e_status'] = "active";	
		$data['d_added'] = date("Y-m-d H:i:s");
		$data['d_modified'] = date("Y-m-d H:i:s");
		ResolutionModal::create($data);

		$userid = auth()->guard('web')->user()->_id;
		$insert['e_type']="order_cancel_req";
		$insert['i_user_id']=$userid;
		$insert['v_order_id']=$orderdata->id;
		$insert['d_added'] = date("Y-m-d H:i:s");
		$insert['d_modified'] = date("Y-m-d H:i:s");
		OrderRequirements::insert($insert);
	
		$order = $orderdata;
		$mailData['USER_FULL_NAME']="";
        $mailData['USER_EMAIL']="";
        $mailData['ORDER_NO']=$order->i_order_no;
        $mailData['ORDER_TITLE']=$order->v_order_detail[0]['name'];
        $mailData['SUBJECT']=$data['v_subject'];
        $mailData['MESSAGE']=$data['l_message'];

        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_fname)){
            $mailData['USER_FULL_NAME']=$order->hasUserSeller()->v_fname;
        }
        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_lname)){
            $mailData['USER_FULL_NAME']= $mailData['USER_FULL_NAME'].' '.$order->hasUserSeller()->v_lname;
        }
        if(count($order->hasUserSeller()) && isset($order->hasUserSeller()->v_email)){
            $mailData['USER_EMAIL']= $order->hasUserSeller()->v_email;
        }

        $mailcontent = EmailtemplateHelper::OrderCancelRequestToSeller($mailData);
        $subject = "Order Cancel Request";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5b5822c51b544d14d97708d3");
        
        $mailids=array(
            $mailData['USER_FULL_NAME']=>$mailData['USER_EMAIL']
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
        return redirect("buyer/orders/detail/".$data['i_order_id'])->with(['success' => 'Your request to cancel the order is sent to the seller.']);
	}

}