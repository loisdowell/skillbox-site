<?php
namespace App\Http\Controllers\Frontend\Account;

use Request, Hash, Lang,Validator,Auth,Storage,File;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Users\Messages as MessagesModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use Aws\S3\S3Client;
use App\Models\Users\Jobs as JobsModel;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use League\Flysystem\Filesystem;
use App\Models\Users\Sellerprofile as SellerprofileModel;


class Messages extends Controller {

	protected $section;

	public function __construct(){
		$this->section = Lang::get('section.dashboard');
	}

	public function userLogin() {
		
		$post_data = Request::all();
		
		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}
		if(!isset($post_data['i_to_id'])) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}
		$userdata = UsersModel::find($post_data['i_to_id']);
		if(!count($userdata)){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}
		
		$sellerdata=array();
		if(isset($post_data['pid'])) {
			$sellerdata = SellerprofileModel::find($post_data['pid']);
		}
		

		$_data=array(
			'userdata'=>$userdata,
			'post_data'=>$post_data,
			'sellerdata'=>$sellerdata,
		);

		// print_r($post_data);
		// exit;

		$messagestr = view('frontend/account/message', $_data); 
		$responsestr = $messagestr->render();

		$response['status']=1;
		$response['responsestr']=$responsestr;
		$response['msg']="you have to send message";
		echo json_encode($response);
		exit;
		
	}

	public function sendMessageSeller() {

		$post_data = Request::all();
		

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->_id;

		if(!isset($post_data['i_to_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}

		if(!isset($post_data['v_subject_title'])){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}

		if(isset($post_data['i_profile_id']) && $post_data['i_profile_id']!="") {
			$sellerdata = SellerprofileModel::where('_id',$post_data['i_profile_id'])->where('e_sponsor',"yes")->where('e_sponsor_status',"start")->first();
			if(count($sellerdata)){
				$updatedata['i_contact']=$sellerdata->i_contact+1;
				SellerprofileModel::find($sellerdata->id)->update($updatedata);
			}
		}

		if(isset($post_data['i_job_id']) && $post_data['i_job_id']!="") {
			$jobdata = JobsModel::where('_id',$post_data['i_job_id'])->where('e_sponsor',"yes")->where('e_sponsor_status',"start")->first();
			if(count($jobdata)){
				$updatedata['i_contact'] = 1;
				if(isset($jobdata->i_contact)){
					$updatedata['i_contact'] = $jobdata->i_contact+1;
				}
				$a = JobsModel::find($jobdata->id)->update($updatedata);
			}
		}
	
		$data['i_parent_id']=0;
		$data['v_subject_title']=$post_data['v_subject_title'];
		$data['i_to_id']=$post_data['i_to_id'];
		$data['i_from_id']=$userid;
		$data['l_message']=$post_data['l_message'];
		$data['e_view']="unread";
		$data['d_added']=date("Y-m-d H:i:s");
		$data['d_modified']=date("Y-m-d H:i:s");
		$mid = MessagesModel::create($data)->id;
		self::sendEmailNotificationSellerForMessage($data['i_to_id'],$post_data);	

		$sellerdata = UsersModel::find($post_data['i_to_id']);
		if(count($sellerdata)){
			if(isset($sellerdata->l_seller_auto_message) && $sellerdata->l_seller_auto_message!=""){
	
				$ctime=date("Y-m-d H:i:s");
				$ltime = $data['d_added'];
				$totaldiff =strtotime($ctime)-strtotime($ltime); 
				$totaldiff = $totaldiff;
				$insertreplay=array();
				$insertreplay['i_parent_id']=$mid;
				$insertreplay['v_subject_title']="";
				$insertreplay['i_to_id']=$userid;
				$insertreplay['i_from_id']=$post_data['i_to_id'];
				$insertreplay['l_message']=$sellerdata->l_seller_auto_message;
				$insertreplay['v_replay_time']=$totaldiff;
				$insertreplay['e_view']="unread";
				$insertreplay['d_added']=date("Y-m-d H:i:s");
				$insertreplay['d_modified']=date("Y-m-d H:i:s");
				$mid = MessagesModel::create($insertreplay)->id;

				self::sendEmailNotificationBuyerForMessage($post_data['i_to_id'],$userid,$sellerdata->l_seller_auto_message);	
			}
		}

		$response['status']=1;
		$response['msg']="Your message has sent successfully.";
		echo json_encode($response);
		exit;

	}

	public function sendEmailNotificationSellerForMessage($id="",$data=""){
        
		if($id==""){
            return 0;
        }

        $userdata = UsersModel::find($id);
		if(!count($userdata)){
			return 0;	
		}

		$sellername="";
        if(isset($userdata->v_fname)){
            $sellername = $userdata->v_fname;
        }
        if(isset($userdata->v_lname)){
            $sellername .= ' '.$userdata->v_lname;
        }

        $selleremail="";
        if(isset($userdata->v_email)){
            $selleremail = $userdata->v_email;
        }

        $buyername="";
        if(isset(auth()->guard('web')->user()->v_fname)){
            $buyername = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname)){
            $buyername .= ' '.auth()->guard('web')->user()->v_fname;
        }

        $messagestr="";
        if(isset($data['v_subject_title']) && $data['v_subject_title']!=""){
        	$messagestr  .= "<b>Subject </b>: ".$data['v_subject_title']."<br>";
        }
        if(isset($data['l_message']) && $data['l_message']!=""){
        	$messagestr  .= "<b>Message </b>: ".$data['l_message'];
        }

        $data=array(
            'USER_FULL_NAME'=>$sellername,
            'BUYER_NAME'=>$buyername,
            'MESSAGE'=>$messagestr,
        );
        $mailcontent = EmailtemplateHelper::sendMessageSeller($data);
        $subject = "Message";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bb5ba4276fbae17da5ac234");
        
        $mailids=array(
            $sellername=>$selleremail,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

    public function sendEmailNotificationBuyerForMessage($id="",$buyerid="",$msg=""){
        
		if($id==""){
            return 0;
        }
        $userdata = UsersModel::find($id);
		if(!count($userdata)){
			return 0;	
		}
		$sellername="";
        if(isset($userdata->v_fname)){
            $sellername = $userdata->v_fname;
        }
        if(isset($userdata->v_lname)){
            $sellername .= ' '.$userdata->v_lname;
        }

        $userdataBuyer = UsersModel::find($buyerid);
		if(!count($userdata)){
			return 0;	
		}
		$buyername="";
        if(isset($userdataBuyer->v_fname)){
            $buyername = $userdataBuyer->v_fname;
        }
        if(isset($userdataBuyer->v_lname)){
            $buyername .= ' '.$userdataBuyer->v_lname;
        }
        
        $buyeremail="";
        if(isset($userdataBuyer->v_email)){
            $buyeremail = $userdataBuyer->v_email;
        }
        $messagestr = "<b>Message </b>: ".$msg;

        $data=array(
            'USER_FULL_NAME'=>$buyername,
            'SELLER_NAME'=>$sellername,
            'MESSAGE'=>$messagestr,
        );
        $mailcontent = EmailtemplateHelper::sendMessageBuyer($data);
        $subject = "Message";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bb5bdfd76fbae2bb2317f43");
        $mailids=array(
            $buyername=>$buyeremail,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

   	public function saveDefaultMessage() {

		$post_data = Request::all();

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");//"Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->_id;

		if(!isset($post_data['l_seller_auto_message'])){
			$response['status']=0;
			$response['msg']=GeneralHelper::errorMessage("Something went wrong.");//"Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}

		$update['l_seller_auto_message'] = $post_data['l_seller_auto_message'];
		UsersModel::find($userid)->update($update);

		$response['status']=1;
		$response['msg']=GeneralHelper::successMessage("Default message succesfully saved.");//"Default message succesfully saved.";
		echo json_encode($response);
		exit;
	}

	public function sendListingReportAdmin() {

		$post_data = Request::all();

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->_id;
		if(!isset($post_data['i_sellerprofile_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}
		$url=url('online').'/'.$post_data['i_sellerprofile_id'];
		
		$messagestr="";
        $messagestr.="<table style='width:100%' border=2>";
       	$messagestr.="<tr>
	                    <td>Username</td>
	                    <td>".$post_data['v_name']."</td>
	                  </tr>"; 

	    $messagestr.="<tr>
	                    <td>Useremail</td>
	                    <td>".$post_data['v_email']."</td>
	                  </tr>";

	    $messagestr.="<tr>
	                    <td>About the issue</td>
	                    <td>".str_replace('_', ' ', $post_data['v_about_issue'])."</td>
	                  </tr>"; 

	    $messagestr.="<tr>
	                    <td>About the issue in detail</td>
	                    <td>".str_replace('_', ' ', $post_data['v_about_issue_detail'])."</td>
	                  </tr>";               

	    $messagestr.="<tr>
	                    <td>Comment</td>
	                    <td>".$post_data['l_comments']."</td>
	                  </tr>";               

	    $messagestr.="<tr>
	                    <td colspan='2'><a href='".$url."'>Click here to open listing</a></td>
	                  </tr>";  
	    $messagestr.="</table>";

	    $sitename = GeneralHelper::getSiteSetting('SITE_NAME');
        if($sitename==""){
            $sitename = "skillbox";
        }

        $siteemail = GeneralHelper::getSiteSetting('SITE_EMAIL');
        if($siteemail==""){
            $siteemail ="hello@skillbox.co.uk";
        }

        $data=array(
            'LISTING_DATA'=>$messagestr,
        );
        $mailcontent = EmailtemplateHelper::ListingreportAdmin($data);
        $subject = "Listing report";

        $subject = EmailtemplateHelper::EmailTemplateSubject("5bb1df6176fbae0ffb648742");
        $mailids=array(
            $sitename=>$siteemail,
        );
        //$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

       	$response['status']=1;
		$response['msg']="Your report for listing succesfully submit.";
		echo json_encode($response);
		exit;
	}

	public function buyerDashboardMessage(){

		$post_data = Request::all();
		$userid = auth()->guard('web')->user()->_id;

		$response=array();

		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
		});
		$buyermessage = $query->where('i_parent_id',0)->orderBy("d_modified","DESC")->paginate(6);
		
		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
		});
		$buyermessagecnt = $query->where('i_parent_id',0)->count();
		
		$total=6;
		if(isset($post_data['page']) && $post_data['page']!=""){
			$total = $total*$post_data['page'];
		}
		$buyermessagecnt = $buyermessagecnt-$total;

		if($buyermessagecnt>6){
			$buyermessagecnt=6;	
		}

		$btnstatus=0;
		if($buyermessage->lastPage()>$buyermessage->currentPage()){
			$btnstatus=1;
		}
		if($buyermessage->currentPage()>1){
			$btnstatus=0;
		}

		$_data=array(
			'buyermessage'=>$buyermessage,
		);

		$messagestr = view('frontend/buyer/dashboard-buyer-message', $_data); 
		$responsestr = $messagestr->render();

		$response['status']=1;
		$response['msgcntdata']=$buyermessagecnt;
		$response['btnstatus']=$btnstatus;
		$response['responsestr']=$responsestr;
		$response['msg']="Get review succ.";
		echo json_encode($response);
		exit;
	}

	public function sellerDashboardMessage(){

		$userid = auth()->guard('web')->user()->_id;
		$response=array();

		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
		});
		$buyermessage = $query->where('i_parent_id',0)->orderBy("d_modified","DESC")->paginate(6);

		//$buyermessage = MessagesModel::where('i_to_id',$userid)->where('i_parent_id',0)->orderBy("d_modified","DESC")->paginate(20);
		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
		});
		$buyermessagecnt = $query->where('i_parent_id',0)->count();

		//$buyermessagecnt = MessagesModel::where('i_to_id',$userid)->where('i_parent_id',0)->count();
		
		$total=6;
		if(isset($post_data['page']) && $post_data['page']!=""){
			$total = $total*$post_data['page'];
		}
		$buyermessagecnt = $buyermessagecnt-$total;

		if($buyermessagecnt>6){
			$buyermessagecnt=6;	
		}

		$btnstatus=0;
		if($buyermessage->lastPage()>$buyermessage->currentPage()){
			$btnstatus=1;
		}

		if($buyermessage->currentPage()>1){
			$btnstatus=0;
		}

		$_data=array(
			'buyermessage'=>$buyermessage,
		);

		$messagestr = view('frontend/seller/dashboard-seller-message', $_data); 
		$responsestr = $messagestr->render();

		$response['status']=1;
		$response['btnstatus']=$btnstatus;
		$response['msgcntdata']=$buyermessagecnt;
		$response['responsestr']=$responsestr;
		$response['msg']="Get review succ.";
		echo json_encode($response);
		exit;
	}

	public function buyerchatHistory() {
		
		$response = array();
		if(!auth()->guard('web')->check()) {
			return redirect('/login');
		}

		$userid = auth()->guard('web')->user()->_id;
		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
				//$q->where('i_from_id',$userid);
		});
		$data = $query->where('i_parent_id',0)->orderBy("d_modified","DESC")->paginate(20);

		
		// $query = MessagesModel::query();
		// $query = $query->where(function ($q) use ($userid) {
		// 	    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
		// 		$q->where('i_from_id',$userid);
		// });
		// $buyermessagecnt = $query->where('i_parent_id',0)->orderBy("d_modified","DESC")->count();
		
		// $total=20;
		// if(isset($post_data['page']) && $post_data['page']!=""){
		// 	$total = $total*$post_data['page'];
		// }
		// $buyermessagecnt = $buyermessagecnt-$total;

		$updatedata['e_view']="read";
		MessagesModel::where('i_from_id',$userid)->update($updatedata);

		$btnstatus=0;
		if($data->lastPage()>$data->currentPage()){
			$btnstatus=1;
		}

		$_data=array(
			'data'=>$data,
			'userid'=>$userid,
			'btnstatus'=>$btnstatus,

		);

		return view('frontend/buyer/chat_histroy',$_data);
	}

	public function sellerchatHistory() {
		
		$response = array();
		if(!auth()->guard('web')->check()) {
			return redirect('/login');
		}
		$userid = auth()->guard('web')->user()->_id;
		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
		});
		$data = $query->where('i_parent_id',0)->orderBy("d_modified","DESC")->paginate(20);
		
		$btnstatus=0;
		if($data->lastPage()>$data->currentPage()){
			$btnstatus=1;
		}
		$updatedata['e_view']="read";
		MessagesModel::where('i_from_id',$userid)->update($updatedata);

		$sellerautomessage="";
		if(isset(auth()->guard('web')->user()->l_seller_auto_message)){
			$sellerautomessage = auth()->guard('web')->user()->l_seller_auto_message;
		}

		$_data=array(
			'data'=>$data,
			'userid'=>$userid,
			'btnstatus'=>$btnstatus,
			'sellerautomessage'=>$sellerautomessage
		);
		return view('frontend/seller/chat_histroy',$_data);
	}
	
	public function buyerchatHistoryajaxtList() {

		$post_data = Request::all();

		$userid = auth()->guard('web')->user()->_id;
		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
		});	
		$data = $query->where('i_parent_id',0)->orderBy("d_modified","DESC")->paginate(20);


		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
		});
		$buyermessagecnt = $query->where('i_parent_id',0)->orderBy("d_modified","DESC")->count();
		
		$total=20;
		if(isset($post_data['page']) && $post_data['page']!=""){
			$total = $total*$post_data['page'];
		}
		$buyermessagecnt = $buyermessagecnt-$total;

		$btnstatus=0;
		if($data->lastPage()>$data->currentPage()){
			$btnstatus=1;
		}
		$_data=array(
			'data'=>$data,
			'userid'=>$userid
		);

		$messagestr = view('frontend/buyer/chathistorylist', $_data); 
		$responsestr = $messagestr->render();

		$pid="";
		if(count($data)){
			foreach ($data as $key => $value) {
				if($key<1){
					$pid = $value->_id;
				}
			}
		}

		$response['status']=1;
		$response['btnstatus']=$btnstatus;
		$response['pid']=$pid;
		$response['responsestr']=$responsestr;
		$response['msgcntdata']=$buyermessagecnt;
		$response['msg']="Get review succ.";
		echo json_encode($response);
		exit;
	}

	public function sellerchatHistoryajaxtList() {

		
		$userid = auth()->guard('web')->user()->_id;
		$post_data = Request::all();
		
		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
		});	
		$data = $query->where('i_parent_id',0)->orderBy("d_modified","DESC")->paginate(20);

		$query = MessagesModel::query();
		$query = $query->where(function ($q) use ($userid) {
			    $q->where('i_to_id', $userid)->orWhere('i_from_id',$userid);
		});
		$buyermessagecnt = $query->where('i_parent_id',0)->orderBy("d_modified","DESC")->count();
		
		$total=20;
		if(isset($post_data['page']) && $post_data['page']!=""){
			$total = $total*$post_data['page'];
		}
		$buyermessagecnt = $buyermessagecnt-$total;


		
		$btnstatus=0;
		if($data->lastPage()>$data->currentPage()){
			$btnstatus=1;
		}
		$_data=array(
			'data'=>$data,
			'userid'=>$userid
		);
		$messagestr = view('frontend/seller/chathistorylist', $_data); 
		$responsestr = $messagestr->render();

		$pid="";
		if(count($data)){
			foreach ($data as $key => $value) {
				if($key<1){
					$pid = $value->_id;
				}
			}
		}

		$response['status']=1;
		$response['btnstatus']=$btnstatus;
		$response['pid']=$pid;
		$response['responsestr']=$responsestr;
		$response['msgcntdata']=$buyermessagecnt;
		
		$response['msg']="Get review succ.";
		echo json_encode($response);
		exit;
	}

	public function messageDetail() {

		$post_data = Request::all();

		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->_id;

		if(!isset($post_data['id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime";
			echo json_encode($response);
			exit;
		}

		$firstMessage = MessagesModel::find($post_data['id']);

		$query = MessagesModel::query();
		$data = $query->where('i_parent_id',$post_data['id'])->orderBy("d_added","DESC")->get();

		$vfname="";
		$vlname="";

		if(count($firstMessage->hasFromUser()) && isset($firstMessage->hasFromUser()->v_fname)){
            $vfname = $firstMessage->hasFromUser()->v_fname;
        }
        if(count($firstMessage->hasFromUser()) && isset($firstMessage->hasFromUser()->v_lname)){
            $vlname = $firstMessage->hasFromUser()->v_lname;
        }

		$othername="Message with ".$vfname." ".$vlname;

		$_data=array(
			'data'=>$data,
			'userid'=>$userid,
			'firstMessage'=>$firstMessage
		);

		$messagestr = view('frontend/buyer/chathistorylistdetail', $_data); 
		$responsestr = $messagestr->render();

		$response['status']=1;
		$response['responsestr']=$responsestr;
		$response['othername']=$othername;
		$response['msg']="Get review succ.";
		echo json_encode($response);
		exit;

	}

	public function sendMessagetoSeller() {

		$post_data = Request::all();
		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime1";
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->_id;

		if(!isset($post_data['i_parent_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime2";
			echo json_encode($response);
			exit;
		}

		if(!isset($post_data['l_message'])){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime3";
			echo json_encode($response);
			exit;
		}

		$mddata = MessagesModel::find($post_data['i_parent_id']);
		if(!count($mddata)){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime4";
			echo json_encode($response);
			exit;
		}

		$mddata->d_modified=date("Y-m-d H:i:s");
		$mddata->save();
			
		$lastdata = MessagesModel::where('i_parent_id',$post_data['i_parent_id'])->orderBy('d_added',"DESC")->first();		
		$totaldiff=0;
		if(count($lastdata)){
			$ctime=date("Y-m-d H:i:s");
			$ltime=$lastdata->d_added;
			$totaldiff =strtotime($ctime)-strtotime($ltime); 
			$totaldiff = $totaldiff;//number_format($totaldiff/3600,2);
		}

		$toid="";
		if($mddata->i_to_id==$userid){
			$toid=$mddata->i_from_id;
		}else{
			$toid=$mddata->i_to_id;
		}	
		
		$data['i_parent_id']=$post_data['i_parent_id'];
		$data['v_subject_title'] = "";
		$data['i_to_id']=$toid;
		$data['i_from_id']=$userid;
		$data['v_replay_time']=$totaldiff;
		$data['l_message']=$post_data['l_message'];
		$data['e_view']="unread";
		$data['d_added']=date("Y-m-d H:i:s");
		$data['d_modified']=date("Y-m-d H:i:s");
		MessagesModel::create($data);
		self::sendEmailNotificationForMessage($toid,$post_data['l_message']);

		$v_image="";
		if(isset(auth()->guard('web')->user()->v_image) && auth()->guard('web')->user()->v_image!=""){
			$v_image = \Storage::cloud()->url(auth()->guard('web')->user()->v_image);
		}else{
			$v_image = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
		}
		
		$messagestr="";
		$messagestr.='<div class="chat-point">';
		$messagestr.='<div class="buyer-name">';
		$messagestr.='<img src="'.$v_image.'" alt="" />';
		$messagestr.='</div>';
		$messagestr.='<div class="chat-tital">';
		$messagestr.='<span>'.$post_data["l_message"].'</span><br>';
		$messagestr.='<span>'.date("h:i a",strtotime($data['d_added'])).'</span>';
		$messagestr.='&nbsp;(<span>'.\Carbon\Carbon::createFromTimeStamp(strtotime($data['d_added']))->diffForHumans().'</span>)';
		$messagestr.='</div>';
		$messagestr.='</div>';
		$response['status']=1;	
		$response['messagestr']=$messagestr;
		$response['msg']="succesfully send message";
		echo json_encode($response);
		exit;

	}

	public function sendMessagetoBuyer() {

		$post_data = Request::all();
		$response = array();
		if(!auth()->guard('web')->check()) {
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime1";
			echo json_encode($response);
			exit;
		}
		$userid = auth()->guard('web')->user()->_id;

		if(!isset($post_data['i_parent_id'])){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime2";
			echo json_encode($response);
			exit;
		}

		if(!isset($post_data['l_message'])){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime3";
			echo json_encode($response);
			exit;
		}

		$mddata = MessagesModel::find($post_data['i_parent_id']);
		if(!count($mddata)){
			$response['status']=0;
			$response['msg']="Something went wrong.please try again after sometime4";
			echo json_encode($response);
			exit;
		}

		$lastdata = MessagesModel::where('i_parent_id',$post_data['i_parent_id'])->orderBy('d_added',"DESC")->first();		
		$totaldiff=0;
		if(count($lastdata)){
			$ctime=date("Y-m-d H:i:s");
			$ltime=$lastdata->d_added;
			$totaldiff =strtotime($ctime)-strtotime($ltime); 
			$totaldiff = $totaldiff;//number_format($totaldiff/3600,2);
		}
		
		$toid="";
		if($mddata->i_to_id==$userid){
			$toid=$mddata->i_from_id;
		}else{
			$toid=$mddata->i_to_id;
		}	
		$data['i_parent_id']=$post_data['i_parent_id'];
		$data['v_subject_title'] = "";
		$data['i_to_id']=$toid;
		$data['i_from_id']=$userid;
		$data['v_replay_time']=$totaldiff;
		$data['e_view']="unread";
		$data['l_message']=$post_data['l_message'];
		$data['d_added']=date("Y-m-d H:i:s");
		$data['d_modified']=date("Y-m-d H:i:s");
		MessagesModel::create($data);

		self::sendEmailNotificationForMessage($toid,$post_data['l_message']);

		$v_image="";
		if(isset(auth()->guard('web')->user()->v_image) && auth()->guard('web')->user()->v_image!=""){
			$v_image = \Storage::cloud()->url(auth()->guard('web')->user()->v_image);
		}else{
			$v_image = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
		}

		$messagestr="";
		$messagestr.='<div class="chat-point">';
		$messagestr.='<div class="buyer-name">';
		$messagestr.='<img src="'.$v_image.'" alt="" />';
		$messagestr.='</div>';
		$messagestr.='<div class="chat-tital">';
		$messagestr.='<span>'.$post_data["l_message"].'</span><br>';
		$messagestr.='<span>'.date("h:i a",strtotime($data['d_added'])).'</span>';
		$messagestr.='&nbsp;(<span>'.\Carbon\Carbon::createFromTimeStamp(strtotime($data['d_added']))->diffForHumans().'</span>)';
		$messagestr.='</div>';
		$messagestr.='</div>';

		$response['status']=1;	
		$response['messagestr']=$messagestr;
		$response['msg']="succesfully send message";
		echo json_encode($response);
		exit;

	}

	public function sendEmailNotificationForMessage($toid="",$message=""){
        
		if($toid==""){
            return 0;
        }

        $userdata = UsersModel::find($toid);
		if(!count($userdata)){
			return 0;	
		}
		$data['USER_FULL_NAME']="";
		$data['FROM_NAME']="";
		$data['MESSAGE']=$message;
		$data['USER_EMAIL']="";

        if(isset(auth()->guard('web')->user()->v_fname)){
            $data['FROM_NAME'] = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname)){
            $data['FROM_NAME'] .= ' '.auth()->guard('web')->user()->v_lname;
        }

        if(isset($userdata->v_fname)){
            $data['USER_FULL_NAME'] = $userdata->v_fname;
        }
        
        if(isset($userdata->v_lname)){
            $data['USER_FULL_NAME'] .= ' '.$userdata->v_lname;
        }
        if(isset($userdata->v_email)){
            $data['USER_EMAIL'] = $userdata->v_email;
        }
       
        $mailcontent = EmailtemplateHelper::sendMessage($data);
        $subject = "Message";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bc1990376fbae0d22218954");
        
        $mailids=array(
            $data['USER_FULL_NAME']=>$data['USER_EMAIL'],
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
  
    }



}