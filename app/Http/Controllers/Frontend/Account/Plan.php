<?php
namespace App\Http\Controllers\Frontend\Account;

use Request, Hash, Lang,Validator,Auth;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Country as CountryModel;
use App\Helpers\General as GeneralHelper;

	
class Plan extends Controller {


	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.upgradeplan');
	}

	public function index() {
		
		if(!auth()->guard('web')->check()) {
			return redirect('/');
		}
		$plandata = GeneralHelper::PlansList();

		$data=array(
			'plandata'=>$plandata,
		);

		return view('frontend/account/planupgrade',$data);
	}


	public function thanksPlan() {

		return view('frontend/account/thanku');

	}
	


}