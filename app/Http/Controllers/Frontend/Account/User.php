<?php
namespace App\Http\Controllers\Frontend\Account;

use Request, Hash, Lang,Validator,Auth,Storage,File,Session;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Models\Country as CountryModel;
use App\Helpers\General as GeneralHelper;
use App\Helpers\Emailtemplate as EmailtemplateHelper;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use League\Flysystem\Filesystem;


class User extends Controller {

	protected $section;
	private $mangopay;

	public function __construct(\MangoPay\MangoPayApi $mangopay){
		$this->section = Lang::get('section.dashboard');
		$this->mangopay = $mangopay;
	}

	public function index() {
		
		$data['usertype']="jobpost";
		if(auth()->guard('web')->check()) {
			$userservice = auth()->guard('web')->user()->v_service;
			
			if($userservice=="online"){
				return redirect('job-post/online');		
			}else if($userservice=="inperson"){
				return redirect('job-post/inperson');		
			}

		}
		return view('frontend/account/signup',$data);
	}

	public function seller() {
		
		$data['usertype']="seller";
		if(auth()->guard('web')->check()) {
			return redirect('seller/dashboard');
		}
		return view('frontend/account/signup',$data);
	}

	public function buyer() {
	 	
	 	$rcart=array();
	 	if(Session::has('rcart')){
	 		$rcart = Session::get('rcart');	
	 	}
	   	$data['usertype']="buyer";
	   	if(count($rcart) && isset($rcart['v_service'])){
	   		$data['v_service']=$rcart['v_service'];	
	   		$data['v_upper_service']="no";	
	   	}
	    if(auth()->guard('web')->check()) {
			return redirect('single-search');
		}
		return view('frontend/account/signup',$data);
	}
	public function EmailConfirmMessage(){

		$userid = auth()->guard('web')->user()->_id;

		if(isset(auth()->guard('web')->user()->e_email_confirm_message) && auth()->guard('web')->user()->e_email_confirm_message=="yes"){
			$update['e_email_confirm_message'] = "no";
			$update['d_modified'] = date("Y-m-d H:i:s");
			UsersModel::find($userid)->update($update);
		}
		return 0;
	}

	public function confirmAccount($id=""){
		
		if($id==""){
			return redirect('/')->with(['warning' => 'Something went wrong.']);
		}

		$userdata = UsersModel::find($id);

		if(!count($userdata)){
			return redirect('/')->with(['warning' => 'Something went wrong.']);
		}
		$update['e_email_confirm'] = "yes";
		$update['e_email_confirm_message'] = "yes";
		$update['d_modified'] = date("Y-m-d H:i:s");
		UsersModel::find($id)->update($update);
		
		$username=$userdata->v_fname.' '.$userdata->v_lname;
		$data=array(
			'name'=>$username,
		);
		$mailcontent = EmailtemplateHelper::AccountVerified($data);
		$subject = "Account verified";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5b0e749376fbae2feb21ecb2");
		
		$mailids=array(
			$username=>$userdata->v_email,
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		return redirect('/login')->with(['success' => 'successfully verified your email.']);
	}

	public function signup() {
		
		$post_data = Request::all();
		$response = array();
		
		$usertype="";
		$userservice="";

		if(isset($post_data['e_type'])){
			$usertype = $post_data['e_type'];
			unset($post_data['e_type']);
		}
		
		if(isset($post_data['v_service'])){
			$userservice = $post_data['v_service'];
			unset($post_data['v_service']);
		}

		$existEmail = UsersModel::where('v_email',$post_data['v_email'])->get();
		if(count($existEmail)){
			if($usertype=="jobpost"){
				return redirect('account/job-post')->with(['warning' => 'Email already exist, please use different email address.']);	
			}else if($usertype=="seller"){
				return redirect('account/seller')->with(['warning' => 'Email already exist, please use different email address.']);	
			}else if($usertype=="buyer"){
				return redirect('account/buyer')->with(['warning' => 'Email already exist, please use different email address.']);	
			}
		}

		if(isset($post_data['_token'])){
			unset($post_data['_token']);
		}
		
		if(isset($post_data['repassword'])){
			unset($post_data['repassword']);
		}

		if(isset($post_data['password'])){
			$post_data['password'] = Hash::make($post_data['password']);
		}
		$data = GeneralHelper::baseRegistrationPlan();
		
		$post_data['v_buyer_online_service'] = "inactive";
		$post_data['v_buyer_inperson_service'] = "inactive";
		$post_data['v_seller_online_service'] = "inactive";
		$post_data['v_seller_inperson_service'] = "inactive";

		if($usertype=="seller"){
			if($userservice=="courses"){
				$post_data['seller']['v_service'] = "online";
				$post_data['seller']['e_temptype'] = "courses";
				$post_data['v_seller_online_service'] = "active";
			}else{
				if($userservice=="inperson"){
					$post_data['v_seller_inperson_service'] = "active";					
				}else{
					$post_data['v_seller_online_service'] = "active";	
				}
				$post_data['seller']['v_service'] = $userservice;
			}
			$post_data['seller']['e_status'] = "active";
			$post_data['buyer']['v_service'] = $post_data['seller']['v_service'];
			$post_data['buyer']['e_status'] = "inactive";
		}

		if($usertype=="buyer" || $usertype=="jobpost"){
			
			if($userservice=="courses"){
				$post_data['buyer']['v_service'] = "online";
				$post_data['seller']['v_service'] = "online";
				$post_data['v_buyer_online_service'] = "active";
			}else{
				
				if($userservice=="inperson"){
					$post_data['v_buyer_inperson_service'] = "active";					
				}else{
					$post_data['v_buyer_online_service'] = "active";	
				}
				$post_data['buyer']['v_service'] = $userservice;		
				$post_data['seller']['v_service'] = $userservice;
			}
			
			$post_data['buyer']['e_status'] = "active";
			if($usertype=="jobpost"){
				$post_data['buyer']['e_temptype'] = "jobpost";
			}
			$post_data['seller']['v_service'] = $userservice;
			$post_data['seller']['e_status'] = "inactive";

		}
		$post_data['v_plan']=array(
			'id'=>"5a65b48cd3e812a4253c9869",
			'duration'=>"monthly",
			'd_start_date'=>date("Y-m-d"),
			'd_end_date'=>date('Y-m-d',strtotime('+4000 days')),
		);

		$color=self::getAvatarColor();
		$user_name = $post_data['v_fname']." ".$post_data['v_lname'];
		$avatar = new \LasseRafn\InitialAvatarGenerator\InitialAvatar();
		$image_avatar = $avatar->name($user_name)
              ->length(2)
              ->fontSize(0.5)
              ->size(250) 
              ->background('#ddd')
              ->color('#222')
              ->generate()
              ->stream('png', 100);

        $fileName = 'profile-'.time().'.png';
	    $uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, $image_avatar->__toString(), 'public');     
	    $post_data['v_image']="users/".$fileName;
		
		$auth = auth()->guard('web');	
		$post_data['e_status']="active";
		$post_data['e_email_confirm']="no";
		
		$post_data['v_level']="0";
		$post_data['v_replies_time']="24 hours";
		$post_data['i_total_avg_review']=0;
		$post_data['i_total_review']=0;
		$post_data['i_course_total_review']=0;
		$post_data['i_course_total_avg_review']=0;
		$post_data['i_job_total_review']=0;
		$post_data['i_job_total_avg_review']=0;

		$post_data['e_login']="yes";
		$post_data['d_added']=date("Y-m-d H:i:s");
		$post_data['d_modified']=date("Y-m-d H:i:s");
		
		if(isset($post_data['v_company_type']) && $post_data['v_company_type']=="INDIVIDUAL"){
			$res = self::createMangopayUser($post_data);
			$post_data['i_mangopay_id']= $res['i_mangopay_id'];
			$post_data['i_wallet_id']= $res['i_wallet_id'];
		}else{
			$res = self::createMangopayUserLegal($post_data);
			$post_data['i_mangopay_id']= $res['i_mangopay_id'];
			$post_data['i_wallet_id']= $res['i_wallet_id'];
		}

		$userid = UsersModel::create($post_data)->id;
		$data=array(
			'name'=>$user_name,
			'confirm_account_link'=>url("confirm/account").'/'.$userid,
		);
		$mailcontent = EmailtemplateHelper::ConfirmUserAccount($data);
		$subject = "Confirm Email";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5ad81f45d3e812c30b3c9869");

		$mailids=array(
			$user_name=>$post_data['v_email']
		);

		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		$auth->attempt(Request::only('v_email', 'password'));
		return redirect('account/profile');
	}

	public function resendConfirmAccountEmail(){

		$userid = auth()->guard('web')->user()->_id;

		$data=array(
			'name'=>auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname,
			'confirm_account_link'=>url("confirm/account").'/'.$userid,
		);
		$mailcontent = EmailtemplateHelper::ConfirmUserAccount($data);
		$subject = "Confirm Email";
		$subject = EmailtemplateHelper::EmailTemplateSubject("5ad81f45d3e812c30b3c9869");
		
		$mailids=array(
			$data['name']=>auth()->guard('web')->user()->v_email,
		);
		$res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);

		$response['status']=1;
		$response['msg']="succesfully resend email.";
		return json_encode($response);
	}	

	public function profilePhoto(){
		$data['v_image']="";
		$data['v_image'] = \Storage::cloud()->url(auth()->guard('web')->user()->v_image);      
		return view('frontend/account/signup_profile',$data);
	}

	public function profilePhotoWebcame(){
		
		return view('frontend/account/signup_profile_webcame');
	}

	public function updateProfilePhoto(){

		$data = Request::all();
		$userid = auth()->guard('web')->user()->_id;
		
		$usertype ="";	
		$userservice ="";

		if(auth()->guard('web')->user()->buyer['e_status'] == "active"){
			$usertype =  "buyer";
			$userservice = auth()->guard('web')->user()->buyer['v_service'];

		}else if(auth()->guard('web')->user()->seller['e_status'] == "active"){
			$usertype = "seller";
			$userservice = auth()->guard('web')->user()->seller['v_service'];
		}

		if(Request::hasFile('v_image')) {

			$rules = ['v_image' => 'mimes:jpg,png,jpeg'];
			$validator = Validator::make($data, $rules);
			if ($validator->fails()) {
				return redirect('account/profile')->with(['warning' => 'Profile image should be jpg,png,jpeg.']);
			}
			$image = Request::file('v_image');
			$fileName = 'profile-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$update['v_image'] = 'users/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$image), 'public');
			$a = UsersModel::find($userid)->update($update);
		}

		if($usertype=="buyer"){
			
			$rcart=array();
		 	if(Session::has('rcart')){
		 		$rcart = Session::get('rcart');	
		 		Session::forget('rcart');
		 	}
		 	if(isset($rcart['rurl']) && $rcart['rurl']!=""){
				return redirect($rcart['rurl']);					
			}	

			if(!isset(auth()->guard('web')->user()->buyer['e_temptype'])){
				return redirect('single-search');
			}else{
				return redirect('buyer/post-job');
			}
		}

		if($usertype=="seller"){

			if(!isset(auth()->guard('web')->user()->seller['e_temptype'])){
				return redirect('seller/profile');
			}else{
				return redirect('courses/add');
			}
		}
		// if($usertype=="buyer" && !isset(auth()->guard('web')->user()->buyer['e_temptype'])){
		// 	return redirect('single-search');
		// }else if($usertype=="seller"){
		// 	return redirect('seller/profile');		
		// }else{
		// 	return redirect('buyer/post-job');
		// }
	}

	public function updateProfilePhotoWebcame(){

		$data = Request::all();
		$userid = auth()->guard('web')->user()->_id;
		if(Request::hasFile('webcam')) {

			$image = Request::file('webcam');
			$fileName = 'profile-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$update['v_image'] = 'users/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$image), 'public');
			$a = UsersModel::find($userid)->update($update);

		}	
		return 1;
	}

	public function WebcameUrl(){

		$data = Request::all();
		$userid = auth()->guard('web')->user()->_id;
		
		$usertype ="";	
		$userservice ="";

		if(auth()->guard('web')->user()->buyer['e_status'] == "active"){
			$usertype =  "buyer";
			$userservice = auth()->guard('web')->user()->buyer['v_service'];

		}else if(auth()->guard('web')->user()->seller['e_status'] == "active"){
			$usertype = "seller";
			$userservice = auth()->guard('web')->user()->seller['v_service'];
		}

		$urlstr="";

	
		if($usertype=="buyer"){
			
			if(!isset(auth()->guard('web')->user()->buyer['e_temptype'])){
				$urlstr = url('single-search');
			}else{
				$urlstr = url('buyer/post-job');
			}			
		}

		if($usertype=="seller"){

			if(!isset(auth()->guard('web')->user()->seller['e_temptype'])){
				$urlstr = url('seller/profile');
			}else{
				$urlstr = url('courses/add');
			}
		}

		return $urlstr;
	}

	public function search(){
		return view('frontend/buyer/single-search-page');
	}

	public function getSkill(){

		$data = Request::all();
		$response['status']=0;
		$response['msg']="Something Went Wrong";	

		if(isset($data['i_category_id'])){
			$skilldata = SkillsModel::where("i_category_id",$data['i_category_id'])->where("e_status","active")->get();
			$optionstr="<option value=''>-select-</option>";
			if(count($skilldata)){
				foreach ($skilldata as $key => $value) {
					if(isset($value->id) && isset($value->v_name)){
						$optionstr .="<option value='".$value->id."'>".$value->v_name."</option>";
					}
				}
			}
			$response['status']=1;
			$response['optionstr']=$optionstr;
			$response['msg']="succesfully list skill list.";
		}
		echo json_encode($response);
		exit;
	}

	public function editProfile(){

		if(!auth()->guard('web')->check()) {
			return redirect('/');		
		}

		$userid = auth()->guard('web')->user()->_id;
		$userdata = UsersModel::find($userid);
		$countrylist = CountryModel::where('e_status','active')->get();

		$leveldata=array();
		if(isset(auth()->guard('web')->user()->v_level) && auth()->guard('web')->user()->v_level!="0"){
            $leveldata = GeneralHelper::LevelData(auth()->guard('web')->user()->v_level);
        }

		$data=array(
			'userdata'=>$userdata,
			'countrylist'=>$countrylist,
			'leveldata'=>$leveldata,
		);
		return view('frontend/account/edit_profile',$data);
	}

	public function identificationDocuments(){
		
		$mangopayid = auth()->guard('web')->user()->i_mangopay_id;
			
		$userdata = auth()->guard('web')->user();
		$kycidenty = array();
		
		if(isset($userdata->i_identity_kyc_id) && count($userdata->i_identity_kyc_id)){
			$i_identity_kyc_id = $userdata->i_identity_kyc_id[0];
			$kycidenty = $this->mangopay->Users->GetKycDocument($mangopayid, $i_identity_kyc_id);
		}
		
		$registration_kyc = array();
		if(isset($userdata->i_registration_kyc_id) && count($userdata->i_registration_kyc_id)){
			$i_registration_kyc_id = auth()->guard('web')->user()->i_registration_kyc_id[0];
			$registration_kyc = $this->mangopay->Users->GetKycDocument($mangopayid, $i_registration_kyc_id);
		}

		$articles_kyc = array();
		if(isset($userdata->i_articles_kyc_id) && count($userdata->i_articles_kyc_id)){
			$i_articles_kyc_id = auth()->guard('web')->user()->i_articles_kyc_id[0];
			$articles_kyc = $this->mangopay->Users->GetKycDocument($mangopayid, $i_articles_kyc_id);
		}

		$shareholder_kyc = array();
		if(isset($userdata->i_shareholder_kyc_id) && count($userdata->i_shareholder_kyc_id)){
			$i_shareholder_kyc_id = auth()->guard('web')->user()->i_shareholder_kyc_id[0];
			$shareholder_kyc = $this->mangopay->Users->GetKycDocument($mangopayid, $i_shareholder_kyc_id);
		}
		$mangopayuser = $this->mangopay->Users->Get($mangopayid);
		$userdata = auth()->guard('web')->user();

		$_data=array(
			'mangopayuser'=>$mangopayuser,
			'userdata'=>$userdata,
			'kycidenty'=>$kycidenty,
			'registration_kyc'=>$registration_kyc,
			'articles_kyc'=>$articles_kyc,
			'shareholder_kyc'=>$shareholder_kyc,
		);

		return view('frontend/account/identificationDocuments',$_data);

	}

	public function uploadIdentificationDocuments(){

		$data = Request::all();
		
		$mangopayid = auth()->guard('web')->user()->i_mangopay_id;
		$userdata = auth()->guard('web')->user();
		

		$updatedata=array();
		if(isset($userdata->v_identity_img) && count($userdata->v_identity_img)){
			foreach ($userdata->v_identity_img as $key => $value) {

				$path = public_path('uploads/doc').'/'.$value;
				
				$KycDocument = new \MangoPay\KycDocument();
				$KycDocument->Type = "IDENTITY_PROOF";
				$result = $this->mangopay->Users->CreateKycDocument($mangopayid, $KycDocument);
				$KycDocumentId = $result->Id;
				
				$result2 = $this->mangopay->Users->CreateKycPageFromFile($mangopayid, $KycDocumentId, $path);
				$KycDocument = new \MangoPay\KycDocument();
				$KycDocument->Id = $KycDocumentId;

				$KycDocument->Status = \MangoPay\KycDocumentStatus::ValidationAsked;
				$result3 = $this->mangopay->Users->UpdateKycDocument($mangopayid, $KycDocument);
				$updatedata['i_identity_kyc_id'][]=$KycDocumentId;
				$updatedata['v_identity_img']=array();
				$updatedata['i_identity_kyc_refused_mail']="0";
			}
		}
		if(isset($userdata->v_company_type) && $userdata->v_company_type!="INDIVIDUAL"){

			if(isset($userdata->v_registration_img) && count($userdata->v_registration_img)){
				foreach ($userdata->v_registration_img as $key => $value) {

					$path = public_path('uploads/doc').'/'.$value;
					
					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Type = "REGISTRATION_PROOF";
					$result = $this->mangopay->Users->CreateKycDocument($mangopayid, $KycDocument);
					$KycDocumentId = $result->Id;
					
					$result2 = $this->mangopay->Users->CreateKycPageFromFile($mangopayid, $KycDocumentId, $path);
					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Id = $KycDocumentId;

					$KycDocument->Status = \MangoPay\KycDocumentStatus::ValidationAsked;
					$result3 = $this->mangopay->Users->UpdateKycDocument($mangopayid, $KycDocument);
					$updatedata['i_registration_kyc_id'][]=$KycDocumentId;
					$updatedata['v_registration_img']=array();
					$updatedata['i_registration_refused_mail']="0";

					
				}
			}

			if(isset($userdata->v_articles_img) && count($userdata->v_articles_img)){
				foreach ($userdata->v_articles_img as $key => $value) {

					$path = public_path('uploads/doc').'/'.$value;
					
					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Type = "ARTICLES_OF_ASSOCIATION";
					$result = $this->mangopay->Users->CreateKycDocument($mangopayid, $KycDocument);
					$KycDocumentId = $result->Id;
					
					$result2 = $this->mangopay->Users->CreateKycPageFromFile($mangopayid, $KycDocumentId, $path);
					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Id = $KycDocumentId;

					$KycDocument->Status = \MangoPay\KycDocumentStatus::ValidationAsked;
					$result3 = $this->mangopay->Users->UpdateKycDocument($mangopayid, $KycDocument);
					$updatedata['i_articles_kyc_id'][]=$KycDocumentId;
					$updatedata['v_articles_img']=array();
					$updatedata['i_articles_kyc_refused_mail']="0";

					
				}
			}

			if(isset($userdata->v_shareholder_img) && count($userdata->v_shareholder_img)){
				foreach ($userdata->v_shareholder_img as $key => $value) {

					$path = public_path('uploads/doc').'/'.$value;
					
					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Type = "SHAREHOLDER_DECLARATION";
					$result = $this->mangopay->Users->CreateKycDocument($mangopayid, $KycDocument);
					$KycDocumentId = $result->Id;
					
					$result2 = $this->mangopay->Users->CreateKycPageFromFile($mangopayid, $KycDocumentId, $path);
					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Id = $KycDocumentId;

					$KycDocument->Status = \MangoPay\KycDocumentStatus::ValidationAsked;
					$result3 = $this->mangopay->Users->UpdateKycDocument($mangopayid, $KycDocument);
					$updatedata['i_shareholder_kyc_id'][]=$KycDocumentId;
					$updatedata['v_shareholder_img']=array();
					$updatedata['i_shareholder_kyc_refused_mail']="0";
				}
			}
		}

		if(count($updatedata)){
			$userid = auth()->guard('web')->user()->_id;
			UsersModel::where('_id',$userid)->update($updatedata);
		}

		return redirect('profile/identification-documents')->with('success', 'Your document has been submitted successfully. Verifying your account may take up to 7 days. As soon as your account is verified by MangoPay we will notify you via email.'); 


		$updatedata=array();
		$destination = public_path('uploads/doc');
		$mangopayid = auth()->guard('web')->user()->i_mangopay_id;

		$fileName = 'img-identity-'.time().'.'.$v_identity_img->getClientOriginalExtension();
		$fileName = str_replace(' ', '_', $fileName);
		$updatedata['v_identity_img'] = $fileName;
		$v_identity_img->move($destination, $fileName);    

		$path = public_path('uploads/doc').'/'.$updatedata['v_identity_img'];

		$KycDocument = new \MangoPay\KycDocument();
		$KycDocument->Type = "IDENTITY_PROOF";
		$result = $this->mangopay->Users->CreateKycDocument($mangopayid, $KycDocument);
		$KycDocumentId = $result->Id;

		$result2 = $this->mangopay->Users->CreateKycPageFromFile($mangopayid, $KycDocumentId, $path);
		$KycDocument = new \MangoPay\KycDocument();
		$KycDocument->Id = $KycDocumentId;

		$KycDocument->Status = \MangoPay\KycDocumentStatus::ValidationAsked;
		$result3 = $this->mangopay->Users->UpdateKycDocument($mangopayid, $KycDocument);
		$updatedata['i_identity_kyc_id']=$KycDocumentId;

		if(isset($userdata->v_company_type) && $userdata->v_company_type!="INDIVIDUAL"){

			$fileName = 'img-registration-'.time().'.'.$v_registration_img->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$updatedata['v_registration_img'] = $fileName;
			$v_registration_img->move($destination, $fileName);    

			$path = public_path('uploads/doc').'/'.$updatedata['v_registration_img'];

			$KycDocument = new \MangoPay\KycDocument();
			$KycDocument->Type = "REGISTRATION_PROOF";
			$result = $this->mangopay->Users->CreateKycDocument($mangopayid, $KycDocument);
			$KycDocumentId = $result->Id;

			$result2 = $this->mangopay->Users->CreateKycPageFromFile($mangopayid, $KycDocumentId, $path);
			$KycDocument = new \MangoPay\KycDocument();
			$KycDocument->Id = $KycDocumentId;

			$KycDocument->Status = \MangoPay\KycDocumentStatus::ValidationAsked;
			$result3 = $this->mangopay->Users->UpdateKycDocument($mangopayid, $KycDocument);
			$updatedata['i_registration_kyc_id']=$KycDocumentId;


			if($userdata->v_company_type=="BUSINESS"){

	
					$fileName = 'img-articles-'.time().'.'.$v_articles_img->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$updatedata['v_articles_img'] = $fileName;
					$v_articles_img->move($destination, $fileName);    

					$path = public_path('uploads/doc').'/'.$updatedata['v_articles_img'];

					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Type = "ARTICLES_OF_ASSOCIATION";
					$result = $this->mangopay->Users->CreateKycDocument($mangopayid, $KycDocument);
					$KycDocumentId = $result->Id;

					$result2 = $this->mangopay->Users->CreateKycPageFromFile($mangopayid, $KycDocumentId, $path);
					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Id = $KycDocumentId;

					$KycDocument->Status = \MangoPay\KycDocumentStatus::ValidationAsked;
					$result3 = $this->mangopay->Users->UpdateKycDocument($mangopayid, $KycDocument);
					$updatedata['i_articles_kyc_id']=$KycDocumentId;


			#--------------------------------------------------------------------------------------------------#

					$fileName = 'img-shareholder-'.time().'.'.$v_shareholder_img->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$updatedata['v_shareholder_img'] = $fileName;
					$v_shareholder_img->move($destination, $fileName);    

					$path = public_path('uploads/doc').'/'.$updatedata['v_shareholder_img'];

					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Type = "SHAREHOLDER_DECLARATION";
					$result = $this->mangopay->Users->CreateKycDocument($mangopayid, $KycDocument);
					$KycDocumentId = $result->Id;

					$result2 = $this->mangopay->Users->CreateKycPageFromFile($mangopayid, $KycDocumentId, $path);
					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Id = $KycDocumentId;

					$KycDocument->Status = \MangoPay\KycDocumentStatus::ValidationAsked;
					$result3 = $this->mangopay->Users->UpdateKycDocument($mangopayid, $KycDocument);
					$updatedata['i_shareholder_kyc_id']=$KycDocumentId;


				
			}else if($userdata->v_company_type=="ORGANIZATION"){

					$fileName = 'img-articles-'.time().'.'.$v_articles_img->getClientOriginalExtension();
					$fileName = str_replace(' ', '_', $fileName);
					$updatedata['v_articles_img'] = $fileName;
					$v_articles_img->move($destination, $fileName);    

					$path = public_path('uploads/doc').'/'.$updatedata['v_articles_img'];

					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Type = "ARTICLES_OF_ASSOCIATION";
					$result = $this->mangopay->Users->CreateKycDocument($mangopayid, $KycDocument);
					$KycDocumentId = $result->Id;

					$result2 = $this->mangopay->Users->CreateKycPageFromFile($mangopayid, $KycDocumentId, $path);
					$KycDocument = new \MangoPay\KycDocument();
					$KycDocument->Id = $KycDocumentId;

					$KycDocument->Status = \MangoPay\KycDocumentStatus::ValidationAsked;
					$result3 = $this->mangopay->Users->UpdateKycDocument($mangopayid, $KycDocument);
					$updatedata['i_articles_kyc_id']=$KycDocumentId;

				
			}
		}

		if(count($updatedata)){
			$userid = auth()->guard('web')->user()->_id;
			UsersModel::where('_id',$userid)->update($updatedata);
		}

		return redirect('profile/identification-documents')->with('success', 'Your document has been submitted successfully. Verifying your account may take up to 7 days. As soon as your account is verified by MangoPay we will notify you via email.'); 

	}

	public function uploadIdentity(){

		$data = Request::all();
		$destination = public_path('uploads/doc');

		$userdata = auth()->guard('web')->user();
		$updatedata=array();
		if(isset($userdata->v_identity_img)){
			$updatedata['v_identity_img'] = $userdata->v_identity_img;
		}

		$fileName="";	
		if(Request::hasFile('file')) {
			$image = Request::file('file');
			$fileName = 'Identity-'.uniqid().time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$updatedata['v_identity_img'][] = $fileName;
			$image->move($destination, $fileName);    
		}

		if(count($updatedata)){
			$userid = auth()->guard('web')->user()->_id;
			UsersModel::where('_id',$userid)->update($updatedata);
		}	

		$appendstr="";
		$appendstr.='<div class="dz-default dz-message"><span>Drop files here to upload</span></div>';
		foreach($updatedata['v_identity_img'] as $k=>$v){
			$imgdaata =url('public/uploads/doc').'/'.$v;
			$appendstr.='<div class="dropzone-previews" style="width: auto;display: inline-block;" id="identity'.$k.'">';
			$appendstr.='<div class="dz-preview dz-processing dz-success dz-image-preview">';
			$appendstr.='<div class="dz-details"> ';
			$appendstr.='<img src="'.$imgdaata.'" style="max-width:100px;">';
			$appendstr.='</div>';
			$appendstr.='<a href="javascript:" onclick="remove_identity';
			$appendstr.="('".$v."','".$k."')";
			$appendstr.='"class="dz-remove">Remove Image</a></div>';
			$appendstr.='</div>';
		}
    	return $appendstr;
    }
    
    public function removeIdentity(){

		$data = Request::all();
		$destination = public_path('uploads/doc');
		$userdata = auth()->guard('web')->user();
		
		$updatedata=array();
		if(isset($userdata->v_identity_img)){
			$updatedata['v_identity_img'] = $userdata->v_identity_img;
		}

		if(count($updatedata)){
			foreach ($updatedata['v_identity_img'] as $key => $value) {
				if($value==$data['name']){
					unset($updatedata['v_identity_img'][$key]);
				}
			}
		}

		if(count($updatedata)){
			$userid = auth()->guard('web')->user()->_id;
			UsersModel::where('_id',$userid)->update($updatedata);
		}
		return 1;
    }

    public function uploadRegistration(){

		$data = Request::all();
		$destination = public_path('uploads/doc');

		$userdata = auth()->guard('web')->user();
		$updatedata=array();
		if(isset($userdata->v_registration_img)){
			$updatedata['v_registration_img'] = $userdata->v_registration_img;
		}

		$fileName="";	
		if(Request::hasFile('file')) {
			$image = Request::file('file');
			$fileName = 'Identity-'.uniqid().time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$updatedata['v_registration_img'][] = $fileName;
			$image->move($destination, $fileName);    
		}

		if(count($updatedata)){
			$userid = auth()->guard('web')->user()->_id;
			UsersModel::where('_id',$userid)->update($updatedata);
		}

		$appendstr="";
		$appendstr.='<div class="dz-default dz-message"><span>Drop files here to upload</span></div>';
		foreach($updatedata['v_registration_img'] as $k=>$v){
			$imgdaata =url('public/uploads/doc').'/'.$v;
			$appendstr.='<div class="dropzone-previews" style="width: auto;display: inline-block;" id="registration'.$k.'">';
			$appendstr.='<div class="dz-preview dz-processing dz-success dz-image-preview">';
			$appendstr.='<div class="dz-details"> ';
			$appendstr.='<img src="'.$imgdaata.'" style="max-width:100px;">';
			$appendstr.='</div>';
			$appendstr.='<a href="javascript:" onclick="remove_registration';
			$appendstr.="('".$v."','".$k."')";
			$appendstr.='"class="dz-remove">Remove Image</a></div>';
			$appendstr.='</div>';
		}
    	return $appendstr;
    }
    
    public function removeRegistration(){

		$data = Request::all();
		$destination = public_path('uploads/doc');
		$userdata = auth()->guard('web')->user();
		
		$updatedata=array();
		if(isset($userdata->v_registration_img)){
			$updatedata['v_registration_img'] = $userdata->v_registration_img;
		}

		if(count($updatedata)){
			foreach ($updatedata['v_registration_img'] as $key => $value) {
				if($value==$data['name']){
					unset($updatedata['v_registration_img'][$key]);
				}
			}
		}

		if(count($updatedata)){
			$userid = auth()->guard('web')->user()->_id;
			UsersModel::where('_id',$userid)->update($updatedata);
		}
		return 1;
    }

    public function uploadArticles(){

		$data = Request::all();
		$destination = public_path('uploads/doc');

		$userdata = auth()->guard('web')->user();
		$updatedata=array();
		if(isset($userdata->v_articles_img)){
			$updatedata['v_articles_img'] = $userdata->v_articles_img;
		}

		$fileName="";	
		if(Request::hasFile('file')) {
			$image = Request::file('file');
			$fileName = 'Identity-'.uniqid().time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$updatedata['v_articles_img'][] = $fileName;
			$image->move($destination, $fileName);    
		}

		if(count($updatedata)){
			$userid = auth()->guard('web')->user()->_id;
			UsersModel::where('_id',$userid)->update($updatedata);
		}

		$appendstr="";
		$appendstr.='<div class="dz-default dz-message"><span>Drop files here to upload</span></div>';
		foreach($updatedata['v_articles_img'] as $k=>$v){
			$imgdaata =url('public/uploads/doc').'/'.$v;
			$appendstr.='<div class="dropzone-previews" style="width: auto;display: inline-block;" id="articles'.$k.'">';
			$appendstr.='<div class="dz-preview dz-processing dz-success dz-image-preview">';
			$appendstr.='<div class="dz-details"> ';
			$appendstr.='<img src="'.$imgdaata.'" style="max-width:100px;">';
			$appendstr.='</div>';
			$appendstr.='<a href="javascript:" onclick="remove_articles';
			$appendstr.="('".$v."','".$k."')";
			$appendstr.='"class="dz-remove">Remove Image</a></div>';
			$appendstr.='</div>';
		}
    	return $appendstr;
    }
    
    public function removeArticles(){

		$data = Request::all();
		$destination = public_path('uploads/doc');
		$userdata = auth()->guard('web')->user();
		
		$updatedata=array();
		if(isset($userdata->v_articles_img)){
			$updatedata['v_articles_img'] = $userdata->v_articles_img;
		}

		if(count($updatedata)){
			foreach ($updatedata['v_articles_img'] as $key => $value) {
				if($value==$data['name']){
					unset($updatedata['v_articles_img'][$key]);
				}
			}
		}

		if(count($updatedata)){
			$userid = auth()->guard('web')->user()->_id;
			UsersModel::where('_id',$userid)->update($updatedata);
		}
		return 1;
    }

    public function uploadShareholder(){

		$data = Request::all();
		$destination = public_path('uploads/doc');

		$userdata = auth()->guard('web')->user();
		$updatedata=array();
		if(isset($userdata->v_shareholder_img)){
			$updatedata['v_shareholder_img'] = $userdata->v_shareholder_img;
		}

		$fileName="";	
		if(Request::hasFile('file')) {
			$image = Request::file('file');
			$fileName = 'Identity-'.uniqid().time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$updatedata['v_shareholder_img'][] = $fileName;
			$image->move($destination, $fileName);    
		}

		if(count($updatedata)){
			$userid = auth()->guard('web')->user()->_id;
			UsersModel::where('_id',$userid)->update($updatedata);
		}

		$appendstr="";
		$appendstr.='<div class="dz-default dz-message"><span>Drop files here to upload</span></div>';
		foreach($updatedata['v_shareholder_img'] as $k=>$v){
			$imgdaata =url('public/uploads/doc').'/'.$v;
			$appendstr.='<div class="dropzone-previews" style="width: auto;display: inline-block;" id="shareholder'.$k.'">';
			$appendstr.='<div class="dz-preview dz-processing dz-success dz-image-preview">';
			$appendstr.='<div class="dz-details"> ';
			$appendstr.='<img src="'.$imgdaata.'" style="max-width:100px;">';
			$appendstr.='</div>';
			$appendstr.='<a href="javascript:" onclick="remove_shareholder';
			$appendstr.="('".$v."','".$k."')";
			$appendstr.='"class="dz-remove">Remove Image</a></div>';
			$appendstr.='</div>';
		}
    	return $appendstr;
    }
    
    public function removeShareholder(){

		$data = Request::all();
		$destination = public_path('uploads/doc');
		$userdata = auth()->guard('web')->user();
		
		$updatedata=array();
		if(isset($userdata->v_shareholder_img)){
			$updatedata['v_shareholder_img'] = $userdata->v_shareholder_img;
		}

		if(count($updatedata)){
			foreach ($updatedata['v_shareholder_img'] as $key => $value) {
				if($value==$data['name']){
					unset($updatedata['v_shareholder_img'][$key]);
				}
			}
		}

		if(count($updatedata)){
			$userid = auth()->guard('web')->user()->_id;
			UsersModel::where('_id',$userid)->update($updatedata);
		}
		return 1;
    }





	public function updateProfile(){
		
		$data = Request::all();
		
		if(!auth()->guard('web')->check()) {
			return redirect('login')->with(['warning' => 'Something went wrong.']);
		}

		$userid = auth()->guard('web')->user()->_id;
		$existemail = UsersModel::where("v_email",$data['v_email'])->where('_id','!=',$userid)->get();

		$userdata = auth()->guard('web')->user();
		
		if(count($existemail)){
			return redirect('profile/edit')->with(['warning' => 'Email Already exist please use different.']);
		}
		
		if(Request::hasFile('v_image')) {
			$rules = ['v_image' => 'mimes:jpg,png,jpeg'];
			$validator = Validator::make($data, $rules);
			if ($validator->fails()) {
				return redirect('profile/edit')->with(['warning' => 'Profile image should be jpg,png,jpeg.']);
			}
			$image = Request::file('v_image');
			if(Storage::disk('s3')->exists(auth()->guard('web')->user()->v_image)){
				Storage::disk('s3')->Delete(auth()->guard('web')->user()->v_image);
			}
			$fileName = 'profile-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$data['v_image'] = 'users/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$image), 'public');
		}

		if(isset($data['_token'])){
			unset($data['_token']);
		}	
		
		if(isset($data['d_birthday']) && $data['d_birthday']!=""){
			//dd($data['d_birthday']);
			$bdata = explode("/", $data['d_birthday']);		

			if(!isset($bdata[0])){
				return redirect('profile/edit')->with(['warning' => 'Please enter valid your birthday']);
			}
			if(!isset($bdata[1])){
				return redirect('profile/edit')->with(['warning' => 'Please enter valid your birthday']);
			}
			if(!isset($bdata[2])){
				return redirect('profile/edit')->with(['warning' => 'Please enter valid your birthday']);
			}

			if($bdata[0]>31){
				return redirect('profile/edit')->with(['warning' => 'Please enter valid your birthday']);
			}

			if($bdata[1]>12){
				return redirect('profile/edit')->with(['warning' => 'Please enter valid your birthday']);
			}
			$year=date("Y");

			if($bdata[2]>$year){
				return redirect('profile/edit')->with(['warning' => 'Please enter valid your birthday']);
			}

			$bdatafinal = $bdata[2].'-'.$bdata[1].'-'.$bdata[0];
			$data['d_birthday'] = date("Y-m-d",strtotime($bdatafinal));
			
			// $day = date("m",strtotime($data['d_birthday']));
			// $month = date("d",strtotime($data['d_birthday']));
			// $year = date("Y",strtotime($data['d_birthday']));
			// $data['d_birthday'] = $year.'-'.$month.'-'.$day;
			// $data['d_birthday']=date("Y-m-d",strtotime($data['d_birthday']));
		}

		//dd($data['d_birthday']);
		
		if($userdata->v_company_type == "INDIVIDUAL"){
		
			if(!isset(auth()->guard('web')->user()->i_mangopay_id)){
				$res = self::createMangopayUser($data);
				$data['i_mangopay_id']= $res['i_mangopay_id'];
				$data['i_wallet_id']= $res['i_wallet_id'];
			}else{
				self::updateMangopayUser($data);
			}
			
		}else{

			if(!isset(auth()->guard('web')->user()->i_mangopay_id)){
				$res = self::createMangopayUserLegal($data);
				$data['i_mangopay_id']= $res['i_mangopay_id'];
				$data['i_wallet_id']= $res['i_wallet_id'];
			}else{
				
				$res = self::updateMangopayUserLegal($data);
			}
		}
		UsersModel::find($userid)->update($data);
		return redirect('dashboard')->with(['success' => "You've successfully updated your profile details."]);

	}

	public function editBilling(){

		if(!auth()->guard('web')->check()) {
			return redirect('/');		
		}

		$userid = auth()->guard('web')->user()->_id;
		$userdata = UsersModel::find($userid);
		$errormsg = \Session::get('warning');

		$data=array(
			'userdata'=>$userdata,
			'errormsg'=>$errormsg,
		);

		return view('frontend/account/edit_billing',$data);

	}

	public function updateBilling(){
		
		
		$data = Request::all();

		if(!auth()->guard('web')->check()) {
			return redirect('login')->with(['warning' => 'Something went wrong.']);
		}
		$userid = auth()->guard('web')->user()->_id;

		$i_bank_id = "";
		if(isset(auth()->guard('web')->user()->i_bank_id)){
			$i_bank_id = auth()->guard('web')->user()->i_bank_id;
		}
		
		$i_mangopay_id = auth()->guard('web')->user()->i_mangopay_id;
		$mangodata = $data['billing_detail'];

		try{
			$BankAccount = new \MangoPay\BankAccount();
			$BankAccount->Type = "GB";
			$BankAccount->Details = new \MangoPay\BankAccountDetailsGB();
			$BankAccount->Details->SortCode = $mangodata['v_SortCode'];
			$BankAccount->Details->AccountNumber = $mangodata['v_AccountNumber'];
			$BankAccount->OwnerName = $mangodata['v_OwnerName'];
			$BankAccount->OwnerAddress = new \MangoPay\Address();
			$BankAccount->OwnerAddress->AddressLine1 = $mangodata['v_AddressLine1'];
			
			if(isset($mangodata['v_AddressLine2']) && $mangodata['v_AddressLine2']!=""){
				$BankAccount->OwnerAddress->AddressLine2 = $mangodata['v_AddressLine2'];	
			}	

			$BankAccount->OwnerAddress->City = $mangodata['v_City'];
			$BankAccount->OwnerAddress->Country = $mangodata['v_Country'];
			$BankAccount->OwnerAddress->PostalCode = $mangodata['v_PostalCode'];
			$result = $this->mangopay->Users->CreateBankAccount($i_mangopay_id, $BankAccount);
			if(count($result)){
				$update['i_bank_id']=$result->Id;	
				$userid = auth()->guard('web')->user()->_id;
				UsersModel::find($userid)->update($update);
			}
		}
		catch (\Exception $e) {
		 	return redirect('profile/billing')->with(['warning' => 'Something went wrong with bank information.']);	
		}
	
		if(isset($data['_token'])){
			unset($data['_token']);
		}

		if(isset($data['wallet']) && $data['wallet']=="1"){
			return redirect('buyer/my-wallet')->with(['success' => 'succesfully added bank details.']);
		}
		if(isset($data['wallet']) && $data['wallet']=="2"){
			return redirect('seller/my-wallet')->with(['success' => 'succesfully added bank details.']);
		}
		return redirect('profile/billing')->with(['success' => 'succesfully added bank details.']);

	}

	public function createMangopayUser($data){
		
		$response['i_mangopay_id']=0;
		$response['i_wallet_id']=0;

		$UserNatural = new \MangoPay\UserNatural();
		$UserNatural->FirstName = $data['v_fname'];//"First Name";
		$UserNatural->LastName = $data['v_lname'];
		$UserNatural->Birthday = 1463496101;
		$UserNatural->Nationality = "GB";
		$UserNatural->Email = $data['v_email'];
		$UserNatural->CountryOfResidence = "GB";
		$result = $this->mangopay->Users->Create($UserNatural);

		if(count($result)){
			
			$mangopayid = $result->Id;
			$Wallet = new \MangoPay\Wallet();
			$Wallet->Tag = "custom meta";
			$Wallet->Owners = array ($result->Id);
			$Wallet->Description = "My skillbox project";
			$Wallet->Currency = "GBP";
			$resultWallet = $this->mangopay->Wallets->Create($Wallet);

			$response['i_mangopay_id']=$result->Id;
			$response['i_wallet_id']=$resultWallet->Id;

		}
		return $response;
	}

	public function getAvatarColor(){ 

		$a=array("900c3f","c70039","ff5733","ffc305","ff0554","7e0742","24900f","000","94d7df","7c543f","d21a1d","eb7322","fcd93c"); 
		$avatarColor=array_rand($a,1); 
		$Color = $a[$avatarColor]; 
		return $Color; 
	}

	public function createMangopayUserLegal($data){
		
		$response['i_mangopay_id']=0;
		$response['i_wallet_id']=0;

		if(!isset($data['v_company_name'])){
			$data['v_company_name']="Company name";
		}

		$UserLegal = new \MangoPay\UserLegal();
		$UserLegal->LegalPersonType=$data['v_company_type'];
		$UserLegal->Name = $data['v_company_name'];
		$UserLegal->LegalRepresentativeBirthday=1463496101;
		$UserLegal->LegalRepresentativeCountryOfResidence="GB";
		$UserLegal->LegalRepresentativeNationality="GB";
		$UserLegal->LegalRepresentativeFirstName=$data['v_fname'];
		$UserLegal->LegalRepresentativeLastName=$data['v_lname'];
		$UserLegal->Email=$data['v_email'];
		$result = $this->mangopay->Users->Create($UserLegal);
		
		if(count($result)){
			
			$mangopayid = $result->Id;
			$Wallet = new \MangoPay\Wallet();
			$Wallet->Tag = "custom meta";
			$Wallet->Owners = array ($result->Id);
			$Wallet->Description = "My skillbox project";
			$Wallet->Currency = "GBP";
			$resultWallet = $this->mangopay->Wallets->Create($Wallet);

			$response['i_mangopay_id']=$result->Id;
			$response['i_wallet_id']=$resultWallet->Id;
		}

		return $response;
	}

	public function updateMangopayUserLegal($data){
		
		$mangopay_id = auth()->guard('web')->user()->i_mangopay_id;
		$john = $this->mangopay->Users->Get($mangopay_id);
	
		if(isset($data['v_fname']) && $data['v_fname']!=""){
			$john->LegalRepresentativeFirstName=$data['v_fname'];	
		}
		if(isset($data['v_lname']) && $data['v_lname']!=""){
			$john->LegalRepresentativeLastName=$data['v_lname'];	
		}

		if(isset($data['l_address']) && $data['l_address']!=""){
			$john->LegalRepresentativeAddress->AddressLine1 = $data['l_address'];	
		}

		if(isset($data['v_city']) && $data['v_city']!=""){
			$john->LegalRepresentativeAddress->City = $data['v_city'];	
		}

		if(isset($data['v_postcode']) && $data['v_postcode']!=""){
			$john->LegalRepresentativeAddress->PostalCode = $data['v_postcode'];	
		}

		if(isset($data['d_birthday']) && $data['d_birthday']!=""){
			$john->LegalRepresentativeBirthday = strtotime($data['d_birthday']);	
		}

		$john->LegalRepresentativeAddress->Region = "GB";	
		$john->LegalRepresentativeAddress->Country = "GB";	

		if(isset($data['v_company_type']) && $data['v_company_type']!=""){
			$john->LegalPersonType = $data['v_company_type'];	
		}
		
		try{
			$a = $this->mangopay->Users->Update($john);
			return 1;
		}
		catch (\Exception $e) {
		 	return redirect('profile/edit')->with(['warning' => 'Something went wrong with details.']);	
		}
		return 1;
	}

	public function updateMangopayUser($data){
		
		$mangopay_id = auth()->guard('web')->user()->i_mangopay_id;
		$john = $this->mangopay->Users->Get($mangopay_id);
		
		
		if(isset($data['v_fname']) && $data['v_fname']!=""){
			$john->FirstName=$data['v_fname'];	
		}
		if(isset($data['v_lname']) && $data['v_lname']!=""){
			$john->LastName=$data['v_lname'];	
		}

		if(isset($data['l_address']) && $data['l_address']!=""){
			$john->Address->AddressLine1 = $data['l_address'];	
		}

		if(isset($data['v_city']) && $data['v_city']!=""){
			$john->Address->City = $data['v_city'];	
		}

		if(isset($data['v_postcode']) && $data['v_postcode']!=""){
			$john->Address->PostalCode = $data['v_postcode'];	
		}

		if(isset($data['d_birthday']) && $data['d_birthday']!=""){
			$john->Birthday = strtotime($data['d_birthday']);	
		}

		$john->Address->Region = "GB";	
		$john->Address->Country = "GB";	
		$john->Nationality = "GB";	
		$john->CountryOfResidence = "GB";	

		try{
			$a = $this->mangopay->Users->Update($john);
			return 1;
		}
		catch (\Exception $e) {
		 	return redirect('profile/edit')->with(['warning' => 'Something went wrong with details.']);	
		}
		return 1;
	}

	public function changePassword(){
		return view('frontend/account/change_password');
	}

	public function updatePassword(){
		$post_data = Request::all();
		// dd($post_data);
		if(!isset($post_data['password']) && $post_data['password'] ==''){
			return redirect('profile/change-password')->with(['warning' => 'this value is required.']);	
		}
		if(!isset($post_data['confirm_password']) && $post_data['confirm_password'] ==''){
			return redirect('profile/change-password')->with(['warning' => 'this value is required.']);	
		}

		if(isset($post_data['_token'])){
			unset($post_data['_token']);
		}
		
		if(isset($post_data['confirm_password'])){
			unset($post_data['confirm_password']);
		}

		if(isset($post_data['password'])){
			$post_data['password'] = Hash::make($post_data['password']);
		}

		$userData = auth()->guard('web')->user();
		$userData->password = $post_data['password'];
		$userData->save();	

		return redirect('profile/change-password')->with(['success' => 'Your password has been Updated succesfully.']);	

	}

}