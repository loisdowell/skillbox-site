<?php
namespace App\Http\Controllers\Frontend\Account;

use Request, Hash, Lang,Validator,Auth,Session,Storage,File;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Support\Support as SupportModel;
use App\Models\Users\Messages as MessagesModel;



use Aws\S3\S3Client;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use League\Flysystem\Filesystem;


class Dashboard extends Controller {

	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.dashboard');
	}

	public function index() {
		
		if(!auth()->guard('web')->check()) {
			return redirect('/');		
		}

		if(!isset(auth()->guard('web')->user()->i_mangopay_id)){
			return redirect('profile/edit');	
		}
		if(!isset(auth()->guard('web')->user()->i_wallet_id)){
			return redirect('profile/edit');	
		}
		
		$summaryData = GeneralHelper::AccountSummary();

		$userid = auth()->guard('web')->user()->_id;
		$userdata = UsersModel::find($userid);
		$usercnt = UsersModel::where("e_status","active")->get();
		$countryname = count($userdata->hasCountry())  ? $userdata->hasCountry()->v_title : '';
					
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");
		$userplandata = GeneralHelper::userPlanDetail($userdata);
	
		$leveldata=array();
		if(isset(auth()->guard('web')->user()->v_level) && auth()->guard('web')->user()->v_level!="0"){
            $leveldata = GeneralHelper::LevelData(auth()->guard('web')->user()->v_level);
        }
        $supportcnt = SupportModel::where('e_view',"unread")->where('i_touser_id',auth()->guard('web')->user()->id)->count();
        $msgcnt = MessagesModel::where('e_view',"unread")->where('i_from_id',auth()->guard('web')->user()->id)->count();

       	$data=array(
			'userdata'=>$userdata,
			'countryname'=>$countryname,
			'usercnt'=>count($usercnt),
			'plandata'=>$plandata,
			'discount'=>$discount,
			'userplandata'=>$userplandata,
			'summaryData'=>$summaryData,
			'leveldata'=>$leveldata,
			'supportcnt'=>$supportcnt,
			'msgcnt'=>$msgcnt,
		);
		return view('frontend/account/dashboard',$data);

	}
	
	public function buyerDashboard() {

		if(!auth()->guard('web')->check()) {
			return redirect('/');		
		}

		$userid = auth()->guard('web')->user()->_id;
		$userdata = UsersModel::find($userid);
		$usercnt = UsersModel::where("e_status","active")->get();
		$countryname = count($userdata->hasCountry())  ? $userdata->hasCountry()->v_title : '';
		$plandata = GeneralHelper::PlansList();
		$discount = GeneralHelper::getSiteSetting("PLAN_YEALY_DISCOUNT");

		$userplandata = GeneralHelper::userPlanData($userid);
		
		$data=array(
			'userdata'=>$userdata,
			'countryname'=>$countryname,
			'plandata'=>$plandata,
			'usercnt'=>count($usercnt),
			'discount'=>$discount,
			'userplandata'=>$userplandata,
		);


		return view('frontend/account/buyer_dashboard',$data);
	}

	public function Alertboxclose() {

		$data = Request::all();
		$userid = auth()->guard('web')->user()->_id;

		$updateuser=array();
		if(isset($data['box']) && $data['box']=="1"){
			$updateuser['v_alert_box_close_1']="1";
		}
		if(isset($data['box']) && $data['box']=="2"){
			$updateuser['v_alert_box_close_2']="1";
		}

		if(count($updateuser)){
			UsersModel::find($userid)->update($updateuser);	
		}	

		$response['status']=1;
		echo json_encode($response);
		exit;
	}

	public function upgradeUserPlan(){
	
		
		$data = Request::all();
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}

		if(isset($data['_token'])){
			unset($data['_token']);
		}
		
		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'dashboard',
			'v_action'=>'upgrade user plan',
			'v_plan_id'=>$data['v_plan']['id'],			
			'v_plan_duration'=>$data['v_plan']['duration'],			
		);

		if($data['v_plan']['id']=="5a65b48cd3e812a4253c9869"){
			$userid = auth()->guard('web')->user()->id;
			$data['d_start_date']=date("Y-m-d");
			$data['d_end_date']=date('Y-m-d',strtotime('+4000 days'));
			$planupdate = GeneralHelper::upgradeUserPlan($userid,$data);
			return redirect('dashboard')->with(['success' => 'Successfully upgrade your plan.']);
		}
		$userdata = auth()->guard('web')->user();
		
		$upgrade = GeneralHelper::upgradeUserPlanDetail($userdata);
		if($upgrade){
			Session::put('ordersummary',$orderdata);
			//return redirect('payment');
			return redirect('payment/card');

		}else{
			return redirect('dashboard')->with(['success' => "You're already subscribed to this plan."]);
		}

	}
	
	public function upgradeUserPlanApp(){
		
		$data = Request::all();
		// $auth = auth()->guard('web');

		$uData  =  UsersModel::where("v_auth_token",$data['token'])->first();
		Auth::guard('web')->login($uData, true);

		$userdata = auth()->guard('web')->user();
	
		if(!auth()->guard('web')->check()) {
			return redirect("/");
		}
		
		$orderdata=array(
			'type'=>'upgradeuserplan',
			'redirect'=>'dashboard',
			'v_action'=>'upgrade user plan',
			'v_plan_id'=>$data['v_plan_id'],			
			'v_plan_duration'=>$data['v_plan_duration'],			
			'app'=>'1',
		);
		
		if($data['v_plan_id']=="5a65b48cd3e812a4253c9869"){
			$userid = auth()->guard('web')->user()->id;
			$data['d_start_date']=date("Y-m-d");
			$data['d_end_date']=date('Y-m-d',strtotime('+4000 days'));
			$planupdate = GeneralHelper::upgradeUserPlan($userid,$data);
			return redirect('dashboard')->with(['success' => 'Successfully upgrade your plan.']);
		}
		$userdata = auth()->guard('web')->user();
		$upgrade = GeneralHelper::upgradeUserPlanDetail($userdata);

		

		
		if($upgrade){
			Session::put('ordersummary',$orderdata);
			return redirect('payment/card');
		}else{
			return redirect('dashboard')->with(['success' => "You're already subscribed to this plan."]);
		}
	}



	public function updateProfileImage(){
		
		$data = Request::all();
		
		$response['status']=0;

		if(!auth()->guard('web')->check()) {
			echo json_encode($response);
			exit;	
		}	
		if(isset($data['_token'])){
			unset($data['_token']);
		}
		$userid = auth()->guard('web')->user()->_id;

		if(isset(auth()->guard('web')->user()->v_image)){
			$imageExists = auth()->guard('web')->user()->v_image;
			if(Storage::disk('s3')->exists($imageExists)){
				Storage::disk('s3')->Delete($imageExists);
			}	
		}	
		
		if(Request::hasFile('v_image')) {

			$image = Request::file('v_image');
			$fileName = 'profile-'.time().'.'.$image->getClientOriginalExtension();
			$fileName = str_replace(' ', '_', $fileName);
			$update['v_image'] = 'users/' . $fileName;
			$uploadS3 = Storage::disk('s3')->put('/users/'.$fileName, File::get((string)$image), 'public');
			$a = UsersModel::find($userid)->update($update);
			
			$response['status']=1;
		}

		echo json_encode($response);
		exit;

	}

	public function cancelAccountConfirm() {

		$userdata = auth()->guard('web')->user();
		$userplandata = GeneralHelper::userPlanDetail($userdata);
		$_data=array(
			'userplandata'=>$userplandata,
		);
		return view('frontend/account/account_delete_confirm',$_data);
	}


	public function deactivateAccount() {

		if(!auth()->guard('web')->check()) {
			return redirect('/');		
		}
		$id = auth()->guard('web')->user()->_id;

		self::deleteaccountnotification();
	   	
	   	$userdata = auth()->guard('web')->user();
        if(count($userdata)){
        	
        	$updateuser['e_login']="no";
        	$updateuser['i_delete']="1";
        	$updateuser['v_email']="";
        	UsersModel::find($id)->update($updateuser);
        	
        	$update['i_delete']="1";
        	SellerprofileModel::where('i_user_id',$id)->update($update);
        	JobsModel::where('i_user_id',$id)->update($update);
        	CoursesModel::where('i_user_id',$id)->update($update);

        	if(auth()->guard('web')->check()) {
				$auth = auth()->guard('web');
				$userid = auth()->guard('web')->user()->id;
			    $auth->logout();
			}
	    }
	    return redirect('/');
 	}

	public function downgradeAccount() {

		if(!auth()->guard('web')->check()) {
			return redirect('/');		
		}
		$id = auth()->guard('web')->user()->_id;
	  
	   	$userdata = auth()->guard('web')->user();
        if(count($userdata)){

        	$update['v_plan']=array(
				'id'=>"5a65b48cd3e812a4253c9869",
				'duration'=>"monthly",
				'd_start_date'=>date("Y-m-d"),
				'd_end_date'=>date('Y-m-d',strtotime('+4000 days')),
			);
			$update['i_card_id']="";
			UsersModel::find($id)->update($update);
			
			$updatedata['e_sponsor']="no";
			$updatedata['e_sponsor_status']="pause";
			$updatedata['i_clicks']=0;
			$updatedata['i_contact']=0;
			$updatedata['i_impression']=0;
			$updatedata['i_impression_perday']=0;
			CoursesModel::where('i_user_id',$id)->update($updatedata);
			JobsModel::where('i_user_id',$id)->update($updatedata);
			SellerprofileModel::where('i_user_id',$id)->update($updatedata);

			$jobdataonline = JobsModel::where('i_user_id',$id)->where('v_service',"online")->orderBy("d_added","DESC")->get();
			$jids=array();
			if(count($jobdataonline) && count($jobdataonline)>5){
				foreach ($jobdataonline as $key => $value) {
					if($key>4){
						$jids[]=$value->id;			
					}	
				}
			}
			if(count($jids)){
				$updatejob['i_delete']="1";
				JobsModel::whereIn('_id',$jids)->update($updatejob);	
			}


			$jobdataonline = JobsModel::where('i_user_id',$id)->where('v_service',"inperson")->orderBy("d_added","DESC")->get();
			$jids=array();
			if(count($jobdataonline) && count($jobdataonline)>5){
				foreach ($jobdataonline as $key => $value) {
					if($key>4){
						$jids[]=$value->id;			
					}	
				}
			}
			if(count($jids)){
				$updatejob['i_delete']="1";
				JobsModel::whereIn('_id',$jids)->update($updatejob);	
			}
	
	    }
	    self::downgradeaccountnotification();
	    return redirect('dashboard')->with(['success' => 'Your account successfully downgraded to Basic plan.']);
 	}

	public function deleteaccountnotification(){
		
		$v_email = auth()->guard('web')->user()->v_email;
        $name="";
        if(isset(auth()->guard('web')->user()->v_fname) && auth()->guard('web')->user()->v_fname!=""){
            $name = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname) && auth()->guard('web')->user()->v_lname!=""){
            $name .= ' '.auth()->guard('web')->user()->v_lname;
        }
        $data=array(
            'USER_FULL_NAME'=>$name,
        );

        $mailcontent = EmailtemplateHelper::AccountDeleteNotification($data);
        $subject = "SKILLBOX Account Cancelled Successfully";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bb6f9f676fbae1787269862");
        $mailids=array(
            $name=>$v_email,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }

   	public function downgradeaccountnotification(){
		
		$v_email = auth()->guard('web')->user()->v_email;
        $name = "";
        if(isset(auth()->guard('web')->user()->v_fname) && auth()->guard('web')->user()->v_fname!=""){
            $name = auth()->guard('web')->user()->v_fname;
        }
        if(isset(auth()->guard('web')->user()->v_lname) && auth()->guard('web')->user()->v_lname!=""){
            $name .= ' '.auth()->guard('web')->user()->v_lname;
        }

        $data=array(
            'USER_FULL_NAME'=>$name,
        );

        $mailcontent = EmailtemplateHelper::AccountDownradeNotification($data);
        $subject = "Your SKILLBOX account downgraded to Basic";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bc0372776fbae163e2fb4b2");
        $mailids=array(
            $name=>$v_email,
        );

        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
    }




	

}