<?php
namespace App\Http\Controllers\Frontend\Account;
use Request, Hash, Lang,Validator,Auth,Session,PDF,App;
use App\Http\Controllers\Controller;
use App\Models\Categories as CategoriesModel;
use App\Models\Users\Users as UsersModel;
use App\Helpers\General as GeneralHelper;
use App\Models\Users\Order as OrderModel;

class Invoices extends Controller {

	protected $section;
	
	public function __construct(){
		$this->section = Lang::get('section.Invoices');
	}

	public function index() {

		if(!auth()->guard('web')->check()) {
			return redirect('/');		
		}

		$data = Request::all();
		
		$userdata = auth()->guard('web')->user();
		$query = OrderModel::query();
		$query = $query->where('i_user_id',$userdata->id);
		$query = $query->where('e_transaction_type','userplan');
		$invoice = $query->orderBy('d_added','DESC')->get();

		$data=array(
			'invoice'=>$invoice,
			'data'=>$data,
		);
		return view('frontend/account/invoice', $data );
	}

	public function downloadInvoice($id=""){ 
			
		$orderdata = OrderModel::find($id);	
		$companyadd = GeneralHelper::getSiteSetting('SITE_ADDRESS');
		$sitename = GeneralHelper::getSiteSetting('SITE_NAME');

		if(count($orderdata) && $orderdata->e_transaction_type == "userplan"){
			
			$data=array();	
			$data['v_name']="";
			$data['v_address']="";
			
			if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_fname) && isset($orderdata->hasUser()->v_lname)){
				$data['v_name']=$orderdata->hasUser()->v_fname.' '.$orderdata->hasUser()->v_lname;
			}

			if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->l_address) && isset($orderdata->hasUser()->l_address)){
				$data['v_address']=$orderdata->hasUser()->l_address;
			}

			if(count($orderdata->hasUser()) && isset($orderdata->hasUser()->v_city) && isset($orderdata->hasUser()->v_city)){
				$data['v_address']= $data['v_address'].' '.$orderdata->hasUser()->v_city;
			}

			$data['d_date'] = $orderdata->d_date;
			$data['orderno'] = $orderdata->i_order_no;
			$data['title'] = ucfirst($orderdata->v_order_detail['v_plan_duration']).' subscription for '.$orderdata->v_order_detail['v_plan_name'];
			$data['qty'] = 1;
			$data['price'] = $orderdata->v_amount;
			$data['subtotal'] = $orderdata->v_amount;
			$data['total'] = $orderdata->v_amount;
			$userdata = UsersModel::find($orderdata->i_user_id);
			
			$_data=array(
				'data'=>$data,
				'companyadd'=>$companyadd,
				'userdata'=>$userdata,
				'sitename'=>$sitename
			);

			$pdf = PDF::loadView('frontend/account/subscription_invoice',$_data);
        	return $pdf->download('invoice.pdf');	

		}


		if(count($orderdata) && $orderdata->e_transaction_type == "course"){

			$userdata = UsersModel::find($orderdata->i_user_id);

			$_data=array(
				'data'=>$orderdata,
				'userdata'=>$userdata
			);

			$pdf = PDF::loadView('frontend/account/courses_invoice',$_data);
        	return $pdf->download('invoice.pdf');	

		}

		if(count($orderdata) && $orderdata->e_transaction_type == "skill"){

			$userdata = UsersModel::find($orderdata->i_user_id);

			$_data=array(
				'data'=>$orderdata,
				'userdata'=>$userdata
			);

			$pdf = PDF::loadView('frontend/account/skill_invoice',$_data);
        	return $pdf->download('invoice.pdf');	

		}


		dd($id); 

	}


	

}