<?php
namespace App\Helpers;

use Request, Validator, Session, DB, Auth,Hash, Carbon\Carbon, Crypt;
use App\Models\Sitesettings as SiteSettingsModel;
use App\Models\Settings\HeaderImages as HeaderImagesModel;
use App\Models\Plan as PlanModel;

use App\Models\EmailTemplate as EmailTemplateModel;
use App\Models\EmailTemplateFooter as EmailTemplateFooter;
use App\Models\EmailTemplateHeader as EmailTemplateHeader;
use App\Models\Emailsend as Emailsend;


use Illuminate\Database\Eloquent\Collection;
use PHPMailerAutoload;
use PHPMailer;


class Emailtemplate {

	/**
	 * get site settings based on key
	 * @param  [string]				$key 	[name of the key]
	 * @return [string|boolean] 	value of key
	*/

	public static function getSiteSetting( $key ) {
		if( isset($key) && $key ) {
			$return = SiteSettingsModel::where('v_key',trim($key))->value('v_value');
			return $return;
		}
		return false;
	}

	public static function mailFromData() {

		$fromdata=array(
			'host'=>self::getSiteSetting("SMTP_HOST"),
			'protocol'=>self::getSiteSetting("SMTP_PROTOCOL"),
			'port'=>self::getSiteSetting("SMTP_PORT"),
			'username'=>self::getSiteSetting("SMTP_USERNAME"),
			'password'=>self::getSiteSetting("SMTP_PASSWORD"),
			'from_email'=>self::getSiteSetting("SMTP_FROM_EMAIL"),
			'from_name'=>self::getSiteSetting("SMTP_FROM_NAME"),
		);
		return $fromdata;
	}


	public static function MailSendGeneral($subject="" , $message="" , $mailids = array() , $attachments = array() ) {
		
		$mailidsdata=array();

		if(count($mailids)){
			foreach ($mailids as $key => $value) {
				$name = str_replace(".", "", $key);
				$name = str_replace("/", "", $name);
				$mailidsdata[$name]=$value;		
			}
		}
	
		$insert=array(
			'subject'=>$subject,
			'message'=>$message,
			'mailids'=>$mailidsdata,
			'date'=>date("Y-m-d H:i:s"),
		);

		if(count($attachments)){
			$insert['attachments']=$attachments;
		}
		
		Emailsend::create($insert);
		return 1;

		// $fromData = self::mailFromData();
		// $replyToMail = $fromData['username'];
		// $replyToName = 'Skillbox Admin';
		
		// $mail = new PHPMailer;
		// $mail->isSMTP();
		// $mail->CharSet    = "utf-8";
		// $mail->Host       = $fromData['host'];
		// $mail->SMTPAuth   = true;
		// $mail->SMTPSecure = $fromData['protocol'];
		// $mail->Port       = $fromData['port'];
		// $mail->Username   = $fromData['username'];
		// $mail->Password   = $fromData['password'];
		// $mail->setFrom($fromData['from_email'] , $fromData['from_name']);

		// $mail->Subject = $subject;
		// $mail->MsgHTML($message);
		// if(count($attachments)){
		// 	foreach ($attachments as $key => $value) {
		// 		$mail->addAttachment($value, $key);		
		// 	}
		// }
		// if(count($mailids)){
		// 	foreach ($mailids as $key => $value) {
		// 		$mail->addAddress($value);
		// 	}
		// }
		// $mail->isHTML(true);
		// $a = $mail->send();
		
		// return $a;
	}

	public static function MailSendGeneralFinal($fromData=array(),$subject="" , $message="" , $mailids = array() , $attachments = array() ) {

		//$fromData = self::mailFromData();
		
		$replyToMail = $fromData['username'];
		$replyToName = 'Skillbox Admin';
		
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->CharSet    = "utf-8";
		$mail->Host       = $fromData['host'];
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = $fromData['protocol'];
		$mail->Port       = $fromData['port'];
		$mail->Username   = $fromData['username'];
		$mail->Password   = $fromData['password'];
		$mail->setFrom($fromData['from_email'] , $fromData['from_name']);

		$mail->Subject = $subject;
		$mail->MsgHTML($message);
		if(count($attachments)){
			foreach ($attachments as $key => $value) {
				$mail->addAttachment($value, $key);		
			}
		}
		if(count($mailids)){
			foreach ($mailids as $key => $value) {
				$mail->addAddress($value);
			}
		}
		$mail->isHTML(true);
		$a = $mail->send();
		
		return $a;
	}
	
	public static function EmailTemplateHeaderDefine(){

		$headerstr="";
		$headerstr.='<table style="width: 100%; max-width: 500px; margin: 0px auto;">';
		$headerstr.='<tbody>';
		$headerstr.='<tr>';
		$headerstr.='<td>';
		$headerstr.='<table>';
		$headerstr.='<tbody>';
		$headerstr.='<tr><td>';
		$headerstr.='<img src="http://192.168.0.200/projects/skillbox/public/Assets/ckeditor/plugins/ckfinder/core/connector/php/images/logo.png" style="margin-left: -13px;" /></td>';
		$headerstr.='</tr>';
		$headerstr.='</tbody>';
		$headerstr.='</table>';
		$headerstr.='<hr style="border: 3px solid #e8128c;" /></td>';
		$headerstr.='</tr>';
		$headerstr.='</tbody>';
		$headerstr.='</table>';
		return $headerstr;
	}

	public static function EmailTemplateFooterDefine(){

		$footerstr="";
		$footerstr.='<div style="width: 100%; max-width: 500px; margin: 0px auto;">';
		$footerstr.='<hr style="border: 3px solid #e8128c;" />';
		$footerstr.='<table style="margin-top: 30px; width: 100%;">';
		$footerstr.='<tbody>';
		$footerstr.='<tr>';
		$footerstr.='<td align="center"><a href="{_unsubscribe_link}">Unsubscribe</a></td>';
		$footerstr.='</tr>';
		$footerstr.='</tbody>';
		$footerstr.='</table>';
		$footerstr.='<hr style="border: 3px solid #e8128c;" />';
		$footerstr.='<table style="margin-top: 30px; width: 100%;">';
		$footerstr.='<tbody>';
		$footerstr.='<tr><td>';
		$footerstr.='<img src="http://192.168.0.200/projects/skillbox/public/Assets/ckeditor/plugins/ckfinder/core/connector/php/images/logo.png" style="width: 100%; max-width: 333.333333333333px; margin-left: -12px;" /></td>';
		$footerstr.='<td style="font-size: 9px; text-align: right; line-height: 12px; width: 25%;"><span>Skillbox</span><br />';
		$footerstr.='<span>PO BOX 7153 </span><br />';
		$footerstr.='<span>Lichfield </span><br />';
		$footerstr.='<span>WS14 4JW </span><br />';
		$footerstr.='<span>Tel: <a href="#" style="color: #0068A5;text-decoration: underline;">12345 528964 </a></span></td>';
		$footerstr.='</tr>';
		$footerstr.='</tbody>';
		$footerstr.='</table>';
		$footerstr.='</div>';
		$footerstr.='<div style="width: 100%; max-width: 500px; margin: 0px auto;">';
		$footerstr.='<table style="margin-top: 9px;" width="100%">';
		$footerstr.='<tbody>';
		$footerstr.='<tr>';
		$footerstr.='</tr>';
		$footerstr.='</tbody>';
		$footerstr.='</table>';
		$footerstr.='</div>';

		return $footerstr;
	}

	public static function EmailTemplateSubject($id = ""){
		$mailbody = EmailTemplateModel::find($id);
		if(!count($mailbody)){
			return 0;
		}
		if(isset($mailbody->v_subject)){
			return $mailbody->v_subject;		
		}
		return 0;
	}
	
	public static function EmailTemplateFull($id = ""){

		$mailbody = EmailTemplateModel::find($id);

		if(!count($mailbody)){
			return 0;
		}
		
		$mailtemplate="";

		if(isset($mailbody->i_header_id) && $mailbody->i_header_id!=""){

			$templateHeader = EmailTemplateHeader::find($mailbody->i_header_id);
			
			if(count($templateHeader)){
				$mailtemplate .= $templateHeader->l_description;
			}else{
				$mailtemplate .= self::EmailTemplateHeaderDefine();
			}
			
		}else{
			$mailtemplate .= self::EmailTemplateHeaderDefine();
		}

		if(isset($mailbody->l_body) && $mailbody->l_body!=""){
			$mailtemplate .= $mailbody->l_body;
		}

		if(isset($mailbody->i_footer_id) && $mailbody->i_footer_id!=""){
			
			$templateFooter = EmailTemplateFooter::find($mailbody->i_footer_id);
			
			if(count($templateFooter)){
				$mailtemplate .= $templateFooter->l_description;
			}else{
				$mailtemplate .= self::EmailTemplateFooterDefine();
			}
			
		}else{
			$mailtemplate .= self::EmailTemplateFooterDefine();
		}

		return $mailtemplate;
	}

	public static function ForgotPassword($data=array()){

		$mailbody = self::EmailTemplateFull('5a60a0f1d3e812bc4b3c986a');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['name'],
			'{PASSWORD_RESET_LINK}' => $data['passwordreseturl'],
		);

		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>You have requested a password reset, please follow the link below to reset your password.<br><br>';
			$mailbody .= '<a href="'.$data['passwordreseturl'].'">Click hear to reset your password.</a></p>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}
	
	public static function OrderPlacedSellerNotification($data=array()){

		$mailbody = self::EmailTemplateFull('5b575807516b460db00053ed');

		$keys = array(
			
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{ORDER_NUMBER}' => $data['ORDER_NUMBER'],
			'{ORDER_DATA}' => $data['ORDER_MESSAGE'],
			'{SELLER_ORDER_LINK}' => $data['SELLER_ORDER_LINK'],

		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Order Successfully Placed<br><br>';
			$mailbody .= 'We are Pleased to confirm your order no '.$data['ORDER_NUMBER'].'<br>';
			$mailbody .= 'Hello,'.$data['ORDER_DATA'].'<br><br>';
			$mailbody .= '<a href="'.$data['SELLER_ORDER_LINK'].'">Click hear </a> to view your order.</p>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function KycComplete($data=array()){

		$mailbody = self::EmailTemplateFull('5c0bc2f176fbae10c00de3f3');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your kyc document has been approved.';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function KycRefused($data=array()){

		$mailbody = self::EmailTemplateFull('5c0bc34876fbae10ee57c122');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{REFUSED_REASON}'  => $data['REFUSED_REASON'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your kyc document has been refused.';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function SkillOrderPlacedBuyerNotification($data=array()){

		$mailbody = self::EmailTemplateFull('5bb5f25176fbae617d680c42');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{ORDER_NUMBER}' => $data['ORDER_NUMBER'],
			'{ORDER_DATA}' => $data['ORDER_MESSAGE'],
			'{BUYER_ORDER_LINK}' => $data['BUYER_ORDER_LINK'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Order Successfully Placed<br><br>';
			$mailbody .= 'We are Pleased to confirm your order no '.$data['ORDER_NUMBER'].'<br>';
			$mailbody .= 'Hello,'.$data['ORDER_DATA'].'<br><br>';
			$mailbody .= '<a href="'.$data['BUYER_ORDER_LINK'].'">Click hear </a> to view your order.</p>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function NewJobNotificationToSeller($data=array()){

		$mailbody = self::EmailTemplateFull('5bbda7e876fbae29842448c2');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{BUYER_NAME}' => $data['BUYER_NAME'],
			'{JOB_DETAIL}' => $data['JOB_DETAIL'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= $data['BUYER_NAME'].'posted job on skillbox.please find below detail.<br>';
			$mailbody .= $data['JOB_DETAIL'].'<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function NewSkillNotification($data=array()){

		$mailbody = self::EmailTemplateFull('5bd1c47b76fbae5a3a238755');
		$keys = array(
			'{CATEGORY}' => $data['CATEGORY'],
			'{SKILL}' => $data['SKILL'],
			'{SKILL_URL}' => $data['SKILL_URL'],
			'{UNSUBSCRIBED_URL}' => $data['UNSUBSCRIBED_URL'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,<br>';
			$mailbody .= 'New skill posted on skillbox please find below detail.<br>';
			$mailbody .= 'Category : '.$data['CATEGORY'].'<br>';
			$mailbody .= 'Skill :'.$data['SKILL'].'<br>';
			$mailbody .= '<a href="'.$data['SKILL_URL'].'">Click here </a> to view skill.<br>';
			$mailbody .= '<a href="'.$data['UNSUBSCRIBED_URL'].'">Click here </a> to Unsubscribed Notify.<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;

	}



	
	public static function BasicEmailTemplate($data=array()){

		$mailbody = self::EmailTemplateFull('5bb5cec376fbae2bb2317f44');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
		);
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your account has been successfully registered for Basic Plan..<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function StandardEmailTemplate($data=array()){

		$mailbody = self::EmailTemplateFull('5bb5cefe76fbae2bb2317f45');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
		);
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your account has been successfully registered for Standard Plan.<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function PremiuemEmailTemplate($data=array()){

		$mailbody = self::EmailTemplateFull('5bb5cf2a76fbae2bb2317f46');

		$keys = array(
			'{USER_FULL_NAME}'=>$data['USER_FULL_NAME'],
		);
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your account has been successfully registered for Premium Plan.<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function TrialStandardPlanEmailTemplate($data=array()){

		$mailbody = self::EmailTemplateFull('5bc0774476fbae2d0a4354f5');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{MONTH}'  => $data['MONTH'],
		);
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your account has been successfully registered for Standard Plan with '.$data['MONTH'].' month free.';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function StandardRecuringEmailTemplate($data=array()){

		$mailbody = self::EmailTemplateFull('5bc1950776fbae0d6850a973');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
		);
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your have been charged for Standard Plan.<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function PremiuemRecuringEmailTemplate($data=array()){

		$mailbody = self::EmailTemplateFull('5bc1946076fbae0c8c23a164');
		$keys = array(
			'{USER_FULL_NAME}'=>$data['USER_FULL_NAME'],
		);
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your have been charged for Premium Plan..<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function supportReplaytoUser($data=array()){

		$mailbody = self::EmailTemplateFull('5bbdf8ba76fbae01fc5a6552');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{SUPPORT_QUERY}' => $data['SUPPORT_QUERY'],
			'{SUPPORT_QUERY_REPLAY}' => $data['SUPPORT_QUERY_REPLAY'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Please find reply for your support ticket.';
			$mailbody .= 'Support query '.$data['SUPPORT_QUERY'];
			$mailbody .= $data['SUPPORT_QUERY_REPLAY'];
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function AssignSupportTicket($data=array()){

		$mailbody = self::EmailTemplateFull('5bd6c2a876fbae537e4660d2');
		$keys = array(
			'{USER_NAME}'  => $data['USER_NAME'],
			'{SUPPORT_QUERY}' => $data['SUPPORT_QUERY'],
			'{SUPPORT_CATEGORY}' => $data['SUPPORT_CATEGORY'],
			'{SUPPORT_QUESTIONS}' => $data['SUPPORT_QUESTIONS'],
			'{SUPPORT_LINK}' => $data['SUPPORT_LINK'],
		);
		$sitekeys=array();
		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_NAME'].'<br><br>';
			$mailbody .= 'Please find below support ticket.';
			$mailbody .= 'Support query '.$data['SUPPORT_QUERY'];
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function CloseSupportTicket($data=array()){

		$mailbody = self::EmailTemplateFull('5bd6c70376fbae530721e632');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{SUPPORT_QUERY}' => $data['SUPPORT_QUERY'],
		);

		$sitekeys=array();
		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'SKILLBOX admin close the ticket.please find below detail.';
			$mailbody .= 'Support query '.$data['SUPPORT_QUERY'];
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function OpenSupportTicket($data=array()){

		$mailbody = self::EmailTemplateFull('5bd6c87076fbae5195337fc3');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{SUPPORT_QUERY}' => $data['SUPPORT_QUERY'],
		);

		$sitekeys=array();
		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'SKILLBOX admin reopen the ticket.please find below detail.';
			$mailbody .= 'Support query '.$data['SUPPORT_QUERY'];
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

		
	public static function AccountDeleteNotification($data=array()){

		$mailbody = self::EmailTemplateFull('5bb6f9f676fbae1787269862');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
		);
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hi,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your account has been cancelled successfully. All your details has been deleted from our system and any monthly or annual subscription you may have is also cancelled..<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}



	public static function AccountDownradeNotification($data=array()){

		$mailbody = self::EmailTemplateFull('5bc0372776fbae163e2fb4b2');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
		);
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hi,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your account is successfully downgraded to our Basic (Free) plan and your monthly subscription is also cancelled..<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}


	public static function CourseAnnouncementCommentNotifyToSeller($data=array()){

		$mailbody = self::EmailTemplateFull('5bbb38c476fbae654c11d283');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{COURSE_TITLE}'  => $data['COURSE_TITLE'],
			'{BUYER_NAME}'  => $data['BUYER_NAME'],
			'{COURSE_ANNOUNCEMENT_COMMENT}'  => $data['COURSE_ANNOUNCEMENT_COMMENT'],
			'{COURSE_SELLER_URL}'=>$data['COURSE_SELLER_URL'],
		);
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hi,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= $data['BUYER_NAME'].'commented on your announcement..<br>';
			$mailbody .= $data['COURSE_ANNOUNCEMENT_COMMENT'];
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	//Subject Dynamic
	public static function OrderRequirementsSeller($data=array()){

		$mailbody = self::EmailTemplateFull('5b5768b8516b460db00053ef');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{ORDER_NO}' => $data['ORDER_NO'],
			'{ORDER_TITLE}' => $data['ORDER_TITLE'],
			'{ORDER_MESSAGE}' => $data['ORDER_MESSAGE'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Buyer has posted order requirements.please find below detail.<br><br>';
			$mailbody .= 'Order No :-'.$data['ORDER_NO'].'<br><br>';
			$mailbody .= 'Order Title :-'.$data['ORDER_TITLE'].'<br><br>';
			$mailbody .= 'Requirements :-'.$data['ORDER_MESSAGE'].'<br><br>';	
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	

	public static function OrderRequirementsBuyer($data=array()){

		$mailbody = self::EmailTemplateFull('5b581eff1b544d14d61d5fa2');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{ORDER_NO}' => $data['ORDER_NO'],
			'{ORDER_TITLE}' => $data['ORDER_TITLE'],
			'{ORDER_MESSAGE}' => $data['ORDER_MESSAGE'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Buyer has posted order requirements.please find below detail.<br><br>';
			$mailbody .= 'Order No :-'.$data['ORDER_NO'].'<br><br>';
			$mailbody .= 'Order Title :-'.$data['ORDER_TITLE'].'<br><br>';
			$mailbody .= 'Requirements :-'.$data['ORDER_MESSAGE'].'<br><br>';	
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function OrderCancelRequestToSeller($data=array()){

		$mailbody = self::EmailTemplateFull('5b5822c51b544d14d97708d3');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{ORDER_NO}' => $data['ORDER_NO'],
			'{ORDER_TITLE}' => $data['ORDER_TITLE'],
			'{SUBJECT}' => $data['SUBJECT'],
			'{MESSAGE}' => $data['MESSAGE'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Buyer request for cancel oder.please find below detail.<br><br>';
			$mailbody .= 'Order No :-'.$data['ORDER_NO'].'<br><br>';
			$mailbody .= 'Order Title :-'.$data['ORDER_TITLE'].'<br><br>';
			$mailbody .= 'Subject :-'.$data['SUBJECT'].'<br><br>';
			$mailbody .= 'Message :-'.$data['MESSAGE'].'<br><br>';	
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function OrderCancelRequestReplyToBuyer($data=array()){

		$mailbody = self::EmailTemplateFull('5b5851ac1b544d58f07925b3');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{ORDER_NO}' => $data['ORDER_NO'],
			'{ORDER_TITLE}' => $data['ORDER_TITLE'],
			'{MESSAGE}' => $data['MESSAGE'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Seller reply for your order cancel request .please find below detail.<br><br>';
			$mailbody .= 'Order No :-'.$data['ORDER_NO'].'<br><br>';
			$mailbody .= 'Order Title :-'.$data['ORDER_TITLE'].'<br><br>';
			$mailbody .= 'Message :-'.$data['MESSAGE'].'<br><br>';	
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function JobExpireNotification($data=array()){

		$mailbody = self::EmailTemplateFull('5b585c111b544d5d44495722');
		
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{JOB_TITLE}' => $data['JOB_TITLE'],
			'{JOB_EXPIRE_DATE}' => $data['JOB_EXPIRE_DATE'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your posted job expire in two days.please find below detail.<br><br>';
			$mailbody .= 'Job Title :-'.$data['JOB_TITLE'].'<br><br>';
			$mailbody .= 'Job expire date :-'.$data['JOB_EXPIRE_DATE'].'<br><br>';
			$mailbody .= self::EmailTemplateFooterDefine();
			
		}
		return $mailbody;
	}

	public static function JobExpiredNotification($data=array()){

		$mailbody = self::EmailTemplateFull('5bd85241dabab22d45465a02');
		
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{JOB_TITLE}' => $data['JOB_TITLE'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Your posted job expired.please find below detail.<br><br>';
			$mailbody .= 'Job Title :-'.$data['JOB_TITLE'].'<br><br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function OrderCanceledToBuyer($data=array()){

		$mailbody = self::EmailTemplateFull('5b5827c71b544d15094fbab2');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{ORDER_NO}' => $data['ORDER_NO'],
			'{ORDER_TITLE}' => $data['ORDER_TITLE'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>';
			$mailbody .= 'Seller canceled your oder.please find below detail.<br><br>';
			$mailbody .= 'Order No :-'.$data['ORDER_NO'].'<br><br>';
			$mailbody .= 'Order Title :-'.$data['ORDER_TITLE'].'<br><br>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function JobpostLive($data=array()){

		$mailbody = self::EmailTemplateFull('5b55fe15516b462cb0001aaa');	
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{JOB_PREVIEW_LINK}'  => $data['JOB_PREVIEW_LINK'],
		);

		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>Your job is posted successfully.';
			$mailbody .= '<a href="'.$data['JOB_PREVIEW_LINK'].'">Click here</a> to preview your job post.</p>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function AppliedJobpost($data=array()){

		$mailbody = self::EmailTemplateFull('5b560ad1516b462ef00036a4');	
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{JOB_NAME}'  => $data['JOB_NAME'],
			'{JOB_RESPONSE_LINK}'  => $data['JOB_RESPONSE_LINK'],
		);
		$sitekeys=array();

		if($mailbody){
			
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>Seller has been applied for.'.$data['JOB_NAME'];
			$mailbody .= '<a href="'.$data['JOB_RESPONSE_LINK'].'">Click here</a> to view your job post reponse..</p>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function DeliverOrder($data=array()){

		$mailbody = self::EmailTemplateFull('5b561895516b462ef00036ad');	
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{ORDER_NO}'  => $data['ORDER_NO'],
			'{ORDER_TITLE}'  => $data['ORDER_TITLE'],
			'{SELLER_NAME}'  => $data['SELLER_NAME'],
		);
		$sitekeys=array();

		if($mailbody){
			
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>Seller deliver order please find below detail.';
			$mailbody .= 'Order no :- '.$data['ORDER_NO'].'<br>';
			$mailbody .= 'Order Title :- '.$data['ORDER_TITLE'].'<br>';
			$mailbody .= 'Seller name :- '.$data['SELLER_NAME'].'<br>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function DeliverOrderBuyerNotSatisfiedToSeller($data=array()){

		$mailbody = self::EmailTemplateFull('5bb6091a76fbae571722f446');	
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{ORDER_NO}'  => $data['ORDER_NO'],
			'{ORDER_TITLE}'  => $data['ORDER_TITLE'],
			'{BUYER_NAME}'  => $data['BUYER_NAME'],
			'{SELLER_ORDER_LINK}'  => $data['SELLER_ORDER_LINK'],
		);
		$sitekeys=array();

		if($mailbody){
			
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= $data['BUYER_NAME'].'not satisfied with order delevery.<br>';
			$mailbody .= 'Order no :- '.$data['ORDER_NO'].'<br>';
			$mailbody .= 'Order Title :- '.$data['ORDER_TITLE'].'<br>';
			$mailbody .= '<a href="'.$data['SELLER_NAME'].'">Click here </a>to view your order.';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function SignupSuccessfully($data=array()){

		$mailbody = self::EmailTemplateFull('5b28f90676fbae5e650edc12');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['name'],
			'{PLAN_NAME}'=>$data['plan_name']
		);
		$sitekeys=array();
		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>You have successfully signed up with '.$data['plan_name'].' in skillbox.<br><br>';
			$mailbody .= 'Please find below invoice.';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function ResetPasswordAdmin($data=array()){

		$mailbody = self::EmailTemplateFull('5b17abfa76fbae34dd205c03');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['name'],
			'{PASSWORD}' => $data['password'],
		);
		
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>Your password has been reset.please find new password below.<br><br>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function Passwordconfirmation($data=array()){

		$mailbody = self::EmailTemplateFull('5b17826c76fbae1c5e740b02');

		$keys = array(
			'{USER_FULL_NAME}'  => $data['name'],
			'{LOGIN_URL}' => $data['loginurl'],
		);

		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>Your password has been succesfully changed.<br><br>';
			$mailbody .= '<a href="'.$data['loginurl'].'">Click hear to login.</a></p>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function ApplyJob($data=array()) {

		$emailTemplate = EmailTemplateModel::find('5b093cfb76fbae5f4402b223');
		$string="";

		if(count($emailTemplate)){

			$keys = array(
				'{USER_JOB_TITLE}'  => $data['jobTitle'],
				'{USER_FULL_NAME}'  => $data['name'],
				'{USER_EMAIL}'  => $data['email'],
				'{USER_CONTACT}'  => $data['contact'],
				'{USER_MESSAGE}'  => $data['message'],
			);

			//$sitekeys=self::SiteSettingKey();
			$sitekeys=array();
			$emailTemplateHeader = EmailTemplateHeader::find('5a609fbfd3e812bc4b3c9869');
			$emailTemplateFooter = EmailTemplateFooter::find('5a60a044d3e81234473c986a');
			$string = "";

			if(count($emailTemplateHeader) && isset($emailTemplateHeader->l_description)){
				$string = $emailTemplateHeader->l_description;
			}

			if(isset($emailTemplate->l_body)){
				$string .= $emailTemplate->l_body;
			}

			if(count($emailTemplateFooter) && isset($emailTemplateFooter->l_description)){
				$string .= $emailTemplateFooter->l_description;
			}					

			$path = public_path('siteimage/');
			
			foreach ($keys as $k => $v) {
				$string = str_replace( $k, $v, $string);
			}
			foreach ($sitekeys as $sk => $sv) {
				$string = str_replace( $sk, $sv, $string );
			}

		}else{

			$string='Hello,<br><br>Please find below reset password.<br><br>';
			$string=$data['password'];
			$string='<br><br>Thanks & regards<br><br>';
			$string='<br><br>Skillbox Support Team.<br><br>';

		}
		return $string;
	}



	public static function ConfirmUserAccount($data=array()){

		$mailbody = self::EmailTemplateFull('5ad81f45d3e812c30b3c9869');

		$keys = array(
			'{USER_FULL_NAME}'  			=> $data['name'],
			'{CONFIRM_YOUR_ACCOUNT_LINK}' 	=> $data['confirm_account_link'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>Please find below confirm your account link.<br><br>';
			$mailbody .= $data['confirm_account_link'];
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function SupportTicketAdmin($data=array()){

		$mailbody = self::EmailTemplateFull('5bd80f8076fbae499a6a4f12');

		$keys = array(
			'{SUPPORT_QUERY}' 	=> $data['SUPPORT_QUERY'],
			'{SUPPORT_CATEGORY}' => $data['SUPPORT_CATEGORY'],
			'{SUPPORT_QUESTIONS}'=> $data['SUPPORT_QUESTIONS'],
		);
		$sitekeys=array();
		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,<br><br>Please find below support ticket.<br><br>';
			$mailbody .= 'Support query'.$data['SUPPORT_QUERY'].'<br>';
			$mailbody .= 'Category'.$data['SUPPORT_QUERY'].'<br>';
			$mailbody .= 'Question'.$data['SUPPORT_QUERY'].'<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function OrderCancelled($data=array()){

		$mailbody = self::EmailTemplateFull('5b0e7dbf76fbae5b834b02e3');

		$keys = array(
			'{USER_FULL_NAME}'  			=> $data['name']
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>Order cancelled.<br><br>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function AccountVerified($data=array()){

		$mailbody = self::EmailTemplateFull('5b0e749376fbae2feb21ecb2');
		$keys = array(
			'{USER_FULL_NAME}' => $data['name'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>Your account has been successfully verified.<br>';
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function OrderconformationSeller($data=array()){

		$mailbody = self::EmailTemplateFull('5b0e89fc76fbae6964350442');
		$keys = array(
			'{USER_FULL_NAME}' => $data['name'],
			'{COURSE_ORDER_DETAIL}' => $data['COURSE_ORDER_DETAIL'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>Order placed.please find below information.<br>';
			$mailbody .= $data['COURSE_ORDER_DETAIL'];
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function OrderconformationCourseBuyer($data=array()){

		$mailbody = self::EmailTemplateFull('5bd691d876fbae16740f8642');
		$keys = array(
			'{USER_FULL_NAME}' => $data['name'],
			'{COURSE_ORDER_DETAIL}' => $data['COURSE_ORDER_DETAIL'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>Order placed.please find below information.<br>';
			$mailbody .= $data['COURSE_ORDER_DETAIL'];
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function WithdrawMoney($data=array()){

		$mailbody = self::EmailTemplateFull('5bd7ffd876fbae1e590bdf42');
		$keys = array(
			'{USER_FULL_NAME}' => $data['name'],
			'{MESSAGE}' => $data['MESSAGE'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>';
			$mailbody .= $data['MESSAGE'];
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function sendMessageSeller($data=array()){

		$mailbody = self::EmailTemplateFull('5bb5ba4276fbae17da5ac234');
		$keys = array(
			'{USER_FULL_NAME}'=>$data['USER_FULL_NAME'],
            '{BUYER_NAME}'=>$data['BUYER_NAME'],
            '{MESSAGE}'=>$data['MESSAGE'],
       );
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>Message from '.$data['BUYER_NAME'].'.<br>';
			$mailbody .= $data['MESSAGE'];
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function sendMessage($data=array()){

		$mailbody = self::EmailTemplateFull('5bc1990376fbae0d22218954');
		$keys = array(
			'{USER_FULL_NAME}'=>$data['USER_FULL_NAME'],
            '{FROM_NAME}'=>$data['FROM_NAME'],
            '{MESSAGE}'=>$data['MESSAGE'],
       );
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>Message from '.$data['FROM_NAME'].'.<br>';
			$mailbody .= $data['MESSAGE'];
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function sendMessageBuyer($data=array()){

		$mailbody = self::EmailTemplateFull('5bb5bdfd76fbae2bb2317f43');
		$keys = array(
			'{USER_FULL_NAME}'=>$data['USER_FULL_NAME'],
            '{SELLER_NAME}'=>$data['SELLER_NAME'],
            '{MESSAGE}'=>$data['MESSAGE'],
       );
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>Message from '.$data['SELLER_NAME'].'.<br>';
			$mailbody .= $data['MESSAGE'];
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function ListingreportAdmin($data=array()){

		$mailbody = self::EmailTemplateFull('5bb1df6176fbae0ffb648742');
		$keys = array(
			'{LISTING_DATA}' => $data['LISTING_DATA'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,<br><br>';
			$mailbody .= $data['LISTING_DATA'];
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function CourseAnnouncementBuyer($data=array()){

		$mailbody = self::EmailTemplateFull('5b0e8ace76fbae69ad6587c2');
		$keys = array(
			'{USER_FULL_NAME}' => $data['name'],
			'{COURSE_ANNOUNCEMENT}' => $data['COURSE_ANNOUNCEMENT'],
			'{COURSE_BUYER_URL}' => $data['COURSE_BUYER_URL'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['name'].'<br><br>Order placed.please find below information.<br>';
			$mailbody .= $data['COURSE_ANNOUNCEMENT'];
			$mailbody .= self::EmailTemplateFooterDefine();

		}
		return $mailbody;
	}

	public static function CourseQAtoSeller($data=array()){

		$mailbody = self::EmailTemplateFull('5b1a1d7e76fbae10da3ec042');
		$keys = array(
			'{SELLER_NAME}' => $data['sellername'],
			'{BUYER_NAME}' => $data['buyername'],
			'{COURSE_NAME}' => $data['coursename'],
			'{COURSE_QUESTION_TITLE}' => $data['v_title'],
			'{COURSE_QUESTION}' => $data['l_questions'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['sellername'].'<br><br>'.$data['buyername'].' asking questions about '.$data['coursename'].' course.<br>';
			$mailbody .= 'Please find following detail.<br><br>';
			$mailbody .= 'Title :-'.$data['v_title'].'<br><br>';
			$mailbody .= 'Question :-'.$data['l_questions'].'<br><br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function CourseQAtoBuyer($data=array()){

		$mailbody = self::EmailTemplateFull('5b1a20a876fbae0b95669873');
		$keys = array(
			'{SELLER_NAME}' => $data['sellername'],
			'{BUYER_NAME}' => $data['buyername'],
			'{COURSE_NAME}' => $data['coursename'],
			'{COURSE_QUESTION_TITLE}' => $data['v_title'],
			'{COURSE_QUESTION}' => $data['l_questions'],
			'{COURSE_ANSWER}' => $data['l_answers'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['buyername'].'<br><br>'.$data['sellername'].' posted  answer about questions of course.<br>';
			$mailbody .= 'Please find following detail.<br><br>';
			$mailbody .= 'Title :-'.$data['v_title'].'<br><br>';
			$mailbody .= 'Question :-'.$data['l_questions'].'<br><br>';
			$mailbody .= 'Answer :-'.$data['l_answers'].'<br><br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function CourseReviewSeller($data=array()){

		$mailbody = self::EmailTemplateFull('5b1a211376fbae19586f56b2');
		$keys = array(
			'{SELLER_NAME}' => $data['sellername'],
			'{BUYER_NAME}' => $data['buyername'],
			'{COURSE_NAME}' => $data['coursename'],
			'{COURSE_REVIEW}' => $data['review_string'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}

		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['sellername'].'<br><br>'.$data['buyername'].'  left review on your course.<br>';
			$mailbody .= 'Please find following detail.<br><br>';
			$mailbody .= $data['review_string'].'';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function SkillReviewSeller($data=array()){

		$mailbody = self::EmailTemplateFull('5bbc7c0576fbae2bcb301e2d');
		$keys = array(
			'{SELLER_NAME}' => $data['SELLER_NAME'],
			'{BUYER_NAME}' => $data['BUYER_NAME'],
			'{SKILL_REVIEW}' => $data['SKILL_REVIEW'],
			'{ORDER_NO}' => $data['ORDER_NO'],
			'{ORDER_TITLE}' => $data['ORDER_TITLE'],
			'{ORDER_SELLER_LINK}' => $data['ORDER_SELLER_LINK'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['SELLER_NAME'].'<br>';
			$mailbody .= $data['BUYER_NAME.'].'left review on your order.<br>';
			$mailbody .= 'Order No,'.$data['ORDER_NO'].'<br>';
			$mailbody .= 'Order Title,'.$data['ORDER_TITLE'].'<br>';
			$mailbody .= 'Please find following detail.<br><br>';
			$mailbody .= $data['SKILL_REVIEW'].'';
			$mailbody .= '<a href="'.$data['ORDER_SELLER_LINK'].'">click here</a> to view.<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function SkillReviewCommentToBuyer($data=array()){

		$mailbody = self::EmailTemplateFull('5bbc81df76fbae1d7640125a');
		$keys = array(
			'{SELLER_NAME}' => $data['SELLER_NAME'],
			'{BUYER_NAME}' => $data['BUYER_NAME'],
			'{SKILL_REVIEW_COMMENT}' => $data['SKILL_REVIEW_COMMENT'],
			'{ORDER_NO}' => $data['ORDER_NO'],
			'{ORDER_TITLE}' => $data['ORDER_TITLE'],
			'{ORDER_BUYER_LINK}' => $data['ORDER_BUYER_LINK'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['BUYER_NAME'].'<br>';
			$mailbody .= $data['SELLER_NAME.'].'left comment on your review.<br>';
			$mailbody .= 'Order No,'.$data['ORDER_NO'].'<br>';
			$mailbody .= 'Order Title,'.$data['ORDER_TITLE'].'<br>';
			$mailbody .= 'Please find following detail.<br><br>';
			$mailbody .= $data['SKILL_REVIEW_COMMENT'].'';
			$mailbody .= '<a href="'.$data['ORDER_BUYER_LINK'].'">click here</a> to view.<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function OrderExtendDeliveryToBuyer($data=array()){

		$mailbody = self::EmailTemplateFull('5bbc987e76fbae2f04230d95');
		$keys = array(
			'{SELLER_NAME}' => $data['SELLER_NAME'],
			'{BUYER_NAME}' => $data['BUYER_NAME'],
			'{ORDER_NO}' => $data['ORDER_NO'],
			'{ORDER_TITLE}' => $data['ORDER_TITLE'],
			'{ORDER_BUYER_LINK}' => $data['ORDER_BUYER_LINK'],
			'{ORDER_EXTEND_REQUEST}' => $data['ORDER_EXTEND_REQUEST'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['BUYER_NAME'].'<br>';
			$mailbody .= $data['SELLER_NAME.'].' request for extend order delivery date..<br>';
			$mailbody .= 'Order No,'.$data['ORDER_NO'].'<br>';
			$mailbody .= 'Order Title,'.$data['ORDER_TITLE'].'<br>';
			$mailbody .= 'Please find following detail.<br><br>';
			$mailbody .= $data['ORDER_EXTEND_REQUEST'].'';
			$mailbody .= '<a href="'.$data['ORDER_BUYER_LINK'].'">click here</a> to view.<br>';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function CourseReviewReplies($data=array()){

		$mailbody = self::EmailTemplateFull('5b1a216e76fbae0b95669874');
		$keys = array(
			'{SELLER_NAME}' => $data['sellername'],
			'{BUYER_NAME}' => $data['buyername'],
			'{COURSE_NAME}' => $data['coursename'],
			'{COURSE_REVIEW}' => $data['review_string'],
		);
		$sitekeys=array();

		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['buyername'].'<br><br>'.$data['sellername'].'  replay for your review of course..<br>';
			$mailbody .= 'Please find following detail.<br><br>';
			$mailbody .= $data['review_string'].'';
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function LevelEarned($data=array()){

		$mailbody = self::EmailTemplateFull('5bc6dccb76fbae2518329498');
		$keys = array(
			'{USER_NAME}' => $data['USER_NAME'],
			'{LEVEL}' => $data['LEVEL'],
		);
		$sitekeys=array();
		if($mailbody){
			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Dear,'.$data['USER_NAME'].'<br><br>Awesome!<br>';
			$mailbody .= "You've just earned a Level ".$data['LEVEL']." Seller status.<br>";
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}


	public static function CourseWelcomeMessage($data=array()){

		$mailbody = self::EmailTemplateFull('5ba8771f76fbae10255546f3');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{COURSE_MESSAGE}' => $data['COURSE_MESSAGE'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{

			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Course message by seller..<br><br>';
			$mailbody .= $data['COURSE_MESSAGE'].'<br><br>';	
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}

	public static function CourseCongratulationsMessage($data=array()){

		$mailbody = self::EmailTemplateFull('5bc8259076fbae2fb4210ab2');
		$keys = array(
			'{USER_FULL_NAME}'  => $data['USER_FULL_NAME'],
			'{COURSE_MESSAGE}' => $data['COURSE_MESSAGE'],
		);
		$sitekeys=array();

		if($mailbody){

			$path = public_path('siteimage/');
			foreach ($keys as $k => $v) {
				$mailbody = str_replace( $k, $v, $mailbody);
			}
			foreach ($sitekeys as $sk => $sv) {
				$mailbody = str_replace( $sk, $sv, $mailbody );
			}
		}else{
			$mailbody .= self::EmailTemplateHeaderDefine();
			$mailbody .= 'Hello,'.$data['USER_FULL_NAME'].'<br><br>';
			$mailbody .= 'Course message by seller..<br><br>';
			$mailbody .= $data['COURSE_MESSAGE'].'<br><br>';	
			$mailbody .= self::EmailTemplateFooterDefine();
		}
		return $mailbody;
	}



}