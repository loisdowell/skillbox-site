<?php
namespace App\Helpers;

use Request, Validator, Session, DB, Auth,Hash, Carbon\Carbon, Crypt,Storage;
use App\Models\Sitesettings as SiteSettingsModel;
use App\Models\Settings\HeaderImages as HeaderImagesModel;
use App\Models\Plan as PlanModel;
use App\Models\Users\Users as UsersModel;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Support\Support as SupportModel;
use App\Models\Users\Jobs as JobsModel;
use App\Models\Users\Buyerreview as Buyerreview;
use App\Models\Users\Ordernumber;
use App\Models\Users\Sellerreview as SellerreviewModal;
use App\Models\Users\Order as OrderModel;
use App\Models\ManageMetaField as ManageMetaFieldModel;
use App\Models\Courses\CoursesReviews as CoursesReviewsModel;
use App\Models\Courses\Courses as CoursesModel;
use App\Models\Users\AppliedJob as AppliedJobModel;
use App\Models\Users\Sellerprofile as SellerprofileModel;
use App\Models\Levels as LevelsModel;
use App\Helpers\Emailtemplate as EmailtemplateHelper;
use App\Models\Notify as NotifyModel;
use App\Models\Footer as FooterModel;


	
class General {

	/**
	 * get site settings based on key
	 * @param  [string]	$key 	[name of the key]
	 * @return [string|boolean] value of key
	*/

	public static function apiPagination($data = array(),$post_data=array()) {	

		$resdata['current_page'] = 0;	
		$resdata['total_page'] = 0;
		$resdata['data_limit'] = 0;
		
		if(isset($post_data['pagination']) && $post_data['pagination']==1 && count($data)){
			if(isset($post_data['page']) && $post_data['page']!=""){
				$resdata['current_page'] = $post_data['page'];
			}
			$resdata['total_page'] = $data->lastPage();
			if(isset($post_data['data_limit']) && $post_data['data_limit']!=""){
				$resdata['data_limit'] = $post_data['data_limit'];
			}else{
				$resdata['data_limit'] = 20;
			}
		}
		return $resdata;
		
	}
	
	
	public static function getSiteSetting( $key ) {	

		if( isset($key) && $key ) {
			$return = SiteSettingsModel::where('v_key',trim($key))->value('v_value');
			return $return;
		}
		return false;
	}

	public static function footerMenu() {	
		
		$footerdata = FooterModel::where('e_status','active')->get();
		return $footerdata;
	}

	public static function time_elapsed_string($ptime){
    	$etime = time() - $ptime;
		if($etime < 1){
        	return '0 seconds';
    	}

    	$a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    	$a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

		foreach ($a as $secs => $str){
			$d = $etime / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
			}
		}
	}
	public static function generateToken(){	
		$token = str_random(60).'-'.time();
		return $token;
  	}


	public static function FormatTime($timestamp){
	
		$difference = time() - $timestamp;
		$periods = array("second", "minute", "hour", "day", "week", "month", "years");
		$lengths = array("60","60","24","7","4.35","12");
 
		if ($difference >= 0) {
			$ending = "ago";
		}else{
			$difference = -$difference;
			$ending = "to go";
		}
		
		$arr_len = count($lengths);
		for($j = 0; $j < $arr_len && $difference >= $lengths[$j]; $j++){
			$difference /= $lengths[$j];
		}
		$difference = round($difference);
	
		if($difference != 1) {
			$periods[$j].= "s";
		}
		$text = "$difference $periods[$j] $ending";
		if($j > 2){
			
		if($ending == "to go"){
			if($j == 3 && $difference == 1){
				$text = "Tomorrow at ". date("g:i a", $timestamp);
			}else{
				$text = date("F j, Y \a\\t g:i a", $timestamp);
			}
			return $text;
		}
 
		if($j == 3 && $difference == 1){
			$text = "Yesterday at ". date("g:i a", $timestamp);
		}else if($j == 3){
			$text = date("l \a\\t g:i a", $timestamp);
		}else if($j < 6 && !($j == 5 && $difference == 12)){
			$text = date("F j \a\\t g:i a", $timestamp);
		}else{
			$text = date("F j, Y \a\\t g:i a", $timestamp);
		}
	}
	return $text;
}


	public static function TitleSlug($strtitle="") {	
		
		 $table = array(
	            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
	            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
	            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
	            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
	            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
	            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
	            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
	            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-'
	    );	 
   		$stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $strtitle);
   		return strtolower(strtr($strtitle, $table));
    }
    
	public static function countrysortcode(){
		
		$list=array(
			"AD"=>"Andorra",
			"AE"=>"United Arab Emirates",
			"AF"=>"Afghanistan",
			"AG"=>"Antigua & Barbuda",
			"AI"=>"Anguilla",
			"AL"=>"Albania",
			"AM"=>"Armenia",
			"AN"=>"Netherlands Antilles",
			"AO"=>"Angola",
			"AQ"=>"Antarctica",
			"AR"=>"Argentina",
			"AS"=>"American Samoa",
			"AT"=>"Austria",
			"AU"=>"Australia",
			"AW"=>"Aruba",
			"AZ"=>"Azerbaijan",
			"BA"=>"Bosnia and Herzegovina",
			"BB"=>"Barbados",
			"BD"=>"Bangladesh",
			"BE"=>"Belgium",
			"BF"=>"Burkina Faso",
			"BG"=>"Bulgaria",
			"BH"=>"Bahrain",
			"BI"=>"Burundi",
			"BJ"=>"Benin",
			"BM"=>"Bermuda",
			"BN"=>"Brunei Darussalam",
			"BO"=>"Bolivia",
			"BR"=>"Brazil",
			"BS"=>"Bahama",
			"BT"=>"Bhutan",
			"BU"=>"Burma (no longer exists)",
			"BV"=>"Bouvet Island",
			"BW"=>"Botswana",
			"BY"=>"Belarus",
			"BZ"=>"Belize",
			"CA"=>"Canada",
			"CC"=>"Cocos (Keeling) Islands",
			"CF"=>"Central African Republic",
			"CG"=>"Congo",
			"CH"=>"Switzerland",
			"CI"=>"Côte D'ivoire (Ivory Coast)",
			"CK"=>"Cook Iislands",
			"CL"=>"Chile",
			"CM"=>"Cameroon",
			"CN"=>"China",
			"CO"=>"Colombia",
			"CR"=>"Costa Rica",
			"CS"=>"Czechoslovakia (no longer exists)",
			"CU"=>"Cuba",
			"CV"=>"Cape Verde",
			"CX"=>"Christmas Island",
			"CY"=>"Cyprus",
			"CZ"=>"Czech Republic",
			"DD"=>"German Democratic Republic (no longer exists)",
			"DE"=>"Germany",
			"DJ"=>"Djibouti",
			"DK"=>"Denmark",
			"DM"=>"Dominica",
			"DO"=>"Dominican Republic",
			"DZ"=>"Algeria",
			"EC"=>"Ecuador",
			"EE"=>"Estonia",
			"EG"=>"Egypt",
			"EH"=>"Western Sahara",
			"ER"=>"Eritrea",
			"ES"=>"Spain",
			"ET"=>"Ethiopia",
			"FI"=>"Finland",
			"FJ"=>"Fiji",
			"FK"=>"Falkland Islands (Malvinas)",
			"FM"=>"Micronesia",
			"FO"=>"Faroe Islands",
			"FR"=>"France",
			"FX"=>"France, Metropolitan",
			"GA"=>"Gabon",
			"GB"=>"United Kingdom (Great Britain)",
			"GD"=>"Grenada",
			"GE"=>"Georgia",
			"GF"=>"French Guiana",
			"GH"=>"Ghana",
			"GI"=>"Gibraltar",
			"GL"=>"Greenland",
			"GM"=>"Gambia",
			"GN"=>"Guinea",
			"GP"=>"Guadeloupe",
			"GQ"=>"Equatorial Guinea",
			"GR"=>"Greece",
			"GS"=>"South Georgia and the South Sandwich Islands",
			"GT"=>"Guatemala",
			"GU"=>"Guam",
			"GW"=>"Guinea-Bissau",
			"GY"=>"Guyana",
			"HK"=>"Hong Kong",
			"HM"=>"Heard & McDonald Islands",
			"HN"=>"Honduras",
			"HR"=>"Croatia",
			"HT"=>"Haiti",
			"HU"=>"Hungary",
			"ID"=>"Indonesia",
			"IE"=>"Ireland",
			"IL"=>"Israel",
			"IN"=>"India",
			"IO"=>"British Indian Ocean Territory",
			"IQ"=>"Iraq",
			"IR"=>"Islamic Republic of Iran",
			"IS"=>"Iceland",
			"IT"=>"Italy",
			"JM"=>"Jamaica",
			"JO"=>"Jordan",
			"JP"=>"Japan",
			"KE"=>"Kenya",
			"KG"=>"Kyrgyzstan",
			"KH"=>"Cambodia",
			"KI"=>"Kiribati",
			"KM"=>"Comoros",
			"KN"=>"St. Kitts and Nevis",
			"KP"=>"Korea, Democratic People's Republic of",
			"KR"=>"Korea, Republic of",
			"KW"=>"Kuwait",
			"KY"=>"Cayman Islands",
			"KZ"=>"Kazakhstan",
			"LA"=>"Lao People's Democratic Republic",
			"LB"=>"Lebanon",
			"LC"=>"Saint Lucia",
			"LI"=>"Liechtenstein",
			"LK"=>"Sri Lanka",
			"LR"=>"Liberia",
			"LS"=>"Lesotho",
			"LT"=>"Lithuania",
			"LU"=>"Luxembourg",
			"LV"=>"Latvia",
			"LY"=>"Libyan Arab Jamahiriya",
			"MA"=>"Morocco",
			"MC"=>"Monaco",
			"MD"=>"Moldova, Republic of",
			"MG"=>"Madagascar",
			"MH"=>"Marshall Islands",
			"ML"=>"Mali",
			"MN"=>"Mongolia",
			"MM"=>"Myanmar",
			"MO"=>"Macau",
			"MP"=>"Northern Mariana Islands",
			"MQ"=>"Martinique",
			"MR"=>"Mauritania",
			"MS"=>"Monserrat",
			"MT"=>"Malta",
			"MU"=>"Mauritius",
			"MV"=>"Maldives",
			"MW"=>"Malawi",
			"MX"=>"Mexico",
			"MY"=>"Malaysia",
			"MZ"=>"Mozambique",
			"NA"=>"Namibia",
			"NC"=>"New Caledonia",
			"NE"=>"Niger",
			"NF"=>"Norfolk Island",
			"NG"=>"Nigeria",
			"NI"=>"Nicaragua",
			"NL"=>"Netherlands",
			"NO"=>"Norway",
			"NP"=>"Nepal",
			"NR"=>"Nauru",
			"NT"=>"Neutral Zone (no longer exists)",
			"NU"=>"Niue",
			"NZ"=>"New Zealand",
			"OM"=>"Oman",
			"PA"=>"Panama",
			"PE"=>"Peru",
			"PF"=>"French Polynesia",
			"PG"=>"Papua New Guinea",
			"PH"=>"Philippines",
			"PK"=>"Pakistan",
			"PL"=>"Poland",
			"PM"=>"St. Pierre & Miquelon",
			"PN"=>"Pitcairn",
			"PR"=>"Puerto Rico",
			"PT"=>"Portugal",
			"PW"=>"Palau",
			"PY"=>"Paraguay",
			"QA"=>"Qatar",
			"RE"=>"Réunion",
			"RO"=>"Romania",
			"RU"=>"Russian Federation",
			"RW"=>"Rwanda",
			"SA"=>"Saudi Arabia",
			"SB"=>"Solomon Islands",
			"SC"=>"Seychelles",
			"SD"=>"Sudan",
			"SE"=>"Sweden",
			"SG"=>"Singapore",
			"SH"=>"St. Helena",
			"SI"=>"Slovenia",
			"SJ"=>"Svalbard & Jan Mayen Islands",
			"SK"=>"Slovakia",
			"SL"=>"Sierra Leone",
			"SM"=>"San Marino",
			"SN"=>"Senegal",
			"SO"=>"Somalia",
			"SR"=>"Suriname",
			"ST"=>"Sao Tome & Principe",
			"SU"=>"Union of Soviet Socialist Republics (no longer exists)",
			"SV"=>"El Salvador",
			"SY"=>"Syrian Arab Republic",
			"SZ"=>"Swaziland",
			"TC"=>"Turks & Caicos Islands",
			"TD"=>"Chad",
			"TF"=>"French Southern Territories",
			"TG"=>"Togo",
			"TH"=>"Thailand",
			"TJ"=>"Tajikistan",
			"TK"=>"Tokelau",
			"TM"=>"Turkmenistan",
			"TN"=>"Tunisia",
			"TO"=>"Tonga",
			"TP"=>"East Timor",
			"TR"=>"Turkey",
			"TT"=>"Trinidad & Tobago",
			"TV"=>"Tuvalu",
			"TW"=>"Taiwan, Province of China",
			"TZ"=>"Tanzania, United Republic of",
			"UA"=>"Ukraine",
			"UG"=>"Uganda",
			"UM"=>"United States Minor Outlying Islands",
			"US"=>"United States of America",
			"UY"=>"Uruguay",
			"UZ"=>"Uzbekistan",
			"VA"=>"Vatican City State (Holy See)",
			"VC"=>"St. Vincent & the Grenadines",
			"VE"=>"Venezuela",
			"VG"=>"British Virgin Islands",
			"VI"=>"United States Virgin Islands",
			"VN"=>"Viet Nam",
			"VU"=>"Vanuatu",
			"WF"=>"Wallis & Futuna Islands",
			"WS"=>"Samoa",
			"YD"=>"Democratic Yemen (no longer exists)",
			"YE"=>"Yemen",
			"YT"=>"Mayotte",
			"YU"=>"Yugoslavia",
			"ZA"=>"South Africa",
			"ZM"=>"Zambia",
			"ZR"=>"Zaire",
			"ZW"=>"Zimbabwe",
			"ZZ"=>"Unknown or unspecified country",
		);
		return $list;

	}


	public static function getHomepageImage() {	
		
		$data = SiteSettingsModel::where('v_key','HOMEPAGE_IMAGE_1')->orWhere('v_key','HOMEPAGE_IMAGE_2')->orWhere('v_key','HOMEPAGE_IMAGE_3')->orWhere('v_key','HOMEPAGE_IMAGE_4')->orWhere('v_key','HOMEPAGE_IMAGE_5')->get();
	
		$response=array();
		if(count($data)){
			foreach ($data as $key => $value) {
				if($value->v_key=="HOMEPAGE_IMAGE_1" && $value->v_value!="" && $value->v_value!=null){
					$response[]=$value->v_value;
				}
				if($value->v_key=="HOMEPAGE_IMAGE_2" && $value->v_value!="" && $value->v_value!=null){
					$response[]=$value->v_value;
				}

				if($value->v_key=="HOMEPAGE_IMAGE_3" && $value->v_value!="" && $value->v_value!=null){
					$response[]=$value->v_value;
				}
				if($value->v_key=="HOMEPAGE_IMAGE_4" && $value->v_value!="" && $value->v_value!=null){
					$response[]=$value->v_value;
				}
				if($value->v_key=="HOMEPAGE_IMAGE_5" && $value->v_value!="" && $value->v_value!=null){
					$response[]=$value->v_value;
				}
			}
		}
		
		shuffle($response);
		return $response;
	}

	public static function LevelData( $level="" ) {	
		
		$leveldata=array();
		if($level!=""){
			$leveldata = LevelsModel::where('i_level',$level)->first();
		}
		return $leveldata;
	}

	public static function LevelDataAll() {	
		
		
		
		$leveldata = LevelsModel::get();
		$levellist = array();
		if(count($leveldata)){
			foreach ($leveldata as $key => $value) {
				$levellist[$value->i_level]=$value->v_text_public;
			}	
		}
		return $levellist;
	}

	//Course and job search
	public static function CourseSearchBold($data =array(),$pricefilter=array(),$existcids=array()){

		$totalimpesion = self::getSiteSetting("SETTING_PER_DAY_IMPRESSIONS");
		if($totalimpesion==""){
			$totalimpesion=5000;
		}else{
			$totalimpesion=(int)$totalimpesion;
		}

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = CoursesModel::query();
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('v_title','like','%'.$data['keyword'].'%')
	              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
	    	});	
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_level_id']) && $data['i_level_id']!=""){
			$query = $query->where('i_level_id',$data['i_level_id']);
		}
		
		if(isset($data['i_language_id']) && count($data['i_language_id'])){
			$query = $query->whereIn('i_language_id',$data['i_language_id']);
		}	

		if(isset($data['i_level_id_sidebar']) && count($data['i_level_id_sidebar'])){
			$query = $query->whereIn('i_level_id',$data['i_level_id_sidebar']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('f_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('f_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('f_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('f_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(count($existcids)){
			$query = $query->whereNotIn('_id', $existcids);	
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="latest"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="lowtohigh"){
				$query = $query->orderBy('f_price',"ASC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('f_price',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('i_impression_perday','<=',$totalimpesion);
		$query = $query->where('e_sponsor_status',"start");
		$query = $query->where('i_delete','!=',"1");
	    $query = $query->where('e_status',"published");
	    
	   	$courses = $query->limit(3)->get();
	    return $courses;
	}

	public static function CourseSearchNew($data =array(),$pricefilter=array(),$existcids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = CoursesModel::query();
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('v_title','like','%'.$data['keyword'].'%')
	              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
	    	});	
		}

		if(count($existcids)){
			$query = $query->whereNotIn('_id', $existcids);	
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_level_id']) && $data['i_level_id']!=""){
			$query = $query->where('i_level_id',$data['i_level_id']);
		}
		
		if(isset($data['i_language_id']) && count($data['i_language_id'])){
			$query = $query->whereIn('i_language_id',$data['i_language_id']);
		}
		if(isset($data['i_level_id_sidebar']) && count($data['i_level_id_sidebar'])){
			$query = $query->whereIn('i_level_id',$data['i_level_id_sidebar']);
		}


		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('f_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('f_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('f_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('f_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="latest"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="lowtohigh"){
				$query = $query->orderBy('f_price',"ASC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('f_price',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		
		$currentDate = date('Y-m-d H:i:s',strtotime('-1 days'));
		$query = $query->where('d_signup_date','>',$currentDate);		
		
		$query = $query->where('i_delete','!=',"1");
	    $query = $query->where('e_status',"published");
	   	$courses = $query->orderBy('RAND()')->limit(3)->get();
		return $courses;
	}

	public static function CourseSearchNormal($data =array(),$pricefilter=array(),$existcids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = CoursesModel::query();
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('v_title','like','%'.$data['keyword'].'%')
	              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
	    	});	
		}

		if(count($existcids)){
			$query = $query->whereNotIn('_id', $existcids);	
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_level_id']) && $data['i_level_id']!=""){
			$query = $query->where('i_level_id',$data['i_level_id']);
		}
		
		if(isset($data['i_language_id']) && count($data['i_language_id'])){
			$query = $query->whereIn('i_language_id',$data['i_language_id']);
		}

		if(isset($data['i_level_id_sidebar']) && count($data['i_level_id_sidebar'])){
			$query = $query->whereIn('i_level_id',$data['i_level_id_sidebar']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('f_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('f_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('f_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('f_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="latest"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="lowtohigh"){
				$query = $query->orderBy('f_price',"ASC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('f_price',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		
		$query = $query->where('e_sponsor_status','!=',"start");
		$query = $query->where('i_delete','!=',"1");
	    $query = $query->where('e_status',"published");
	   	$courses = $query->limit(5)->get();
		return $courses;
	}
	
	public static function CourseSearchBoldLoad($data =array(),$pricefilter=array(),$existcids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = CoursesModel::query();
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('v_title','like','%'.$data['keyword'].'%')
	              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
	    	});	
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_level_id']) && $data['i_level_id']!=""){
			$query = $query->where('i_level_id',$data['i_level_id']);
		}
	
		if(isset($data['i_language_id']) && count($data['i_language_id'])){
			$query = $query->whereIn('i_language_id',$data['i_language_id']);
		}

		if(isset($data['i_level_id_sidebar']) && count($data['i_level_id_sidebar'])){
			$query = $query->whereIn('i_level_id',$data['i_level_id_sidebar']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('f_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('f_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('f_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('f_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="latest"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="lowtohigh"){
				$query = $query->orderBy('f_price',"ASC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('f_price',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		
		if(count($existcids)){
			$query = $query->whereNotIn('_id', $existcids);	
		}
		
		$query = $query->where('e_sponsor_status',"start");
		$query = $query->where('i_delete','!=',"1");
	    $query = $query->where('e_status',"published");
	   	$courses = $query->orderBy('RAND()')->limit(2)->get();
		return $courses;
	}

	public static function CourseSearchNormalSidebar($data =array(),$pricefilter=array(),$existcids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = CoursesModel::query();
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('v_title','like','%'.$data['keyword'].'%')
	              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
	    	});	
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_level_id']) && $data['i_level_id']!=""){
			$query = $query->where('i_level_id',$data['i_level_id']);
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="latest"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="lowtohigh"){
				$query = $query->orderBy('f_price',"ASC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('f_price',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}
		$query = $query->where('i_delete','!=',"1");
	    $query = $query->where('e_status',"published");
	   	$courses = $query->get();
		return $courses;
	}

	public static function CourseSearchNormalSidebar1($data =array(),$pricefilter=array(),$existcids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = CoursesModel::query();
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('v_title','like','%'.$data['keyword'].'%')
	              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
	    	});	
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_level_id']) && $data['i_level_id']!=""){
			$query = $query->where('i_level_id',$data['i_level_id']);
		}

		if(isset($data['i_level_id_sidebar']) && count($data['i_level_id_sidebar'])){
			$query = $query->whereIn('i_level_id',$data['i_level_id_sidebar']);
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="latest"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="lowtohigh"){
				$query = $query->orderBy('f_price',"ASC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('f_price',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}
		$query = $query->where('i_delete','!=',"1");
	    $query = $query->where('e_status',"published");
	   	$courses = $query->get();
		return $courses;
	}


	public static function SkillSearchBoldSidebar($data =array(),$pricefilter=array(),$existskillids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = SellerprofileModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}

		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('i_mainskill_id',$data['i_mainskill_id'])
	              ->orWhere('i_otherskill_id',$data['i_mainskill_id']);
	    	});
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}
		
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$skilldata = $query->get();
		return $skilldata;
	}
	public static function JobSearchBoldSidebar($data =array(),$pricefilter=array(),$existjobids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
		
		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}

		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status','!=',"inactive");
	  	$jobsdata = $query->get();
	  	return $jobsdata;
	}

	
	//Online Skill and job search
	public static function SkillSearchBold($data =array(),$pricefilter=array(),$existskillids=array()){

		$totalimpesion = self::getSiteSetting("SETTING_PER_DAY_IMPRESSIONS");
		if($totalimpesion==""){
			$totalimpesion=5000;
		}else{
			$totalimpesion=(int)$totalimpesion;
		}

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = SellerprofileModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}

		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('i_mainskill_id',$data['i_mainskill_id'])
	              ->orWhere('i_otherskill_id',$data['i_mainskill_id']);
	    	});
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('i_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('i_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(count($existskillids)){
			$query = $query->whereNotIn('_id', $existskillids);	
		}

		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_experience_level',$data['v_skill_level_looking']);
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('i_impression_perday','<=',$totalimpesion);
		$query = $query->where('e_sponsor_status',"start");
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$skilldata = $query->orderBy('RAND()')->limit(3)->get()->toArray();
		shuffle($skilldata);
		return $skilldata;
	}

	public static function SkillSearchNew($data =array(),$existskillids=array(),$pricefilter=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = SellerprofileModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}

		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('i_mainskill_id',$data['i_mainskill_id'])
	              ->orWhere('i_otherskill_id',$data['i_mainskill_id']);
	    	});
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(count($existskillids)){
			$query = $query->whereNotIn('_id', $existskillids);	
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('i_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('i_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_experience_level',$data['v_skill_level_looking']);
		}

		$currentDate = date('Y-m-d H:i:s',strtotime('-1 days'));
		$query = $query->where('d_signup_date','>',$currentDate);		

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$skilldata = $query->limit(3)->get()->toArray();
		shuffle($skilldata);
		return $skilldata;
	}

	public static function SkillSearchNoraml($data =array(),$existskillids=array(),$pricefilter=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = SellerprofileModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}

		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('i_mainskill_id',$data['i_mainskill_id'])
	              ->orWhere('i_otherskill_id',$data['i_mainskill_id']);
	    	});
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}
		
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_experience_level',$data['v_skill_level_looking']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('i_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('i_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}


		if(count($existskillids)){
			$query = $query->whereNotIn('_id', $existskillids);	
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('e_sponsor_status','!=',"start");
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$skilldata = $query->orderBy('RAND()')->limit(5)->get()->toArray();
		if(count($skilldata)){
	  		shuffle($skilldata);	
	  	}
		return $skilldata;
	}

	public static function SkillSearchBoldLoad($data =array(),$pricefilter=array(),$existskillids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = SellerprofileModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}

		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('i_mainskill_id',$data['i_mainskill_id'])
	              ->orWhere('i_otherskill_id',$data['i_mainskill_id']);
	    	});
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('i_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('i_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}
		
		if(count($existskillids)){
			$query = $query->whereNotIn('_id', $existskillids);	
		}

		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_experience_level',$data['v_skill_level_looking']);
		}

		
		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		
		$query = $query->where('e_sponsor_status',"start");
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$skilldata = $query->orderBy('RAND()')->limit(2)->get()->toArray();
		shuffle($skilldata);
		return $skilldata;
	}

	//Online job search
	public static function JobSearchBold($data =array(),$pricefilter=array(),$existjobids=array()){

		$totalimpesion = self::getSiteSetting("SETTING_PER_DAY_IMPRESSIONS");
		if($totalimpesion==""){
			$totalimpesion=5000;
		}else{
			$totalimpesion=(int)$totalimpesion;
		}


		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
	
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_skill_level_looking',$data['v_skill_level_looking']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('v_budget_amt','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('v_budget_amt','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(count($existjobids)){
			$query = $query->whereNotIn('_id', $existjobids);	
		}
		$query = $query->where('i_impression_perday','<=',$totalimpesion);
		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		$query = $query->where('e_sponsor_status',"start");
		if(isset($data['relevance']) && $data['relevance']!=""){
			
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status','!=',"inactive");
	  	$jobsdata = $query->limit(3)->get()->toArray();
	  	return $jobsdata;
	}

	public static function JobSearchNew($data =array(),$pricefilter=array(),$existjobids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
		

		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_skill_level_looking',$data['v_skill_level_looking']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('v_budget_amt','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('v_budget_amt','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(count($existjobids)){
			$query = $query->whereNotIn('_id', $existjobids);	
		}
		
		$currentDate = date('Y-m-d H:i:s',strtotime('-1 days'));
		$query = $query->where('d_signup_date','>',$currentDate);		

		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status','!=',"inactive");
	  	$jobsdata = $query->limit(3)->get()->toArray();
	  
	  	if(count($jobsdata)){
	  		shuffle($jobsdata);	
	  	}
	  	return $jobsdata;
	}

	public static function JobSearchUrgent($data =array(),$pricefilter=array(),$existjobids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
	
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_skill_level_looking',$data['v_skill_level_looking']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('v_budget_amt','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('v_budget_amt','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(count($existjobids)){
			$query = $query->whereNotIn('_id', $existjobids);	
		}

		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		$query = $query->where('e_urgent',"yes");
		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		$query = $query->where('e_status','!=',"inactive");
		$query = $query->where('i_delete','!=',"1");
	  	$jobsdata = $query->limit(3)->get()->toArray();

	  	if(count($jobsdata)){
	  		shuffle($jobsdata);	
	  	}

	  	return $jobsdata;
	}

	public static function JobSearchNormal($data =array(),$pricefilter=array(),$existjobids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
	
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_skill_level_looking',$data['v_skill_level_looking']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('v_budget_amt','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('v_budget_amt','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(count($existjobids)){
			$query = $query->whereNotIn('_id', $existjobids);	
		}

		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('e_sponsor_status','!=',"start");
		$query = $query->where('e_status','!=',"inactive");
		$query = $query->where('i_delete','!=',"1");
	  	$jobsdata = $query->limit(5)->get()->toArray();
	  	return $jobsdata;
	}
	
	public static function JobSearchBoldLoad($data =array(),$pricefilter=array(),$existjobids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
		
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_skill_level_looking',$data['v_skill_level_looking']);
		}
		
		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('v_budget_amt','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('v_budget_amt','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}	

		if(count($existjobids)){
			$query = $query->whereNotIn('_id', $existjobids);	
		}
		
		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		//$query = $query->where('e_urgent',"yes");
		$query = $query->where('e_sponsor_status',"start");
		if(isset($data['relevance']) && $data['relevance']!=""){
			
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		$query = $query->where('e_status','!=',"inactive");
		$query = $query->where('i_delete','!=',"1");
	  	$jobsdata = $query->limit(2)->get()->toArray();
	  	if(count($jobsdata)){
	  		shuffle($jobsdata);	
	  	}
	  	return $jobsdata;
	}



	//Inperson search
	public static function InpersonSkillSearchBoldSidebar($data =array(),$pricefilter=array(),$existskillids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = SellerprofileModel::query()->where('v_service', 'inperson');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}

		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('i_mainskill_id',$data['i_mainskill_id'])
	              ->orWhere('i_otherskill_id',$data['i_mainskill_id']);
	    	});
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){

			$distance=50;
			if(isset($data['i_radius']) && $data['i_radius']!=""){
				$distance = (int)$data['i_radius'];
			}

			$latitude = (float)$data['v_latitude'];
			$longitude = (float)$data['v_longitude'];
			$searchlocation['$geometry']['type']='Point';
			$searchlocation['$geometry']['coordinates']=array($longitude,$latitude);
			$searchlocation['$maxDistance']=$distance;
			$query = $query->where('location','near',$searchlocation);					

		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$skilldata = $query->get();
		return $skilldata;
	}
	public static function InpersonJobSearchBoldSidebar($data =array(),$pricefilter=array(),$existjobids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'inperson');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
		
		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){

			$distance=50;
			if(isset($data['i_radius']) && $data['i_radius']!=""){
				$distance = (int)$data['i_radius'];
			}

			$latitude = (float)$data['v_latitude'];
			$longitude = (float)$data['v_longitude'];
			$searchlocation['$geometry']['type']='Point';
			$searchlocation['$geometry']['coordinates']=array($longitude,$latitude);
			$searchlocation['$maxDistance']=$distance;
			$query = $query->where('location','near',$searchlocation);					

		}
	
		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status','!=',"inactive");
	  	$jobsdata = $query->get();
	  	return $jobsdata;
	}

	//Inperson service skill search
	public static function InpersonSkillSearchBold($data =array(),$pricefilter=array(),$existskillids=array()){

		$totalimpesion = self::getSiteSetting("SETTING_PER_DAY_IMPRESSIONS");
		if($totalimpesion==""){
			$totalimpesion=5000;
		}else{
			$totalimpesion=(int)$totalimpesion;
		}


		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = SellerprofileModel::query()->where('v_service', 'inperson');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}

		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('i_mainskill_id',$data['i_mainskill_id'])
	              ->orWhere('i_otherskill_id',$data['i_mainskill_id']);
	    	});
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}
		
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_experience_level',$data['v_skill_level_looking']);
		}


		if(count($existskillids)){
			$query = $query->whereNotIn('_id', $existskillids);	
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('i_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('i_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}
		
		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){

			$distance=50;
			if(isset($data['i_radius']) && $data['i_radius']!=""){
				$distance = (int)$data['i_radius'];
			}

			$latitude = (float)$data['v_latitude'];
			$longitude = (float)$data['v_longitude'];
			$searchlocation['$geometry']['type']='Point';
			$searchlocation['$geometry']['coordinates']=array($longitude,$latitude);
			$searchlocation['$maxDistance']=$distance;
			$query = $query->where('location','near',$searchlocation);					

		}


		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('i_impression_perday','<=',$totalimpesion);
		$query = $query->where('e_sponsor_status',"start");
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$skilldata = $query->orderBy('RAND()')->limit(4)->get()->toArray();
		shuffle($skilldata);
		return $skilldata;
	}

	public static function InpersonSkillSearchNew($data =array(),$existskillids=array(),$pricefilter=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = SellerprofileModel::query()->where('v_service', 'inperson');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}

		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('i_mainskill_id',$data['i_mainskill_id'])
	              ->orWhere('i_otherskill_id',$data['i_mainskill_id']);
	    	});
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(count($existskillids)){
			$query = $query->whereNotIn('_id', $existskillids);	
		}

		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_experience_level',$data['v_skill_level_looking']);
		}
		
		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('i_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('i_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){

			$distance=50;
			if(isset($data['i_radius']) && $data['i_radius']!=""){
				$distance = (int)$data['i_radius'];
			}

			$latitude = (float)$data['v_latitude'];
			$longitude = (float)$data['v_longitude'];
			$searchlocation['$geometry']['type']='Point';
			$searchlocation['$geometry']['coordinates']=array($longitude,$latitude);
			$searchlocation['$maxDistance']=$distance;
			$query = $query->where('location','near',$searchlocation);					

		}

		$currentDate = date('Y-m-d H:i:s',strtotime('-1 days'));
		$query = $query->where('d_signup_date','>',$currentDate);		

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$skilldata = $query->get()->toArray();
		shuffle($skilldata);
		return $skilldata;
	}

	public static function InpersonSkillSearchNoraml($data =array(),$existskillids=array(),$pricefilter=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = SellerprofileModel::query()->where('v_service', 'inperson');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}

		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('i_mainskill_id',$data['i_mainskill_id'])
	              ->orWhere('i_otherskill_id',$data['i_mainskill_id']);
	    	});
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}
		
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_experience_level',$data['v_skill_level_looking']);
		}

		
		if(count($existskillids)){
			$query = $query->whereNotIn('_id', $existskillids);	
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('i_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('i_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}
	
		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){

			$distance=50;
			if(isset($data['i_radius']) && $data['i_radius']!=""){
				$distance = (int)$data['i_radius'];
			}

			$latitude = (float)$data['v_latitude'];
			$longitude = (float)$data['v_longitude'];
			$searchlocation['$geometry']['type']='Point';
			$searchlocation['$geometry']['coordinates']=array($longitude,$latitude);
			$searchlocation['$maxDistance']=$distance;
			$query = $query->where('location','near',$searchlocation);					

		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('e_sponsor_status','!=',"start");
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$skilldata = $query->orderBy('RAND()')->get()->toArray();
		if(count($skilldata)){
	  		shuffle($skilldata);	
	  	}
		return $skilldata;
	}
		//Load More Serach
	public static function SkillSearchNoramlLoad($data =array(),$existskillids=array(),$pricefilter=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}
		
		$query = SellerprofileModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_profile_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}

		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where(function($query ) use($data){
	        $query->where('i_mainskill_id',$data['i_mainskill_id'])
	              ->orWhere('i_otherskill_id',$data['i_mainskill_id']);
	    	});
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}
		
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_experience_level',$data['v_skill_level_looking']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('i_price','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('i_price', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('i_price','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}
		
		if(count($existskillids)){
			$query = $query->whereNotIn('_id', $existskillids);	
		}

		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		$query = $query->where('i_delete','!=',"1");
		$query = $query->where('e_status',"active");
		$skilldata = $query->orderBy('RAND()')->get()->toArray();
		if(count($skilldata)){
	  		shuffle($skilldata);	
	  	}
		return $skilldata;
	}


	//Inperson service job search
	public static function InpersonJobSearchBold($data =array(),$pricefilter=array(),$existjobids=array()){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'inperson');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
	
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_skill_level_looking',$data['v_skill_level_looking']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('v_budget_amt','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('v_budget_amt','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}
		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){

			$distance=50;
			if(isset($data['i_radius']) && $data['i_radius']!=""){
				$distance = (int)$data['i_radius'];
			}

			$latitude = (float)$data['v_latitude'];
			$longitude = (float)$data['v_longitude'];
			$searchlocation['$geometry']['type']='Point';
			$searchlocation['$geometry']['coordinates']=array($longitude,$latitude);
			$searchlocation['$maxDistance']=$distance;
			$query = $query->where('location','near',$searchlocation);					

		}

		if(count($existjobids)){
			$query = $query->whereNotIn('_id', $existjobids);	
		}

		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		$query = $query->where('e_sponsor_status',"start");
		if(isset($data['relevance']) && $data['relevance']!=""){
			
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		$query = $query->where('e_status','!=',"inactive");
		$query = $query->where('i_delete','!=',"1");
	  	$jobsdata = $query->get()->toArray();
	  	if(count($jobsdata)){
	  		shuffle($jobsdata);	
	  	}
	  	return $jobsdata;
	}

	public static function InpersonJobSearchNew($data =array(),$pricefilter=array(),$existjobids){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'inperson');
		
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}
		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
		
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_skill_level_looking',$data['v_skill_level_looking']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('v_budget_amt','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('v_budget_amt','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){
			
			$distance=50;
			if(isset($data['i_radius']) && $data['i_radius']!=""){
				$distance = (int)$data['i_radius'];
			}

			$latitude = (float)$data['v_latitude'];
			$longitude = (float)$data['v_longitude'];
			$searchlocation['$geometry']['type']='Point';
			$searchlocation['$geometry']['coordinates']=array($longitude,$latitude);
			$searchlocation['$maxDistance']=$distance;
			$query = $query->where('location','near',$searchlocation);					
		}
		if(count($existjobids)){
			$query = $query->whereNotIn('_id', $existjobids);	
		}

		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		$query = $query->where('e_status','!=',"inactive");
		$query = $query->where('i_delete','!=',"1");
	  	$jobsdata = $query->get()->toArray();
	  	if(count($jobsdata)){
	  		shuffle($jobsdata);	
	  	}
	  	return $jobsdata;
	}

	public static function InpersonJobSearchUrgent($data =array(),$pricefilter=array(),$existjobids){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'inperson');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
		
		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('v_budget_amt','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('v_budget_amt','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_skill_level_looking',$data['v_skill_level_looking']);
		}


		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){

			$distance=50;
			if(isset($data['i_radius']) && $data['i_radius']!=""){
				$distance = (int)$data['i_radius'];
			}


			$latitude = (float)$data['v_latitude'];
			$longitude = (float)$data['v_longitude'];
			$searchlocation['$geometry']['type']='Point';
			$searchlocation['$geometry']['coordinates']=array($longitude,$latitude);
			$searchlocation['$maxDistance']=$distance;
			$query = $query->where('location','near',$searchlocation);					
		}


		if(count($existjobids)){
			$query = $query->whereNotIn('_id', $existjobids);	
		}

		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		$query = $query->where('e_urgent',"yes");
		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}
		$query = $query->where('e_status','!=',"inactive");
		$query = $query->where('i_delete','!=',"1");
	  	$jobsdata = $query->get()->toArray();

	  	if(count($jobsdata)){
	  		shuffle($jobsdata);	
	  	}

	  	return $jobsdata;
	}

	public static function InpersonJobSearchNormal($data =array(),$pricefilter=array(),$existjobids){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'inperson');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
	
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_skill_level_looking',$data['v_skill_level_looking']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('v_budget_amt','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('v_budget_amt','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(isset($data['v_latitude']) && $data['v_latitude']!="" && isset($data['v_longitude']) && $data['v_longitude']!=""){

			$distance=50;
			if(isset($data['i_radius']) && $data['i_radius']!=""){
				$distance = (int)$data['i_radius'];
			}

			$latitude = (float)$data['v_latitude'];
			$longitude = (float)$data['v_longitude'];
			$searchlocation['$geometry']['type']='Point';
			$searchlocation['$geometry']['coordinates']=array($longitude,$latitude);
			$searchlocation['$maxDistance']=$distance;
			$query = $query->where('location','near',$searchlocation);					
		}


		if(count($existjobids)){
			$query = $query->whereNotIn('_id', $existjobids);	
		}

		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('e_sponsor_status','!=',"start");
		$query = $query->where('e_status','!=',"inactive");
		$query = $query->where('i_delete','!=',"1");
	  	$jobsdata = $query->get()->toArray();
	  	//if(count($jobsdata)){
	  		shuffle($jobsdata);	
	  	//}
	  	return $jobsdata;
	}
	//Load More Serach
	public static function JobSearchNormalLoad($data =array(),$pricefilter=array(),$existjobids){

		$userid = 0;
		if(auth()->guard('web')->check()) {
			$userid = auth()->guard('web')->user()->id;   
		}

		$query = JobsModel::query()->where('v_service', 'online');
		if(isset($data['keyword']) && $data['keyword']!=""){
			$query = $query->where(function($query ) use($data){
		        $query->where('v_job_title','like','%'.$data['keyword'].'%')
		              ->orWhere('v_search_tags', 'like', '%' . $data['keyword'].'%');
		    });
		}

		if(auth()->guard('web')->check()) {
			$query = $query->where('i_user_id',"!=",$userid);
		}

		if(isset($data['i_category_id']) && $data['i_category_id']!=""){
			$query = $query->where('i_category_id',$data['i_category_id']);
		}
		if(isset($data['i_mainskill_id']) && $data['i_mainskill_id']!=""){
			$query = $query->where('i_mainskill_id',$data['i_mainskill_id'])->orWhere('i_otherskill_id',$data['i_mainskill_id']);
		}
		
		if(isset($data['v_skill_level_looking']) && count($data['v_skill_level_looking'])){
			$query = $query->whereIn('v_skill_level_looking',$data['v_skill_level_looking']);
		}

		if(isset($data['i_price_id']) && count($data['i_price_id'])){
			
			$pdata=array();
			$vdata=array();
			
			if(count($pricefilter)){
				$query = $query->where(function($query) use ( $pricefilter , $data ) {
					$cnt=1;
					foreach ($pricefilter as $key => $value) {
						$vdata=array();	
						if(in_array($value->id, $data['i_price_id'])){
							
							$vdata['v_to_value'] = (int)$value['v_to_value'];		
							$vdata['v_from_value'] = (int)$value['v_from_value'];	
							
							if($cnt==1){
								if($vdata['v_to_value']!=0){
									$query->whereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->where('v_budget_amt','>=',$vdata['v_from_value']);		
								}	
								
							}else{
								if($vdata['v_to_value']!=0){
									$query->orwhereBetween('v_budget_amt', [$vdata['v_from_value'], $vdata['v_to_value']]);		
								}else{
									$query->orwhere('v_budget_amt','>=',$vdata['v_from_value']);		
								}
							}
							$cnt=$cnt+1;
						}
					}
				});
			}	
		}

		if(count($existjobids)){
			$query = $query->whereNotIn('_id', $existjobids);	
		}

		$currentDate=date("Y-m-d");
		$query = $query->where('d_expiry_date','>',$currentDate);	
		if(isset($data['relevance']) && $data['relevance']!=""){
			if($data['relevance']=="sponsored"){
				$query = $query->where('e_sponsor_status',"start");
			}else if($data['relevance']=="oldest_first"){
				$query = $query->orderBy('newest_first',"DESC");
			}else if($data['relevance']=="hightolow"){
				$query = $query->orderBy('d_added',"DESC");
			}else if($data['relevance']=="toprated"){
				$query = $query->orderBy('i_total_avg_review',"DESC");
			}
		}else{
			$query = $query->orderBy('i_impression_day');	
		}

		$query = $query->where('e_status','!=',"inactive");
		$query = $query->where('i_delete','!=',"1");
	  	$jobsdata = $query->get()->toArray();
	  	return $jobsdata;
	}


	public static function SkillNotifyEmailNotification($sid=""){

		$Skilldata = SellerprofileModel::find($sid);
		if(!count($Skilldata)){
			return 0;
		}
		
		$notifydata = NotifyModel::where('i_category_id',$Skilldata->i_category_id)->where('i_skill_id',$Skilldata->i_mainskill_id)->where('i_delete',"!=","1")->get();

		if(!count($notifydata)){
			return 0;
		}

		$data['CATEGORY']="";
		if(count($Skilldata->hasCategory()) && isset($Skilldata->hasCategory()->v_name)){
			$data['CATEGORY']=$Skilldata->hasCategory()->v_name;
		}

		$data['SKILL']="";
		if(count($Skilldata->hasSkill()) && isset($Skilldata->hasSkill()->v_name)){
			$data['SKILL']=$Skilldata->hasSkill()->v_name;
		}	
		$titlefata="skill";
		if(isset($Skilldata->v_profile_title)){
			$titlefata = $Skilldata->v_profile_title;
		}
		$data['SKILL_URL']=url('online').'/'.$Skilldata->id.'/'.$titlefata;

	    if(count($notifydata)){
			foreach ($notifydata as $key => $value) {

				$data['UNSUBSCRIBED_URL']=url('unsubscribed-notify').'/'.$value->id;
			    
			    $value->i_count=$value->i_count+1;
			    $value->save();
			    
			    $mailcontent = EmailtemplateHelper::NewSkillNotification($data);
		        $subject = "New skill on skillbox";
		        $subject = EmailtemplateHelper::EmailTemplateSubject("5bd1c47b76fbae5a3a238755");
		        
		        $mailids=array(
		            "skill"=>$value->v_email,
		        );
		        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		    }
		}
	}

	public static function JobNotifyEmailNotificationSingle($jid=""){

		$jobdata = JobsModel::find($jid);
		if(!count($jobdata)){
			return 0;
		}
		$sellerdata = SellerprofileModel::where("e_status","active")->where('i_category_id',$jobdata->i_category_id)->get();
		if(!count($sellerdata)){
			return 0;
		}

		$imgdata="";
		if(isset($jobdata->v_photos) && count($jobdata->v_photos)){
	            if(Storage::disk('s3')->exists($jobdata->v_photos[0])){
	                $imgdata = \Storage::cloud()->url($jobdata->v_photos[0]);
	            }else{
	                $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
	            }
        }else{
            $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
        }

        $urldata = url('online-job').'/'.$jobdata->id.'/'.$jobdata->v_job_title;
       	$orderstr="";
        $orderstr.="<table style='width:100%;border-spacing: 0px;' border=1 >";
        $orderstr.="<tbody><tr>
                    <td width='10%'><img src='".$imgdata."' width='150px' height='150px'></td>
                    <td width='80%'>
                    	<p>&nbsp;<b>Title</b>  ".$jobdata->v_job_title."</p>
                    	<p>&nbsp;<b>Description</b> ".$jobdata->l_job_description."</p>
                    </td>
                    </tr>
                    <tr>
                    <td colspan='2'><a href='".$urldata."'>Click here</a> to view</td></tr></tbody>";
        $orderstr.="</table>";
        
        $data['BUYER_NAME'] = auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname;
        $data['JOB_DETAIL']=$orderstr;

        if(count($sellerdata)){
			foreach ($sellerdata as $key => $value) {
				$data['USER_FULL_NAME'] = "";
				$selleremail="";
				if(count($value->hasUser()) && isset($value->hasUser()->v_fname)){
		            $data['USER_FULL_NAME'] = $value->hasUser()->v_fname;
		        }
		        if(count($value->hasUser()) && isset($value->hasUser()->v_lname)){
		            $data['USER_FULL_NAME'] = $data['USER_FULL_NAME'].' '.$value->hasUser()->v_lname;
		        }
		        if(count($value->hasUser()) && isset($value->hasUser()->v_email)){
		            $selleremail = $value->hasUser()->v_email;
		        }

		        $mailcontent = EmailtemplateHelper::NewJobNotificationToSeller($data);
		        $subject = "New job posting";
		        $subject = EmailtemplateHelper::EmailTemplateSubject("5bbda7e876fbae29842448c2");
		        $mailids=array(
		            $data['USER_FULL_NAME']=>$selleremail,
		        );
		        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
		    }
		}
	}

	public static function JobNotifyEmailNotificationMultiple($sid=""){
		

		$sellerdata = SellerprofileModel::find($sid);
		if(!count($sellerdata)){
			return 0;
		}

		$currentDate=date("Y-m-d");
		$jobdata = JobsModel::where("e_status","active")->where("e_notify","yes")->where('d_expiry_date','>',$currentDate)->where('i_category_id',$sellerdata->i_category_id)->get();
		if(!count($jobdata)){
			return 0;
		}

		$orderstr="";
		$orderstr.="<table style='width:100%;border-spacing: 0px;' border=1>";
		$orderstr.="<tbody>";
		foreach ($jobdata as $key => $value) {
				
				$urldata = url('online-job').'/'.$value->id.'/'.$value->v_job_title;
				$imgdata="";
				if(isset($value->v_photos) && count($value->v_photos)){
			            if(Storage::disk('s3')->exists($value->v_photos[0])){
			                $imgdata = \Storage::cloud()->url($value->v_photos[0]);
			            }else{
			                $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
			            }
		        }else{
		            $imgdata = \Storage::cloud()->url('jobpost/photos/No_Image_Available.jpg');
		        }

		        $orderstr.="<tr>
                    <td width='10%'><img src='".$imgdata."' width='150px' height='150px'></td>
                    <td width='80%'>
                    	<p>&nbsp;<b>Title</b>  ".$value->v_job_title."</p>
                    	<p>&nbsp;<b>Description</b> ".$value->l_job_description."</p>
                    </td>
                    </tr>
                    <tr>
                    <td colspan='2'><a href='".$urldata."'>Click here</a> to view</td></tr>";

                if(count($value->hasUser()) && isset($value->hasUser()->v_fname)){
		            $data['BUYER_NAME'] = $value->hasUser()->v_fname;
		        }
		        if(count($value->hasUser()) && isset($value->hasUser()->v_lname)){
		            $data['BUYER_NAME'] = $data['BUYER_NAME'].' '.$value->hasUser()->v_lname;
		        }
		            
		}
		$orderstr.="</tbody>";
		$orderstr.="</table>";
		$data['JOB_DETAIL']=$orderstr;
		$data['USER_FULL_NAME'] = auth()->guard('web')->user()->v_fname.' '.auth()->guard('web')->user()->v_lname;
		$selleremail = auth()->guard('web')->user()->v_email;

		$mailcontent = EmailtemplateHelper::NewJobNotificationToSeller($data);
        $subject = "New job posting";
        $subject = EmailtemplateHelper::EmailTemplateSubject("5bbda7e876fbae29842448c2");
        
        $mailids=array(
            $data['USER_FULL_NAME']=>$selleremail,
        );
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids);
        return $res;
    }

    public static function SendSkillboxSubscriptionEmail($userdata=array(),$filepath=""){
		
		if(!count($userdata)){
			return 0;
		}
		
		$userdata = UsersModel::find($userdata->id);	
		
		$data['USER_FULL_NAME']=$userdata->v_fname.' '.$userdata->v_lname;
		$data['USER_EMAIL'] = $userdata->v_email;	

		$mailcontent="";
		$subject="";
		if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b9f4d3e8124f123c986c"){
        	$mailcontent = EmailtemplateHelper::PremiuemEmailTemplate($data);
        	$subject = "Skillbox Premium Plan Subscription";
        	$subject = EmailtemplateHelper::EmailTemplateSubject("5bb5cf2a76fbae2bb2317f46");
        
        }else if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b757d3e8125e323c986a"){
        	$mailcontent = EmailtemplateHelper::StandardEmailTemplate($data);
        	$subject = "Skillbox Standard Plan Subscription";
        	$subject = EmailtemplateHelper::EmailTemplateSubject("5bb5cefe76fbae2bb2317f45");
        
        }else{
        	$mailcontent = EmailtemplateHelper::BasicEmailTemplate($data);
        	$subject = "Skillbox Basic Plan Subscription";
        	$subject = EmailtemplateHelper::EmailTemplateSubject("5bb5cec376fbae2bb2317f44");

        }

        $mailids = array(
            $data['USER_FULL_NAME']=>$data['USER_EMAIL'],
        );

        $attachments = [];
        if($filepath!=""){
        	$attachments[] = $filepath;
        }
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids,$attachments);
        return $res;
    }

    public static function SendSkillboxRecuringSubscriptionEmail($userdata=array(),$filepath=""){
		
		if(!count($userdata)){
			return 0;
		}
		$userdata = UsersModel::find($userdata->id);	
		
		$data['USER_FULL_NAME']=$userdata->v_fname.' '.$userdata->v_lname;
		$data['USER_EMAIL'] = $userdata->v_email;	

		$mailcontent="";
		$subject="";
		if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b9f4d3e8124f123c986c"){
        	$mailcontent = EmailtemplateHelper::PremiuemRecuringEmailTemplate($data);
        	$subject = "Skillbox Premium Plan Charged";
        	$subject = EmailtemplateHelper::EmailTemplateSubject("5bc1946076fbae0c8c23a164");
        	
        }else if(isset($userdata->v_plan['id']) && $userdata->v_plan['id']=="5a65b757d3e8125e323c986a"){
        	$mailcontent = EmailtemplateHelper::StandardRecuringEmailTemplate($data);
        	$subject = "Skillbox Standard Plan Charged";
        	$subject = EmailtemplateHelper::EmailTemplateSubject("5bc1950776fbae0d6850a973");

        }

        $mailids = array(
            $data['USER_FULL_NAME']=>$data['USER_EMAIL'],
        );

        $attachments = [];
        if($filepath!=""){
        	$attachments[] = $filepath;
        }
        $res = EmailtemplateHelper::MailSendGeneral($subject,$mailcontent,$mailids,$attachments);
        return $res;
    }

    public static function getProfileData( $id ) {	

		$userdata = UsersModel::find($id);
		
		$reponse['profile']="no";
		$reponse['withskillbox']="";
		$reponse['reply']="1";	

		if(count($userdata)){
			if(isset($userdata->e_email_confirm) && $userdata->e_email_confirm=="yes"){
				$reponse['profile']="yes";
			}

				$start =strtotime($userdata->d_added);
	          	$end =  time();
	          	$diff=$end - $start;
	          	$days_between = round($diff / 86400);
	          	if($days_between>30){
	              $days_between = round($days_between / 30);
	              $days_between = $days_between." Months";
	          	}else{

	          		if($days_between>1){
	                    $days_between = round($days_between)." days";    
	                }else{
	                    $days_between = "1 day";
	                }
	           	}
	          	$reponse['withskillbox'] = $days_between;//Carbon::createFromTimeStamp(strtotime($userdata->d_added))->diffForHumans();
		}
		return $reponse;
	}

	public static function getCourseSellerReview($id) {	

		$reviewdata = CoursesReviewsModel::where('i_seller_id',$id)->get();

		$reponse['total']=count($reviewdata);
		$reponse['avgtotal']="";
		
		$totalreview="";	
		$multiplayavg = count($reviewdata)*3;	
		if(count($reviewdata)){
			foreach ($reviewdata as $key => $value) {
				$totalreview =$totalreview+$value['f_rate_instruction']+$value['f_rate_navigate']+$value['f_rate_reccommend'];		
			}
			
			$reponse['avgtotal'] = $totalreview/$multiplayavg;
			$reponse['avgtotal'] = 	$reponse['avgtotal']/count($reviewdata);	
		}

		$reponse['avgtotal'] = 0;//$totalreview/$multiplayavg;
		//$reponse['avgtotal'] = //$reponse['avgtotal']/count($reviewdata);
		return $reponse;
	}

	public static function AccountSummary() {	

		$response['buyer']['activeprofile']=0;
		$response['buyer']['activeads']=0;
		$response['buyer']['totaladsimpressions']=0;
		$response['buyer']['totalreviews']=0;
		$response['buyer']['jobsposted']=0;
		$response['buyer']['jobresponse']=0;
		$response['buyer']['totalspent']=0;

		## Buyer Account Summary
		$userid = auth()->guard('web')->user()->_id;
		$userdata = auth()->guard('web')->user();
		
		if(isset($userdata->v_buyer_online_service) && $userdata->v_buyer_online_service=="active"){
			$response['buyer']['activeprofile']=1;
		}

		if(isset($userdata->v_buyer_inperson_service) && $userdata->v_buyer_inperson_service=="active"){
			$response['buyer']['activeprofile']=$response['buyer']['activeprofile']+1;
		}

		$jobdata = JobsModel::where('i_user_id',$userid)->get();
		$response['buyer']['jobsposted'] = count($jobdata);	

		$jids=array();
		$activeads =0;
		$i_impression=0;
		
		if(count($jobdata)){
			foreach ($jobdata as $key => $value) {
				$jids[]=$value->id;
				
				if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
					$activeads = $activeads+1; 
				}
				if(isset($value->i_impression)){
					$i_impression = $i_impression+$value->i_impression;
				}		
			}
		}

		$response['buyer']['jobresponse'] = AppliedJobModel::whereIn('i_applied_id',$jids)->count();
		$response['buyer']['activeads']=$activeads;
		$response['buyer']['totalreviews'] = Buyerreview::where('i_buyer_id',$userid)->count();
		$orderdata = OrderModel::where('i_user_id',$userid)->where('e_payment_status',"success")->get();	

		$totalamt=0;
		foreach ($orderdata as $key => $value) {
			// if($value->e_transaction_type!="userplan"){
				$totalamt = $totalamt+$value->v_amount+$value->v_buyer_commission+$value->v_buyer_processing_fees;
			// }	
		}
		$response['buyer']['totalspent'] = number_format($totalamt);
		$response['buyer']['totaladsimpressions'] = number_format($i_impression);

		#=============buyer end==================================#

		$response['seller']['activeprofile']=0;
		$response['seller']['activeads']=0;
		$response['seller']['totaladsimpressions']=0;
		$response['seller']['totalreviews']=0;
		$response['seller']['jobscompleted']=0;
		$response['seller']['jobpending']=0;
		$response['seller']['jobearning']=0;
		$response['seller']['paymentpending']=0;

		if(isset($userdata->v_seller_online_service) && $userdata->v_seller_online_service=="active"){
			$response['seller']['activeprofile']=1;
		}

		if(isset($userdata->v_seller_inperson_service) && $userdata->v_seller_inperson_service=="active"){
			$response['seller']['activeprofile']=$response['seller']['activeprofile']+1;
		}
		$orderdata = OrderModel::where('i_seller_id',$userid)->where('e_transaction_type','skill')->get();

		$jobscompleted=0;
		$jobpending=0;
		$jobearning = 0;
		$paymentpending = 0;

		if(count($orderdata)){
			foreach ($orderdata as $key => $value) {
				
				if(isset($value->v_order_status) && $value->v_order_status=="completed"){
					$jobscompleted=$jobscompleted+1;
					$jobearning = $value->v_amount*10/100;				

				}
				if(isset($value->v_order_status) && $value->v_order_status=="active"){
					$jobpending=$jobpending+1;				
					$paymentpending = $paymentpending+1;
				}

			}
		}
		$response['seller']['jobscompleted']=$jobscompleted;
		$response['seller']['jobpending']=$jobpending;
		$response['seller']['jobearning']=number_format($jobearning,2);
		$response['seller']['paymentpending']=number_format($paymentpending);
		$response['seller']['totalreviews'] = SellerreviewModal::where('i_seller_id',$userid)->count();
		
		$coursedata = CoursesModel::where('i_user_id',$userid)->get();	
		

		$i_impression=0;
		$activeads = 0;

		if(count($coursedata)){
			foreach ($coursedata as $key => $value) {
				if(isset($value['e_sponsor_status']) && $value['e_sponsor_status']=="start"){
					$activeads = $activeads+1; 
				}
				if(isset($value->i_impression)){
					$i_impression = $i_impression+$value->i_impression;
				}

			}
		}

		$activeprofile = SellerprofileModel::where('i_user_id',$userid)->where('e_sponsor_status',"start")->count();
		$activeads = $activeads+$activeprofile;
		$response['seller']['totaladsimpressions']=number_format($i_impression);;
		$response['seller']['activeads']=$activeads;
		return $response;

	}

	public static function MetaFiledData($vslug="",$page=""){	

		$pagedata=array();

		if($vslug==""){

			$pagedata['v_meta_title'] = "Skillbox ".$page;
			$pagedata['v_meta_keywords'] = "Skillbox ".$page;
			$pagedata['l_meta_description'] = "Skillbox ".$page;
			$pagedata['v_title'] = $page;
			$pagedata['v_sub_headline']=$page;
			$pagedata['v_image'] = "";
			return $pagedata;			
		}

		$data = ManageMetaFieldModel::where('v_key',$vslug)->first();
		//dd($vslug);

		if(count($data)){
			$pagedata['v_title'] = $data->v_title;
			$pagedata['v_sub_headline'] = $data->v_sub_headline;
			$pagedata['v_meta_title'] = $data->v_meta_title;
			$pagedata['v_meta_keywords'] = $data->v_meta_keywords;
			$pagedata['l_meta_description'] = $data->l_meta_description;
			$pagedata['v_image'] = $data->v_image;
		}else{
			
			$pagedata['v_title'] = $page;
			$pagedata['v_sub_headline'] = $page;
			$pagedata['v_meta_title'] = "Skillbox ".$page;
			$pagedata['v_meta_keywords'] = "Skillbox ".$page;
			$pagedata['l_meta_description'] = "Skillbox ".$page;
			$pagedata['v_image'] = "";
			
		}

		return $pagedata;
		
	}

	public static function SiteLogo() {	

		$sitelogo = self::getSiteSetting('SITE_LOGO');

		$imgurl = "";
		if($sitelogo!=""){
			if(Storage::disk('s3')->exists($sitelogo)){
				$imgurl = \Storage::cloud()->url($sitelogo);
			}else{
				$imgurl = \Storage::cloud()->url('common/1516341130-l-logo.png');	
			}
		}else{
			$imgurl = \Storage::cloud()->url('common/1516341130-l-logo.png');
		}
		return $imgurl;
	
	}

	public static function sellerLevel($userid='') {

		$order = OrderModel::where('i_seller_id',$userid)->where('e_transaction_type','skill')->where('v_order_status',"completed")->count();
		$level=1;
		if($order>10 && $order<=20){
			$level=2;
		}
		else if($order>20){
			$level=3;
		}
		return $level;

	}


	public static function OrderRefNumbaer() {
		
		$ordernumber = Ordernumber::first();
	 
	    $orderno = 0;
        if(!count($ordernumber)){
            $orderno=1;
            $insertordernumber=array(
                'i_order_no'=>1
            );
            Ordernumber::insert($insertordernumber);
        }else{
            $orderno = $ordernumber->i_order_no+1;
            $updateordernumber=array(
                'i_order_no'=>$orderno
            );
            Ordernumber::find($ordernumber->id)->update($updateordernumber);
        }
        $orderrefnumber = str_pad( $orderno, 10, "ODR000000", STR_PAD_LEFT );	
        return $orderrefnumber;	

	}

	public static function baseRegistrationPlan() {

		$data=array(
			'v_plan_id'=>"5a65b48cd3e812a4253c9869",
			'v_plan_duration'=>"monthly",

		);
		return $data;

	}

	public static function ResolutionProblem(){

		$data=array(
			'1'=>"I didn't receive what I ordered",
			'2'=>"The seller is not responding",
			'3'=>"The quality of the  work I received was poor",
			'4'=>"The seller can't deliver on time",
			'5'=>"Other",
		);

		return $data;

	}

	public static function sellerReview($id=""){

		
		$reviewdata = SellerreviewModal::where('i_seller_id',$id)->get();
		
		$review['avgtotal']=0;
		$review['total']=count($reviewdata);
		
		$review['i_communication_star']=0;
		$review['i_described_star']=0;
		$review['i_recommend_star']=0;

		if(count($reviewdata)){
			foreach ($reviewdata as $key => $value) {
				$review['avgtotal'] = 	$review['avgtotal']+(($value['i_communication_star']+$value['i_described_star']+$value['i_recommend_star'])/3);
				$review['i_communication_star'] = $review['i_communication_star']+$value['i_communication_star'];
				$review['i_described_star'] = 	$review['i_described_star']+$value['i_described_star'];
				$review['i_recommend_star'] = 	$review['i_recommend_star']+$value['i_recommend_star'];
			}
			
		}
		if($review['total']!=0){
			$review['avgtotal'] = $review['avgtotal']/$review['total'];
			$review['avgtotal'] = number_format($review['avgtotal'],1);
			$review['i_communication_star'] = $review['i_communication_star']/$review['total'];
			$review['i_communication_star'] = number_format($review['i_communication_star'],1);
			$review['i_described_star'] = $review['i_described_star']/$review['total'];
			$review['i_described_star'] = number_format($review['i_described_star'],1);
			$review['i_recommend_star'] = $review['i_recommend_star']/$review['total'];
			$review['i_recommend_star'] = number_format($review['i_recommend_star'],1);
		}

		return $review;

	}

	public static function userPlanDetail($userdata=array()){

		$userplan = array();
		if(isset($userdata->v_plan) && count($userdata->v_plan)){
			$userplan = $userdata->v_plan;
		}
		
		$currentdate = date("Y-m-d");
		$expdate="";
		if(isset($userplan['d_end_date'])){
			$expdate=$userplan['d_end_date'];
		}
		
		$response['upgrade']=1;
		if(isset($userplan['id']) && $userplan['id']=="5a65b9f4d3e8124f123c986c" && strtotime($currentdate)<strtotime($expdate) ){
			$response['upgrade']=0;
		}else{
			$response['upgrade']=1;
		}

		$response['v_plan_id']=$userplan['id'];
		$response['v_plan_duration']=$userplan['duration'];
		$response['d_start_date']="";
		$response['d_end_date']="";

		if(isset($userplan['d_start_date'])){
			$response['d_start_date']=$userplan['d_start_date'];
		}
		if(isset($userplan['d_end_date'])){
			$response['d_end_date']=$userplan['d_end_date'];
		}

		$plandata = PlanModel::find($userplan['id']);

		$response['v_plan_name']="";
		if(count($plandata) && isset($plandata->v_name)){
			$response['v_plan_name']=$plandata->v_name;
		}
		return $response;
	}

	
	

	public static function upgradeUserPlanDetail($userdata=array(),$planid=""){

		if(!count($userdata)){
			return 1;
		}

		$userplan = array();
		if(isset($userdata->v_plan) && count($userdata->v_plan)){
			$userplan = $userdata->v_plan;
		}
		$currentdate = date("Y-m-d");
		if($userplan['id']==$planid && strtotime($currentdate)<strtotime($userplan['d_end_date'])){
			return 0;			
		}

		return 1;

	}

	public static function userbasicPlanCheck($userdata=array()){

		if(!count($userdata)){
			return 0;
		}

		$userplan = array();
		if(isset($userdata->v_plan) && count($userdata->v_plan)){
			$userplan = $userdata->v_plan;
		}
		$currentdate = date("Y-m-d");
		
		if($userplan['id']== "5a65b48cd3e812a4253c9869"){
			return 1;			
		}
		return 0;
	}


	public static function userdashboardimage($data=array()){

		$imgstr="";

		if(isset($data->v_image) && $data->v_image!=""){
			$imgurl = \Storage::cloud()->url($data->v_image);
			$imgstr='<img src="'.$imgurl.'" class="img-responsive proinfo-img " alt="Your Image">';
		}else if(isset($data->v_fname) && $data->v_fname!="" && isset($data->v_lname) && $data->v_lname!=""){
			$imgstr.='<div class="letter dashboardname">';
			$imgstr.='<div class="letter-text-myprifile">';
			$imgstr.=strtoupper(substr($data->v_fname, 0, 1)).''.strtoupper(substr($data->v_lname, 0, 1));
			$imgstr.='</div>';
			$imgstr.='</div>';
		}else{
			$imgurl = \Storage::cloud()->url('users/img-upload.png');
			$imgstr ='<img src="'.$imgurl.'" class="img-responsive proinfo-img" alt="Your Image">';
		}

		return $imgstr;
	}

	public static function userroundimagemyprofile($data=array()){

		$imgstr="";

		if(isset($data['v_image']) && $data['v_image']!=""){
			$imgurl = \Storage::cloud()->url($data['v_image']);
			$imgstr='<img src="'.$imgurl.'" alt="Your Image">';
		}else if(isset($data['v_fname']) && $data['v_fname']!="" && isset($data['v_lname']) && $data['v_lname']!=""){
			$imgstr.='<div class="letter myprofilelettertext">';
			$imgstr.='<div class="letter-text">';
			$imgstr.=strtoupper(substr($data['v_fname'], 0, 1)).''.strtoupper(substr($data['v_lname'], 0, 1));
			$imgstr.='</div>';
			$imgstr.='</div>';
		}else{
			$imgurl = \Storage::cloud()->url('users/img-upload.png');
			$imgstr ='<img src="'.$imgurl.'" alt="Your Image">';
		}

		return $imgstr;
	}

	public static function userroundimage($data=array()){

		$imgstr="";

		if(isset($data['v_image']) && $data['v_image']!=""){
			$imgurl = \Storage::cloud()->url($data['v_image']);
			$imgstr='<img src="'.$imgurl.'" alt="Your Image">';
		}else if(isset($data['v_fname']) && $data['v_fname']!="" && isset($data['v_lname']) && $data['v_lname']!=""){
			$imgstr.='<div class="letter roundimgname">';
			$imgstr.='<div class="letter-text">';
			$imgstr.=strtoupper(substr($data['v_fname'], 0, 1)).''.strtoupper(substr($data['v_lname'], 0, 1));
			$imgstr.='</div>';
			$imgstr.='</div>';
		}else{
			$imgurl = \Storage::cloud()->url('users/img-upload.png');
			$imgstr ='<img src="'.$imgurl.'" alt="Your Image">';
		}

		return $imgstr;
	}

	public static function adminroundimage($data=array()){

		$imgstr="";
		if(isset($data['v_profile']) && $data['v_profile']!=""){
			$imgurl = \Storage::cloud()->url($data['v_profile']);
			$imgstr='<img src="'.$imgurl.'" alt="Your Image">';
		}else{
			$imgurl = \Storage::cloud()->url('users/img-upload.png');
			$imgstr ='<img src="'.$imgurl.'" alt="Your Image">';
		}
		return $imgstr;
	}

	public static function userCurrentPlan($userId){

		$userdata = UsersModel::find($userId);
		$response=array();

		$planId="";
		$planDuration="";

		if(count($userdata)){
			if(isset($userdata->v_plan) && count($userdata->v_plan)){
				$planId =  $userdata->v_plan['id'];
				$planDuration =  $userdata->v_plan['duration'];
			}
		}
		
		$plandata = PlanModel::find($planId);
		
		$response['planId']= $planId;
		$response['planDuration']= $planDuration;
		$response['planName']= $planDuration;
		$response['isBasic']= 0;

		if($planId=="5a65b48cd3e812a4253c9869"){
			$response['isBasic']= 1;			
		}

		if(count($plandata)){
			$response['planName']= $plandata->v_name;
			if($planDuration=="monthly"){
				$response['price']= $plandata->f_monthly_dis_price;			
			}else{
				$response['price']= $plandata->f_yearly_dis_price;			
			}
		}



		return $response;
	}

	public static function userProfileRoundImage($data){

		$img="";
		if(isset($data['v_image']) && $data['v_image']!=""){
			$imgsrc =url('public/uploads/userprofiles')."/".$data['v_image'];
			$img ='<img src="'.$imgsrc.'"alt="" />';    

		}else if(isset($data['v_fname']) && $data['v_fname']!=""){
			$img ='<div class="letter"><div class="letter-text-myprifile">'.strtoupper(substr($data['v_fname'], 0, 1)).''.strtoupper(substr($data['v_lname'], 0, 1)).'</div></div>';
	   }else{
	   		$img ='<img src="'.url('public/uploads/userprofiles/img-upload.png').'"alt="" />';    
	   }
	   return $img;
	}

	public static function successMessage($msg=""){
		
		$response="";		
		$response.='<div class="alert alert-success alert-big alert-dismissable br-5">';
		$response.='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
		$response.='<strong><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span>'.$msg.'</span>';
		$response.='</div>';
		return $response;
	}


	public static function errorMessage($msg=""){
		
		$response="";
		$response.='<div class="alert alert-info alert-danger">';
		$response.='<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
		$response.='<strong> <i class="fa fa-warning"></i>&nbsp;&nbsp;</strong>&nbsp;&nbsp;<span>'.$msg.'</span>';
		$response.='</div>';
		return $response;	

	}	

	public static function supportQueryData() {

		$queryData=array(
			'technical_problem'=>'Technical Problem',
			'general_questions'=>'General Questions',
			'how_to_use_skillbox'=>'How to use skillbox',
			'billing'=>'Billing',
			'other'=>'Other',
		);
		return $queryData;
	}

	public static function supportNumber(){

		$data = SupportModel::orderBy("d_added","DESC")->first();
		
		$ticket_number=0;

		if(count($data)){
			if(isset($data->i_ticket_num) && $data->i_ticket_num!=""){
				$ticket_number=$data->i_ticket_num+1;
			}else{
				$ticket_number=1;	
			}
		}else{
			$ticket_number=1;
		}
		return $ticket_number;
	}

	public static function generate_password(){
		  
		  $length = 10;
		  $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.
		            '0123456789-=~!@#$%^&*()_+,./<>?;:[]{}\|';
		            
		  $str = '';
		  $max = strlen($chars) - 1;

		  for ($i=0; $i < $length; $i++)
		    $str .= $chars[mt_rand(0, $max)];

		  return $str;
	}


	public static function PlansList() {


		$plandata = PlanModel::where('e_status','active')->get();

		$discount = self::getSiteSetting("PLAN_YEALY_DISCOUNT");

		if(!$discount){
			$discount=10;
		}

		$planlist=array();
		if(count($plandata)){
			foreach ($plandata as $key => $value) {
			
				$yearprice = $value->f_monthly_price*12*$discount/100;		
				$yearpricedis = $value->f_monthly_dis_price*12*$discount/100;		

				$finalyearprice = $value->f_monthly_price*12-$yearprice;
				$finalyearpricedis = $value->f_monthly_dis_price*12-$yearpricedis;

				$planlist[]=array(
					'id'=>$value->id,
					'v_name'=>$value->v_name,
					'v_subtitle'=>$value->v_subtitle,
					'e_status'=>$value->e_status,	
					'l_bullet'=>$value->l_bullet,
					'l_addon'=>$value->l_addon,
					'f_monthly_price'=>number_format($value->f_monthly_price,2),
					'f_monthly_dis_price'=>number_format($value->f_monthly_dis_price,2),
					'f_yealry_price'=>number_format($finalyearprice,2),
					'f_yealry_dis_price'=>number_format($finalyearpricedis,2),
				);
			}
		}

		return $planlist;
	}

	public static function currentUserPlanData(){

		if(count(auth()->guard('web')->user()->buyer)){
			$usertype =  "buyer";
			$userservice = auth()->guard('web')->user()->buyer['v_service'];
		}else if(count(auth()->guard('web')->user()->seller)){
			$usertype = "seller";
			$userservice = auth()->guard('web')->user()->seller['v_service'];
		}

	}

	public static function userPlanData($userId){

		$userdata = UsersModel::find($userId);
		$response=array();
		$planId="";
		$planDuration="";

		if(count($userdata)){
			if(isset($userdata->v_plan) && count($userdata->v_plan)){
				$planId =  $userdata->v_plan['id'];
				$planDuration =  $userdata->v_plan['duration'];
			}
		}

		$plandata = PlanModel::find($planId);
	
		$response['planId']= $planId;
		$response['planDuration']= $planDuration;
		$response['planName']= $planDuration;

		if(count($plandata)){
			$response['planName']= $plandata->v_name;
			if($planDuration=="monthly"){
				$response['price']= $plandata->f_monthly_dis_price;			
			}else{
				$response['price']= $plandata->f_yearly_dis_price;			
			}
		}
		return $response;

	}

	

	public static function checkUserJobPostingData($userdata=array()){

		if(!count($userdata)){
			return 0;
		}
		
		$plan = array();
		if(!isset($userdata->v_plan)){
			return 0;
		}else{
			$plan = $userdata->v_plan;
		}

		$userid = $userdata->id;
		$userservice = $userdata->buyer['v_service'];

		$onlinejobpost=0;
		$inpersonjobpost=0;

		if($plan['id']=="5a65b48cd3e812a4253c9869"){
			
			$onlinejobpost=3;
			$inpersonjobpost=3;

			$jobcnt = JobsModel::where("i_user_id",$userid)->where("v_service",$userservice)->count();
			if($jobcnt>$onlinejobpost){
				return 0;
			}
			return 1;

		}else if($plan['id']=="5a65b757d3e8125e323c986a"){
			
			$onlinejobpost=5;
			$inpersonjobpost=5;
			$jobcnt = JobsModel::where("i_user_id",$userid)->where("v_service",$userservice)->count();
			if($jobcnt>$onlinejobpost){
				return 0;
			}
			return 1;	

		}else if($plan['id']=="5a65b9f4d3e8124f123c986c"){
			
			$onlinejobpost=0;
			$inpersonjobpost=0;

			return 1;
		}

		return 0;
	}


	public static function jobpostingrestriction($userdata=array()){

		if(!count($userdata)){
			return 0;
		}
		$userid = $userdata->id;
		$userservice = $userdata->buyer['v_service'];
		$currentDate=date("Y-m-d");
		if($userdata->v_plan['id']=="5a65b48cd3e812a4253c9869"){
			
			$onlinejobpost=5;
			$inpersonjobpost=5;
			$jobcnt = JobsModel::where("i_user_id",$userid)->where("e_status","active")->where('d_expiry_date','>',$currentDate)->where("v_service",$userservice)->count();
			if($jobcnt>=$onlinejobpost){
				return 0;
			}
			return 1;

		}else if($userdata->v_plan['id']=="5a65b757d3e8125e323c986a"){

			$onlinejobpost=10;
			$inpersonjobpost=10;
			$jobcnt = JobsModel::where("i_user_id",$userid)->where("e_status","active")->where('d_expiry_date','>',$currentDate)->where("v_service",$userservice)->count();
			if($jobcnt>=$onlinejobpost){
				return 0;
			}
			return 1;	

		}else if($userdata->v_plan['id']=="5a65b9f4d3e8124f123c986c"){
			
			$onlinejobpost=30;
			$inpersonjobpost=30;
			$jobcnt = JobsModel::where("i_user_id",$userid)->where("e_status","active")->where('d_expiry_date','>',$currentDate)->where("v_service",$userservice)->count();
			if($jobcnt>=$onlinejobpost){
				return 0;
			}
			return 1;
		}
		return 0;
	}

	
	public static function checkUserjobpostplan($userdata=array()){

		if(!count($userdata)){
			return 0;
		}

		$plan = array();
		if(!isset($userdata->v_plan)){
			return 0;
		}else{
			$plan = $userdata->v_plan;
		}

		if($plan['id']=="5a65b48cd3e812a4253c9869"){
			return 1;	
		}else if($plan['id']=="5a65b757d3e8125e323c986a"){
			return 1;	
		}else if($plan['id']=="5a65b9f4d3e8124f123c986c"){
			return 0;	
		}
		return 1;	
	}
	
	public static function upgradeUserPlan($userid="",$data=array()){

		if($userid==""){
			return 0;
		}

		$userdata = UsersModel::find($userid);
		if(!count($userdata)){
			return 0;
		}

		if(isset($data['v_plan']['id']) && $data['v_plan']['id']!="" && isset($data['v_plan']['duration']) && $data['v_plan']['duration']!="" ){
			$data['d_modified']= date("Y-m-d H:i:s");
			UsersModel::find($userid)->update($data);
			return 1;
		}else{
			return 0;
		}


	}

	public static function checkBasicPlan($userid=""){

		$userdata = UsersModel::find($userid);
		
		if(!count($userdata)){
			return 0;
		}

		if(!isset($userdata['v_plan'])){
			return 0;
		}
	
		if(!isset($userdata['v_plan']['id'])){
			return 0;
		}

		if($userdata['v_plan']['id']!="5a65b48cd3e812a4253c9869"){
			return 1;
		}
		return 0;
		
	}

	public static function checkPremiumPlan($userid=""){

		$userdata = UsersModel::find($userid);
		
		if(!count($userdata)){
			return 0;
		}

		if(!isset($userdata['v_plan'])){
			return 0;
		}
	
		if(!isset($userdata['v_plan']['id'])){
			return 0;
		}

		if($userdata['v_plan']['id']!="5a65b9f4d3e8124f123c986c"){
			return 1;
		}
		return 0;
		
	}

	
}