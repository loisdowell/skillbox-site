<?php

namespace App\Models;

// use Illuminate\Foundation\Auth\User as Authenticatable;
use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class EmailTemplate extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	protected $table = 'tbl_email_template';

	public $timestamps = false;

	protected $fillable = [
		'i_header_id',
		'i_footer_id',
		'v_title',
		'v_subject',
		'v_from',
		'e_type',
		'l_body',
		'e_status',
		'd_added',
		'd_modified',
		'i_delete',
	];

	public function hasTemplateHeader() {
		return $this->hasOne('App\Models\EmailTemplateHeader','id','i_header_id');
	}

	public function hasTemplateFooter() {
		return $this->hasOne('App\Models\EmailTemplateFooter','id','i_footer_id');
	}
}