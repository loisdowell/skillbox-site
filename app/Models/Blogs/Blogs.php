<?php

namespace App\Models\Blogs;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Blogs extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_blogs';

    public $fillable = [
			'v_name',
			'v_slug',
			'v_author_name',
			'l_short_description',
			'l_long_description',
			'v_image',
			'i_user_id',
			'v_meta_title',
			'v_meta_keywords',
			'l_meta_description',
			'e_user_type',
			'd_publish_date',
			'd_added',
			'd_modified',
			'e_status'
		];

}







