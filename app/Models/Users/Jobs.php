<?php

namespace App\Models\Users;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Jobs extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_jobs';

    public $fillable = [
				'i_user_id',
				'v_service',
				'i_category_id',
				'i_mailskill_id',
				'i_otherskill_id',
				'v_skill_level_looking',
				'v_job_title',
				'v_budget_type',
				'v_budget_amt',
				'l_short_description',
				'l_job_description',
				'v_website',
				'v_contact',
				'v_avalibility_from',
				'v_avalibility_to',
				'v_avalibilitytime_from',
				'v_avalibilitytime_to',
				'v_start_month',
				'v_start_date',
				'v_start_year',
				'd_start_date',
				'd_expiry_date',
				'v_long_expect_duration_type',
				'i_long_expect_duration',
				'i_permanent_role',
				'e_sponsor',
				'e_sponsor_status',
				'v_city',
				'v_pincode',
				'v_latitude',
				'v_longitude',
				'v_photos',
				'v_video',
				'v_search_tags',
				'v_start_date',
				'v_tags',
				'e_urgent',
				'e_notify',
				'e_status',
				'i_impression',
				'i_impression_perday',
				'i_impression_day',
				'i_contact',
				'i_clicks',
				'i_total_review',
				'i_total_avg_review',
				'i_applied',
				'd_signup_date',
				'location',
				'e_expire_notification',
				'e_expired_notification',
				'd_added',
				'd_modified',
				'i_delete',
			];

	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}

	public function hasCategory() {
		return $this->hasOne('App\Models\Categories','_id','i_category_id')->first();
	}

	public function hasSkill() {
		return $this->hasOne('App\Models\Skills','_id','i_mailskill_id')->first();
	}

	public function hasOtherSkill() {
		return $this->hasOne('App\Models\Skills','_id','i_otherskill_id')->first();
	}

}







