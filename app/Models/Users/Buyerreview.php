<?php

namespace App\Models\Users;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Buyerreview extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;
	protected $collection = 'tbl_buyer_review';

    public $fillable = [
		'i_user_id',
		'i_job_id',
		'i_buyer_id',
		'l_comment',
		'i_work_star',
		'i_support_star',
		'i_opportunities_star',
		'i_work_again_star',
		'i_avg_star',
		'd_added',
		'd_modified',
	];

	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}

	public function hasBuyer() {
		return $this->hasOne('App\Models\Users\Users','_id','i_seller_id')->first();
	}

		
}







