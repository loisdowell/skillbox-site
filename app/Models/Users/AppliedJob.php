<?php

namespace App\Models\Users;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class AppliedJob extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_applied_jobs';

    public $fillable = [
		'i_user_id',
		'i_applied_id',
		'i_seler_profile_id',
		'i_buyer_id',
		'v_service',
		'd_added'
	];

	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}
	
	public function hasSellerProfile() {
		return $this->hasOne('App\Models\Users\Sellerprofile','_id','i_seler_profile_id')->first();
	}

	public function hasJob() {
		return $this->hasOne('App\Models\Users\Jobs','_id','i_applied_id')->first();
	}

}







