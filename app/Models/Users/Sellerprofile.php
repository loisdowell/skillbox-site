<?php

namespace App\Models\Users;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Sellerprofile extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_seller_profile';

    public $fillable = [
		'i_user_id',
		'v_service',
		'e_type',	
		'i_category_id',
		'i_mainskill_id',
		'i_otherskill_id',
		'v_experience_level',
		'e_notification_subscribe',
		'v_profile_title',
		'v_hourly_rate',
		'v_website',
		'v_contact_phone',
		'l_short_description',
		'l_brief_description',
		'v_english_proficiency',
		'v_introducy_video',
		'v_work_photos',
		'v_work_video',
		'information',
		'v_avalibility_from',
		'v_avalibility_to',
		'v_avalibilitytime_from',
		'v_avalibilitytime_to',
		'v_packages',
		'v_order_requirements',
		'v_search_tags',
		'i_total_review',
		'i_total_avg_review',
		'i_price',
		'e_status',
		'v_city',
		'v_doc',
		'v_pincode',
		'v_latitude',
		'v_longitude',
		'v_facebook',
		'v_twitter',
		'v_pinterest',
		'v_instagram',
		'i_impression',
		'i_impression_perday',
		'i_impression_day',
		'i_contact',
		'location',
		'i_clicks',
		'e_sponsor',
		'e_sponsor_status',
		'i_delete',
		'd_signup_date',
		'd_added',
		'd_modified',

	];

	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}

	public function hasCategory() {
		return $this->hasOne('App\Models\Categories','_id','i_category_id')->first();
	}

	public function hasSkill() {
		return $this->hasOne('App\Models\Skills','_id','i_mainskill_id')->first();
	}

	public function hasOtherSkill() {
		return $this->hasOne('App\Models\Skills','_id','i_otherskill_id')->first();
	}
	

}







