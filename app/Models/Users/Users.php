<?php

namespace App\Models\Users;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Users extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_users';

    public $fillable = [
				'v_fname',
				'v_lname',	
				'v_email',
				'v_image',
				'i_mangopay_id',
				'i_wallet_id',
				'i_kyc_id',
				'i_card_id',
				'i_bank_id',
				'mangopay_seller',
				'i_mangopay_id_buyer',
				'i_wallet_id_buyer',
				'i_kyc_id_buyer',
				'i_card_id_buyer',
				'i_bank_id_buyer',
				'v_identity_img',
				'v_address_img',
				'v_registration_img',
				'v_articles_img',
				'v_shareholder_img',
				'i_identity_kyc_id',
				'i_address_kyc_id',
				'i_registration_kyc_id',
				'i_articles_kyc_id',
				'i_shareholder_kyc_id',
				'password',
				'seller',
				'buyer',
				'e_type',	
				'temp_type',
				'v_contact',
				'i_country_id',
				'i_state_id',
				'v_city',
				'l_address',
				'v_postcode',
				'd_birthday',
				'v_gender',
				'v_company_type',
				'v_company_name',
				'v_avalibility_from',
				'v_avalibility_to',
				'v_avalibility_from_time',
				'v_avalibility_to_time',
				'v_company_regnumber',
				'v_vat_number',
				'i_level',
				'i_newsletter',
				'v_plan',
				'billing_detail',
				'e_status',
				'e_email_confirm',
				'e_email_confirm_message',
				'v_facebook_id',
				'i_total_review',
				'i_total_avg_review',
				'i_course_total_review',
				'i_course_total_avg_review',
				'i_job_total_review',
				'i_job_total_avg_review',
				'v_phone',
				'v_website',
				'v_buyer_online_service',
				'v_buyer_inperson_service',
				'v_seller_online_service',
				'v_seller_inperson_service',
				'v_replies_time',
				'l_seller_auto_message',
				'v_level',
				'e_login',
				'e_standard_free_used',
				'e_basic_mail_send',
				'e_profile_status',
				'v_alert_box_close_1',
				'v_alert_box_close_2',
				'v_kyc_success_mail',
				'i_identity_kyc_refused_mail',
				'i_address_kyc_refused_mail',
				'i_registration_refused_mail',
				'i_articles_kyc_refused_mail',
				'i_shareholder_kyc_refused_mail',
				'v_device_token',
				'v_auth_token',
				'd_added',
				'd_modified',
				'i_delete',
	];


	public function hasCategory() {
		return $this->hasOne('App\Models\Categories','_id','i_category_id')->first();
	}

	public function hasCountry() {
		return $this->hasOne('App\Models\Country','_id','i_country_id')->first();
	}


}







