<?php

namespace App\Models\Users;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Messages extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_messages';

    public $fillable = [
				'i_parent_id',
				'i_from_id',
				'i_to_id',
				'v_subject_title',
				'l_message',
				'e_view',
				'v_replay_time',
				'd_added',
				'd_modified'
			];

	public function hasFromUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_from_id')->first();
	}
	public function hasToUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_to_id')->first();
	}

}







