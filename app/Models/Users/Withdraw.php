<?php

namespace App\Models\Users;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Withdraw extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;
	public $timestamps = false;
	protected $collection = 'tbl_withdraw';
   
    public $fillable = [
		'i_user_id',
		'i_amount',
		'i_withdraw_id',
		'i_wallet_id',
		'i_author_id',
		'e_status',
		'd_added',
		'd_modified',
	];

}







