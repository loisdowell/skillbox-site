<?php

namespace App\Models\Users;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class SellerInfo extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_seller_info';

    public $fillable = [
							'i_user_id',
							'e_experience',
							'v_profile_title',
							'v_phone',
							'v_hours_working',
							'v_project_rate',
							'f_price_start',
							'l_short_description',
							'l_brief_description',
							'v_english_level',
							'v_callouts',
							'i_sponsored'
						];

}







