<?php
namespace App\Models\Users;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Order extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;
	public $timestamps = false;
	protected $collection = 'tbl_order';

    public $fillable = [
		'i_order_no',
		'i_user_id',
		'i_seller_id',
		'i_seller_profile_id',
		'i_course_id',
		'v_profile_title',
		'v_order_start',
		'd_order_start_date',
		'i_seller_package',
		'v_duration_time',
		'v_duration_in',
		'e_transaction_type',
		'e_payment_type',
		'v_transcation_id',
		'e_payment_status',
		'e_seller_payment_status',
		'v_amount',
		'v_buyer_commission',
		'v_buyer_processing_fees',
		'v_seller_commission',
		'v_seller_processing_fees',
		'v_processing_fees',
		'v_order_detail',
		'v_order_status',
		'v_buyer_deliver_status',
		'd_order_delivered_date',
		'd_order_completed_date',
		'i_order_review',
		'e_seller_payment_amount',
		'e_seller_connection_fee',
		'e_seller_commission',
		'v_order_review',
		'v_order_requirements',
		'v_order_deliver_date',
		'v_order_notstatisfied_date',
		'v_order_requirements_submited',
		'v_sponser_sales',
		'i_delete',
		'e_cancelled_by',
		'v_admin_fees',
		'd_date',
		'd_added',
		'd_modified',
	];
	
	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}

	public function hasUserSeller() {
		return $this->hasOne('App\Models\Users\Users','_id','i_seller_id')->first();
	}

	




}







