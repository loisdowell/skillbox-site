<?php

namespace App\Models\Users;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Sellerreview extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;
	protected $collection = 'tbl_seller_review';

    public $fillable = [
		'i_user_id',
		'i_order_id',
		'i_seller_id',
		'i_seller_profile_id',
		'l_comment',
		'i_communication_star',
		'i_described_star',
		'i_recommend_star',
		'i_avg_star',
		'l_answers',
		'd_added',
		'd_modified',
	];

	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}

	public function hasSeller() {
		return $this->hasOne('App\Models\Users\Users','_id','i_seller_id')->first();
	}

		
}







