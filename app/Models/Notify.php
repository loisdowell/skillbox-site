<?php

namespace App\Models;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Notify extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_notify';

    public $fillable = [
			'v_email',
			'i_category_id',
			'i_skill_id',
			'e_notify',
			'i_count',
			'e_status',
			'i_delete',
			'd_added',
			'd_modified',
		];

	public function hasCategory() {
		return $this->hasOne('App\Models\Categories','_id','i_category_id')->first();
	}

	public function hasSkill() {
		return $this->hasOne('App\Models\Skills','_id','i_skill_id')->first();
	}
		
}







