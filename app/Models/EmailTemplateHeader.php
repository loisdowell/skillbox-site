<?php

namespace App\Models;

// use Illuminate\Foundation\Auth\User as Authenticatable;
use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class EmailTemplateHeader extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	protected $table = 'tbl_email_template_header';

	public $timestamps = false;

	protected $fillable = [
		'v_title',
		'l_description',
		'e_status',
		'd_added',
		'd_modified',
	];	
}