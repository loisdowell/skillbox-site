<?php

namespace App\Models;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class City extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_city';

    public $fillable = [
							'i_state_id',
							'v_title',
							'v_key',
							'd_added',
							'd_modified',
							'e_status'
						];

	public function hasState() {
		return $this->hasOne('App\Models\State','_id','i_state_id')->first();
	}

}







