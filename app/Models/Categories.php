<?php

namespace App\Models;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Categories extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_categories';

    public $fillable = [
			'v_type',
			'v_name',
			'v_sub_headline',
			'v_main_image',
			'v_image',
			'v_image_mobile',
			'd_added',
			'd_modified',
			'e_status'
		];

	public function hasParent() {
		return $this->hasOne('App\Models\Categories','_id','i_parent_id')->first();
	}

}







