<?php

namespace App\Models;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Footer extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {
	use Authenticatable, Authorizable, CanResetPassword;
	public $timestamps = false;
	protected $collection = 'tbl_footer_menu';

    public $fillable = [
		'v_title',
		'i_page_id',
		'v_column',
		'e_status',
		'd_added',
		'd_modified',
	];

	public function hasPage() {
		return $this->hasOne('App\Models\Pages\Pages','_id','i_page_id')->first();
	}
	

}







