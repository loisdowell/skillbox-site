<?php

namespace App\Models\Courses;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class CoursesReviews extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;
	protected $collection = 'tbl_course_reviews';

    public $fillable = [
		'i_courses_id',
		'i_user_id',
		'i_seller_id',
		'l_comment',
		'f_rate_instruction',
		'f_rate_navigate',
		'f_rate_reccommend',
		'i_star',
		'l_answers',
		'e_status',
		'd_added',
		'd_modified'
	];
	
	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}

	public function hasSeller() {
		return $this->hasOne('App\Models\Users\Users','_id','i_seller_id')->first();
	}

}







