<?php

namespace App\Models\Courses;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class BuyerCourse extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_buyer_course';

    public $fillable = [
		
		'i_course_id',
		'i_user_id',
		'v_order_id',	
		'e_payment_type',
		'v_transcation_id',
		'e_payment_status',
		'e_status',
		'v_total_lectures',
		'v_complete_lectures',
		'v_complete_ids',
		'd_added',
		'd_modified'

	];

	public function hasCourse() {
		return $this->hasOne('App\Models\Courses\Courses','_id','i_course_id')->first();
	}

	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}


}







