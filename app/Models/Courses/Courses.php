<?php

namespace App\Models\Courses;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Courses extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;
	protected $collection = 'tbl_courses';

    public $fillable = [
			'i_user_id',
			'v_title',
			'v_sub_title',
			'v_author_name',
			'i_language_id',
			'i_level_id',
			'i_duration',
			'i_category_id',
			'v_requirement',
			'l_description',
			'v_instructor_image',
			'v_course_thumbnail',
			'l_instructor_details',
			'section',
			'settings',
			'messages',
			'l_video',
			'f_price',
			'v_courses_section',
			'v_search_tags',
			'e_sponsor',
			'e_sponsor_status',
			'e_qa_enabled',
			'e_reviews_enabled',
			'e_announcements_enabled',
			'review',
			'i_total_avg_review',
			'i_total_review',
			'i_impression',
			'i_impression_perday',
			'i_impression_day',
			'i_contact',
			'i_clicks',
			'i_crone',
			'e_status',
			'd_signup_date',
			'd_added',
			'd_modified',
			'i_delete',
	];

	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}

	public function hasLevel() {
		return $this->hasOne('App\Models\Courses\CoursesLevel','_id','i_level_id')->first();
	}

	public function hasCategory() {
		return $this->hasOne('App\Models\Courses\CoursesCategory','_id','i_category_id')->first();
	}

}







