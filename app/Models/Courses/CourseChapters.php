<?php

namespace App\Models\Courses;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class CourseChapters extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_course_chapters';

    public $fillable = [
							'i_course_id',
							'i_parent_id',
							'v_title',
							'l_description',
							'l_documents',
							'l_video',
							'e_status',
							'd_added',
							'd_modified'
						];

	public function hasCourse() {
		return $this->hasOne('App\Models\Courses\Courses','_id','i_course_id')->first();
	}
	public function hasParent() {
		return $this->hasOne('App\Models\Courses\CourseChapters','_id','i_parent_id')->first();
	}

}







