<?php

namespace App\Models\Courses;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class CourseComments extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_course_comments';

    public $fillable = [
							'i_course_id',
							'i_title_id',
							'v_type',
							'l_comment',
							'd_added',
							'd_modified',
							'e_status'
						];

	public function hasCourse() {
		return $this->hasOne('App\Models\Courses\Courses','_id','i_course_id')->first();
	}

	public function hasTitle() {
		return $this->hasOne('App\Models\Courses\CourseFaqs','_id','i_title_id')->first();
	}

}







