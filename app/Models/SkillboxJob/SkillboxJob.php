<?php

namespace App\Models\SkillboxJob;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class SkillboxJob extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_skillbox_job';

    public $fillable = [
			'i_category_id',
			'v_title',
			'v_country',
			'v_city',
			'v_job_time',
			'v_short_description',
			'l_description',
			'e_status',
			'd_added',
			'd_modified',
			
	];

}







