<?php

namespace App\Models\SkillboxJob;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class SkillboxJobApply extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_skillbox_job_apply';

    public $fillable = [
			'v_fname',
			'v_lname',
			'v_email',
			'v_phone_number',
			'v_cover_letter',
			'v_attach_cv',
			'd_added',
			'd_modified',
			
	];

}







