<?php

namespace App\Models;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Plan extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_plan';

    public $fillable = [
				'v_name',
				'l_description',
				'f_monthly_price',
				'f_monthly_dis_price',
				'f_yearly_price',
				'f_yearly_dis_price',
				'l_bullet',
				'l_addon',
				'v_subtitle',
				'd_added',
				'e_status'
			];

}







