<?php

namespace App\Models\Support;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Support extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_support';

    public $fillable = [
		'i_ticket_num',
		'i_parent_id',
		'i_admin_id',
		'i_user_id',
		'i_user_type',
		'e_user_type',
		'e_category',
		'v_document',
		'e_type',
		'e_query',
		'l_question',
		'e_status',
		'e_view_status',
		'e_view',
		'i_touser_id',
		'e_priority',
		'd_added',
		'd_modified',
	];

	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}
	
	public function hasQuery() {
		return $this->hasOne('App\Models\Support\SupportQuery','_id','e_query')->first();
	}

	public function hasAdmin() {
		return $this->hasOne('App\Models\Admin','_id','i_user_id')->first();
	}
	public function hasAdminAssign() {
		return $this->hasOne('App\Models\Admin','_id','i_admin_id')->first();
	}

}







