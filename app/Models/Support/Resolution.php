<?php

namespace App\Models\Support;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Resolution extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_resolution';

    public $fillable = [
		'i_order_id',
		'i_user_id',
		'i_seller_id',
		'i_issue_id',
		'v_subject',
		'l_message',
		'l_reply',
		'd_reply_date',
		'e_status',
		'i_delete',
		'd_added',
		'd_modified',
	];

	public function hasUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_user_id')->first();
	}

	public function hasSeller() {
		return $this->hasOne('App\Models\Users\Users','_id','i_seller_id')->first();
	}

	public function hasOrder() {
		return $this->hasOne('App\Models\Users\Order','_id','i_order_id')->first();
	}

}







