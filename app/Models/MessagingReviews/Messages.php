<?php

namespace App\Models\MessagingReviews;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Messages extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_messages';

    public $fillable = [
							'i_parent_id',
							'i_from_user_id',
							'i_to_user_id',
							'v_subject',
							'l_message',
							'd_added',
							'd_modified',
							'e_status',
							'e_view_status'
						];

	public function hasFromUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_from_user_id')->first();
	}

	public function hasToUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_to_user_id')->first();
	}


}







