<?php

namespace App\Models\MessagingReviews;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Reviews extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_reviews';

    public $fillable = [
							'e_type',
							'i_from_user_id',
							'i_ref_id',
							'i_stars',
							'v_title',
							'l_message',
							'd_added',
							'e_status',
							'e_view_status'
						];

	public function hasFromUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_from_user_id')->first();
	}

	public function hasReferenceUser() {
		return $this->hasOne('App\Models\Users\Users','_id','i_ref_id')->first();
	}

	public function hasJob() {
		return $this->hasOne('App\Models\Users\Jobs','_id','i_ref_id')->first();
	}

	public function hasCourse() {
		return $this->hasOne('App\Models\Courses\Courses','_id','i_ref_id')->first();
	}

}







