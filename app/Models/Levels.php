<?php

namespace App\Models;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Levels extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;
	protected $collection = 'tbl_levels';
    
    public $fillable = [
			'v_title',
			'i_level',
			'v_icon',
			'v_text_public',
			'v_text_seller',
			'd_added',
			'd_modified'
	];
	
	public function hasSkill() {
		return $this->hasOne('App\Models\Skills','_id','i_skills')->first();
	}

}







