<?php

namespace App\Models\Pages;

use Moloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Pages extends Moloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;

	protected $collection = 'tbl_pages';

    public $fillable = [	'i_parent_id',
							'v_title',
							'v_key',
							'v_banner_text',
							'v_page_icon',
							'v_banner_img',
							'l_short_description',
							'l_description',
							'v_meta_title',
							'v_meta_keywords',
							'l_meta_description',
							'd_added',
							'd_modified',
							'e_status'
						];

}







