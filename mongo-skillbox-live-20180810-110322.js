
/** tbl_admin indexes **/
db.getCollection("tbl_admin").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_applied_jobs indexes **/
db.getCollection("tbl_applied_jobs").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_blocked_ipaddress indexes **/
db.getCollection("tbl_blocked_ipaddress").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_blogs indexes **/
db.getCollection("tbl_blogs").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_buyer_course indexes **/
db.getCollection("tbl_buyer_course").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_buyer_review indexes **/
db.getCollection("tbl_buyer_review").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_categories indexes **/
db.getCollection("tbl_categories").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_city indexes **/
db.getCollection("tbl_city").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_company_ethos indexes **/
db.getCollection("tbl_company_ethos").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_country indexes **/
db.getCollection("tbl_country").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_course_announcements indexes **/
db.getCollection("tbl_course_announcements").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_course_category indexes **/
db.getCollection("tbl_course_category").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_course_comments indexes **/
db.getCollection("tbl_course_comments").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_course_faqs indexes **/
db.getCollection("tbl_course_faqs").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_course_language indexes **/
db.getCollection("tbl_course_language").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_course_level indexes **/
db.getCollection("tbl_course_level").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_course_qa indexes **/
db.getCollection("tbl_course_qa").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_course_reviews indexes **/
db.getCollection("tbl_course_reviews").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_courses indexes **/
db.getCollection("tbl_courses").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_email_template indexes **/
db.getCollection("tbl_email_template").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_email_template_footer indexes **/
db.getCollection("tbl_email_template_footer").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_email_template_header indexes **/
db.getCollection("tbl_email_template_header").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_faqs indexes **/
db.getCollection("tbl_faqs").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_feedback indexes **/
db.getCollection("tbl_feedback").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_jobs indexes **/
db.getCollection("tbl_jobs").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_levels indexes **/
db.getCollection("tbl_levels").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_meta_fields_banner indexes **/
db.getCollection("tbl_meta_fields_banner").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_newsletter indexes **/
db.getCollection("tbl_newsletter").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_order indexes **/
db.getCollection("tbl_order").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_order_number indexes **/
db.getCollection("tbl_order_number").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_order_requirements indexes **/
db.getCollection("tbl_order_requirements").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_packages indexes **/
db.getCollection("tbl_packages").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_pages indexes **/
db.getCollection("tbl_pages").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_plan indexes **/
db.getCollection("tbl_plan").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_price_filter indexes **/
db.getCollection("tbl_price_filter").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_resolution indexes **/
db.getCollection("tbl_resolution").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_seller_profile indexes **/
db.getCollection("tbl_seller_profile").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_seller_review indexes **/
db.getCollection("tbl_seller_review").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_serches indexes **/
db.getCollection("tbl_serches").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_shortlisted_course indexes **/
db.getCollection("tbl_shortlisted_course").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_shortlisted_jobs indexes **/
db.getCollection("tbl_shortlisted_jobs").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_shortlisted_skills indexes **/
db.getCollection("tbl_shortlisted_skills").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_sitesettings indexes **/
db.getCollection("tbl_sitesettings").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_skillbox_job indexes **/
db.getCollection("tbl_skillbox_job").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_skillbox_job_apply indexes **/
db.getCollection("tbl_skillbox_job_apply").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_skillbox_job_category indexes **/
db.getCollection("tbl_skillbox_job_category").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_skills indexes **/
db.getCollection("tbl_skills").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_sponsor_ad_fees indexes **/
db.getCollection("tbl_sponsor_ad_fees").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_state indexes **/
db.getCollection("tbl_state").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_support indexes **/
db.getCollection("tbl_support").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_support_category indexes **/
db.getCollection("tbl_support_category").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_users indexes **/
db.getCollection("tbl_users").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tbl_admin records **/
db.getCollection("tbl_admin").insert({
  "_id": ObjectId("5b5a104e1b544d0d7b387a73"),
  "v_name": "Altagency",
  "v_email": "hello@altagency.co.uk",
  "password": "$2y$10$DotQsUXuEZC0SDYUhs1K8uyG942W3V7sV3bsOjOzOsHcCi4vfWcA2",
  "e_status": "active",
  "remember_token": "Uh3PEVmLHQjKDMmrqK08B8lz5yhlvfjbA7xLkrMKnMfjHXSHkFji0Rhxo2co",
  "v_profile": "profile-image-54441516195164.jpg"
});

/** tbl_applied_jobs records **/
db.getCollection("tbl_applied_jobs").insert({
  "_id": ObjectId("5b28ee4876fbae48a0579b63"),
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-06-19 11:51:36",
  "i_applied_id": "5b16810576fbae669b298c93"
});
db.getCollection("tbl_applied_jobs").insert({
  "_id": ObjectId("5b561009516b462ef00036a8"),
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-07-23 17:27:37",
  "i_applied_id": "5b560495516b462cb0001aac"
});
db.getCollection("tbl_applied_jobs").insert({
  "_id": ObjectId("5b5f4aeb1b544d1234370fe2"),
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-07-30 17:29:15",
  "i_applied_id": "5b166bcf76fbae3b55286fe3"
});

/** tbl_blocked_ipaddress records **/

/** tbl_blogs records **/
db.getCollection("tbl_blogs").insert({
  "_id": ObjectId("5a60382dd3e81291133c9869"),
  "v_name": "Patio maintenance that actually works",
  "l_short_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
  "e_status": "active",
  "l_long_description": "<h2 style=\"margin: 0px 0px 10px; padding: 0px; font-weight: 400; line-height: 24px; font-family: DauphinPlain; font-size: 24px; background-color: rgb(255, 255, 255);\">What is Lorem Ipsum?</h2>\r\n\r\n<p background-color:=\"\" font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: \"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2 style=\"margin: 0px 0px 10px; padding: 0px; font-weight: 400; line-height: 24px; font-family: DauphinPlain; font-size: 24px; background-color: rgb(255, 255, 255);\">What is Lorem Ipsum?</h2>\r\n\r\n<p background-color:=\"\" font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: \"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n",
  "v_image": "categories/categories-1531501083.jpg",
  "i_user_id": "5ad9d97c76fbae0a773e7672",
  "e_user_type": "admin",
  "d_added": "2018-01-18 06:01:17",
  "d_modified": "2018-07-13 04:58:04"
});
db.getCollection("tbl_blogs").insert({
  "_id": ObjectId("5a603858d3e81293133c9869"),
  "v_name": "Patio maintenance that actually works\t",
  "l_short_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
  "e_status": "active",
  "l_long_description": "<h2 style=\"margin: 0px 0px 10px; padding: 0px; font-weight: 400; line-height: 24px; font-family: DauphinPlain; font-size: 24px; background-color: rgb(255, 255, 255);\">What is Lorem Ipsum?</h2>\r\n\r\n<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n",
  "v_image": "blog-image-31961516255320.png",
  "i_user_id": "5a5ee53d579be102b00941ba",
  "e_user_type": "admin",
  "d_added": "2018-01-18 06:02:00"
});

/** tbl_buyer_course records **/
db.getCollection("tbl_buyer_course").insert({
  "_id": ObjectId("5b17866f76fbae1c5b6a9a14"),
  "i_course_id": "5b16610b76fbae3d393c9a03",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b17866f76fbae1c5b6a9a13",
  "e_payment_type": "paypal",
  "v_transcation_id": "76A855299X9030816",
  "e_payment_status": "pending",
  "e_status": "completed",
  "v_complete_lectures": NumberInt(5),
  "v_complete_ids": [
    "5b1661bc76fbae3c3e66bd34-videoplayback.mp4",
    "5b1661bc76fbae3c3e66bd34-section_doc-1528193477.js",
    "5b1661bc76fbae3c3e66bd34-section_video-1528193468.mp4",
    "5b1661c776fbae3c3e66bd35-section_video-1528193479.mp4",
    "5b1661d276fbae3c3e66bd36-section_video-1528193490.mp4"
  ],
  "d_added": "2018-06-06 06:59:59",
  "d_modified": "2018-07-03 10:23:45",
  "v_total_lectures": NumberInt(3)
});
db.getCollection("tbl_buyer_course").insert({
  "_id": ObjectId("5b179b2676fbae1c6643b025"),
  "i_course_id": "5b1674f576fbae5bca3eb652",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b179b2676fbae1c6643b024",
  "e_payment_type": "paypal",
  "v_transcation_id": "7G82718904384531T",
  "e_payment_status": "pending",
  "e_status": "completed",
  "v_total_lectures": NumberInt(1),
  "v_complete_lectures": NumberInt(1),
  "v_complete_ids": [
    "5b16758576fbae5bc93da212-section_video-1528198533.mp4"
  ],
  "d_added": "2018-06-06 08:28:22",
  "d_modified": "2018-07-12 16:14:21"
});
db.getCollection("tbl_buyer_course").insert({
  "_id": ObjectId("5b17cd9b76fbae1c5f654af4"),
  "i_course_id": "5b16621d76fbae3c8e6c7994",
  "i_user_id": "5b1666cd76fbae3d393c9a04",
  "v_order_id": "5b17cd9b76fbae1c5f654af3",
  "e_payment_type": "paypal",
  "v_transcation_id": "65H581971J447422A",
  "e_payment_status": "pending",
  "e_status": "not_started",
  "v_total_lectures": NumberInt(2),
  "v_complete_lectures": "0",
  "v_complete_ids": [
    
  ],
  "d_added": "2018-06-06 12:03:39",
  "d_modified": "2018-06-06 12:03:39"
});
db.getCollection("tbl_buyer_course").insert({
  "_id": ObjectId("5b3a211a516b460a68003e6d"),
  "i_course_id": "5b3a1fdb516b460a68003e69",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b3a211a516b460a68003e6c",
  "e_payment_type": "paypal",
  "v_transcation_id": "05V56303YG5614044",
  "e_payment_status": "success",
  "e_status": "completed",
  "v_total_lectures": NumberInt(3),
  "v_complete_lectures": NumberInt(5),
  "v_complete_ids": [
    "5b3a205d516b460a68003e6a-section_video-1530536029.mp4",
    "5b3a205d516b460a68003e6a-section_video-1530536035.mp4",
    "5b3a205d516b460a68003e6a-section_doc-1530536041.pdf",
    "5b3a205d516b460a68003e6a-videoplayback.mp4",
    "5b3a205d516b460a68003e6a-testing.mp4"
  ],
  "d_added": "2018-07-02 12:56:58",
  "d_modified": "2018-08-08 15:56:30"
});
db.getCollection("tbl_buyer_course").insert({
  "_id": ObjectId("5b478c72516b4620200031ad"),
  "i_course_id": "5b16703f76fbae487952a662",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b478c72516b4620200031ac",
  "e_payment_type": "paypal",
  "v_transcation_id": "34A199467J148864D",
  "e_payment_status": "pending",
  "e_status": "inprogress",
  "v_total_lectures": NumberInt(4),
  "v_complete_lectures": NumberInt(2),
  "v_complete_ids": [
    "5b1670b376fbae3d3f03e805-section_video-1528197299.mp4",
    "5b1670b376fbae3d3f03e805-section_doc-1528197301.js"
  ],
  "d_added": "2018-07-12 17:14:26",
  "d_modified": "2018-07-12 17:22:56"
});

/** tbl_buyer_review records **/
db.getCollection("tbl_buyer_review").insert({
  "_id": ObjectId("5b3b589b516b463248002fce"),
  "i_work_star": "4",
  "i_support_star": "5",
  "i_opportunities_star": "3",
  "i_work_again_star": "4",
  "i_job_id": "5b16810576fbae669b298c93",
  "l_comment": "Enter Your Review",
  "i_avg_star": "13.0",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_buyer_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-07-03 11:06:03",
  "d_modified": "2018-07-03 11:06:03"
});
db.getCollection("tbl_buyer_review").insert({
  "_id": ObjectId("5b5f49fa1b544d122b0951b2"),
  "i_work_star": "5",
  "i_support_star": "5",
  "i_opportunities_star": "5",
  "i_work_again_star": "5",
  "i_job_id": "5b560495516b462cb0001aac",
  "l_comment": "This is test review",
  "i_avg_star": "16.3",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_buyer_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-07-30 17:25:14",
  "d_modified": "2018-07-30 17:25:14"
});
db.getCollection("tbl_buyer_review").insert({
  "_id": ObjectId("5b5f4b231b544d57a2399ea3"),
  "i_work_star": "5",
  "i_support_star": "5",
  "i_opportunities_star": "5",
  "i_work_again_star": "5",
  "i_job_id": "5b166bcf76fbae3b55286fe3",
  "l_comment": "This is test review",
  "i_avg_star": "16.3",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_buyer_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-07-30 17:30:11",
  "d_modified": "2018-07-30 17:30:11"
});

/** tbl_categories records **/
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a606cecd3e81206403c9869"),
  "v_name": "Home & Garden",
  "e_status": "active",
  "v_type": "inperson",
  "v_image": "categories/categories-1528367931.png",
  "d_added": "2018-01-18 09:46:20",
  "d_modified": "2018-01-18 09:46:20"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a606e4ed3e81206403c986a"),
  "v_name": "Cleaning & Ironing",
  "e_status": "active",
  "v_type": "inperson",
  "v_image": "categories/categories-1527312317.png",
  "d_added": "2018-01-18 09:52:14",
  "d_modified": "2018-01-18 09:52:14"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a606efbd3e81219263c986f"),
  "v_name": "Creative Art & Design",
  "e_status": "active",
  "v_type": "online",
  "v_image": "categories/categories-1527333719.png",
  "d_added": "2018-01-18 09:55:07",
  "d_modified": "2018-01-18 09:55:07"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a607009d3e812063f3c9869"),
  "v_name": "Marketing & Strategy",
  "e_status": "active",
  "v_type": "online",
  "v_image": "categories/categories-1528365647.png",
  "d_added": "2018-01-18 09:59:37",
  "d_modified": "2018-01-18 09:59:37"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a607698d3e812302b3c986b"),
  "v_name": "Bussiness & Educational",
  "e_status": "active",
  "v_type": "online",
  "v_image": "categories/categories-1527312266.png",
  "d_added": "2018-01-18 10:27:36",
  "d_modified": "2018-01-18 10:27:36"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d46fd3e81240363c986a"),
  "v_name": "Valeting & Repairs",
  "e_status": "active",
  "v_type": "inperson",
  "d_added": "2018-01-22 12:09:19",
  "d_modified": "2018-01-22 12:09:19",
  "v_image": "categories/categories-1528365685.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d483d3e8120c333c986a"),
  "v_name": "Personnel Assistant",
  "e_status": "active",
  "v_type": "inperson",
  "d_added": "2018-01-22 12:09:39",
  "d_modified": "2018-01-22 12:09:39",
  "v_image": "categories/categories-1528367992.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d494d3e812a4253c986a"),
  "v_name": "Technical Support",
  "e_status": "active",
  "v_type": "inperson",
  "d_added": "2018-01-22 12:09:56",
  "d_modified": "2018-01-22 12:09:56",
  "v_image": "categories/categories-1528365710.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d4a0d3e81255333c9869"),
  "v_name": "Home Care",
  "e_status": "active",
  "v_type": "inperson",
  "d_added": "2018-01-22 12:10:08",
  "d_modified": "2018-01-22 12:10:08",
  "v_image": "categories/categories-1528365725.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d4b3d3e812a0323c9869"),
  "v_name": "Animal Care",
  "e_status": "active",
  "v_type": "inperson",
  "d_added": "2018-01-22 12:10:27",
  "d_modified": "2018-01-22 12:10:27",
  "v_image": "categories/categories-1528365739.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d587d3e8125e323c986c"),
  "v_name": "Hair & Beauty",
  "e_status": "active",
  "v_type": "inperson",
  "d_added": "2018-01-22 12:13:59",
  "d_modified": "2018-01-22 12:13:59",
  "v_image": "categories/categories-1528367663.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d5add3e812a0323c986b"),
  "v_name": "Design & Creativity",
  "e_status": "active",
  "v_type": "online",
  "d_added": "2018-01-22 12:14:37",
  "d_modified": "2018-01-22 12:14:37",
  "v_image": "categories/categories-1528367375.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d5c1d3e81219333c9869"),
  "v_name": "Writing & Proofreading",
  "e_status": "active",
  "v_type": "online",
  "d_added": "2018-01-22 12:14:57",
  "d_modified": "2018-01-22 12:14:57",
  "v_image": "categories/categories-1528367381.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d5ced3e812a4253c986c"),
  "v_name": "Translation",
  "e_status": "active",
  "v_type": "online",
  "d_added": "2018-01-22 12:15:10",
  "d_modified": "2018-01-22 12:15:10",
  "v_image": "categories/categories-1528368040.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d5d9d3e8120c333c986e"),
  "v_name": "Legal",
  "e_status": "active",
  "v_type": "online",
  "d_added": "2018-01-22 12:15:21",
  "d_modified": "2018-01-22 12:15:21",
  "v_image": "categories/categories-1528367575.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d5e2d3e8120c333c986f"),
  "v_name": "Admin & Research",
  "e_status": "active",
  "v_type": "online",
  "d_added": "2018-01-22 12:15:30",
  "d_modified": "2018-01-22 12:15:30",
  "v_image": "categories/categories-1528367403.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d5edd3e81240363c986c"),
  "v_name": "Customer Support",
  "e_status": "active",
  "v_type": "online",
  "d_added": "2018-01-22 12:15:41",
  "d_modified": "2018-01-22 12:15:41",
  "v_image": "categories/categories-1528367568.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d605d3e81255333c986b"),
  "v_name": "Accounting & HR",
  "e_status": "active",
  "v_type": "online",
  "d_added": "2018-01-22 12:16:05",
  "d_modified": "2018-01-22 12:16:05",
  "v_image": "categories/categories-1528367562.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d611d3e812a0323c986c"),
  "v_name": "Business & Educational Management",
  "e_status": "active",
  "v_type": "online",
  "d_added": "2018-01-22 12:16:17",
  "d_modified": "2018-01-22 12:16:17",
  "v_image": "categories/categories-1528367412.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5a65d63cd3e81237303c986b"),
  "v_name": "Other Category",
  "e_status": "active",
  "v_type": "online",
  "d_added": "2018-01-22 12:17:00",
  "d_modified": "2018-01-22 12:17:00",
  "v_image": "categories/categories-1528367554.png"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5b5b4cd31b544d0bc7058228"),
  "v_name": "Removals & Assembly",
  "e_status": "active",
  "v_type": "inperson",
  "v_image": "categories/categories-1532710096.png",
  "d_added": "2018-07-27 04:48:19",
  "d_modified": "2018-07-27 04:48:19"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5b5b4cfe1b544d05312b0797"),
  "v_name": "Catering",
  "e_status": "active",
  "v_type": "inperson",
  "v_image": "categories/categories-1532710140.png",
  "d_added": "2018-07-27 04:49:02",
  "d_modified": "2018-07-27 04:49:02"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5b5b4d1e1b544d0c0715b826"),
  "v_name": "Transport & Hire",
  "e_status": "active",
  "v_type": "inperson",
  "v_image": "categories/categories-1532710172.png",
  "d_added": "2018-07-27 04:49:34",
  "d_modified": "2018-07-27 04:49:34"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5b5b4d461b544d0cc469e8d4"),
  "v_name": "Physio & Health",
  "e_status": "active",
  "v_type": "inperson",
  "v_image": "categories/categories-1532710211.png",
  "d_added": "2018-07-27 04:50:14",
  "d_modified": "2018-07-27 04:50:14"
});
db.getCollection("tbl_categories").insert({
  "_id": ObjectId("5b5b4d631b544d0c0b0fd685"),
  "v_name": "Buddy Up",
  "e_status": "active",
  "v_type": "inperson",
  "v_image": "categories/categories-1532710240.png",
  "d_added": "2018-07-27 04:50:43",
  "d_modified": "2018-07-27 04:50:43"
});

/** tbl_city records **/
db.getCollection("tbl_city").insert({
  "_id": ObjectId("5a609abdd3e8120f483c9869"),
  "v_title": "Meerut",
  "i_state_id": "5a609a88d3e812704a3c9869",
  "e_status": "active",
  "d_added": "2018-01-18 01:01:49",
  "d_modified": "2018-01-18 01:01:49"
});
db.getCollection("tbl_city").insert({
  "_id": ObjectId("5a609accd3e812062b3c986b"),
  "v_title": "Ahmedabad",
  "i_state_id": "5a609a7bd3e81237433c9869",
  "e_status": "active",
  "d_added": "2018-01-18 01:02:04",
  "d_modified": "2018-01-18 01:02:04"
});
db.getCollection("tbl_city").insert({
  "_id": ObjectId("5a609afbd3e812f51e3c986b"),
  "v_title": "Vasai-Virar",
  "i_state_id": "5a609a90d3e812b94b3c986a",
  "e_status": "active",
  "d_added": "2018-01-18 01:02:51",
  "d_modified": "2018-01-18 01:02:51"
});

/** tbl_company_ethos records **/
db.getCollection("tbl_company_ethos").insert({
  "_id": ObjectId("5b0e497076fbae0e57243ef2"),
  "v_title": "PURPOSE",
  "v_description": "Saving our customers stress, time and money by providing a quick link to the skilled community",
  "i_order": "1",
  "e_status": "active",
  "v_image": "company-ethos/company-ethos-1527663429.png",
  "d_added": "2018-05-30 06:49:20",
  "d_modified": "2018-05-30 06:49:51"
});
db.getCollection("tbl_company_ethos").insert({
  "_id": ObjectId("5b0e4ba776fbae0e5c1002a3"),
  "v_title": "PURPOSE TAL",
  "v_description": "Saving our customers stress, time and money by providing a quick link to the skilled community",
  "i_order": "2",
  "e_status": "active",
  "v_image": "company-ethos/company-ethos-1527663526.png",
  "d_added": "2018-05-30 06:58:47",
  "d_modified": "2018-05-30 06:58:47"
});
db.getCollection("tbl_company_ethos").insert({
  "_id": ObjectId("5b0e4bdd76fbae223d531b13"),
  "v_title": "PURPOSE HOME",
  "v_description": "Saving our customers stress, time and money by providing a quick link to the skilled community",
  "i_order": "3",
  "e_status": "active",
  "v_image": "company-ethos/company-ethos-1527663580.png",
  "d_added": "2018-05-30 06:59:41",
  "d_modified": "2018-05-30 06:59:41"
});
db.getCollection("tbl_company_ethos").insert({
  "_id": ObjectId("5b0e4c0e76fbae0c8d4892b3"),
  "v_title": "PURPOSE VISION",
  "v_description": "Saving our customers stress, time and money by providing a quick link to the skilled community",
  "i_order": "4",
  "e_status": "active",
  "v_image": "company-ethos/company-ethos-1527663628.png",
  "d_added": "2018-05-30 07:00:30",
  "d_modified": "2018-05-30 07:00:30"
});

/** tbl_country records **/
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5a6099f0d3e812f51e3c986a"),
  "v_title": "India",
  "e_status": "active",
  "d_added": "2018-01-18 12:58:24",
  "d_modified": "2018-01-18 12:58:24"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5a609a68d3e81234473c9869"),
  "v_title": "China",
  "e_status": "active",
  "d_added": "2018-01-18 01:00:24",
  "d_modified": "2018-01-18 01:00:24"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28cc376fbae0a9f50e3a2"),
  "v_title": "Afghanistan",
  "e_status": "active",
  "d_added": "2018-05-09 05:53:07",
  "d_modified": "2018-05-09 05:53:07"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28d7576fbae0a9f50e3a3"),
  "v_title": "\tAustria",
  "e_status": "active",
  "d_added": "2018-05-09 05:56:05",
  "d_modified": "2018-05-09 05:56:05"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28da176fbae0b274ad462"),
  "v_title": "\tBelgium",
  "e_status": "active",
  "d_added": "2018-05-09 05:56:49",
  "d_modified": "2018-05-09 05:56:49"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28dd876fbae1c9b206742"),
  "v_title": "Canada",
  "e_status": "active",
  "d_added": "2018-05-09 05:57:44",
  "d_modified": "2018-05-09 05:57:44"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28df976fbae0a9e637172"),
  "v_title": "Denmark",
  "e_status": "active",
  "d_added": "2018-05-09 05:58:17",
  "d_modified": "2018-05-09 05:58:17"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28e1076fbae0e7f0236c2"),
  "v_title": "Dominica",
  "e_status": "active",
  "d_added": "2018-05-09 05:58:40",
  "d_modified": "2018-05-09 05:58:40"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28e3776fbae1c9b206743"),
  "v_title": "Egypt",
  "e_status": "active",
  "d_added": "2018-05-09 05:59:19",
  "d_modified": "2018-05-09 05:59:19"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28e9176fbae1c9b206744"),
  "v_title": "Germany",
  "e_status": "active",
  "d_added": "2018-05-09 06:00:49",
  "d_modified": "2018-05-09 06:00:49"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28ec276fbae0a9c34d1a2"),
  "v_title": "Greece",
  "e_status": "active",
  "d_added": "2018-05-09 06:01:38",
  "d_modified": "2018-05-09 06:01:38"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28ee376fbae1ce44d4e12"),
  "v_title": "\tGreenland",
  "e_status": "active",
  "d_added": "2018-05-09 06:02:11",
  "d_modified": "2018-05-09 06:02:11"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28f0176fbae1c9d53b252"),
  "v_title": "Hong Kong",
  "e_status": "active",
  "d_added": "2018-05-09 06:02:41",
  "d_modified": "2018-05-09 06:02:41"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28f1576fbae1c9b206745"),
  "v_title": "\tIceland",
  "e_status": "active",
  "d_added": "2018-05-09 06:03:01",
  "d_modified": "2018-05-09 06:03:01"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28f3b76fbae0a9e637174"),
  "v_title": "Indonesia",
  "e_status": "active",
  "d_added": "2018-05-09 06:03:39",
  "d_modified": "2018-05-09 06:03:39"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28f5b76fbae0b274ad463"),
  "v_title": "\tIraq",
  "e_status": "active",
  "d_added": "2018-05-09 06:04:11",
  "d_modified": "2018-05-09 06:04:11"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28f7476fbae0e7f0236c3"),
  "v_title": "\tItaly",
  "e_status": "active",
  "d_added": "2018-05-09 06:04:36",
  "d_modified": "2018-05-09 06:04:36"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28f8776fbae0a9c34d1a3"),
  "v_title": "Japan",
  "e_status": "active",
  "d_added": "2018-05-09 06:04:55",
  "d_modified": "2018-05-09 06:04:55"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af28fd876fbae0a9e637175"),
  "v_title": "France",
  "e_status": "active",
  "d_added": "2018-05-09 06:06:16",
  "d_modified": "2018-05-09 06:06:16"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2900b76fbae0a9f50e3a4"),
  "v_title": "\tKenya",
  "e_status": "active",
  "d_added": "2018-05-09 06:07:07",
  "d_modified": "2018-05-09 06:07:07"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2901e76fbae0a9c34d1a4"),
  "v_title": "Korea, Democratic People's Rep. (North Korea)",
  "e_status": "active",
  "d_added": "2018-05-09 06:07:26",
  "d_modified": "2018-05-09 06:07:26"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2903876fbae0ce54f8e52"),
  "v_title": "Korea, Republic of (South Korea)",
  "e_status": "active",
  "d_added": "2018-05-09 06:07:52",
  "d_modified": "2018-05-09 06:07:52"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2905376fbae0b274ad464"),
  "v_title": "\tLithuania",
  "e_status": "active",
  "d_added": "2018-05-09 06:08:19",
  "d_modified": "2018-05-09 06:08:19"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2906776fbae1ce44d4e13"),
  "v_title": "\tLuxembourg",
  "e_status": "active",
  "d_added": "2018-05-09 06:08:39",
  "d_modified": "2018-05-09 06:08:39"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2907e76fbae0a9d736262"),
  "v_title": "\tMalaysia",
  "e_status": "active",
  "d_added": "2018-05-09 06:09:02",
  "d_modified": "2018-05-09 06:09:02"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2909476fbae1c9b206746"),
  "v_title": "Macau",
  "e_status": "active",
  "d_added": "2018-05-09 06:09:24",
  "d_modified": "2018-05-09 06:09:24"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af290b176fbae1ce44d4e14"),
  "v_title": "Maldives",
  "e_status": "active",
  "d_added": "2018-05-09 06:09:53",
  "d_modified": "2018-05-09 06:09:53"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af290cf76fbae0b274ad465"),
  "v_title": "\tMexico",
  "e_status": "active",
  "d_added": "2018-05-09 06:10:23",
  "d_modified": "2018-05-09 06:10:23"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af290e476fbae1c9d53b253"),
  "v_title": "Monaco",
  "e_status": "active",
  "d_added": "2018-05-09 06:10:44",
  "d_modified": "2018-05-09 06:10:44"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af290fa76fbae0a9d736263"),
  "v_title": "Myanmar, Burma",
  "e_status": "active",
  "d_added": "2018-05-09 06:11:06",
  "d_modified": "2018-05-09 06:11:06"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2911576fbae1c9b206747"),
  "v_title": "\tMozambique",
  "e_status": "active",
  "d_added": "2018-05-09 06:11:33",
  "d_modified": "2018-05-09 06:11:33"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2913476fbae0e7f0236c4"),
  "v_title": "Nepal",
  "e_status": "active",
  "d_added": "2018-05-09 06:12:04",
  "d_modified": "2018-05-09 06:12:04"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2915276fbae0a9f50e3a5"),
  "v_title": "Netherlands",
  "e_status": "active",
  "d_added": "2018-05-09 06:12:34",
  "d_modified": "2018-05-09 06:12:34"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2916c76fbae0ce54f8e53"),
  "v_title": "Netherlands Antilles",
  "e_status": "active",
  "d_added": "2018-05-09 06:13:00",
  "d_modified": "2018-05-09 06:13:00"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2918076fbae1ce44d4e15"),
  "v_title": "\tNew Zealand",
  "e_status": "active",
  "d_added": "2018-05-09 06:13:20",
  "d_modified": "2018-05-09 06:13:20"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2919676fbae0a9f50e3a6"),
  "v_title": "\tNorthern Mariana Islands",
  "e_status": "active",
  "d_added": "2018-05-09 06:13:42",
  "d_modified": "2018-05-09 06:13:42"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af291a776fbae0b274ad466"),
  "v_title": "\tNorway",
  "e_status": "active",
  "d_added": "2018-05-09 06:13:59",
  "d_modified": "2018-05-09 06:13:59"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af291bb76fbae0a9e637176"),
  "v_title": "Oman",
  "e_status": "active",
  "d_added": "2018-05-09 06:14:19",
  "d_modified": "2018-05-09 06:14:19"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af291d476fbae1ce44d4e16"),
  "v_title": "Paraguay",
  "e_status": "active",
  "d_added": "2018-05-09 06:14:44",
  "d_modified": "2018-05-09 06:14:44"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af291fd76fbae0b274ad467"),
  "v_title": "\tPeru",
  "e_status": "active",
  "d_added": "2018-05-09 06:15:25",
  "d_modified": "2018-05-09 06:15:25"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2921176fbae0a9c34d1a5"),
  "v_title": "Philippines",
  "e_status": "active",
  "d_added": "2018-05-09 06:15:45",
  "d_modified": "2018-05-09 06:15:45"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2922376fbae0a9f50e3a7"),
  "v_title": "Pitcairn Island",
  "e_status": "active",
  "d_added": "2018-05-09 06:16:03",
  "d_modified": "2018-05-09 06:16:03"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2923476fbae1ce44d4e17"),
  "v_title": "\tPoland",
  "e_status": "active",
  "d_added": "2018-05-09 06:16:20",
  "d_modified": "2018-05-09 06:16:20"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2924376fbae0e7f0236c5"),
  "v_title": "\tPortugal",
  "e_status": "active",
  "d_added": "2018-05-09 06:16:35",
  "d_modified": "2018-05-09 06:16:35"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2925d76fbae0a9f50e3a8"),
  "v_title": "\tQatar",
  "e_status": "active",
  "d_added": "2018-05-09 06:17:01",
  "d_modified": "2018-05-09 06:17:01"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2928576fbae1ce44d4e18"),
  "v_title": "Reunion Island",
  "e_status": "active",
  "d_added": "2018-05-09 06:17:41",
  "d_modified": "2018-05-09 06:17:41"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2929f76fbae0e7f0236c6"),
  "v_title": "Russian Federation",
  "e_status": "active",
  "d_added": "2018-05-09 06:18:07",
  "d_modified": "2018-05-09 06:18:07"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af292bb76fbae0a9d736264"),
  "v_title": "Rwanda",
  "e_status": "active",
  "d_added": "2018-05-09 06:18:35",
  "d_modified": "2018-05-09 06:18:35"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af292eb76fbae0ce54f8e54"),
  "v_title": "Saint Lucia",
  "e_status": "active",
  "d_added": "2018-05-09 06:19:23",
  "d_modified": "2018-05-09 06:19:23"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af292fb76fbae0a9c34d1a6"),
  "v_title": "San Marino",
  "e_status": "active",
  "d_added": "2018-05-09 06:19:39",
  "d_modified": "2018-05-09 06:19:39"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2930d76fbae0b274ad468"),
  "v_title": "Sao Tome and Principe",
  "e_status": "active",
  "d_added": "2018-05-09 06:19:57",
  "d_modified": "2018-05-09 06:19:57"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2931a76fbae0e7f0236c7"),
  "v_title": "Saudi Arabia",
  "e_status": "active",
  "d_added": "2018-05-09 06:20:10",
  "d_modified": "2018-05-09 06:20:10"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2932e76fbae0b274ad469"),
  "v_title": "Senegal",
  "e_status": "active",
  "d_added": "2018-05-09 06:20:30",
  "d_modified": "2018-05-09 06:20:30"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2934376fbae0ce54f8e55"),
  "v_title": "Singapore",
  "e_status": "active",
  "d_added": "2018-05-09 06:20:51",
  "d_modified": "2018-05-09 06:20:51"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2935676fbae0e7f0236c8"),
  "v_title": "\tSouth Africa",
  "e_status": "active",
  "d_added": "2018-05-09 06:21:10",
  "d_modified": "2018-05-09 06:21:10"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2936376fbae0a9c34d1a7"),
  "v_title": "\tSouth Sudan",
  "e_status": "active",
  "d_added": "2018-05-09 06:21:23",
  "d_modified": "2018-05-09 06:21:23"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2937476fbae0b274ad46a"),
  "v_title": "\tSpain",
  "e_status": "active",
  "d_added": "2018-05-09 06:21:40",
  "d_modified": "2018-05-09 06:21:40"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2938d76fbae0a9d736265"),
  "v_title": "Sri Lanka",
  "e_status": "active",
  "d_added": "2018-05-09 06:22:05",
  "d_modified": "2018-05-09 06:22:05"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af293a376fbae0a9f50e3a9"),
  "v_title": "\tSwaziland",
  "e_status": "active",
  "d_added": "2018-05-09 06:22:27",
  "d_modified": "2018-05-09 06:22:27"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af293b876fbae0a9c34d1a8"),
  "v_title": "\tSwitzerland",
  "e_status": "active",
  "d_added": "2018-05-09 06:22:48",
  "d_modified": "2018-05-09 06:22:48"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af293c776fbae1c9d53b254"),
  "v_title": "Sweden",
  "e_status": "active",
  "d_added": "2018-05-09 06:23:03",
  "d_modified": "2018-05-09 06:23:03"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af293db76fbae0a9d736266"),
  "v_title": "Thailand",
  "e_status": "active",
  "d_added": "2018-05-09 06:23:23",
  "d_modified": "2018-05-09 06:23:23"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af293e776fbae1ce44d4e19"),
  "v_title": "\tTibet",
  "e_status": "active",
  "d_added": "2018-05-09 06:23:35",
  "d_modified": "2018-05-09 06:23:35"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af293f376fbae0ce54f8e56"),
  "v_title": "Togo",
  "e_status": "active",
  "d_added": "2018-05-09 06:23:47",
  "d_modified": "2018-05-09 06:23:47"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2940176fbae1ce44d4e1a"),
  "v_title": "Tokelau",
  "e_status": "active",
  "d_added": "2018-05-09 06:24:01",
  "d_modified": "2018-05-09 06:24:01"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2942b76fbae0a9e637177"),
  "v_title": "Turkey",
  "e_status": "active",
  "d_added": "2018-05-09 06:24:43",
  "d_modified": "2018-05-09 06:24:43"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2944076fbae0a9c34d1a9"),
  "v_title": "Turks and Caicos Islands",
  "e_status": "active",
  "d_added": "2018-05-09 06:25:04",
  "d_modified": "2018-05-09 06:25:04"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2944f76fbae0e7f0236c9"),
  "v_title": "Tuvalu",
  "e_status": "active",
  "d_added": "2018-05-09 06:25:19",
  "d_modified": "2018-05-09 06:25:19"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2945c76fbae1c9d53b255"),
  "v_title": "\tUganda",
  "e_status": "active",
  "d_added": "2018-05-09 06:25:32",
  "d_modified": "2018-05-09 06:25:32"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2946e76fbae1c9b206748"),
  "v_title": "\tUkraine",
  "e_status": "active",
  "d_added": "2018-05-09 06:25:50",
  "d_modified": "2018-05-09 06:25:50"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2948576fbae0e7f0236ca"),
  "v_title": "United Arab Emirates",
  "e_status": "active",
  "d_added": "2018-05-09 06:26:13",
  "d_modified": "2018-05-09 06:26:13"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2949d76fbae1ce44d4e1b"),
  "v_title": "\tUnited Kingdom",
  "e_status": "active",
  "d_added": "2018-05-09 06:26:37",
  "d_modified": "2018-05-09 06:26:37"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af294b376fbae0e7f0236cb"),
  "v_title": "\tUnited States",
  "e_status": "active",
  "d_added": "2018-05-09 06:26:59",
  "d_modified": "2018-05-09 06:26:59"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af294c876fbae0a9c34d1aa"),
  "v_title": "\tVenezuela",
  "e_status": "active",
  "d_added": "2018-05-09 06:27:20",
  "d_modified": "2018-05-09 06:27:20"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af294d876fbae0a9e637178"),
  "v_title": "Vietnam",
  "e_status": "active",
  "d_added": "2018-05-09 06:27:36",
  "d_modified": "2018-05-09 06:27:36"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af294e976fbae0a9c34d1ab"),
  "v_title": "Virgin Islands (British)",
  "e_status": "active",
  "d_added": "2018-05-09 06:27:53",
  "d_modified": "2018-05-09 06:27:53"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af294f776fbae0b274ad46b"),
  "v_title": "Virgin Islands (U.S.)",
  "e_status": "active",
  "d_added": "2018-05-09 06:28:07",
  "d_modified": "2018-05-09 06:28:07"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2950776fbae1c9d53b256"),
  "v_title": "\tWestern Sahara",
  "e_status": "active",
  "d_added": "2018-05-09 06:28:23",
  "d_modified": "2018-05-09 06:28:23"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2951d76fbae0a9c34d1ac"),
  "v_title": "Wallis and Futuna Islands\t",
  "e_status": "active",
  "d_added": "2018-05-09 06:28:45",
  "d_modified": "2018-05-09 06:28:45"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2952f76fbae0a9f50e3aa"),
  "v_title": "\tYemen",
  "e_status": "active",
  "d_added": "2018-05-09 06:29:03",
  "d_modified": "2018-05-09 06:29:03"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2953e76fbae0a9c34d1ad"),
  "v_title": "\tZambia",
  "e_status": "active",
  "d_added": "2018-05-09 06:29:18",
  "d_modified": "2018-05-09 06:29:18"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2954b76fbae0a9d736267"),
  "v_title": "Zimbabwe",
  "e_status": "active",
  "d_added": "2018-05-09 06:29:31",
  "d_modified": "2018-05-09 06:29:31"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2956276fbae1c9b206749"),
  "v_title": "Trinidad and Tobago",
  "e_status": "active",
  "d_added": "2018-05-09 06:29:54",
  "d_modified": "2018-05-09 06:29:54"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2957476fbae1c9d53b257"),
  "v_title": "\tSlovenia",
  "e_status": "active",
  "d_added": "2018-05-09 06:30:12",
  "d_modified": "2018-05-09 06:30:12"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2959476fbae1ce44d4e1c"),
  "v_title": "\tParaguay",
  "e_status": "active",
  "d_added": "2018-05-09 06:30:44",
  "d_modified": "2018-05-09 06:30:44"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af295a776fbae0a9c34d1ae"),
  "v_title": "\tNicaragua",
  "e_status": "active",
  "d_added": "2018-05-09 06:31:03",
  "d_modified": "2018-05-09 06:31:03"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af295ba76fbae1c9d53b258"),
  "v_title": "Morocco",
  "e_status": "active",
  "d_added": "2018-05-09 06:31:22",
  "d_modified": "2018-05-09 06:31:22"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af295d576fbae0a9f50e3ab"),
  "v_title": "\tAustralia",
  "e_status": "active",
  "d_added": "2018-05-09 06:31:49",
  "d_modified": "2018-05-09 06:31:49"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af295e976fbae0b274ad46c"),
  "v_title": "Antarctica",
  "e_status": "active",
  "d_added": "2018-05-09 06:32:09",
  "d_modified": "2018-05-09 06:32:09"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af295fc76fbae0ce54f8e57"),
  "v_title": "\tBahrain",
  "e_status": "active",
  "d_added": "2018-05-09 06:32:28",
  "d_modified": "2018-05-09 06:32:28"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2960a76fbae0e7f0236cc"),
  "v_title": "\tBarbados",
  "e_status": "active",
  "d_added": "2018-05-09 06:32:42",
  "d_modified": "2018-05-09 06:32:42"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2961b76fbae1c9d53b259"),
  "v_title": "\tBurundi",
  "e_status": "active",
  "d_added": "2018-05-09 06:32:59",
  "d_modified": "2018-05-09 06:32:59"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2962e76fbae0ce54f8e58"),
  "v_title": "\tChristmas Island",
  "e_status": "active",
  "d_added": "2018-05-09 06:33:18",
  "d_modified": "2018-05-09 06:33:18"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2963e76fbae1ce44d4e1d"),
  "v_title": "Cyprus",
  "e_status": "active",
  "d_added": "2018-05-09 06:33:34",
  "d_modified": "2018-05-09 06:33:34"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2965176fbae1c9d53b25a"),
  "v_title": "Ecuador",
  "e_status": "active",
  "d_added": "2018-05-09 06:33:53",
  "d_modified": "2018-05-09 06:33:53"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2965f76fbae0ce54f8e59"),
  "v_title": "\tEthiopia",
  "e_status": "active",
  "d_added": "2018-05-09 06:34:07",
  "d_modified": "2018-05-09 06:34:07"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2967476fbae0a9c34d1af"),
  "v_title": "\tFinland",
  "e_status": "active",
  "d_added": "2018-05-09 06:34:28",
  "d_modified": "2018-05-09 06:34:28"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2968876fbae0a9e637179"),
  "v_title": "Georgia",
  "e_status": "active",
  "d_added": "2018-05-09 06:34:48",
  "d_modified": "2018-05-09 06:34:48"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af2969776fbae0a9f50e3ac"),
  "v_title": "Haiti",
  "e_status": "active",
  "d_added": "2018-05-09 06:35:03",
  "d_modified": "2018-05-09 06:35:03"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af296bc76fbae0a9e63717a"),
  "v_title": "Kazakhstan",
  "e_status": "active",
  "d_added": "2018-05-09 06:35:40",
  "d_modified": "2018-05-09 06:35:40"
});
db.getCollection("tbl_country").insert({
  "_id": ObjectId("5af296ec76fbae0ce54f8e5a"),
  "v_title": "\tLesotho",
  "e_status": "active",
  "d_added": "2018-05-09 06:36:28",
  "d_modified": "2018-05-09 06:36:28"
});

/** tbl_course_announcements records **/
db.getCollection("tbl_course_announcements").insert({
  "_id": ObjectId("5b177ee276fbae1c5918b122"),
  "i_courses_id": "5b1672e376fbae5894392f52",
  "v_title": " Lorem Ipsum",
  "l_announcements": " It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n\r\n",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "e_status": "active",
  "d_added": "2018-06-06 06:27:46",
  "d_modified": "2018-06-06 06:27:46"
});
db.getCollection("tbl_course_announcements").insert({
  "_id": ObjectId("5b17b2f476fbae1c5e740b04"),
  "i_courses_id": "5b16610b76fbae3d393c9a03",
  "v_title": "What is Lorem Ipsum?",
  "l_announcements": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "e_status": "active",
  "d_added": "2018-06-06 10:09:56",
  "d_modified": "2018-06-06 10:09:56",
  "l_comments": [
    {
      "i_user_id": "5b165e1376fbae3c3e66bd33",
      "v_text": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
      "d_added": "2018-06-06 10:11:08"
    },
    {
      "i_user_id": "5b165e1376fbae3c3e66bd33",
      "v_text": "1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
      "d_added": "2018-06-06 10:11:23"
    }
  ]
});
db.getCollection("tbl_course_announcements").insert({
  "_id": ObjectId("5b5f43d01b544d0542551783"),
  "i_courses_id": "5b5f42b61b544d57a2399ea2",
  "v_title": "asdsa",
  "l_announcements": "dsadsa",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "e_status": "active",
  "d_added": "2018-07-30 16:58:56",
  "d_modified": "2018-07-30 16:58:56"
});
db.getCollection("tbl_course_announcements").insert({
  "_id": ObjectId("5b5f43ed1b544d0544378ad3"),
  "i_courses_id": "5b5f42b61b544d57a2399ea2",
  "v_title": "asdasd",
  "l_announcements": "sadsadsadsa",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "e_status": "active",
  "d_added": "2018-07-30 16:59:25",
  "d_modified": "2018-07-30 16:59:25"
});

/** tbl_course_category records **/
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5ac478d9d3e812e9233c986b"),
  "v_title": "Creative Art & Design",
  "e_status": "active",
  "d_added": "2018-04-04 07:03:53",
  "d_modified": "2018-04-04 07:07:04"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5ac478fcd3e812050c3c9869"),
  "v_title": "Cooking & Nutrition",
  "e_status": "active",
  "d_added": "2018-04-04 07:04:28",
  "d_modified": "2018-04-04 07:04:28"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5ac47907d3e812bf2f3c9869"),
  "v_title": "Tutoring Services",
  "e_status": "active",
  "d_added": "2018-04-04 07:04:39",
  "d_modified": "2018-04-04 07:04:39"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5ac47914d3e812b32f3c9869"),
  "v_title": "Music Teacher",
  "e_status": "active",
  "d_added": "2018-04-04 07:04:52",
  "d_modified": "2018-07-27 04:01:18"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5ac47921d3e812b52f3c9869"),
  "v_title": "Tech Lessons",
  "e_status": "active",
  "d_added": "2018-04-04 07:05:05",
  "d_modified": "2018-07-27 04:01:35"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5ac4792cd3e81293243c986a"),
  "v_title": " Nature & Environment",
  "e_status": "active",
  "d_added": "2018-04-04 07:05:16",
  "d_modified": "2018-04-04 07:05:16"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5ac4793cd3e812b42f3c9869"),
  "v_title": "Business Guidance",
  "e_status": "active",
  "d_added": "2018-04-04 07:05:32",
  "d_modified": "2018-07-27 04:01:55"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5ac47947d3e812271b3c986a"),
  "v_title": " Personal Trainer Coach ",
  "e_status": "active",
  "d_added": "2018-04-04 07:05:43",
  "d_modified": "2018-07-27 04:02:47"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5ac47950d3e812cf243c9869"),
  "v_title": "Nursing Guide",
  "e_status": "active",
  "d_added": "2018-04-04 07:05:52",
  "d_modified": "2018-07-27 04:02:20"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5b5b423e1b544d053226b493"),
  "v_title": "Career Coach",
  "e_status": "active",
  "d_added": "2018-07-27 04:03:10",
  "d_modified": "2018-07-27 04:03:10"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5b5b42491b544d053226b494"),
  "v_title": "Writing & Publishing",
  "e_status": "active",
  "d_added": "2018-07-27 04:03:21",
  "d_modified": "2018-07-27 04:03:21"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5b5b42571b544d0cc508e8d3"),
  "v_title": "Creativity & Design",
  "e_status": "active",
  "d_added": "2018-07-27 04:03:35",
  "d_modified": "2018-07-27 04:03:35"
});
db.getCollection("tbl_course_category").insert({
  "_id": ObjectId("5b5b42641b544d05312b0793"),
  "v_title": "Gardening Design",
  "e_status": "active",
  "d_added": "2018-07-27 04:03:48",
  "d_modified": "2018-07-27 04:03:48"
});

/** tbl_course_comments records **/

/** tbl_course_faqs records **/
db.getCollection("tbl_course_faqs").insert({
  "_id": ObjectId("5a619826d3e812da1a3c9869"),
  "i_course_id": "5a607f6dd3e812b94b3c9869",
  "v_title": "Lorem Ipsum is simply dummy text of the printing.",
  "v_type": "announcement",
  "e_status": "active",
  "d_added": "2018-01-19 07:03:02",
  "d_modified": "2018-01-19 07:05:04"
});
db.getCollection("tbl_course_faqs").insert({
  "_id": ObjectId("5a6198b5d3e8128a0d3c986a"),
  "i_course_id": "5a607f6dd3e812b94b3c9869",
  "v_title": "Why Lorem Ipsum is simply dummy text of the printing?",
  "v_type": "question",
  "e_status": "active",
  "d_added": "2018-01-19 07:05:25"
});

/** tbl_course_language records **/
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac471a6d3e812ec233c9869"),
  "v_title": "CHINESE",
  "e_status": "active",
  "d_added": "2018-04-04 06:33:10",
  "d_modified": "2018-04-04 06:33:10"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac471b0d3e8124a1e3c9869"),
  "v_title": "SPANISH",
  "e_status": "active",
  "d_added": "2018-04-04 06:33:20",
  "d_modified": "2018-04-04 06:33:20"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac471b8d3e8124a1e3c986a"),
  "v_title": "ENGLISH",
  "e_status": "active",
  "d_added": "2018-04-04 06:33:28",
  "d_modified": "2018-04-04 06:33:28"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac471c1d3e81299203c9869"),
  "v_title": "HINDI",
  "e_status": "active",
  "d_added": "2018-04-04 06:33:37",
  "d_modified": "2018-04-04 06:33:37"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac471c9d3e81299203c986a"),
  "v_title": "ARABIC",
  "e_status": "active",
  "d_added": "2018-04-04 06:33:45",
  "d_modified": "2018-05-25 01:15:27"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac471d2d3e812c0273c9869"),
  "v_title": "PORTUGUESE",
  "e_status": "active",
  "d_added": "2018-04-04 06:33:54",
  "d_modified": "2018-04-04 06:33:54"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac471dbd3e812e9233c9869"),
  "v_title": "BENGALI",
  "e_status": "active",
  "d_added": "2018-04-04 06:34:03",
  "d_modified": "2018-04-04 06:34:03"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac471e2d3e812e9233c986a"),
  "v_title": "RUSSIAN",
  "e_status": "active",
  "d_added": "2018-04-04 06:34:10",
  "d_modified": "2018-04-04 06:34:10"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac471f7d3e812271b3c9869"),
  "v_title": "GERMAN",
  "e_status": "active",
  "d_added": "2018-04-04 06:34:31",
  "d_modified": "2018-04-04 06:34:31"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac47201d3e812ec233c986a"),
  "v_title": "FRENCH",
  "e_status": "active",
  "d_added": "2018-04-04 06:34:41",
  "d_modified": "2018-04-04 06:34:41"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac47209d3e8124a1e3c986b"),
  "v_title": "TAMIL",
  "e_status": "active",
  "d_added": "2018-04-04 06:34:49",
  "d_modified": "2018-04-04 06:34:49"
});
db.getCollection("tbl_course_language").insert({
  "_id": ObjectId("5ac47217d3e81299203c986b"),
  "v_title": "URDU",
  "e_status": "active",
  "d_added": "2018-04-04 06:35:03",
  "d_modified": "2018-04-04 06:35:03"
});

/** tbl_course_level records **/
db.getCollection("tbl_course_level").insert({
  "_id": ObjectId("5ac47ae0d3e81268343c9869"),
  "v_title": "Entry",
  "e_status": "active",
  "d_added": "2018-04-04 07:12:32",
  "d_modified": "2018-05-23 09:10:38"
});
db.getCollection("tbl_course_level").insert({
  "_id": ObjectId("5ac47ae7d3e812cf243c986a"),
  "v_title": "Intermediate",
  "e_status": "active",
  "d_added": "2018-04-04 07:12:39",
  "d_modified": "2018-05-23 09:11:04"
});
db.getCollection("tbl_course_level").insert({
  "_id": ObjectId("5ac47aeed3e812b32f3c986a"),
  "v_title": "Expert",
  "e_status": "active",
  "d_added": "2018-04-04 07:12:46",
  "d_modified": "2018-05-23 09:11:20"
});

/** tbl_course_qa records **/
db.getCollection("tbl_course_qa").insert({
  "_id": ObjectId("5b17ae8876fbae1c5c7e1b03"),
  "i_courses_id": "5b16610b76fbae3d393c9a03",
  "v_title": "PageMaker including versions of Lorem Ipsum.",
  "l_questions": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-06-06 09:51:04",
  "d_modified": "2018-06-06 09:51:04",
  "l_answers": [
    {
      "i_user_id": "5b165e1376fbae3c3e66bd33",
      "v_text": "Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      "d_added": "2018-06-06 09:56:58"
    }
  ]
});
db.getCollection("tbl_course_qa").insert({
  "_id": ObjectId("5b1a2ea676fbae10d75237e2"),
  "i_courses_id": "5b16610b76fbae3d393c9a03",
  "v_title": "This is test QA",
  "l_questions": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-06-08 07:22:14",
  "d_modified": "2018-06-08 07:22:14"
});
db.getCollection("tbl_course_qa").insert({
  "_id": ObjectId("5b1a2ec476fbae0b927d5913"),
  "i_courses_id": "5b16610b76fbae3d393c9a03",
  "v_title": "This is test QA",
  "l_questions": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-06-08 07:22:44",
  "d_modified": "2018-06-08 07:22:44",
  "l_answers": [
    {
      "i_user_id": "5b165e1376fbae3c3e66bd33",
      "v_text": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
      "d_added": "2018-06-08 09:16:09"
    },
    {
      "i_user_id": "5b165e1376fbae3c3e66bd33",
      "v_text": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
      "d_added": "2018-06-08 09:16:16"
    },
    {
      "i_user_id": "5b165e1376fbae3c3e66bd33",
      "v_text": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
      "d_added": "2018-06-08 09:16:41"
    },
    {
      "i_user_id": "5b165e1376fbae3c3e66bd33",
      "v_text": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
      "d_added": "2018-06-08 09:24:38"
    },
    {
      "i_user_id": "5b165e1376fbae3c3e66bd33",
      "v_text": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
      "d_added": "2018-06-08 09:25:33"
    }
  ]
});

/** tbl_course_reviews records **/
db.getCollection("tbl_course_reviews").insert({
  "_id": ObjectId("5b1a54b576fbae10d75237e4"),
  "i_courses_id": "5b16610b76fbae3d393c9a03",
  "f_rate_instruction": "4.5",
  "f_rate_navigate": "4.5",
  "f_rate_reccommend": "4.5",
  "l_comment": "Rate this course and leave the review. The edit review option will be avaliable to you for up to 24 hours.\r\nRate this course and leave the review. The edit review option will be avaliable to you for up to 24 hours.\r\nRate this course and leave the review. The edit review option will be avaliable to you for up to 24 hours.\r\nRate this course and leave the review. The edit review option will be avaliable to you for up to 24 hours.\r\nRate this course and leave the review. The edit review option will be avaliable to you for up to 24 hours.",
  "i_star": 5,
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-06-08 10:04:37",
  "d_modified": "2018-06-08 10:04:37",
  "l_answers": {
    "i_user_id": "5b165e1376fbae3c3e66bd33",
    "l_comment": "Rate this course and leave the review. The edit review option will be avaliable to you for up to 24 hours. Rate this course and leave the review. The edit review option will be avaliable to you for up to 24 hours. ",
    "d_added": "2018-06-08 10:15:47"
  }
});
db.getCollection("tbl_course_reviews").insert({
  "_id": ObjectId("5b4780b4516b4620200031ab"),
  "i_courses_id": "5b3a1fdb516b460a68003e69",
  "f_rate_instruction": "5",
  "f_rate_navigate": "5",
  "f_rate_reccommend": "5",
  "l_comment": "This is test Review",
  "i_star": 5,
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_seller_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-07-12 16:24:20",
  "d_modified": "2018-07-12 16:24:20"
});

/** tbl_courses records **/
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b16610b76fbae3d393c9a03"),
  "v_title": "Deep Learning: GANs and Variational ",
  "v_sub_title": "Generative Adversarial Networks and Variational Autoencoders in Python, Theano, and Tensorflow",
  "v_author_name": "Ray Nanda",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "125",
  "i_category_id": "5ac478d9d3e812e9233c986b",
  "v_requirement": "Know how to build a neural network in Theano and/or Tensorflow<br /><br />\r\nProbability<br /><br />\r\nMultivariate Calculus<br /><br />\r\nNumpy, etc.",
  "l_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br /><br />\r\n<br /><br />\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br /><br />\r\n<br /><br />\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br /><br />\r\n<br /><br />\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br /><br />\r\n<br /><br />\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br /><br />\r\n<br /><br />\r\n",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-06-05 10:08:11",
  "d_modified": "2018-07-30 17:15:21",
  "l_instructor_details": "I am a data scientist, big data engineer, and full stack software engineer.<br /><br />\r\n<br /><br />\r\nI received my masters degree in computer engineering with a specialization in machine learning and pattern recognition.<br /><br />\r\n<br /><br />\r\nExperience includes online advertising and digital media as both a data scientist (optimizing click and conversion rates) and big data engineer (building data processing pipelines). Some big data technologies I frequently use are Hadoop, Pig, Hive, MapReduce, and Spark.<br /><br />\r\n<br /><br />\r\nI've created deep learning models to predict click-through rate and user behavior, as well as for image and signal processing and modeling text.<br /><br />\r\n<br /><br />\r\nMy work in recommendation systems has applied Reinforcement Learning and Collaborative Filtering, and we validated the results using A/B testing.<br /><br />\r\n<br /><br />\r\nI have taught undergraduate and graduate students in data science, statistics, machine learning, algorithms, calculus, computer graphics, and physics for students attending universities such as Columbia University, NYU, Hunter College, and The New School. <br /><br />\r\n<br /><br />\r\nMultiple businesses have benefitted from my web programming expertise. I do all the backend (server), frontend (HTML/JS/CSS), and operations/deployment work. Some of the technologies I've used are: Python, Ruby/Rails, PHP, Bootstrap, jQuery (Javascript), Backbone, and Angular. For storage/databases I've used MySQL, Postgres, Redis, MongoDB, and more.",
  "f_price": 325,
  "section": [
    {
      "v_title": "Introduction and Outline",
      "video": {
        "v_title": [
          "Welcome"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528193468.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "How to succeed in this course"
        ],
        "v_doc": [
          "courses/video/section_doc-1528193477.js"
        ]
      },
      "_id": ObjectId("5b1661bc76fbae3c3e66bd34")
    },
    {
      "v_title": "Generative Modeling Review",
      "video": {
        "v_title": [
          "What does it mean to Sample?"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528193479.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Sampling Demo: Bayes Classifier"
        ],
        "v_doc": [
          "courses/video/section_doc-1528193488.js"
        ]
      },
      "_id": ObjectId("5b1661c776fbae3c3e66bd35")
    },
    {
      "v_title": "Variational Autoencoders",
      "video": {
        "v_title": [
          "Variational Autoencoder Architecture"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528193490.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Parameterizing a Gaussian with a Neural Network"
        ],
        "v_doc": [
          "courses/video/section_doc-1528193499.js"
        ]
      },
      "_id": ObjectId("5b1661d276fbae3c3e66bd36")
    }
  ],
  "v_search_tags": [
    "Deep Learning",
    "GANs",
    "Variational "
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528193453.jpeg",
  "l_video": "courses/video/introduction_video-1528193458.mp4",
  "e_sponsor": "yes",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(1),
    "avgtotal": 4.5
  },
  "e_sponsor_status": "start",
  "i_impression": NumberInt(29),
  "i_clicks": NumberInt(4)
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b16621d76fbae3c8e6c7994"),
  "v_title": "Artificial Intelligence: Reinforcement ",
  "v_sub_title": "Complete guide to artificial intelligence and machine learning, prep for deep reinforcement learning",
  "v_author_name": "Ray Nanda",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "125",
  "i_category_id": "5ac478fcd3e812050c3c9869",
  "v_requirement": "Calculus<br />\r\nProbability<br />\r\nMarkov Models<br />\r\nThe Numpy Stack<br />\r\nHave experience with at least a few supervised machine learning methods<br />\r\nGradient descent<br />\r\nGood object-oriented programming skills",
  "l_description": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br />\r\n<br />\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br />\r\n<br />\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br />\r\n<br />\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br />\r\n<br />\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br />\r\n<br />\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br />\r\n<br />\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br />\r\n<br />\r\n",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-06-05 10:12:45",
  "d_modified": "2018-08-06 18:00:41",
  "l_instructor_details": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.<br />\r\n<br />\r\n",
  "f_price": 100,
  "section": [
    {
      "v_title": "Introduction and Outline",
      "video": {
        "v_title": [
          " Introduction and outline"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528193694.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "What is Reinforcement Learning?"
        ],
        "v_doc": [
          "courses/video/section_doc-1528193704.js"
        ]
      },
      "_id": ObjectId("5b16629e76fbae3d430f74d3")
    },
    {
      "v_title": "Return of the Multi-Armed Bandit",
      "video": {
        "v_title": [
          "Problem Setup and The Explore-Exploit Dilemma"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528193706.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Epsilon-Greedy"
        ],
        "v_doc": [
          "courses/video/section_doc-1528193715.js"
        ]
      },
      "_id": ObjectId("5b1662aa76fbae3d430f74d4")
    }
  ],
  "v_search_tags": [
    "Artificial",
    "Intelligence",
    "Reinforcement "
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528193682.jpeg",
  "l_video": "courses/video/introduction_video-1528193685.mp4",
  "e_sponsor": "yes",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  },
  "e_sponsor_status": "start",
  "i_impression": NumberInt(27)
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b16703f76fbae487952a662"),
  "v_title": "iOS 11 & Swift 4 - The Complete iOS App ",
  "v_sub_title": "Learn iOS 11 App Development From Beginning to End. Using Xcode 9 and Swift 4. Includes Full ARKit and CoreML Modules!",
  "v_author_name": "Ray Nanda",
  "i_language_id": "5ac471b0d3e8124a1e3c9869",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "125",
  "i_category_id": "5ac47907d3e812bf2f3c9869",
  "v_requirement": "No programming experience needed - I'll teach you everything you need to know<br />\r\nA Mac laptop or iMac (or a PC running macOS)<br />\r\nNo paid software required - all apps will be created in Xcode 9 (which is free)<br />\r\nI'll walk you through, step-by-step how to get Xcode installed set up",
  "l_description": "Welcome to the Complete iOS App Development Bootcamp. With over 15,000 ratings and a 4.8 average this is the HIGHEST RATED iOS Course of all time! ⭐️⭐️⭐️⭐️⭐️ <br />\r\n<br />\r\nThis Swift 4 course is based on our in-person app development bootcamp in London. We've perfected the curriculum over 3 years of in-person teaching. <br />\r\n<br />\r\nOur complete app development bootcamp teaches you how to code using Swift 4 and build beautiful iOS 11 apps for iPhone and iPad. Even if you have ZERO programming experience.<br />\r\n<br />\r\nWe'll take you step-by-step through engaging video tutorials and teach you everything you need to know to succeed as an iOS app developer.<br />\r\n<br />\r\nThe course includes over hours and hours of HD video tutorials and builds your programming knowledge through making real world apps. e.g. Pokemon Go, Whatsapp, QuizUp and Yahoo Weather.<br />\r\n<br />\r\nIt's updated to include over 5 hours of content on Apple's brand new ARKit for making Augmented Reality apps and CoreML for making intelligent apps with Machine Learning. You'll be building image recognition apps and incredible 3D animated AR apps.<br />\r\n<br />\r\nBy the end of this course, you will be fluently programming in Swift 4 and be ready to make your own apps or start a freelancing job as an iOS 11 developer.<br />\r\n<br />\r\nYou'll also have a portfolio of over 20 apps that you can show off to any potential employer.<br />\r\n<br />\r\nSign up today, and look forward to:<br />\r\n<br />\r\nOver 50 hours of HD 1080p video content<br />\r\nBuilding over 20 fully-fledged including ones that use Firebase, machine learning and augmented reality<br />\r\nAll the knowledge you need to start building any app you want<br />\r\nThousands of dollars worth of design assets<br />\r\nOur best selling 12 Rules to Learn to Code eBook<br />\r\n$8000+ app development bootcamp course materials and curriculum",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-06-05 11:13:03",
  "d_modified": "2018-06-05 11:15:08",
  "l_instructor_details": "'m Angela, I'm an iOS and WatchOS developer fluent in Swift and Objective-C. I'm the lead iOS instructor at the London App Brewery where we've taught over 900+ students in-person at our London classroom. <br />\r\n<br />\r\nMy first foray into programming was when I was just 12 years old, wanting to build my own video games. Since then, I've made over 70+ apps and some awesome games. In my courses, you'll find lots of geeky humour but also plenty of explanations and animations to make sure everything is easy to understand. I'll be there for you every step of the way.",
  "f_price": 100,
  "section": [
    {
      "v_title": " Getting Started with iOS 11 and Swift 4",
      "video": {
        "v_title": [
          "What You'll Get in This Course"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528197299.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "How to Get All the Free Stuff"
        ],
        "v_doc": [
          "courses/video/section_doc-1528197301.js"
        ]
      },
      "_id": ObjectId("5b1670b376fbae3d3f03e805")
    },
    {
      "v_title": "iOS Interface Builder - The $999 App",
      "video": {
        "v_title": [
          "asdsadsadsa"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528197302.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "asdsadsadsadsa"
        ],
        "v_doc": [
          "courses/video/section_doc-1528197306.js"
        ]
      },
      "_id": ObjectId("5b1670b676fbae3d3f03e806")
    }
  ],
  "v_search_tags": [
    "iOS",
    "development"
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528197295.jpeg",
  "l_video": "courses/video/introduction_video-1528197297.mp4",
  "e_sponsor": "no",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  }
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b1672e376fbae5894392f52"),
  "v_title": "The Complete Junior to Senior Web Developer Roadmap (2018)",
  "v_sub_title": "Go from Junior Developer to Senior Developer. Learn all the technical skills Senior Web Developers know in 2018!",
  "v_author_name": "Katherine Read",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "6 ",
  "i_category_id": "5ac478d9d3e812e9233c986b",
  "v_requirement": "Basic understanding of HTML, CSS, and Javascript<br /><br />\r\nPrepare to learn real life skills and build real web apps that will get you hired<br /><br />\r\nCompletion of \"The Complete Web Developer in 2018: Zero to Mastery\" is a bonus but not necessary<br /><br />\r\n",
  "l_description": "Welcome to the The Complete Junior to Senior Web Developer Roadmap! This is the tutorial you've been looking for to no longer be a junior developer, level up your skills, and earn a higher salary. This extensive course doesn’t just cover a small portion of the industry. This covers everything you need to know to go from Junior Developer, to learning the in-demand technical skills that some of the top developers in the industry know. <br /><br />\r\n<br /><br />\r\nI guarantee you that you won't find a course that is as comprehensive, up to date, and better quality, than this tutorial. You will be guided in your journey to become an admired and respected Senior Developer one day.<br /><br />\r\n<br /><br />\r\nThis is the hardest work I have ever done in my life, and it took me months to plan, months to film, months to edit, and years of experience to create. No course like this exists out there because this is a really difficult topic to teach and to combine all of the technologies we cover into one course is a long hard process.<br /><br />\r\n<br /><br />\r\nBy the end of this course you will have a fully functioning image recognition app to show off on your portfolio. More importantly, you will be able to implement and add the below skillsets to your resume, impress your boss, and ace your next interview (Trust me, some of these may not mean anything to you now, but they are topics that ALL Senior Developers know). You will be taken from absolute zero to knowing how to:<br /><br />\r\n<br /><br />\r\nUse React and Redux to build complex and large applications.<br /><br />\r\nImprove performance of any web application (Code Splitting, Load Balancing, Caching, Code Optimizations, and much more)<br /><br />\r\nUse SSH on a remote linux server and generate ssh keys<br /><br />\r\nUse Webpack 4 and Parcel to set up a project and bundle your files<br /><br />\r\nImplement proper security and securing your apps like an expert<br /><br />\r\nUse AWS Lambda and Serverless architecture to manage applications that can handle millions of users",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 11:24:19",
  "d_modified": "2018-06-06 06;29:38",
  "l_instructor_details": "Andrei is the instructor of the highest rated Web Development course on Udemy as well as one of the fastest growing. His graduates have moved on to work for some of the biggest tech companies around the world like Apple. He has been working as a senior software developer in Silicon Valley and Toronto for many years, and is now taking all that he has learned, to teach programming skills and to help you discover the amazing career opportunities that being a developer allows in life. <br /><br />\r\n<br /><br />\r\nHaving been a self taught programmer, he understands that there is an overwhelming number of online courses, tutorials and books that are overly verbose and inadequate at teaching proper skills. ",
  "f_price": 250,
  "section": [
    {
      "v_title": " Introduction",
      "video": {
        "v_title": [
          " Introduction"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528198178.mp4"
        ]
      },
      "doc": {
        "v_title": [
          " Join Our Online Classroom!"
        ],
        "v_doc": [
          "courses/video/section_doc-1528198180."
        ]
      },
      "_id": ObjectId("5b177da776fbae1c6150e602")
    },
    {
      "v_title": "Meet The Community",
      "video": {
        "v_title": [
          "Meet The Community"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528198180.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Meet The Community"
        ],
        "v_doc": [
          "courses/video/section_doc-1528198182."
        ]
      },
      "_id": ObjectId("5b177da776fbae1c6150e603")
    }
  ],
  "v_search_tags": [
    "new  Community"
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528198170.jpg",
  "l_video": "courses/video/introduction_video-1528198176.mp4",
  "e_sponsor": "no",
  "messages": {
    "welcome_message": "",
    "welcome_congratulations": ""
  },
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  }
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b1674f576fbae5bca3eb652"),
  "v_title": "The Ultimate Drawing Course - Beginner to Advanced",
  "v_sub_title": "Learn the #1 most important building block of all art",
  "v_author_name": "Katherine Read",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae7d3e812cf243c986a",
  "i_duration": "6 month",
  "i_category_id": "5ac47914d3e812b32f3c9869",
  "v_requirement": "Paper<br />\r\nPencil<br />\r\nEraser<br />\r\nRuler<br />\r\nMotivation to learn!",
  "l_description": "Join over 150,000 learning student and start gaining the drawing skills you've always wanted.<br />\r\n<br />\r\nThe Ultimate Drawing Course will show you how to create advanced art that will stand up as professional work. This course will enhance or give you skills in the world of drawing - or your money back<br />\r\n<br />\r\nThe course is your track to obtaining drawing skills like you always knew you should have! Whether for your own projects or to draw for other people.<br />\r\n<br />\r\nThis course will take you from having little knowledge in drawing to creating advanced art and having a deep understanding of drawing fundamentals.<br />\r\n<br />\r\nSo what else is in it for you?<br />\r\n<br />\r\nYou’ll create over 50 different projects in this course that will take you from beginner to expert!<br />\r\n<br />\r\nYou’ll gain instant access to all 11 sections of the course.<br />\r\n<br />\r\nThe course is setup to quickly take you through step by step, the process of drawing in many different styles. It will equip you with the knowledge to create stunning designs and illustration!<br />\r\n<br />\r\nDon’t believe me? I offer you a full money back guarantee within the first 30 days of purchasing the course.<br />\r\n<br />\r\nHere’s what you get with the course:<br />\r\n<br />\r\nYou’ll get access to the11 sections of the course that will teach you the fundamentals of drawing from the ground up. The course is supported with over 10 hours of clear content that I walk you through each step of the way.<br />\r\n<br />\r\nAll at your fingers tips instantly.<br />\r\n<br />\r\nThe course starts with the basics. You will get an in depth understanding of the fundamentals of drawing. Fundamentals are the most important part of creating professional art. You will learn everything from line fundamentals all the way up to highlight and shadows.<br />\r\n<br />\r\nNext you’ll learn how perspective works and how to incorporate it into your art. You will be learning 1, 2, and 3 point perspective.<br />\r\n<br />\r\nOnce you’ve learned perspective you are going to learn how to create texture and apply it to your drawings.<br />\r\n<br />\r\nThen you are going to learn how to draw from life. Observing life and drawing it is a very important skill when it comes to art.<br />\r\n<br />\r\nAt this point you’ll be ready to start drawing the human face. We will spend a whole section learning how to draw the human face from different angles.<br />\r\n<br />\r\nNext you’ll you’re going to learn how to draw the human figure.<br />\r\n<br />\r\nLastly you will gain access to the bonus section where I’ll teach you how I draw animation styled characters step by step.<br />\r\n<br />\r\nOver the 7 chapters you will learn:<br />\r\n<br />\r\nHow to draw an eye<br />\r\n<br />\r\nLine fundamentals<br />\r\n<br />\r\nShape and form fundamental<br />\r\n<br />\r\nHow to use value and contrast<br />\r\n<br />\r\nSpace and perspective<br />\r\n<br />\r\nStill life drawing<br />\r\n<br />\r\nCreating texture<br />\r\n<br />\r\nDrawing the human face<br />\r\n<br />\r\nDrawing the human figure<br />\r\n<br />\r\nDrawing animation styled art<br />\r\n<br />\r\nWhat else will you get?<br />\r\n<br />\r\n- Personal contact with me, the course tutor<br />\r\n<br />\r\n- Lifetime access to course materials<br />\r\n<br />\r\n- Understanding of how professional art is created<br />\r\n<br />\r\n- Quizzes and exercise work sheets<br />\r\n<br />\r\nThis all comes under one convenient easy to use platform. Plus you will get fast, friendly, responsive support on the Udemy Q&A section of the course or direct message.<br />\r\n<br />\r\nI will be here for you every step of the way!<br />\r\n<br />\r\nSo what are you waiting for? Sign up now and change your art world today!<br />\r\n<br />\r\nWho is the target audience?<br />\r\nStudents wanting to learn how to draw<br />\r\nStudents willing to put in a couple hours to learn how to draw<br />\r\nStudents willing to take action and start drawing amazing artwork<br />\r\nStudents wanting to add another skill to their tool belt<br />\r\n",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 11:33:09",
  "d_modified": "2018-06-05 11:35:35",
  "l_instructor_details": "Jaysen Batchelor<br />\r\nIllustrator & Designer<br />\r\nAt the age of 17 I began a 6th month internship with an animation studio out of Utah. After my internship was up, I was hired on to build backgrounds and to design props and characters for animation projects. I then began to do my own freelance work on the side through networking and using online freelancing sites. Now I work as a freelance Illustrator and designer working on my own time from where ever I want.",
  "f_price": 100,
  "section": [
    {
      "v_title": "Learn how to get a guaranteed win out of this course! ",
      "video": {
        "v_title": [
          "Learn how to get a guaranteed win out of this course! "
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528198533.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Learn how to get a guaranteed win out of this course! "
        ],
        "v_doc": [
          "courses/video/section_doc-1528198534."
        ]
      },
      "_id": ObjectId("5b16758576fbae5bc93da212")
    }
  ],
  "v_search_tags": [
    "course "
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528198523.jpg",
  "l_video": "courses/video/introduction_video-1528198531.mp4",
  "e_sponsor": "no",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  }
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b1675aa76fbae59b43dfe12"),
  "v_title": "The Modern Python 3 Bootcamp",
  "v_sub_title": "A Unique Interactive Python Experience With Nearly 200 Exercises and Quizzes",
  "v_author_name": "Katherine Read",
  "i_language_id": "5ac471b0d3e8124a1e3c9869",
  "i_level_id": "5ac47aeed3e812b32f3c986a",
  "i_duration": "5 month",
  "i_category_id": "5ac47921d3e812b52f3c9869",
  "v_requirement": "A computer! We've got you covered whether you have a Mac or a PC.<br />\r\nPrepare to write thousands of lines of python exercises!<br />\r\nNo previous experience with Python or coding is required.",
  "l_description": "Welcome to the Modern Python 3 Bootcamp!  I've launched my fair share of Udemy courses, but I've never done anything quite this crazy.<br />\r\n<br />\r\nThis course is a unique experience on Udemy. There are dozens of existing Python courses you can choose from, but this course is the only one that has nearly 200 interactive challenges you can complete right here in your browser. I didn't just tack on a couple of exercises at the end; this course is fully built around the coding exercises.   This course is all about getting you writing code ASAP, rather than sitting back watching a bunch of videos.  <br />\r\n<br />\r\nIn development, I referred to this course as a hybrid between a typical Udemy course and an interactive Codecademy-style course.  You get over 25 hours of in-depth videos and my dumb jokes along with meticulously created exercises and quizzes to test your knowledge as you go.<br />\r\n<br />\r\nMany of the older Python courses still focus on Python 2.  This course is all about writing the most modern, up-to-date Python code, so Python 3 was the obvious choice.  The course covers all the latest additions and changes to the Python language.  The course also places a large emphasis on thinking like a Python developer, and writing code the \"Pythonic\" way.<br />\r\n<br />\r\nAs for the curriculum, This course teaches all the key topics covered by other Python courses, but also covers more advanced topics like web scraping, crawling, and testing, just to name a few.  Please feel free to explore the curriculum and watch some of the free preview videos!<br />\r\n<br />\r\nPython is consistently ranked in either first or second place as the most in-demand programming languages across the job market.  It has applications in data science, machine learning, web development, self-driving cars, automation, and many many other disciplines.  There has never been a better time to learn it!<br />\r\n<br />\r\nI'm spent years teaching people to program at in-person bootcamps in the San Francisco Bay Area.  In recent years, I've started bringing my methods from the classroom to the online world. In my first year teaching online, I was selected as the Best Newcomer Instructor in the Udemy Instructor Awards 2015.  I'm passionate about making the best possible online learning experiences that mirror my in-person courses.<br />\r\n<br />\r\nIf you want to take ONE COURSE to master Python take this course.<br />\r\n<br />\r\nWho is the target audience?<br />\r\nAnyone who wants to learn Python.<br />\r\nAnyone who is curious about data science, machine learning, or web development<br />\r\nAnyone who wants to get tons of practice with the interactive exercises.<br />\r\n",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 11:36:10",
  "d_modified": "2018-06-05 11:39:09",
  "l_instructor_details": "Hi! I'm Colt. I'm a developer with a serious love for teaching. I've spent the last few years teaching people to program at 2 different immersive bootcamps where I've helped hundreds of people become web developers and change their lives. My graduates work at companies like Google, Salesforce, and Square.<br />\r\n<br />\r\nMost recently, I led Galvanize's SF's 6 month immersive program as Lead Instructor and Curriculum Director. After graduating from my class, 94% of my students went on to receive full-time developer roles. I also worked at Udacity as a Senior Course Developer on the web development team where I got to reach thousands of students daily.<br />\r\n<br />\r\nI’ve since focused my time on bringing my classroom teaching experience to an online environment. In 2016 I launched my Web Developer Bootcamp course, which has since gone on to become one of the best selling and top rated courses on Udemy. I was also voted Udemy’s Best New Instructor of 2016.<br />\r\n<br />\r\nI've spent years figuring out the \"formula\" to teaching technical skills in a classroom environment, and I'm really excited to finally share my expertise with you. I can confidently say that my online courses are without a doubt the most comprehensive ones on the market.<br />\r\n<br />\r\nJoin me on this crazy adventure!<br />\r\n<br />\r\n",
  "f_price": 520,
  "section": [
    {
      "v_title": " Python3 vs. Python2",
      "video": {
        "v_title": [
          " Python3 vs. Python2"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528198747.mp4"
        ]
      },
      "doc": {
        "v_title": [
          " Python3 vs. Python2"
        ],
        "v_doc": [
          "courses/video/section_doc-1528198749."
        ]
      },
      "_id": ObjectId("5b16765b76fbae3d430f74d5")
    }
  ],
  "v_search_tags": [
    " Python3",
    "Python2"
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528198735.jpg",
  "l_video": "courses/video/introduction_video-1528198745.mp4",
  "e_sponsor": "no",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  }
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b1676d976fbae5f5d707bd2"),
  "v_title": "The Python Bible™ | Everything You Need to Program in Python",
  "v_sub_title": "Build 11 Projects and go from Beginner to Pro in Python with the World's Most Fun Project-Based Python Course!",
  "v_author_name": "Katherine Read",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "2 month ",
  "i_category_id": "5ac47914d3e812b32f3c9869",
  "v_requirement": "Access to a computer with an internet connection.<br />\r\nHave a fun upbeat attitude and be ready to become awesome!",
  "l_description": "\"Just WOW, if you want to start Python, this is the place! - \" David Cristea ★★★★★<br />\r\n<br />\r\n\"If you can take just one Python course, make sure it's this one.\" - A. Barbosa ★★★★★<br />\r\n<br />\r\n\"The information is extremely well presented. Best Python training I have found so far, and I have been looking for several weeks!\" - Tanara ★★★★★<br />\r\n<br />\r\n\"I have tried many python courses on Udemy but this one is the best of them all.\" - Natalie ★★★★★<br />\r\n<br />\r\n",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 11:41:13",
  "d_modified": "2018-06-05 11:43:17",
  "l_instructor_details": "Ziyad Yehia<br />\r\nEnergetic Udemy Instructor with a Project-based Approach<br />\r\nHi there, I'm Ziyad! I am a Udemy Instructor with over 40,000 students and I specialize in getting beginner level students up and running with Python and Linux with my highly effective project-based approach. <br />\r\n<br />\r\nI previously taught computing to students at an UK-based international college and now put my full efforts into providing an outstanding learning experience to students around the world on Udemy.<br />\r\n<br />\r\nI am best known for my results-driven teaching as well as my clear, energetic delivery. If you like to learn things quickly and thoroughly and like to have loads of fun on the way, then I cannot wait to work with you and help you to achieve your goals!<br />\r\n<br />\r\nZiyad Yehia<br />\r\nEnergetic Udemy Instructor with a Project-based Approach<br />\r\nHi there, I'm Ziyad! I am a Udemy Instructor with over 40,000 students and I specialize in getting beginner level students up and running with Python and Linux with my highly effective project-based approach. <br />\r\n<br />\r\nI previously taught computing to students at an UK-based international college and now put my full efforts into providing an outstanding learning experience to students around the world on Udemy.<br />\r\n<br />\r\nI am best known for my results-driven teaching as well as my clear, energetic delivery. If you like to learn things quickly and thoroughly and like to have loads of fun on the way, then I cannot wait to work with you and help you to achieve your goals!<br />\r\n<br />\r\n",
  "f_price": "563",
  "section": [
    {
      "v_title": "Installing Python on Windows ",
      "video": {
        "v_title": [
          "Installing Python on Windows "
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528198995.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Installing Python on Windows "
        ],
        "v_doc": [
          "courses/video/section_doc-1528198997."
        ]
      },
      "_id": ObjectId("5b16775376fbae58ea6c3a14")
    }
  ],
  "v_search_tags": [
    " Python",
    "bible Course"
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528198989.jpg",
  "l_video": "courses/video/introduction_video-1528198993.mp4",
  "e_sponsor": "no",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  }
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b16778476fbae599423cd22"),
  "v_title": "React 16 - The Complete Guide (incl. React Router 4 & Redux)",
  "v_sub_title": "Dive in and learn React from scratch! Learn Reactjs, Redux, React Routing, Animations, Next.js basics and way more!",
  "v_author_name": "Katherine Read",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae7d3e812cf243c986a",
  "i_duration": "5 month",
  "i_category_id": "5ac47921d3e812b52f3c9869",
  "v_requirement": "JavaScript + HTML + CSS fundamentals are absolutely required<br />\r\nYou DON'T need to be a JavaScript expert to succeed in this course!<br />\r\nES6+ JavaScript knowledge is beneficial but not a must-have<br />\r\nNO prior React or any other JS framework experience is required!",
  "l_description": "What's this course about?<br />\r\n<br />\r\nLearn React or dive deeper into it. Learn the theory, solve assignments, practice in demo projects and build one big application which is improved throughout the course: The Burger Builder!<br />\r\n<br />\r\nMore details please!<br />\r\n<br />\r\nJavaScript is the major driver of modern web applications since it's the only programming language which runs in the browser and hence allows you to provide highly reactive apps. You'll be able to achieve mobile-app like user experiences in the web.<br />\r\n<br />\r\nBut using JavaScript can be challenging - it quickly becomes overwhelming to create a nice web app with vanilla JavaScript and jQuery only.<br />\r\n<br />\r\nReact to the rescue! <br />\r\n<br />\r\nReact is all about components - basically custom HTML elements - with which you can quickly build amazing and powerful web apps. Just build a component once, configure it to your needs, dynamically pass data into it (or listen to your own events!) and re-use it as often as needed.<br />\r\n<br />\r\nNeed to display a list of users in your app? It's as simple as creating a \"User\" component and outputting it as often as needed.<br />\r\n<br />\r\nThis course will start at the very basics and explain what exactly React is and how you may use it (and for which kind of apps). Thereafter, we'll go all the way from basic to advanced. We'll not just scratch the surface but dive deeply into React as well as popular libraries like react-router and Redux. <br />\r\n<br />\r\nBy the end of the course, you can build amazing React (single page) applications!<br />\r\n<br />\r\nA detailed list with the course content can be found below.<br />\r\n<br />\r\nWho's teaching you in this course?<br />\r\n<br />\r\nMy name is Maximilian Schwarzmüller, I'm a freelance web developer and worked with React in many projects. I'm also a 5-star rated instructor here on Udemy. I cover React's most popular alternatives - Vue and Angular - as well as many other topics. I know what I'm talking about and I know where the pain points can be found.<br />\r\n<br />\r\nIt's my goal to get you started with React as quick as possible and ensure your success. But I don't just focus on students getting started. I want everyone to benefit from my courses, that's why we'll dive deeply into React and why I made sure to also share knowledge that's helpful to advanced React developers. <br />\r\n<br />\r\nIs this course for you?<br />\r\n<br />\r\nThis course is for you if ...<br />\r\n<br />\r\n...you're just getting started with frontend/ JavaScript development and only got the JS basics set (no prior React or other framework experience is required!)<br />\r\n...you're experienced with Angular or Vue but want to dive into React<br />\r\n...know the React basics but want to refresh them and/ or dive deeper<br />\r\n...already worked quite a bit with React but want to dive deeper and see it all come together in a bigger app<br />\r\nWhat should you bring to succeed in that course?<br />\r\n<br />\r\nHTML + CSS + JavaScript knowledge is required. You don't need to be an expert but the basics need to be set<br />\r\nNO advanced JavaScript knowledge is required, though you'll be able to move even quicker through the course if you know next-gen JavaScript features like ES6 Arrow functions. A short refresher about the most important next-gen features is provided in the course though.<br />\r\nWhat's inside the course?<br />\r\n<br />\r\nThe \"What\", \"Why\" and \"How\"<br />\r\nReact Basics (Base features, syntax and concepts)<br />\r\nHow to output lists and conditional content<br />\r\nStyling of React components<br />\r\nA deep dive into the internals of React and advanced component features<br />\r\nHow to access Http content from within React apps (AJAX)<br />\r\nRedux, Redux, Redux ... from basics to advanced!<br />\r\nForms and form validation in React apps<br />\r\nAuthentication<br />\r\nAn introduction to unit testing<br />\r\nAn introduction to Next.js<br />\r\nReact app deployment instructions<br />\r\n...and much more!<br />\r\nWho is the target audience?<br />\r\nStudents who want to learn how to build reactive and fast web apps<br />\r\nAnyone who's interested in learning an extremely popular technology used by leading tech companies like Netflix<br />\r\nStudents who want to take their web development skills to the next level and learn a future-proof technology<br />\r\n",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 11:44:04",
  "d_modified": "2018-06-05 11:49:05",
  "l_instructor_details": "Bundling the courses and know how of successful instructors, Academind strives to deliver high quality online education. <br />\r\n<br />\r\nOnline Education, Real-Life Success - that's what Academind stands for. Learn topics like web development, data analyses and more in a fun and engaging way.<br />\r\n<br />\r\nCurrently, you can find courses published by Maximilian Schwarzmüller and Manuel Lorenz, more instructors to come!<br />\r\n<br />\r\nKeep learning!<br />\r\n<br />\r\n",
  "f_price": 50,
  "section": [
    {
      "v_title": " Real-World SPAs & React Web Apps ",
      "video": {
        "v_title": [
          " Real-World SPAs & React Web Apps "
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528199341.mp4"
        ]
      },
      "doc": {
        "v_title": [
          " Real-World SPAs & React Web Apps "
        ],
        "v_doc": [
          "courses/video/section_doc-1528199343."
        ]
      },
      "_id": ObjectId("5b1678ad76fbae5e482ecbf2")
    },
    {
      "v_title": "Adding the Right React Version to Codepen",
      "_id": ObjectId("5b1678af76fbae5e482ecbf3")
    },
    {
      "video": {
        "v_title": [
          "Adding the Right React Version to Codepen"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528199343.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Adding the Right React Version to Codepen"
        ],
        "v_doc": [
          "courses/video/section_doc-1528199345."
        ]
      },
      "_id": ObjectId("5b1678af76fbae5e482ecbf4")
    }
  ],
  "v_search_tags": [
    "React"
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528199333.jpg",
  "l_video": "courses/video/introduction_video-1528199339.mp4",
  "e_sponsor": "no",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  }
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b16791676fbae5a7c5c68d2"),
  "v_title": "Test Data Generation for Selenium and Appium",
  "v_sub_title": " Third Party API tools to generate Random Unique Test Data in Java and C#",
  "v_author_name": "Katherine Read",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae7d3e812cf243c986a",
  "i_duration": "3 month",
  "i_category_id": "5ac47921d3e812b52f3c9869",
  "v_requirement": "Basic Knowledge of Automation Testing<br />\r\nBasic Knowledge of Selenium or Appium<br />\r\nBasic Knowledge of Java or C#<br />\r\nBasic Knowledge of Test Data<br />\r\nBasic Automation Framework Knowledge.<br />\r\n",
  "l_description": "Test data is very important in manual testing as well as in automation testing. But if the test data changes in manual testing then it is not a big issue when compared to automation testing. As in the manual testing the tester can change/create the data as he is aware that the data changes and he will have enough application knowledge. But when it comes to the automation, the tool can not have this kind of knowledge or information. To avoid these kind of issues we can use some third party APIs to generate random unique test data or fake data.",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 11:50:46",
  "d_modified": "2018-06-05 11:53:44",
  "l_instructor_details": "I am Krishna Sakinala, Working as Specialist Quality Lead in one of the Top CMMI Level 5 Organizations. I call myself as Passionate Tester. My Hobbies are writing technical blogs, Learning and Researching on new automation tools and automation related topics. I possess 10+ years of experience in manual and automation testing tools like selenium and appium as well as web services testing.",
  "f_price": "360",
  "section": [
    {
      "v_title": "Test Data Generation for Automation Testing - Selenium& Appium",
      "video": {
        "v_title": [
          "Test Data Generation for Automation Testing - Selenium& Appium"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528199622.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Test Data Generation for Automation Testing - Selenium& Appium"
        ],
        "v_doc": [
          "courses/video/section_doc-1528199624."
        ]
      },
      "_id": ObjectId("5b1679c676fbae5e4601d982")
    }
  ],
  "v_search_tags": [
    "Selenium",
    " Appium"
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528199615.jpg",
  "l_video": "courses/video/introduction_video-1528199621.mp4",
  "e_sponsor": "no",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  }
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b167a7676fbae599423cd23"),
  "v_title": "Modern JavaScript From The Beginning",
  "v_sub_title": "Learn and build projects with pure JavaScript (No frameworks or libraries)",
  "v_author_name": "Katherine Read",
  "i_language_id": "5ac471b0d3e8124a1e3c9869",
  "i_level_id": "5ac47aeed3e812b32f3c986a",
  "i_duration": "2 month",
  "i_category_id": "5ac4792cd3e81293243c986a",
  "v_requirement": "Basic HTML / CSS knowledge",
  "l_description": "This is a front to back JavaScript course for absolutely everybody. We start with the basic fundamentals and work our way to advanced programming WITHOUT relying on frameworks or libraries at all. You will learn a ton of pure JavaScript, whether you are a beginner or an established JS programmer. There is something for everyone...",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 11:56:38",
  "d_modified": "2018-06-05 11:58:20",
  "l_instructor_details": "Full Stack Web Developer & Instructor at Traversy Media<br />\r\nBrad Traversy has been programming for around 12 years and teaching for almost 5 years. He is the owner of Traversy Media which is a successful web development YouTube channel and specializes in everything from HTML5 to front end frameworks like Angular as well as server side technologies like Node.js, PHP and Python. Brad has mastered explaining ",
  "f_price": 250,
  "section": [
    {
      "v_title": "Project Files & Questions",
      "video": {
        "v_title": [
          "Project Files & Questions"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528199898.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Project Files & Questions"
        ],
        "v_doc": [
          "courses/video/section_doc-1528199899."
        ]
      },
      "_id": ObjectId("5b167ada76fbae599423cd24")
    }
  ],
  "v_search_tags": [
    "Questions"
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528199891.jpg",
  "l_video": "courses/video/introduction_video-1528199896.mp4",
  "e_sponsor": "no",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  }
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b167b5876fbae594f236912"),
  "v_title": "Advance Hadoop and Hive for Testers",
  "v_sub_title": "Hadoop and Hive for Big Data Testing",
  "v_author_name": "Katherine Read",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47aeed3e812b32f3c986a",
  "i_duration": "2 Month",
  "i_category_id": "5ac4792cd3e81293243c986a",
  "v_requirement": "As such there is no prerequisite required for this course and it is useful for any level of tester. Any Tester profile candidate can start this tutorial.<br />\r\nAny QA can start this course who has basic knowledge of any database and Unix. There is no specific prerequisite other than this.<br />\r\n",
  "l_description": "This course is specially designed for Testing profile students who wanted to build there career into Big Data Testing. So I have designed this course so they can start working with Hadoop and Hive in big data testing. All the users who are working in QA profile and wanted to move into big data testing domain should take this course and go through the complete tutorials which has advance knowledge.<br />\r\n<br />\r\nI have included the material which is needed for big data testing profile and it has all the necessary contents which is required for learning Hadoop, Hive and Unix.<br />\r\n<br />\r\nIt will give the detailed information for different Hadoop and Hive commands which is needed by the tester to move into bigger umbrella i.e. Big Data Testing.<br />\r\n<br />\r\nThis course is well structured with all elements of different Hadoop, Hive with advance commands in practical manner separated by different topics. Students should take this course who wanted to learn Hadoop, Hive and advance Unix from scratch.<br />\r\n<br />\r\nWho is the target audience?<br />\r\nAny Level of Testing Profile Candidate is the target audience for this course.<br />\r\nAny big data QA beginner use this technology to enhance their skills.<br />\r\n",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 12:00:24",
  "d_modified": "2018-06-05 12:02:18",
  "l_instructor_details": "Lead Big Data Engineer<br />\r\nLead Big Data Engineer<br />\r\nHaving more than 10 years industry experience in DevOps and QA Profile.<br />\r\n<br />\r\nWorked on different big data tools and technologies like Hadoop, Cassandra, HBase, Hive, Pig, Sqoop, Flume, Spark and Scala etc. <br />\r\n<br />\r\nISTQB and SOAPUI Certified professional. I have worked with CMMi level 5 companies and provided services to clients in fortune 15 companies like AT&T, American Express.<br />\r\n<br />\r\nTrained more than 250 professionals in classroom training and more than 100 professionals online.<br />\r\n<br />\r\nTraining has been my passion and I am on my way to create courses which will help any beginner/tester to step by step learn and be able to become a expert in Big data QA.<br />\r\n<br />\r\n",
  "f_price": 320,
  "section": [
    {
      "v_title": "5 V's of Big Data ",
      "video": {
        "v_title": [
          "5 V's of Big Data "
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528200136.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "5 V's of Big Data "
        ],
        "v_doc": [
          "courses/video/section_doc-1528200138."
        ]
      },
      "_id": ObjectId("5b167bc876fbae5e475580d3")
    }
  ],
  "v_search_tags": [
    " Big Data "
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528200130.jpg",
  "l_video": "courses/video/introduction_video-1528200134.mp4",
  "e_sponsor": "no",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  }
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b167c6076fbae5e4601d984"),
  "v_title": "Test Management Tools Package[ ALM , TestLink, JIRA ]",
  "v_sub_title": "Including Licensed and Free Test Management tool",
  "v_author_name": "Katherine Read",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "2 Month",
  "i_category_id": "5ac47921d3e812b52f3c9869",
  "v_requirement": "Windows machine with at least 2 GB ram",
  "l_description": "Test management most commonly refers to the activity of managing the computer software testing process. A test management tool is software used to manage tests (automated or manual) that have been previously specified by a test procedure. It is often associated with automation software.<br />\r\n<br />\r\nThis Course covers basic to advance concept of test management<br />\r\n<br />\r\nAny workflow: a great choice for any team, whether you use an agile or traditional approach.<br />\r\nAny team size: teams of one to 1000s of testers love working with TestRail every day.<br />\r\nAny method: manage your functional, exploratory and automated tests with ease.<br />\r\nWe also covers<br />\r\n<br />\r\nManage requirements, features and use-cases<br />\r\nCreate, edit and execute test-cases<br />\r\nTrack bugs, enhancements, risks and issues<br />\r\nMap tests to requirements to track coverage<br />\r\nSupport for template test cases<br />\r\nLink bugs to test steps during test execution<br />\r\nDrill down from requirements to tests and incidents<br />\r\n<br />\r\n<br />\r\n<br />\r\n<br />\r\n<br />\r\n<br />\r\nWho is the target audience?<br />\r\nTesting looking opportunity in project managemeny<br />\r\nFreshers looking opportunity in software testing<br />\r\n",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 12:04:48",
  "d_modified": "2018-06-05 12:06:04",
  "l_instructor_details": "Testing World<br />\r\nDirector of Testing World<br />\r\nHandling Testing World Operations<br />\r\n<br />\r\nInvolve in Testing Course Video update and management<br />\r\n<br />\r\nHandling online and Face to face classes of Testing World<br />\r\n<br />\r\nMasters In Computer Science<br />\r\n<br />\r\nExpertise in manual testing & Automation testing using Selenium, QTP, LoadRunner, Jmeter and Mobile Automation<br />\r\n<br />\r\nFound of Testing World<br />\r\n<br />\r\nTrained more than 4000 students online <br />\r\n<br />\r\nActively involved in corporate training<br />\r\n<br />\r\n",
  "f_price": 34,
  "section": [
    {
      "v_title": " Setup ALM account",
      "video": {
        "v_title": [
          " Setup ALM account"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528200363.mp4"
        ]
      },
      "doc": {
        "v_title": [
          " Setup ALM account"
        ],
        "v_doc": [
          "courses/video/section_doc-1528200364."
        ]
      },
      "_id": ObjectId("5b167cab76fbae5e475580d4")
    }
  ],
  "v_search_tags": [
    " ALM account"
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528200355.jpg",
  "l_video": "courses/video/introduction_video-1528200361.mp4",
  "e_sponsor": "yes",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  },
  "i_impression": NumberInt(26)
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b167d6476fbae62635ec3f4"),
  "v_title": "Business Analyst: Software Testing Processes & Techniques",
  "v_sub_title": "Reduce project bugs with repeatable testing processes. Learn to create test cases, scenarios, track defects, & more!",
  "v_author_name": "Katherine Read",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae7d3e812cf243c986a",
  "i_duration": "3 month",
  "i_category_id": "5ac4792cd3e81293243c986a",
  "v_requirement": "This course covers all of the fundamentals. No prior knowledge is required.<br />\r\nKnowledge of the role and responsibilities of a Business Analyst is helpful, but not required.",
  "l_description": "Increase your value and bolster your career by enhancing your Business Analysis skills with Software Testing processes and techniques!<br />\r\n<br />\r\nIn today's world, organizations are expecting Business Analysts to not only elicit and document requirements, but also to be engaged throughout the full life cycle of the project. One of the most important aspects to any project is the testing that is performed. Bugs and defects can sink your projects. <br />\r\n<br />\r\nThis course will teach you, using real-world examples, how to successfully perform software testing.",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 12:09:08",
  "d_modified": "2018-06-05 12:10:48",
  "l_instructor_details": "The BA Guide | Jeremy Aschenbrenner<br />\r\nBusiness Analyst Trainer & Coach | Best Selling Instructor<br />\r\nCome learn with more than 31,000+ students!<br />\r\n<br />\r\nHi there! My name is Jeremy Aschenbrenner and I am so glad you have found me!<br />\r\n<br />\r\nI am a veteran Business Analyst with more than 1000 completed projects. I've done business analysis work for companies in many industries including health care, telecommunications, automotive, trucking, consumer goods, financial services, food and beverage, and more.<br />\r\n<br />\r\nI created The BA Guide to break down the barriers to becoming a Business ",
  "f_price": 41,
  "section": [
    {
      "v_title": "Lecture Slides",
      "video": {
        "v_title": [
          "Lecture Slides"
        ],
        "i_preview": [
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1528200646.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Lecture Slides"
        ],
        "v_doc": [
          "courses/video/section_doc-1528200648."
        ]
      },
      "_id": ObjectId("5b167dc676fbae53d553ed44")
    }
  ],
  "v_search_tags": [
    "Lecture Slides",
    "Business"
  ],
  "e_status": "published",
  "v_instructor_image": "courses/instructor_image-1528200640.jpg",
  "l_video": "courses/video/introduction_video-1528200645.mp4",
  "e_sponsor": "no",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "review": {
    "total": NumberInt(0),
    "avgtotal": "0"
  }
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b3a1fdb516b460a68003e69"),
  "v_title": "D course",
  "v_sub_title": "D course",
  "v_author_name": "Ray Nanda",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "100",
  "i_category_id": "5ac478d9d3e812e9233c986b",
  "v_requirement": "Course Requirments",
  "l_description": "Course Description",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "d_added": "2018-07-02 12:51:39",
  "d_modified": "2018-08-06 17:58:32",
  "e_sponsor": "yes",
  "e_status": "published",
  "f_price": 100,
  "l_instructor_details": "Instructor Details",
  "l_video": "courses/video/introduction_video-1530536020.mp4",
  "section": [
    {
      "v_title": "Section 1",
      "video": {
        "v_title": [
          "Section 1 Video",
          "Section 1 Video 2"
        ],
        "i_preview": [
          "off",
          "off"
        ],
        "v_video": [
          "courses/video/section_video-1530536029.mp4",
          "courses/video/section_video-1530536035.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "Section 2 Video"
        ],
        "v_doc": [
          "courses/video/section_doc-1530536041.pdf"
        ]
      },
      "_id": ObjectId("5b3a205d516b460a68003e6a")
    }
  ],
  "v_instructor_image": "courses/instructor_image-1530536018.jpg",
  "v_search_tags": [
    "das",
    "sa"
  ],
  "review": {
    "total": NumberInt(1),
    "avgtotal": NumberInt(5)
  },
  "i_impression": NumberInt(28),
  "e_sponsor_status": "start"
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b5f42b61b544d57a2399ea2"),
  "v_title": "asdsadsad",
  "v_sub_title": "sadsadsad",
  "v_author_name": "Ray Nanda",
  "i_language_id": "5ac471f7d3e812271b3c9869",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "1235",
  "i_category_id": "5ac47950d3e812cf243c9869",
  "v_requirement": "asdasdsa",
  "l_description": "asdsadsadsa",
  "review": {
    "total": NumberInt(0),
    "avgtotal": NumberInt(0)
  },
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "d_added": "2018-07-30 16:54:14",
  "d_modified": "2018-08-06 18:00:43",
  "e_sponsor": "yes",
  "e_status": "published",
  "f_price": NumberInt(0),
  "l_instructor_details": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br />\r\n<br />\r\n",
  "l_video": "courses/video/introduction_video-1532969783.mp4",
  "section": [
    {
      "v_title": "asdasdasdas",
      "video": {
        "v_title": [
          "asdadsadsadas"
        ],
        "i_preview": [
          "on"
        ],
        "v_video": [
          "courses/video/section_video-1532969786.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "sadsadsadsadsa"
        ],
        "v_doc": [
          "courses/video/section_doc-1532969787.jpeg"
        ]
      },
      "_id": ObjectId("5b5f433a1b544d579e75fc82")
    }
  ],
  "v_instructor_image": "courses/instructor_image-1532969781.jpeg",
  "v_search_tags": [
    "sadsadsa",
    "asdasdsadsa",
    "asdsadasdas"
  ],
  "i_impression": NumberInt(26),
  "e_sponsor_status": "pause"
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b6bed8d1b544d16406dc483"),
  "v_title": "Hands-On Python & R In Data Science",
  "v_sub_title": "aa",
  "v_author_name": " Machine",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "Machine",
  "i_category_id": "5ac478d9d3e812e9233c986b",
  "v_requirement": "This course has been designed by two professional Data Scientists so that we can share our knowledge and help you learn complex theory, algorithms and coding libraries in a simple way.<br />\r\n<br />\r\n",
  "l_description": "This course has been designed by two professional Data Scientists so that we can share our knowledge and help you learn complex theory, algorithms and coding libraries in a simple way.<br />\r\n<br />\r\n",
  "review": {
    "total": NumberInt(0),
    "avgtotal": NumberInt(0)
  },
  "i_user_id": "5b6beb561b544d16406dc482",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "d_added": "2018-08-09 07:30:21",
  "d_modified": "2018-08-09 07:30:21"
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b6bed8f1b544d16406dc484"),
  "v_title": "Hands-On Python & R In Data Science",
  "v_sub_title": "aa",
  "v_author_name": " Machine",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "Machine",
  "i_category_id": "5ac478d9d3e812e9233c986b",
  "v_requirement": "This course has been designed by two professional Data Scientists so that we can share our knowledge and help you learn complex theory, algorithms and coding libraries in a simple way.<br />\r\n<br />\r\n",
  "l_description": "This course has been designed by two professional Data Scientists so that we can share our knowledge and help you learn complex theory, algorithms and coding libraries in a simple way.<br />\r\n<br />\r\n",
  "review": {
    "total": NumberInt(0),
    "avgtotal": NumberInt(0)
  },
  "i_user_id": "5b6beb561b544d16406dc482",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "d_added": "2018-08-09 07:30:23",
  "d_modified": "2018-08-09 07:30:23"
});
db.getCollection("tbl_courses").insert({
  "_id": ObjectId("5b6bed991b544d0ec8598b62"),
  "v_title": "Hands-On Python & R In Data Science",
  "v_sub_title": "aa",
  "v_author_name": " Machine",
  "i_language_id": "5ac471b8d3e8124a1e3c986a",
  "i_level_id": "5ac47ae0d3e81268343c9869",
  "i_duration": "Machine",
  "i_category_id": "5ac478d9d3e812e9233c986b",
  "v_requirement": "This course has been designed by two professional Data Scientists so that we can share our knowledge and help you learn complex theory, algorithms and coding libraries in a simple way.<br />\r\n<br />\r\n",
  "l_description": "This course has been designed by two professional Data Scientists so that we can share our knowledge and help you learn complex theory, algorithms and coding libraries in a simple way.<br />\r\n<br />\r\n",
  "review": {
    "total": NumberInt(0),
    "avgtotal": NumberInt(0)
  },
  "i_user_id": "5b6beb561b544d16406dc482",
  "e_qa_enabled": "on",
  "e_reviews_enabled": "on",
  "e_announcements_enabled": "on",
  "d_added": "2018-08-09 07:30:33",
  "d_modified": "2018-08-09 07:33:14",
  "e_sponsor": "no",
  "e_status": "published",
  "f_price": NumberInt(0),
  "l_instructor_details": "This course has been designed by two professional Data Scientists so that we can share our knowledge and help you learn complex theory, algorithms and coding libraries in a simple way.<br />\r\n<br />\r\n",
  "l_video": "courses/video/introduction_video-1533799962.mp4",
  "section": [
    {
      "v_title": "This course has been designed by two professional Data Scientists so that we can share our knowledge and help you learn complex theory, algorithms and coding libraries in a simple way.",
      "video": {
        "v_title": [
          "This course has been designed by two professional Data Scientists so that we can share our knowledge and help you learn complex theory, algorithms and coding libraries in a simple way."
        ],
        "i_preview": [
          "on"
        ],
        "v_video": [
          "courses/video/section_video-1533799973.mp4"
        ]
      },
      "doc": {
        "v_title": [
          "This course has been designed by two professional Data Scientists so that we can share our knowledge and help you learn complex theory, algorithms and coding libraries in a simple way."
        ],
        "v_doc": [
          "courses/video/section_doc-1533799986.pdf"
        ]
      },
      "_id": ObjectId("5b6bee251b544d13c430ac92")
    }
  ],
  "v_instructor_image": "courses/instructor_image-1533799960.jpeg",
  "v_search_tags": [
    "app",
    "sss",
    "aex"
  ]
});

/** tbl_email_template records **/
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5a60a0f1d3e812bc4b3c986a"),
  "v_title": "Forgot password",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Forgot password",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">FORGOT PASSWORD</h1>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tYou have requested a password reset, please follow the link below to reset your password.</p>\r\n\r\n\t\t\t<p><br />\r\n\t\t\t<a href=\"{PASSWORD_RESET_LINK}\">Click here to reset your password.</a></p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-01-18 13:28:17",
  "d_modified": "2018-08-08 03:31:46"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5a60a203d3e812b94b3c986b"),
  "v_title": "Newsletter",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Newsletter",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "",
  "e_status": "inactive",
  "d_added": "2018-01-18 13:32:51",
  "d_modified": "2018-04-19 04:47:34"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5ad81f45d3e812c30b3c9869"),
  "v_title": "Confirm your account",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Confirm your account",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Confirm your account</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tPlease find below confirm your account link<br />\r\n\t\t\t<br />\r\n\t\t\t<a href=\"{CONFIRM_YOUR_ACCOUNT_LINK}\">Click here to confirm.</a></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-04-19 04:47:01",
  "d_modified": "2018-07-25 11:53:03"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b093cfb76fbae5f4402b223"),
  "v_title": "Apply for job",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Apply for job",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Apply for job</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>Hello <span>{USER_FULL_NAME}</span></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-05-26 10:54:51",
  "d_modified": "2018-05-26 10:55:33"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b0e749376fbae2feb21ecb2"),
  "v_title": "Account verified",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Account verified",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">&nbsp;Account verified</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tYour account has been successfully verified<br />\r\n\t\t\t&nbsp;</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-05-30 09:53:23",
  "d_modified": "2018-05-30 09:53:23"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b0e7dbf76fbae5b834b02e3"),
  "v_title": "Order cancelled",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Order cancelled",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Order cancelled</h1>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #f59329;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top:44px; width:500px\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td><strong>Hi {USER_FULL_NAME},</strong>\r\n\r\n\t\t\t<p>Order cancelled.</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-05-30 10:32:31",
  "d_modified": "2018-05-30 10:47:42"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b0e89fc76fbae6964350442"),
  "v_title": "Course order inform seller",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Course order information",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Order information</h1>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #f59329;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top:44px; width:500px\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td><strong>Hi {USER_FULL_NAME},</strong>\r\n\r\n\t\t\t<p>Order placed.please find below information</p>\r\n\r\n\t\t\t<p>{COURSE_ORDER_DETAIL}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-05-30 11:24:44",
  "d_modified": "2018-05-30 11:24:44"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b0e8ace76fbae69ad6587c2"),
  "v_title": "Course announcement  inform to buyer",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Course announcement",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Course Announcement</h1>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #f59329;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top:44px; width:500px\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td><strong>Hi {USER_FULL_NAME},</strong>\r\n\r\n\t\t\t<p>Course announcement by seller.</p>\r\n\r\n\t\t\t<p>{COURSE_ANNOUNCEMENT}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-05-30 11:28:14",
  "d_modified": "2018-05-30 11:28:14"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b17826c76fbae1c5e740b02"),
  "v_title": "Password confirmation",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Password confirmation",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">PASSWORD CONFIRMATION</h1>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tYour password has been succesfully changed.</p>\r\n\r\n\t\t\t<p><a href=\"{LOGIN_URL}\">Click hear to login.</a></p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-06-06 06:42:52",
  "d_modified": "2018-06-06 06:46:55"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b17abfa76fbae34dd205c03"),
  "v_title": "Reset Password",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Reset Password",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">RESET PASSWORD</h1>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tYour password has been reset.please find new password below.</p>\r\n\r\n\t\t\t<p>Password :- {PASSWORD}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-06-06 09:40:10",
  "d_modified": "2018-06-06 09:40:10"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b1a1d7e76fbae10da3ec042"),
  "v_title": "Course QA to seller",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Course QA to seller",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Course QA</h1>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #f59329;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top:44px; width:500px\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td><strong>Hi {SELLER_NAME},</strong>\r\n\r\n\t\t\t<p>{BUYER_NAME}&nbsp;asking questions about {COURSE_NAME} course.</p>\r\n\r\n\t\t\t<p>Please find following detail.</p>\r\n\r\n\t\t\t<p><strong>Title</strong> :- {COURSE_QUESTION_TITLE}</p>\r\n\r\n\t\t\t<p><strong>Question</strong> :- {COURSE_QUESTION}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-06-08 06:09:02",
  "d_modified": "2018-06-08 08:20:05"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b1a20a876fbae0b95669873"),
  "v_title": "Course QA to buyer",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Course QA to buyer",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Course QA</h1>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #f59329;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top:44px; width:500px\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td><strong>Hi {BUYER_NAME},</strong>\r\n\r\n\t\t\t<p>{SELLER_NAME} posted&nbsp;&nbsp;answer about&nbsp;questions of course.</p>\r\n\r\n\t\t\t<p>Please find following detail.</p>\r\n\r\n\t\t\t<p><strong>Title</strong>&nbsp;:- {COURSE_QUESTION_TITLE}</p>\r\n\r\n\t\t\t<p><strong>Question</strong>&nbsp;:- {COURSE_QUESTION}</p>\r\n\r\n\t\t\t<p><strong>Answer</strong>&nbsp;:- {COURSE_ANSWER}</p>\r\n\r\n\t\t\t<p>&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-06-08 06:22:32",
  "d_modified": "2018-06-08 09:13:09"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b1a211376fbae19586f56b2"),
  "v_title": "Course review",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Course review",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Course Review</h1>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #f59329;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top:44px; width:500px\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td><strong>Hi {SELLER_NAME},</strong>\r\n\r\n\t\t\t<p>{BUYER_NAME}&nbsp;left review on your course.</p>\r\n\r\n\t\t\t<p>Please find following detail.</p>\r\n\r\n\t\t\t<p>{COURSE_REVIEW}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-06-08 06:24:19",
  "d_modified": "2018-06-08 06:24:19"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b1a216e76fbae0b95669874"),
  "v_title": "Course review replies",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Course review replies",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Course Review Replies</h1>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #f59329;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top:44px; width:500px\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td><strong>Hi {BUYER_NAME},</strong>\r\n\r\n\t\t\t<p>{SELLER_NAME} replay for your review of course.</p>\r\n\r\n\t\t\t<p>Please find following detail.</p>\r\n\r\n\t\t\t<p>{COURSE_REVIEW}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-06-08 06:25:50",
  "d_modified": "2018-06-08 06:25:50"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b28f90676fbae5e650edc12"),
  "v_title": "Signup successfully",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Signup successfully",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "abc@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Signup successfully</h1>\r\n\r\n\t\t\t\t\t\t<hr style=\"border-style: solid; border-color: rgb(232, 18, 140); border-image: initial;\" /></div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tYou have successfully signed up in skillbox</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-06-19 12:37:26",
  "d_modified": "2018-06-19 12:37:26"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b55fe15516b462cb0001aaa"),
  "v_title": "Job post live",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Job post live",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">&nbsp;Job post preview</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tYour job is posted successfully.<a href=\"{JOB_PREVIEW_LINK}\">Click here</a> to preview your job post<br />\r\n\t\t\t&nbsp;</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-07-23 16:11:01",
  "d_modified": "2018-07-23 16:11:01"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b560ad1516b462ef00036a4"),
  "v_title": " Applied Job",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": " Applied Job",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">&nbsp;Seller applied for job</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tSeller has been applied for {JOB_NAME} jobs. <a href=\"{JOB_RESPONSE_LINK}\">click here</a> to view your job post reponse.<br />\r\n\t\t\t&nbsp;</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-07-23 17:05:21",
  "d_modified": "2018-07-23 05:26:47"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b561895516b462ef00036ad"),
  "v_title": "Deliver order",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Deliver order",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">&nbsp;Order&nbsp;Deliver&nbsp;</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tSeller deliver order please find below detail.</p>\r\n\r\n\t\t\t<p>Order no :- {ORDER_NO}</p>\r\n\r\n\t\t\t<p>Order Title:- {ORDER_TITLE}</p>\r\n\r\n\t\t\t<p>Seller name :- {SELLER_NAME}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-07-23 18:04:05",
  "d_modified": "2018-07-23 18:04:05"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b575807516b460db00053ed"),
  "v_title": "Order placed",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Order placed",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">&nbsp;Order placed</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\t{BUYER_FULL_NAME} purchase your skill successfully.</p>\r\n\r\n\t\t\t<p><a href=\"{SELLER_ORDER_LINK}\">Click here</a> to view your order.</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-07-24 16:47:03",
  "d_modified": "2018-07-24 05:37:04"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b5768b8516b460db00053ef"),
  "v_title": "Order requirements Seller",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Order requirements",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Order Requirement</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tBuyer has posted order requirements.please find below detail.</p>\r\n\r\n\t\t\t<p>Order no :- {ORDER_NO}</p>\r\n\r\n\t\t\t<p>Order no :- {ORDER_TITLE}</p>\r\n\r\n\t\t\t<p>Requirements&nbsp; :- {ORDER_MESSAGE}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-07-24 17:58:16",
  "d_modified": "2018-07-24 05:58:40"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b581eff1b544d14d61d5fa2"),
  "v_title": "Order requirements Buyer",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Order requirements Buyer",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Order Requirement</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tSeller has posted message on order requirements.please find below detail.</p>\r\n\r\n\t\t\t<p>Order no :- {ORDER_NO}</p>\r\n\r\n\t\t\t<p>Order no :- {ORDER_TITLE}</p>\r\n\r\n\t\t\t<p>Requirements&nbsp; :- {ORDER_MESSAGE}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-07-25 06:55:59",
  "d_modified": "2018-07-25 07:06:02"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b5822c51b544d14d97708d3"),
  "v_title": "Order cancel request to seller",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Order cancel request to seller",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Order Cancel Request</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tBuyer request for cancel oder.please find below detail</p>\r\n\r\n\t\t\t<p>Order no :- {ORDER_NO}</p>\r\n\r\n\t\t\t<p>Order Title :- {ORDER_TITLE}</p>\r\n\r\n\t\t\t<p>Subject :- {SUBJECT}</p>\r\n\r\n\t\t\t<p>Message&nbsp; :- {MESSAGE}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-07-25 07:12:05",
  "d_modified": "2018-07-25 07:32:52"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b5827c71b544d15094fbab2"),
  "v_title": "Order canceled  to buyer",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Order canceled  to buyer",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Order Canceled</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tSeller&nbsp;canceled your oder.please find below detail</p>\r\n\r\n\t\t\t<p>Order no :- {ORDER_NO}</p>\r\n\r\n\t\t\t<p>Order Title :- {ORDER_TITLE}</p>\r\n\r\n\t\t\t<p>&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-07-25 07:33:27",
  "d_modified": "2018-07-25 07:33:27"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b5851ac1b544d58f07925b3"),
  "v_title": "Order cancel request reply",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Order cancel request reply",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Order Cancel Request Reply</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tSeller reply for your order cancel request .please find below detail</p>\r\n\r\n\t\t\t<p>Order no :- {ORDER_NO}</p>\r\n\r\n\t\t\t<p>Order Title :- {ORDER_TITLE}</p>\r\n\r\n\t\t\t<p>Message :- {MESSAGE}</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-07-25 10:32:12",
  "d_modified": "2018-07-25 10:32:12"
});
db.getCollection("tbl_email_template").insert({
  "_id": ObjectId("5b585c111b544d5d44495722"),
  "v_title": "Job expires ",
  "i_header_id": "5a609fbfd3e812bc4b3c9869",
  "v_subject": "Job expires ",
  "i_footer_id": "5a60a044d3e81234473c986a",
  "v_from": "demo@gmail.com",
  "l_body": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td style=\"text-align: center;\">\r\n\t\t\t\t\t\t<div style=\"width: 500px; color: rgb(85, 85, 85);\">\r\n\t\t\t\t\t\t<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Job expires&nbsp;</h1>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #e8128c;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<p>Hello <span>{USER_FULL_NAME}</span><br />\r\n\t\t\t<br />\r\n\t\t\tYour posted job expire in two days.please find below detail</p>\r\n\r\n\t\t\t<p>Job Title :- {JOB_TITLE}</p>\r\n\r\n\t\t\t<p>Job expire date :- {JOB_EXPIRE_DATE}</p>\r\n\r\n\t\t\t<p>&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style=\"padding-top: 20px;\"><strong>Thank you,<br />\r\n\t\t\tSkillbox Administrator.</strong></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-07-25 11:16:33",
  "d_modified": "2018-07-25 11:16:33"
});

/** tbl_email_template_footer records **/
db.getCollection("tbl_email_template_footer").insert({
  "_id": ObjectId("5a60a044d3e81234473c986a"),
  "v_title": "Footer 1",
  "l_description": "<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<hr style=\"border: 3px solid #e8128c;\" />\r\n<table style=\"margin-top: 30px; width: 100%;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td align=\"center\"><a href=\"{_unsubscribe_link}\">Unsubscribe</a></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 3px solid #e8128c;\" />\r\n<table style=\"margin-top: 30px; width: 100%;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td><img src=\"http://192.168.0.200/projects/skillbox/public/Assets/ckeditor/plugins/ckfinder/core/connector/php/images/logo.png\" style=\"width: 100%; max-width: 333.333333333333px; margin-left: -12px;\" /></td>\r\n\t\t\t<td style=\"font-size: 9px; text-align: right; line-height: 12px; width: 25%;\"><span>Skillbox</span><br />\r\n\t\t\t<span>PO BOX 7153 </span><br />\r\n\t\t\t<span>Lichfield </span><br />\r\n\t\t\t<span>WS14 4JW </span><br />\r\n\t\t\t<span>Tel: <a href=\"#\" style=\"color: #0068A5;text-decoration: underline;\">12345 528964 </a></span></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 9px;\" width=\"100%\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n",
  "e_status": "active",
  "d_added": "2018-01-18 13:25:24",
  "d_modified": "2018-04-19 04:51:22"
});

/** tbl_email_template_header records **/
db.getCollection("tbl_email_template_header").insert({
  "_id": ObjectId("5a609fbfd3e812bc4b3c9869"),
  "v_title": "Header 1",
  "l_description": "<table style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<table>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><img src=\"http://192.168.0.200/projects/skillbox/public/Assets/ckeditor/plugins/ckfinder/core/connector/php/images/logo.png\" style=\"margin-left: -13px;\" /></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\r\n\t\t\t<hr style=\"border: 3px solid #e8128c;\" /></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n",
  "e_status": "active",
  "d_added": "2018-01-18 13:23:11",
  "d_modified": "2018-04-19 04:51:02"
});

/** tbl_faqs records **/
db.getCollection("tbl_faqs").insert({
  "_id": ObjectId("5b1a43b776fbae20a920a333"),
  "e_type": "general",
  "l_answer": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
  "v_question": "What is Lorem Ipsum?",
  "e_status": "active"
});
db.getCollection("tbl_faqs").insert({
  "_id": ObjectId("5b1a43d676fbae28ca5926b2"),
  "e_type": "general",
  "l_answer": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
  "v_question": "Why do we use it?",
  "e_status": "active"
});
db.getCollection("tbl_faqs").insert({
  "_id": ObjectId("5b1a43ef76fbae10d75237e3"),
  "e_type": "buyer",
  "l_answer": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,",
  "v_question": "Where does it come from?",
  "e_status": "active"
});
db.getCollection("tbl_faqs").insert({
  "_id": ObjectId("5b1a440c76fbae19586f56b3"),
  "e_type": "seller",
  "l_answer": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.",
  "v_question": "Where can I get some?",
  "e_status": "active"
});

/** tbl_feedback records **/
db.getCollection("tbl_feedback").insert({
  "_id": ObjectId("5b1781e676fbae1c5b6a9a12"),
  "v_first_name": "katherine",
  "v_last_name": "Read",
  "v_email": "katherineread.cis@gmail.com",
  "l_description": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
  "d_added": "2018-06-06 06;40:38"
});

/** tbl_jobs records **/
db.getCollection("tbl_jobs").insert({
  "_id": ObjectId("5b166bcf76fbae3b55286fe3"),
  "v_service": "online",
  "i_category_id": "5a607009d3e812063f3c9869",
  "i_mailskill_id": "5a60714ed3e81220273c9869",
  "i_otherskill_id": [
    "5a60716bd3e812062b3c986a"
  ],
  "v_skill_level_looking": "entry",
  "e_status": "active",
  "d_added": "2018-06-05 10:54:07",
  "d_modified": "2018-06-05 12:39:49",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "v_job_title": "Business Marketing",
  "v_budget_type": "total_budget",
  "v_budget_amt": 300,
  "l_job_description": "<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English</p>",
  "v_website": "https://www.lipsum.com/",
  "v_contact": "9632147479",
  "v_avalibility_from": "wednesday",
  "v_avalibility_to": "Friday",
  "v_avalibilitytime_from": "12:02",
  "v_avalibilitytime_to": "16:03",
  "v_start_month": "6",
  "v_start_date": "5",
  "v_start_year": "2018",
  "i_long_expect_duration": "10",
  "v_long_expect_duration_type": "hours",
  "i_permanent_role": "on",
  "v_city": "UK",
  "v_pincode": "400034",
  "d_start_date": "2018-06-05",
  "d_expiry_date": "2018-07-05",
  "v_video": [
    "jobpost/videos/job-video-1528196358.mp4"
  ],
  "v_photos": [
    "jobpost/photos/job-photo-1528196366.jpeg",
    "jobpost/photos/job-photo-1528196367.jpeg"
  ],
  "i_total_avg_review": "0",
  "i_total_review": NumberInt(0),
  "i_applied": NumberInt(0),
  "e_sponsor": "yes",
  "i_impression": NumberInt(29)
});
db.getCollection("tbl_jobs").insert({
  "_id": ObjectId("5b16810576fbae669b298c93"),
  "v_service": "online",
  "i_category_id": "5a607009d3e812063f3c9869",
  "i_mailskill_id": "5a60716bd3e812062b3c986a",
  "i_otherskill_id": [
    "5a60716bd3e812062b3c986a"
  ],
  "v_skill_level_looking": "entry",
  "e_status": "active",
  "d_added": "2018-06-05 12:24:37",
  "d_modified": "2018-06-05 12;45:05",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "v_job_title": "Drawing Designer",
  "v_budget_type": "total_budget",
  "v_budget_amt": 500,
  "l_job_description": "<p>Join over 150,000 learning student and start gaining the drawing skills you've always wanted.</p><p>The Ultimate Drawing Course will show you how to create advanced art that will stand up as professional work. This course will enhance or give you skills in the world of drawing - or your money back</p><p>The course is your track to obtaining drawing skills like you always knew you should have! Whether for your own projects or to draw for other people.</p><p>This course will take you from having little knowledge in drawing to creating advanced art and having a deep understanding of drawing fundamentals.</p><p>So what else is in it for you?</p><p><strong><strong>You’ll create </strong></strong><strong><strong>over 50</strong></strong><strong> </strong><strong><strong>different projects in this course that will take you from beginner to expert!</strong></strong></p><p>You’ll gain instant access to all 11 sections of the course.</p><p>The course is setup to quickly take you through step by step, the process of drawing in many different styles. It will equip you with the knowledge to create stunning designs and illustration!</p><p><strong><strong>Don’t believe me? I offer you a full money back guarantee within the first 30 days of purchasing the course.</strong></strong></p><p>Here’s what you get with the course:</p><p>You’ll get access to the11 sections of the course that will teach you the fundamentals of drawing from the ground up. The course is supported with over 10 hours of clear content that I walk you through each step of the way.</p><p>All at your fingers tips instantly.</p><ul><li><p>The course starts with the basics. You will get an in depth understanding of the fundamentals of drawing. Fundamentals are the most important part of creating professional art. You will learn everything from line fundamentals all the way up to highlight and shadows.</p></li><li><p>Next you’ll learn how perspective works and how to incorporate it into your art. You will be learning 1, 2, and 3 point perspective.</p></li><li><p>Once you’ve learned perspective you are going to learn how to create texture and apply it to your drawings.</p></li><li><p>Then you are going to learn how to draw from life. Observing life and drawing it is a very important skill when it comes to art.</p></li><li><p>At this point you’ll be ready to start drawing the human face. We will spend a whole section learning how to draw the human face from different angles.</p></li><li><p>Next you’ll you’re going to learn how to draw the human figure.</p></li><li><p>Lastly you will gain access to the bonus section where I’ll teach you how I draw animation styled characters step by step.</p></li></ul><p>Over the 7 chapters you will learn:</p><ul><li><p>How to draw an eye</p></li><li><p>Line fundamentals</p></li><li><p>Shape and form fundamental</p></li><li><p>How to use value and contrast</p></li><li><p>Space and perspective</p></li><li><p>Still life drawing</p></li><li><p>Creating texture</p></li><li><p>Drawing the human face</p></li><li><p>Drawing the human figure</p></li><li><p>Drawing animation styled art</p></li></ul><p>What else will you get?</p><p>- Personal contact with me, the course tutor</p><p>- Lifetime access to course materials</p><p>- Understanding of how professional art is created</p><p>- Quizzes and exercise work sheets</p><p>This all comes under one convenient easy to use platform. Plus you will get fast, friendly, responsive support on the Udemy Q&A section of the course or direct message.</p><p>I will be here for you every step of the way!</p><p><strong><strong>So what are you waiting for? Sign up now </strong></strong><strong><strong>and change your art world today!</strong></strong></p><p>Who is the target audience?</p><ul><li>Students wanting to learn how to draw</li><li>Students willing to put in a couple hours to learn how to draw</li><li>Students willing to take action and start drawing amazing artwork</li><li>Students wanting to add another skill to their tool belt</li></ul><p> </p>",
  "v_website": "https://www.lipsum.com/",
  "v_contact": "962587414",
  "v_avalibility_from": "tuesday",
  "v_avalibility_to": "monday",
  "v_avalibilitytime_from": "10:25",
  "v_avalibilitytime_to": "16:20",
  "v_start_month": "1",
  "v_start_date": "1",
  "v_start_year": "2018",
  "i_long_expect_duration": "20",
  "v_long_expect_duration_type": "hours",
  "v_city": "mava",
  "v_pincode": "40025",
  "d_start_date": "2018-01-01",
  "d_expiry_date": "2018-07-05",
  "v_video": [
    "jobpost/videos/job-video-1528201697.mp4"
  ],
  "v_photos": [
    "jobpost/photos/job-photo-1528201702.jpg"
  ],
  "i_total_avg_review": NumberInt(4),
  "i_total_review": NumberInt(1),
  "i_applied": NumberInt(1)
});
db.getCollection("tbl_jobs").insert({
  "_id": ObjectId("5b16821476fbae676d437a62"),
  "v_service": "online",
  "i_category_id": "5a607698d3e812302b3c986b",
  "i_mailskill_id": "5a6076c0d3e812fa3f3c9869",
  "i_otherskill_id": [
    "5a6076c0d3e812fa3f3c9869"
  ],
  "v_skill_level_looking": "entry",
  "e_status": "active",
  "d_added": "2018-06-05 12:29:08",
  "d_modified": "2018-06-05 12:32:26",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "v_job_title": " Geometry Master",
  "v_budget_type": "total_budget",
  "v_budget_amt": 200,
  "l_job_description": "<p><strong>HOW BECOME A GEOMETRY MASTER IS SET UP TO MAKE COMPLICATED MATH EASY</strong></p><p>This 146-lesson course includes video explanations of everything from Geometry, and it includes more than 60 quizzes (with solutions!) to help you test your understanding along the way. Become a Geometry Master is organized into the following sections:</p>",
  "v_website": "https://www.lipsum.com/",
  "v_contact": "9632587474",
  "v_avalibility_from": "monday",
  "v_avalibility_to": "Friday",
  "v_avalibilitytime_from": "01:10",
  "v_avalibilitytime_to": "02:20",
  "v_start_month": "1",
  "v_start_date": "1",
  "v_start_year": "2019",
  "i_long_expect_duration": "2",
  "v_long_expect_duration_type": "day",
  "v_city": "Lorem Ipsum",
  "v_pincode": "400236",
  "d_start_date": "2019-01-01",
  "d_expiry_date": "2018-07-05",
  "v_video": [
    "jobpost/videos/job-video-1528201940.mp4"
  ],
  "v_photos": [
    "jobpost/photos/job-photo-1528201944.jpg"
  ],
  "i_total_avg_review": "0",
  "i_total_review": NumberInt(0),
  "i_applied": NumberInt(0)
});
db.getCollection("tbl_jobs").insert({
  "_id": ObjectId("5b1684fc76fbae673e1912a3"),
  "v_service": "online",
  "i_category_id": "5a607698d3e812302b3c986b",
  "i_mailskill_id": "5a6076c0d3e812fa3f3c9869",
  "i_otherskill_id": [
    "5a6076c0d3e812fa3f3c9869"
  ],
  "v_skill_level_looking": "entry",
  "e_status": "active",
  "d_added": "2018-06-05 12:41:32",
  "d_modified": "2018-06-05 12:47:18",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "v_job_title": "Web  Developer",
  "v_budget_type": "total_budget",
  "v_budget_amt": 200,
  "l_job_description": "<p>This course is specially designed for Software Testing professionals, This will take students from basic level to advance in decent pace videos.</p><p>Here we will cover SQL queries and Unix commands which we can use in daily QA activities and also we have covered many interview question which are asked in interviews.</p><p>In today's competitive environment, companies need software testers who are having database as well as Unix/Linux knowledge, this course going to make you ready to perform basic to advance level backend QA activities.</p>",
  "v_website": "https://www.lipsum.com/",
  "v_contact": "9874563212",
  "v_avalibility_from": "monday",
  "v_avalibility_to": "Friday",
  "v_avalibilitytime_from": "12:02",
  "v_avalibilitytime_to": "10:03",
  "v_start_month": "1",
  "v_start_date": "1",
  "v_start_year": "2018",
  "i_long_expect_duration": "2",
  "v_long_expect_duration_type": "hours",
  "v_city": "Lorem",
  "v_pincode": "40025",
  "d_start_date": "2018-01-01",
  "d_expiry_date": "2018-07-05",
  "v_video": [
    "jobpost/videos/job-video-1528202831.mp4"
  ],
  "v_photos": [
    "jobpost/photos/job-photo-1528202836.jpg"
  ],
  "i_total_avg_review": "0",
  "i_total_review": NumberInt(0),
  "i_applied": NumberInt(0)
});
db.getCollection("tbl_jobs").insert({
  "_id": ObjectId("5b168ed976fbae70f856ba23"),
  "v_service": "online",
  "i_category_id": "5a606efbd3e81219263c986f",
  "i_mailskill_id": "5a607117d3e812063f3c986a",
  "i_otherskill_id": [
    "5a607117d3e812063f3c986a",
    "5a607124d3e812db133c9869"
  ],
  "v_skill_level_looking": "intermediate",
  "e_status": "active",
  "d_added": "2018-06-05 13:23:37",
  "d_modified": "2018-08-08 15;15:58",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_job_title": "Need Webdeveloper",
  "v_budget_type": "total_budget",
  "v_budget_amt": 125,
  "l_job_description": "<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>",
  "v_website": "www.asdsadsa.com",
  "v_contact": "123456789",
  "v_avalibility_from": "monday",
  "v_avalibility_to": "monday",
  "v_avalibilitytime_from": "10:20",
  "v_avalibilitytime_to": "15:30",
  "v_start_month": "7",
  "v_start_date": "1",
  "v_start_year": "2018",
  "i_long_expect_duration": "125",
  "v_long_expect_duration_type": "hours",
  "v_city": "Birmingham Club Drive, Bloomfield Township, MI, USA",
  "v_pincode": "48301",
  "d_start_date": "2018-07-01",
  "d_expiry_date": "2018-07-05",
  "v_video": [
    "jobpost/videos/job-video-1528205077.mp4"
  ],
  "v_photos": [
    "jobpost/photos/job-photo-1528205081.jpeg"
  ],
  "v_latitude": "42.54123929999999",
  "v_longitude": "-83.2811509",
  "e_sponsor": "yes",
  "e_sponsor_status": "start",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": 0,
  "i_applied": NumberInt(0),
  "i_clicks": NumberInt(1),
  "i_impression": NumberInt(27),
  "v_search_tags": [
    "sdfdsfds",
    "sdfdsfdsfds",
    "sdfdsfdsfds"
  ]
});
db.getCollection("tbl_jobs").insert({
  "_id": ObjectId("5b560495516b462cb0001aac"),
  "v_service": "online",
  "i_category_id": "5a606efbd3e81219263c986f",
  "i_mailskill_id": "5a607117d3e812063f3c986a",
  "i_otherskill_id": [
    "5a607124d3e812db133c9869",
    "5a607117d3e812063f3c986a"
  ],
  "v_skill_level_looking": "intermediate",
  "e_status": "active",
  "d_added": "2018-07-23 16:38:45",
  "d_modified": "2018-07-23 16:42:00",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_expiry_date": "2018-08-22",
  "d_start_date": "2018-08-01",
  "i_long_expect_duration": "150",
  "i_permanent_role": "on",
  "l_job_description": "<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, .</p>",
  "v_avalibility_from": "monday",
  "v_avalibility_to": "Friday",
  "v_avalibilitytime_from": "10:20",
  "v_avalibilitytime_to": "12:30",
  "v_budget_amt": 100,
  "v_budget_type": "hourly_rate",
  "v_city": "Ahmedabad, Gujarat, India",
  "v_contact": "123456789",
  "v_job_title": "Test Job",
  "v_latitude": "23.022505",
  "v_long_expect_duration_type": "hours",
  "v_longitude": "72.57136209999999",
  "v_photos": [
    "jobpost/photos/job-photo-1532364120.png"
  ],
  "v_pincode": "123",
  "v_start_date": "1",
  "v_start_month": "8",
  "v_start_year": "2018",
  "v_video": [
    "jobpost/videos/job-video-1532364112.mp4"
  ],
  "v_website": "www.abcd.com",
  "i_applied": 1,
  "i_total_avg_review": NumberInt(0),
  "i_total_review": 0
});
db.getCollection("tbl_jobs").insert({
  "_id": ObjectId("5b5855dc1b544d427563ac82"),
  "v_service": "online",
  "i_category_id": "5a606efbd3e81219263c986f",
  "i_mailskill_id": "5a607117d3e812063f3c986a",
  "i_otherskill_id": [
    "5a607117d3e812063f3c986a",
    "5a607124d3e812db133c9869"
  ],
  "v_skill_level_looking": "intermediate",
  "e_status": "active",
  "d_added": "2018-07-25 10:50:04",
  "d_modified": "2018-07-25 10:52:34",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_expiry_date": "2018-07-23",
  "d_start_date": "2018-08-01",
  "i_applied": NumberInt(0),
  "i_long_expect_duration": "125",
  "i_permanent_role": "on",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "l_job_description": "<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
  "v_avalibility_from": "monday",
  "v_avalibility_to": "Friday",
  "v_avalibilitytime_from": "10:30",
  "v_avalibilitytime_to": "15:30",
  "v_budget_amt": 1256,
  "v_budget_type": "hourly_rate",
  "v_city": "Bangalore, Karnataka, India",
  "v_contact": "12316546456",
  "v_job_title": "Hello Test Job",
  "v_latitude": "12.9715987",
  "v_long_expect_duration_type": "hours",
  "v_longitude": "77.59456269999998",
  "v_photos": [
    "jobpost/photos/job-photo-1532515953.jpg"
  ],
  "v_pincode": "123456",
  "v_start_date": "1",
  "v_start_month": "8",
  "v_start_year": "2018",
  "v_video": [
    "jobpost/videos/job-video-1532515949.mp4"
  ],
  "v_website": "www.abcd,conm"
});
db.getCollection("tbl_jobs").insert({
  "_id": ObjectId("5b5861c01b544d14da26abb3"),
  "v_service": "online",
  "i_category_id": "5a606efbd3e81219263c986f",
  "i_mailskill_id": "5a607117d3e812063f3c986a",
  "i_otherskill_id": [
    "5a607117d3e812063f3c986a",
    "5a607124d3e812db133c9869"
  ],
  "v_skill_level_looking": "intermediate",
  "e_status": "inactive",
  "d_added": "2018-07-25 11:40:48",
  "d_modified": "2018-07-25 11:40:48",
  "i_user_id": "5b165e1376fbae3c3e66bd33"
});
db.getCollection("tbl_jobs").insert({
  "_id": ObjectId("5b6b07321b544d05305571c3"),
  "v_service": "online",
  "i_category_id": "5a606efbd3e81219263c986f",
  "i_mailskill_id": "5a607117d3e812063f3c986a",
  "i_otherskill_id": [
    "5a607117d3e812063f3c986a",
    "5a607124d3e812db133c9869"
  ],
  "v_skill_level_looking": "intermediate",
  "e_status": "inactive",
  "d_added": "2018-08-08 15:07:30",
  "d_modified": "2018-08-08 15:07:30",
  "i_user_id": "5b165e1376fbae3c3e66bd33"
});

/** tbl_levels records **/
db.getCollection("tbl_levels").insert({
  "_id": ObjectId("5a60721fd3e812302b3c986a"),
  "v_name": "Level 1",
  "i_skills": "5a65d66bd3e8124f123c986f",
  "i_discount": "",
  "d_added": "2018-01-18 10:08:31",
  "d_modified": "2018-01-22 12:19:35"
});

/** tbl_meta_fields_banner records **/
db.getCollection("tbl_meta_fields_banner").insert({
  "_id": ObjectId("5b1a23b676fbae1c9c4009b2"),
  "v_title": "Home Page",
  "v_key": "home-page",
  "v_meta_title": "Skillbox Home",
  "v_meta_keywords": "Skillbox Home",
  "l_meta_description": "Skillbox Home",
  "v_image": "meta/meta-image-1528455307.png",
  "d_added": "2018-06-08 06:35:34",
  "d_modified": "2018-06-08 10:55:09",
  "e_status": "active"
});
db.getCollection("tbl_meta_fields_banner").insert({
  "_id": ObjectId("5b1a610d76fbae491e3a7022"),
  "v_title": "Blog & Education",
  "v_key": "blog-education",
  "v_meta_title": "Skillbox Blog & Education",
  "v_meta_keywords": "Skillbox Blog & Education",
  "l_meta_description": "Skillbox Blog & Education",
  "v_image": "meta/meta-image-1528455434.png",
  "d_added": "2018-06-08 10:57:17",
  "d_modified": "2018-06-08 10:57:17",
  "e_status": "active"
});
db.getCollection("tbl_meta_fields_banner").insert({
  "_id": ObjectId("5b1a61e576fbae4c1b69f9a2"),
  "v_title": "Skillbox Careers",
  "v_key": "skillbox-careers",
  "v_meta_title": "Skillbox Careers",
  "v_meta_keywords": "Skillbox Careers",
  "l_meta_description": "Skillbox Careers",
  "v_image": "meta/meta-image-1528455650.png",
  "d_added": "2018-06-08 11:00:53",
  "d_modified": "2018-06-08 11:00:53",
  "e_status": "active"
});
db.getCollection("tbl_meta_fields_banner").insert({
  "_id": ObjectId("5b1a622d76fbae4a69689712"),
  "v_title": "Skillbox FAQ",
  "v_key": "skillbox-faq",
  "v_meta_title": "Skillbox FAQ",
  "v_meta_keywords": "Skillbox FAQ",
  "l_meta_description": "Skillbox FAQ",
  "v_image": "meta/meta-image-1528455721.png",
  "d_added": "2018-06-08 11:02:05",
  "d_modified": "2018-06-08 11:02:05",
  "e_status": "active"
});
db.getCollection("tbl_meta_fields_banner").insert({
  "_id": ObjectId("5b1a62aa76fbae491c3bd762"),
  "v_title": "Course Search Result",
  "v_key": "course-search-result",
  "v_meta_title": "Course Search Result",
  "v_meta_keywords": "Course Search Result",
  "l_meta_description": "Course Search Result",
  "v_image": "meta/meta-image-1528455843.png",
  "d_added": "2018-06-08 11:04:10",
  "d_modified": "2018-06-08 11:04:10",
  "e_status": "active"
});
db.getCollection("tbl_meta_fields_banner").insert({
  "_id": ObjectId("5b1a630576fbae10d75237e5"),
  "v_title": "Online Skill and Job Result",
  "v_key": "online-skill-and-job-result",
  "v_meta_title": "Online Skill and Job Result",
  "v_meta_keywords": "Online Skill and Job Result",
  "l_meta_description": "Online Skill and Job Result",
  "v_image": "meta/meta-image-1528455934.png",
  "d_added": "2018-06-08 11:05:41",
  "d_modified": "2018-06-08 11:05:41",
  "e_status": "active"
});
db.getCollection("tbl_meta_fields_banner").insert({
  "_id": ObjectId("5b1a631c76fbae491e3a7023"),
  "v_title": "Inperson Skill and Job Result",
  "v_key": "inperson-skill-and-job-result",
  "v_meta_title": "Inperson Skill and Job Result",
  "v_meta_keywords": "Inperson Skill and Job Result",
  "l_meta_description": "Inperson Skill and Job Result",
  "d_added": "2018-06-08 11:06:04",
  "d_modified": "2018-06-08 11:06:32",
  "e_status": "active",
  "v_image": "meta/meta-image-1528455988.png"
});
db.getCollection("tbl_meta_fields_banner").insert({
  "_id": ObjectId("5b58651b1b544d5d44495724"),
  "v_title": "Buyer Single search",
  "v_key": "buyer-single-search",
  "v_meta_title": "Search jobs and skill",
  "v_meta_keywords": "Search jobs and skill",
  "l_meta_description": "Search jobs and skill",
  "v_image": "meta/meta-image-1532519704.png",
  "d_added": "2018-07-25 11:55:07",
  "d_modified": "2018-07-25 11:55:07",
  "e_status": "active"
});

/** tbl_newsletter records **/

/** tbl_order records **/
db.getCollection("tbl_order").insert({
  "_id": ObjectId("5b17866f76fbae1c5b6a9a13"),
  "i_order_no": "ODR0000002",
  "e_transaction_type": "course",
  "e_payment_type": "paypal",
  "v_transcation_id": "76A855299X9030816",
  "e_payment_status": "pending",
  "v_amount": "341.00",
  "v_order_detail": [
    {
      "id": "5b16610b76fbae3d393c9a03",
      "name": "Deep Learning: GANs and Variational ",
      "price": 325
    }
  ],
  "v_processing_fees": "16",
  "v_order_status": "active",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_date": "2018-06-06",
  "d_added": "2018-06-06 06:59:59",
  "d_modified": "2018-06-06 06:59:59"
});
db.getCollection("tbl_order").insert({
  "_id": ObjectId("5b179a5176fbae1d6d503293"),
  "i_order_no": "ODR0000002",
  "e_transaction_type": "skill",
  "e_payment_type": "paypal",
  "v_transcation_id": "2KJ29061T5190482W",
  "e_payment_status": "pending",
  "v_amount": "315.00",
  "v_order_detail": [
    {
      "id": "5b165e7a76fbae3c8e6c7993",
      "i_seller_id": "5b165e1376fbae3c3e66bd33",
      "name": "Graphics & Design / Logo Design",
      "qty": "3",
      "price": 100,
      "package": "basic_package"
    }
  ],
  "v_processing_fees": "15.00",
  "v_duration_time": "10",
  "v_duration_in": "hours",
  "v_order_status": "completed",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_seller_profile_id": "5b165e7a76fbae3c8e6c7993",
  "v_profile_title": "Graphics & Design / Logo Design",
  "i_seller_id": "5b165e1376fbae3c3e66bd33",
  "d_date": "2018-06-06",
  "d_added": "2018-06-06 08:24:49",
  "d_modified": "2018-07-12 18:16:56",
  "d_order_completed_date": "2018-07-12 18:17:15",
  "i_order_review": "1"
});
db.getCollection("tbl_order").insert({
  "_id": ObjectId("5b179b2676fbae1c6643b024"),
  "i_order_no": "ODR0000002",
  "e_transaction_type": "course",
  "e_payment_type": "paypal",
  "v_transcation_id": "7G82718904384531T",
  "e_payment_status": "pending",
  "v_amount": "105.00",
  "v_order_detail": [
    {
      "id": "5b1674f576fbae5bca3eb652",
      "name": "The Ultimate Drawing Course - Beginner to Advanced",
      "price": 100
    }
  ],
  "v_processing_fees": "5",
  "v_order_status": "active",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_date": "2018-06-06",
  "d_added": "2018-06-06 08:28:22",
  "d_modified": "2018-06-06 08:28:22"
});
db.getCollection("tbl_order").insert({
  "_id": ObjectId("5b17cd9b76fbae1c5f654af3"),
  "i_order_no": "ODR0000002",
  "e_transaction_type": "course",
  "e_payment_type": "paypal",
  "v_transcation_id": "65H581971J447422A",
  "e_payment_status": "pending",
  "v_amount": "105.00",
  "v_order_detail": [
    {
      "id": "5b16621d76fbae3c8e6c7994",
      "name": "Artificial Intelligence: Reinforcement ",
      "price": 100
    }
  ],
  "v_processing_fees": "5",
  "v_order_status": "active",
  "i_user_id": "5b1666cd76fbae3d393c9a04",
  "d_date": "2018-06-06",
  "d_added": "2018-06-06 12:03:39",
  "d_modified": "2018-06-06 12:03:39"
});
db.getCollection("tbl_order").insert({
  "_id": ObjectId("5b17d43776fbae34dd205c05"),
  "i_order_no": "ODR0000002",
  "e_transaction_type": "skill",
  "e_payment_type": "paypal",
  "v_transcation_id": "8KP059327W730512K",
  "e_payment_status": "success",
  "v_amount": "263.00",
  "v_order_detail": [
    {
      "id": "5b16672776fbae3c3e66bd37",
      "i_seller_id": "5b16661a76fbae3d3f03e803",
      "name": "Designer Head",
      "qty": NumberInt(1),
      "price": 250,
      "package": "basic_package"
    }
  ],
  "v_processing_fees": "12.50",
  "v_duration_time": "12",
  "v_duration_in": "hours",
  "v_order_status": "active",
  "i_user_id": "5b1666cd76fbae3d393c9a04",
  "i_seller_profile_id": "5b16672776fbae3c3e66bd37",
  "v_profile_title": "Designer Head",
  "i_seller_id": "5b16661a76fbae3d3f03e803",
  "d_date": "2018-06-06",
  "d_added": "2018-06-06 12:31:51",
  "d_modified": "2018-07-18 17:59:55",
  "e_seller_payment_status": ""
});
db.getCollection("tbl_order").insert({
  "_id": ObjectId("5b3a211a516b460a68003e6c"),
  "i_order_no": "ODR0000002",
  "e_transaction_type": "course",
  "e_payment_type": "paypal",
  "v_transcation_id": "05V56303YG5614044",
  "e_payment_status": "success",
  "v_amount": "105.00",
  "v_order_detail": [
    {
      "id": "5b3a1fdb516b460a68003e69",
      "name": "D course",
      "price": 100
    }
  ],
  "v_processing_fees": "5",
  "v_order_status": "active",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_date": "2018-07-02",
  "d_added": "2018-07-02 12:56:58",
  "d_modified": "2018-07-13 16:08:38",
  "e_seller_payment_status": ""
});
db.getCollection("tbl_order").insert({
  "_id": ObjectId("5b44dab6516b4602b00067b9"),
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "e_transaction_type": "userplan",
  "e_payment_type": "paypal",
  "v_transcation_id": "4DU521959Y312413X",
  "e_payment_status": "pending",
  "v_amount": "9.99",
  "v_order_detail": {
    "v_plan_id": "5a65b757d3e8125e323c986a",
    "v_plan_name": "Standard Plan",
    "v_plan_duration": "monthly",
    "v_action": "upgrade user plan"
  },
  "d_date": "2018-07-10",
  "d_added": "2018-07-10 16:11:34",
  "d_modified": "2018-07-10 16:11:34",
  "i_order_no": "ODR0000002"
});
db.getCollection("tbl_order").insert({
  "_id": ObjectId("5b478c72516b4620200031ac"),
  "i_order_no": "ODR0000007",
  "e_transaction_type": "course",
  "e_payment_type": "paypal",
  "v_transcation_id": "34A199467J148864D",
  "e_payment_status": "pending",
  "v_amount": "105.00",
  "v_order_detail": [
    {
      "id": "5b16703f76fbae487952a662",
      "name": "iOS 11 & Swift 4 - The Complete iOS App ",
      "price": 100
    }
  ],
  "v_processing_fees": "5",
  "v_order_status": "active",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_seller_id": "5b165e1376fbae3c3e66bd33",
  "e_seller_payment_status": "pending",
  "d_date": "2018-07-12",
  "d_added": "2018-07-12 17:14:26",
  "d_modified": "2018-07-12 17:14:26"
});
db.getCollection("tbl_order").insert({
  "_id": ObjectId("5b4f7c29516b462714005316"),
  "i_order_no": "ODR0000008",
  "e_transaction_type": "skill",
  "e_payment_type": "paypal",
  "v_transcation_id": "94223998K3297145K",
  "e_payment_status": "success",
  "v_amount": "105.00",
  "v_order_detail": [
    {
      "id": "5b165e7a76fbae3c8e6c7993",
      "i_seller_id": "5b165e1376fbae3c3e66bd33",
      "name": "Graphics & Design / Logo Design",
      "qty": NumberInt(1),
      "price": 100,
      "package": "basic_package"
    }
  ],
  "v_processing_fees": "5.00",
  "v_duration_time": "10",
  "v_duration_in": "hours",
  "v_order_status": "active",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_seller_profile_id": "5b165e7a76fbae3c8e6c7993",
  "v_profile_title": "Graphics & Design / Logo Design",
  "i_seller_id": "5b165e1376fbae3c3e66bd33",
  "d_date": "2018-07-18",
  "d_added": "2018-07-18 17:43:05",
  "d_modified": "2018-07-25 08:35:57",
  "e_seller_payment_status": ""
});

/** tbl_order_number records **/
db.getCollection("tbl_order_number").insert({
  "_id": ObjectId("5b093a3176fbae5dde4c6483"),
  "i_order_no": NumberInt(8)
});

/** tbl_order_requirements records **/
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b06ba4976fbae0bbe61e1e4"),
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "l_message": "<p>1.Please provide with your details</p><p>hi</p><p>Thank you for this great service.</p><p>I have uploaded the zip file here https://www.wetransfer.com/downloads/24fba7278cf3dced34293e2fc9efc1af2015</p><p>The link contains 13 images. I need them in transparent PNG format thanks.</p><p>Hope to get them soon. If you have any questions please feel free to ask.</p><p>Regards</p><p>Rehan</p>",
  "v_document": "",
  "d_added": "2018-05-24 13:12:41",
  "d_modified": "2018-05-24 13:12:41"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b07d18376fbae0f3060bbc3"),
  "v_duration_time": "1",
  "v_duration_in": "hours",
  "e_type": "extendtime",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "reject",
  "d_added": "2018-05-25 09:04:03",
  "d_modified": "2018-05-25 12:07:02",
  "v_order_id": "5b06ba4976fbae0bbe61e1e3"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b07dd4776fbae3e913b7512"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "v_duration_time": "1",
  "v_duration_in": "days",
  "e_type": "extendtime",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "reject",
  "d_added": "2018-05-25 09:54:15",
  "d_modified": "2018-05-25 12:07:51"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b07ddb176fbae4088351be2"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "v_duration_time": "1",
  "v_duration_in": "hours",
  "e_type": "extendtime",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "accept",
  "d_added": "2018-05-25 09:56:01",
  "d_modified": "2018-05-25 12:08:36"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b07e39176fbae09d86d5ac3"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "l_message": "asdasdasdsadsadsadsa sdsadsad adasdsad sadsadsad sdsadsadsa sadasdsad",
  "v_document": [
    "order/job-video-1527243662.png"
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "",
  "d_added": "2018-05-25 10:21:05",
  "d_modified": "2018-05-25 10:21:05"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b07ed5a76fbae48a2006262"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "l_message": "fssdfsdfds fsdfdsf sdfdsfds dsf dsfdfdsfd dsfdsfd dfs ds fd ff ds sdf fsdf",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "",
  "d_added": "2018-05-25 11:02:50",
  "d_modified": "2018-05-25 11:02:50"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b07ff1376fbae4f80633fb2"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "l_message": "This is tets message",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "",
  "d_added": "2018-05-25 12:18:27",
  "d_modified": "2018-05-25 12:18:27"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b080c3e76fbae754d53cec3"),
  "e_type": "order_deliver",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "d_added": "2018-05-25 13:14:38",
  "d_modified": "2018-05-25 13:14:38"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b080c3e76fbae754d53cec4"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "v_order_deliver": "1",
  "l_message": "complete ordfer",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "",
  "d_added": "2018-05-25 13:14:38",
  "d_modified": "2018-05-25 13:14:38"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b08fb4176fbae0db462d0f3"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "l_message": "sdfdsfdsfsdfdsfdsfsdfsdfsdfdfdsfdsfd",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "d_added": "2018-05-26 06:14:25",
  "d_modified": "2018-05-26 06:14:25"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b08fb4176fbae0db247e0d2"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "l_message": "sdfdsfdsfsdfdsfdsfsdfsdfsdfdfdsfdsfd",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "d_added": "2018-05-26 06:14:25",
  "d_modified": "2018-05-26 06:14:25"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b08fb6476fbae0e9a49bd82"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "l_message": "sdfdsfdsfsdfdsfdsfsdfsdfsdfdfdsfdsfd",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "d_added": "2018-05-26 06:15:00",
  "d_modified": "2018-05-26 06:15:00"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b09011976fbae0e99157614"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "l_message": "Order completed Thank you",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "d_added": "2018-05-26 06:39:21",
  "d_modified": "2018-05-26 06:39:21"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b09031f76fbae0e8d2d7cb3"),
  "v_order_id": "5b06ba4976fbae0bbe61e1e3",
  "l_message": "Order completed Thank you",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "d_added": "2018-05-26 06:47:59",
  "d_modified": "2018-05-26 06:47:59"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b0958a876fbae7f1b4af484"),
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_order_id": "5b0958a876fbae7f1b4af483",
  "l_message": "<p>1.Please provide with your details</p><p>hi</p><p>Thank you for this great service.</p><p>I have uploaded the zip file here https://www.wetransfer.com/downloads/24fba7278cf3dced34293e2fc9efc1af2015</p><p>The link contains 13 images. I need them in transparent PNG format thanks.</p><p>Hope to get them soon. If you have any questions please feel free to ask.</p><p>Regards</p><p>Rehan</p>",
  "v_document": "",
  "d_added": "2018-05-26 12:52:56",
  "d_modified": "2018-05-26 12:52:56"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b095a1376fbae5f4402b226"),
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_order_id": "5b095a1376fbae5f4402b225",
  "l_message": "<p>1.Please provide with your details</p><p>hi</p><p>Thank you for this great service.</p><p>I have uploaded the zip file here https://www.wetransfer.com/downloads/24fba7278cf3dced34293e2fc9efc1af2015</p><p>The link contains 13 images. I need them in transparent PNG format thanks.</p><p>Hope to get them soon. If you have any questions please feel free to ask.</p><p>Regards</p><p>Rehan</p>",
  "v_document": "",
  "d_added": "2018-05-26 12:58:59",
  "d_modified": "2018-05-26 12:58:59"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b095d1976fbae6f8d632433"),
  "v_order_id": "5b095a1376fbae5f4402b225",
  "l_message": "asdsdsadsa",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "",
  "d_added": "2018-05-26 13:11:53",
  "d_modified": "2018-05-26 13:11:53"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b095eb376fbae5f493c0a95"),
  "e_type": "order_deliver",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_order_id": "5b095a1376fbae5f4402b225",
  "d_added": "2018-05-26 13:18:43",
  "d_modified": "2018-05-26 13:18:43"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b095eb376fbae5f493c0a96"),
  "v_order_id": "5b095a1376fbae5f4402b225",
  "v_order_deliver": "1",
  "l_message": "asdsadsadsadsa",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "",
  "d_added": "2018-05-26 13:18:43",
  "d_modified": "2018-05-26 13:18:43"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b0bbf5d76fbae0b824d4695"),
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_order_id": "5b0bbf5d76fbae0b824d4694",
  "l_message": "<p>1.Please provide with your details</p><p>hi</p><p>Thank you for this great service.</p><p>I have uploaded the zip file here https://www.wetransfer.com/downloads/24fba7278cf3dced34293e2fc9efc1af2015</p><p>The link contains 13 images. I need them in transparent PNG format thanks.</p><p>Hope to get them soon. If you have any questions please feel free to ask.</p><p>Regards</p><p>Rehan</p>",
  "v_document": "",
  "d_added": "2018-05-28 08:35:41",
  "d_modified": "2018-05-28 08:35:41"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b0bbf6f76fbae0c920bc913"),
  "v_order_id": "5b0bbf5d76fbae0b824d4694",
  "l_message": "asdasdsadsadas",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "",
  "d_added": "2018-05-28 08:35:59",
  "d_modified": "2018-05-28 08:35:59"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b0bbff276fbae14b862fb93"),
  "v_order_id": "5b0bbf5d76fbae0b824d4694",
  "v_duration_time": "125",
  "v_duration_in": "hours",
  "e_type": "extendtime",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "reject",
  "d_added": "2018-05-28 08:38:10",
  "d_modified": "2018-05-28 08:38:21"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b0bc01076fbae0b824d4696"),
  "e_type": "order_deliver",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_order_id": "5b0bbf5d76fbae0b824d4694",
  "d_added": "2018-05-28 08:38:40",
  "d_modified": "2018-05-28 08:38:40"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b0bc01276fbae0b824d4697"),
  "v_order_id": "5b0bbf5d76fbae0b824d4694",
  "v_order_deliver": "1",
  "l_message": "dfgfdgfdgfdgdfgfdgfdgdfg",
  "v_document": [
    "order/job-video-1527496720.png"
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "",
  "d_added": "2018-05-28 08:38:42",
  "d_modified": "2018-05-28 08:38:42"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b0bc40376fbae256f314d06"),
  "v_order_id": "5b0bbf5d76fbae0b824d4694",
  "l_message": "fdgfdgdfgfdgfdgfd",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "",
  "d_added": "2018-05-28 08:55:31",
  "d_modified": "2018-05-28 08:55:31"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b0bc41276fbae264c443623"),
  "v_order_id": "5b0bbf5d76fbae0b824d4694",
  "v_order_deliver": "0",
  "l_message": "hgfhgfhgfhgfhgfhgfhf",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5ad42a74d3e8122f133c9869",
  "v_extend_resquest_status": "",
  "d_added": "2018-05-28 08:55:46",
  "d_modified": "2018-05-28 08:55:46"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b179a5176fbae1d6d503294"),
  "e_type": "message",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b179a5176fbae1d6d503293",
  "l_message": "<p>- Original and unique different concepts for your brandring.</p><p>- Print and Web versions (CMYK &amp; RGB).</p><p>- Vertical, horizontal and simplified alternate versions.<br />- Full colour, black and white colour variants.<br />- Vector files (Ai, PDF).<br />- Transparent background (PNG).</p><p>- Social Media Kit: Facebook &amp; Twitter cover + profile photo design.</p><p>- Stationery Designs: Business card &amp; letterhead design.&nbsp;</p><p>We can also on some other graphic design pieces (full stationery, packaging, website, app, etc) according to your requirements, so please feel free to contact me if you have any questions.</p>",
  "v_document": "",
  "d_added": "2018-06-06 08:24:49",
  "d_modified": "2018-06-06 08:24:49"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b179a7a76fbae1d6d503295"),
  "v_order_id": "5b179a5176fbae1d6d503293",
  "l_message": "sdkjhnfkjdshgidskfhkjsdhfjksd",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_extend_resquest_status": "",
  "d_added": "2018-06-06 08:25:30",
  "d_modified": "2018-06-06 08:25:30"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b17d43776fbae34dd205c06"),
  "e_type": "message",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "v_order_id": "5b17d43776fbae34dd205c05",
  "l_message": "",
  "v_document": "",
  "d_added": "2018-06-06 12:31:51",
  "d_modified": "2018-06-06 12:31:51"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b3b6d56516b463248002fd3"),
  "v_order_id": "5b179a5176fbae1d6d503293",
  "l_message": "dsadas",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_extend_resquest_status": "",
  "d_added": "2018-07-03 12:34:30",
  "d_modified": "2018-07-03 12:34:30"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b479b18516b4625dc00765b"),
  "e_type": "order_deliver",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b179a5176fbae1d6d503293",
  "d_added": "2018-07-12 18:16:56",
  "d_modified": "2018-07-12 18:16:56"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b479b18516b4625dc00765c"),
  "v_order_id": "5b179a5176fbae1d6d503293",
  "v_order_deliver": "1",
  "l_message": "Finale delery",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_extend_resquest_status": "",
  "d_added": "2018-07-12 18:16:56",
  "d_modified": "2018-07-12 18:16:56"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b4f7c29516b462714005317"),
  "e_type": "message",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b4f7c29516b462714005316",
  "l_message": "<p>- Original and unique different concepts for your brandring.</p><p>- Print and Web versions (CMYK &amp; RGB).</p><p>- Vertical, horizontal and simplified alternate versions.<br />- Full colour, black and white colour variants.<br />- Vector files (Ai, PDF).<br />- Transparent background (PNG).</p><p>- Social Media Kit: Facebook &amp; Twitter cover + profile photo design.</p><p>- Stationery Designs: Business card &amp; letterhead design.&nbsp;</p><p>We can also on some other graphic design pieces (full stationery, packaging, website, app, etc) according to your requirements, so please feel free to contact me if you have any questions.</p>",
  "v_document": "",
  "d_added": "2018-07-18 17:43:05",
  "d_modified": "2018-07-18 17:43:05"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b4f82fe516b462714005319"),
  "v_order_id": "5b4f7c29516b462714005316",
  "v_order_deliver": "0",
  "l_message": "Hi  This is test message \r\n\r\nWe can also on some other graphic design pieces (full stationery, packaging, website, app, etc) according to your requirements, so please feel free to contact me if you have any questions.\r\n\r\n",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_extend_resquest_status": "",
  "d_added": "2018-07-18 18:12:14",
  "d_modified": "2018-07-18 18:12:14"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b561b8f516b462ef00036ae"),
  "e_type": "order_deliver",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b4f7c29516b462714005316",
  "d_added": "2018-07-23 18:16:47",
  "d_modified": "2018-07-23 18:16:47"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b561bc3516b462ef00036af"),
  "e_type": "order_deliver",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b4f7c29516b462714005316",
  "d_added": "2018-07-23 18:17:39",
  "d_modified": "2018-07-23 18:17:39"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b561c05516b462ef00036b0"),
  "e_type": "order_deliver",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b4f7c29516b462714005316",
  "d_added": "2018-07-23 18:18:45",
  "d_modified": "2018-07-23 18:18:45"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b561c0f516b462ef00036b1"),
  "e_type": "order_deliver",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_order_id": "5b4f7c29516b462714005316",
  "d_added": "2018-07-23 18:18:55",
  "d_modified": "2018-07-23 18:18:55"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b561c13516b462ef00036b2"),
  "v_order_id": "5b4f7c29516b462714005316",
  "v_order_deliver": "1",
  "l_message": "This is test deliver order",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_extend_resquest_status": "",
  "d_added": "2018-07-23 18:18:59",
  "d_modified": "2018-07-23 18:18:59"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b576e15516b460db00053f0"),
  "v_order_id": "5b4f7c29516b462714005316",
  "l_message": "This is not deliver proper.and one more thinfg this test emnail",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_extend_resquest_status": "",
  "d_added": "2018-07-24 18:21:09",
  "d_modified": "2018-07-24 18:21:09"
});
db.getCollection("tbl_order_requirements").insert({
  "_id": ObjectId("5b58219f1b544d14d61d5fa3"),
  "v_order_id": "5b4f7c29516b462714005316",
  "v_order_deliver": "0",
  "l_message": "This is test message for the update.",
  "v_document_order": [
    null
  ],
  "e_type": "message",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "v_extend_resquest_status": "",
  "d_added": "2018-07-25 07:07:11",
  "d_modified": "2018-07-25 07:07:11"
});

/** tbl_packages records **/

/** tbl_pages records **/
db.getCollection("tbl_pages").insert({
  "_id": ObjectId("5a60509fd3e81219263c986a"),
  "i_parent_id": "",
  "v_title": "About us",
  "v_key": "about-us",
  "l_short_description": "",
  "l_description": "<div class=\"container\">\r\n<div class=\"scale-proper\">\r\n<div class=\"text-center\">\r\n<div class=\"vision-block\">\r\n<h3>Purpose</h3>\r\n\r\n<p class=\"purpose-status\">Saving our customers stress, time and money by providing a quick link to the skilled community</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"purpose-concept\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-4\">\r\n<div class=\"purpose-category\" style=\" object-fit: cover; \">\r\n<div class=\"purpose-img\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/1.1.png\" /></div>\r\n\r\n<div class=\"category-text section-backcolor\">\r\n<h2>GRASS MAINTANCE</h2>\r\n\r\n<p>I will create your garden design and backyard patio design, 2d and 3d sketches</p>\r\n\r\n<div class=\"star-category\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/star-final.png\" />\r\n<p class=\"text-star\">4.9 <span>( 34 )</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"starting-price\">\r\n<div class=\"pull-left\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/hart.png\" /></div>\r\n\r\n<div class=\"pull-right\">\r\n<p>STARTING AT <span>£5</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-4\">\r\n<div class=\"purpose-category active\">\r\n<div class=\"purpose-img\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/1.1.png\" /></div>\r\n\r\n<div class=\"category-text section-backcolor\">\r\n<h2>GRASS MAINTANCE</h2>\r\n\r\n<p>I will create your garden design and backyard patio design, 2d and 3d sketches</p>\r\n\r\n<div class=\"star-category\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/star-final.png\" />\r\n<p class=\"text-star\">4.9 <span>( 34 )</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"starting-price\">\r\n<div class=\"pull-left\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/hart.png\" /></div>\r\n\r\n<div class=\"pull-right\">\r\n<p>STARTING AT <span>£5</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-4\">\r\n<div class=\"purpose-category\" style=\"object-fit: none;\">\r\n<div class=\"purpose-img\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/1.1.png\" /></div>\r\n\r\n<div class=\"category-text section-backcolor\">\r\n<h2>GRASS MAINTANCE</h2>\r\n\r\n<p>I will create your garden design and backyard patio design, 2d and 3d sketches</p>\r\n\r\n<div class=\"star-category\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/star-final.png\" />\r\n<p class=\"text-star\">4.9 <span>( 34 )</span></p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"starting-price\">\r\n<div class=\"pull-left\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/hart.png\" /></div>\r\n\r\n<div class=\"pull-right\">\r\n<p>STARTING AT <span>£5</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n<div class=\"vision-block\">\r\n<h3>Values</h3>\r\n\r\n<p class=\"purpose-status\">Creating an eneragised community that has a sense of unconditional pride</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"container-fluid\">\r\n<div class=\"row\">\r\n<div class=\"header-img\"><img alt=\"\" class=\"img-responsive\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/value-img.png\" />\r\n<div class=\"container\">\r\n<div class=\"about-page\">\r\n<h2>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</h2>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"container\">\r\n<div class=\"text-center\">\r\n<div class=\"vision-block\">\r\n<h3>Culture</h3>\r\n\r\n<p class=\"purpose-status\">Discover, Select & Coannect to establish exciting new connections and skills</p>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-sm-4\">\r\n<div class=\"culture-img\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/culture-pic.png\" />\r\n<div class=\"img-containt\">Design & Creativity</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-4\">\r\n<div class=\"culture-img\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/culture-pic.png\" />\r\n<div class=\"img-containt\">Design & Creativity</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-4\">\r\n<div class=\"culture-img\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/culture-pic.png\" />\r\n<div class=\"img-containt\">Design & Creativity</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-sm-4\">\r\n<div class=\"culture-img\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/culture-pic.png\" />\r\n<div class=\"img-containt\">Design & Creativity</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-4\">\r\n<div class=\"culture-img\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/culture-pic.png\" />\r\n<div class=\"img-containt\">Design & Creativity</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-sm-4\">\r\n<div class=\"culture-img\"><img alt=\"\" src=\"http://www.skillbox.co.uk/public/Assets/frontend/images/culture-pic.png\" />\r\n<div class=\"img-containt\">Design & Creativity</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"text-center\">\r\n<div class=\"vision-block\">\r\n<h3>Vision</h3>\r\n\r\n<p>Through the SkillBox community we will make new skills available to the world</p>\r\n</div>\r\n</div>\r\n\r\n<div id=\"vmap\" style=\"width: 100%; height: 800px; margin: 0px auto; transform: rotateX(60deg);\"> </div>\r\n</div>\r\n<script>\r\n        jQuery(document).ready(function() {\r\n            jQuery('#vmap').vectorMap({\r\n                map: 'world_en',\r\n                backgroundColor: '#ffffff',\r\n                color: '#e8128c',\r\n\r\n                selectedColor: '#e8128c',\r\n                enableZoom: true,\r\n                showTooltip: true,\r\n                scaleColors: ['#e8128c', '#e8128c'],\r\n                values: sample_data,\r\n                normalizeFunction: 'polynomial'\r\n            });\r\n        });\r\n    </script>",
  "v_meta_title": "about",
  "v_meta_keywords": "about",
  "l_meta_description": "about",
  "e_status": "active",
  "v_banner_img": "pages/page-1531758102.png",
  "d_added": "2018-01-18 07:45:35",
  "d_modified": "2018-01-18 07:45:35",
  "v_banner_text": "About us"
});
db.getCollection("tbl_pages").insert({
  "_id": ObjectId("5a60512ad3e81219263c986b"),
  "i_parent_id": "5a60509fd3e81219263c986a",
  "v_title": "Purpose",
  "v_key": "purpose",
  "l_short_description": "<p background-color:=\"\" font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n",
  "l_description": "<p background-color:=\"\" font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p background-color:=\"\" font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n",
  "v_meta_title": "about",
  "v_meta_keywords": "about",
  "l_meta_description": "about",
  "e_status": "active",
  "v_page_icon": "pages-image-72251516261674.png",
  "d_added": "2018-01-18 07:47:54",
  "d_modified": "2018-01-18 07:47:54",
  "v_banner_img": "pages/page-1531758161.png",
  "v_banner_text": "Purpose"
});
db.getCollection("tbl_pages").insert({
  "_id": ObjectId("5aa778f1d3e812c50e3c9869"),
  "i_parent_id": "",
  "v_title": "privacy-policy",
  "v_key": "privacy-policy",
  "l_short_description": "This is test",
  "l_description": "<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-xs-12\">\r\n<div class=\"main-skillpolicy\">\r\n<div class=\"skill-box-policy\">\r\n<h2>SKILLBOX&#39;S Privacy Policy</h2>\r\n</div>\r\n\r\n<p class=\"para-privacy-policy\">SKILLBOX of 28 Cedar Court Road, Cheltenham, Gloucester GL53 7RB (\"We\") are committed to protecting and respecting your privacy. <span class=\"newline\"> This policy (together with our terms of use and any other documents referred to on it as set out at www.skillboxuk.com (Terms of Use)) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. PLEASE READ THE FOLLOWING CAREFULLY TO UNDERSTAND OUR VIEWS AND PRACTICES REGARDING YOUR PERSONAL DATA AND HOW WE WILL TREAT IT. BY VISITING WWW.SKILLBOXUK.COM YOU ARE ACCEPTING AND CONSENTING TO THE PRACTICES DESCRIBED IN THIS POLICY. For the purpose of the Data Protection Act 1998 (the Act), the data controller is SKILLBOX of 28 Cedar Court Road, Cheltenham, Gloucestershire, GL53 7RB. </span></p>\r\n\r\n<p class=\"collected-info\">Information we collect from you</p>\r\n\r\n<p class=\"para-heading\">We will collect and process the following data about you:</p>\r\n\r\n<div class=\"para-multiple\">\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information you give us. </span> This is information about you that you give us by filling in forms on our site www.skillboxuk.com (our site) or by corresponding with us by phone, e-mail or otherwise. It includes information you provide when you register to use our site, subscribe to our service, search for a product, place an order on our site, participate in discussion boards or other social media functions on our site, and when you report a problem with our site. The information you give us may include your name, address, e-mail address and phone number, financial and credit card information, personal description and photograph.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"para-multiple\">\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information we collect about you. </span> With regard to each of your visits to our site we will automatically collect the following information:</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> information about your visit, including the full Uniform Resource Locators (URL), clickstream to, through and from our site (including date and time), products you viewed or searched for&#39; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), methods used to browse away from the page, and any phone number used to call our customer service number.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"para-multiple\">\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information we receive from other sources.</span> We are working closely with third parties (including, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers, search information providers, credit reference agencies). We will notify you when we receive information about you from them and the purposes for which we intend to use that information.</li>\r\n</ul>\r\n</div>\r\n\r\n<p class=\"collected-info\">Cookies</p>\r\n\r\n<div class=\"para-multiple\">Our website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site.</div>\r\n\r\n<p class=\"para-heading\">Uses made of the information</p>\r\n\r\n<div class=\"para-multiple\">We use information held about you in the following ways:</div>\r\n\r\n<div class=\"para-multiple\">\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information you give to us.</span> We will use this information:</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to carry out our obligations arising from any contracts entered into between you and us and to provide you with the information, products and services that you request from us;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to provide you with information about other goods and services we offer that are similar to those that you have already purchased or enquired about;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to provide you, or permit selected third parties to provide you, with information about goods or services we feel may interest you. If you are an existing customer, we will only contact you by electronic means (e-mail) with information about goods and services similar to those which were the subject of a previous sale or negotiations of a sale to you. If you are a new customer, and where we permit selected third parties to use your data, we (or they) will contact you by electronic means only if you have consented to this. If you do not want us to use your data in this way, or to pass your details on to third parties for marketing purposes, please tick the relevant box situated on the form on which we collect your data (the registration form);</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to notify you about changes to our service;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to ensure that content from our site is presented in the most effective manner for you and for your computer.</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information we collect about you. We will use this information: </span></li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> Information we collect about you. We will use this information:</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to administer our site and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to improve our site to ensure that content is presented in the most effective manner for you and for your computer;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to allow you to participate in interactive features of our service, when you choose to do so;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> as part of our efforts to keep our site safe and secure;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you; 2</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to make suggestions and recommendations to you and other users of our site about goods or services that may interest you or them.</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information we receive from other sources. </span> We will combine this information with information you give to us and information we collect about you. We will use this information and the combined information for the purposes set out above (depending on the types of information we receive).</li>\r\n</ul>\r\n</div>\r\n\r\n<p class=\"para-heading\">Disclosure of your information</p>\r\n\r\n<div class=\"para-multiple\">You agree that we have the right to share your personal information with selected third parties including:\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> business partners, suppliers and sub-contractors for the performance of any contract we enter into with them or you;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> advertisers and advertising networks that require the data to select and serve relevant adverts to you and others. We may provide them with aggregate information about our users (for example, we may inform them that 500 men aged under 30 have clicked on their advertisement on any given day). We may also use such aggregate information to help advertisers reach the kind of audience they want to target (for example, women in SW1). We may make use of the personal data we have collected from you to enable us to comply with our advertisers&#39; wishes by displaying their advertisement to that target audience;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> analytics and search engine providers that assist us in the improvement and optimisation of our site.</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li>We will disclose your personal information to third parties:</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> In the event that we sell or buy any business or assets, in which case we will disclose your personal data to the prospective seller or buyer of such business or assets.</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> If SKILLBOX or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order:</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to enforce or apply our terms of use set out at www.skillboxuk.com and other agreements; or</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to protect the rights, property, or safety of SKILLBOX, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</li>\r\n</ul>\r\n</div>\r\n\r\n<p class=\"para-heading\">Where we store your personal data</p>\r\n\r\n<div class=\"para-multiple\">The data that we collect from you will be transferred to, and stored at, a destination outside the European Economic Area (\"EEA\"). It will also be processed by staff operating outside the EEA who work for us or for one of our suppliers. This includes staff engaged in, among other 3 things, the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy. <span class=\"newline\"> All information you provide to us is stored on our secure servers. Any payment transactions will be encrypted using SSL technology. Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone. </span> <span class=\"newline\"> Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access. </span></div>\r\n\r\n<p class=\"para-heading\">Your rights</p>\r\n\r\n<div class=\"para-multiple\"><span class=\"newline\">You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data. You can also exercise the right at any time by contacting us at SKILLBOX, 28 Cedar Court Road, Cheltenham, Gloucestershire, GL53 7RB. </span> <span class=\"newline\"> Our site may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites. </span></div>\r\n\r\n<p class=\"para-heading\">Access to information</p>\r\n\r\n<div class=\"para-multiple\">The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request will be subject to a fee of £10 to meet our costs in providing you with details of the information we hold about you.</div>\r\n\r\n<p class=\"para-heading\">Changes to our privacy policy</p>\r\n\r\n<div class=\"para-multiple\">Any changes we make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail. Please check back frequently to see any updates or changes to our privacy policy.</div>\r\n\r\n<p class=\"para-heading\">Contact</p>\r\n\r\n<div class=\"para-multiple\">Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to Hello@skillboxuk.com.</div>\r\n<a href=\"javascript:\" id=\"return-to-top\" style=\"display: none;\">TOP</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n",
  "v_meta_title": "privacy-policy",
  "v_meta_keywords": "privacy-policy",
  "l_meta_description": "privacy-policy",
  "e_status": "active",
  "v_page_icon": "pages-image-49321520924913.png",
  "v_banner_img": "pages/page-1531758198.png",
  "d_added": "2018-03-13 07:08:33",
  "d_modified": "2018-03-13 07:08:33",
  "v_banner_text": "SKILLBOX PRIVACY POLICY"
});
db.getCollection("tbl_pages").insert({
  "_id": ObjectId("5aa7a20ad3e812c60e3c9869"),
  "v_title": "Terms-of-service",
  "v_key": "terms-of-service",
  "v_banner_text": "SKILLBOX TERMS OF SERVICE",
  "l_description": "<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-xs-12\">\r\n<div class=\"main-term-service\">\r\n<div class=\"term-service-heading\">\r\n<h2>SKILLBOX&#39;S Terms of Use</h2>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">What&#39;s in these terms?</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>These terms tell you the rules for using our website <strong>www.skillboxuk.com</strong> (our site).</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Who we are and how to contact us</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>www.skillboxuk.com is a site operated by Peter Campion trading as SKILLBOX (\"We\"). Our main trading address is 28 Cedar Court Road, Cheltenham, Gloucestershire, GL53 7RB.</li>\r\n\t<li>To contact us, please email <strong>Hello@skillboxuk.com</strong></li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">By using our site you accept these terms</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>By using our site, you confirm that you accept these terms of use and that you agree to comply with them.</li>\r\n\t<li>If you do not agree to these terms, you must not use our site.</li>\r\n\t<li>We recommend that you print a copy of these terms for future reference.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">There are other terms that may apply to you</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Our Privacy Policy which sets out the terms on which we process any personal data we collect from you, or that you provide to us can be found on our site. By using our site, you consent to such processing and you warrant that all data provided by you is accurate.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">We may make changes to these terms</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We amend these terms from time to time. Every time you wish to use our site, please check these terms to ensure you understand the terms that apply at that time.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">We may make changes to our site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We may update and change our site from time to time to reflect changes to our products, our users&#39; needs and our business priorities.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">We may suspend or withdraw our site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Our site is made available free of charge.</li>\r\n\t<li>We do not guarantee that our site, or any content on it, will always be available or be uninterrupted. We may suspend or withdraw or restrict the availability of all or any part of our site for business and operational reasons. We will try to give you reasonable notice of any suspension or withdrawal.</li>\r\n\t<li>You are also responsible for ensuring that all persons who access our site through your internet connection are aware of these terms of use and other applicable terms and conditions, and that they comply with them.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Our site is for users in the UK</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Our site is directed to people residing in the United Kingdom. We do not represent that content available on or through our site is appropriate for use or available in other locations.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">You must keep your account details safe</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>If you choose, or you are provided with, a user identification code, password or any other piece of information as part of our security procedures, you must treat such information as confidential. You must not disclose it to any third party.</li>\r\n\t<li>We have the right to disable any user identification code or password, whether chosen by you or allocated by us, at any time, if in our reasonable opinion you have failed to comply with any of the provisions of these terms of use.</li>\r\n\t<li>If you know or suspect that anyone other than you knows your user identification code or password, you must promptly notify us at <strong> Hello@skillboxuk.com. </strong></li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">How you may use material on our site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We are the owner or the licensee of all intellectual property rights in our site, and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved.</li>\r\n\t<li>You may print off one copy, and may download extracts, of any page(s) from our site for your personal use and you may draw the attention of others within your organisation to content posted on our site.</li>\r\n\t<li>You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text. Our status (and that of any identified contributors) as the authors of content on our site must always be acknowledged.</li>\r\n\t<li>You must not use any part of the content on our site for commercial purposes without obtaining a licence to do so from us or our licensors.</li>\r\n\t<li>If you print off, copy or download any part of our site in breach of these terms of use, your right to use our site will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Acceptable Use Restrictions</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>You may use our site only for lawful purposes. You may not use our site:</li>\r\n</ul>\r\n\r\n<ul class=\"list-style list-space\">\r\n\t<li>In any way that breaches any applicable local, national or international law or regulation.</li>\r\n\t<li>In any way that is unlawful or fraudulent, or has any unlawful or fraudulent purpose or effect.</li>\r\n\t<li>For the purpose of harming or attempting to harm minors in any way.</li>\r\n\t<li>To transmit, or procure the sending of, any unsolicited or unauthorised advertising or promotional material or any other form of similar solicitation (spam).</li>\r\n\t<li>To knowingly transmit any data, send or upload any material that contains viruses, Trojan horses, worms, time-bombs, keystroke loggers, spyware, adware or any other harmful programs or similar computer code designed to adversely affect the operation of any computer software or hardware.</li>\r\n\t<li>To transmit any material that:</li>\r\n\t<li>is defamatory, obscene, offensive, hateful or inflammatory to any person or otherwise objectionable in relation to your use of our site;</li>\r\n\t<li>promotes sexually explicit material;</li>\r\n\t<li>promotes violence;</li>\r\n\t<li>promotes discrimination based on race, sex religion, nationality, disability, sexual orientation or age;</li>\r\n\t<li>infringes any intellectual property rights of any other person;</li>\r\n\t<li>is likely to deceive any person;</li>\r\n\t<li>breaches any legal duty owed to a third party (such as a contractual duty or a duty of confidence);</li>\r\n\t<li>promotes any illegal activity;</li>\r\n\t<li>is threatening, abusive or invades another&#39;s privacy, or causes annoyance, inconvenience or needless anxiety;</li>\r\n\t<li>is likely to harass, upset, embarrass, alarm or annoy any other person;</li>\r\n\t<li>Impersonates any person, or misrepresent your identity or affiliation with any person;</li>\r\n\t<li>Gives the impression that the material emanates from SKILLBOX, if this is not the case; or</li>\r\n\t<li>advocates, promotes, incites any party to commit, or assist any unlawful or criminal act such as (by way of example only) copyright infringement or computer misuse.</li>\r\n</ul>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>You also agree not to reproduce, duplicate, copy or re-sell any part of our site in contravention of the provisions of these terms of use</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Do not rely on information on this site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>The content on our site is provided for general information only. It is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on our site.</li>\r\n\t<li>Although we make reasonable efforts to update the information on our site, we make no representations, warranties or guarantees, whether express or implied, that the content on our site is accurate, complete or up to date.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">We are not responsible for websites we link to</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Where our site contains links to other sites and resources provided by third parties, these links are provided for your information only. Such links should not be interpreted as approval by us of those linked websites or information you may obtain from them.</li>\r\n\t<li>We have no control over the contents of those sites or resources.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">User-generated content is not approved by us</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>This website may include information and materials uploaded by other users of the site. This information and these materials have not been verified or approved by us. The views expressed by other users on our site do not represent our views or values.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>If you wish to complain about information and materials uploaded by other users please contact us on Complaints@skillboxuk.com.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Our responsibility for loss or damage suffered by you</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Whether you are a consumer or a business user, we do not exclude or limit in any way our liability to you where it would be unlawful to do so. This includes liability for death or personal injury caused by our negligence or the negligence of our employees, agents or subcontractors and for fraud or fraudulent misrepresentation.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">If you are a business user:</div>\r\n\r\n<ul class=\"list-style list-space\">\r\n\t<li>We exclude all implied conditions, warranties, representations or other terms that may apply to our site or any content on it.</li>\r\n\t<li>We will not be liable to you for any loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with:</li>\r\n\t<li>use of, or inability to use, our site; or</li>\r\n\t<li>use of or reliance on any content displayed on our site.</li>\r\n\t<li>In particular, we will not be liable for:</li>\r\n\t<li>loss of profits, sales, business, or revenue;</li>\r\n\t<li>business interruption;</li>\r\n\t<li>loss of anticipated savings;</li>\r\n\t<li>loss of business opportunity, goodwill or reputation; or</li>\r\n\t<li>any indirect or consequential loss or damage.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Uploading content to our site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Whenever you make use of a feature that allows you to upload content to our site, or to make contact with other users of our site, you must comply with the content standards set out in Acceptable Use Restrictions (see above).</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>You warrant that any such contribution does comply with those standards, and you will be liable to us and indemnify us for any breach of that warranty. This means you will be responsible for any loss or damage we suffer as a result of your breach of warranty.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Any content you upload to our site will be considered non-confidential and non-proprietary. You retain all of your ownership rights in your content, but you are required to grant us a limited licence to use, store and copy that content and to distribute and make it available to third parties. The rights you license to us are described in Rights you are giving us to use material you upload (see below).</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We also have the right to disclose your identity to any third party who is claiming that any content posted or uploaded by you to our site constitutes a violation of their intellectual property rights, or of their right to privacy.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We have the right to remove any posting you make on our site if, in our opinion, your post does not comply with the content standards set out in Acceptable Use Restrictions (see above).</li>\r\n\t<li>You are solely responsible for securing and backing up your content.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Rights you are giving us to use material you upload</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>When you upload or post content to our site, you grant us a non-exclusive, perpetual, irrevocable, worldwide licence (including the right to sub-licence) to publish, use, promote and distribute the content whether on the site, electronically, via digital platforms or media, or any other purpose whatsoever.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">We are not responsible for viruses and you must not introduce them</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We do not guarantee that our site will be secure or free from bugs or viruses.</li>\r\n\t<li>You are responsible for configuring your information technology, computer programmes and platform to access our site. You should use your own virus protection software.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>You must not misuse our site by knowingly introducing viruses, trojans, worms, logic bombs or other material that is malicious or technologically harmful. You must not attempt to gain unauthorised access to our site, the server on which our site is stored or any server, computer or database connected to our site. You must not attack our site via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our site will cease immediately.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Rules about linking to our site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li class=\"rules-link-space\">You may link to our home page, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it.</li>\r\n\t<li class=\"rules-link-space\">You must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.</li>\r\n\t<li class=\"rules-link-space\">You must not establish a link to our site in any website that is not owned by you.</li>\r\n\t<li class=\"rules-link-space\">Our site must not be framed on any other site, nor may you create a link to any part of our site other than the home page.</li>\r\n\t<li class=\"rules-link-space\">We reserve the right to withdraw linking permission without notice.</li>\r\n\t<li>The website in which you are linking must comply in all respects with the content standards set out in Acceptable Use Restrictions (see above).</li>\r\n\t<li>If you wish to link to or make any use of content on our site other than that set out above, please contact Media@skillboxuk.com.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Which country&#39;s laws apply to any disputes?</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li><strong>If you are a consumer,</strong> please note that these terms of use, their subject matter and their formation, are governed by English law. You and we both agree that the courts of England and Wales will have exclusive jurisdiction except that if you are a resident of Northern Ireland you may also bring proceedings in Northern Ireland, and if you are resident of Scotland, you may also bring proceedings in Scotland.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li><strong>If you are a business, </strong> these terms of use, their subject matter and their formation (and any non-contractual disputes or claims) are governed by English law. We both agree to the exclusive jurisdiction of the courts of England and Wales.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n",
  "v_meta_title": "SKILLBOX TERMS OF SERVICE",
  "v_meta_keywords": "SKILLBOX TERMS OF SERVICE",
  "l_meta_description": "SKILLBOX TERMS OF SERVICE",
  "e_status": "active",
  "v_banner_img": "pages/page-1531758237.png",
  "d_added": "2018-03-13 10:03:54",
  "d_modified": "2018-03-13 10:03:54"
});
db.getCollection("tbl_pages").insert({
  "_id": ObjectId("5aa7a24fd3e812c2173c9869"),
  "v_title": "Feedback",
  "v_key": "feedback",
  "v_banner_text": "FEEDBACK",
  "l_description": "<div class=\"col-sm-12 col-xs-12\">\r\n<div class=\"feedback\">\r\n<div class=\"feedback-title\">\r\n<h3>Your feedback is important to us</h3>\r\n</div>\r\n\r\n<div class=\"feedback-msg\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est</div>\r\n</div>\r\n</div>\r\n\r\n<form action=\"http://192.168.0.228/projects/skillbox/feedback\" class=\"horizontal-form\" data-parsley-validate=\"\" enctype=\"multipart/form-data\" id=\"feedbackform\" method=\"POST\" name=\"feedbackform\" novalidate=\"\" role=\"form\"><input name=\"_token\" type=\"hidden\" value=\"eEQvLRW0HBGQhqvlNGSkGoxk77igro06cmtJYekl\" />\r\n<div class=\"col-sm-12 col-xs-12\">\r\n<div class=\"form-feedback\">\r\n<div class=\"row-form-field\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-6 col-xs-12\"><label class=\"field-name\">First Name </label> <input class=\"form-control input-field\" name=\"v_first_name\" required=\"\" type=\"text\" /></div>\r\n\r\n<div class=\"col-sm-6 col-xs-12\"><label class=\"field-name\">Last Name </label> <input class=\"form-control input-field\" name=\"v_last_name\" required=\"\" type=\"text\" /></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row-form-field\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-6 col-xs-12\"><label class=\"field-name\">Email </label> <input class=\"form-control input-field\" name=\"v_email\" required=\"\" type=\"email\" /></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row-form-field\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12 col-xs-12\"><label class=\"field-name\">Please share as much details you can </label><textarea class=\"form-control cover-letter\" name=\"l_description\" required=\"\" rows=\"7\"></textarea></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"text-xs-center\"><button class=\"btn btn-submit\" type=\"submit\">Submit</button></div>\r\n</div>\r\n</div>\r\n</form>\r\n",
  "v_meta_title": "FEEDBACK",
  "v_meta_keywords": "FEEDBACK",
  "l_meta_description": "FEEDBACK",
  "e_status": "active",
  "v_banner_img": "pages/page-1531758289.png",
  "d_added": "2018-03-13 10:05:03",
  "d_modified": "2018-03-13 10:05:03"
});
db.getCollection("tbl_pages").insert({
  "_id": ObjectId("5aa7a2b6d3e812be183c9869"),
  "v_title": "Trust-safety",
  "v_key": "trust-safety",
  "v_banner_text": "TRUST & SAFETY",
  "l_description": "<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-xs-12\">\r\n<div class=\"para-1\">\r\n<div class=\"main-trustsafetys\">\r\n<div class=\"heading-trust\">\r\n<h2>Trust & Safety</h2>\r\n</div>\r\n\r\n<p class=\"first-para\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br />\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n",
  "v_meta_title": "TRUST & SAFETY",
  "v_meta_keywords": "TRUST & SAFETY",
  "l_meta_description": "TRUST & SAFETY",
  "e_status": "active",
  "v_banner_img": "pages/page-1531758322.png",
  "d_added": "2018-03-13 10:06:46",
  "d_modified": "2018-03-13 10:06:46"
});
db.getCollection("tbl_pages").insert({
  "_id": ObjectId("5aa7a318d3e812be183c986a"),
  "v_title": " Help & Support",
  "v_key": "help-support",
  "v_banner_text": " Help & Support",
  "l_description": "<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-xs-12\">\r\n<div class=\"main-term-service\">\r\n<div class=\"term-service-heading\">\r\n<h2>SKILLBOX&#39;S Terms of Use</h2>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">What&#39;s in these terms?</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>These terms tell you the rules for using our website <strong>www.skillboxuk.com</strong> (our site).</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Who we are and how to contact us</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>www.skillboxuk.com is a site operated by Peter Campion trading as SKILLBOX (\"We\"). Our main trading address is 28 Cedar Court Road, Cheltenham, Gloucestershire, GL53 7RB.</li>\r\n\t<li>To contact us, please email <strong>Hello@skillboxuk.com</strong></li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">By using our site you accept these terms</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>By using our site, you confirm that you accept these terms of use and that you agree to comply with them.</li>\r\n\t<li>If you do not agree to these terms, you must not use our site.</li>\r\n\t<li>We recommend that you print a copy of these terms for future reference.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">There are other terms that may apply to you</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Our Privacy Policy which sets out the terms on which we process any personal data we collect from you, or that you provide to us can be found on our site. By using our site, you consent to such processing and you warrant that all data provided by you is accurate.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">We may make changes to these terms</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We amend these terms from time to time. Every time you wish to use our site, please check these terms to ensure you understand the terms that apply at that time.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">We may make changes to our site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We may update and change our site from time to time to reflect changes to our products, our users&#39; needs and our business priorities.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">We may suspend or withdraw our site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Our site is made available free of charge.</li>\r\n\t<li>We do not guarantee that our site, or any content on it, will always be available or be uninterrupted. We may suspend or withdraw or restrict the availability of all or any part of our site for business and operational reasons. We will try to give you reasonable notice of any suspension or withdrawal.</li>\r\n\t<li>You are also responsible for ensuring that all persons who access our site through your internet connection are aware of these terms of use and other applicable terms and conditions, and that they comply with them.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Our site is for users in the UK</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Our site is directed to people residing in the United Kingdom. We do not represent that content available on or through our site is appropriate for use or available in other locations.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">You must keep your account details safe</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>If you choose, or you are provided with, a user identification code, password or any other piece of information as part of our security procedures, you must treat such information as confidential. You must not disclose it to any third party.</li>\r\n\t<li>We have the right to disable any user identification code or password, whether chosen by you or allocated by us, at any time, if in our reasonable opinion you have failed to comply with any of the provisions of these terms of use.</li>\r\n\t<li>If you know or suspect that anyone other than you knows your user identification code or password, you must promptly notify us at <strong> Hello@skillboxuk.com. </strong></li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">How you may use material on our site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We are the owner or the licensee of all intellectual property rights in our site, and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved.</li>\r\n\t<li>You may print off one copy, and may download extracts, of any page(s) from our site for your personal use and you may draw the attention of others within your organisation to content posted on our site.</li>\r\n\t<li>You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text. Our status (and that of any identified contributors) as the authors of content on our site must always be acknowledged.</li>\r\n\t<li>You must not use any part of the content on our site for commercial purposes without obtaining a licence to do so from us or our licensors.</li>\r\n\t<li>If you print off, copy or download any part of our site in breach of these terms of use, your right to use our site will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Acceptable Use Restrictions</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>You may use our site only for lawful purposes. You may not use our site:</li>\r\n</ul>\r\n\r\n<ul class=\"list-style list-space\">\r\n\t<li>In any way that breaches any applicable local, national or international law or regulation.</li>\r\n\t<li>In any way that is unlawful or fraudulent, or has any unlawful or fraudulent purpose or effect.</li>\r\n\t<li>For the purpose of harming or attempting to harm minors in any way.</li>\r\n\t<li>To transmit, or procure the sending of, any unsolicited or unauthorised advertising or promotional material or any other form of similar solicitation (spam).</li>\r\n\t<li>To knowingly transmit any data, send or upload any material that contains viruses, Trojan horses, worms, time-bombs, keystroke loggers, spyware, adware or any other harmful programs or similar computer code designed to adversely affect the operation of any computer software or hardware.</li>\r\n\t<li>To transmit any material that:</li>\r\n\t<li>is defamatory, obscene, offensive, hateful or inflammatory to any person or otherwise objectionable in relation to your use of our site;</li>\r\n\t<li>promotes sexually explicit material;</li>\r\n\t<li>promotes violence;</li>\r\n\t<li>promotes discrimination based on race, sex religion, nationality, disability, sexual orientation or age;</li>\r\n\t<li>infringes any intellectual property rights of any other person;</li>\r\n\t<li>is likely to deceive any person;</li>\r\n\t<li>breaches any legal duty owed to a third party (such as a contractual duty or a duty of confidence);</li>\r\n\t<li>promotes any illegal activity;</li>\r\n\t<li>is threatening, abusive or invades another&#39;s privacy, or causes annoyance, inconvenience or needless anxiety;</li>\r\n\t<li>is likely to harass, upset, embarrass, alarm or annoy any other person;</li>\r\n\t<li>Impersonates any person, or misrepresent your identity or affiliation with any person;</li>\r\n\t<li>Gives the impression that the material emanates from SKILLBOX, if this is not the case; or</li>\r\n\t<li>advocates, promotes, incites any party to commit, or assist any unlawful or criminal act such as (by way of example only) copyright infringement or computer misuse.</li>\r\n</ul>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>You also agree not to reproduce, duplicate, copy or re-sell any part of our site in contravention of the provisions of these terms of use</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Do not rely on information on this site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>The content on our site is provided for general information only. It is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on our site.</li>\r\n\t<li>Although we make reasonable efforts to update the information on our site, we make no representations, warranties or guarantees, whether express or implied, that the content on our site is accurate, complete or up to date.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">We are not responsible for websites we link to</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Where our site contains links to other sites and resources provided by third parties, these links are provided for your information only. Such links should not be interpreted as approval by us of those linked websites or information you may obtain from them.</li>\r\n\t<li>We have no control over the contents of those sites or resources.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">User-generated content is not approved by us</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>This website may include information and materials uploaded by other users of the site. This information and these materials have not been verified or approved by us. The views expressed by other users on our site do not represent our views or values.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>If you wish to complain about information and materials uploaded by other users please contact us on Complaints@skillboxuk.com.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Our responsibility for loss or damage suffered by you</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Whether you are a consumer or a business user, we do not exclude or limit in any way our liability to you where it would be unlawful to do so. This includes liability for death or personal injury caused by our negligence or the negligence of our employees, agents or subcontractors and for fraud or fraudulent misrepresentation.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">If you are a business user:</div>\r\n\r\n<ul class=\"list-style list-space\">\r\n\t<li>We exclude all implied conditions, warranties, representations or other terms that may apply to our site or any content on it.</li>\r\n\t<li>We will not be liable to you for any loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with:</li>\r\n\t<li>use of, or inability to use, our site; or</li>\r\n\t<li>use of or reliance on any content displayed on our site.</li>\r\n\t<li>In particular, we will not be liable for:</li>\r\n\t<li>loss of profits, sales, business, or revenue;</li>\r\n\t<li>business interruption;</li>\r\n\t<li>loss of anticipated savings;</li>\r\n\t<li>loss of business opportunity, goodwill or reputation; or</li>\r\n\t<li>any indirect or consequential loss or damage.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Uploading content to our site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Whenever you make use of a feature that allows you to upload content to our site, or to make contact with other users of our site, you must comply with the content standards set out in Acceptable Use Restrictions (see above).</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>You warrant that any such contribution does comply with those standards, and you will be liable to us and indemnify us for any breach of that warranty. This means you will be responsible for any loss or damage we suffer as a result of your breach of warranty.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>Any content you upload to our site will be considered non-confidential and non-proprietary. You retain all of your ownership rights in your content, but you are required to grant us a limited licence to use, store and copy that content and to distribute and make it available to third parties. The rights you license to us are described in Rights you are giving us to use material you upload (see below).</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We also have the right to disclose your identity to any third party who is claiming that any content posted or uploaded by you to our site constitutes a violation of their intellectual property rights, or of their right to privacy.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We have the right to remove any posting you make on our site if, in our opinion, your post does not comply with the content standards set out in Acceptable Use Restrictions (see above).</li>\r\n\t<li>You are solely responsible for securing and backing up your content.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Rights you are giving us to use material you upload</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>When you upload or post content to our site, you grant us a non-exclusive, perpetual, irrevocable, worldwide licence (including the right to sub-licence) to publish, use, promote and distribute the content whether on the site, electronically, via digital platforms or media, or any other purpose whatsoever.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">We are not responsible for viruses and you must not introduce them</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>We do not guarantee that our site will be secure or free from bugs or viruses.</li>\r\n\t<li>You are responsible for configuring your information technology, computer programmes and platform to access our site. You should use your own virus protection software.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li>You must not misuse our site by knowingly introducing viruses, trojans, worms, logic bombs or other material that is malicious or technologically harmful. You must not attempt to gain unauthorised access to our site, the server on which our site is stored or any server, computer or database connected to our site. You must not attack our site via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our site will cease immediately.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Rules about linking to our site</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li class=\"rules-link-space\">You may link to our home page, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it.</li>\r\n\t<li class=\"rules-link-space\">You must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.</li>\r\n\t<li class=\"rules-link-space\">You must not establish a link to our site in any website that is not owned by you.</li>\r\n\t<li class=\"rules-link-space\">Our site must not be framed on any other site, nor may you create a link to any part of our site other than the home page.</li>\r\n\t<li class=\"rules-link-space\">We reserve the right to withdraw linking permission without notice.</li>\r\n\t<li>The website in which you are linking must comply in all respects with the content standards set out in Acceptable Use Restrictions (see above).</li>\r\n\t<li>If you wish to link to or make any use of content on our site other than that set out above, please contact Media@skillboxuk.com.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<div class=\"paragraph-heading\">Which country&#39;s laws apply to any disputes?</div>\r\n\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li><strong>If you are a consumer,</strong> please note that these terms of use, their subject matter and their formation, are governed by English law. You and we both agree that the courts of England and Wales will have exclusive jurisdiction except that if you are a resident of Northern Ireland you may also bring proceedings in Northern Ireland, and if you are resident of Scotland, you may also bring proceedings in Scotland.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"paragraph\">\r\n<ul class=\"list-unstyled list-style\">\r\n\t<li><strong>If you are a business, </strong> these terms of use, their subject matter and their formation (and any non-contractual disputes or claims) are governed by English law. We both agree to the exclusive jurisdiction of the courts of England and Wales.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n",
  "v_meta_title": " Help & Support",
  "v_meta_keywords": " Help & Support",
  "l_meta_description": " Help & Support",
  "e_status": "active",
  "v_banner_img": "pages/page-1531758349.png",
  "d_added": "2018-03-13 10:08:24",
  "d_modified": "2018-03-13 10:08:24"
});
db.getCollection("tbl_pages").insert({
  "_id": ObjectId("5aa7a4c4d3e812361c3c9869"),
  "v_title": " Skillbox Careers",
  "v_key": "skillbox-careers",
  "v_banner_text": " Skillbox Careers",
  "l_description": "<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-xs-12\">\r\n<div class=\"main-skillpolicy\">\r\n<div class=\"skill-box-policy\">\r\n<h2>SKILLBOX&#39;S Privacy Policy</h2>\r\n</div>\r\n\r\n<p class=\"para-privacy-policy\">SKILLBOX of 28 Cedar Court Road, Cheltenham, Gloucester GL53 7RB (\"We\") are committed to protecting and respecting your privacy. <span class=\"newline\"> This policy (together with our terms of use and any other documents referred to on it as set out at www.skillboxuk.com (Terms of Use)) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. PLEASE READ THE FOLLOWING CAREFULLY TO UNDERSTAND OUR VIEWS AND PRACTICES REGARDING YOUR PERSONAL DATA AND HOW WE WILL TREAT IT. BY VISITING WWW.SKILLBOXUK.COM YOU ARE ACCEPTING AND CONSENTING TO THE PRACTICES DESCRIBED IN THIS POLICY. For the purpose of the Data Protection Act 1998 (the Act), the data controller is SKILLBOX of 28 Cedar Court Road, Cheltenham, Gloucestershire, GL53 7RB. </span></p>\r\n\r\n<p class=\"collected-info\">Information we collect from you</p>\r\n\r\n<p class=\"para-heading\">We will collect and process the following data about you:</p>\r\n\r\n<div class=\"para-multiple\">\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information you give us. </span> This is information about you that you give us by filling in forms on our site www.skillboxuk.com (our site) or by corresponding with us by phone, e-mail or otherwise. It includes information you provide when you register to use our site, subscribe to our service, search for a product, place an order on our site, participate in discussion boards or other social media functions on our site, and when you report a problem with our site. The information you give us may include your name, address, e-mail address and phone number, financial and credit card information, personal description and photograph.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"para-multiple\">\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information we collect about you. </span> With regard to each of your visits to our site we will automatically collect the following information:</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> information about your visit, including the full Uniform Resource Locators (URL), clickstream to, through and from our site (including date and time), products you viewed or searched for&#39; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), methods used to browse away from the page, and any phone number used to call our customer service number.</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"para-multiple\">\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information we receive from other sources.</span> We are working closely with third parties (including, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers, search information providers, credit reference agencies). We will notify you when we receive information about you from them and the purposes for which we intend to use that information.</li>\r\n</ul>\r\n</div>\r\n\r\n<p class=\"collected-info\">Cookies</p>\r\n\r\n<div class=\"para-multiple\">Our website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site.</div>\r\n\r\n<p class=\"para-heading\">Uses made of the information</p>\r\n\r\n<div class=\"para-multiple\">We use information held about you in the following ways:</div>\r\n\r\n<div class=\"para-multiple\">\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information you give to us.</span> We will use this information:</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to carry out our obligations arising from any contracts entered into between you and us and to provide you with the information, products and services that you request from us;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to provide you with information about other goods and services we offer that are similar to those that you have already purchased or enquired about;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to provide you, or permit selected third parties to provide you, with information about goods or services we feel may interest you. If you are an existing customer, we will only contact you by electronic means (e-mail) with information about goods and services similar to those which were the subject of a previous sale or negotiations of a sale to you. If you are a new customer, and where we permit selected third parties to use your data, we (or they) will contact you by electronic means only if you have consented to this. If you do not want us to use your data in this way, or to pass your details on to third parties for marketing purposes, please tick the relevant box situated on the form on which we collect your data (the registration form);</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to notify you about changes to our service;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to ensure that content from our site is presented in the most effective manner for you and for your computer.</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information we collect about you. We will use this information: </span></li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> Information we collect about you. We will use this information:</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to administer our site and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to improve our site to ensure that content is presented in the most effective manner for you and for your computer;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to allow you to participate in interactive features of our service, when you choose to do so;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> as part of our efforts to keep our site safe and secure;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you; 2</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to make suggestions and recommendations to you and other users of our site about goods or services that may interest you or them.</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"bolt-dot\" src=\"images/dot.png\" /> <span class=\"para-bold-statement\"> Information we receive from other sources. </span> We will combine this information with information you give to us and information we collect about you. We will use this information and the combined information for the purposes set out above (depending on the types of information we receive).</li>\r\n</ul>\r\n</div>\r\n\r\n<p class=\"para-heading\">Disclosure of your information</p>\r\n\r\n<div class=\"para-multiple\">You agree that we have the right to share your personal information with selected third parties including:\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> business partners, suppliers and sub-contractors for the performance of any contract we enter into with them or you;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> advertisers and advertising networks that require the data to select and serve relevant adverts to you and others. We may provide them with aggregate information about our users (for example, we may inform them that 500 men aged under 30 have clicked on their advertisement on any given day). We may also use such aggregate information to help advertisers reach the kind of audience they want to target (for example, women in SW1). We may make use of the personal data we have collected from you to enable us to comply with our advertisers&#39; wishes by displaying their advertisement to that target audience;</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> analytics and search engine providers that assist us in the improvement and optimisation of our site.</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li>We will disclose your personal information to third parties:</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> In the event that we sell or buy any business or assets, in which case we will disclose your personal data to the prospective seller or buyer of such business or assets.</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> If SKILLBOX or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order:</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to enforce or apply our terms of use set out at www.skillboxuk.com and other agreements; or</li>\r\n</ul>\r\n\r\n<ul>\r\n\t<li><img alt=\"dot\" class=\"normal-dot\" src=\"images/dot.png\" /> to protect the rights, property, or safety of SKILLBOX, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</li>\r\n</ul>\r\n</div>\r\n\r\n<p class=\"para-heading\">Where we store your personal data</p>\r\n\r\n<div class=\"para-multiple\">The data that we collect from you will be transferred to, and stored at, a destination outside the European Economic Area (\"EEA\"). It will also be processed by staff operating outside the EEA who work for us or for one of our suppliers. This includes staff engaged in, among other 3 things, the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy. <span class=\"newline\"> All information you provide to us is stored on our secure servers. Any payment transactions will be encrypted using SSL technology. Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone. </span> <span class=\"newline\"> Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access. </span></div>\r\n\r\n<p class=\"para-heading\">Your rights</p>\r\n\r\n<div class=\"para-multiple\"><span class=\"newline\">You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data. You can also exercise the right at any time by contacting us at SKILLBOX, 28 Cedar Court Road, Cheltenham, Gloucestershire, GL53 7RB. </span> <span class=\"newline\"> Our site may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites. </span></div>\r\n\r\n<p class=\"para-heading\">Access to information</p>\r\n\r\n<div class=\"para-multiple\">The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request will be subject to a fee of £10 to meet our costs in providing you with details of the information we hold about you.</div>\r\n\r\n<p class=\"para-heading\">Changes to our privacy policy</p>\r\n\r\n<div class=\"para-multiple\">Any changes we make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail. Please check back frequently to see any updates or changes to our privacy policy.</div>\r\n\r\n<p class=\"para-heading\">Contact</p>\r\n\r\n<div class=\"para-multiple\">Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to Hello@skillboxuk.com.</div>\r\n<a href=\"javascript:\" id=\"return-to-top\" style=\"display: none;\">TOP</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n",
  "v_meta_title": " Skillbox Careers",
  "v_meta_keywords": " Skillbox Careers",
  "l_meta_description": " Skillbox Careers",
  "e_status": "active",
  "v_banner_img": "pages/page-1531758378.png",
  "d_added": "2018-03-13 10:15:32",
  "d_modified": "2018-03-13 10:15:32"
});
db.getCollection("tbl_pages").insert({
  "_id": ObjectId("5b4cd909516b46238c001f9a"),
  "v_title": "how to guide",
  "v_key": "how-to-guide",
  "v_banner_text": "how to guide",
  "l_description": "<div class=\"container\">\r\n<div class=\"scale-proper\">\r\n<div class=\"text-center\">\r\n<div class=\"vision-block\">\r\n<h2 style=\"margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">What is Lorem Ipsum?</h2>\r\n\r\n<p open=\"\" style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p open=\"\" style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \"> </p>\r\n\r\n<h2 style=\"margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Where does it come from?</h2>\r\n\r\n<p open=\"\" style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p>\r\n\r\n<p open=\"\" style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \"> </p>\r\n\r\n<h2 style=\"margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Where can I get some?</h2>\r\n\r\n<p open=\"\" style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n\r\n<p open=\"\" style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \"> </p>\r\n\r\n<h2 style=\"margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Why do we use it?</h2>\r\n\r\n<p open=\"\" style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n",
  "v_meta_title": "how to guide",
  "v_meta_keywords": "how to guide",
  "l_meta_description": "how to guide",
  "e_status": "active",
  "v_banner_img": "pages/page-1531762945.png",
  "d_added": "2018-07-16 05:42:33",
  "d_modified": "2018-07-16 05:42:33"
});
db.getCollection("tbl_pages").insert({
  "_id": ObjectId("5b688f5d1b544d050a2bea93"),
  "v_title": "Learn the way",
  "v_key": "learn-the-way",
  "v_banner_text": "Learn the way",
  "l_description": "<p background-color:=\"\" font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p background-color:=\"\" font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n",
  "v_meta_title": "Learn the way",
  "v_meta_keywords": "Learn the way",
  "l_meta_description": "Learn the way",
  "e_status": "active",
  "v_banner_img": "pages/page-1533579097.png",
  "d_added": "2018-08-06 06:11:41",
  "d_modified": "2018-08-06 06:11:41"
});

/** tbl_plan records **/
db.getCollection("tbl_plan").insert({
  "_id": ObjectId("5a65b48cd3e812a4253c9869"),
  "v_name": "Basic Plan",
  "e_status": "active",
  "f_monthly_price": "0",
  "f_yearly_price": "0",
  "l_bullet": [
    "Better Visibility",
    "Three FREE post job under In Person and Online account (To post more jobs simply upgrade to standard or premium)",
    "Sell your skills (Have profile in both accounts In Person and online)",
    "Unlimited Videos",
    "10 Photos per Job Posting",
    "Access to the free chat system",
    "FREE for life"
  ],
  "d_added": "2018-01-22 09:53:16",
  "v_subtitle": "Happy to see how it goes?",
  "f_monthly_dis_price": "0",
  "f_yearly_dis_price": "0",
  "l_addon": {
    "text": [
      "Make as Urgent £7 offer £5",
      "Notify Seller about your Job Posting £5 offer £3"
    ],
    "price": [
      "5",
      "3"
    ]
  }
});
db.getCollection("tbl_plan").insert({
  "_id": ObjectId("5a65b757d3e8125e323c986a"),
  "v_name": "Standard Plan",
  "v_subtitle": "Need to quickly fill your job post or sell your skills?",
  "e_status": "active",
  "l_bullet": [
    "Everything in Basic",
    "Bold ads - For top of category listings for your jobs and skills ",
    "Five free Job posts per account ",
    "Display your primary contact details( Telephone and web address )",
    "Double your click Rate"
  ],
  "f_monthly_price": "14.99",
  "f_monthly_dis_price": "9.99",
  "f_yearly_price": "167.8",
  "f_yearly_dis_price": "150.88",
  "d_added": "2018-01-22 10:05:11",
  "l_addon": {
    "text": [
      "Make as Urgent £7 offer £5",
      "Notify Seller about your Job Posting £5 offer £3"
    ],
    "price": [
      "5",
      "3"
    ]
  }
});
db.getCollection("tbl_plan").insert({
  "_id": ObjectId("5a65b9f4d3e8124f123c986c"),
  "v_name": "Premium Plan",
  "v_subtitle": "Regularly have a Job going",
  "e_status": "active",
  "l_bullet": [
    "Everything in Standard",
    "Full open access to Skillbox",
    "Unlimited Job Posting",
    "Bold ads - For top of category listings for your jobs and skills",
    "Home Page featured Area Ad Showcase for one profile"
  ],
  "f_monthly_price": "36.99",
  "f_monthly_dis_price": "25.99",
  "f_yearly_price": "443.88",
  "f_yearly_dis_price": "311.88",
  "d_added": "2018-01-22 10:16:20"
});

/** tbl_price_filter records **/
db.getCollection("tbl_price_filter").insert({
  "_id": ObjectId("5b167c0476fbae58ea6c3a16"),
  "v_title": "Under £50",
  "v_from_value": "0",
  "v_to_value": 50,
  "e_status": "active",
  "i_order": "1",
  "d_added": "2018-04-04 07:05:52",
  "d_modified": "2018-06-06 05:18:55"
});
db.getCollection("tbl_price_filter").insert({
  "_id": ObjectId("5b167c2076fbae628f770ee3"),
  "v_title": "£50 - £100",
  "v_from_value": 50,
  "v_to_value": 100,
  "e_status": "active",
  "i_order": "2",
  "d_added": "2018-04-04 07:05:52",
  "d_modified": "2018-04-04 07:05:52"
});
db.getCollection("tbl_price_filter").insert({
  "_id": ObjectId("5b167c3c76fbae594f236914"),
  "v_title": "£100 - £200",
  "v_from_value": 100,
  "v_to_value": 200,
  "e_status": "active",
  "i_order": "3",
  "d_added": "2018-04-04 07:05:52",
  "d_modified": "2018-04-04 07:05:52"
});
db.getCollection("tbl_price_filter").insert({
  "_id": ObjectId("5b167c5776fbae62635ec3f2"),
  "v_title": "£200 - £300",
  "v_from_value": "200",
  "v_to_value": "300",
  "e_status": "active",
  "i_order": "4",
  "d_added": "2018-04-04 07:05:52",
  "d_modified": "2018-04-04 07:05:52"
});
db.getCollection("tbl_price_filter").insert({
  "_id": ObjectId("5b167c7176fbae5f596146d3"),
  "v_title": "£300 - £400",
  "v_from_value": 300,
  "v_to_value": 400,
  "e_status": "active",
  "i_order": "5",
  "d_added": "2018-04-04 07:05:52",
  "d_modified": "2018-04-04 07:05:52"
});
db.getCollection("tbl_price_filter").insert({
  "_id": ObjectId("5b167c8376fbae53d553ed43"),
  "v_title": "£400 - £500",
  "v_from_value": 400,
  "v_to_value": 500,
  "e_status": "active",
  "i_order": "6",
  "d_added": "2018-04-04 07:05:52",
  "d_modified": "2018-04-04 07:05:52"
});
db.getCollection("tbl_price_filter").insert({
  "_id": ObjectId("5b167ca576fbae5e482ecbf6"),
  "v_title": "£500 +",
  "v_from_value": 500,
  "v_to_value": 0,
  "e_status": "active",
  "i_order": "7",
  "d_added": "2018-04-04 07:05:52",
  "d_modified": "2018-04-04 07:05:52"
});

/** tbl_resolution records **/
db.getCollection("tbl_resolution").insert({
  "_id": ObjectId("5b5826a41b544d157a425452"),
  "i_order_id": "5b4f7c29516b462714005316",
  "i_issue_id": "2",
  "v_subject": "Poor Code",
  "l_message": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n\r\n",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_seller_id": "5b165e1376fbae3c3e66bd33",
  "e_status": "active",
  "d_added": "2018-07-25 07:28:36",
  "d_modified": "2018-07-25 07:28:36",
  "d_reply_date": "2018-07-25 10:39:06",
  "l_reply": "This is tets replay"
});
db.getCollection("tbl_resolution").insert({
  "_id": ObjectId("5b5826b61b544d1dc84db902"),
  "i_order_id": "5b4f7c29516b462714005316",
  "i_issue_id": "2",
  "v_subject": "Poor Code",
  "l_message": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n\r\n",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_seller_id": "5b165e1376fbae3c3e66bd33",
  "e_status": "active",
  "d_added": "2018-07-25 07:28:54",
  "d_modified": "2018-07-25 07:28:54",
  "d_reply_date": "2018-07-25 10:28:10"
});

/** tbl_seller_profile records **/
db.getCollection("tbl_seller_profile").insert({
  "_id": ObjectId("5b165e7a76fbae3c8e6c7993"),
  "i_category_id": "5a606efbd3e81219263c986f",
  "i_mainskill_id": "5a607117d3e812063f3c986a",
  "i_otherskill_id": [
    "5a607117d3e812063f3c986a",
    "5a607124d3e812db133c9869"
  ],
  "v_experience_level": "intermediate",
  "e_notification_subscribe": "on",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-06-05 09:57:14",
  "d_modified": "2018-07-30 17:13:31",
  "v_profile_title": "Graphics & Design / Logo Design",
  "v_hourly_rate": "12",
  "v_website": "www.asdsadsa.com",
  "v_contact_phone": "12345646870",
  "l_short_description": "I Will Design The Perfect Logo For Your Business",
  "l_brief_description": "<p>I normally work on more concept proposals than my Gig says :-)<br />We will work on a creative, unique and professional logo design for your business.</p><p>It's very important to achieve exactly what you need for your business, so we enjoy working very close with our customers.</p><p>You will get from this gig:</p><p>- Original and unique different concepts for your brandring.</p><p>- Print and Web versions (CMYK & RGB).</p><p>- Vertical, horizontal and simplified alternate versions.<br />- Full colour, black and white colour variants.<br />- Vector files (Ai, PDF).<br />- Transparent background (PNG).</p><p>- Social Media Kit: Facebook & Twitter cover + profile photo design.</p><p>- Stationery Designs: Business card & letterhead design. </p><p>We can also on some other graphic design pieces (full stationery, packaging, website, app, etc) according to your requirements, so please feel free to contact me if you have any questions.</p><p>Style</p><ul><li>Flat/Minimalist</li></ul><p>File Format</p><ul><li>AI</li><li>JPG</li><li>PDF</li><li>PNG</li></ul>",
  "v_english_proficiency": "professional",
  "information": {
    "basic_package": {
      "v_title": "Basic Package",
      "v_bullets": [
        "Source File",
        "5 Initial Concepts Included",
        "Logo Transparency",
        "High Resolution"
      ],
      "v_delivery_time": "10",
      "v_delivery_type": "hours",
      "v_price": "100",
      "add_on": {
        "title": [
          "sppedup"
        ],
        "v_price": [
          "12  "
        ]
      }
    },
    "standard_package": {
      "v_title": "Standard Package",
      "v_bullets": [
        "Source File",
        "5 Initial Concepts Included",
        "Social Media Kit",
        "High Resolution"
      ],
      "v_delivery_time": "125",
      "v_delivery_type": "hours",
      "v_price": "150",
      "add_on": {
        "title": [
          "Add-on-1"
        ],
        "v_price": [
          "10  "
        ]
      }
    },
    "premium_package": {
      "v_title": "Premium Package",
      "v_bullets": [
        "3D Mockup",
        "Social Media Kit",
        "Stationery Designs",
        "Source File"
      ],
      "v_delivery_time": "80",
      "v_delivery_type": "hours",
      "v_price": "199",
      "add_on": {
        "title": [
          "Add-on-1"
        ],
        "v_price": [
          "10  "
        ]
      }
    }
  },
  "v_order_requirements": "<p>- Original and unique different concepts for your brandring.</p><p>- Print and Web versions (CMYK & RGB).</p><p>- Vertical, horizontal and simplified alternate versions.<br />- Full colour, black and white colour variants.<br />- Vector files (Ai, PDF).<br />- Transparent background (PNG).</p><p>- Social Media Kit: Facebook & Twitter cover + profile photo design.</p><p>- Stationery Designs: Business card & letterhead design. </p><p>We can also on some other graphic design pieces (full stationery, packaging, website, app, etc) according to your requirements, so please feel free to contact me if you have any questions.</p>",
  "v_introducy_video": "users/video/introducy_video-1528193076.mp4",
  "v_work_video": "users/video/work_video-1528193091.mp4",
  "v_work_photos": [
    "users/work_photo-1528193100.jpeg"
  ],
  "v_tags": [
    "Logo",
    "logo design",
    "graphics"
  ],
  "v_service": "online",
  "i_total_avg_review": NumberInt(5),
  "i_total_review": NumberInt(1),
  "e_status": "active"
});
db.getCollection("tbl_seller_profile").insert({
  "_id": ObjectId("5b16672776fbae3c3e66bd37"),
  "i_category_id": "5a606efbd3e81219263c986f",
  "i_mainskill_id": "5a607117d3e812063f3c986a",
  "i_otherskill_id": [
    "5a607117d3e812063f3c986a",
    "5a607124d3e812db133c9869"
  ],
  "v_experience_level": "expert",
  "e_notification_subscribe": "on",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "d_added": "2018-06-05 10:34:15",
  "d_modified": "2018-06-05 10:51:00",
  "v_profile_title": "Designer Head",
  "v_hourly_rate": "20",
  "v_website": "https://twitter.com",
  "v_contact_phone": "965823147",
  "l_short_description": " Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
  "l_brief_description": "<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>",
  "v_english_proficiency": "professional",
  "information": {
    "basic_package": {
      "v_title": "Lorem Ipsum",
      "v_bullets": [
        ""
      ],
      "v_delivery_time": "12",
      "v_delivery_type": "hours",
      "v_price": "250",
      "add_on": {
        "title": [
          " web page "
        ],
        "v_price": [
          "35 "
        ]
      }
    },
    "standard_package": {
      "v_title": "",
      "v_bullets": [
        ""
      ],
      "v_delivery_time": "",
      "v_delivery_type": "hours",
      "v_price": "",
      "add_on": {
        "title": [
          ""
        ],
        "v_price": [
          " "
        ]
      }
    },
    "premium_package": {
      "v_title": "",
      "v_bullets": [
        ""
      ],
      "v_delivery_time": "",
      "v_delivery_type": "hours",
      "v_price": "",
      "add_on": {
        "title": [
          ""
        ],
        "v_price": [
          " "
        ]
      }
    }
  },
  "v_order_requirements": "",
  "v_service": "online",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "e_status": "active"
});
db.getCollection("tbl_seller_profile").insert({
  "_id": ObjectId("5b16678576fbae3d3f03e804"),
  "i_category_id": "5a607009d3e812063f3c9869",
  "i_mainskill_id": "5a60714ed3e81220273c9869",
  "i_otherskill_id": [
    "5a60714ed3e81220273c9869"
  ],
  "v_experience_level": "entry",
  "e_notification_subscribe": "on",
  "i_user_id": "5b1666cd76fbae3d393c9a04",
  "d_added": "2018-06-05 10:35:49",
  "d_modified": "2018-06-05 10:41:10",
  "v_profile_title": "Web Deveoper",
  "v_hourly_rate": "110",
  "v_website": "www.google.com",
  "v_contact_phone": "1789722991",
  "l_short_description": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'",
  "l_brief_description": "<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'</p>",
  "v_english_proficiency": "professional",
  "information": {
    "basic_package": {
      "v_title": "Basic Package",
      "v_bullets": [
        "Basic Package 1 ",
        "Basic Package 2"
      ],
      "v_delivery_time": "100",
      "v_delivery_type": "hours",
      "v_price": "120",
      "add_on": {
        "title": [
          "Add-ons"
        ],
        "v_price": [
          "10"
        ]
      }
    },
    "standard_package": {
      "v_title": "Standard Package",
      "v_bullets": [
        "Standard Package 1",
        "Standard Package 2"
      ],
      "v_delivery_time": "1",
      "v_delivery_type": "hours",
      "v_price": "45",
      "add_on": {
        "title": [
          "Add-on"
        ],
        "v_price": [
          "45"
        ]
      }
    },
    "premium_package": {
      "v_title": "Premium Package",
      "v_bullets": [
        "Premium Package 1",
        "Premium Package 2"
      ],
      "v_delivery_time": "1",
      "v_delivery_type": "hours",
      "v_price": "500",
      "add_on": {
        "title": [
          "Add-on"
        ],
        "v_price": [
          "45"
        ]
      }
    }
  },
  "v_order_requirements": "<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'</p>",
  "v_introducy_video": "users/video/introducy_video-1528195245.mp4",
  "v_work_video": "users/video/work_video-1528195256.mp4",
  "v_work_photos": [
    "users/work_photo-1528195265.jpg"
  ],
  "v_tags": [
    "Ipsum",
    "letters",
    "Content"
  ],
  "v_service": "online",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "e_status": "active"
});
db.getCollection("tbl_seller_profile").insert({
  "_id": ObjectId("5b17d38176fbae34dd205c04"),
  "i_category_id": "5a607698d3e812302b3c986b",
  "i_mainskill_id": "5a6076c0d3e812fa3f3c9869",
  "i_otherskill_id": [
    "5a6076c0d3e812fa3f3c9869"
  ],
  "v_experience_level": "intermediate",
  "e_notification_subscribe": "on",
  "v_service": "inperson",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "d_added": "2018-06-06 12:28:49",
  "d_modified": "2018-06-07 08:50:08",
  "v_profile_title": "Test Inperson Profile",
  "v_hourly_rate": "12",
  "v_website": "www.asdsadsa.com",
  "v_contact_phone": "12345646870",
  "l_short_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n",
  "l_brief_description": "<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
  "v_english_proficiency": "professional",
  "v_order_requirements": "<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
  "v_introducy_video": "users/video/introducy_video-1528288304.mp4",
  "v_work_video": "users/video/work_video-1528288311.mp4",
  "v_work_photos": [
    "users/work_photo-1528288314.jpeg"
  ],
  "information": {
    "basic_package": {
      "v_title": "Basic Package",
      "v_bullets": [
        "Package 1 title",
        "Package 1 title"
      ],
      "v_delivery_time": "125",
      "v_delivery_type": "hours",
      "v_price": "100",
      "add_on": {
        "title": [
          "Add-on "
        ],
        "v_price": [
          "12"
        ]
      }
    },
    "standard_package": {
      "v_title": "Standard Package",
      "v_bullets": [
        "sadasd",
        "Standard Package"
      ],
      "v_delivery_time": "125",
      "v_delivery_type": "month",
      "v_price": "150",
      "add_on": {
        "title": [
          "Add-on"
        ],
        "v_price": [
          "15"
        ]
      }
    },
    "premium_package": {
      "v_title": "Premium Package",
      "v_bullets": [
        "Premium Package"
      ],
      "v_delivery_time": "22",
      "v_delivery_type": "hours",
      "v_price": "221",
      "add_on": {
        "title": [
          "asdsads"
        ],
        "v_price": [
          "22"
        ]
      }
    }
  },
  "i_total_avg_review": NumberInt(5),
  "i_total_review": NumberInt(1),
  "e_status": "active"
});
db.getCollection("tbl_seller_profile").insert({
  "_id": ObjectId("5b28f2a276fbae73f3225f32"),
  "i_category_id": "5a606cecd3e81206403c9869",
  "i_mainskill_id": "5a65d66bd3e8124f123c986f",
  "i_otherskill_id": [
    "5a65d66bd3e8124f123c986f",
    "5a65d67fd3e81204363c986b"
  ],
  "v_experience_level": "intermediate",
  "e_notification_subscribe": "on",
  "v_service": "inperson",
  "i_user_id": "5b28f27e76fbae48a0579b64",
  "d_added": "2018-06-19 12:10:10",
  "d_modified": "2018-06-19 12:12:50",
  "v_profile_title": "asdsad",
  "v_hourly_rate": "12",
  "v_website": "asdsadsa.com",
  "v_contact_phone": "12345646870",
  "information": {
    "basic_package": {
      "v_title": "Basic Package",
      "v_bullets": [
        "dfdsfdsfds",
        "asaSASAsa"
      ],
      "v_delivery_time": "125",
      "v_delivery_type": "hours",
      "v_price": "10",
      "add_on": {
        "title": [
          "Add-on "
        ],
        "v_price": [
          "12"
        ]
      }
    },
    "standard_package": {
      "v_title": "Standard Package",
      "v_bullets": [
        "Standard Package",
        "dsfdsfdsfdsf sdfdsfds"
      ],
      "v_delivery_time": "125",
      "v_delivery_type": "hours",
      "v_price": "95",
      "add_on": {
        "title": [
          "asdsadsa"
        ],
        "v_price": [
          "15"
        ]
      }
    },
    "premium_package": {
      "v_title": "Premium Package",
      "v_bullets": [
        "Premium Package",
        "sdsadsa asdsadsa"
      ],
      "v_delivery_time": "22",
      "v_delivery_type": "hours",
      "v_price": "221",
      "add_on": {
        "title": [
          "asdsads"
        ],
        "v_price": [
          "22"
        ]
      }
    }
  },
  "l_short_description": "sadsadsadsadsa",
  "l_brief_description": "<p>sadsadsadsa</p>",
  "v_english_proficiency": "native",
  "v_order_requirements": "<p>asdsadsa sadsadsadsa asdsadsa asdasdsa sadasdsa sadsadsadsa</p>",
  "v_introducy_video": "users/video/introducy_video-1529410365.mp4",
  "v_work_video": "users/video/work_video-1529410368.mp4",
  "v_work_photos": [
    "users/work_photo-1529410370.jpeg"
  ],
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "e_status": "active"
});
db.getCollection("tbl_seller_profile").insert({
  "_id": ObjectId("5b575242516b460db00053eb"),
  "i_category_id": "5a606efbd3e81219263c986f",
  "i_mainskill_id": "5a607117d3e812063f3c986a",
  "i_otherskill_id": [
    "5a607117d3e812063f3c986a"
  ],
  "v_experience_level": "intermediate",
  "e_notification_subscribe": "on",
  "v_service": "online",
  "i_user_id": "5b5751a1516b460db00053ea",
  "d_added": "2018-07-24 16:22:26",
  "d_modified": "2018-07-24 16:24:41",
  "information": {
    "basic_package": {
      "v_title": "Basic Package",
      "v_bullets": [
        "sadasdsaas",
        "asdsadasdas"
      ],
      "v_delivery_time": "12",
      "v_delivery_type": "hours",
      "v_price": "125",
      "add_on": {
        "title": [
          "asdsadas"
        ],
        "v_price": [
          "12"
        ]
      }
    },
    "standard_package": {
      "v_title": "Standard Package",
      "v_bullets": [
        "sdfdsf",
        "sdfsdf",
        "fdsfdsfsd",
        "sdfsdfsdfsd"
      ],
      "v_delivery_time": "125",
      "v_delivery_type": "hours",
      "v_price": "125",
      "add_on": {
        "title": [
          "sdfdsfsd"
        ],
        "v_price": [
          "125"
        ]
      }
    },
    "premium_package": {
      "v_title": "Premium Package",
      "v_bullets": [
        "sdasdsadsadas",
        "sdsadsa",
        "sadsadsa",
        "asdsadsadas"
      ],
      "v_delivery_time": "125",
      "v_delivery_type": "hours",
      "v_price": "125",
      "add_on": {
        "title": [
          "asdsadas"
        ],
        "v_price": [
          "125"
        ]
      }
    }
  },
  "l_brief_description": "<p>sdfdsfdsjdsioj idsnfiodsncvksduiofn vcisndfisd fk sduiofnsdf sduufnsd fsdifndsm fsdufnsdm fisdnfdsm fsduufd sm,f sdfsdn ofid fsdo fsdkf sdiof sd,fsdoin</p><p>sdfdsfdsjdsioj idsnfiodsncvksduiofn vcisndfisd fk sduiofnsdf sduufnsd fsdifndsm fsdufnsdm fisdnfdsm fsduufd sm,f sdfsdn ofid fsdo fsdkf sdiof sd,fsdoin</p><p>sdfdsfdsjdsioj idsnfiodsncvksduiofn vcisndfisd fk sduiofnsdf sduufnsd fsdifndsm fsdufnsdm fisdnfdsm fsduufd sm,f sdfsdn ofid fsdo fsdkf sdiof sd,fsdoin</p>",
  "l_short_description": "sdfdsfdsjdsioj idsnfiodsncvksduiofn vcisndfisd fk sduiofnsdf sduufnsd fsdifndsm fsdufnsdm fisdnfdsm fsduufd sm,f sdfsdn ofid fsdo fsdkf sdiof sd,fsdoin",
  "v_contact_phone": "1234654566",
  "v_english_proficiency": "professional",
  "v_hourly_rate": "12",
  "v_introducy_video": "users/video/introducy_video-1532449467.mp4",
  "v_order_requirements": "<p>asdsadsa sdfcsddsav dsfsdfdsf sdfsdf sdvsdcsdcc </p>",
  "v_profile_title": "This is test profile",
  "v_website": "asdsads.com",
  "v_work_photos": [
    "users/work_photo-1532449480.png"
  ],
  "v_work_video": "users/video/work_video-1532449474.mp4",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "e_status": "active"
});
db.getCollection("tbl_seller_profile").insert({
  "_id": ObjectId("5b5b62891b544d0bc7058230"),
  "i_category_id": "5a65d5add3e812a0323c986b",
  "i_mainskill_id": "5b5b5df51b544d2b3b78dae8",
  "i_otherskill_id": [
    "5b5b5a0a1b544d05312b079d",
    "5b5b5db31b544d0cc469e8d8",
    "5b5b5dde1b544d3fe76d5c63"
  ],
  "v_experience_level": "intermediate",
  "e_notification_subscribe": "on",
  "v_service": "online",
  "i_user_id": "5b5b617f1b544d2b3b78daed",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "d_added": "2018-07-27 18:20:57",
  "d_modified": "2018-07-27 18:20:57"
});

/** tbl_seller_review records **/
db.getCollection("tbl_seller_review").insert({
  "_id": ObjectId("5b479b38516b4625dc00765d"),
  "i_order_id": "5b179a5176fbae1d6d503293",
  "i_communication_star": "5",
  "i_described_star": "5",
  "i_recommend_star": "5",
  "l_comment": "This is test review data",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_seller_id": "5b165e1376fbae3c3e66bd33",
  "i_avg_star": "5.0",
  "d_added": "2018-07-12 18:17:28",
  "d_modified": "2018-07-12 18:17:28"
});

/** tbl_serches records **/
db.getCollection("tbl_serches").insert({
  "_id": ObjectId("5a609b30d3e8120f483c986a"),
  "v_text": "blogs",
  "v_ipaddress": "192.168.0.203",
  "d_added": "2018-01-18 01:03:44"
});

/** tbl_shortlisted_course records **/

/** tbl_shortlisted_jobs records **/
db.getCollection("tbl_shortlisted_jobs").insert({
  "_id": ObjectId("5b5f49cf1b544d0545602763"),
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_shortlist_id": "5b166bcf76fbae3b55286fe3",
  "d_added": "2018-07-30 17:24:31"
});

/** tbl_shortlisted_skills records **/
db.getCollection("tbl_shortlisted_skills").insert({
  "_id": ObjectId("5b28ea7d76fbae7304384a42"),
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "i_shortlist_id": "5b16672776fbae3c3e66bd37",
  "d_added": "2018-06-19 11:35:25"
});

/** tbl_sitesettings records **/
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c9869"),
  "v_key": "SITE_NAME",
  "v_value": "Skill Box"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c986a"),
  "v_key": "SITE_EMAIL",
  "v_value": "demo@gmail.com"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c986b"),
  "v_key": "CURRENCY_FOR_PAYMENT",
  "v_value": "9.99"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c986c"),
  "v_key": "SERVICE_FEES_BELOW",
  "v_value": "111"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c986d"),
  "v_key": "CURRENCY_SYMBOL",
  "v_value": "£"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c986e"),
  "v_key": "SECOEND_TOP_LEVEL_CAT_FEE",
  "v_value": "1"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c986f"),
  "v_key": "SERVICE_FEES_ABOVE",
  "v_value": "111"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c9870"),
  "v_key": "SMTP_HOST",
  "v_value": "smtp.gmail.com"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c9871"),
  "v_key": "SMTP_FROM_EMAIL",
  "v_value": "altagencydeveloper@gmail.com"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c9872"),
  "v_key": "SMTP_USERNAME",
  "v_value": "altagencydeveloper@gmail.com"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c9873"),
  "v_key": "SMTP_PORT",
  "v_value": "465"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c9874"),
  "v_key": "SMTP_PROTOCOL",
  "v_value": "ssl"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c9875"),
  "v_key": "SMTP_FROM_NAME",
  "v_value": "Skillbox Admin"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a617b0bd3e812ec093c9876"),
  "v_key": "SMTP_PASSWORD",
  "v_value": "alt000craig"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a61878ad3e8128a0d3c9869"),
  "v_key": "SITE_LOGO",
  "v_value": "1516341130-l-logo.png"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a7d9720d3e812cc253c9869"),
  "v_key": "PLAN_YEALY_DISCOUNT",
  "v_value": "15"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a828ac3d3e8122b243c9869"),
  "v_key": "SITE_FOOTER_TEXT",
  "v_value": "© 2018 SkillBox. All rights reserved"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a828ac3d3e8122b243c986a"),
  "v_key": "FACEBOOK_SOCIAL_LINK",
  "v_value": "https://www.facebook.com/skillbox"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a828ac3d3e8122b243c986b"),
  "v_key": "LINKEDIN_SOCIAL_LINK",
  "v_value": "https://www.facebook.com/skillbox"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a828ac3d3e8122b243c986c"),
  "v_key": "TEITTER_SOCIAL_LINK",
  "v_value": "https://twitter.com/hashtag/skillbox"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a828ac3d3e8122b243c986d"),
  "v_key": "SHOW_ACTIVE_USER",
  "v_value": "yes"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a828ac3d3e8122b243c986e"),
  "v_key": "YOUTUBE_SOCIAL_LINK",
  "v_value": "https://youtube.com/skillbox"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5a828ac3d3e8122b243c986f"),
  "v_key": "SHOW_APP_DOWNLOAD",
  "v_value": "yes"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5afd22f876fbae2d2c4a0da2"),
  "v_key": "PAYPAL_MODE",
  "v_value": "sendbox"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5afd22f876fbae2d2c4a0da3"),
  "v_key": "PAYPAL_EMIAL",
  "v_value": "shailesh.crestinfotech@gmail.com"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5b0e9e7f76fbae0e5c1002a5"),
  "v_key": "ACTIVE_SUBSCRIBERS_ALERTBOX",
  "v_value": " Hurrah! Skillbox reached 14 active subscribers"
});
db.getCollection("tbl_sitesettings").insert({
  "_id": ObjectId("5b0e9e7f76fbae0e5c1002a6"),
  "v_key": "SKILLBOX_APP_ALERTBOX",
  "v_value": "Download Skillbox Android App <a href=\"\">Here</a>"
});

/** tbl_skillbox_job records **/

/** tbl_skillbox_job_apply records **/

/** tbl_skillbox_job_category records **/
db.getCollection("tbl_skillbox_job_category").insert({
  "_id": ObjectId("5b09018976fbae0db133b2e4"),
  "v_title": "Business Development",
  "e_status": "active",
  "d_added": "2018-05-26 06:41:13",
  "d_modified": "2018-05-26 06:45:10",
  "i_order": "1"
});
db.getCollection("tbl_skillbox_job_category").insert({
  "_id": ObjectId("5b09028b76fbae0e99157615"),
  "v_title": "Design",
  "i_order": "2",
  "e_status": "active",
  "d_added": "2018-05-26 06:45:31",
  "d_modified": "2018-05-26 06:45:31"
});
db.getCollection("tbl_skillbox_job_category").insert({
  "_id": ObjectId("5b0902a876fbae0db462d0f4"),
  "v_title": "Engineering",
  "i_order": "3",
  "e_status": "active",
  "d_added": "2018-05-26 06:46:00",
  "d_modified": "2018-05-26 06:46:00"
});

/** tbl_skills records **/
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a607081d3e812b91b3c9869"),
  "v_name": "Home Repairs",
  "i_category_id": "5a606cecd3e81206403c9869",
  "e_status": "active",
  "d_added": "2018-01-18 10:01:37",
  "d_modified": "2018-01-22 12:17:35"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a6070efd3e812f51e3c9869"),
  "v_name": "Floor Cleaning",
  "i_category_id": "5a606e4ed3e81206403c986a",
  "e_status": "active",
  "d_added": "2018-01-18 10:03:27",
  "d_modified": "2018-01-18 10:03:27"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a607105d3e81241433c9869"),
  "v_name": "Doors & windows Cleaning",
  "i_category_id": "5a606e4ed3e81206403c986a",
  "e_status": "active",
  "d_added": "2018-01-18 10:03:49",
  "d_modified": "2018-01-18 10:03:49"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a607117d3e812063f3c986a"),
  "v_name": "Logo ",
  "i_category_id": "5a606efbd3e81219263c986f",
  "e_status": "active",
  "d_added": "2018-01-18 10:04:07",
  "d_modified": "2018-01-18 10:04:07"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a607124d3e812db133c9869"),
  "v_name": "Website",
  "i_category_id": "5a606efbd3e81219263c986f",
  "e_status": "active",
  "d_added": "2018-01-18 10:04:20",
  "d_modified": "2018-01-18 10:04:20"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a60714ed3e81220273c9869"),
  "v_name": "Digital Marketing",
  "i_category_id": "5a607009d3e812063f3c9869",
  "e_status": "active",
  "d_added": "2018-01-18 10:05:02",
  "d_modified": "2018-01-18 10:05:02"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a60716bd3e812062b3c986a"),
  "v_name": "Public Relations",
  "i_category_id": "5a607009d3e812063f3c9869",
  "e_status": "active",
  "d_added": "2018-01-18 10:05:31",
  "d_modified": "2018-01-18 10:05:31"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a6076c0d3e812fa3f3c9869"),
  "v_name": "Business",
  "i_category_id": "5a607698d3e812302b3c986b",
  "e_status": "active",
  "d_added": "2018-01-18 10:28:16",
  "d_modified": "2018-01-18 10:28:16"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a65d66bd3e8124f123c986f"),
  "v_name": "Decorating",
  "i_category_id": "5a606cecd3e81206403c9869",
  "e_status": "active",
  "d_added": "2018-01-22 12:17:47",
  "d_modified": "2018-01-22 12:17:47"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a65d674d3e81240363c986d"),
  "v_name": "Gardening",
  "i_category_id": "5a606cecd3e81206403c9869",
  "e_status": "active",
  "d_added": "2018-01-22 12:17:56",
  "d_modified": "2018-01-22 12:17:56"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a65d67fd3e81204363c986b"),
  "v_name": "Tool Hire",
  "i_category_id": "5a606cecd3e81206403c9869",
  "e_status": "active",
  "d_added": "2018-01-22 12:18:07",
  "d_modified": "2018-01-22 12:18:07"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a65d68ed3e812a0323c986d"),
  "v_name": "Odd Jobs",
  "i_category_id": "5a606cecd3e81206403c9869",
  "e_status": "active",
  "d_added": "2018-01-22 12:18:22",
  "d_modified": "2018-01-22 12:18:22"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a65d697d3e812a0323c986e"),
  "v_name": "Guttering & Drainage",
  "i_category_id": "5a606cecd3e81206403c9869",
  "e_status": "active",
  "d_added": "2018-01-22 12:18:31",
  "d_modified": "2018-01-22 12:18:31"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a65d6a1d3e812a0323c986f"),
  "v_name": "Jet Wash Service",
  "i_category_id": "5a606cecd3e81206403c9869",
  "e_status": "active",
  "d_added": "2018-01-22 12:18:41",
  "d_modified": "2018-01-22 12:18:41"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a65d6b0d3e81219333c986a"),
  "v_name": "Tree Surgeon",
  "i_category_id": "5a606cecd3e81206403c9869",
  "e_status": "active",
  "d_added": "2018-01-22 12:18:56",
  "d_modified": "2018-01-22 12:18:56"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5a65d6b9d3e812a4253c986d"),
  "v_name": "Other",
  "i_category_id": "5a606cecd3e81206403c9869",
  "e_status": "active",
  "d_added": "2018-01-22 12:19:05",
  "d_modified": "2018-01-22 12:19:05"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b46f51b544d05312b0794"),
  "v_name": "Vehicle Valeting",
  "i_category_id": "5a65d46fd3e81240363c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:23:17",
  "d_modified": "2018-07-27 04:23:17"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47091b544d0ede265512"),
  "v_name": "Vehicle Repairs",
  "i_category_id": "5a65d46fd3e81240363c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:23:37",
  "d_modified": "2018-07-27 04:23:37"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47181b544d0c0a4ed3d3"),
  "v_name": "Tuning",
  "i_category_id": "5a65d46fd3e81240363c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:23:52",
  "d_modified": "2018-07-27 04:23:52"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b472e1b544d0c0b0fd683"),
  "v_name": "Home Cleaning",
  "i_category_id": "5a606e4ed3e81206403c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:24:14",
  "d_modified": "2018-07-27 04:24:14"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b473c1b544d05312b0795"),
  "v_name": "Ironing",
  "i_category_id": "5a606e4ed3e81206403c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:24:28",
  "d_modified": "2018-07-27 04:24:28"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b474b1b544d0c0715b823"),
  "v_name": "Spring Clean",
  "i_category_id": "5a606e4ed3e81206403c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:24:43",
  "d_modified": "2018-07-27 04:24:43"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b475a1b544d0bc7058223"),
  "v_name": "Carpet Shampoo",
  "i_category_id": "5a606e4ed3e81206403c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:24:58",
  "d_modified": "2018-07-27 04:24:58"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47681b544d0c0a4ed3d4"),
  "v_name": "Oven Clean",
  "i_category_id": "5a606e4ed3e81206403c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:25:12",
  "d_modified": "2018-07-27 04:25:12"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47761b544d053226b495"),
  "v_name": "Window Clean",
  "i_category_id": "5a606e4ed3e81206403c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:25:26",
  "d_modified": "2018-07-27 04:25:26"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47861b544d0cc508e8d4"),
  "v_name": "Business Clean",
  "i_category_id": "5a606e4ed3e81206403c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:25:42",
  "d_modified": "2018-07-27 04:25:42"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47921b544d0cc508e8d5"),
  "v_name": "Other",
  "i_category_id": "5a606e4ed3e81206403c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:25:54",
  "d_modified": "2018-07-27 04:25:54"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47a81b544d0bc7058224"),
  "v_name": "Writing",
  "i_category_id": "5a65d483d3e8120c333c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:26:16",
  "d_modified": "2018-07-27 04:26:16"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47b71b544d0c0a4ed3d5"),
  "v_name": "E-mailing",
  "i_category_id": "5a65d483d3e8120c333c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:26:31",
  "d_modified": "2018-07-27 04:26:31"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47c31b544d0c0b0fd684"),
  "v_name": "Scheduling",
  "i_category_id": "5a65d483d3e8120c333c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:26:43",
  "d_modified": "2018-07-27 04:26:43"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47d21b544d0cc508e8d6"),
  "v_name": "Phone services",
  "i_category_id": "5a65d483d3e8120c333c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:26:58",
  "d_modified": "2018-07-27 04:26:58"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47e11b544d0c0715b824"),
  "v_name": "Social media service",
  "i_category_id": "5a65d483d3e8120c333c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:27:13",
  "d_modified": "2018-07-27 04:27:45"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b47ee1b544d0bc7058225"),
  "v_name": "Other",
  "i_category_id": "5a65d483d3e8120c333c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:27:26",
  "d_modified": "2018-07-27 04:27:26"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b48301b544d0cc508e8d7"),
  "v_name": "Computer Issues",
  "i_category_id": "5a65d494d3e812a4253c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:28:32",
  "d_modified": "2018-07-27 04:28:32"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b48401b544d0ede265513"),
  "v_name": "Hardware upgrade",
  "i_category_id": "5a65d494d3e812a4253c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:28:48",
  "d_modified": "2018-07-27 04:28:48"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b48551b544d053226b496"),
  "v_name": "Software Upgrade",
  "i_category_id": "5a65d494d3e812a4253c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:29:09",
  "d_modified": "2018-07-27 04:29:09"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b48631b544d0c0a4ed3d6"),
  "v_name": "Blogging & Publication",
  "i_category_id": "5a65d494d3e812a4253c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:29:23",
  "d_modified": "2018-07-27 04:29:23"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b486e1b544d0c0a4ed3d7"),
  "v_name": "Other",
  "i_category_id": "5a65d494d3e812a4253c986a",
  "e_status": "active",
  "d_added": "2018-07-27 04:29:34",
  "d_modified": "2018-07-27 04:29:34"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b488b1b544d0c0715b825"),
  "v_name": "House sitting",
  "i_category_id": "5a65d4a0d3e81255333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:30:03",
  "d_modified": "2018-07-27 04:30:03"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b48991b544d0bc7058226"),
  "v_name": "Babysitting",
  "i_category_id": "5a65d4a0d3e81255333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:30:17",
  "d_modified": "2018-07-27 04:30:17"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b48aa1b544d053226b497"),
  "v_name": "Au-Pair",
  "i_category_id": "5a65d4a0d3e81255333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:30:34",
  "d_modified": "2018-07-27 04:30:34"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b48bd1b544d0c0a4ed3d8"),
  "v_name": "Nursing",
  "i_category_id": "5a65d4a0d3e81255333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:30:53",
  "d_modified": "2018-07-27 04:30:53"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b48c91b544d0cc508e8d8"),
  "v_name": "Other",
  "i_category_id": "5a65d4a0d3e81255333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:31:05",
  "d_modified": "2018-07-27 04:31:05"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4c231b544d0bc7058227"),
  "v_name": "Dog walking",
  "i_category_id": "5a65d4b3d3e812a0323c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:45:23",
  "d_modified": "2018-07-27 04:45:23"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4c311b544d053226b498"),
  "v_name": " Animal grooming",
  "i_category_id": "5a65d4b3d3e812a0323c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:45:37",
  "d_modified": "2018-07-27 04:45:37"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4c3d1b544d05312b0796"),
  "v_name": "Animal sitting",
  "i_category_id": "5a65d4b3d3e812a0323c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:45:49",
  "d_modified": "2018-07-27 04:45:49"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4c4f1b544d0cc508e8d9"),
  "v_name": "Animal feeding",
  "i_category_id": "5a65d4b3d3e812a0323c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:46:07",
  "d_modified": "2018-07-27 04:46:07"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4c5f1b544d0ede265514"),
  "v_name": "Animal Care",
  "i_category_id": "5a65d4b3d3e812a0323c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:46:23",
  "d_modified": "2018-07-27 04:46:23"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4c6a1b544d0cc469e8d3"),
  "v_name": "Other",
  "i_category_id": "5a65d4b3d3e812a0323c9869",
  "e_status": "active",
  "d_added": "2018-07-27 04:46:34",
  "d_modified": "2018-07-27 04:46:34"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4d771b544d0c0a4ed3d9"),
  "v_name": " Sports Buddy",
  "i_category_id": "5b5b4d631b544d0c0b0fd685",
  "e_status": "active",
  "d_added": "2018-07-27 04:51:03",
  "d_modified": "2018-07-27 04:51:03"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4d8a1b544d0cc508e8da"),
  "v_name": "Music Buddy",
  "i_category_id": "5b5b4d631b544d0c0b0fd685",
  "e_status": "active",
  "d_added": "2018-07-27 04:51:22",
  "d_modified": "2018-07-27 04:51:22"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4d9c1b544d0ede265515"),
  "v_name": "Dance Buddy",
  "i_category_id": "5b5b4d631b544d0c0b0fd685",
  "e_status": "active",
  "d_added": "2018-07-27 04:51:40",
  "d_modified": "2018-07-27 04:51:40"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4e381b544d0bc7058229"),
  "v_name": "Hair Cut & Style",
  "i_category_id": "5a65d587d3e8125e323c986c",
  "e_status": "active",
  "d_added": "2018-07-27 04:54:16",
  "d_modified": "2018-07-27 04:54:16"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4e4d1b544d0c0b0fd686"),
  "v_name": "Beauty Treatment",
  "i_category_id": "5a65d587d3e8125e323c986c",
  "e_status": "active",
  "d_added": "2018-07-27 04:54:37",
  "d_modified": "2018-07-27 04:54:37"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4e5d1b544d05312b0798"),
  "v_name": "Make up & Design",
  "i_category_id": "5a65d587d3e8125e323c986c",
  "e_status": "active",
  "d_added": "2018-07-27 04:54:53",
  "d_modified": "2018-07-27 04:54:53"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b4e6e1b544d0cc371f853"),
  "v_name": "Blow Dry",
  "i_category_id": "5a65d587d3e8125e323c986c",
  "e_status": "active",
  "d_added": "2018-07-27 04:55:10",
  "d_modified": "2018-07-27 04:55:10"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b51c21b544d0c0715b827"),
  "v_name": " Home removals",
  "i_category_id": "5b5b4cd31b544d0bc7058228",
  "e_status": "active",
  "d_added": "2018-07-27 05:09:22",
  "d_modified": "2018-07-27 05:09:22"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b53491b544d053226b499"),
  "v_name": "Flat pack assembly",
  "i_category_id": "5b5b4cd31b544d0bc7058228",
  "e_status": "active",
  "d_added": "2018-07-27 05:15:53",
  "d_modified": "2018-07-27 05:15:53"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b535d1b544d0cc371f854"),
  "v_name": "Business removals",
  "i_category_id": "5b5b4cd31b544d0bc7058228",
  "e_status": "active",
  "d_added": "2018-07-27 05:16:13",
  "d_modified": "2018-07-27 05:16:13"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b536f1b544d05312b0799"),
  "v_name": "Man with a Van",
  "i_category_id": "5b5b4cd31b544d0bc7058228",
  "e_status": "active",
  "d_added": "2018-07-27 05:16:31",
  "d_modified": "2018-07-27 05:16:31"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b53821b544d2b3b78dae2"),
  "v_name": "Tip Trip",
  "i_category_id": "5b5b4cd31b544d0bc7058228",
  "e_status": "active",
  "d_added": "2018-07-27 05:16:50",
  "d_modified": "2018-07-27 05:16:50"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b53941b544d0cc508e8db"),
  "v_name": "Other",
  "i_category_id": "5b5b4cd31b544d0bc7058228",
  "e_status": "active",
  "d_added": "2018-07-27 05:17:08",
  "d_modified": "2018-07-27 05:17:08"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b53cf1b544d2b0654b372"),
  "v_name": "Personnel Chef",
  "i_category_id": "5b5b4cfe1b544d05312b0797",
  "e_status": "active",
  "d_added": "2018-07-27 05:18:07",
  "d_modified": "2018-07-27 05:18:07"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b53e91b544d0c0715b828"),
  "v_name": "Event Catering",
  "i_category_id": "5b5b4cfe1b544d05312b0797",
  "e_status": "active",
  "d_added": "2018-07-27 05:18:33",
  "d_modified": "2018-07-27 05:18:33"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b53fd1b544d32084edad2"),
  "v_name": "Celebration Cake",
  "i_category_id": "5b5b4cfe1b544d05312b0797",
  "e_status": "active",
  "d_added": "2018-07-27 05:18:53",
  "d_modified": "2018-07-27 05:18:53"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b540f1b544d0bc705822a"),
  "v_name": "Meals on Wheels",
  "i_category_id": "5b5b4cfe1b544d05312b0797",
  "e_status": "active",
  "d_added": "2018-07-27 05:19:11",
  "d_modified": "2018-07-27 05:19:11"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b54271b544d2b0654b373"),
  "v_name": "Pre-pared party food",
  "i_category_id": "5b5b4cfe1b544d05312b0797",
  "e_status": "active",
  "d_added": "2018-07-27 05:19:35",
  "d_modified": "2018-07-27 05:19:35"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b54371b544d2b3b78dae3"),
  "v_name": "Other",
  "i_category_id": "5b5b4cfe1b544d05312b0797",
  "e_status": "active",
  "d_added": "2018-07-27 05:19:51",
  "d_modified": "2018-07-27 05:19:51"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b54621b544d2b3b78dae4"),
  "v_name": " Van Hire",
  "i_category_id": "5b5b4d1e1b544d0c0715b826",
  "e_status": "active",
  "d_added": "2018-07-27 05:20:34",
  "d_modified": "2018-07-27 05:20:34"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b54781b544d326605d563"),
  "v_name": "Car Hire",
  "i_category_id": "5b5b4d1e1b544d0c0715b826",
  "e_status": "active",
  "d_added": "2018-07-27 05:20:56",
  "d_modified": "2018-07-27 05:20:56"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b548c1b544d05312b079a"),
  "v_name": "Share a Lift",
  "i_category_id": "5b5b4d1e1b544d0c0715b826",
  "e_status": "active",
  "d_added": "2018-07-27 05:21:16",
  "d_modified": "2018-07-27 05:21:16"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b54a11b544d0cc469e8d6"),
  "v_name": "Airport Lift",
  "i_category_id": "5b5b4d1e1b544d0c0715b826",
  "e_status": "active",
  "d_added": "2018-07-27 05:21:37",
  "d_modified": "2018-07-27 05:21:37"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b54b61b544d0bc705822b"),
  "v_name": "Taxi Services",
  "i_category_id": "5b5b4d1e1b544d0c0715b826",
  "e_status": "active",
  "d_added": "2018-07-27 05:21:58",
  "d_modified": "2018-07-27 05:21:58"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b54c91b544d2b3b78dae5"),
  "v_name": "Machine Hire",
  "i_category_id": "5b5b4d1e1b544d0c0715b826",
  "e_status": "active",
  "d_added": "2018-07-27 05:22:17",
  "d_modified": "2018-07-27 05:22:17"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b54db1b544d326605d564"),
  "v_name": "Other",
  "i_category_id": "5b5b4d1e1b544d0c0715b826",
  "e_status": "active",
  "d_added": "2018-07-27 05:22:35",
  "d_modified": "2018-07-27 05:22:35"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b55001b544d0bc705822c"),
  "v_name": "Nutrition Diet",
  "i_category_id": "5b5b4d461b544d0cc469e8d4",
  "e_status": "active",
  "d_added": "2018-07-27 05:23:12",
  "d_modified": "2018-07-27 05:23:12"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b55111b544d053226b49a"),
  "v_name": "Massage",
  "i_category_id": "5b5b4d461b544d0cc469e8d4",
  "e_status": "active",
  "d_added": "2018-07-27 05:23:29",
  "d_modified": "2018-07-27 05:23:29"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b55231b544d2b3b78dae6"),
  "v_name": "Physio",
  "i_category_id": "5b5b4d461b544d0cc469e8d4",
  "e_status": "active",
  "d_added": "2018-07-27 05:23:47",
  "d_modified": "2018-07-27 05:23:47"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b55361b544d326605d565"),
  "v_name": "General fitness",
  "i_category_id": "5b5b4d461b544d0cc469e8d4",
  "e_status": "active",
  "d_added": "2018-07-27 05:24:06",
  "d_modified": "2018-07-27 05:24:06"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b554b1b544d32084edad4"),
  "v_name": "Personnel Trainer ",
  "i_category_id": "5b5b4d461b544d0cc469e8d4",
  "e_status": "active",
  "d_added": "2018-07-27 05:24:27",
  "d_modified": "2018-07-27 05:24:27"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b55d21b544d0cc469e8d7"),
  "v_name": "Colouring & Highlights",
  "i_category_id": "5a65d587d3e8125e323c986c",
  "e_status": "active",
  "d_added": "2018-07-27 05:26:42",
  "d_modified": "2018-07-27 05:26:42"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b55e41b544d32084edad5"),
  "v_name": "Wax Treatment",
  "i_category_id": "5a65d587d3e8125e323c986c",
  "e_status": "active",
  "d_added": "2018-07-27 05:27:00",
  "d_modified": "2018-07-27 05:27:00"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b55f21b544d2b0654b374"),
  "v_name": "Other",
  "i_category_id": "5a65d587d3e8125e323c986c",
  "e_status": "active",
  "d_added": "2018-07-27 05:27:14",
  "d_modified": "2018-07-27 05:27:14"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b56481b544d33f34629f2"),
  "v_name": "Other",
  "i_category_id": "5b5b4d631b544d0c0b0fd685",
  "e_status": "active",
  "d_added": "2018-07-27 05:28:40",
  "d_modified": "2018-07-27 05:28:40"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5a0a1b544d05312b079d"),
  "v_name": "Logo Design & Branding",
  "i_category_id": "5a65d5add3e812a0323c986b",
  "e_status": "active",
  "d_added": "2018-07-27 05:44:42",
  "d_modified": "2018-07-27 05:44:42"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5da21b544d3fe76d5c62"),
  "v_name": "Graphic Design",
  "i_category_id": "5a65d5add3e812a0323c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:00:02",
  "d_modified": "2018-07-27 06:00:02"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5db31b544d0cc469e8d8"),
  "v_name": "Animation",
  "i_category_id": "5a65d5add3e812a0323c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:00:19",
  "d_modified": "2018-07-27 06:00:19"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5dc11b544d38cf687b52"),
  "v_name": "Illustration",
  "i_category_id": "5a65d5add3e812a0323c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:00:33",
  "d_modified": "2018-07-27 06:00:33"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5dd01b544d3a4b77edf3"),
  "v_name": "Photography",
  "i_category_id": "5a65d5add3e812a0323c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:00:48",
  "d_modified": "2018-07-27 06:00:48"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5dde1b544d3fe76d5c63"),
  "v_name": "Presentations",
  "i_category_id": "5a65d5add3e812a0323c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:01:02",
  "d_modified": "2018-07-27 06:01:02"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5df51b544d2b3b78dae8"),
  "v_name": "Video Production",
  "i_category_id": "5a65d5add3e812a0323c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:01:25",
  "d_modified": "2018-07-27 06:01:25"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5e051b544d0cc469e8d9"),
  "v_name": "Interior Design",
  "i_category_id": "5a65d5add3e812a0323c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:01:41",
  "d_modified": "2018-07-27 06:01:41"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5e7f1b544d0bc705822e"),
  "v_name": " Product Design",
  "i_category_id": "5a65d5add3e812a0323c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:03:43",
  "d_modified": "2018-07-27 06:03:43"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5e901b544d0cc469e8da"),
  "v_name": "Other",
  "i_category_id": "5a65d5add3e812a0323c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:04:00",
  "d_modified": "2018-07-27 06:04:00"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5efb1b544d3a3e631d13"),
  "v_name": "Academic & Research Writing",
  "i_category_id": "5a65d5c1d3e81219333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:05:47",
  "d_modified": "2018-07-27 06:05:47"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5f131b544d3fe76d5c64"),
  "v_name": "Article & Blogging",
  "i_category_id": "5a65d5c1d3e81219333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:06:11",
  "d_modified": "2018-07-27 06:06:11"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5f291b544d403a665cb2"),
  "v_name": "Creative Writing",
  "i_category_id": "5a65d5c1d3e81219333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:06:33",
  "d_modified": "2018-07-27 06:06:33"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5f3e1b544d2b3b78dae9"),
  "v_name": "Editing & Proofreading",
  "i_category_id": "5a65d5c1d3e81219333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:06:54",
  "d_modified": "2018-07-27 06:06:54"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5f571b544d3a3e631d14"),
  "v_name": "Resumes & Cover letters",
  "i_category_id": "5a65d5c1d3e81219333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:07:19",
  "d_modified": "2018-07-27 06:07:19"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5f681b544d3fe76d5c65"),
  "v_name": "Other",
  "i_category_id": "5a65d5c1d3e81219333c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:07:36",
  "d_modified": "2018-07-27 06:07:36"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5f821b544d0bc705822f"),
  "v_name": "Language Translation",
  "i_category_id": "5a65d5ced3e812a4253c986c",
  "e_status": "active",
  "d_added": "2018-07-27 06:08:02",
  "d_modified": "2018-07-27 06:08:02"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5f9a1b544d38cf687b53"),
  "v_name": "Legal Translation",
  "i_category_id": "5a65d5ced3e812a4253c986c",
  "e_status": "active",
  "d_added": "2018-07-27 06:08:26",
  "d_modified": "2018-07-27 06:08:26"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5faf1b544d409f44bdf2"),
  "v_name": "Medical Translation",
  "i_category_id": "5a65d5ced3e812a4253c986c",
  "e_status": "active",
  "d_added": "2018-07-27 06:08:47",
  "d_modified": "2018-07-27 06:08:47"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5fc51b544d0cc469e8db"),
  "v_name": "Technical Translation",
  "i_category_id": "5a65d5ced3e812a4253c986c",
  "e_status": "active",
  "d_added": "2018-07-27 06:09:09",
  "d_modified": "2018-07-27 06:09:09"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5fd31b544d403a665cb3"),
  "v_name": "Other",
  "i_category_id": "5a65d5ced3e812a4253c986c",
  "e_status": "active",
  "d_added": "2018-07-27 06:09:23",
  "d_modified": "2018-07-27 06:09:23"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b5fed1b544d3a3e631d15"),
  "v_name": "Contract law",
  "i_category_id": "5a65d5d9d3e8120c333c986e",
  "e_status": "active",
  "d_added": "2018-07-27 06:09:49",
  "d_modified": "2018-07-27 06:09:49"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b60041b544d3fe76d5c66"),
  "v_name": "Corporate Law",
  "i_category_id": "5a65d5d9d3e8120c333c986e",
  "e_status": "active",
  "d_added": "2018-07-27 06:10:12",
  "d_modified": "2018-07-27 06:10:12"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b60161b544d05312b079e"),
  "v_name": "Legal advice",
  "i_category_id": "5a65d5d9d3e8120c333c986e",
  "e_status": "active",
  "d_added": "2018-07-27 06:10:30",
  "d_modified": "2018-07-27 06:10:30"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b60271b544d3a4b77edf4"),
  "v_name": "Other",
  "i_category_id": "5a65d5d9d3e8120c333c986e",
  "e_status": "active",
  "d_added": "2018-07-27 06:10:47",
  "d_modified": "2018-07-27 06:10:47"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b60401b544d38cf687b54"),
  "v_name": "Virtual Assistant",
  "i_category_id": "5a65d5e2d3e8120c333c986f",
  "e_status": "active",
  "d_added": "2018-07-27 06:11:12",
  "d_modified": "2018-07-27 06:11:12"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b60581b544d2b3b78daea"),
  "v_name": "Data entry",
  "i_category_id": "5a65d5e2d3e8120c333c986f",
  "e_status": "active",
  "d_added": "2018-07-27 06:11:36",
  "d_modified": "2018-07-27 06:11:36"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b606d1b544d0cc469e8dc"),
  "v_name": "Online Research",
  "i_category_id": "5a65d5e2d3e8120c333c986f",
  "e_status": "active",
  "d_added": "2018-07-27 06:11:57",
  "d_modified": "2018-07-27 06:11:57"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b60831b544d05312b079f"),
  "v_name": "Other Support",
  "i_category_id": "5a65d5e2d3e8120c333c986f",
  "e_status": "active",
  "d_added": "2018-07-27 06:12:19",
  "d_modified": "2018-07-27 06:12:19"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b609d1b544d3a4b77edf5"),
  "v_name": "Customer Services",
  "i_category_id": "5a65d5edd3e81240363c986c",
  "e_status": "active",
  "d_added": "2018-07-27 06:12:45",
  "d_modified": "2018-07-27 06:12:45"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b60b41b544d38cf687b55"),
  "v_name": "Technical Support",
  "i_category_id": "5a65d5edd3e81240363c986c",
  "e_status": "active",
  "d_added": "2018-07-27 06:13:08",
  "d_modified": "2018-07-27 06:13:08"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b60ce1b544d2b3b78daeb"),
  "v_name": "Other",
  "i_category_id": "5a65d5edd3e81240363c986c",
  "e_status": "active",
  "d_added": "2018-07-27 06:13:34",
  "d_modified": "2018-07-27 06:13:34"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b60ec1b544d0cc469e8dd"),
  "v_name": "Advertising",
  "i_category_id": "5a607009d3e812063f3c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:14:04",
  "d_modified": "2018-07-27 06:14:04"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b61041b544d05312b07a0"),
  "v_name": "E-mail Marketing",
  "i_category_id": "5a607009d3e812063f3c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:14:28",
  "d_modified": "2018-07-27 06:14:28"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b611b1b544d3a4b77edf6"),
  "v_name": "Market & Customer Research",
  "i_category_id": "5a607009d3e812063f3c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:14:51",
  "d_modified": "2018-07-27 06:14:51"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b61311b544d38cf687b56"),
  "v_name": "Marketing Strategy",
  "i_category_id": "5a607009d3e812063f3c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:15:13",
  "d_modified": "2018-07-27 06:15:13"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b61441b544d2b3b78daec"),
  "v_name": "Social Media Marketing",
  "i_category_id": "5a607009d3e812063f3c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:15:32",
  "d_modified": "2018-07-27 06:15:32"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b615b1b544d0cc469e8de"),
  "v_name": "Other",
  "i_category_id": "5a607009d3e812063f3c9869",
  "e_status": "active",
  "d_added": "2018-07-27 06:15:55",
  "d_modified": "2018-07-27 06:15:55"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b61761b544d05312b07a1"),
  "v_name": "Accounting",
  "i_category_id": "5a65d605d3e81255333c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:16:22",
  "d_modified": "2018-07-27 06:16:22"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b61991b544d3a4b77edf7"),
  "v_name": "QuickBooks",
  "i_category_id": "5a65d605d3e81255333c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:16:57",
  "d_modified": "2018-07-27 06:16:57"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b61c41b544d0cc469e8df"),
  "v_name": "Financial Planning",
  "i_category_id": "5a65d605d3e81255333c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:17:40",
  "d_modified": "2018-07-27 06:17:40"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b61d51b544d05312b07a2"),
  "v_name": "Human Resources",
  "i_category_id": "5a65d605d3e81255333c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:17:57",
  "d_modified": "2018-07-27 06:17:57"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b623a1b544d3a3e631d16"),
  "v_name": "ACAS advice",
  "i_category_id": "5a65d605d3e81255333c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:19:38",
  "d_modified": "2018-07-27 06:19:38"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b62561b544d2b3b78daee"),
  "v_name": "Other Accounting",
  "i_category_id": "5a65d605d3e81255333c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:20:06",
  "d_modified": "2018-07-27 06:20:06"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b62821b544d05312b07a3"),
  "v_name": "Other",
  "i_category_id": "5a65d605d3e81255333c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:20:50",
  "d_modified": "2018-07-27 06:20:50"
});
db.getCollection("tbl_skills").insert({
  "_id": ObjectId("5b5b629b1b544d38cf687b57"),
  "v_name": "Start-up Advise",
  "i_category_id": "5a607698d3e812302b3c986b",
  "e_status": "active",
  "d_added": "2018-07-27 06:21:15",
  "d_modified": "2018-07-27 06:21:15"
});

/** tbl_sponsor_ad_fees records **/
db.getCollection("tbl_sponsor_ad_fees").insert({
  "_id": ObjectId("5a609b53d3e812062b3c986c"),
  "i_number": "123123",
  "e_type": "months",
  "f_price": "11",
  "i_impression_limit": "",
  "i_until_connected": "1",
  "i_order": "1",
  "e_status": "active",
  "d_added": "2018-01-18 01:04:19"
});

/** tbl_state records **/
db.getCollection("tbl_state").insert({
  "_id": ObjectId("5a609a7bd3e81237433c9869"),
  "v_title": "Gujarat",
  "i_country_id": "5a6099f0d3e812f51e3c986a",
  "e_status": "active",
  "d_added": "2018-01-18 01:00:43",
  "d_modified": "2018-01-18 01:00:43"
});
db.getCollection("tbl_state").insert({
  "_id": ObjectId("5a609a88d3e812704a3c9869"),
  "v_title": "UP",
  "i_country_id": "5a6099f0d3e812f51e3c986a",
  "e_status": "active",
  "d_added": "2018-01-18 01:00:56",
  "d_modified": "2018-01-18 01:00:56"
});
db.getCollection("tbl_state").insert({
  "_id": ObjectId("5a609a90d3e812b94b3c986a"),
  "v_title": "MP",
  "i_country_id": "5a6099f0d3e812f51e3c986a",
  "e_status": "active",
  "d_added": "2018-01-18 01:01:04",
  "d_modified": "2018-01-18 01:01:04"
});

/** tbl_support records **/
db.getCollection("tbl_support").insert({
  "_id": ObjectId("5b168d3776fbae6d674ff952"),
  "e_query": "technical_problem",
  "e_type": "inperson",
  "l_question": "What is Lorem Ipsum?",
  "e_priority": "low",
  "i_parent_id": "",
  "i_user_id": "5b16661a76fbae3d3f03e803",
  "e_status": "open",
  "e_view_status": "unread",
  "i_ticket_num": NumberInt(1),
  "d_added": "2018-06-05 13:16:39",
  "d_modified": "2018-06-05 13:16:39"
});
db.getCollection("tbl_support").insert({
  "_id": ObjectId("5b5873ae1b544d739208a842"),
  "e_query": "general_questions",
  "e_type": "inperson",
  "l_question": "dfgdfgdf",
  "e_priority": "medium",
  "i_parent_id": "",
  "i_user_id": "5b165e1376fbae3c3e66bd33",
  "e_status": "open",
  "e_view_status": "unread",
  "i_ticket_num": NumberInt(2),
  "d_added": "2018-07-25 12:57:18",
  "d_modified": "2018-07-25 12:57:18"
});

/** tbl_support_category records **/
db.getCollection("tbl_support_category").insert({
  "_id": ObjectId("5a658166d3e8121b0a3c9869"),
  "v_name": "In Person",
  "e_status": "active",
  "d_added": "2018-01-22 06:15:02"
});

/** tbl_users records **/
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b165e1376fbae3c3e66bd33"),
  "v_email": "raynanda.cis1@gmail.com",
  "password": "$2y$10$QyLbos.a86lO8k5.3tGfo.grKxgvBrNGs/5weMOvWBvS4JfN662b6",
  "i_newsletter": "on",
  "seller": {
    "v_service": "online",
    "e_status": "active"
  },
  "buyer": {
    "v_service": "online",
    "e_status": "active"
  },
  "v_plan": {
    "duration": "monthly",
    "id": "5a65b757d3e8125e323c986a",
    "d_start_date": "2018-07-10",
    "d_end_date": "2018-08-10"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "d_added": "2018-06-05 09:55:31",
  "d_modified": "2018-07-30 17;14:54",
  "v_image": "users/profile-1528192583.jpg",
  "billing_detail": {
    "v_type": "bank",
    "v_bankname": "",
    "v_account_holder_name": "",
    "v_ifsc": "",
    "v_accountno": "",
    "paypal_email": "raynanda.cis@gmail.com"
  },
  "v_fname": "Ray",
  "v_lname": "Nanda",
  "v_contact": "123456789",
  "i_country_id": "5af2949d76fbae1ce44d4e1b",
  "v_city": "Birmingham",
  "l_address": "22 Harborne Rd, Birmingham, B15 3AA",
  "v_postcode": "B15",
  "d_birthday": "2018-04-14",
  "v_gender": "male",
  "v_company_type": "Public limited company",
  "v_company_name": "Test Compnay",
  "v_vat_number": "sdfdsfdsfdsfds1s32df13ds21f32sd1",
  "v_company_regnumber": "dsf13sd1f3ds1f2ds1f3ds",
  "remember_token": "I4Lesrzjrdj8OACe1QPc5qcBi9pwDdxGu5HZ68xGzjVSlfgHbJRnq7uqVfp4",
  "i_total_avg_review": NumberInt(5),
  "i_total_review": NumberInt(1),
  "e_sponsor_profile_status": "pause"
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b16661a76fbae3d3f03e803"),
  "v_email": "katherineread.cis@gmail.com",
  "password": "$2y$10$w6hpocnV0skb7OCy8eqK0uzQYEvVTQKTARyXQaPPkZlSOxzRY45GO",
  "i_newsletter": "on",
  "seller": {
    "v_service": "online",
    "e_status": "active"
  },
  "buyer": {
    "v_service": "online",
    "e_status": "active"
  },
  "v_plan": {
    "duration": "monthly",
    "id": "5a65b48cd3e812a4253c9869"
  },
  "e_status": "active",
  "e_email_confirm": "yes",
  "d_added": "2018-06-05 10:29:46",
  "d_modified": "2018-06-06 06:11:41",
  "v_image": "users/profile-1528204528.jpg",
  "billing_detail": {
    "v_type": "paypal",
    "paypal_email": "katherineread.cis@gmail.com",
    "v_bankname": "",
    "v_accountno": "",
    "v_ifsc": "",
    "v_account_holder_name": ""
  },
  "v_fname": "Katherine",
  "v_lname": "Read",
  "v_contact": "9874563211",
  "i_country_id": "5a609a68d3e81234473c9869",
  "v_city": "Muva",
  "l_address": "F/16/17, Commerce Ctr., 78 Tardeo Road, Tardeo",
  "v_postcode": "400034",
  "d_birthday": "2018-06-06",
  "v_gender": "female",
  "v_company_type": "Private company limited",
  "v_company_name": "Xyz  pvt ltd",
  "v_vat_number": "400034",
  "v_company_regnumber": "400034",
  "remember_token": "K28Te6cMcy2sqIcjcF6y7CQmN0O3wUmIwJ31mEzXtvaRZVotIRPEyV5tp7ML",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0)
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b1666cd76fbae3d393c9a04"),
  "v_email": "louisbland.cis@gmail.com",
  "password": "$2y$10$El8KKMi6rPa7vdn0stTe.u8KR4g.8Pn70Y/HIcaWIc2ivaRGDwAqW",
  "i_newsletter": "on",
  "buyer": {
    "v_service": "online",
    "e_status": "active"
  },
  "seller": {
    "v_service": "online",
    "e_status": "active"
  },
  "v_plan": {
    "duration": "monthly",
    "id": "5a65b48cd3e812a4253c9869"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "d_added": "2018-06-05 10:32:45",
  "d_modified": "2018-06-06 10:03:20",
  "v_image": "users/profile-1528194789.jpg",
  "v_fname": "Louis",
  "v_lname": "Bland",
  "v_contact": "1234567890",
  "i_country_id": "5af28fd876fbae0a9e637175",
  "v_city": "London",
  "l_address": "338-346 goswell road, London, London",
  "v_postcode": "EC1V 7LQ",
  "d_birthday": "2009-01-27",
  "v_gender": "male",
  "v_company_type": "Private company limited",
  "v_company_name": "Google",
  "v_vat_number": "123456",
  "v_company_regnumber": "1234567890",
  "billing_detail": {
    "v_type": "bank",
    "v_bankname": "",
    "v_account_holder_name": "",
    "v_ifsc": "",
    "v_accountno": "",
    "paypal_email": "mark.hoey@premiercore.com"
  },
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0)
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b178a6876fbae1c6643b022"),
  "v_email": "maisybell.cis@gmail.com",
  "password": "$2y$10$rxFOIjy6.tih7aggSAVWdeRqqQSUYHssuk92aICMyIgcBM/GdSPz2",
  "i_newsletter": "on",
  "buyer": {
    "v_service": "inperson",
    "e_status": "active"
  },
  "seller": {
    "v_service": "inperson",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-06-06",
    "d_end_date": "2029-05-19"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "d_added": "2018-06-06 07:16:56",
  "d_modified": "2018-06-06 07:16:56",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0)
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b28f27e76fbae48a0579b64"),
  "v_email": "ddavid@gmail.com",
  "password": "$2y$10$VlBo.s8N5oI6eP3sQtnGu.OFJ.rvBMJMKESnJ70ZvhptihjliMFdK",
  "i_newsletter": "on",
  "seller": {
    "v_service": "inperson",
    "e_status": "active"
  },
  "buyer": {
    "v_service": "inperson",
    "e_status": "inactive"
  },
  "v_plan": {
    "duration": "monthly",
    "id": "5a65b48cd3e812a4253c9869"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "d_added": "2018-06-19 12:09:34",
  "d_modified": "2018-06-19 12:46:23",
  "billing_detail": {
    "v_type": "paypal",
    "v_bankname": "",
    "v_account_holder_name": "",
    "v_ifsc": "",
    "v_accountno": "",
    "paypal_email": "testalt@alt.com"
  },
  "remember_token": "OYNE4jv2awHtlmMnO0NVzEI1EizJMYLOWPHCnOMypDMRPUmAFkyOz8IOCm0w",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0)
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b28fc8a76fbae5e617708f3"),
  "v_email": "tessssstaccount@gmail.com",
  "password": "$2y$10$kknaJAiYZC9urUu.EF7o.evU67dPvYNHGQE3OvDyzVC6RqGTpkq5S",
  "i_newsletter": "on",
  "seller": {
    "v_service": "courses",
    "e_status": "active"
  },
  "buyer": {
    "v_service": "courses",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-06-19",
    "d_end_date": "2029-06-01"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "d_added": "2018-06-19 12:52:26",
  "d_modified": "2018-06-19 12:52:26",
  "remember_token": "BfQKxiWqsDAcm8TywYDBk4ZxCpFKd8xD93Uh1JlGgZRBDOcmJZ1sqCukC5LQ",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0)
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b29006576fbae72fd1d9562"),
  "v_email": "sadsadsadsa@gmail.com",
  "password": "$2y$10$R8f8S0Nodeqgag4.dC3iEePA7WrpkF.LukKjPTlkWy971Tn42WJDe",
  "i_newsletter": "on",
  "seller": {
    "v_service": "online",
    "e_temptype": "courses",
    "e_status": "active"
  },
  "buyer": {
    "v_service": "online",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-06-19",
    "d_end_date": "2029-06-01"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "d_added": "2018-06-19 13:08:53",
  "d_modified": "2018-06-19 13:08:53",
  "remember_token": "tr0KiSDm67BPLDNBXzCQymwxUsaws254wFQIj8Ts5iq8Q2dx5KzyQFk4MPHa",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0)
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b29020f76fbae73f3225f33"),
  "v_email": "dfsdfsafsdfds@gmail.com",
  "password": "$2y$10$lrTqruYtmhjXr0bBH06rPe07FLS.U.IzzmRHaZw3mo0Pz8.6Zn4Na",
  "i_newsletter": "on",
  "seller": {
    "v_service": "online",
    "e_temptype": "courses",
    "e_status": "active"
  },
  "buyer": {
    "v_service": "online",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-06-19",
    "d_end_date": "2029-06-01"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "d_added": "2018-06-19 13:15:59",
  "d_modified": "2018-06-19 13:15:59",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0)
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b5751a1516b460db00053ea"),
  "v_email": "asdsadw@gmail.com",
  "password": "$2y$10$HB52vo2LOLfEIBFvROpN1.IDwR5s44IMqMpX5p5yMDOJnjd/O4Me2",
  "i_newsletter": "on",
  "seller": {
    "v_service": "online",
    "e_status": "active"
  },
  "buyer": {
    "v_service": "online",
    "e_status": "inactive"
  },
  "v_plan": {
    "duration": "monthly",
    "id": "5a65b48cd3e812a4253c9869"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "d_added": "2018-07-24 16:19:45",
  "d_modified": "2018-07-24 16:25:55",
  "billing_detail": {
    "v_type": "paypal",
    "v_bankname": "",
    "v_account_holder_name": "",
    "v_ifsc": "",
    "v_accountno": "",
    "paypal_email": "sdsadsadsa@gmail.com"
  },
  "d_birthday": "2018-07-18",
  "i_country_id": "5af28f3b76fbae0a9e637174",
  "l_address": "aSAsaSAs",
  "v_city": "asdasASAs",
  "v_company_name": "aSAsaSA",
  "v_company_regnumber": "a1s32d1as32d1as32d1as",
  "v_company_type": "Public limited company",
  "v_contact": "132132132",
  "v_fname": "asdsa",
  "v_gender": "male",
  "v_lname": "dasdas",
  "v_postcode": "123156",
  "v_vat_number": "saSAss0a30sa321S3A21",
  "remember_token": "L26DB5z5dBSpI4HogjOMf3XGvkIreWIcqAPHrnI9UaRX7SxTdqoYw94VgM7u",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0)
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b5864031b544d14da26abb4"),
  "v_email": "raynanda.cis@gmail.com",
  "password": "$2y$10$Dw0G.cU.DAR5DEyB4DJ67.KkZk1UxvIv36mzwIF0HhhGoI9FceWIi",
  "i_newsletter": "on",
  "buyer": {
    "v_service": "online",
    "e_status": "active"
  },
  "seller": {
    "v_service": "online",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-07-25",
    "d_end_date": "2029-07-07"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "d_added": "2018-07-25 11:50:27",
  "d_modified": "2018-07-25 11:50:27",
  "remember_token": "ROetTD9whRzn1IvyeGBPD473qJrXlTI0adDPgEd1F73PUw8jPRujkByBVAv4"
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b5b617f1b544d2b3b78daed"),
  "v_email": "dasaswdsd@gmail.com",
  "password": "$2y$10$.D3aKh9wyw2N/P3D3uXs.O46oPdLw7AA.Fj12tf/cGCaz6IyhYX0q",
  "i_newsletter": "on",
  "seller": {
    "v_service": "online",
    "e_status": "active"
  },
  "buyer": {
    "v_service": "online",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-07-27",
    "d_end_date": "2029-07-09"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "d_added": "2018-07-27 18:16:31",
  "d_modified": "2018-07-27 18:16:31"
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b5f545e1b544d12353e0092"),
  "v_email": "asdasdsadsa@gmail.com",
  "password": "$2y$10$..JKE6m.NsRb9/eGm/T9fOMrxL36kxDuVyAKPHrOzokgxshf0k9XO",
  "i_newsletter": "on",
  "buyer": {
    "v_service": "online",
    "e_status": "active"
  },
  "seller": {
    "v_service": "online",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-07-30",
    "d_end_date": "2029-07-12"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "d_added": "2018-07-30 18:09:34",
  "d_modified": "2018-07-30 18:09:34"
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b608d851b544d0de54cd002"),
  "v_email": "abhdsd@gmail.com",
  "password": "$2y$10$QU0yvascZzKskJketWqZ/.zpx.IHqmEg6bNNOEh1YKaaDZ6V3ejZO",
  "i_newsletter": "on",
  "buyer": {
    "v_service": "online",
    "e_status": "active"
  },
  "seller": {
    "v_service": "online",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-07-31",
    "d_end_date": "2029-07-13"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "d_added": "2018-07-31 16:25:40",
  "d_modified": "2018-07-31 16:25:40",
  "i_mangopay_id": "52861695",
  "billing_detail": {
    "v_OwnerName": "sadsadsa",
    "v_AccountNumber": "11696419",
    "v_SortCode": "010039",
    "v_AddressLine1": "123123123",
    "v_AddressLine2": "123123",
    "v_City": "123123",
    "v_Country": "fr",
    "v_PostalCode": "123546",
    "v_Region": "Region"
  },
  "i_wallet_id": "52863032",
  "i_bank_id": "52863263"
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b649aef1b544d15442d7d93"),
  "v_email": "asdsasaweqw@gmail.com",
  "password": "$2y$10$JRRl/OYhAwiBKB/VD.jlj.EjlmVjhbMuJsMgvjtAbRr8VhRno3XrO",
  "i_newsletter": "on",
  "buyer": {
    "v_service": "online",
    "e_status": "active"
  },
  "seller": {
    "v_service": "online",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-08-03",
    "d_end_date": "2029-07-16"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "d_added": "2018-08-03 18:11:56",
  "d_modified": "2018-08-03 18:11:56",
  "i_mangopay_id": "53044855"
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b686c1e1b544d0d8322f932"),
  "v_email": "sadsdsawewa@gmail.com",
  "password": "$2y$10$uUe.9Uzrj8GeTL7T5zHhLOLtNA/T5BhkkIQYDtZbELP7d95ZWE4Eu",
  "i_newsletter": "on",
  "buyer": {
    "v_service": "online",
    "e_status": "active"
  },
  "seller": {
    "v_service": "online",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-08-06",
    "d_end_date": "2029-07-19"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "d_added": "2018-08-06 15:41:18",
  "d_modified": "2018-08-06 15:41:18",
  "i_mangopay_id": NumberInt(0),
  "remember_token": "akmYcfUFbyUMzlv4ZQ13cIDkV8JMwcbuoHKmaVxIpP6RJaFiDZpgQEcPa2Zb"
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b686ea31b544d0d8322f933"),
  "v_email": "skhdsajkdhkasjh@gmail.com",
  "password": "$2y$10$zwSMlpz/mY7y/PHsWhxr5efe/MTXGfF/xHUDU8kOEdsWlz7ED5onm",
  "i_newsletter": "on",
  "buyer": {
    "v_service": "online",
    "e_status": "active"
  },
  "seller": {
    "v_service": "online",
    "e_status": "inactive"
  },
  "v_plan": {
    "id": "5a65b48cd3e812a4253c9869",
    "duration": "monthly",
    "d_start_date": "2018-08-06",
    "d_end_date": "2029-07-19"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "d_added": "2018-08-06 15:52:03",
  "d_modified": "2018-08-06 15:52:03",
  "i_mangopay_id": NumberInt(0),
  "remember_token": "Z8A5B0nkDroKAiWeAEnvdlhqkEh93ltA1rQwi7ELdHrdJcGcXqdxf55ibUD3"
});
db.getCollection("tbl_users").insert({
  "_id": ObjectId("5b6beb561b544d16406dc482"),
  "v_email": "ronakldhunt.cis@gmail.com",
  "password": "$2y$10$SNqHAHMF6mYMurTF0pQUCejsdrpBiweKQk7yCNhFx1eMTwU0PXgNu",
  "i_newsletter": "on",
  "buyer": {
    "v_service": "online",
    "e_status": "active"
  },
  "seller": {
    "v_service": "online",
    "e_status": "active"
  },
  "v_plan": {
    "duration": "monthly",
    "id": "5a65b48cd3e812a4253c9869"
  },
  "e_status": "active",
  "e_email_confirm": "no",
  "i_total_avg_review": NumberInt(0),
  "i_total_review": NumberInt(0),
  "d_added": "2018-08-09 07:20:54",
  "d_modified": "2018-08-09 07;33:22",
  "i_mangopay_id": NumberInt(0),
  "remember_token": "ZipYviVYSHg0JQkIsSHe9h7W6AfGjZ9M6MbFWTs1nHaQ6lPt1wkRubrnfRoG"
});
