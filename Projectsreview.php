<?php

namespace App\Http\Controllers\Admin\PMS;

use Validator, Session, Request, DB, Auth, Lang,Redirect,File;
use App\Models\Adminsystem\Admin;
use App\Models\Configuration\Sitesettings;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Hash;

use PHPMailerAutoload; 
use PHPMailer;

use App\Models\PMS\Projects as ProjectsModel;
use App\Models\HR\Employees as EmployeesModel;
use App\Models\PMS\ProjectsReview as ProjectsReviewModel;

use App\Http\Helpers\General;
use App\Http\Helpers\MailTemplate;



class Projectsreview extends Controller {

    
    public function index($project_id) {

        if(!getRights("PROJECTSREVIEW","iList")){
            return redirect( 'admin/dashboard/')->with( 'warning', 'You Dont Have Authorization For This Section.');
        }

        $projectData = ProjectsModel::query();
        $projectData = $projectData->where('id', $project_id);
        $projectData = $projectData->first();

        $emp_ids = [];
        if(isset($projectData) && count($projectData)){
          $assignEmployee = $projectData['vAssigned'];

          if($assignEmployee != ''){
            $emp_ids = explode(',', $assignEmployee);
          }
        }


        $employees = DB::table('tbl_employees')->orderBy("vName","ASC")->whereIn('id',$emp_ids)->get();
        $employeesList=array();
        
        if(count($employees)){
          foreach ($employees as $key =>$value) {
              $value=(array)$value;
              $employeesList[$value['id']]=$value['vName'];
          }
        }

       
        // $projectReviewData = ProjectsReviewModel::query();
        // $projectReviewData = $projectReviewData->where('i_project_id', $project_id);  
        // $projectReviewData = $projectReviewData->get();

        $projectReviewData = ProjectsReviewModel::query();
        $projectReviewData = $projectReviewData->selectRaw('
                            tbl_projects_review.*,
                            t1.vName as empName,
                            t1.id as empid
                        ');
        $projectReviewData = $projectReviewData->leftJoin('tbl_employees as t1', 't1.id', '=', 'tbl_projects_review.i_user_id');
        $projectReviewData = $projectReviewData->where('i_project_id', $project_id);
        $projectReviewData = $projectReviewData->get();

        $projectReviewDataList = [];
        if(isset($projectReviewData) && count($projectReviewData)){

            foreach ($projectReviewData as $key => $value) {
              $projectReviewDataList[$value->i_user_id] = $value;
            }
        }
        
        $data=array(
            '_action'=>"list",
            '_projectData' => $projectData,
            '_projectReviewDataList'=>$projectReviewDataList,
            '_emp_ids'=>$emp_ids,
            '_employeesList' => $employeesList,
            '_project_id' => $project_id
        );


       
        return view('Admin/PMS/projectsreview', $data);
    }




    public function Add($project_id) { 
      $post_data = Request::all();   
      
      if(!getRights("PROJECTSREVIEW","iEdit")){
          return redirect( 'admin/dashboard/')->with( 'warning', 'You Dont Have Authorization For This Section.');
      }


      if(isset($post_data['hidden']) && !empty($post_data['hidden'])){

          $empId= $empType ="";
          if (auth()->guard('admin')->check()) {
              $empId=Auth::guard('admin')->user()->id;
              $empType = 'admin';
          }else{
              $empId=Auth::guard('employees')->user()->id;
              $empType = 'emp';
          }


          $create = [];
          foreach ($post_data['hidden'] as $key => $value) {
              $update = [];
              if($value != ''){
                  $update = [

                      'i_project_id' => $project_id,
                      'i_user_id' => $key,
                      'i_understanding_star' => isset($post_data['i_understanding_star'][$key]) ? $post_data['i_understanding_star'][$key] : '',
                      'i_code_star' => isset($post_data['i_code_star'][$key]) ? $post_data['i_code_star'][$key] : '',
                      'i_logic_star' => isset($post_data['i_logic_star'][$key]) ? $post_data['i_logic_star'][$key] : '',
                      'l_comments' => isset($post_data['l_comments'][$key]) ? $post_data['l_comments'][$key] : '',
                      'i_added_by_type' => $empType,
                      'i_added_by_id' => $empId,
                      'd_added' => date("Y-m-d h:i:s"),
                      'd_modified' => date("Y-m-d h:i:s"),
                  ];

                  ProjectsReviewModel::find($value)->update($update );
              }
              else{
                $create[]= [

                      'i_project_id' => $project_id,
                      'i_user_id' => $key,
                      'i_understanding_star' => isset($post_data['i_understanding_star'][$key]) ? $post_data['i_understanding_star'][$key] : '',
                      'i_code_star' => isset($post_data['i_code_star'][$key]) ? $post_data['i_code_star'][$key] : '',
                      'i_logic_star' => isset($post_data['i_logic_star'][$key]) ? $post_data['i_logic_star'][$key] : '',
                      'l_comments' => isset($post_data['l_comments'][$key]) ? $post_data['l_comments'][$key] : '',
                      'i_added_by_type' => $empType,
                      'i_added_by_id' => $empId,
                      'd_added' => date("Y-m-d h:i:s"),
                      'd_modified' => date("Y-m-d h:i:s"),
                  ];
              }
          }
          
          if(isset($create) && count($create)){

            ProjectsReviewModel::insert($create);
          }
      }
      return redirect( 'admin/projects-review/'.$project_id)->with( 'success', 'successfully Add timemanagement.' ); 
    }

    
}