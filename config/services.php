<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    // Local Detail 
    // 'facebook' => [
    //     'client_id' => '196021424488808',
    //     'client_secret' => '587914c5828b38ada1b32fbcf1011b80',
    //     'redirect' => 'http://192.168.0.228/projects/skillbox/facebook/callback',
    // ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => url('facebook/callback'),
    ],
   
    //Live Detail 
    // 'facebook' => [
    //     'client_id' => '299049847617348',
    //     'client_secret' => 'ef013dcd563fa6b21bdbf3f8a0d343e3',
    //     'redirect' => 'https://www.skillbox.co.uk/dev/facebook/callback',
    // ],
    
    // 'mangopay' => [
    //   'env'    => "sandbox",//env('MANGOPAY_ENV', 'sandbox'),  // or "production"
    //   'key'    => "godfather",//env('MANGOPAY_KEY'),             // your Mangopay client ID
    //   'secret' => "T11BKopLwJDhBodvKActsfcFHV9qG6otwZc8Mpp4obN9Jtp4gg",//env('MANGOPAY_SECRET'),          
    //   // your Mangopay client password
    // ],

    'mangopay' => [
      'env'    => env('MANGOPAY_ENV'),              // or "production"
      'key'    => env('MANGOPAY_KEY'),             // your Mangopay client ID
      'secret' => env('MANGOPAY_SECRET'),          // your Mangopay client password
    ],

    // 'mangopay' => [
    //   'env'    => "production",
    //   'key'    => "skillbox",
    //   'secret' => "ZQ1eg0kaHuK2DEnGRKBqjU9ionPeitRKiCqJRAGZDBaxuoHmDc",
     
    // ],


   
];
